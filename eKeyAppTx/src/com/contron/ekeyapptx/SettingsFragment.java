/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.profile.HelperFragment;
import com.contron.ekeyapptx.user.ModifyPwdFragment;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.common.OnToggleFragmentListener;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.ConfigUtil;
import com.contron.ekeypublic.util.VersionUtil;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SettingsFragment extends Fragment {
    public static final String KEY_VERSION_ADDRESS = "key_version_address";
    public static final String KEY_POST = "key_post";
    public static final String SOCKET_POST = "Socket_post";
    public static final String KEY_WSDL_ADDRESS = "key_wsdl_address";
    public static final String KEY_SOCKET_ADDRESS = "key_socket_address";
    public static final String KEY_RECLEN_TIME = "key_reclen_time";
    public static final String KEY_IP_ADDRESS = "key_ip_address";
    public static final String KEY_UNLOCK_TYPE = "key_unlock_type";

    public static final String UNLOCK_BLUETOOTH = "unlock_bluetooth";
    public static final String UNLOCK_OTHER = "unlock_other";

    private ConfigUtil configUtil;
    @ViewInject(R.id.settings_et_ip)
    private EditText etIP;// 服务器IP地址
    @ViewInject(R.id.settings_et_port)
    private EditText etPort;// 服务端口

    @ViewInject(R.id.settings_et_socket_port)
    private EditText etSocketPort;// Socket端口

    @ViewInject(R.id.settings_et_reclen)
    private EditText etReclen;// 注销时长
    @ViewInject(R.id.settings_btn_modify_pwd)
    private Button btnModifyPwd;

    @ViewInject(R.id.settings_btn_picklocation)
    private Button picklocation;// 采集位置

    @ViewInject(R.id.settings_btn_unlocktype)
    private Button btnUnlockType;// 采集位置

    @ViewInject(R.id.tx_unlocktype)
    private TextView txUnlockType;// 采集位置

    @ViewInject(R.id.settings_btn_initdata)
    private Button initdata;// 初始化数据

    private BasicActivity mActivity;
    private OnToggleFragmentListener mCallBack;
    private RequestHttp requestHttp;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallBack = (OnToggleFragmentListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BasicActivity) getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container,
                false);
        ViewUtils.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        configUtil = ConfigUtil.getInstance(mActivity);

        //默认设置

        // 从配置文件中读取设置

        // IP地址
        String ip = configUtil.getString(KEY_IP_ADDRESS);
        if (!TextUtils.isEmpty(ip)) {
            etIP.setText(ip);
        }

        // 服务端口
        String port = configUtil.getString(KEY_POST);
        if (!TextUtils.isEmpty(port)) {
            etPort.setText(port);
        }

        // socket端口
        String socketPort = configUtil.getString(SOCKET_POST);
        if (!TextUtils.isEmpty(socketPort)) {
            etSocketPort.setText(socketPort);
        }

        // 自动注销时长
        String strReclenTime = configUtil.getString(KEY_RECLEN_TIME);
        if (!TextUtils.isEmpty(strReclenTime))
            etReclen.setText(strReclenTime);

        // 没登录则隐藏部分功能
        if (!EkeyAPP.getInstance().isLoginStatus()) {
            btnModifyPwd.setVisibility(View.GONE);
            picklocation.setVisibility(View.GONE);
            initdata.setVisibility(View.GONE);
            btnUnlockType.setVisibility(View.GONE);
            txUnlockType.setVisibility(View.GONE);
        } else {// 登录则禁用IP\端口输入
            etIP.setEnabled(false);
            etPort.setEnabled(false);
            etSocketPort.setEnabled(false);
//			etReclen.setEnabled(false);
            startUnLockConfigTask();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item2_name);

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden)
            onResume();
    }

    private void saveSetting() {

        // web服务
        String serviceAddress = etIP.getText().toString() + ":"
                + etPort.getText().toString();

        // socket服务
        String socketAddress = etIP.getText().toString() + ":"
                + etSocketPort.getText().toString();

        // 初始WebService地址
        configUtil.setString(KEY_WSDL_ADDRESS, "http://" + serviceAddress);

        // 访问socket服务地址
        configUtil.setString(KEY_SOCKET_ADDRESS, "http://" + socketAddress);

        // IP地址
        configUtil.setString(KEY_IP_ADDRESS, etIP.getText().toString());

        // 服务端口
        configUtil.setString(KEY_POST, etPort.getText().toString());

        // socket端口
        configUtil.setString(SOCKET_POST, etSocketPort.getText().toString());

        // 初始APP升级版本文件地址
        configUtil.setString(KEY_VERSION_ADDRESS,
                String.format(Globals.VERSION_URL, serviceAddress));

        // 初始注销时长
        configUtil.setString(KEY_RECLEN_TIME, etReclen.getText().toString());
    }

    @OnClick({R.id.settings_btn_initdata, R.id.settings_btn_modify_pwd,
            R.id.settings_btn_about, R.id.settings_btn_picklocation,
            R.id.settings_btn_locktype, R.id.settings_btn_unlocktype,
            R.id.settings_btn_help, R.id.settings_btn_update})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.settings_btn_initdata:// 初始化数据
                if (etIP.getText().toString().isEmpty()
                        || etPort.getText().toString().isEmpty()) {
                    mActivity.showToast(R.string.shared_edittext_empty);
                    return;
                }

                EkeyAPP.getInstance().initData(mActivity, true, true, true);// 下载初始化数据
                break;
            case R.id.settings_btn_modify_pwd:// 修改密码
                mCallBack.hideAdd(SettingsFragment.class,
                        ModifyPwdFragment.newInstance());
                break;
            case R.id.settings_btn_locktype:// 设置蓝牙锁类型
                // mCallBack.hideAdd(SettingsFragment.class,
                // SettingLockTypeFragment.newInstance());
                break;
            case R.id.settings_btn_picklocation:// 采集锁经度纬度
                PickLocationActivity.gotoActivity(mActivity);
                break;
            case R.id.settings_btn_unlocktype:// 采集锁经度纬度
//			showUnlocktypeDialog();
                break;
            case R.id.settings_btn_about:// 关于
                // mCallBack.hideAdd(SettingsFragment.class,
                // AboutFragment.newInstance());
                mActivity.showMsgBox(
                        "关于",
                        getString(R.string.about_company,
                                VersionUtil.getVersionName(mActivity)),
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                            }
                        });
                break;
            case R.id.settings_btn_help: //帮助
                mCallBack.hideAdd(SettingsFragment.class, HelperFragment.newInstance());
                break;
            case R.id.settings_btn_update: //系统更新
                mCallBack.hideAdd(SettingsFragment.class, UpdateFragment.newInstance());
                break;
            default:
                break;
        }
    }

    private void showUnlocktypeDialog() {
        final Dialog dialog = new Dialog(getActivity());

        View contentView = LayoutInflater.from(getActivity()).inflate(R.layout.single_choice_dialog_layout, null, false);
        dialog.setTitle("选择解锁类型");
        dialog.setContentView(contentView);
        RadioButton bluetoothRadio = (RadioButton) contentView.findViewById(R.id.bluetooth_radio);
        RadioButton otherRadio = (RadioButton) contentView.findViewById(R.id.other_radio);
        boolean isUseBluetoothUnlock = UNLOCK_BLUETOOTH.equals(configUtil.getString(KEY_UNLOCK_TYPE));
        bluetoothRadio.setChecked(isUseBluetoothUnlock);
        otherRadio.setChecked(!isUseBluetoothUnlock);
        bluetoothRadio.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                configUtil.setString(KEY_UNLOCK_TYPE, UNLOCK_BLUETOOTH);
                dialog.dismiss();
            }
        });

        otherRadio.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                configUtil.setString(KEY_UNLOCK_TYPE, UNLOCK_OTHER);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.save, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBack();
                break;
            case R.id.menu_save:
                if (etIP.getText().toString().isEmpty()
                        || etPort.getText().toString().isEmpty()
                        || etSocketPort.getText().toString().isEmpty()
                        || etReclen.getText().toString().isEmpty()) {
                    mActivity.showToast(R.string.shared_edittext_empty);
                    return false;
                }

                // 判断IP格式
                if (!judgeIP(etIP.getText().toString())) {
                    mActivity.showToast("ip地址格式不正确,请检查！");
                    return false;
                }

                // 判断服务端口范围
                if (!judgePort(etPort.getText().toString())) {
                    mActivity.showToast("服务端口不在正常范围1~65535内!");
                    return false;
                }

                // 判断socket端口范围
                if (!judgePort(etSocketPort.getText().toString())) {
                    mActivity.showToast("推送端口不在正常范围1~65535内!");
                    return false;
                }

                String reclenTime = etReclen.getText().toString();
                // 第一位不能为0
                if (reclenTime.indexOf("0") == 0) {
                    mActivity.showToast("请检查自动注销时长,正常范围为1~99");
                    return false;
                }

                saveSetting();
                logout();
                mActivity.showToast("只有在后台退出程序，保存的数据才会生效！");
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onBack() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            mActivity.getSupportFragmentManager().popBackStack();
        else
            mActivity.finish();
    }

    private void logout() {
        EkeyAPP.getInstance().exitRecLen();
    }

    /**
     * 判断port格式
     */
    private boolean judgePort(String port) {

        if (port.isEmpty()) {
            return false;
        }
        // 第一位不能为0
        if (port.indexOf("0") == 0)
            return false;

        int intPort = Integer.parseInt(port);

        if (intPort > 65535 || intPort < 0) {
            return false;
        }

        return true;
    }

    /**
     * 判断IP格式
     */
    private boolean judgeIP(String ip) {

        if (ip.isEmpty() || !ip.contains(".")) {
            return false;
        }

        int cnt = 0;
        int offset = 0;
        while ((offset = ip.indexOf(".", offset)) != -1) {
            offset = offset + 1;
            cnt++;
        }

        // 包含.个数
        if (cnt != 3)
            return false;

        String[] iparray = ip.split("\\.");

        if (iparray.length != 4)
            return false;

        // 判断按.分割后的数
        for (String splitIp : iparray) {

            if (TextUtils.isEmpty(splitIp))
                return false;

            if (splitIp.length() > 1 && splitIp.indexOf("0") == 0)
                return false;

            int number = Integer.parseInt(splitIp);
            if (number < 0 || number > 255)
                return false;
        }

        return true;

    }

    private void startUnLockConfigTask() {
        User user = mActivity.getUser();
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框
        // 设置参数
        RequestParams params = new RequestParams(user);
        requestHttp.doPost(Globals.CONF_UNLOCK_ORDER, params,
                new RequestHttp.HttpRequestCallBackString() {

                    @Override
                    public void resultValue(final String value) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mActivity.dismissDialog();
                                if (value.contains("success")) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(value);
                                        JSONArray jsonArray = jsonObject
                                                .getJSONArray("items");
                                        if (jsonArray.length() == 0) return;
                                        String lockType = jsonArray.getString(0);
                                        txUnlockType.setText(lockType);
                                        if ("蓝牙".equals(lockType)) {
                                            configUtil.setString(KEY_UNLOCK_TYPE, UNLOCK_BLUETOOTH);
                                        } else if ("智能锁".equals(lockType)) {
                                            configUtil.setString(KEY_UNLOCK_TYPE, UNLOCK_OTHER);
                                        } else {
                                            configUtil.setString(KEY_UNLOCK_TYPE, UNLOCK_OTHER);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    try {
                                        JSONObject json = new JSONObject(value);
                                        String error = json.getString("error");
                                        mActivity.showMsgBox(error);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }
                        });

                    }
                });
    }

}
