package com.contron.ekeyapptx;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.User;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

public class SelectUserActivity extends BasicActivity {

	public static int REQUESTCODE = 3;
	public static int RESULTCODE = 4;

	@ViewInject(R.id.list_user)
	private ListView mlistUser;

	@ViewInject(R.id.searchView_user)
	private SearchView sear;// 查询控件

	private List<User> allListUser = new ArrayList<User>();// 所用用户集合
	private List<User> tempListUser = new ArrayList<User>();// 临时显示用户集合
	private ContronAdapter<User> mContronAdapter;// 适配器
	private BasicActivity mActivity;
	private Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_user);
		ViewUtils.inject(this);
		EkeyAPP.getInstance().addActivity(this);
		mActivity = this;
		intent = getIntent();
		getBasicActionBar().setTitle("选择操作人");

		mContronAdapter = new ContronAdapter<User>(this,
				R.layout.item_selectuser, tempListUser) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					final User item, int position) {
				viewHolder.setText(R.id.tv_name, item.getName());// 用户名
				viewHolder.setText(R.id.tv_username, item.getUsername());// 用户手机
				viewHolder.setText(R.id.tv_section, item.getSection());// 机构

				final RadioButton radbutSelectUser = viewHolder
						.findView(R.id.radbut_ok);

				radbutSelectUser.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						intent.putExtra("user", item);
						mActivity.setResult(RESULTCODE, intent);
						mActivity.finish();
					}
				});
			}
		};
		mlistUser.setAdapter(mContronAdapter);
		sear.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String arg0) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean onQueryTextChange(String arg0) {
				setFilter(arg0);
				return false;
			}
		});// 设置sear控件的查询事件。

		getData();// 从本地获取用户数据
	}

	/**
	 * 从数据库存读取用户数据
	 */
	private void getData() {
		// 获取数据
		allListUser.clear();// 先清空数据
		allListUser.addAll(DBHelp.getInstance(this).getEkeyDao().getAllUser());
		tempListUser.addAll(allListUser);
		mContronAdapter.notifyDataSetChanged();
	}

	/**
	 * 界面跳转
	 * 
	 * @param context
	 */
	public static void startActivityForResult(Fragment fragment) {
		fragment.startActivityForResult(new Intent(fragment.getActivity(),
				SelectUserActivity.class), REQUESTCODE);
	}

	/***
	 * 根据传入字符搜索
	 * 
	 * @param str
	 */

	private void setFilter(String str) {
		tempListUser.clear();

		if (str.equals("")) {
			tempListUser.addAll(allListUser);
		} else {
			for (int i = 0; i < allListUser.size(); i++) {
				User user = allListUser.get(i);
				if (user.getName().contains(str)
						|| user.getUsername().contains(str)) {
					tempListUser.add(user);
				}

			}
		}
		mContronAdapter.notifyDataSetChanged();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.picklocation, menu);
		//
		// MenuItem searchItem = menu.findItem(R.id.action_search); //
		// 获取菜单中的R.id.action_search菜单控件。
		// sear = (SearchView) searchItem.getActionView();//
		// 通过searchItem菜控件的getActionView()方法获取SearchView对象。
		// sear.setQueryHint("输入用户名及手机号进行搜索");// 设置sear控件展开时显示的文本。
		//
		// sear.setOnQueryTextListener(new OnQueryTextListener() {
		//
		// @Override
		// public boolean onQueryTextSubmit(String arg0) {
		// // TODO Auto-generated method stub
		// return false;
		// }
		//
		// @Override
		// public boolean onQueryTextChange(String arg0) {
		// setFilter(arg0);
		// return false;
		// }
		// });// 设置sear控件的查询事件。
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
