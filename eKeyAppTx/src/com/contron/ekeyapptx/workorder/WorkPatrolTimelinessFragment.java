package com.contron.ekeyapptx.workorder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.map.InfoWindowActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.SectionAll;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.WorkPatrolTimeliness;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.view.SelectQueryWorkpatrolDialog;
import com.contron.ekeypublic.view.SelectQueryWorkpatrolDialog.SelectListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 巡检率
 * 
 * @author luoyilong
 *
 */
public class WorkPatrolTimelinessFragment extends Fragment {
	@ViewInject(R.id.lv_public)
	private ListView lisview;

	@ViewInject(R.id.tx_empty)
	private TextView empty;
	
	@ViewInject(R.id.workpatrol_timeliness_head)
	private LinearLayout invis;
	
	private List<Dict> mListYear = new ArrayList<Dict>();
	private List<SectionAll> mListAgency = new ArrayList<SectionAll>();
	private List<Dict> mListCycle = new ArrayList<Dict>();

	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求对象
	private ContronAdapter<WorkPatrolTimeliness> mAdapter;// 适配器
	private View loadMoreView;// 下一页视图
	private View headerView;// 表头
	private RadioButton loadMoreButton;// 下一页按钮
	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数
	private User user = null;
	private JSONObject queryJsonObject;// 后台请求参数

	private List<WorkPatrolTimeliness> workPatrolList = new ArrayList<WorkPatrolTimeliness>();// 保存查询出来的操作日志

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.public_list_horizontalscroll,
				container, false);// 关联布局文件
		ViewUtils.inject(this, rootView);
		mActivity = (BasicActivity) getActivity();
		user = mActivity.getUser();

		headerView = inflater.inflate(
				R.layout.fragment_workpatrol_timeliness_head, null);

		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadWorkPatrol(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item29_name);
		setHasOptionsMenu(true);

		mAdapter = new ContronAdapter<WorkPatrolTimeliness>(mActivity,
				R.layout.fragment_workpatrol_timeliness_item, workPatrolList) {
			@Override
			public void setViewValue(ContronViewHolder holder,
					final WorkPatrolTimeliness item, final int position) {
				holder.setText(R.id.tx_patrolcontent, item.getPatrol_content());

				holder.setText(R.id.tx_agency, item.getSname());

				holder.setText(R.id.tx_patrolcycle, item.getPatrol_cycle());

				holder.setText(R.id.tx_cyclecount,
						"" + item.getTime());

				float rate = Float.parseFloat(item.getPatrol_rate())*100;
				holder.setText(R.id.tx_patrol_finishliness,
						"" + String.valueOf(rate) + "%");

				holder.setText(R.id.tx_should_patrolcount,
						"" + item.getPatrol_object_count());
				holder.setText(R.id.tx_real_patrolcount,
						"" + item.getPatrol_exec_count());
				holder.setText(R.id.tx_leak_patrolcount, "" + item.getUnfinished());

				TextView butexcutework = holder.findView(R.id.tx_operation);

				// 操作点击事件
				butexcutework.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						JSONObject jsonObject = null;
						try {
							jsonObject = new JSONObject(queryJsonObject
									.toString());
							jsonObject.put("pid", item.getId());
						} catch (JSONException e) {
							e.printStackTrace();
						}

						WorkPatrolTimelinessDetailActivity.startActivity(
								mActivity, jsonObject);
					}
				});

				TextView butTrack = holder.findView(R.id.tx_track);
				butTrack.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(getActivity(), InfoWindowActivity.class);
						intent.putExtra("pid", item.getId()+"");
						getActivity().startActivity(intent);
					}
				});

			}
		};

		lisview.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if (firstVisibleItem >= 1) {
					invis.setVisibility(View.VISIBLE);
				} else {

					invis.setVisibility(View.GONE);
				}
			}
		});
		
		lisview.addFooterView(loadMoreView); // 设置列表底部视图
		lisview.addHeaderView(headerView); // 设置列表底部视图
		lisview.setAdapter(mAdapter);
		lisview.setEmptyView(empty);
		getData();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		downloadWorkPatrol(0);
	}

	/**
	 * 获取常量表中的派工类型、及超时时长
	 */
	private void getData() {
		EkeyDao ekeyDao = DBHelp.getInstance(mActivity).getEkeyDao();

		if (null == ekeyDao)
			return;

		mListYear = new ArrayList<Dict>();
		int year = 2016;
		for (int i = 0; i < 101; i++) {
			String years = year++ + "";
			Dict dict = new Dict("年份", years + "年", years);
			mListYear.add(dict);
		}

		mListAgency = ekeyDao.getSectionAll("机构");

		mListCycle = ekeyDao.getDicType("patrol_cycle");
	}

	/**
	 * 查询条件
	 */
	private void queryConditionDialog() {
		List<Dict> tempYear = new ArrayList<Dict>();
		tempYear.addAll(mListYear);

		List<Dict> tempCycle = new ArrayList<Dict>();
		tempCycle.addAll(mListCycle);

		List<SectionAll> tempAgency = new ArrayList<SectionAll>();
		tempAgency.addAll(mListAgency);

		SelectQueryWorkpatrolDialog dialogDepartment = new SelectQueryWorkpatrolDialog(
				mActivity, new SelectListener() {

					@Override
					public void select(JSONObject jsonObject) {
						queryJsonObject = jsonObject;
						downloadWorkPatrol(0);

					}
				}, tempYear, tempAgency, tempCycle, "巡检率查询条件");
		dialogDepartment.show(mActivity.getSupportFragmentManager(), "");// 弹出缺陷地点选择框
	}

	/**
	 * 下载巡检任务
	 */
	private void downloadWorkPatrol(int currentItem) {

		if (user == null)
			return;
		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		if (currentItem == 0) {// 当前页数为0时清空数据
			mcurrentItem = 0;
			workPatrolList.clear();
		}

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		if (queryJsonObject == null) {
			queryJsonObject = new JSONObject();
		}
		// 设置参数
		try {
			queryJsonObject.put("k", user.getKValue());
			queryJsonObject.put("page", currentItem);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		requestHttp.doPost(Globals.WORKPATROL_FINISH, queryJsonObject,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();

								if (value.contains("success")) {
									ArrayList<WorkPatrolTimeliness> listTasking;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										JSONArray jsonArrayTempTask = jsonObject
												.getJSONArray("items");
										// 获取任务
										listTasking = new Gson().fromJson(
												jsonArrayTempTask.toString(),
												new TypeToken<List<WorkPatrolTimeliness>>() {
												}.getType());

										// 获取总页数
										if (mcurrentItem == 0) {
											mItemCount = jsonObject
													.getInt("pages");
										}

										if (listTasking != null
												&& listTasking.size() > 0) {
											workPatrolList.addAll(listTasking);
										}
										mAdapter.notifyDataSetChanged();
									} catch (JSONException e) {
										e.printStackTrace();
									}

								} else {
									try {
										JSONObject json = new JSONObject(value);
										String error = json.getString("error");
										mActivity.showMsgBox(error);

									} catch (JSONException e) {
										e.printStackTrace();
									}

								}

							}
						});

					}
				});
	}

	/**
	 * 获取实例
	 * 
	 * @return
	 */
	public static WorkPatrolTimelinessFragment newInstance() {
		return new WorkPatrolTimelinessFragment();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.select, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();
			break;
		case R.id.menu_search:
			queryConditionDialog();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
