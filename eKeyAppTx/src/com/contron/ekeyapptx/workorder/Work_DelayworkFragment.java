package com.contron.ekeyapptx.workorder;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.Workbill;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;

import com.lidroid.xutils.view.annotation.ViewInject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * 待接单
 * 
 * @author luoyilong
 *
 */
public class Work_DelayworkFragment extends Fragment {

	@ViewInject(R.id.lv_public)
	private ListView lisview;

	@ViewInject(R.id.tx_empty)
	private TextView empty;

	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求对象
	private ContronAdapter<Workbill> mAdapter;// 适配器
	private View loadMoreView;// 下一页视图
	private RadioButton loadMoreButton;// 下一页按钮
	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数
	private List<Workbill> taskings = new ArrayList<Workbill>();// 保存查询出来的操作日志
	protected boolean isVisible;// 当前界面是否可见
	private boolean isPrepared = false; // 标志位，标志已经初始化完成。
	private User user = null;

	/**
	 * 在这里实现Fragment数据的缓加载.
	 * 
	 * @param isVisibleToUser
	 */
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (getUserVisibleHint()) {
			isVisible = true;
			refresh();
		} else {
			isVisible = false;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
		user = mActivity.getUser();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.public_list, container, false);// 关联布局文件
		ViewUtils.inject(this, rootView);
		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadDelayTasking(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
		isPrepared = true;
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAdapter = new ContronAdapter<Workbill>(mActivity,
				R.layout.fragment_workbill_item, taskings) {
			@Override
			public void setViewValue(ContronViewHolder holder,
					final Workbill item, final int position) {
				holder.setText(R.id.tx_workdesc, "派工内容：" + item.getWorkdesc());

				holder.setText(R.id.tx_worktype, "派工类型：" + item.getWorktype());

				holder.setText(R.id.tx_limited, "超时时长：" + item.getLimited()
						+ "小时");

				holder.setText(R.id.tx_oname, "基        站：" + item.getOname());

				holder.setText(R.id.tx_sname, "执行单位：" + item.getSname());

				holder.setText(R.id.tx_create_by,
						"派  工  人：" + item.getApply_by());

				holder.setText(R.id.tx_createtime,
						"派工时间：" + item.getApply_at());
				holder.setText(R.id.tx_wkb_applyin_at,"进站时间：" + item.getApplyin_at());

				holder.setText(R.id.tx_wkb_applylevae_at,"离站时间：" + item.getApplyleave_at());

				holder.setText(R.id.tx_status, "工单状态：" + item.getStatus());

				Button butdelaywork = holder.findView(R.id.but_delaywork);
				butdelaywork.setVisibility(View.VISIBLE);
				// 接单点击事件
				butdelaywork.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						delayWorTasking(position);
					}
				});

			}
		};

		lisview.addFooterView(loadMoreView); // 设置列表底部视图
		lisview.setAdapter(mAdapter);
		lisview.setEmptyView(empty);

		if (isVisible)
			downloadDelayTasking(0);
	}

	/**
	 * 刷新数据
	 */
	private void refresh() {
		if (!isPrepared || !isVisible)
			return;
		downloadDelayTasking(0);
	}

	private void clear() {
		mcurrentItem = 0;
		taskings.clear();
		if(lisview.getFooterViewsCount()>0){
			lisview.removeFooterView(loadMoreView);
		}
		lisview.addFooterView(loadMoreView);
		lisview.setAdapter(mAdapter);
	}

	/**
	 * 下载派工任务
	 */
	private void downloadDelayTasking(int currentItem) {
		if (user == null)
			return;

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		// 设置参数
		RequestParams params = new RequestParams(user);
		params.putParams("page", currentItem);
		params.putParams("status", new JSONArray().put("新单"));
		params.putParams("sid", user.getSid());

		requestHttp.doPost(Globals.WORKBILL_GET, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								if (value.contains("success")) {
									ArrayList<Workbill> listTasking;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										JSONArray jsonArrayTempTask = jsonObject
												.getJSONArray("items");
										// 获取任务
										listTasking = new Gson().fromJson(
												jsonArrayTempTask.toString(),
												new TypeToken<List<Workbill>>() {
												}.getType());

										// 获取总页数
										if (mcurrentItem == 0) {
											clear();
											mItemCount = jsonObject
													.getInt("pages");
										}

										if (listTasking != null
												&& listTasking.size() > 0) {
											taskings.addAll(listTasking);
											mAdapter.notifyDataSetChanged();
										}

									} catch (JSONException e) {
										e.printStackTrace();
									}

								} else {
									try {
										JSONObject json = new JSONObject(value);
										String error = json.getString("error");
										mActivity.showMsgBox(error);

									} catch (JSONException e) {
										e.printStackTrace();
									}

								}

							}
						});

					}
				});
	}

	/**
	 * 接派工任务
	 */
	private void delayWorTasking(final int position) {

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		Workbill tasking = taskings.get(position);
		if (null == tasking)
			return;

		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话框

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("status", "接单");

		String url = String.format(Globals.WORKBILL_ACCEPT, tasking.getId());

		requestHttp.doPost(url, params, new HttpRequestCallBackString() {

			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mActivity.dismissDialog();
						if (value.contains("success")) {
							taskings.remove(position);
							mAdapter.notifyDataSetChanged();
						} else {
							String error = "";
							try {
								JSONObject json = new JSONObject(value);
								error = json.getString("error");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							mActivity.showToast(error);
						}

					}
				});

			}
		});
	}

	/**
	 * 获取实例
	 * 
	 * @return
	 */
	public static Work_DelayworkFragment newInstance() {
		return new Work_DelayworkFragment();
	}

}
