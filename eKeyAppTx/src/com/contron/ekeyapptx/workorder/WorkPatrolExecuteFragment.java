package com.contron.ekeyapptx.workorder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.doorlock.DoorLockerActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.WorkPatrol;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WorkPatrolExecuteFragment extends Fragment {

	@ViewInject(R.id.lv_public)
	private ListView lisview;

	@ViewInject(R.id.tx_empty)
	private TextView empty;

	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求对象
	private ContronAdapter<WorkPatrol> mAdapter;// 适配器
	private View loadMoreView;// 下一页视图
	private RadioButton loadMoreButton;// 下一页按钮
	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数
	private List<WorkPatrol> workPatrolList = new ArrayList<WorkPatrol>();// 保存查询出来的操作日志

	private User user = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.public_list, container, false);// 关联布局文件
		ViewUtils.inject(this, rootView);
		mActivity = (BasicActivity) getActivity();
		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);
		user = mActivity.getUser();
		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadWorkPatrol(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item27_name);
		setHasOptionsMenu(true);

		mAdapter = new ContronAdapter<WorkPatrol>(mActivity,
				R.layout.fragment_workpatrol_item, workPatrolList) {
			@Override
			public void setViewValue(ContronViewHolder holder,
					final WorkPatrol item, final int position) {
				holder.setText(R.id.tx_content, "巡检内容：" + item.getPatrol_content());

				holder.setText(R.id.tx_sname, "执行单位：" + item.getSname());

				holder.setText(R.id.tx_create_by,
						"创  建  人：" + item.getCreateBy());

				holder.setText(R.id.tx_createtime,
						"派工时间：" + item.getCreate_at());

				holder.setText(R.id.tx_begin_at,
						"开始时间：" + item.getBegin_at());

				holder.setText(R.id.tx_end_at,
						"结束时间：" + item.getEnd_at());

				holder.setText(R.id.tx_cycle, "巡检周期：" + item.getPatrol_cycle());

				holder.setText(R.id.tx_demand, "巡检要求：" + item.getPatrol_demand());

				List<DeviceObject> listObject = item.getObjects();
				StringBuffer oname = new StringBuffer();
				if (listObject != null) {
					for (DeviceObject tempDevice : listObject) {
						oname.append(tempDevice.getName() + "、");
					}
					holder.setText(R.id.tx_oname, "基        站：" + oname.toString());
				}

				Button butexcutework = holder.findView(R.id.but_excutework);
				butexcutework.setVisibility(View.VISIBLE);
				// 执行点击事件
				butexcutework.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						DoorLockerActivity.startActivity(mActivity, item);
					}
				});

			}
		};

		lisview.addFooterView(loadMoreView); // 设置列表底部视图
		lisview.setAdapter(mAdapter);
		lisview.setEmptyView(empty);

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		downloadWorkPatrol(0);
	}

	/**
	 * 下载巡检任务
	 */
	private void downloadWorkPatrol(int currentItem) {

		if (user == null)
			return;

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		if (currentItem == 0) {// 当前页数为0时清空数据
			mcurrentItem = 0;
			workPatrolList.clear();
		}

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		// 设置参数
		RequestParams params = new RequestParams(user);
		params.putParams("page", currentItem);
//		params.putParams("sid", user.getSid());

		requestHttp.doPost(Globals.WORKPATROL_GET, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();

								if (value.contains("success")) {
									ArrayList<WorkPatrol> listTasking;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										JSONArray jsonArrayTempTask = jsonObject
												.getJSONArray("items");
										// 获取任务
										listTasking = new Gson().fromJson(
												jsonArrayTempTask.toString(),
												new TypeToken<List<WorkPatrol>>() {
												}.getType());

										// 获取总页数
										if (mcurrentItem == 0) {
											mItemCount = jsonObject
													.getInt("pages");
										}

										if (listTasking != null
												&& listTasking.size() > 0) {
											workPatrolList.addAll(listTasking);
											mAdapter.notifyDataSetChanged();

										}

									} catch (JSONException e) {
										e.printStackTrace();
									}

								} else {
									try {
										JSONObject json = new JSONObject(value);
										String error = json.getString("error");
										mActivity.showMsgBox(error);

									} catch (JSONException e) {
										e.printStackTrace();
									}

								}

							}
						});

					}
				});
	}

	/**
	 * 获取实例
	 * 
	 * @return
	 */
	public static WorkPatrolExecuteFragment newInstance() {
		return new WorkPatrolExecuteFragment();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// inflater.inflate(R.menu.add, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
