package com.contron.ekeyapptx.workorder;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.doorlock.DoorLockerActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.Workbill;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.InputDialogFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 工单执行
 *
 * @author luoyilong
 *
 */
public class Work_ExcuteFragment extends Fragment {

	@ViewInject(R.id.lv_public)
	private ListView lisview;

	@ViewInject(R.id.tx_empty)
	private TextView empty;

	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求对象
	private ContronAdapter<Workbill> mAdapter;// 适配器
	private View loadMoreView;// 下一页视图
	private RadioButton loadMoreButton;// 下一页按钮
	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数
	private List<Workbill> taskings = new ArrayList<Workbill>();// 保存查询出来的操作日志
	protected boolean isVisible;// 当前界面是否可见
	private boolean isPrepared = false;// 标志位，标志已经初始化完成。
	private User user = null;

	/**
	 * 在这里实现Fragment数据的缓加载.
	 *
	 * @param isVisibleToUser
	 */
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (getUserVisibleHint()) {
			isVisible = true;
			refresh();
		} else {
			isVisible = false;
		}
	}

	/**
	 * 刷新数据
	 */
	private void refresh() {
		if (!isPrepared || !isVisible)
			return;
		downloadTasking(0);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
		user = mActivity.getUser();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View rootView = inflater
				.inflate(R.layout.public_list, container, false);// 关联布局文件
		ViewUtils.inject(this, rootView);

		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadTasking(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
		isPrepared = true;
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mAdapter = new ContronAdapter<Workbill>(mActivity,
				R.layout.fragment_workbill_item, taskings) {
			@Override
			public void setViewValue(ContronViewHolder holder,
									 final Workbill item, final int position) {
				holder.setText(R.id.tx_workdesc, "派工内容：" + item.getWorkdesc());

				holder.setText(R.id.tx_worktype, "派工类型：" + item.getWorktype());

				holder.setText(R.id.tx_limited, "超时时长：" + item.getLimited()
						+ "小时");

				holder.setText(R.id.tx_oname, "基        站：" + item.getOname());

				holder.setText(R.id.tx_sname, "执行单位：" + item.getSname());

				holder.setText(R.id.tx_create_by,
						"派  工  人：" + item.getApply_by());

				holder.setText(R.id.tx_createtime,
						"派工时间：" + item.getApply_at());

				holder.setText(R.id.tx_wkb_applyin_at,"进站时间：" + item.getApplyin_at());

				holder.setText(R.id.tx_wkb_applylevae_at,"离站时间：" + item.getApplyleave_at());

				holder.setText(R.id.tx_status, "工单状态：" + item.getStatus());

				Button butNavigation = holder.findView(R.id.but_navigation);
				butNavigation.setVisibility(View.VISIBLE);
				butNavigation.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (item.getObject() == null) return;
						toNavigation(item.getObject().getLat(), item.getObject().getLng());
					}
				});

				Button butexcutework = holder.findView(R.id.but_excutework);
				butexcutework.setVisibility(View.VISIBLE);
				// 执行点击事件
				butexcutework.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						DoorLockerActivity.startActivity(mActivity, item);
					}
				});

				Button fulfillwork = holder.findView(R.id.but_fulfillwork);
				fulfillwork.setVisibility(View.VISIBLE);
				// 回单点击事件
				fulfillwork.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						barckWork(position);
					}
				});

			}
		};

		lisview.addFooterView(loadMoreView); // 设置列表底部视图
		lisview.setAdapter(mAdapter);
		lisview.setEmptyView(empty);
		if (isVisible)
			downloadTasking(0);
	}

	public static boolean isAvilible(Context context, String packageName){
		//获取packagemanager
		final PackageManager packageManager = context.getPackageManager();
		//获取所有已安装程序的包信息
		List<PackageInfo> packageInfos = packageManager.getInstalledPackages(0);
		//用于存储所有已安装程序的包名
		List<String> packageNames = new ArrayList<String>();
		//从pinfo中将包名字逐一取出，压入pName list中
		if(packageInfos != null){
			for(int i = 0; i < packageInfos.size(); i++){
				String packName = packageInfos.get(i).packageName;
				packageNames.add(packName);
			}
		}
		//判断packageNames中是否有目标程序的包名，有TRUE，没有FALSE
		return packageNames.contains(packageName);
	}

	private void toNavigation(double lat, double lon) {
		if (isAvilible(getContext(), "com.autonavi.minimap")) {
			try{
				Intent intent = Intent.getIntent("androidamap://navi?sourceApplication=配网智能锁控系统&poiname=我的目的地&lat="+lat+"&lon="+lon+"&dev=0");
				getContext().startActivity(intent);
			} catch (URISyntaxException e)
			{e.printStackTrace(); }
		}else{
			Toast.makeText(getContext(), "您尚未安装高德地图", Toast.LENGTH_LONG).show();
			Uri uri = Uri.parse("market://details?id=com.autonavi.minimap");
			Intent intent = new Intent(Intent.ACTION_VIEW, uri);
			getContext().startActivity(intent);
		}
	}


	private void clear() {
		mcurrentItem = 0;
		taskings.clear();
		if(lisview.getFooterViewsCount()>0){
			lisview.removeFooterView(loadMoreView);
		}
		lisview.addFooterView(loadMoreView);
		lisview.setAdapter(mAdapter);
	}

	/**
	 * 下载派工任务
	 */
	private void downloadTasking(int currentItem) {

		if (user == null)
			return;

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		// 设置参数
		RequestParams params = new RequestParams(user);
		params.putParams("page", currentItem);
		params.putParams("status", new JSONArray().put("接单"));
		params.putParams("rec_username", user.getUsername());

		requestHttp.doPost(Globals.WORKBILL_GET, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								if (value.contains("success")) {
									ArrayList<Workbill> listTasking;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										JSONArray jsonArrayTempTask = jsonObject
												.getJSONArray("items");
										// 获取任务
										listTasking = new Gson().fromJson(
												jsonArrayTempTask.toString(),
												new TypeToken<List<Workbill>>() {
												}.getType());

										// 获取总页数
										if (mcurrentItem == 0) {
											clear();
											mItemCount = jsonObject
													.getInt("pages");
										}

										if (listTasking != null
												&& listTasking.size() > 0) {
											taskings.addAll(listTasking);
											mAdapter.notifyDataSetChanged();
										}

									} catch (JSONException e) {
										e.printStackTrace();
									}

								} else {
									try {
										JSONObject json = new JSONObject(value);
										String error = json.getString("error");
										mActivity.showMsgBox(error);

									} catch (JSONException e) {
										e.printStackTrace();
									}

								}

							}
						});

					}
				});
	}

	/**
	 * 回单填写原因
	 *
	 * @param position
	 */
	private void barckWork(final int position) {
		InputDialogFragment inputDialogFragment = new InputDialogFragment();
		inputDialogFragment.setTitleId(R.string.backwork_dialog_title);
//		inputDialogFragment.setdefaultMsg("工作已完成");

		inputDialogFragment.setHintId(R.string.backwork_dialog_hiht);
		inputDialogFragment
				.setOnInputTextListener(new InputDialogFragment.OnInputTextListener() {
					@Override
					public void OnInputText(String text) {
						fulfillworkTasking(position, text);
					}
				});
		inputDialogFragment.show(mActivity.getFragmentManager(),
				"inputDialogFragment");
	}

	/**
	 * 派工任务回单
	 */
	private void fulfillworkTasking(final int position, String memo) {

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		Workbill tasking = taskings.get(position);
		if (null == tasking)
			return;

		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话框
		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("status", "回单");
		params.putParams("backmemo", memo);

		String url = String.format(Globals.WORKBILL_RETURN, tasking.getId());
		requestHttp.doPost(url, params, new HttpRequestCallBackString() {

			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mActivity.dismissDialog();
						if (value.contains("success")) {
							taskings.remove(position);
							mAdapter.notifyDataSetChanged();
						} else {
							String error = "";
							try {
								JSONObject json = new JSONObject(value);
								error = json.getString("error");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							mActivity.showToast(error);
						}

					}
				});

			}
		});
	}

	/**
	 * 获取实例
	 *
	 * @return
	 */
	public static Work_ExcuteFragment newInstance() {
		return new Work_ExcuteFragment();
	}
}
