package com.contron.ekeyapptx.workorder;

import java.util.ArrayList;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.MyFragmentPagerAdapter;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

/**
 * 我的工单
 *
 * @author luoyilong
 *
 */
public class WorkApprovalActivity extends BasicActivity {

    @ViewInject(R.id.viewpager)
    private ViewPager mv_viewPager;// viewpager控件

    @ViewInject(R.id.rad_approvalwork)
    private RadioButton mtv_approvalwork;

    @ViewInject(R.id.rad_approvedwork)
    private RadioButton mtv_approvedwork;

    @ViewInject(R.id.rad_rejectwork)
    private RadioButton mtv_rejectwork;



    private ArrayList<Fragment> fragmentList;

//	private int bmpW;// 横线图片宽度
//	private int offset;// 图片移动的偏移量

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_work);
        getBasicActionBar().setTitle(R.string.main_tab_item31_name);
//		mActivity = this;
        ViewUtils.inject(this);
        InitView();
    }

    /**
     * 初始化控件
     */
    private void InitView() {
        /** 标签页 **/
        mtv_approvalwork.setOnClickListener(new txListener(0));
        mtv_approvedwork.setOnClickListener(new txListener(1));
        mtv_rejectwork.setOnClickListener(new txListener(2));
        mtv_approvalwork.setChecked(true);

        /** ViewPager **/
        fragmentList = new ArrayList<Fragment>();
        Fragment approvalFragment = WorkApprovalFragment.newInstance();
        Fragment approvedFragment = WorkApprovedFragment.newInstance();
        Fragment rejectedFragment = WorkRejectedFragment.newInstance();
        fragmentList.add(approvalFragment);
        fragmentList.add(approvedFragment);
        fragmentList.add(rejectedFragment);
        // 给ViewPager设置适配器
        mv_viewPager.setAdapter(new MyFragmentPagerAdapter(
                getSupportFragmentManager(), fragmentList));
        mv_viewPager.setCurrentItem(0);// 设置当前显示标签页为第一页
        mv_viewPager.setOnPageChangeListener(new MyOnPageChangeListener());// 页面变化时的监听器

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.execute_work, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    /**
     * tab标签点击事件
     *
     * @author luoyilong
     *
     */
    public class txListener implements View.OnClickListener {
        private int index = 0;

        public txListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            int position = v.getId();
            mtv_approvalwork.setChecked(position == R.id.rad_approvalwork);
            mtv_approvedwork.setChecked(position == R.id.rad_approvedwork);
            mtv_rejectwork.setChecked(position == R.id.rad_rejectwork);
            mv_viewPager.setCurrentItem(index);
        }
    }

    // 页面滑动监听
    public class MyOnPageChangeListener implements OnPageChangeListener {
//		private int one = offset * 2 + bmpW;// 两个相邻页面的偏移量

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageSelected(int arg0) {
            int position = 0;
            switch (arg0) {
                case 0:
                    position = R.id.rad_approvalwork;
                    break;
                case 1:
                    position = R.id.rad_approvedwork;
                    break;
                case 2:
                    position = R.id.rad_rejectwork;
                    break;
            }

            mtv_approvalwork.setChecked(position == R.id.rad_approvalwork);
            mtv_approvedwork.setChecked(position == R.id.rad_approvedwork);
            mtv_rejectwork.setChecked(position == R.id.rad_rejectwork);
        }
    }

    // 界面跳转
    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, WorkApprovalActivity.class);
        activity.startActivity(intent);
    }
}
