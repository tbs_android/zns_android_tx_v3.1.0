package com.contron.ekeyapptx.workorder;

import java.util.ArrayList;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.MyFragmentPagerAdapter;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

/**
 * 我的工单
 * 
 * @author luoyilong
 *
 */
public class MyWorkActivity extends BasicActivity {

	@ViewInject(R.id.viewpager)
	private ViewPager mv_viewPager;// viewpager控件

	@ViewInject(R.id.rad_delaywork)
	private RadioButton mtv_delaywork;// 等接工单

	@ViewInject(R.id.rad_excutework)
	private RadioButton mtv_excutework;// 执行工单

	@ViewInject(R.id.rad_fulfillwork)
	private RadioButton mtv_fulfillwork;// 完成工单



	private ArrayList<Fragment> fragmentList;

//	private int bmpW;// 横线图片宽度
//	private int offset;// 图片移动的偏移量

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_work);
		getBasicActionBar().setTitle(R.string.main_tab_item25_name);
//		mActivity = this;
		ViewUtils.inject(this);
		InitView();
	}

	/**
	 * 初始化控件
	 */
	private void InitView() {
		/** 标签页 **/
		mtv_delaywork.setOnClickListener(new txListener(0));
		mtv_excutework.setOnClickListener(new txListener(1));
		mtv_fulfillwork.setOnClickListener(new txListener(2));
		mtv_delaywork.setChecked(true);

		/** ViewPager **/
		fragmentList = new ArrayList<Fragment>();
		Fragment delayFragment = Work_DelayworkFragment.newInstance();
		Fragment excuteFragment = Work_ExcuteFragment.newInstance();
		Fragment fulfillFragment = Work_FulfillFragment.newInstance();
		fragmentList.add(delayFragment);
		fragmentList.add(excuteFragment);
		fragmentList.add(fulfillFragment);
		// 给ViewPager设置适配器
		mv_viewPager.setAdapter(new MyFragmentPagerAdapter(
				getSupportFragmentManager(), fragmentList));
		mv_viewPager.setCurrentItem(0);// 设置当前显示标签页为第一页
		mv_viewPager.setOnPageChangeListener(new MyOnPageChangeListener());// 页面变化时的监听器

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.execute_work, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);

	}

	/**
	 * tab标签点击事件
	 * 
	 * @author luoyilong
	 *
	 */
	public class txListener implements View.OnClickListener {
		private int index = 0;

		public txListener(int i) {
			index = i;
		}

		@Override
		public void onClick(View v) {
			int position = v.getId();
			mtv_delaywork.setChecked(position == R.id.rad_delaywork);
			mtv_excutework.setChecked(position == R.id.rad_excutework);
			mtv_fulfillwork.setChecked(position == R.id.rad_fulfillwork);
			mv_viewPager.setCurrentItem(index);
		}
	}

	// 页面滑动监听
	public class MyOnPageChangeListener implements OnPageChangeListener {
//		private int one = offset * 2 + bmpW;// 两个相邻页面的偏移量

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageSelected(int arg0) {

			int position = 0;

			switch (arg0) {
			case 0:
				position = R.id.rad_delaywork;
				break;
			case 1:
				position = R.id.rad_excutework;
				break;
			case 2:
				position = R.id.rad_fulfillwork;
				break;
			default:
				position = R.id.rad_delaywork;
				break;
			}

			mtv_delaywork.setChecked(position == R.id.rad_delaywork);
			mtv_excutework.setChecked(position == R.id.rad_excutework);
			mtv_fulfillwork.setChecked(position == R.id.rad_fulfillwork);

			// TODO Auto-generated method stub
			// Animation animation = new TranslateAnimation(currIndex * one,
			// arg0
			// * one, 0, 0);// 平移动画
//			currIndex = arg0;
			// animation.setFillAfter(true);// 动画终止时停留在最后一帧，不然会回到没有执行前的状态
			// animation.setDuration(200);// 动画持续时间0.2秒
			// mimg_cursor.startAnimation(animation);// 是用ImageView来显示动画的
//			int i = currIndex + 1;
//			Toast.makeText(MyWorkActivity.this, "您选择了第" + i + "个页卡",
//					Toast.LENGTH_SHORT).show();
		}
	}

	// 界面跳转
	public static void startActivity(Activity activity) {
		Intent intent = new Intent(activity, MyWorkActivity.class);
		activity.startActivity(intent);
	}
}
