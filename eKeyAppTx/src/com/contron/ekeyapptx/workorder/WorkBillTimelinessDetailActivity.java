package com.contron.ekeyapptx.workorder;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.Workbill;
import com.contron.ekeypublic.http.RequestHttp;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

public class WorkBillTimelinessDetailActivity extends BasicActivity {

	@ViewInject(R.id.lv_public)
	private ListView lisview;
	@ViewInject(R.id.tx_empty)
	private TextView empty;
	
	@ViewInject(R.id.workbill_detail_timeliness_head)
	private LinearLayout invis;
	
	private ContronAdapter<Workbill> mAdapter;// 适配器
	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求对象
	private View loadMoreView;// 下一页视图
	private View headerView;// 表头
	private RadioButton loadMoreButton;// 下一页按钮
	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数
	private List<Workbill> mWorkbillList = new ArrayList<Workbill>();
	private User user = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.public_list_horizontalscroll);
		getBasicActionBar().setTitle("进站及时率详情");
		ViewUtils.inject(this);
		mActivity = this;
		user = mActivity.getUser();
		mWorkbillList = getIntent().getParcelableArrayListExtra("request");
		LayoutInflater inflater = mActivity.getLayoutInflater();
		headerView = inflater.inflate(
				R.layout.activity_workbill_timeliness_detail_head, null);

		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);
		loadMoreView.setVisibility(View.GONE);

//		// 下一页按钮
//		loadMoreButton.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//
//				mcurrentItem++;
//				if (mcurrentItem < mItemCount)// 当前页小于总页数
//				{
//					downloadWorkBillDetail(mcurrentItem);
//				} else {
//					mActivity.showMsgBox("已经是最后页了！");
//				}
//
//			}
//
//		});

		setadapter();
//		downloadWorkBillDetail(0);
	}

	/**
	 * 设置适配器
	 */
	private void setadapter() {

		if (mWorkbillList == null)
			mWorkbillList = new ArrayList<Workbill>();

		// 初始化适配器
		mAdapter = new ContronAdapter<Workbill>(this,
				R.layout.activity_workbill_timeliness_detail_item,
				mWorkbillList) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					Workbill item, int position) {
				viewHolder.setText(R.id.tx_workdesc, item.getWorkdesc());
				viewHolder.setText(R.id.tx_oname, item.getOname());
				viewHolder.setText(R.id.tx_createtime, item.getApply_at());
				viewHolder.setText(R.id.tx_sname, item.getSname());
				viewHolder.setText(R.id.tx_rectime, item.getRec_at());
				viewHolder.setText(R.id.tx_rec_by, item.getRec_by());
				viewHolder.setText(R.id.tx_intime, item.getIn_at());
				viewHolder.setText(R.id.tx_backtime, item.getBack_at());

			}
		};

		lisview.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if (firstVisibleItem >= 1) {
					invis.setVisibility(View.VISIBLE);
				} else {

					invis.setVisibility(View.GONE);
				}
			}
		});
		
		lisview.addFooterView(loadMoreView); // 设置列表底部视图
		lisview.addHeaderView(headerView);
		lisview.setAdapter(mAdapter);
		lisview.setEmptyView(empty);

	}


	// 界面跳转
	public static void startActivity(Context context, List<Workbill> details) {
		Intent intent = new Intent(context,
				WorkBillTimelinessDetailActivity.class);
		ArrayList<Workbill> arrayList = new ArrayList<Workbill>(details);
		intent.putParcelableArrayListExtra("request", arrayList);
		context.startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.save, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
