package com.contron.ekeyapptx.workorder;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.treeListAdapter.Node;
import com.contron.ekeypublic.treeListAdapter.SimpleTreeAdapter;
import com.contron.ekeypublic.treeListAdapter.SimpleTreeAdapter.BackResult;
import com.contron.ekeypublic.treeListAdapter.TreeListViewAdapter.OnTreeNodeClickListener;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import com.contron.ekeyapptx.R;

public class SectionActivity extends BasicActivity {

	@ViewInject(R.id.lv_public)
	private ListView lisview;

	private List<Section> mDatas = new ArrayList<Section>();
	private EkeyDao ekeyDao;
	private BasicActivity mActivity;
	private SimpleTreeAdapter mAdapter;

	private boolean hasData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.public_list);
		mActivity = this;
		ViewUtils.inject(this);
		getBasicActionBar().setTitle("选择机构");
		ekeyDao = DBHelp.getInstance(this).getEkeyDao();
		hasData = getIntent().getBooleanExtra("hasData", false);
		if (hasData) {
			mDatas = getIntent().getParcelableArrayListExtra(
					"sections");
		}
		else {
			getSection();
		}
		setAdapter();
	}

	private BackResult backResult = new BackResult() {

		@Override
		public void onBackResult(Node node) {
			// TODO Auto-generated method stub
			Section section = new Section();
			section.setId(node.getId());
			section.setName(node.getName());
			onBack(section);
		}
	};

	// 返回时设置返回参数
	private void onBack(Section section) {
		Intent data = getIntent();
		Bundle bundle=new Bundle();
		bundle.putParcelable("section", section);
		data.putExtras(bundle);
		setResult(RESULT_OK, data);
		finish();
	}

	/**
	 * 设置适配器
	 */
	private void setAdapter() {

		try {
			mAdapter = new SimpleTreeAdapter(backResult, lisview, this, mDatas,
					10);

			mAdapter.setOnTreeNodeClickListener(new OnTreeNodeClickListener() {

				@Override
				public void onClick(Node node, int position) {

					LogUtils.e(node.getName());

				}
			});

			lisview.setAdapter(mAdapter);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取常量表中的派工类型、及超时时长
	 */
	private void getSection() {
		if (null == ekeyDao)
			return;
		mDatas = ekeyDao.getSection("机构");
	}

	// 界面跳转
	public static void startActivity(Activity activity, int requestCode) {
		Intent intent = new Intent(activity, SectionActivity.class);
		activity.startActivityForResult(intent, requestCode);
	}

	// 界面跳转
	public static void startActivity(Activity activity, int requestCode, ArrayList<Section> sections) {
		Intent intent = new Intent(activity, SectionActivity.class);
		intent.putParcelableArrayListExtra("sections", sections);
		intent.putExtra("hasData", true);
		activity.startActivityForResult(intent, requestCode);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.section, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
