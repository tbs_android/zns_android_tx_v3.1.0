package com.contron.ekeyapptx.workorder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.contron.ekeyapptx.R;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.plan.DeviceActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.ParString;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.DateTimePickDialogUtil;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class WorkPatrolAddActivity extends BasicActivity {

	private static final int REQUEST_CODE_DEVICE = 100;
	private static final int REQUEST_CODE_SECTION = 101;

	@ViewInject(R.id.tx_content)
	private EditText medContent;// 巡检内容

	@ViewInject(R.id.spin_cycle)
	private Spinner mspinCycle;// 巡检周期

	@ViewInject(R.id.but_sname)
	private Button mbutSname;// 执行单位

	@ViewInject(R.id.but_oname)
	private Button mbutOname;// 基站

	@ViewInject(R.id.tx_demand)
	private EditText mtxDemand;// 巡检要求

	@ViewInject(R.id.tx_memo)
	private EditText mtxMemo;// 备注

	@ViewInject(R.id.begin_date_edit)
	private EditText beginDateEdit;

	@ViewInject(R.id.end_date_edit)
	private EditText endDateEdit;

	private BasicActivity mActivity;
	// 周期数据
	private List<Dict> dictCycle;

	// 选择的基站
	SparseArray<ParString> mobject = new SparseArray<ParString>();

	// 选择的单位
	private Section msection;

	private EkeyDao ekeyDao;

	// 周期适配器
	private ContronAdapter<Dict> adapterCycle;

	// 后台请求对象
	private RequestHttp requestHttp;

	// 选择周期
	private Dict selectCycle;

	private ArrayList<Section> Objects = new ArrayList<Section>();
//	private ArrayList<DeviceObject> Objects = new ArrayList<DeviceObject>();
	private String beginTime;
	private String endTime;
	private ArrayList<Section> resultObject = new ArrayList<Section>();
	private ArrayList<Section> selectedSection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_work_patrol_add);
		mActivity = this;
		ViewUtils.inject(this);
		getBasicActionBar().setTitle("新增巡检");
		initViews();
		ekeyDao = DBHelp.getInstance(this).getEkeyDao();
		getObject();
		getDic();
		setadapter();
	}

	private void initViews() {
		beginDateEdit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DateTimePickDialogUtil dateTimePicKDialog = new DateTimePickDialogUtil(
						WorkPatrolAddActivity.this, true);
				dateTimePicKDialog.setInitDateTime(DateUtil.getCurrentDate("yyyy年MM月dd日 HH:mm"), (endTime == null || endTime.equals("")) ? null : endDateEdit.getText().toString());
				beginTime = dateTimePicKDialog.dateTimePicKDialog(beginDateEdit, new SimpleDateFormat("yyyyMMddHHmmss"));

			}
		});

		endDateEdit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DateTimePickDialogUtil dateTimePicKDialog = new DateTimePickDialogUtil(
						WorkPatrolAddActivity.this, true);
				if (beginDateEdit.getText().length() > 0) {
					dateTimePicKDialog.setInitDateTime(beginDateEdit.getText().toString(), null);
				}
				else {
					dateTimePicKDialog.setInitDateTime(DateUtil.getCurrentDate("yyyy年MM月dd日 HH:mm"), null);
				}
				endTime = dateTimePicKDialog.dateTimePicKDialog(endDateEdit, new SimpleDateFormat("yyyyMMddHHmmss"));
			}
		});
	}

	/**
	 * 设置适配器
	 */
	private void setadapter() {

		if (dictCycle == null)
			dictCycle = new ArrayList<Dict>();

		dictCycle.add(new Dict("周期","周度","周度"));
		dictCycle.add(new Dict("周期","月度","月度"));
		dictCycle.add(new Dict("周期","季度","季度"));
		dictCycle.add(new Dict("周期","年度","年度"));
		// 初始化适配器
		adapterCycle = new ContronAdapter<Dict>(this,
				android.R.layout.simple_spinner_item, dictCycle) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder, Dict item,
					int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};

		adapterCycle
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mspinCycle.setAdapter(adapterCycle);
		mspinCycle.setSelection(0);
		mspinCycle.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				selectCycle = (Dict) arg0.getItemAtPosition(arg2);

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	/**
	 * 获取常量表中的派工类型、及超时时长
	 */
	private void getDic() {
		if (null == ekeyDao)
			return;
		dictCycle = ekeyDao.getDicType("patrol_cycle");
	}

	@OnClick({ R.id.but_sname, R.id.but_oname })
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.but_sname:
		{
			if (null == mobject || mobject.size() == 0) {
				this.showToast("请先选择基站！");
				return;
			}
			SectionActivity.startActivity(this, REQUEST_CODE_SECTION, selectedSection);
		}

			break;
		case R.id.but_oname:
			if (resultObject != null && resultObject.size() > 0) {
				DeviceActivity.startActivityForResult(this, false, REQUEST_CODE_DEVICE, resultObject);
			} else {
				DeviceActivity.startActivityForResult(this, false, REQUEST_CODE_DEVICE, Objects);
			}
			break;

		default:
			break;
		}
	}

	/**
	 * 获取设备
	 */
	public void getObject() {
		List<Section> stations = DBHelp.getInstance(mActivity).getEkeyDao()
				.getSection();
		if (stations == null)
			stations = new ArrayList<Section>();
		for (Section sec : stations) {
			List<DeviceObject> devices = new ArrayList<DeviceObject>();
			for (DeviceObject dev : sec.getDeviceList()) {
				if (dev.getAgent_section() != null && !dev.getAgent_section().equals("")) {
					devices.add(dev);
				}
			}
			sec.getDeviceList().clear();
			sec.getDeviceList().addAll(devices);
		}
		Objects.clear();
		Objects.addAll(stations);
//		if (requestHttp == null)
//			requestHttp = new RequestHttp(this.getWld());// 服务请求
//		RequestParams params = new RequestParams(this.getUser());
//		requestHttp.doPost(Globals.WORKPATROL_SECTION_GET, params,
//				new HttpRequestCallBackString() {
//
//					@Override
//					public void resultValue(final String value) {
//						mActivity.runOnUiThread(new Runnable() {
//							@Override
//							public void run() {
//								mActivity.dismissDialog();
//								if (value.contains("success")) {
//									List<Section> Sectionlist = new ArrayList<Section>();
//									try {
//										JSONObject jsonObject = new JSONObject(value);
//										JSONArray jsonArraySections = jsonObject
//												.getJSONArray("items");
//										Sectionlist = new Gson().fromJson(
//												jsonArraySections.toString(),
//												new TypeToken<List<Section>>() {
//												}.getType());
//									} catch (JSONException e) {
//										e.printStackTrace();
//									}
//									Objects.clear();
//									Objects.addAll(Sectionlist);
//								} else {
//									String error = "";
//									try {
//										JSONObject json = new JSONObject(value);
//										error = json.getString("error");
//									} catch (JSONException e) {
//										e.printStackTrace();
//									}
//									mActivity.showToast(error);
//								}
//							}
//						});
//
//					}
//				});


	}

	/**
	 * 保存派工单
	 */
	private void saveTasking() {

		if (requestHttp == null)
			requestHttp = new RequestHttp(this.getWld());// 服务请求

		String content = medContent.getText().toString();// 派工内容

		if (null == content || "".equals(content)) {
			this.showToast("巡检内容不能为空！");
			return;
		}

		if (content.length() > 255) {
			this.showToast("巡检内容不得超过255位！");
			return;
		}

		if (null == selectCycle) {
			this.showToast("请选择周期！");
			return;
		}

		if (null == msection) {
			this.showToast("请选择执行单位！");
			return;
		}

		if (null == mobject || mobject.size() == 0) {
			this.showToast("请选择基站！");
			return;
		}

		String demand = mtxDemand.getText().toString();

		if (null == demand || "".equals(demand)) {
			this.showToast("巡检要求不能为空！");
			return;
		}

		if (demand.length() > 255) {
			this.showToast("巡检要求不得超过255位！");
			return;
		}

		if (null == beginTime || "".equals(beginTime)) {
			this.showToast("请选择开始时间！");
			return;
		}

		if (null == endTime || "".equals(endTime)) {
			this.showToast("请选择结束时间！");
			return;
		}

		String memo = mtxMemo.getText().toString();

		JSONArray object = new JSONArray();
		try {
			for (int i = 0; i < mobject.size(); i++) {
				object.put(i, mobject.valueAt(i).getId());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		this.startDialog(R.string.progressDialog_title_submit);// 弹出对话框

		// 设置参数
		RequestParams params = new RequestParams(this.getUser());
		params.putParams("patrol_cycle", selectCycle.getValue());
		params.putParams("patrol_content", content);
		beginTime = DateUtil.getStringByFormat(beginDateEdit.getText().toString(), "yyyy年MM月dd日 HH:mm", "yyyyMMddHHmmss");
		endTime = DateUtil.getStringByFormat(endDateEdit.getText().toString(), "yyyy年MM月dd日 HH:mm", "yyyyMMddHHmmss");
		params.putParams("begin_at", beginTime);
		params.putParams("end_at", endTime);
		params.putParams("oid", object);
		params.putParams("sid", msection.getId());
		params.putParams("patrol_demand", demand);
		params.putParams("patrol_item", "");
		params.putParams("memo", memo);



		// 请求接口
		requestHttp.doPost(Globals.WORKPATROL_NEW, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {
									mActivity.showMsgBox("提示", "新增巡检成功！",
											new OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
													mActivity.finish();
												}
											});
								} else {
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}
							}
						});

					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// super.onActivityResult(arg0, arg1, arg2);

		if (requestCode == REQUEST_CODE_DEVICE) {
			if (resultCode == Activity.RESULT_OK && data != null) {
				Bundle bundle = data.getExtras();
				if (bundle != null) {
					setStationsData(bundle);
				}
			}
		} else if (requestCode == REQUEST_CODE_SECTION) {

			if (resultCode == Activity.RESULT_OK && data != null) {
				Bundle bundle = data.getExtras();
				if (bundle != null) {
					setSectionData(bundle);
				}
			}
		}
	}

	/**
	 * 保存已选择的设备
	 * 
	 * @param bundle
	 */
	private void setStationsData(Bundle bundle) {
		ArrayList<Section> obj = bundle.getParcelableArrayList("stations");
		if (obj == null || obj.size() <= 0) {
			return;
		}
		resultObject.clear();
		resultObject.addAll(obj);
		if (resultObject != null && resultObject.size() > 0) {
			mobject.clear();
			for (Section station : resultObject) {
				for (DeviceObject device : station.deviceList) {
					if (device.isCheck) {

						ParString parString = new ParString(device.id,
								device.getName());
						mobject.append(device.id, parString);

						if (selectedSection == null) {
							selectedSection = new ArrayList<Section>();
						}
						selectedSection.clear();
						selectedSection.add(station);
					}
				}
			}

		}

		// 显示所选择设备
		Display();
	}

	/**
	 * 在按钮上显示选择的设备名称
	 */
	private void Display() {

		StringBuffer sb = new StringBuffer();
		if (mobject != null && mobject.size() > 0) {
			for (int i = 0; i < mobject.size(); i++) {
				sb.append(mobject.valueAt(i).getName() + ",");
			}

			sb.deleteCharAt(sb.length() - 1);
			mbutOname.setText(sb.toString());
		} else {
			mbutOname.setText(R.string.device_title);
		}

	}

	/**
	 * 保存已选择的机构
	 * 
	 * @param bundle
	 */
	private void setSectionData(Bundle bundle) {

		msection = bundle.getParcelable("section");

		if (null != msection) {
			mbutSname.setText(msection.getName());
		}

	}

	// 界面跳转
	public static void startActivity(Context context) {
		Intent intent = new Intent(context, WorkPatrolAddActivity.class);
		context.startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.save, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		case R.id.menu_save:// 保存新增派工
			saveTasking();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
