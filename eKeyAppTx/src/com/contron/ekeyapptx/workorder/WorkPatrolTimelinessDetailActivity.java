package com.contron.ekeyapptx.workorder;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.WorkPatrolDetail;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;

public class WorkPatrolTimelinessDetailActivity extends BasicActivity {

	@ViewInject(R.id.lv_public)
	private ListView lisview;
	@ViewInject(R.id.tx_empty)
	private TextView empty;

	@ViewInject(R.id.workpatrol_detail_timeliness_head)
	private LinearLayout invis;

	private JSONObject queryJsonObject;// 后台请求参数
	private ContronAdapter<WorkPatrolDetail> mAdapter;// 适配器
	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求对象
	private View loadMoreView;// 下一页视图
	private View headerView;// 表头
	private RadioButton loadMoreButton;// 下一页按钮
	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数
	private List<WorkPatrolDetail> mWorkbillList = new ArrayList<WorkPatrolDetail>();
	private User user = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.public_list_horizontalscroll);
		getBasicActionBar().setTitle("巡检完成率详情");
		ViewUtils.inject(this);
		mActivity = this;
		user = mActivity.getUser();
		String request = getIntent().getStringExtra("request");
		try {
			queryJsonObject = new JSONObject(request);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		LayoutInflater inflater = mActivity.getLayoutInflater();
		headerView = inflater.inflate(
				R.layout.activity_workpatrol_timeliness_detail_head, null);

		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadWorkPatrolDetail(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});

		setadapter();
		downloadWorkPatrolDetail(0);
	}

	/**
	 * 设置适配器
	 */
	private void setadapter() {

		if (mWorkbillList == null)
			mWorkbillList = new ArrayList<WorkPatrolDetail>();

		// 初始化适配器
		mAdapter = new ContronAdapter<WorkPatrolDetail>(this,
				R.layout.activity_workpatrol_timeliness_detail_item,
				mWorkbillList) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
									 WorkPatrolDetail item, int position) {

				if (null != item.getObject()){
					viewHolder.setText(R.id.tx_oname, item.getObject().getName());
					viewHolder.setText(R.id.tx_intime, item.getPatrol_at());
					viewHolder.setText(R.id.tx_districts, item.getObject().getDistricts());
					viewHolder.setText(R.id.tx_section, "" + item.getObject().getSection());
				} else {
					showToast("设备信息为空，请检查服务器数据是否正常！");
				}
				viewHolder.setText(R.id.tx_name, item.getPatrol_by());
			}
		};

		lisview.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if (firstVisibleItem >= 1) {
					invis.setVisibility(View.VISIBLE);
				} else {
					invis.setVisibility(View.GONE);
				}
			}
		});
		lisview.addFooterView(loadMoreView); // 设置列表底部视图
		lisview.addHeaderView(headerView);
		lisview.setAdapter(mAdapter);
		lisview.setEmptyView(empty);

	}

	/**
	 * 下载巡检详情
	 */
	private void downloadWorkPatrolDetail(int currentItem) {

		if (user == null)
			return;

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		if (currentItem == 0) {// 当前页数为0时清空数据
			mcurrentItem = 0;
			mWorkbillList.clear();
		}

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		// 设置参数
		String pid = "";
		try {
			queryJsonObject.put("k", user.getKValue());
			queryJsonObject.put("page", currentItem);
			pid = queryJsonObject.getString("pid");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String url = String.format(Globals.WORKPATROL_FINISH_DETAIL, pid);
		requestHttp.doPost(url, queryJsonObject,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();

								if (value.contains("success")) {
									ArrayList<WorkPatrolDetail> listTasking;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										JSONArray jsonArrayTempTask = jsonObject
												.getJSONArray("items");
										// 获取任务
										listTasking = new Gson().fromJson(
												jsonArrayTempTask.toString(),
												new TypeToken<List<WorkPatrolDetail>>() {
												}.getType());

										if (listTasking != null
												&& listTasking.size() > 0) {
											mWorkbillList.addAll(listTasking);
										}
										mAdapter.notifyDataSetChanged();
									} catch (JSONException e) {
										e.printStackTrace();
									}

								} else {
									try {
										JSONObject json = new JSONObject(value);
										String error = json.getString("error");
										mActivity.showMsgBox(error);

									} catch (JSONException e) {
										e.printStackTrace();
									}

								}

							}
						});

					}
				});
	}

	// 界面跳转
	public static void startActivity(Context context, JSONObject jsonObject) {
		Intent intent = new Intent(context,
				WorkPatrolTimelinessDetailActivity.class);
		intent.putExtra("request", jsonObject.toString());
		context.startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.save, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
