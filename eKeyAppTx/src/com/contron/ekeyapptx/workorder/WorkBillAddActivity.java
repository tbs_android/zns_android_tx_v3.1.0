package com.contron.ekeyapptx.workorder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.plan.DeviceActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.ParString;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.DateTimePickDialogUtil;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

public class WorkBillAddActivity extends BasicActivity {
	private static final int REQUEST_CODE_DEVICE = 100;
	private static final int REQUEST_CODE_SECTION = 101;

	@ViewInject(R.id.tx_workdesc)
	private EditText medWorkdesc;// 派工内容

	/*
	@ViewInject(R.id.spin_worktype)
	private Spinner mspinWorktype;// 派工类型
	// 选择派工类型
	private Dict selectWorktype;
	*/

	@ViewInject(R.id.spin_limited)
	private Spinner mspinLimited;// 超时时长

	@ViewInject(R.id.but_sname)
	private Button mbutSname;// 执行单位

	@ViewInject(R.id.but_oname)
	private Button mbutOname;// 基站

	@ViewInject(R.id.tx_memo)
	private EditText mtxMemo;// 备注

	@ViewInject(R.id.in_date_edit)
	private EditText inDateEdit;

	@ViewInject(R.id.out_date_edit)
	private EditText outDateEdit;

	@ViewInject(R.id.mobile_edit)
	private EditText mobileEdit;

	@ViewInject(R.id.need_accompany_check)
	private CheckBox needAccompanyCheck;

	@ViewInject(R.id.validate_check)
	private CheckBox validateCheck;

	private BasicActivity mActivity;
	// 工单类型数据
	private List<Dict> dictWorktype;

	// 选择的基站
	private ParString mobject;

	// 选择的单位
	private Section msection;

	// 超时时长
	private List<Dict> dictLimited;

	// 超时时长
	// private SparseArray<String> spinLimitedData = new SparseArray<String>();

	private EkeyDao ekeyDao;

	// 工单类型适配器
	private ContronAdapter<Dict> adapterWorktype;

	// 工单时长适配器
	private ContronAdapter<Dict> adapterLimited;

	// 后台请求对象
	private RequestHttp requestHttp;

	// 选择派工超时时长
	private Dict selectLimited;

	private String inTime;
	private String outTime;

	private ArrayList<Section> Objects = new ArrayList<Section>();
	private ArrayList<Section> resultObject = new ArrayList<Section>();
	private ArrayList<Section> selectedSection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tasking_add);
		mActivity = this;
		ViewUtils.inject(this);
		getBasicActionBar().setTitle("新增派工");
		ekeyDao = DBHelp.getInstance(this).getEkeyDao();
		getObject();
		getDic();
		setadapter();
		initViews();
	}

	private void initViews() {
		inDateEdit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DateTimePickDialogUtil dateTimePicKDialog = new DateTimePickDialogUtil(
						WorkBillAddActivity.this, true);
				dateTimePicKDialog.setInitDateTime(DateUtil.getCurrentDate("yyyy年MM月dd日 HH:mm"), (outTime == null || outTime.equals("")) ? null : outDateEdit.getText().toString());
				inTime = dateTimePicKDialog.dateTimePicKDialog(inDateEdit,new SimpleDateFormat("yyyyMMddHHmmss"));


			}
		});

		outDateEdit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				DateTimePickDialogUtil dateTimePicKDialog = new DateTimePickDialogUtil(
						WorkBillAddActivity.this, true);
				if (inDateEdit.getText().length() > 0) {
					dateTimePicKDialog.setInitDateTime(inDateEdit.getText().toString(), null);
				}
				else {
					dateTimePicKDialog.setInitDateTime(DateUtil.getCurrentDate("yyyy年MM月dd日 HH:mm"), null);
				}
				outTime = dateTimePicKDialog.dateTimePicKDialog(outDateEdit, new SimpleDateFormat("yyyyMMddHHmmss"));
			}
		});
	}


	/**
	 * 设置适配器
	 */
	private void setadapter() {

		if (dictWorktype == null)
			dictWorktype = new ArrayList<Dict>();

		if (dictLimited == null)
			dictLimited = new ArrayList<Dict>();
		for (int i=1; i<11; i++) {
			dictLimited.add(new Dict("work_limited", i+"小时", ""+i));
		}

		/*
		// 初始化适配器
		adapterWorktype = new ContronAdapter<Dict>(this,
				android.R.layout.simple_spinner_item, dictWorktype) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder, Dict item,
					int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};

		adapterWorktype
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mspinWorktype.setAdapter(adapterWorktype);
		mspinWorktype.setSelection(0);
		mspinWorktype.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				selectWorktype = (Dict) arg0.getItemAtPosition(arg2);

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		*/
		// 初始化适配器
		adapterLimited = new ContronAdapter<Dict>(this,
				android.R.layout.simple_spinner_item, dictLimited) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder, Dict item,
					int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};

		adapterLimited
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mspinLimited.setAdapter(adapterLimited);
		mspinLimited.setSelection(0);
		mspinLimited.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {

				selectLimited = (Dict) arg0.getItemAtPosition(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	/**
	 * 获取常量表中的派工类型、及超时时长
	 */
	private void getDic() {
		if (null == ekeyDao)
			return;
		dictWorktype = ekeyDao.getDicType("work_type");
		dictLimited = ekeyDao.getDicType("work_limited");
	}

	@OnClick({ R.id.but_sname, R.id.but_oname })
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.but_sname: {
			if (null == mobject) {
				this.showToast("请先选择基站！");
				return;
			}
			SectionActivity.startActivity(this, REQUEST_CODE_SECTION, selectedSection);
		}
			break;
		case R.id.but_oname:
			if (resultObject != null && resultObject.size() > 0) {
				DeviceActivity.startActivityForResult(this, false, REQUEST_CODE_DEVICE, resultObject);
			} else {
				DeviceActivity.startActivityForResult(this, false, REQUEST_CODE_DEVICE, Objects);
			}
			break;

		default:
			break;
		}
	}

	/**
	 * 获取设备
	 */
	public void getObject() {
		List<Section> stations = DBHelp.getInstance(mActivity).getEkeyDao()
				.getSection();
		if (stations == null)
			stations = new ArrayList<Section>();
		Objects.clear();
		Objects.addAll(stations);

	}

	/**
	 * 保存派工单
	 */
	private void saveTasking() {

		if (requestHttp == null)
			requestHttp = new RequestHttp(this.getWld());// 服务请求

		String workdesc = medWorkdesc.getText().toString();// 派工内容

		if (null == workdesc || "".equals(workdesc)) {
			this.showToast("派工内容不能为空！");
			return;
		}

		if (workdesc.length() > 500) {
			this.showToast("任务内容不得超过500位！");
			return;
		}

/*
		if (null == selectWorktype) {
			this.showToast("请选择派工类型！");
			return;
		}
*/
		inTime = DateUtil.getStringByFormat(inDateEdit.getText().toString(),"yyyy年MM月dd日 HH:mm","yyyyMMddHHmmss");

		if (null == inTime || "".equals(inTime)) {
			this.showToast("请选择进站时间！");
			return;
		}

		outTime = DateUtil.getStringByFormat(outDateEdit.getText().toString(),"yyyy年MM月dd日 HH:mm","yyyyMMddHHmmss");
		if (null == outTime || "".equals(outTime)) {
			this.showToast("请选择离站时间！");
			return;
		}

		if (TextUtils.isEmpty(mobileEdit.getText().toString())) {
			this.showToast("请填写联系电话！");
			return;
		}

		if (mobileEdit.getText().toString().length() > 20) {
			this.showToast("联系电话不得超过20位！");
			return;
		}

		if (null == selectLimited) {
			this.showToast("请选择超时时长！");
			return;
		}

		if (null == msection) {
			this.showToast("请选择执行单位！");
			return;
		}

		if (null == mobject) {
			this.showToast("请选择基站！");
			return;
		}

		String memo = mtxMemo.getText().toString();

		this.startDialog(R.string.progressDialog_title_submit);// 弹出对话框
		// 设置参数
		RequestParams params = new RequestParams(this.getUser());
		params.putParams("worktype", "");
		params.putParams("workdesc", workdesc);
		params.putParams("in_by", this.getUser().getUsername());
		params.putParams("oid", mobject.getId());
		params.putParams("sid", msection.getId());
		params.putParams("applyin_at", inTime);
		params.putParams("applyleave_at", outTime);
		params.putParams("mobile", mobileEdit.getText().toString());
		params.putParams("need_accompany", needAccompanyCheck.isChecked() ? 1 : 0);
		params.putParams("validate_lng_lat", validateCheck.isChecked() ? 1 : 0);
		params.putParams("limited", Integer.parseInt(selectLimited.getValue()));
		params.putParams("memo", memo);

		// 请求接口
		requestHttp.doPost(Globals.WORKBILL_NEW, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {
									mActivity.showMsgBox("提示", "新增派工单成功！",
											new OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
													mActivity.finish();
												}
											});
								} else {
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}
							}
						});

					}
				});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// super.onActivityResult(arg0, arg1, arg2);

		if (requestCode == REQUEST_CODE_DEVICE) {
			if (resultCode == Activity.RESULT_OK && data != null) {
				Bundle bundle = data.getExtras();
				if (bundle != null) {
					setStationsData(bundle);
				}
			}
		} else if (requestCode == REQUEST_CODE_SECTION) {

			if (resultCode == Activity.RESULT_OK && data != null) {
				Bundle bundle = data.getExtras();
				if (bundle != null) {
					setSectionData(bundle);
				}
			}
		}
	}

	/**
	 * 保存已选择的设备
	 * 
	 * @param bundle
	 */
	private void setStationsData(Bundle bundle) {
		ArrayList<Section> obj = bundle.getParcelableArrayList("stations");
		if (obj == null || obj.size() <= 0) {
			return;
		}
		resultObject.clear();
		resultObject.addAll(obj);
		if (resultObject != null && resultObject.size() > 0) {

			for (Section station : resultObject) {
				for (DeviceObject device : station.deviceList) {
					if (device.isCheck) {
						mobject = new ParString(device.id, device.getName());

						if (selectedSection == null) {
							selectedSection = new ArrayList<Section>();
						}
						selectedSection.clear();
						selectedSection.add(station);
					}
				}
			}

		}

		if (mobject != null) {
			mbutOname.setText(mobject.getName());
		}

	}

	/**
	 * 保存已选择的机构
	 * 
	 * @param bundle
	 */
	private void setSectionData(Bundle bundle) {

		msection = bundle.getParcelable("section");

		if (null != msection) {
			mbutSname.setText(msection.getName());
		}

	}

	// 界面跳转
	public static void startActivity(Context context) {
		Intent intent = new Intent(context, WorkBillAddActivity.class);
		context.startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.save, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			return true;
		case R.id.menu_save:// 保存新增派工
			saveTasking();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
