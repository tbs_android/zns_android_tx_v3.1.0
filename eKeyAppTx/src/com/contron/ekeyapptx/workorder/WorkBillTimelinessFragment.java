package com.contron.ekeyapptx.workorder;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.SectionAll;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.WorkBillTimeliness;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.SelectQueryWorkbillDialog;
import com.contron.ekeypublic.view.SelectQueryWorkbillDialog.SelectListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.contron.ekeyapptx.R;

/**
 * 派工进站及时率
 * 
 * @author luoyilong
 *
 */
public class WorkBillTimelinessFragment extends Fragment {

	@ViewInject(R.id.lv_public)
	private ListView lisview;

	@ViewInject(R.id.tx_empty)
	private TextView empty;

	@ViewInject(R.id.workbill_timeliness_head)
	private LinearLayout invis;
	
	private List<Dict> mListChecktype = new ArrayList<Dict>();
	private List<SectionAll> mListAgency = new ArrayList<SectionAll>();
	private List<SectionAll> mListSection = new ArrayList<SectionAll>();

	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求对象
	private ContronAdapter<WorkBillTimeliness> mAdapter;// 适配器
	private View loadMoreView;// 下一页视图
	private View headerView;// 表头
	private RadioButton loadMoreButton;// 下一页按钮
	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数
	private User user = null;
	private JSONObject queryJsonObject;// 后台请求参数

	

	private List<WorkBillTimeliness> workPatrolList = new ArrayList<WorkBillTimeliness>();// 保存查询出来的操作日志

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.public_list_horizontalscroll,
				container, false);// 关联布局文件
		ViewUtils.inject(this, rootView);
		mActivity = (BasicActivity) getActivity();
		headerView = inflater.inflate(R.layout.fragment_worktimeliness_head,
				null);
		user = mActivity.getUser();
		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadWorkPatrol(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item28_name);
		setHasOptionsMenu(true);

		mAdapter = new ContronAdapter<WorkBillTimeliness>(mActivity,
				R.layout.fragment_worktimeliness_item, workPatrolList) {
			@Override
			public void setViewValue(ContronViewHolder holder,
					final WorkBillTimeliness item, final int position) {
				holder.setText(R.id.queryobj, item.getName());

				Float incomeRate = Float.parseFloat(item.getIncomeRate()) * 100;

				holder.setText(R.id.tx_inrate, Float.toString(incomeRate) + "%");

				holder.setText(R.id.tx_plancount, "" + item.getShould());

				holder.setText(R.id.tx_realcount, "" + item.getActual());

				holder.setText(R.id.tx_intimecount, "" + item.getIncome());

				holder.setText(R.id.tx_uncount, "" + item.getUnfinished());

				TextView butexcutework = holder.findView(R.id.tx_operation);
				// 操作点击事件
				butexcutework.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						WorkBillTimelinessDetailActivity.startActivity(
								mActivity, item.getDetails());
					}
				});

			}
		};

		lisview.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if (firstVisibleItem >= 1) {
					invis.setVisibility(View.VISIBLE);
				} else {

					invis.setVisibility(View.GONE);
				}
			}
		});

		lisview.addFooterView(loadMoreView); // 设置列表底部视图
		lisview.addHeaderView(headerView); // 设置列表底部视图
//		lisview.addHeaderView(headerView);// ListView条目中的悬浮部分 添加到头部
		lisview.setAdapter(mAdapter);
		lisview.setEmptyView(empty);
		getData();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		downloadWorkPatrol(0);
	}

	/**
	 * 获取常量表中的派工类型、及超时时长
	 */
	private void getData() {
		EkeyDao ekeyDao = DBHelp.getInstance(mActivity).getEkeyDao();

		if (null == ekeyDao)
			return;

		mListChecktype = ekeyDao.getDicType("intime_view");
		mListAgency = ekeyDao.getSectionAll("机构");
		mListSection = ekeyDao.getSectionAll("区域");
	}

	/**
	 * 查询条件
	 */
	private void queryConditionDialog() {

		List<Dict> tempChecktype = new ArrayList<Dict>();
		tempChecktype.addAll(mListChecktype);

		List<SectionAll> tempAgency = new ArrayList<SectionAll>();
		tempAgency.addAll(mListAgency);

		List<SectionAll> tempSection = new ArrayList<SectionAll>();
		tempSection.addAll(mListSection);

		SelectQueryWorkbillDialog dialogDepartment = new SelectQueryWorkbillDialog(
				mActivity, new SelectListener() {

					@Override
					public void select(JSONObject jsonObject) {
						queryJsonObject = jsonObject;
						downloadWorkPatrol(0);

					}
				}, tempChecktype, tempAgency, tempSection, "到站及时率查询条件");
		dialogDepartment.show(mActivity.getSupportFragmentManager(), "");// 弹出缺陷地点选择框
	}

	/**
	 * 下载巡检任务
	 */
	private void downloadWorkPatrol(int currentItem) {

		if (user == null)
			return;

		if (queryJsonObject == null) {
			String currDate = DateUtil.getCurrentDate("yyyyMMdd");
			String currstartDate = DateUtil.getCurrentDate("yyyyMM");

			queryJsonObject = new JSONObject();
			try {
				queryJsonObject.put("type", 1);
				queryJsonObject.put("createfrom", currstartDate + "01000000");
				queryJsonObject.put("createto", currDate + "235959");
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		if (currentItem == 0) {// 当前页数为0时清空数据
			mcurrentItem = 0;
			workPatrolList.clear();
		}

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		// 设置参数
		try {
			queryJsonObject.put("k", user.getKValue());
			queryJsonObject.put("page", currentItem);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		requestHttp.doPost(Globals.WORKBILL_INTIME, queryJsonObject,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();

								if (value.contains("success")) {
									ArrayList<WorkBillTimeliness> listTasking;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										JSONArray jsonArrayTempTask = jsonObject
												.getJSONArray("items");
										// 获取任务
										listTasking = new Gson().fromJson(
												jsonArrayTempTask.toString(),
												new TypeToken<List<WorkBillTimeliness>>() {
												}.getType());

										// 获取总页数
										if (mcurrentItem == 0) {
											mItemCount = jsonObject
													.getInt("pages");

										}

										if (listTasking != null
												&& listTasking.size() > 0) {
											workPatrolList.addAll(listTasking);
										}
										mAdapter.notifyDataSetChanged();
									} catch (JSONException e) {
										e.printStackTrace();
									}

								} else {
									try {
										JSONObject json = new JSONObject(value);
										String error = json.getString("error");
										mActivity.showMsgBox(error);

									} catch (JSONException e) {
										e.printStackTrace();
									}

								}

							}
						});

					}
				});
	}

	/**
	 * 获取实例
	 * 
	 * @return
	 */
	public static WorkBillTimelinessFragment newInstance() {
		return new WorkBillTimelinessFragment();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.select, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();
			break;
		case R.id.menu_search:
			queryConditionDialog();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
