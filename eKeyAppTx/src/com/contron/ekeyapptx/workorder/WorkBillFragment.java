package com.contron.ekeyapptx.workorder;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.Workbill;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.ViewUtils;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 任务派工
 * 
 * @author luoyilong
 *
 */
public class WorkBillFragment extends Fragment {

	@ViewInject(R.id.lv_public)
	private ListView lisview;

	@ViewInject(R.id.tx_empty)
	private TextView empty;

	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求对象
	private ContronAdapter<Workbill> mAdapter;// 适配器
	private View loadMoreView;// 下一页视图
	private RadioButton loadMoreButton;// 下一页按钮
	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数
	private User user = null;
	private List<Workbill> taskings = new ArrayList<Workbill>();// 保存查询出来的操作日志

	/**
	 * 获取实例
	 * 
	 * @return
	 */
	public static WorkBillFragment newInstance() {
		WorkBillFragment fragment = new WorkBillFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
		user = mActivity.getUser();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.public_list, container, false);
		ViewUtils.inject(this, view);

		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadTasking(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mActivity.getBasicActionBar().setTitle(
				getString(R.string.main_tab_item24_name));
		setHasOptionsMenu(true);

		mAdapter = new ContronAdapter<Workbill>(mActivity,
				R.layout.fragment_workbill_item, taskings) {
			@Override
			public void setViewValue(ContronViewHolder holder,
					final Workbill item, final int position) {
				holder.setText(R.id.tx_workdesc, "派工内容：" + item.getWorkdesc());

				holder.setText(R.id.tx_worktype, "派工类型：" + item.getWorktype());

				holder.setText(R.id.tx_limited, "超时时长：" + item.getLimited()
						+ "小时");

				holder.setText(R.id.tx_oname, "基        站：" + item.getOname());

				holder.setText(R.id.tx_sname, "执行单位：" + item.getSname());

				holder.setText(R.id.tx_create_by,
						"派  工  人：" + item.getApply_by());

				holder.setText(R.id.tx_createtime,
						"派工时间：" + item.getApply_at());

				holder.setText(R.id.tx_wkb_applyin_at,"进站时间：" + item.getApplyin_at());

				holder.setText(R.id.tx_wkb_applylevae_at,"离站时间：" + item.getApplyleave_at());

				holder.setText(R.id.tx_status, "工单状态：" + item.getStatus());

				// if (!TextUtils.isEmpty(item.getBeginAt()))
				// holder.setText(R.id.tx_begin_at,
				// "开始时间：" + DateUtil.timeCompare(item.getBeginAt()));
				// if (!TextUtils.isEmpty(item.getEndAt()))
				// holder.setText(R.id.tx_end_at,
				// "结束时间：" + DateUtil.timeCompare(item.getEndAt()));

			}
		};
		lisview.addFooterView(loadMoreView); // 设置列表底部视图
		lisview.setAdapter(mAdapter);
		lisview.setEmptyView(empty);

	}

	@Override
	public void onResume() {
		super.onResume();
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item24_name);
		downloadTasking(0);
	}

	/**
	 * 下载派工任务
	 */
	private void downloadTasking(int currentItem) {

		if (user == null)
			return;
		
		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		if (currentItem == 0) {// 当前页数为0时清空数据
			mcurrentItem = 0;
			taskings.clear();
			mAdapter.notifyDataSetChanged();
		}

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		// 设置参数
		RequestParams params = new RequestParams(user);
		params.putParams("page", currentItem);
		// params.putParams("create_username", user.getUsername());

		requestHttp.doPost(Globals.WORKBILL_GET, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {
									ArrayList<Workbill> listTasking;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										JSONArray jsonArrayTempTask = jsonObject
												.getJSONArray("items");
										// 获取任务
										listTasking = new Gson().fromJson(
												jsonArrayTempTask.toString(),
												new TypeToken<List<Workbill>>() {
												}.getType());

										// 获取总页数
										if (mcurrentItem == 0) {
											mItemCount = jsonObject
													.getInt("pages");
										}

										if (listTasking != null
												&& listTasking.size() > 0) {
											taskings.addAll(listTasking);
											mAdapter.notifyDataSetChanged();

											DbUtils dbUtils = DBHelp.getInstance(mActivity).getDb();
											SQLiteDatabase database = dbUtils.getDatabase();
											try {
												database.beginTransaction();
												dbUtils.replaceAll(listTasking);// 保存到数据库
												database.setTransactionSuccessful();
											} catch (DbException e) {
												e.printStackTrace();
											} finally {
												database.endTransaction();
											}
										}

									} catch (JSONException e) {
										e.printStackTrace();
									}

								} else {
									try {
										JSONObject json = new JSONObject(value);
										String error = json.getString("error");
										mActivity.showMsgBox(error);

									} catch (JSONException e) {
										e.printStackTrace();
									}

								}

							}
						});

					}
				});
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		boolean per = mActivity.isPermision("新建派工单");
		if (per) {
			inflater.inflate(R.menu.add, menu);
		}
		// super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();
			break;
		case R.id.menu_add:
			WorkBillAddActivity.startActivity(mActivity);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
