package com.contron.ekeyapptx;

import java.util.ArrayList;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.bluetooth.BtConnAuto;
import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType.BtPIO;
import com.lidroid.xutils.util.LogUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

public class SettingLockTypeFragment extends ListFragment {

	private BasicActivity mActivity;
	private BtMsgService mBtMsgService;
	private BluetoothAdapter mBluetoothAdapter;
	private boolean showDialog;
	private ProgressDialog mProgressDialog;
	private boolean mScanning;
	private Handler mHandler = new Handler();
	private ContronAdapter<Lockset> mContronAdapter;
	private Intent gattServiceIntent;

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,
				IBinder service) {
			mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
			if (mBtMsgService.initialize()) {
                mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
				LogUtils.i("蓝牙服务绑定成功");
				mBluetoothAdapter = mBtMsgService.getBluetoothAdapter();
				mActivity.startDialog(R.string.progressDialog_title_searchline);
				scanLeDevice(true);
			} else {
				LogUtils.e("Unable to initialize Bluetooth");
				mActivity.showToast("绑定蓝牙服务失败");
				mActivity.finish();
			}

		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBtMsgService = null;
		}
	};

	public static SettingLockTypeFragment newInstance() {
		return new SettingLockTypeFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
	}

	@Override
	public void onResume() {
		super.onResume();
		mActivity.getBasicActionBar().setTitle(R.string.setting_lock_type);
	}

	// @Override
	// public View onCreateView(LayoutInflater inflater, ViewGroup container,
	// Bundle savedInstanceState) {
	// View view = inflater.inflate(R.layout.fragment_settinglocktype,
	// container, false);
	// ViewUtils.inject(this, view);
	// return view;
	// }

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		EventBusManager.getInstance().register(this);
		gattServiceIntent = new Intent(mActivity, BtMsgService.class);

		mActivity.bindService(gattServiceIntent, mServiceConnection,
				Activity.BIND_AUTO_CREATE);

		mContronAdapter = new ContronAdapter<Lockset>(mActivity,
				R.layout.fragment_settinglocktype_item,
				new ArrayList<Lockset>()) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					Lockset item, int position) {
				TextView txName = viewHolder.findView(R.id.tx_name);
				RadioButton radconnect = viewHolder.findView(R.id.rad_connect);

			}
		};

		setListAdapter(mContronAdapter);
		setEmptyText("暂无数据");

	}

	/**
	 * 连接蓝牙
	 * 
	 * @author hupei
	 * @date 2015�?5�?9�? 上午9:51:31
	 */
	private void connBt(String address) {
		if (mBtMsgService != null) {
			LogUtils.e("连接�?�?");
			
			String bttmp = address.replaceAll("(.{2})", "$1:");
			String btaddr = bttmp.substring(0, bttmp.length() - 1);
			mBtMsgService.autoConnect(btaddr);
			showDialog = true;
		}
	}

	/**
	 * 用于非提示的自动连接蓝牙
	 * 
	 * @author guanhaiping
	 * @date 2015�?11�?18日上�?9:03:48
	 * @param address
	 */
	private void connBtNoDialog(String address) {
		if (mBtMsgService != null) {
			LogUtils.e("自动连接�?�?");
			String bttmp = address.replaceAll("(.{2})", "$1:");
			String btaddr = bttmp.substring(0, bttmp.length() - 1);
			mBtMsgService.autoConnect(btaddr);
			showDialog = false;
		}
	}

	/**
	 * 蓝牙锁搜�?
	 * 
	 * @param enable
	 */
	private void scanLeDevice(final boolean enable) {
		if (enable && !mScanning) {
			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (mScanning) {
						mScanning = false;
						mBluetoothAdapter.stopLeScan(mLeScanCallback);
						mActivity.dismissDialog();
					}
				}
			}, 10 * 1000);
			mScanning = true;
			mBluetoothAdapter.startLeScan(mLeScanCallback);
		} else {
			mActivity.dismissDialog();
			mScanning = false;
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
		}
	}

	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice bluetoothDevice,
				final int rssi, final byte[] scanRecord) {
			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					LogUtils.i("device=" + bluetoothDevice.getAddress());
					String address = bluetoothDevice.getAddress().replace(":",
							"");

					Lockset mLockset = DBHelp.getInstance(mActivity)
							.getEkeyDao().findFirstLockset(address);

					if (mLockset != null
							&& !mContronAdapter.getDatas().contains(mLockset)) {
						// mDoorLocker = new DoorLocker();
						// mDoorLocker.state = 1; // 授权的钥�? 演示�?
						// mDoorLocker.lockerTypeId = mLockset.getType()
						// .substring(0, 2);
						// // mDoorLocker.lockerTypeId="AT";
						// mDoorLocker.bthAddr = mLockset.getBtaddr();
						// mDoorLocker.deviceName = mLockset.getName();
						// mDoorLocker.lockerId = mLockset.getId();
						// mDoorLocker.section = mLockset.getSection();

						mContronAdapter.addData(mLockset);
					}

				}
			});
		}
	};

	

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

		super.onCreateOptionsMenu(menu, inflater);
	}

	/**
	 * 接收BtMsgService发出的PIO命令 <br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015�?9�?22�? 上午10:31:51
	 * @param btPIO
	 */
	public void onEventMainThread(BtPIO btPIO) {
		if (!btPIO.isSuccess())
			return;

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				mBtMsgService.writeValueString("AT+PIO20");
			}
		}, 2000);
	}

	/**
	 * 接收BtMsgService发出的蓝牙自动连�? <br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015�?9�?22�? 上午11:24:45
	 * @param btConnAuto
	 */
	public void onEventMainThread(BtConnAuto btConnAuto) {
		if (btConnAuto.isConnFinish()) {
			mBtMsgService.removeCallbacksConn();
			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
				mProgressDialog = null;
			}
		}
	}

	/**
	 * 接收BtMsgService发出的状�?<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015�?9�?22�? 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtState state) {
		switch (state) {
		case DISCONNECTED:
			break;
		case CONNECTED:
			break;
		case DISCOVERED:
			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
				mProgressDialog = null;
			}
			// if (mDoorLocker != null) {
			// if (showDialog == false) {// 挨个去查询蓝牙锁状�??
			// if (!mDoorLocker.lockerTypeId.equalsIgnoreCase("AT"))
			// mBtMsgService.writeValue(new BtSendCheckLockerState()
			// .createBtMsg());
			// } else
			// // 连接成功会弹出，�?选择设备的提示框
			// showDoorLockerDiaog(mDoorLocker);
			// }
			break;
		default:
			break;
		}
	}

	
	private void onBack() {
		if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
			mActivity.getSupportFragmentManager().popBackStack();
		else
			mActivity.finish();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBack();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		scanLeDevice(false);

		EventBusManager.getInstance().unregister(this);

		if (mBtMsgService != null) {
			if (mBtMsgService.isConnected())// 连接中则断开
				mBtMsgService.disconnect();
			else
				mBtMsgService.removeCallbacksConn();
		}
		mActivity.unbindService(mServiceConnection);
	}

}
