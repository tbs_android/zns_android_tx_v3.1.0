/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx;

import java.util.List;

import com.contron.ekeypublic.common.OnToggleFragmentListener;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.view.MsgBoxFragment;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

/**
 * APP基础activity
 * 
 * @author hupei
 * @date 2015年4月1日 上午9:50:24
 */
public class BasicActivity extends FragmentActivity implements
		OnToggleFragmentListener {
	protected static final int EXIT_TIME_END = 2000;// 按二次退出时间的间隔
	protected long exitTime = 0;// 记录按第一次退出的时间
	private ProgressDialog progressDialog;

	// @Override
	// protected void onCreate(Bundle arg0) {
	// super.onCreate(arg0);
	//
	// StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
	// .detectDiskReads().detectDiskWrites().detectNetwork()
	// .penaltyLog().build());
	// StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
	// .detectLeakedSqlLiteObjects().detectLeakedClosableObjects()
	// .penaltyLog().penaltyDeath().build());
	//
	// }

	public ActionBar getBasicActionBar() {
		// ActionBar actionBar= getSupportActionBar();
		android.app.ActionBar actionBar = this.getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(false);
		return actionBar;
	}

	public ActionBar getLoginBasicActionBar() {
		// ActionBar actionBar= getSupportActionBar();
		android.app.ActionBar actionBar = this.getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowHomeEnabled(true);
		return actionBar;
	}

	public void showMsgBox(int messageId) {
		showMsgBox(0, messageId, true, null, null);
	}

	public void showMsgBox(String message) {
		showMsgBox(0, message, true, null, null);
	}

	public void showMsgBox(int messageId,
			DialogInterface.OnClickListener okListener) {
		showMsgBox(0, messageId, true, okListener, null);
	}

	protected void showMsgBox(int titleId, int messageId) {
		showMsgBox(titleId, messageId, true, null, null);
	}

	protected void showMsgBox(int titleId, int messageId,
			DialogInterface.OnClickListener okListener) {
		showMsgBox(titleId, messageId, false, okListener, null);
	}

	public void showMsgBox(String titleId, String messageId,
			DialogInterface.OnClickListener okListener) {
		showMsgBox(titleId, messageId, false, okListener, null);
	}

	public void showMsgBox(int titleId, int messageId, boolean cancel,
			DialogInterface.OnClickListener okListener,
			DialogInterface.OnClickListener noListener) {
		showMsgBox(titleId == 0 ? "" : getString(titleId),
				getString(messageId), cancel, okListener, noListener);
	}

	protected void showMsgBox(int titleId, String messageId, boolean cancel,
			DialogInterface.OnClickListener okListener,
			DialogInterface.OnClickListener noListener) {
		showMsgBox(titleId == 0 ? "" : getString(titleId), messageId, cancel,
				okListener, noListener);
	}

	public void showMsgBox(String title, String message, boolean cancel,
			DialogInterface.OnClickListener okListener,
			DialogInterface.OnClickListener noListener) {
		MsgBoxFragment boxFragment = new MsgBoxFragment();
		boxFragment.setTitle(title);
		boxFragment.setMessage(message);
		boxFragment.setPositiveOnClickListener(okListener);
		boxFragment.setNegativeOnClickListener(noListener);
		boxFragment.setCanceledOnTouchOutside(cancel);
		boxFragment.show(getFragmentManager(), "msgBox");

	}

	public void showToast(int textResourceId) {
		showToast(getResources().getString(textResourceId));
	}

	public void showToast(CharSequence text) {
		Toast.makeText(this, text, Toast.LENGTH_LONG).show();
	}

	// 隐藏软键盘
	public void hideSoftInputView() {
		InputMethodManager manager = ((InputMethodManager) this
				.getSystemService(Activity.INPUT_METHOD_SERVICE));
		if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN) {
			if (getCurrentFocus() != null)
				manager.hideSoftInputFromWindow(getCurrentFocus()
						.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO 处理自动注销清零业务
		EkeyAPP.getInstance().clearRecLen();
		return super.dispatchTouchEvent(ev);
	}

	@Override
	public void hideAdd(Class<? extends Fragment> hideFragmentClass,
			Fragment addFragment) {
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		if (!addFragment.isAdded())// 先判断是否被add过
			transaction.add(R.id.content, addFragment,
					addFragment.getClass().getSimpleName())
					.addToBackStack(null);
		Fragment fragment = fm.findFragmentByTag(hideFragmentClass
				.getSimpleName());
		if (fragment != null)
			transaction.hide(fragment);
		transaction.commit();
		// transaction.commitAllowingStateLoss();
	}

	@Override
	public void repalce(Fragment fragment, boolean isToBackStack) {
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		if (!fragment.isAdded())// 不存在则加，否则隐藏，内存不足时
			transaction.replace(R.id.content, fragment, fragment.getClass()
					.getSimpleName());
		else
			transaction.hide(fragment);
		if (isToBackStack)// 判断是否添加到堆栈中
			transaction.addToBackStack(null);
		transaction.commit();

		// transaction.commitAllowingStateLoss();

	}

	/**
	 * 圆形进度对话框
	 * 
	 * @param context
	 *            上下文
	 * @param tip
	 *            显示内容
	 * */
	public ProgressDialog showSpinnerDialog(final Context context, String tip,
			DialogInterface.OnClickListener clickListener) {
		return showSpinnerDialog(context, "", tip, clickListener);
	}

	/**
	 * 圆形进度对话框
	 * 
	 * @param context
	 *            上下文
	 * @param tip
	 *            显示内容
	 * */
	public ProgressDialog showSpinnerDialog(final Context context, String tip) {
		return showSpinnerDialog(context, "", tip, null);
	}

	/**
	 * 圆形进度对话框
	 * 
	 * @param context
	 *            上下文
	 * @param title
	 *            显示标题
	 * @param tip
	 *            显示内容
	 * @param clickListener
	 *            取消事件
	 * */
	@SuppressWarnings("deprecation")
	public ProgressDialog showSpinnerDialog(final Context context,
			String title, String tip,
			DialogInterface.OnClickListener clickListener) {
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		if (!TextUtils.isEmpty(title))
			progressDialog.setTitle(title);
		progressDialog.setMessage(tip);
		progressDialog.setCancelable(true);

		if (clickListener != null)
			progressDialog.setButton("取消", clickListener);

		progressDialog.show();
		return progressDialog;
	}
	/**
	 * 判断 用户权限
	 * @param permision
	 * @return true/有权限，false/无权限
	 */
	public boolean isPermision(String permision) {
		List<String> listPermision = getUser().getPermision();
	
		if (listPermision == null || listPermision.size() == 0
				|| "".equals(permision))
			return false;

		for (String per : listPermision) {
			if (permision.equals(per)||"0".equals(per)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 显示对话框
	 */
	public void startDialog(int msg) {
		progressDialog = showSpinnerDialog(this, getResources().getString(msg));
	}
	
	/**
	 * 显示对话框
	 */
	public void startDialog(String msg) {
		progressDialog = showSpinnerDialog(this, msg);
	}

	/**
	 * 显示对话框
	 */
	public void startDialog(int msg,
			DialogInterface.OnClickListener clickListener) {
		progressDialog = showSpinnerDialog(this, "",
				getResources().getString(msg), clickListener);
	}

	/**
	 * 显示对话框
	 */
	public void startDialog(String msg,
			DialogInterface.OnClickListener clickListener) {
		progressDialog = showSpinnerDialog(this, "", msg, clickListener);
	}

	/**
	 * 关闭对话框
	 */
	public void dismissDialog() {
		if (progressDialog != null)
			progressDialog.dismiss();
	}

	/**
	 * 获取服务地址
	 * 
	 * @return
	 */
	public String getWld() {
		return EkeyAPP.getInstance().getWLD();
	}

	/**
	 * 获取用户
	 * 
	 * @return
	 */
	public User getUser() {
		return EkeyAPP.getInstance().getUser();
	}

}
