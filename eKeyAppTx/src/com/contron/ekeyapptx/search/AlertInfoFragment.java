package com.contron.ekeyapptx.search;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Alert;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.ClearEditText;
import com.contron.ekeypublic.view.DateTimePickerFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.contron.ekeypublic.util.DateUtil.DATE_FORMAT_YMDHMS;
import static com.contron.ekeypublic.util.DateUtil.dateFormatYMDHMS;

public class AlertInfoFragment extends Fragment {
    @ViewInject(R.id.lv_public)
    private ListView lisview;

    @ViewInject(R.id.tx_empty)
    private TextView empty;

    @ViewInject(R.id.btn_search)
    private TextView btnSearch;

    @ViewInject(R.id.et_search)
    private ClearEditText etSearch;

    @ViewInject(R.id.btn_begin_time)
    private Button btnBeginTime;

    @ViewInject(R.id.btn_end_time)
    private Button btnEndTime;

    private Calendar beginCalendar = Calendar.getInstance();
    private Calendar endCalendar = Calendar.getInstance();
    private String strStartTime = "";
    private String strEndTime = DateUtil.getCurrentDate(DateUtil.DATE_FORMAT_YMDHMS);

    private BasicActivity mActivity;
    private RequestHttp requestHttp;// 后台请求对象
    private ContronAdapter<Alert> mAdapter;// 适配器
    private View loadMoreView;// 下一页视图
    private RadioButton loadMoreButton;// 下一页按钮
    private int mcurrentItem = 0;// 当前页数
    private int mItemCount = 1;// 总页数
    private User user = null;

    private List<Alert> alerts = new ArrayList<Alert>();// 保存查询出来的操作日志

    /**
     * 获取实例
     *
     * @return
     */
    public static AlertInfoFragment newInstance() {
        AlertInfoFragment fragment = new AlertInfoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alert, container, false);
        ViewUtils.inject(this, view);
        String startTime = DateUtil.getCurrentDate("yyyyMMdd");
        strStartTime = startTime + "000000";

        loadMoreView = inflater.inflate(R.layout.item_footerview, null);
        loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

        // 下一页按钮
        loadMoreButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                mcurrentItem++;
                if (mcurrentItem < mItemCount)// 当前页小于总页数
                {
                    downloadAlert(mcurrentItem);
                } else {
                    mActivity.showMsgBox("已经是最后页了！");
                }

            }

        });

        btnSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadAlert(0);
            }
        });
        btnBeginTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTimePickerFragment datePickerBegin = DateTimePickerFragment
                        .newInstance(
                                new DateTimePickerFragment.OnDateTimeDoneListener() {

                                    @Override
                                    public void onDateTime(int year,
                                                           int monthOfYear, int dayOfMonth,
                                                           int hourOfDay, int minute,
                                                           String dateTime) {

                                        beginCalendar.set(Calendar.YEAR, year);
                                        beginCalendar.set(Calendar.MONTH,
                                                monthOfYear);
                                        beginCalendar.set(Calendar.DAY_OF_MONTH,
                                                dayOfMonth);
                                        beginCalendar.set(Calendar.HOUR_OF_DAY,
                                                hourOfDay);
                                        beginCalendar.set(Calendar.MINUTE, minute);

                                        strStartTime = dateTime;

                                        btnBeginTime.setText(dateTime.substring(0, 16));
                                    }
                                }, true);
                datePickerBegin.setInitDateTime(DateUtil.timeCompareToLine(strStartTime), DateUtil.timeCompareToLine(strEndTime));
                datePickerBegin.show(mActivity.getFragmentManager(), "dateStart");
            }
        });
        btnEndTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DateTimePickerFragment datePickerEnd = DateTimePickerFragment
                        .newInstance(
                                new DateTimePickerFragment.OnDateTimeDoneListener() {

                                    @Override
                                    public void onDateTime(int year,
                                                           int monthOfYear, int dayOfMonth,
                                                           int hourOfDay, int minute,
                                                           String dateTime) {

                                        endCalendar.set(Calendar.YEAR, year);
                                        endCalendar.set(Calendar.MONTH,
                                                monthOfYear);
                                        endCalendar.set(Calendar.DAY_OF_MONTH,
                                                dayOfMonth);
                                        endCalendar.set(Calendar.HOUR_OF_DAY,
                                                hourOfDay);
                                        endCalendar.set(Calendar.MINUTE, minute);

                                        strEndTime = dateTime;

                                        btnEndTime.setText(dateTime.substring(0, 16));
                                    }
                                }, true);
                    datePickerEnd.setInitDateTime(DateUtil.getCurrentDate(dateFormatYMDHMS), null);
                datePickerEnd.show(mActivity.getFragmentManager(), "dateEnd");
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item33_name));
        setHasOptionsMenu(true);

        mAdapter = new ContronAdapter<Alert>(mActivity,
                R.layout.fragment_alert_item, alerts) {
            @Override
            public void setViewValue(ContronViewHolder holder,
                                     final Alert item, final int position) {
                holder.setText(R.id.tx_msg, item.getMsg());
                String alertTime = DateUtil.getStringByFormat(item.getCreate_at(), DATE_FORMAT_YMDHMS, dateFormatYMDHMS);
                holder.setText(R.id.tx_msg_date, alertTime);

            }
        };
        lisview.addFooterView(loadMoreView); // 设置列表底部视图
        lisview.setAdapter(mAdapter);
        lisview.setEmptyView(empty);
        downloadAlert(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item33_name);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 下载告警信息
     */
    private void downloadAlert(int currentItem) {

        if (user == null)
            return;

        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

        if (currentItem == 0) {// 当前页数为0时清空数据
            mcurrentItem = 0;
            alerts.clear();
            mAdapter.notifyDataSetChanged();
        }

        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框


        // 设置参数
        RequestParams params = new RequestParams(user);
        params.putParams("page", currentItem);
        try {
            JSONObject createAt = new JSONObject().put("from", strStartTime).put("to", strEndTime);
            params.putParams("create_at", createAt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String searchText = etSearch.getText().toString().trim();
        if (!"".equals(searchText)) {
            params.putParams("search", searchText);
        }

        requestHttp.doPost(Globals.ALERT, params,
                new HttpRequestCallBackString() {

                    @Override
                    public void resultValue(final String value) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mActivity.dismissDialog();
                                if (value.contains("success")) {
                                    ArrayList<Alert> listTasking;
                                    try {
                                        JSONObject jsonObject = new JSONObject(
                                                value);
                                        JSONArray jsonArrayTempTask = jsonObject
                                                .getJSONArray("items");
                                        // 获取任务
                                        listTasking = new Gson().fromJson(
                                                jsonArrayTempTask.toString(),
                                                new TypeToken<List<Alert>>() {
                                                }.getType());

                                        // 获取总页数
                                        if (mcurrentItem == 0) {
                                            mItemCount = jsonObject
                                                    .getInt("pages");
                                        }

                                        if (listTasking != null
                                                && listTasking.size() > 0) {
                                            alerts.addAll(listTasking);
                                            mAdapter.notifyDataSetChanged();

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    try {
                                        JSONObject json = new JSONObject(value);
                                        String error = json.getString("error");
                                        mActivity.showMsgBox(error);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }
                        });

                    }
                });
    }


}
