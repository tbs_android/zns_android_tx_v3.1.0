package com.contron.ekeyapptx;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.LockerResult;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.CygPickerDateFragment;
import com.contron.ekeypublic.util.CygPickerDateFragment.OnDateDoneListener;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.ImageTextButton;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * 
 * @author luoyilong 操作记录查询
 */
public class OperateSelectFragment extends Fragment {

	private BasicActivity mActivity;

	@ViewInject(R.id.imgbut_startdate)
	private ImageTextButton startimgbut;// 开始时间

	@ViewInject(R.id.imgbut_stopdate)
	private ImageTextButton stoptimgbut;// 结束时间

	@ViewInject(R.id.listview_selectoperate)
	private ListView lisview;

	@ViewInject(R.id.tx_empty)
	private TextView empty;

	private View loadMoreView;// 下一页视图
	private RadioButton loadMoreButton;// 下一页按钮
	private RequestHttp requestHttp;// 服务请求

	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数

	private List<LockerResult> doorLockers = new ArrayList<LockerResult>();// 保存查询出来的操作日志

	private ContronAdapter<LockerResult> mAdapter;

	public static OperateSelectFragment newInstance() {
		return new OperateSelectFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_operate_select,
				container, false);
		ViewUtils.inject(this, view);

		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					String startim = startimgbut.getText().toString();
					String stoptim = stoptimgbut.getText().toString();
					selectResult(startim, stoptim, mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		mActivity = (BasicActivity) getActivity();
		mActivity.getBasicActionBar().setTitle("操作记录查询");
		
		stoptimgbut.setText(DateUtil.getCurrentDate("yyyy-MM-dd"));// 设置初始结束时间
		requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		mAdapter = new ContronAdapter<LockerResult>(mActivity,
				R.layout.fragment_operate_item, doorLockers) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					LockerResult item, int position) {

				LinearLayout layout = (LinearLayout) viewHolder
						.findView(R.id.layout_operate);

				if (position == 0) {// 第一个item设置背景颜色
					layout.setBackgroundColor(getResources().getColor(
							R.color.bg_titlebar));
				} else {
					layout.setBackgroundColor(getResources().getColor(
							android.R.color.white));
				}

				viewHolder
						.setText(R.id.tx_lockname, "操作设备：" + item.getOname())
						.setText(R.id.tx_lockby, "操  作  人：" + item.getUnlock_by())
						.setText(
								R.id.tx_lockdate,
								"操作日期："
										+ DateUtil.timeCompare(item
												.getUnlock_at()));

				String state = item.getResult();
				TextView tvStatus = viewHolder.findView(R.id.tx_lockstate);

				tvStatus.setText(state);

				if (!TextUtils.isEmpty(state)) {
					if (state.contains("失败") || state.contains("越权")) {
						tvStatus.setTextColor(Color.RED);
					} else {
						tvStatus.setTextColor(Color.GREEN);
					}
				}

			}
		};

		lisview.addFooterView(loadMoreView); // 设置列表底部视图
		lisview.setAdapter(mAdapter);
		lisview.setEmptyView(empty);
		selectResult("", "", 0);// 查询操作日志
	}

	/**
	 * 查询操作日志
	 * 
	 * @param ticketResult
	 */
	private void selectResult(String startDate, String stopDate, int currentItem) {

		mActivity.startDialog(R.string.progressDialog_title_login);// 弹出对话框

		if (currentItem == 0) {// 当前页数为0时清空数据			
			mcurrentItem=0;
			doorLockers.clear();
			mAdapter.notifyDataSetChanged();
		}

		String startTempDate = "";
		String stoptTempDate = "";

		if (!TextUtils.isEmpty(startDate))
			startTempDate = startDate.replace("-", "") + "000000";

		if (!TextUtils.isEmpty(stopDate))
			stoptTempDate = stopDate.replace("-", "") + "235959";
		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("unlock_at_from", startTempDate);
		params.putParams("unlock_at_to", stoptTempDate);
		params.putParams("page", currentItem);

		requestHttp.doPost(Globals.HISTORY, params,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {

						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {// 返回成功
								if (value.contains("success")) {
									List<LockerResult> doorLocker = JsonTools
											.getOperationResult(value);

									// 获取总页数
									if (mcurrentItem == 0) {
										try {
											JSONObject json = new JSONObject(
													value);
											mItemCount = json.getInt("pages");
										} catch (JSONException e) {
											e.printStackTrace();
										}
									}

//									if (mItemCount < 2 && mcurrentItem == 0) {
//										lisview.removeFooterView(loadMoreView);
//										// 删除列表底部视图
//									}

									// 加载数据
									if (doorLocker != null
											&& doorLocker.size() > 0) {
										doorLockers.addAll(doorLocker);
										mAdapter.notifyDataSetChanged();

									}
								}

								mActivity.dismissDialog();
							}
						});

					}
				});

	}

	@OnClick({ R.id.imgbut_startdate, R.id.imgbut_stopdate })
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgbut_startdate:
			getDate(R.id.imgbut_startdate);
			break;
		case R.id.imgbut_stopdate:
			getDate(R.id.imgbut_stopdate);
			break;
		}

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 弹出日期选择框获取时间
	 * 
	 * @param button
	 *            点击的控
	 * 
	 */
	private void getDate(final int button) {

		CygPickerDateFragment date = CygPickerDateFragment
				.newInstance(new OnDateDoneListener() {

					@Override
					public void onDate(int year, int monthOfYear,
							int dayOfMonth, String date) {
						LogUtils.e("=====日期=======�?" + date);
						switch (button) {
						case R.id.imgbut_startdate:
							startDateCompare(date);
							break;
						case R.id.imgbut_stopdate:
							stopDateCompare(date);
							break;
						}
					}
				});

		date.show(mActivity.getFragmentManager(), "datetime");
	}

	/**
	 * 对所选择开始时间进行判断、更新
	 * 
	 * @param startDate
	 */
	private void startDateCompare(String startDate) {

		if (TextUtils.isEmpty(startDate)
				|| startDate.equals(startimgbut.getText().toString()))
			return;

		if (DateUtil.timeCompareWithCurrentDate(startDate)) {
			mActivity.showToast("开始时间不能大于当前时间！");
			return;
		}

		String stopDate = stoptimgbut.getText();
		if (!TextUtils.isEmpty(stopDate)) {
			if (DateUtil.timeCompare(stopDate, startDate)) {
				mActivity.showToast("开始时间不能大于结束时间！");
				return;
			}
		}
		startimgbut.setText(startDate);
		selectResult(startDate, stopDate, 0);
	}

	/**
	 * 对所选择结束时间进行判断、更新
	 * 
	 * @param date
	 */
	private void stopDateCompare(String stopDate) {

		if (TextUtils.isEmpty(stopDate)
				|| stopDate.equals(stoptimgbut.getText().toString()))
			return;

		if (DateUtil.timeCompareWithCurrentDate(stopDate)) {
			mActivity.showToast("结束时间不能大于当前时间！");
			return;
		}

		String startDate = startimgbut.getText();
		if (!TextUtils.isEmpty(startDate)) {
			if (DateUtil.timeCompare(stopDate, startDate)) {
				mActivity.showToast("结束时间不能小于开始时间！");
				return;
			}
		}
		stoptimgbut.setText(stopDate);
		selectResult(startDate, stopDate, 0);
	}

}
