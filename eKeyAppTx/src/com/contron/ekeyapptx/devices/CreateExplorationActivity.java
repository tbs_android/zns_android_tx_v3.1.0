package com.contron.ekeyapptx.devices;


import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.plan.DeviceActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.ParString;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateExplorationActivity extends BasicActivity {

    private static final int REQUEST_CODE_DEVICE = 100;
    private static final int REQUEST_CODE_SECTION = 101;

    @ViewInject(R.id.device_desc_edit)
    private EditText device_desc_edit;

    @ViewInject(R.id.select_device_btn)
    private Button select_device_btn;

    @ViewInject(R.id.device_1_img)
    private ImageView device_1_img;

    @ViewInject(R.id.device_2_img)
    private ImageView device_2_img;

    @ViewInject(R.id.device_3_img)
    private ImageView device_3_img;

    @ViewInject(R.id.device_4_img)
    private ImageView device_4_img;

    @ViewInject(R.id.device_1_del)
    private TextView device_1_del;

    @ViewInject(R.id.device_2_del)
    private TextView device_2_del;

    @ViewInject(R.id.device_3_del)
    private TextView device_3_del;

    @ViewInject(R.id.device_4_del)
    private TextView device_4_del;

    private ArrayList<Section> objects = new ArrayList<Section>();

    // 选择的基站
    private ParString mobject;
    private String mPicPath;
    private String mDirPath;

    // 选择的单位
    private Section msection;
    private RequestHttp requestHttp;
    private String eid = "1";
    private boolean mIsPickCamera;
    private ArrayList<Section> resultObject = new ArrayList<Section>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_exploration);
        ViewUtils.inject(this);
        getBasicActionBar().setTitle("新增设备勘查");
        getObject();
        mDirPath = Environment.getExternalStorageDirectory()+"/Ekey/";
        mPicPath = mDirPath + "temp.png";
    }

    /**
     * 获取设备
     */
    public void getObject() {
        EkeyDao ekeyDao = DBHelp.getInstance(this).getEkeyDao();
        List<Section> stations = ekeyDao.getSection();
        if (stations == null)
            stations = new ArrayList<Section>();
        objects.clear();
        objects.addAll(stations);

    }

    @OnClick({ R.id.select_device_btn, R.id.device_1_img,
            R.id.device_2_img, R.id.device_3_img, R.id.device_4_img,
            R.id.device_1_del,
            R.id.device_2_del, R.id.device_3_del, R.id.device_4_del})
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.select_device_btn:
                if (resultObject != null && resultObject.size() > 0) {
                    DeviceActivity.startActivityForResult(this, false, REQUEST_CODE_DEVICE, resultObject);
                } else {
                    DeviceActivity.startActivityForResult(this, false, REQUEST_CODE_DEVICE, objects);
                }
                break;
            case R.id.device_1_img:
                showPicturePicker(1);
                break;
            case R.id.device_2_img:
                showPicturePicker(2);
                break;
            case R.id.device_3_img:
                showPicturePicker(3);
                break;
            case R.id.device_4_img:
                showPicturePicker(4);
                break;

            case R.id.device_1_del:
                device_1_img.setImageResource(android.R.drawable.ic_input_add);
                device_1_del.setVisibility(View.GONE);
                deleteImgFile("device_1_img");
                break;
            case R.id.device_2_del:
                device_2_img.setImageResource(android.R.drawable.ic_input_add);
                device_2_del.setVisibility(View.GONE);
                deleteImgFile("device_2_img");
                break;
            case R.id.device_3_del:
                device_3_img.setImageResource(android.R.drawable.ic_input_add);
                device_3_del.setVisibility(View.GONE);
                deleteImgFile("device_3_img");
                break;
            case R.id.device_4_del:
                device_4_img.setImageResource(android.R.drawable.ic_input_add);
                device_4_del.setVisibility(View.GONE);
                deleteImgFile("device_4_img");
                break;
        }

    }

    private void startPickImage(int requestCode) {
        mIsPickCamera = false;
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, requestCode);
    }

    private void startCamera(int requestCode) {
        mIsPickCamera = true;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File picFile = new File(mPicPath);
        File dirFile = new File(mDirPath);
        try {
            if(!dirFile.exists()){
                dirFile.mkdirs();
            }
            if (picFile.exists()) {
                picFile.delete();
            }
            picFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String authorities = getPackageName() + ".fileProvider";
            uri= FileProvider.getUriForFile(this, authorities, picFile);
        } else {
            uri = Uri.fromFile(picFile);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, requestCode);
    }

    private void showPicturePicker(final int requestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("图片来源");
        builder.setNegativeButton("取消", null);
        builder.setItems(new String[] {"拍照", "相册"} , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        startCamera(requestCode);
                        break;
                    case 1:
                        startPickImage(requestCode);
                        break;
                }
            }
        });
        builder.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK)
            return;
        switch (requestCode) {
            case 1:
                device_1_img.setImageBitmap(getBitmap(data, "device_1_img"));
                device_1_del.setVisibility(View.VISIBLE);
                break;
            case 2:
                device_2_img.setImageBitmap(getBitmap(data, "device_2_img"));
                device_2_del.setVisibility(View.VISIBLE);
                break;
            case 3:
                device_3_img.setImageBitmap(getBitmap(data, "device_3_img"));
                device_3_del.setVisibility(View.VISIBLE);
                break;
            case 4:
                device_4_img.setImageBitmap(getBitmap(data, "device_4_img"));
                device_4_del.setVisibility(View.VISIBLE);
                break;
            case REQUEST_CODE_DEVICE:
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        setStationsData(bundle);
                    }
                }
                break;
            case REQUEST_CODE_SECTION:
                if (data != null) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        setSectionData(bundle);
                    }
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 保存照片
     * @param mBitmap
     * @return
     */
    public void saveBitmap(Bitmap mBitmap, String fileName) {
        if (mBitmap == null)
            return;
        String sdStatus = Environment.getExternalStorageState();
        if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
            Toast.makeText(this,"内存卡异常，请检查内存卡插入是否正确",Toast.LENGTH_SHORT).show();
            return;
        }

        File dirFile=new File(mDirPath);
        if(!dirFile.exists()){
            dirFile.mkdir();
        }
        File f = new File(mDirPath, fileName + ".jpg");
        if (f.exists()) {
            f.delete();
        }
        try {
            FileOutputStream fOut = null;
            fOut = new FileOutputStream(f);
            mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    private void deleteImgFile(String fileName) {
        File dirFile=new File(mDirPath);
        if(!dirFile.exists()){
            dirFile.mkdir();
        }
        File f = new File(mDirPath, fileName + ".jpg");
        if (f.exists()) {
            f.delete();
        }
    }


    private Bitmap getBitmap(Intent data, String fileName) {

        if (mIsPickCamera) {
            try {
                File picFile = new File(mPicPath);
                if (picFile.exists()) {
                    FileInputStream fis = new FileInputStream(picFile);
                    BitmapFactory.Options opt = new BitmapFactory.Options();
                    opt.inSampleSize = 5;
                    opt.inPreferredConfig = Bitmap.Config.RGB_565;
                    Bitmap bitmap = BitmapFactory.decodeStream(fis, null, opt);
                    saveBitmap(bitmap, fileName);
                    fis.close();
                    picFile.delete();
                    return bitmap;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if (data == null)
                return null;
            Uri uri = data.getData();
            Log.e("uri", uri.toString());
            ContentResolver cr = this.getContentResolver();
            try {
                BitmapFactory.Options options=new BitmapFactory.Options();
                options.inJustDecodeBounds = false;
                options.inSampleSize = 5;
                Bitmap bitmap =BitmapFactory.decodeStream(cr.openInputStream(uri), null, options);
                saveBitmap(bitmap, fileName);
                return bitmap;
            } catch (FileNotFoundException e) {
                Log.e("Exception", e.getMessage(),e);
            }
        }

        return null;
    }

    /**
     * 保存已选择的设备
     *
     * @param bundle
     */
    private void setStationsData(Bundle bundle) {
        ArrayList<Section> obj = bundle.getParcelableArrayList("stations");
        if (obj == null || obj.size() <= 0) {
            return;
        }
        resultObject.clear();
        resultObject.addAll(obj);
        if (resultObject != null && resultObject.size() > 0) {

            for (Section station : resultObject) {
                for (DeviceObject device : station.deviceList) {
                    if (device.isCheck) {
                        mobject = new ParString(device.id, device.getName());
                    }
                }
            }

        }

        if (mobject != null) {
            select_device_btn.setText(mobject.getName());
        }

    }

    /**
     * 保存已选择的机构
     *
     * @param bundle
     */
    private void setSectionData(Bundle bundle) {

        msection = bundle.getParcelable("section");

        if (null != msection) {
            select_device_btn.setText(msection.getName());
        }
    }

    // 界面跳转
    public static void startActivity(Context context) {
        Intent intent = new Intent(context, CreateExplorationActivity.class);
        context.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.menu_save:// 保存新增派工
                newExplorationTask(device_desc_edit.getText().toString());
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void newExplorationTask(String memo) {

        if (requestHttp == null)
            requestHttp = new RequestHttp(getWld());// 服务请求

        if (null == mobject) {
            this.showToast("请选择基站！");
            return;
        }

        if (TextUtils.isEmpty(memo)) {
            this.showToast("请添加文字描述！");
            return;
        }

        if (memo.length() > 255) {
            this.showToast("文字描述不得超过255位！");
            return;
        }


        startDialog(R.string.progressDialog_title_submit);// 弹出对话框
        // 设置参数
        RequestParams params = new RequestParams(getUser());
        params.putParams("memo", memo);
        params.putParams("object_name", mobject.getName());

        String url = String.format(Globals.EXPLORATION_NEW, mobject.getId());
        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissDialog();
                        try {
                            if (value.contains("success")) {
                                JSONObject json = new JSONObject(value);
                                eid = json.getString("id");
                                uploadImage(eid);
                            } else {
                                String error = "";

                                JSONObject json = new JSONObject(value);
                                error = json.getString("error");

                                showToast(error);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
    }

    /**
     * 上传图片
     *
     * @param eid
     */
    private void uploadImage(String eid) {
        String url = String.format(Globals.EXPLORATION_UPLOAD_IMG, eid);
        String dirPath =Environment.getExternalStorageDirectory()+"/Ekey/";
        final File dirFile=new File(dirPath);
        if (!dirFile.exists()) {
            finish();
            return;
        }

        File[] files = dirFile.listFiles();
        if (files == null || files.length == 0) {
            showToast("新增成功");
            finish();
            return;
        }
        List<String> filepaths = new ArrayList<>();
        for (final File imgFile : files) {
            filepaths.add(imgFile.getAbsolutePath());
        }
        requestHttp.uploadImage(url, filepaths, "portrait", new RequestHttp.UploadCallback() {
            @Override
            public void onUploadResult(final boolean isOk) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showToast(isOk ? "上传成功" : "上传失败");
//                        imgFile.delete();
                        if (isOk) {
                            finish();
                        }
                    }
                });

            }

            @Override
            public void onUploadResultData(JSONArray uploadResultData) {

            }
        });
    }
}
