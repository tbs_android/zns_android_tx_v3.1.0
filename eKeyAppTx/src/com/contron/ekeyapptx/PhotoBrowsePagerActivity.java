package com.contron.ekeyapptx;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager.PageTransformer;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import java.util.ArrayList;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.view.CustomViewPager;
import com.lidroid.xutils.util.LogUtils;

/**
 * 查看图片
 *
 * @author liuyinjun
 */
public class PhotoBrowsePagerActivity extends BasicActivity {
	private ArrayList<String> urlList;
	private PhotoBrowsePagerAdapter browsePagerAdapter;
	private CustomViewPager viewPager;
	private BasicActivity mActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_browse_pager);
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		mActivity = this;
		urlList = bundle.getStringArrayList("urlList");
		int item = bundle.getInt("item");
		String title = bundle.getString("title");

		mActivity.getBasicActionBar().setTitle(title);

		LogUtils.e(urlList.size() + "=" + item);

		viewPager = findViewById(R.id.photo_browse_pager);
		browsePagerAdapter = new PhotoBrowsePagerAdapter(
				getSupportFragmentManager(), urlList);
		viewPager.setAdapter(browsePagerAdapter);
		viewPager.setCurrentItem(item);
		viewPager.setPageTransformer(true, new DepthPageTransformer());
	}

	public class DepthPageTransformer implements PageTransformer {
		private static final float MIN_SCALE = 0.75f;

		public void transformPage(View view, float position) {
			int pageWidth = view.getWidth();

			if (position < -1) { // [-Infinity,-1)
				// This page is way off-screen to the left.
				view.setAlpha(0);

			} else if (position <= 0) { // [-1,0]
				// Use the default slide transition when moving to the left page
				view.setAlpha(1);
				view.setTranslationX(0);
				view.setScaleX(1);
				view.setScaleY(1);

			} else if (position <= 1) { // (0,1]
				// Fade the page out.
				view.setAlpha(1 - position);

				// Counteract the default slide transition
				view.setTranslationX(pageWidth * -position);

				// Scale the page down (between MIN_SCALE and 1)
				float scaleFactor = MIN_SCALE + (1 - MIN_SCALE)
						* (1 - Math.abs(position));
				view.setScaleX(scaleFactor);
				view.setScaleY(scaleFactor);

			} else { // (1,+Infinity]
				// This page is way off-screen to the right.
				view.setAlpha(0);
			}
		}
	}

	/**
	 * FragmentStatePagerAdapter 离开Item即销毁, 减少内在占用
	 *
	 * @author hupei
	 * @date 2014-5-4 下午5:07:51
	 */
	class PhotoBrowsePagerAdapter extends FragmentStatePagerAdapter {
		private ArrayList<String> urlList;

		public PhotoBrowsePagerAdapter(FragmentManager fm,
				ArrayList<String> urlList) {
			super(fm);
			this.urlList = urlList;
		}

		public void addSrc(String imgUrl) {
			this.urlList.add(imgUrl);
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public Fragment getItem(int arg0) {
			return new PhotoBrowseFragment(urlList.get(arg0), mActivity, viewPager);
		}

		@Override
		public int getCount() {
			return urlList.size();
		}

		public String getPictureUrl(int arg0) {
			return urlList.get(arg0);
		}

	}

	@SuppressLint("ValidFragment")
	public static class PhotoBrowseFragment extends Fragment implements
			OnTouchListener {
		private static final int NONE = 0;// 初始状态
		private static final int DRAG = 1;// 拖动
		private static final int ZOOM = 2;// 缩放
		private Matrix matrix = new Matrix();
		private Matrix savedMatrix = new Matrix();
		private PointF start = new PointF();
		private PointF mid = new PointF();
		private DisplayMetrics displayMetrics;
		private int mode = NONE;
		private float oldDist = 1f;
		private float minScaleR;// 最小缩放比例
		private static final float MAX_SCALE = 4f;// 最大缩放比例
		private long startMillis = 0;
		private Bitmap bitmap;
		private String pictrueUrl;
		private ViewSwitcher viewSwitcher;
		private TextView tvDialogProgress;
		private ImageView imageView;
		private View view;
		private RequestHttp requestHttp;
		private BasicActivity mActivity;
		private CustomViewPager viewPager;

		public PhotoBrowseFragment(String pictrueUrl, BasicActivity activity, CustomViewPager viewPager) {
			this.pictrueUrl = pictrueUrl;
			mActivity = activity;
			this.viewPager = viewPager;
			requestHttp = new RequestHttp(mActivity.getWld());
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {

			view = inflater.inflate(R.layout.fragemnt_photo_browse, container,
					false);
			viewSwitcher = view
					.findViewById(R.id.photo_browse_viewSwitcher);
			imageView = view
					.findViewById(R.id.photo_browse_imageView);
			ProgressBar progressBar = view
					.findViewById(R.id.dialog_weblayout_upload_photo_ProgressBar);
			progressBar.setIndeterminateDrawable(getResources().getDrawable(
					R.drawable.progressbar_yellow));
			tvDialogProgress = view
					.findViewById(R.id.dialog_weblayout_upload_photo_tvLoad);
            displayMetrics = getResources().getDisplayMetrics();
//			displayMetrics = new DisplayMetrics();
//			getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);// 获取分辨率
			imageView.setOnTouchListener(this);// 设置触屏监听
			return view;
		}

		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			Glide.with(this).load(pictrueUrl).asBitmap().into(loadImgtarget);//方法中设置asBitmap可以设置回调类型
			// 获取头像
//			requestHttp.downloadImg(pictrueUrl, displayMetrics.widthPixels, displayMetrics.heightPixels, new BitmapCallback() {
//
//				@Override
//				public void resultValueBitmap(final Bitmap bitmap) {
//
//					mActivity.runOnUiThread(new Runnable() {
//						@Override
//						public void run() {
//							if (bitmap != null) {
//								initBitmap(bitmap);
//								isShowNext(viewSwitcher, 0);
//								imageView.setImageBitmap(bitmap);
//							} else {
//
//							}
//						}
//					});
//
//				}
//			});

		}

		private SimpleTarget loadImgtarget = new SimpleTarget<Bitmap>() {
			@Override
			public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
				// do something with the bitmap
				// for demonstration purposes, let's just set it to an ImageView
				initBitmap(bitmap);
				isShowNext(viewSwitcher, 0);
				imageView.setImageBitmap( bitmap );
			}
		};

		/**
		 * 是否显示子控件
		 * 
		 * @param viewSwitcher
		 *            控件
		 * @param childIndex
		 *            viewSwitcher 控件里面子控件的索引
		 * @date 2014-8-1 上午11:36:58
		 */
		private void isShowNext(ViewSwitcher viewSwitcher, int childIndex) {
			if (viewSwitcher == null)
				return;

			if (viewSwitcher.getDisplayedChild() == childIndex)
				viewSwitcher.showNext();
		}

		private void initBitmap(Bitmap bmap) {
			bitmap = bmap;
			minZoom();// 计算最小缩放比
			checkView();// 设置图像居中
			imageView.setImageMatrix(matrix);
		}

		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			ImageView imgView = (ImageView) v;
			switch (event.getAction() & MotionEvent.ACTION_MASK) {
			// 设置拖拉模式(主点)
			case MotionEvent.ACTION_DOWN:
				startMillis = System.currentTimeMillis();// 记住点下的时间, 抬起时计算
				savedMatrix.set(matrix);
				start.set(event.getX(), event.getY());
				mode = DRAG;
				break;
			// 设置多点触摸模式(副点)
			case MotionEvent.ACTION_POINTER_DOWN:
				oldDist = spacing(event);
				if (oldDist > 10f) {
					savedMatrix.set(matrix);
					midPoint(mid, event);
					mode = ZOOM;
				}
				break;
			case MotionEvent.ACTION_UP:
				long exit = System.currentTimeMillis() - startMillis;
				if (exit < 100) {// 抬起的时间小于100则销毁界面
					getActivity().finish();
				}
				break;
			case MotionEvent.ACTION_POINTER_UP:
				mode = NONE;
				break;
			// 若为DRAG模式，则点击移动图片
			case MotionEvent.ACTION_MOVE:
				if (mode == DRAG) {
					matrix.set(savedMatrix);
					// 设置位移
					matrix.postTranslate(event.getX() - start.x, event.getY()
							- start.y);
				}
				// 若为ZOOM模式，则多点触摸缩放
				else if (mode == ZOOM) {
					float newDist = spacing(event);
					if (newDist > 10f) {
						matrix.set(savedMatrix);
						float scale = newDist / oldDist;
						// 设置缩放比例和图片中点位置
						matrix.postScale(scale, scale, mid.x, mid.y);
					}
				}
				break;
			}
			imgView.setImageMatrix(matrix);
			checkView();
			return true;
		}

		/**
		 * 最小缩放比例，最大为100%
		 */
		private void minZoom() {
			// if (bitmap == null) {
			// return;
			// }
			if (bitmap.getWidth() >= displayMetrics.widthPixels)
				minScaleR = ((float) displayMetrics.widthPixels)
						/ bitmap.getWidth();
			else
				minScaleR = 1.0f;

			if (minScaleR < 1.0) {
				matrix.postScale(minScaleR, minScaleR);
			}
		}

		/**
		 * 限制最大最小缩放比例，自动居中
		 */
		private void checkView() {
			float p[] = new float[9];
			matrix.getValues(p);
			if (mode == ZOOM) {
				if (p[0] < minScaleR) {
					// 缩放到最小比例时, 启用viewPager滑动
					viewPager.setPagerTouchEnabled(true);
					matrix.setScale(minScaleR, minScaleR);
				} else if (p[0] > MAX_SCALE) {
					matrix.set(savedMatrix);
				} else {
					// 只要有放大, 禁用viewPager滑动
					viewPager.setPagerTouchEnabled(false);
				}
			}
			center();
		}

		private void center() {
			center(true, true);
		}

		/**
		 * 横向、纵向居中
		 */
		private void center(boolean horizontal, boolean vertical) {
			if (bitmap == null) {
				return;
			}
			Matrix m = new Matrix();
			m.set(matrix);
			RectF rect = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
			m.mapRect(rect);

			float height = rect.height();
			float width = rect.width();
			float deltaX = 0, deltaY = 0;

			if (vertical) {
				// 图片小于屏幕大小，则居中显示。大于屏幕，上方留空则往上移，下方留空则往下移
				int screenHeight = displayMetrics.heightPixels;
				if (height < screenHeight) {
					deltaY = (screenHeight - height) / 2 - rect.top;
				} else if (rect.top > 0) {
					deltaY = -rect.top;
				} else if (rect.bottom < screenHeight) {
					deltaY = imageView.getHeight() - rect.bottom;
				}
			}

			if (horizontal) {
				int screenWidth = displayMetrics.widthPixels;
				if (width < screenWidth) {
					deltaX = (screenWidth - width) / 2 - rect.left;
				} else if (rect.left > 0) {
					deltaX = -rect.left;
					// 放大后, 滑动左边界即启动viewPager滑动事件
					viewPager.setPagerTouchEnabled(true);
				} else if (rect.right < screenWidth) {
					deltaX = screenWidth - rect.right;
					// 放大后, 滑动右边界即启动viewPager滑动事件
					viewPager.setPagerTouchEnabled(true);
				}
			}
			matrix.postTranslate(deltaX, deltaY);
		}

		/**
		 * 计算移动距离
		 */
		private float spacing(MotionEvent event) {
			float x = event.getX(0) - event.getX(1);
			float y = event.getY(0) - event.getY(1);
			float temp = (float) Math.sqrt(x * x + y * y);
			return temp;
		}

		/**
		 * 计算中点位置
		 */
		private void midPoint(PointF point, MotionEvent event) {
			float x = event.getX(0) + event.getX(1);
			float y = event.getY(0) + event.getY(1);
			point.set(x / 2, y / 2);
		}
	}

	public static void gotoActivity(Context context, ArrayList<String> urlList,
			int item, String title) {
		Intent intent = new Intent(context, PhotoBrowsePagerActivity.class);
		Bundle bundle = new Bundle();
		bundle.putStringArrayList("urlList", urlList);
		bundle.putInt("item", item);
		bundle.putString("title", title);
		intent.putExtras(bundle);
		context.startActivity(intent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		switch (id) {
		case android.R.id.home:
			mActivity.finish();
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}
}
