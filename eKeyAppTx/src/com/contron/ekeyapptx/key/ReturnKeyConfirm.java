package com.contron.ekeyapptx.key;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.SmartkeyBorrow;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 钥匙归还确认
 * 
 * @author luoyilong
 *
 */
public class ReturnKeyConfirm extends SwipeRefreshListFragment {
	public static ReturnKeyConfirm newInstance() {
		ReturnKeyConfirm fragment = new ReturnKeyConfirm();
		return fragment;
	}

	private BasicActivity mActivity;
	private ContronAdapter<SmartkeyBorrow> mAdapter; //适配器
	private List<SmartkeyBorrow> smartkey = new ArrayList<SmartkeyBorrow>();
	private RequestHttp requestHttp;//请求服务
	private View loadMoreView;// 下一页视图
	private RadioButton loadMoreButton;// 下一页按钮
	private int mcurrentItem = 0;// 当前页数
	private int mItemCount = 1;// 总页数

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
	}

	@Override
	public void onListRefresh() {
		super.onListRefresh();
		downloadLendKeyInfo(0);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item14_name);

		mAdapter = new ContronAdapter<SmartkeyBorrow>(mActivity,
				R.layout.fragment_return_key_confirm, smartkey) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					final SmartkeyBorrow item, final int position) {
				TextView tv = viewHolder.findView(android.R.id.text2);

				tv.setText("钥匙名称：" + item.getSname() + "\n借  用  人："
						+ item.getCreate_by() + "\n借用时间：" +  item.getCreate_at());
				tv.append("\n归还登记人：" + item.getBorrow_by() + "\n归还时间："
						+  item.getReturn_at());
			}
		};
		loadMore();
		getListView().addFooterView(loadMoreView); // 设置列表底部视图
		setListAdapter(mAdapter);
		super.onActivityCreated(savedInstanceState);
	}

	/**
	 * 加载下一页
	 */
	private void loadMore() {

		LayoutInflater inflater = LayoutInflater.from(mActivity);

		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);

		// 下一页按�?
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadLendKeyInfo(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});

	}

	/**
	 * 获取已归还钥匙记录
	 */
	private void downloadLendKeyInfo(int currentItem) {
		setRefreshing(false);
		mActivity.startDialog(R.string.progressDialog_title_load);

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());

		if (currentItem == 0) {// 当前页数为0时清空数据
			smartkey.clear();
			mAdapter.notifyDataSetChanged();
		}

		// 参数设置
		RequestParams prams = new RequestParams(mActivity.getUser());
		prams.putParams("page", currentItem);
		prams.putParams("status", "已归还");

		// 请求
		requestHttp.doPost(Globals.SMARTKEY_BORROW, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {

						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();

								if (value.contains("success")) {// 返回成功
									List<SmartkeyBorrow> smartkey1 = null;
									try {
										JSONObject json = new JSONObject(value);
										JSONArray array = json
												.getJSONArray("items");

										smartkey1 = new Gson().fromJson(
												array.toString(),
												new TypeToken<List<SmartkeyBorrow>>() {
												}.getType());

										// 获取总页数
										if (mcurrentItem == 0) {
											try {
												mItemCount = json
														.getInt("pages");
											} catch (JSONException e) {
												e.printStackTrace();
											}
										}

									} catch (JSONException e) {
										e.printStackTrace();
									}

									if (mItemCount < 2 && mcurrentItem == 0) {
										getListView().removeFooterView(
												loadMoreView);
										// 删除列表底部视图
									}

									if (smartkey1 != null
											&& smartkey1.size() > 0) {
										smartkey.addAll(smartkey1);
										mAdapter.notifyDataSetChanged();
									} else {
										setEmptyText("暂无数据！");
									}

								} else {// 返回失败
									setEmptyText("暂无数据！");
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}

							}
						});
					}
				});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			mActivity.finish();
		return super.onOptionsItemSelected(item);
	}

}
