package com.contron.ekeyapptx.key;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtSearchManage;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.SmartkeyBorrow;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType.EvenNfcLyAddress;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.nfc.NFC;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.util.LogUtils;

/**
 * 钥匙归还登记
 * 
 * @author luoyilong
 *
 */
public class ReturnKeyRegist extends SwipeRefreshListFragment {
	public BasicActivity mActivity;
	private ContronAdapter<SmartkeyBorrow> mAdapter; // 适配器
	private List<SmartkeyBorrow> smartkeylist = new ArrayList<SmartkeyBorrow>();;
	private NFC mNFC; // nfc类
	private RequestHttp requestHttp;// 后台请求服务
	private SmartkeyBorrow smartkey = null;
	protected BtMsgService mBtMsgService;// 蓝牙服务
	protected BtSearchManage mBtSearchManage;// 蓝牙管理工厂
	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
			if (mBtMsgService.initialize()) {
                mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
				LogUtils.i("初始化蓝牙成功");
				mBtSearchManage = new BtSearchManage(mActivity,
						mBtMsgService.getBluetoothAdapter(),
						new BtSearchManage.OnSelectBtKeyListener() {

							@Override
							public void onSelectBtKey(Smartkey btKey) {
								// TODO Auto-generated method stub
								mBtSearchManage.scanLeDevice(false);
								confirmDialog(btKey.getBtaddr());
							}

						});
			} else {
				LogUtils.e("Unable to initialize Bluetooth");
				mActivity.showToast("初始化蓝牙失败");
				mActivity.finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBtMsgService = null;
		}
	};

	// 返回当前对象
	public static ReturnKeyRegist newInstance() {
		ReturnKeyRegist fragment = new ReturnKeyRegist();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
		mNFC = new NFC(mActivity);
	}

	@Override
	public void onListRefresh() {
		// TODO Auto-generated method stub
		super.onListRefresh();
		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());
		downloadLendKeyInfo();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item13_name);

		// 注册EventBusManager事件
		EventBusManager.getInstance().register(this);

		mAdapter = new ContronAdapter<SmartkeyBorrow>(mActivity,
				R.layout.fragment_return_register, smartkeylist) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					final SmartkeyBorrow item, final int position) {
				TextView tv = viewHolder.findView(android.R.id.text2);

				tv.setText("钥匙名称：" + item.getSname() + "\n借  用  人："
						+ item.getCreate_by() + "\n借用时间：" +  item.getCreate_at());
			}
		};
		setListAdapter(mAdapter);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		Intent gattServiceIntent = new Intent(mActivity, BtMsgService.class);
		mActivity.bindService(gattServiceIntent, mServiceConnection,
				Activity.BIND_AUTO_CREATE);

	}

	@Override
	public void onPause() {
		super.onPause();
		LogUtils.i("onPause");
		if (mBtMsgService != null) {
			if (mBtMsgService.isConnected())// 判断蓝牙连接状态
				mBtMsgService.disconnect();
			else
				mBtMsgService.removeCallbacksConn();
		}
		mActivity.unbindService(mServiceConnection);

		mNFC.onColse();// 关闭NFC功能
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		// 注消EventBusManager事件
		EventBusManager.getInstance().unregister(this);
	}

	/**
	 * 更新归还状态
	 * 
	 * @param btKey
	 */
	private void uploadLendKeyInfo() {

		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话框

		// 参数设置
		RequestParams prams = new RequestParams(mActivity.getUser());

		// 请求Url
		String url = String.format(Globals.SMARTKEY_RETURN_KID,
				smartkey.getSid());

		// 请求
		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {
			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mActivity.dismissDialog();
						if (value.contains("success")) {// 返回成功
							mActivity.showToast("归还登记成功！");
							smartkeylist.remove(smartkey);
							mAdapter.notifyDataSetChanged();
							mActivity.invalidateOptionsMenu();
						} else {// 返回失败
							String error = "";
							try {
								JSONObject json = new JSONObject(value);
								error = json.getString("error");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							mActivity.showToast(error);

						}

					}
				});
			}
		});

	}

	/**
	 * 获取未归还的钥匙借出记录
	 */
	private void downloadLendKeyInfo() {
		setRefreshing(false);
		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		// 清空数据
		smartkeylist.clear();
		mAdapter.notifyDataSetChanged();

		// 参数设置
		RequestParams prams = new RequestParams(mActivity.getUser());
		prams.putParams("page", -1);
		prams.putParams("status", "未归还");

		// 请求
		requestHttp.doPost(Globals.SMARTKEY_BORROW, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {

						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {// 返回成功
									List<SmartkeyBorrow> smartkey1 = null;

									try {
										JSONObject json = new JSONObject(value);
										JSONArray array = json
												.getJSONArray("items");

										smartkey1 = new Gson().fromJson(
												array.toString(),
												new TypeToken<List<SmartkeyBorrow>>() {
												}.getType());
									} catch (JSONException e) {
										e.printStackTrace();
									}

									if (smartkey1 != null
											&& smartkey1.size() > 0) {
										smartkeylist.addAll(smartkey1);
										mAdapter.notifyDataSetChanged();
										mActivity.invalidateOptionsMenu();
									} else {
										setEmptyText("暂无数据！");
									}

								} else {// 返回失败
									setEmptyText("暂无数据！");
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}

							}
						});
					}
				});
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		if (smartkeylist != null && smartkeylist.size() > 0) {
			mNFC.onStart(0);
			inflater.inflate(R.menu.selectkey, menu);
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		if (item.getItemId() == android.R.id.home) {
			mActivity.finish();
		} else if (item.getItemId() == R.id.menu_selectkey) {
			if (mBtSearchManage != null)
				mBtSearchManage.scanLeDevice(true);
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 接收TicketActivity发送过来的nfc蓝牙地址信息
	 * 
	 */
	public void onEventMainThread(EvenNfcLyAddress lyaddre) {
		String address = lyaddre.address;
		if (address.length() > 14)
			getSmartkey(address.substring(2, 14).toUpperCase());
	}

	/**
	 * 判断当前蓝牙地址是否在数据库中存在
	 *
	 * @param address
	 */
	private void getSmartkey(String address) {
		Smartkey smartkey = DBHelp.getInstance(mActivity).getEkeyDao()
				.getSmartkey(address);
		if (smartkey != null) {// 己注册钥匙
			confirmDialog(address);
		} else {// 未注册钥匙
			mActivity.showMsgBox(R.string.borrowkey_noregistkey);
		}
	}

	/**
	 * 钥匙归还确认
	 */
	private void confirmDialog(String address) {

		for (SmartkeyBorrow lend : smartkeylist) {
			if (address.equals(lend.getBtaddr())) {
				smartkey = lend;
				break;
			}
		}

		if (smartkey != null) {
			String msg = "借用钥匙：" + smartkey.getSname() + "\n借  用  人："
					+ smartkey.getCreate_by() + "\n借用时间："
					+ smartkey.getCreate_at();
			mActivity.showMsgBox("钥匙归还登记", msg, false, new OnClickListener() {
				// 确认
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					uploadLendKeyInfo();
				}
			}, new OnClickListener() {
				// 取消
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		} else {
			mActivity.showMsgBox(R.string.lendkey_msg, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
		}

	}

}
