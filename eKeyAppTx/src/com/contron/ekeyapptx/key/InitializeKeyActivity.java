package com.contron.ekeyapptx.key;

import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtSearchManage;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.bluetooth.BtTimeOut;
import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtErrorCode;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvAckMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.bluetooth.code.send.BtSendInitialKeyMsg;
import com.contron.ekeypublic.bluetooth.sc.SCReceiveBaseResult;
import com.contron.ekeypublic.bluetooth.sc.SCReceiveMsg;
import com.contron.ekeypublic.bluetooth.sc.SCSendMsg;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType.BtHex;
import com.contron.ekeypublic.http.KeyDataPrepare;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;

public class InitializeKeyActivity extends BasicActivity {

	@ViewInject(R.id.chtx_keyconfig)
	private CheckedTextView mChboxKeyconfig;// 钥匙配置

	@ViewInject(R.id.chtx_userinfo)
	private CheckedTextView mChboxUserinfo;// 用户信息

	@ViewInject(R.id.chtx_lockname)
	private CheckedTextView mChboxLockName;// 锁名称

	@ViewInject(R.id.chtx_lockinfo)
	private CheckedTextView mChboxLockinfo;// 锁信息

	@ViewInject(R.id.chtx_logic)
	private CheckedTextView mChboxLogic;// 锁逻辑

	private BtMsgService mBtMsgService;// 蓝牙服务
	protected BtSearchManage mBtSearchManage;// 蓝牙管理工厂
	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求服务
	private boolean dataPrepareState = false;// 数据准备状态 true准备就绪
	private BtSendInitialKeyMsg initialKeyMsg;// 钥匙配置报文
	private String btKeyName = "";
	private String btKeyadd = "";
	private boolean updataState = false;// 钥匙数据更新状态
	private ContronAdapter<String> apdater;
	private String error = "";
	private boolean isSCLock;

	// 解析保存用户、锁信息集合数据
	private KeyDataPrepare keyData = new KeyDataPrepare();

	// 保存选择打包项
	private SparseArray<String> selectInitialize = new SparseArray<String>();

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,
				IBinder service) {
			mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
			if (mBtMsgService.initialize()) {
				LogUtils.i("蓝牙服务绑定成功");
				mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
				mBtSearchManage = new BtSearchManage(mActivity,
						mBtMsgService.getBluetoothAdapter(),
						new BtSearchManage.OnSelectBtKeyListener() {
							@Override
							public void onSelectBtKey(Smartkey btKey) {
								// TODO 选择蓝牙
								scanLeDevice(false);
								mActivity
										.startDialog(R.string.main_bluetooth_msg0);
								btKeyName = btKey.getName();
								String bttmp = btKey.getBtaddr().replaceAll(
										"(.{2})", "$1:");
								btKeyadd = bttmp.substring(0,
										bttmp.length() - 1);

								EkeyAPP.getInstance().setBtAddress(btKeyadd);
								connBt();
							}
						});

			} else {
				LogUtils.e("Unable to initialize Bluetooth");
				showToast("绑定蓝牙服务失败");
				finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBtMsgService = null;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.isSCLock = EkeyAPP.getInstance().isSCLock();
		setContentView(R.layout.activity_initialize_key);
		LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout1);
		linearLayout.setVisibility(isSCLock ? View.INVISIBLE : View.VISIBLE);
		getBasicActionBar().setTitle(R.string.main_tab_item21_name);
		mActivity = this;
		ViewUtils.inject(this);
		EkeyAPP.getInstance().addActivity(this);
		EventBusManager.getInstance().register(this);
		Intent gattServiceIntent = new Intent(this, BtMsgService.class);
		bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
	}

	/**
	 * 打包钥匙按钮
	 * 
	 * @param v
	 * @throws DbException
	 */
	@OnClick({ R.id.but_initalizekey, R.id.chtx_keyconfig, R.id.chtx_lockinfo,
			R.id.chtx_lockname, R.id.chtx_userinfo, R.id.chtx_logic })
	public void onClick(View v) throws DbException {
		switch (v.getId()) {
		case R.id.but_initalizekey:
			checkLockType();
			break;
		case R.id.chtx_keyconfig:
//			togglChboxTextStatus(mChboxKeyconfig);
			break;
		case R.id.chtx_lockinfo:
			togglChboxTextStatus(mChboxLockinfo);
			break;
		case R.id.chtx_lockname:
			togglChboxTextStatus(mChboxLockName);
			break;
		case R.id.chtx_userinfo:
			togglChboxTextStatus(mChboxUserinfo);
			break;
		case R.id.chtx_logic:
			togglChboxTextStatus(mChboxLogic);
			break;
		default:
			break;
		}
	}

	/**
	 * 切换CheckedTextView状态
	 * 
	 * @param view
	 */
	private void togglChboxTextStatus(CheckedTextView view) {

		if (view.isChecked()) {
			view.setChecked(false);
		} else {
			view.setChecked(true);
		}

	}

	/**
	 * 启动、停止\搜索
	 * 
	 * @param enable
	 */
	private void scanLeDevice(boolean enable) {

		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
			if (!mBtMsgService.isConnected())
				mBtMsgService.removeCallbacksConn();
		}

		if (mBtSearchManage != null)
			mBtSearchManage.scanLeDevice(enable);
	}

	/**
	 * 连接蓝牙
	 */
	protected void connBt() {
		LogUtils.e("连接开始");

		String btAddress = EkeyAPP.getInstance().getBtAddress();
		if (mBtMsgService != null && !TextUtils.isEmpty(btAddress)) {
			if (mBtMsgService.isConnected())// 如果已经连接则断开
				mBtMsgService.disconnect();

			mBtMsgService.autoConnect(btAddress);
		} else {
			if (mBtSearchManage != null)
				mBtSearchManage.scanLeDevice(true);
		}
	}

	/**
	 * 断开蓝牙连接
	 */
	private void disconnect() {
		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		scanLeDevice(false);
		// 注销EventBusManager事件
		EventBusManager.getInstance().unregister(this);
		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
			if (!mBtMsgService.isConnected())
				mBtMsgService.removeCallbacksConn();
		}
		// 解除蓝牙服务邦定
		mActivity.unbindService(mServiceConnection);
		EkeyAPP.getInstance().removeActivity(this);
	}

	/**
	 * 接收BtMsgService发出的状态消息<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtState state) {
		switch (state) {
		case CONNECTING:// 连接中
			break;
		case DISCONNECTED:// 连接断开
			break;
		case CONNECTED:// 连接成功
			break;
		case CONNECTOUTTIME:// 连接超时
			mActivity.dismissDialog();
			showMsgBox("蓝牙连接超时,请重试!");
			break;
		case DISCOVERED:// 发现设备，可发送蓝牙命令了
			updataState = false;
			mActivity.dismissDialog();

			if (isSCLock) {
				mActivity.startDialog("正在初始化...请稍候！");
				if (mBtMsgService != null) {
					mBtMsgService.writeValue(SCSendMsg.getInstance().superClearFrame());
				}
			}
			else {
				mActivity.startDialog("钥匙数据更新中...请稍候！");
				if (mBtMsgService != null && selectInitialize.size() > 0) {
					// 发送时间
					mBtMsgService.writeValue(initialKeyMsg.createDateFrameMsg());
				}
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 接收mBtTimeOut超时连接<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author luoyilong
	 * @date 2016年6月24日 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtTimeOut mBtTimeOut) {
		mActivity.dismissDialog();
		showMsgBox(error);
		disconnect();
		LogUtils.i("mBtTimeOut");
	}

	/**
	 * 接收BtMsgService发出的蓝牙规约数据<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author luoyilong
	 * @date 2016年6月20日 上午10:25:18
	 * @param btHex
	 */
	public void onEventMainThread(BtHex btHex) {
		final String read = btHex.commd;
		if (!TextUtils.isEmpty(read)) {

			if (isSCLock) {
				byte[] extraData = BtMsgConver.hexStrToByte(read);
				SCReceiveMsg receiveMsg = new SCReceiveMsg(extraData);
				if (receiveMsg.getRecvType() == 0x01) {
					SCReceiveBaseResult receiveBaseResult = receiveMsg.parseLockInfoMsg();
					if (receiveBaseResult.ismState()) {
						mBtMsgService.writeValue(SCSendMsg.getInstance().clearFrame());
					}
				}
				if (receiveMsg.getRecvType() == 0x08) {
					SCReceiveBaseResult receiveBaseResult = receiveMsg.parseClearMsg();
					if (receiveBaseResult.ismState()) {
						mBtMsgService.writeValue(SCSendMsg.getInstance().initLockerFrame());
					}
				}
				if (receiveMsg.getRecvType() == 0x09) {
					SCReceiveBaseResult receiveBaseResult = receiveMsg.parseSuperClearMsg();
					if (receiveBaseResult.ismState()) {
						mBtMsgService.writeValue(SCSendMsg.getInstance().initLockerFrame());
					}
				}
				if (receiveMsg.getRecvType() == 0x02) {
					SCReceiveBaseResult receiveBaseResult = receiveMsg.parseInitMsg();
					if (receiveBaseResult.ismState()) {
						mActivity.showMsgBox("初始化成功");
					}
					else {
						mActivity.showMsgBox("初始化失败");
					}
					mActivity.dismissDialog();
					disconnect();
				}
				return;
			}
			BtRecvMsg btRecvMsg = BtMsgTools.recv(read);
			if (btRecvMsg != null) {

				if (BtCommand.CODE_01.value.equals(btRecvMsg.command)) {// 接时间返回信息

					BtRecvAckMsg btRecvAckMsg = (BtRecvAckMsg) btRecvMsg;
					btRecvAckMsg.createDataSource();

					// 响应成功
					if (btRecvAckMsg.btErrorCode == BtErrorCode.SUCCESS) {

						if (BtFunctionCode.CODE_00.value
								.equals(btRecvMsg.functionCode)) {// 校时 完成返回ACK码

							if (selectInitialize.keyAt(0) == 0) {// 如果第一个是钥匙配置信息
								error = "打包钥匙配置信息失败！";
								mBtMsgService
										.writeValue(initialKeyMsg
												.createFenFrameMsg(btKeyName,
														btKeyadd));
							} else {
								String function = selectInitialize.valueAt(0);
								setError(function);
								mBtMsgService.writeValue(initialKeyMsg
										.createTotalFrameMsg(selectInitialize
												.valueAt(0)));
							}

						}
					} else {// 校时失败
						mActivity.dismissDialog();
						showMsgBox("初始化钥匙-校时失败,请重试！"
								+ btRecvAckMsg.btErrorCode.value);
						disconnect();
					}

				} else if (BtCommand.CODE_02.value.equals(btRecvMsg.command)) {// 接打包钥匙返回数据

					BtRecvAckMsg btRecvAckMsg = (BtRecvAckMsg) btRecvMsg;
					btRecvAckMsg.createDataSource();

					// 响应成功
					if (btRecvAckMsg.btErrorCode == BtErrorCode.SUCCESS) {

						if (BtFunctionCode.CODE_00.value
								.equals(btRecvMsg.functionCode)) {// 打包配置信息 完成

							// 分帧发送完成则发送总帧
							createTotalFrameMsg(BtFunctionCode.CODE_00);

						} else if (BtFunctionCode.CODE_01.value
								.equals(btRecvMsg.functionCode)) {// 打包锁ID 总帧数

							// 发送分帧
							createFenFrameMsg(BtFunctionCode.CODE_02);

						} else if (BtFunctionCode.CODE_02.value
								.equals(btRecvMsg.functionCode)) {// 打包锁ID 完成

							// 分帧发送完成则发送总帧
							createTotalFrameMsg(BtFunctionCode.CODE_02);

						} else if (BtFunctionCode.CODE_03.value
								.equals(btRecvMsg.functionCode)) {// 打包锁RFID
																	// 总帧数

							// 发送分帧
							createFenFrameMsg(BtFunctionCode.CODE_04);

						} else if (BtFunctionCode.CODE_04.value
								.equals(btRecvMsg.functionCode)) {// 打包锁RFID
																	// 完成

							// 分帧发送完成则发送总帧
							createTotalFrameMsg(BtFunctionCode.CODE_04);

						} else if (BtFunctionCode.CODE_05.value
								.equals(btRecvMsg.functionCode)) {// 打包锁名称 总帧数

							// 发送分帧
							createFenFrameMsg(BtFunctionCode.CODE_06);

						} else if (BtFunctionCode.CODE_06.value
								.equals(btRecvMsg.functionCode)) {// 打包锁名称 完成

							// 分帧发送完成则发送总帧
							createTotalFrameMsg(BtFunctionCode.CODE_06);

						} else if (BtFunctionCode.CODE_07.value
								.equals(btRecvMsg.functionCode)) {// 打包锁类型 总帧数

							// 发送分帧
							createFenFrameMsg(BtFunctionCode.CODE_08);

						} else if (BtFunctionCode.CODE_08.value
								.equals(btRecvMsg.functionCode)) {// 打包锁类型 完成

							// 分帧发送完成则发送总帧
							createTotalFrameMsg(BtFunctionCode.CODE_08);

						} else if (BtFunctionCode.CODE_09.value
								.equals(btRecvMsg.functionCode)) {// 打包锁站号 总帧数

							// 发送分帧
							createFenFrameMsg(BtFunctionCode.CODE_0A);

						} else if (BtFunctionCode.CODE_0A.value
								.equals(btRecvMsg.functionCode)) {// 打包锁站号 完成
							// 分帧发送完成则发送总帧
							createTotalFrameMsg(BtFunctionCode.CODE_0A);

						} else if (BtFunctionCode.CODE_0B.value
								.equals(btRecvMsg.functionCode)) {// 打包逻辑关系
																	// 总帧数
							// 发送分帧
							createFenFrameMsg(BtFunctionCode.CODE_0C);

						} else if (BtFunctionCode.CODE_0C.value
								.equals(btRecvMsg.functionCode)) {// 打包逻辑关系 完成

							// 分帧发送完成则发送总帧
							createTotalFrameMsg(BtFunctionCode.CODE_0C);

						} else if (BtFunctionCode.CODE_0D.value
								.equals(btRecvMsg.functionCode)) {// 打包用户 总帧数

							// 发送分帧
							createFenFrameMsg(BtFunctionCode.CODE_0E);

						} else if (BtFunctionCode.CODE_0E.value
								.equals(btRecvMsg.functionCode)) {// 打包用户 完成

							// 分帧发送完成则发送总帧
							createTotalFrameMsg(BtFunctionCode.CODE_0E);
						}

					} else {// 初始化钥匙失败
						mActivity.dismissDialog();
						showMsgBox("初始化钥匙失败,请重试！"
								+ btRecvAckMsg.btErrorCode.value);
						disconnect();
					}

				} else {// 数据响应不匹配
					mActivity.showMsgBox(R.string.shared_bt_error);
					mActivity.dismissDialog();
					disconnect();
				}

			} else {// 数据解析失败
				mActivity.showMsgBox(R.string.shared_bt_error1);
				mActivity.dismissDialog();
				disconnect();
			}
		} else {// 接收数据为空
			mActivity.showMsgBox(R.string.shared_bt_error2);
			mActivity.dismissDialog();
			disconnect();
		}
	}

	/**
	 * 下载设备锁信息
	 */
	private void downloadDeviceLock() {

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("page", -1);
		params.putParams("search", "");
		params.putParams("section", 0);

		// 请求接口
		String url = String.format(Globals.SECTION_DEPARTMENTID_OBJECT, 0);

		requestHttp.doPost(url, params, new HttpRequestCallBackString() {

			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (value.contains("success")) {
							// 转换成实体对象
							keyData.JsonToLock(value);

							downloadUser();// 下载用户信息

						} else {
							mActivity.dismissDialog();
							String error;
							try {
								JSONObject jsonObject = new JSONObject(value);
								error = jsonObject.getString("error");
								showMsgBox(error);

							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
					}
				});

			}
		});
	}

	/**
	 * 下载用户信息
	 */
	private void downloadUser() {

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		// 设置参数
		RequestParams prams = new RequestParams(mActivity.getUser());
		prams.putParams("page", -1);
		prams.putParams("search", "启用");

		requestHttp.doPost(Globals.All_USER, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {

						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {// 返回成功
									// 转换成实体对象
									keyData.JsonToUser(value);
									mActivity.dismissDialog();
									dataPrepareState = true;
									checkData();
								} else {// 返回失败

									String error;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										error = jsonObject.getString("error");
										showMsgBox(error);

									} catch (JSONException e) {
										e.printStackTrace();
									}
								}
							}
						});

					}
				});

	}

	/**
	 * 检查是直接选择设备还是先下载数据
	 */
	private void checkLockType() {
		if (isSCLock) {
			scanLeDevice(true);
		}
		else {
			checkData();
		}
	}

	/**
	 * 检查数据是否准备就绪
	 */
	private void checkData() {
		boolean state = setSelectInitialie();

		if (!state) {// 判断是否选择打包项目
			showMsgBox("请选择初始化钥匙项！");

		} else if (!dataPrepareState) {// 数据未准备就绪
			downloadDeviceLock();

		} else {// 数据准备就绪

			// 重新实例化
			initialKeyMsg = new BtSendInitialKeyMsg(keyData.getLockDatas(),
					keyData.getAllUser());
			// startDlog("数据准备就绪,请继续！");
			scanLeDevice(true);

		}
	}

	/**
	 * 设置选择打包项目
	 */
	private boolean setSelectInitialie() {
		boolean selectState = false;
		selectInitialize.clear();

		if (mChboxKeyconfig.isChecked()) {
			selectInitialize.append(0, BtFunctionCode.CODE_00.value);// 打包钥匙配置信息
			selectState = true;
		}

		if (mChboxUserinfo.isChecked()) {
			selectInitialize.append(13, BtFunctionCode.CODE_0D.value);// 打包用户信息
			selectState = true;
		}

		if (mChboxLockName.isChecked()) {
			selectInitialize.append(5, BtFunctionCode.CODE_05.value);// 打包锁名称
			selectState = true;
		}

		if (mChboxLogic.isChecked()) {
			selectInitialize.append(11, BtFunctionCode.CODE_0B.value);// 打包锁逻辑
			selectState = true;
		}

		if (mChboxLockinfo.isChecked()) {
			selectInitialize.append(1, BtFunctionCode.CODE_01.value);// 打包锁ID
			selectInitialize.append(3, BtFunctionCode.CODE_03.value);// 打包锁RFID
			selectInitialize.append(7, BtFunctionCode.CODE_07.value);// 打包锁类型
			// selectInitialize.append(9, BtFunctionCode.CODE_09.value);// 打包锁站号
			// 注：锁站号是合在锁ID里一起打包
			selectState = true;
		}

		return selectState;

	}

	/**
	 * 发送总帧
	 * 
	 * @param functionCode
	 */
	private void createTotalFrameMsg(BtFunctionCode functionCode) {

		if (!initialKeyMsg.isSendDone(functionCode)) {// 分帧未发送完成，接着发送分帧
			mBtMsgService.writeValue(initialKeyMsg
					.createFenFrameMsg(functionCode));
		} else {// 分帧发送完成，发送下一个总帧
			selectInitialize.removeAt(0);
			if (selectInitialize.size() > 0) {
				String function = selectInitialize.valueAt(0);
				setError(function);
				mBtMsgService.writeValue(initialKeyMsg
						.createTotalFrameMsg(function));
			} else {

				if (!updataState)// 如果己收到过返回成功则直接返回
				{
					mActivity.dismissDialog();
					updataState = true;
					showMsgBox("钥匙初始化完成！");
					disconnect();
				}

			}

		}

	}

	/**
	 * 设置错误异常
	 * 
	 * @param function
	 */
	private void setError(String function) {
		if (BtFunctionCode.CODE_00.value.equals(function)) {
			error = "打包钥匙配置信息失败！";
		} else if (BtFunctionCode.CODE_0D.value.equals(function)) {
			error = "打包用户信息失败！";
		} else if (BtFunctionCode.CODE_05.value.equals(function)) {
			error = "打包锁名称失败！";
		} else if (BtFunctionCode.CODE_0B.value.equals(function)) {
			error = "打包验电逻辑失败！";
		} else {
			error = "打包锁信息失败！";
		}
	}

	/**
	 * 发送分帧
	 * 
	 * @param functionCode
	 */
	private void createFenFrameMsg(BtFunctionCode functionCode) {

		// 发送分帧
		mBtMsgService.writeValue(initialKeyMsg.createFenFrameMsg(functionCode));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.initialize_key, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 界面跳转
	 * 
	 * @param context
	 */
	public static void gotoActivity(Context context) {
		context.startActivity(new Intent(context, InitializeKeyActivity.class));
	}

	/**
	 * 弹出提示框
	 */
	public void showMsgBox(String msg) {
		mActivity.showMsgBox("提示", msg, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
	}

}
