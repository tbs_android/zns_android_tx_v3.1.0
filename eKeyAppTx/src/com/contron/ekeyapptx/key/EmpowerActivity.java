package com.contron.ekeyapptx.key;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.bluetooth.BtConnAuto;
import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.bluetooth.BtTimeOut;
import com.contron.ekeypublic.bluetooth.code.BtErrorCode;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvAckMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvEmpowerMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.bluetooth.code.send.BtSendEmpowerMsg;
import com.contron.ekeypublic.bluetooth.sc.SCLockInfo;
import com.contron.ekeypublic.bluetooth.sc.SCReceiveBaseResult;
import com.contron.ekeypublic.bluetooth.sc.SCReceiveMsg;
import com.contron.ekeypublic.bluetooth.sc.SCSendMsg;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.OfflineHistory;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.WorkPatrol;
import com.contron.ekeypublic.entities.Workbill;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType.BtBatt;
import com.contron.ekeypublic.eventbus.EventBusType.BtHex;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.AppUtils;
import com.contron.ekeypublic.util.DateUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 智能钥匙操作界面
 * 
 * @author luoyilong
 *
 */
public class EmpowerActivity extends BasicActivity {
	@ViewInject(R.id.empower_textView1)
	private TextView tvName;
	@ViewInject(R.id.empower_listView1)
	private ListView listView;
	@ViewInject(android.R.id.empty)
	private TextView emptyView;
	// 接收界面跳转参数
	private TempTask tempTask = null;// 临时任务对象

	private Workbill mWorkbill = null;// 派工任务对象

	private WorkPatrol mWorkPatrol = null;// 巡检任务对象

	private Lockset mlockset;// 权限内锁
	private Lockset mlocksetALL;// 所有有锁具
	private ContronAdapter<Lockset> mAdapter;
	private int opStatus = 0;
	private BtMsgService mBtMsgService;// 蓝牙服务
	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台服务请求
	private User user;
	private List<DeviceObject> objects;

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,
				IBinder service) {
			mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
			if (mBtMsgService.initialize()) {
				mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
				LogUtils.i("蓝牙服务绑定成功");
				new Timer().schedule(new TimerTask() {
					@Override
					public void run() {
						connBt();
					}
				}, 1000);

			} else {
				LogUtils.e("Unable to initialize Bluetooth");
				showToast("绑定蓝牙服务失败");
				finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBtMsgService = null;
		}
	};



	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.fragment_empower);
		EkeyAPP.getInstance().addActivity(this);
		user = EkeyAPP.getInstance().getUser();
		downloadLocksetAll();

		/**绑定服务 **/
		EmpowerActivity.this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Intent gattServiceIntent = new Intent(EmpowerActivity.this, BtMsgService.class);
				bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
				EventBusManager.getInstance().register(EmpowerActivity.this);
			}
		});

		getBasicActionBar().setTitle(
				getString(R.string.upload_ticket_result_title,
						getString(R.string.btstate_conning)));
		ViewUtils.inject(this);
		mActivity = this;
		requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		Bundle state = getIntent().getExtras();
		if (state != null) {
			tempTask = (TempTask) state.get("temptask");
			mWorkbill = (Workbill) state.get("Workbill");
			mWorkPatrol = (WorkPatrol) state.get("WorkPatrol");

			if (mWorkbill != null)
				tvName.setText("任务名：派工任务");
			else if (tempTask != null)
				tvName.setText("任务名：" + tempTask.getName());
			else if (mWorkPatrol != null)
				tvName.setText("任务名：巡检任务");
		}
		mAdapter = new ContronAdapter<Lockset>(this,
				R.layout.fragment_empower_item, new ArrayList<Lockset>()) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					Lockset item, int position) {

				RelativeLayout layout = viewHolder
						.findView(R.id.relatlayout_empower);
				if (position == 0) {// 第一个item设置背景颜色
					layout.setBackgroundColor(getResources().getColor(
							R.color.bg_titlebar));
				} else {
					layout.setBackgroundColor(getResources().getColor(
							android.R.color.white));
				}

				viewHolder.setText(R.id.empower_item_textView1,
						item.getName() + "\t" + item.getRfid()).setText(
						R.id.empower_item_textView2, item.getCurrentDate());
				TextView tvStatus = viewHolder
						.findView(R.id.empower_item_textView3);

				if (item.getOpStatus() == 6) {
					tvStatus.setTextColor(Color.GREEN);
					tvStatus.setText(R.string.upload_ticket_result_item_enable);
				} else {
					tvStatus.setTextColor(Color.RED);
					tvStatus.setText(R.string.upload_ticket_result_item_disable);
				}
			}
		};
		listView.setEmptyView(emptyView);
		listView.setAdapter(mAdapter);
	}

	/**
	 * 连接蓝牙
	 * 
	 * @author hupei
	 * @date 2015�?5�?9�? 上午9:51:31
	 */
	private void connBt() {

		LogUtils.e("连接中..");
		if (mBtMsgService != null && !mBtMsgService.isConnected()
				&& !TextUtils.isEmpty(EkeyAPP.getInstance().getBtAddress()))
			mBtMsgService.autoConnect(EkeyAPP.getInstance().getBtAddress());
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		// 注销EventBusManager事件
		EventBusManager.getInstance().unregister(this);
		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
			if (!mBtMsgService.isConnected())
				mBtMsgService.removeCallbacksConn();
		}

		// 解除邦定
		unbindService(mServiceConnection);
		EkeyAPP.getInstance().removeActivity(this);
	}

	private synchronized void getAuthorize(String lockerRFID) {

		if (user == null) {
			showToast("读用户信息失败");
			return;
		}

		// 在线模式无网络
		if (user.isOnline && !AppUtils.isNetworkAvailable(mActivity)) {
			showToast("当前无网络禁止开锁,请连接网络后再试!");
			return;
		}

		if (tempTask != null) {// 临时鉴权开锁
			// 任务结束时间
//			String temptaskEndDate = DateUtil.getStringByFormat(
//					tempTask.getEndAt(), "yyyyMMddHHmmss",
//					"yyyy-MM-dd HH:mm:ss");
			String temptaskEndDate = tempTask.getEndAt();
			int minutes = DateUtil.getMinutes(temptaskEndDate);
			if (minutes > 0) // 未过期
				mlockset = findLocket(lockerRFID);
			else// 过期
			{
				showMsgBox(R.string.temporary_ticket_overdue,
						new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								mActivity.finish();// 关闭当前窗口
							}
						});
				return;
			}
		} else if (mWorkbill != null) {// 派工
			mlockset = findLocket(lockerRFID);
		} else if (mWorkPatrol != null) {// 巡检
			mlockset = findLocket(lockerRFID);
		} else {// 智能钥匙开锁
			if (!user.isOnline) { //离线情况下取本地数据
				mlockset = DBHelp.getInstance(this).getEkeyDao()
						.findFirstLocksetRfid(lockerRFID);
			} else {
				mlockset = findLocket(lockerRFID);
			}
		}

		if (mlockset != null) {// 当前用户权限范围内锁
			opStatus = 6;
			mlockset.setOpStatus(6);
			mlockset.setCurrentDate(DateUtil.getCurrentDate());
			mAdapter.addData(mlockset, 0);
		} else {// 当前用户权限范围外锁
			opStatus = 255;
			mlocksetALL = DBHelp.getInstance(this).getEkeyDao()
					.findFirstLocksetAllRfid(lockerRFID);
			if (mlocksetALL != null) {// 越权锁码
				mlocksetALL.setOpStatus(255);
				mlocksetALL.setCurrentDate(DateUtil.getCurrentDate());
				mAdapter.addData(mlocksetALL, 0);
			} else {// 未知锁码
				Lockset lockset = new Lockset();
				lockset.setRfid(lockerRFID);
				lockset.setName("未知锁码");
				lockset.setOpStatus(255);
				mAdapter.addData(lockset, 0);
			}
		}
		if (EkeyAPP.getInstance().isSCLock()) {
			SCSendMsg.getInstance().setMUserId(EkeyAPP.getInstance().getUser());
			mBtMsgService.writeValue(SCSendMsg.getInstance().openDoorFrame(2));
		}
		else {
			BtSendEmpowerMsg empowerMsg = new BtSendEmpowerMsg(lockerRFID,
					opStatus == 6);
			mBtMsgService.writeValue(empowerMsg.createBtMsg());
		}

		upload();// 上传操作记录
	}

	/**
	 * 离线鉴权 从tempTask锁集合中查找对应rfid的锁
	 * 
	 * @param rfid
	 * @return
	 */
	private Lockset findLocket(String rfid) {

		if (tempTask != null) {// 临时任务
			for (Lockset lockset : tempTask.getLocksetList()) {
				if (TextUtils.equals(lockset.getRfid(), rfid))
					return lockset;
			}
		} else if (mWorkbill != null) {// 派工任务
			if (mWorkbill.getObject() != null) {
				List<Lockset> locksetList = mWorkbill.getObject().getLockset();
				if (locksetList != null) {
					for (Lockset lockset : locksetList) {
						if (TextUtils.equals(lockset.getRfid(), rfid))
							return lockset;
					}
				}
			}
		} else if(mWorkPatrol!=null) {
			List<Lockset> locksetList = getLocksetList(mWorkPatrol.getObjects());
			if (locksetList != null) {
				for (Lockset lockset : locksetList) {
					if (TextUtils.equals(lockset.getRfid(), rfid))
						return lockset;
				}
			}
		} else {
			List<Lockset> locksetList = getLocksetList(objects);
			if (locksetList != null) {
				for (Lockset lockset : locksetList) {
					if (TextUtils.equals(lockset.getRfid(), rfid))
						return lockset;
				}
			}
		}
		return null;
	}

	private List<Lockset> getLocksetList(List<DeviceObject> deviceObjectList) {
		List<Lockset> locksetList = new ArrayList<Lockset>();
		if (deviceObjectList != null && deviceObjectList.size() > 0) {
			for (DeviceObject deviceObject : deviceObjectList) {
				if (deviceObject.getLockset() != null && deviceObject.getLockset().size() > 0){
					locksetList.addAll(deviceObject.getLockset());
				}
			}
		}
		return locksetList;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.bt_conn, menu);
		if (mBtMsgService != null) {
			menu.findItem(R.id.menu_refresh)
					.setActionView(R.layout.actionbar_indeterminate_progress)
					.setVisible(mBtMsgService.isConnectIng());
			menu.findItem(R.id.menu_conn).setTitle(
					mBtMsgService.isConnected() ? "断开" : "连接");
		}
		return super.onCreateOptionsMenu(menu);
	}

	@SuppressLint("RestrictedApi")
    @Override
	protected boolean onPrepareOptionsPanel(View view, Menu menu) {
		for (int i = 0; i < menu.size(); i++) {
			MenuItem menuItem = menu.getItem(i);
			if (menuItem.getItemId() == R.id.menu_refresh ||
					menuItem.getItemId() == R.id.menu_conn) {
				menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			}
		}
		return super.onPrepareOptionsPanel(view, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.menu_conn:
			if (mBtMsgService != null && mBtMsgService.isConnected()) {
				mBtMsgService.disconnect();

			} else {
				connBt();
			}
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 接收BtMsgService发出的状态消�?<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015�?9�?22�? 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtState state) {
		switch (state) {
		case CONNECTING:
			invalidateOptionsMenu();
			break;
		case DISCONNECTED:
			getBasicActionBar().setTitle(
					getString(R.string.upload_ticket_result_title,
							getString(R.string.btstate_disconn)));
			invalidateOptionsMenu();
			break;
		case CONNECTED:
			break;
		case CONNECTOUTTIME:// 连接超时
			mActivity.dismissDialog();
			showMsgBox("蓝牙连接超时,请重试!");
			break;
		case DISCOVERED:
			invalidateOptionsMenu();
			getBasicActionBar().setTitle(
					getString(R.string.upload_ticket_result_title,
							getString(R.string.btstate_conned)));
			if (EkeyAPP.getInstance().isSCLock()) {
				if (mBtMsgService != null) {
					mBtMsgService.writeValue(SCSendMsg.getInstance().readLockerFrame());
				}
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 接收BtMsgService发出的状态消息<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午9:36:04
	 * @param btConnAuto
	 */
	public void onEventMainThread(BtConnAuto btConnAuto) {
		if (btConnAuto.isConnFinish()) {
			getBasicActionBar().setTitle(
					getString(R.string.upload_ticket_result_title,
							getString(R.string.btstate_disconn)));
			mBtMsgService.removeCallbacksConn();
		} else {
			getBasicActionBar().setTitle(
					getString(R.string.upload_ticket_result_title,
							getString(R.string.btstate_conning)));
			invalidateOptionsMenu();
		}
	}

	/**
	 * 接收BtMsgService发出蓝牙数据超时
	 * <p>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午11:55:44
	 * @param btTimeOut
	 */
	public void onEventMainThread(BtTimeOut btTimeOut) {
		// showMsgBox(R.string.shared_bt_timeout);
	}

	/**
	 * 接收BtMsgService发出的蓝牙电量
	 * <p>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午10:03:04
	 * @param btBatt
	 */
	public void onEventMainThread(BtBatt btBatt) {
		LogUtils.i("电量:" + btBatt.des);
		final boolean mBattMin = btBatt.isMin;
//		 if (mBattMin) {
//		 showMsgBox(R.string.batt_min_titile, R.string.batt_min_msg,
//		 new DialogInterface.OnClickListener() {
//
//		 @Override
//		 public void onClick(DialogInterface dialog, int which) {
//		 EkeyAPP.getInstance().finishActivity(
//		 TicketActivity.class);
//		 finish();
//		 }
//		 });
//		 }
	}

	/**
	 * 接收BtMsgService发出的蓝牙规约数据<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午10:25:18
	 * @param btHex
	 */
	public void onEventMainThread(BtHex btHex) {
		final String read = btHex.commd;
		if (!TextUtils.isEmpty(read)) {
			if (EkeyAPP.getInstance().isSCLock()) {
				byte[] extraData = BtMsgConver.hexStrToByte(btHex.commd);
				SCReceiveMsg receiveMsg = new SCReceiveMsg(extraData);
				if (receiveMsg.getRecvType() == 0x01) {
					SCReceiveBaseResult receiveBaseResult = receiveMsg.parseLockInfoMsg();
					if (receiveBaseResult.ismState()) {
						getAuthorize(BtMsgConver.bytesToHexStr(SCLockInfo.getInstance().getmLockerId()));
					}
				}
				if (receiveMsg.getRecvType() == 0x03) {
					mActivity.dismissDialog();
					SCReceiveBaseResult receiveBaseResult = receiveMsg.parseSwitchMsg();
					if (receiveBaseResult.ismState()) {
						showMsgBox("开锁成功");
					}
					else {
						showMsgBox("开锁失败");
					}
				}
				return;
			}
			BtRecvMsg btRecvMsg = BtMsgTools.recv(read);
			if (btRecvMsg != null) {
				if (btRecvMsg instanceof BtRecvEmpowerMsg) {// 接收授权记录
					BtRecvEmpowerMsg authorizeMsg = (BtRecvEmpowerMsg) btRecvMsg;
					authorizeMsg.createDataSource();
					getAuthorize(authorizeMsg.dataSource);
				} else if (btRecvMsg instanceof BtRecvAckMsg) {// 接收发送解锁的ACK
					BtRecvAckMsg ackMsg = (BtRecvAckMsg) btRecvMsg;
					ackMsg.createDataSource();
					if (ackMsg.btErrorCode == BtErrorCode.SUCCESS) {
						if (mBtMsgService != null)
							mBtMsgService.battStart();
						// upload();
					} else {
						showToast(ackMsg.btErrorCode.value);
					}
				} else {// 数据响应不匹�?
					// showMsgBox(R.string.shared_bt_error);
				}
			} else {// 数据解析失败
				showMsgBox(R.string.shared_bt_error1);
			}
		} else {// 接收数据为空
			showMsgBox(R.string.shared_bt_error2);
		}
	}

	/**
	 * 更新结果
	 */
	private void upload() {

		if (user == null)// 用户为NULL
			return;
		else if (user.isOnline)// 非离线模式上传操作记录
			uploadResult();
		else
			saveResult();// 离线模式保存到本地
	}

	/**
	 * 上传操作記錄
	 * 
	 */
	private void uploadResult() {

		if (mlockset == null && mlocksetALL == null)
			return;

		// 参数设置
		RequestParams params = new RequestParams(mActivity.getUser());

		if (mlockset != null) {
			params.putParams("lid", mlockset.getId());
			params.putParams("result", mlockset.getOpStatus());
		} else {
			params.putParams("lid", mlocksetALL.getId());
			params.putParams("result", mlocksetALL.getOpStatus());
		}

		if (tempTask != null)// 临时鉴权
		{
			params.putParams("action", "temp");// 鉴权开锁fixed|临时申请开锁temp
			params.putParams("tid", tempTask.getId());
		} else if (mWorkbill != null) {
			params.putParams("action", "workbill");// 派工
			params.putParams("tid", mWorkbill.getId());
		} else if (mWorkPatrol != null) {
//			params.putParams("action", "workpatrol");// 巡检
			params.putParams("action", "fixed");// 巡检
			params.putParams("tid", mWorkPatrol.getId());
		} else// 在线鉴权
		{
			params.putParams("action", "fixed");// 鉴权开锁fixed|临时申请开锁temp
			params.putParams("tid", 0);
		}

		requestHttp.doPost(Globals.UPDATA_LOADRESULT, params,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

						// 在线保存失败后保存记录数据
						if (value.contains("error")) {
							saveResult();
						}
					}
				});

	}

	/**
	 * 保存操作記錄
	 * 
	 */
	private void saveResult() {

		if (mlockset == null && mlocksetALL == null)
			return;

		int tid = 0;
		String action;

		int lid = 0;
		int result = 0;
		if (mlockset != null) {// 权限范围内锁
			lid = mlockset.getId();
			result = mlockset.getOpStatus();
		} else {// 权限范围外锁
			lid = mlocksetALL.getId();
			result = mlocksetALL.getOpStatus();

		}

		if (tempTask != null)// 临时鉴权
		{
			action = "temp";// 鉴权开锁fixed|临时申请开锁temp
			tid = tempTask.getId();
		} else if (mWorkbill != null) {
			action = "workbill";// 派工
			tid = mWorkbill.getId();
		} else if (mWorkPatrol != null) {
//			action = "workpatrol";// 巡检
			action = "fixed";// 巡检
			tid = mWorkPatrol.getId();
		} else// 在线鉴权
		{
			action = "fixed";// 鉴权开锁fixed|临时申请开锁temp
			tid = 0;
		}

		String currdate = DateUtil.getCurrentDate("yyyyMMddHHmmss");

		// 离线锁对象
		OfflineHistory operationRecord = new OfflineHistory(tid, action,
				result, lid, currdate);

		DbUtils dbUtils = DBHelp.getInstance(mActivity).getDb();
		SQLiteDatabase database = dbUtils.getDatabase();
		database.beginTransaction();
		try {
			dbUtils.replace(operationRecord);// 保存到数据库
			database.setTransactionSuccessful();
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			database.endTransaction();
		}

	}

	// 临时鉴权开锁界面跳转
	public static void startActivity(Context context, TempTask temptask) {
		Intent intent = new Intent(context, EmpowerActivity.class);
		if (temptask != null) {
			Bundle bund = new Bundle();
			bund.putParcelable("temptask", temptask);
			intent.putExtras(bund);
		}
		context.startActivity(intent);
	}

	// 临时鉴权开锁界面跳转
	public static void startActivityWorkbill(Context context, Workbill tasking) {
		Intent intent = new Intent(context, EmpowerActivity.class);
		Bundle bund = new Bundle();
		bund.putParcelable("Workbill", tasking);
		intent.putExtras(bund);
		context.startActivity(intent);
	}

	// 临时鉴权开锁界面跳转
	public static void startActivityWorkPatrol(Context context,
			WorkPatrol tasking) {
		Intent intent = new Intent(context, EmpowerActivity.class);
		Bundle bund = new Bundle();
		bund.putParcelable("WorkPatrol", tasking);
		intent.putExtras(bund);
		context.startActivity(intent);
	}

	// 界面跳转
	public static void startActivity(Context context) {
		Intent intent = new Intent(context, EmpowerActivity.class);
		context.startActivity(intent);
	}

	private void downloadLocksetAll() {
		if (!user.isOnline) {
			return;
		}
		// 设置参数
		RequestParams prams = new RequestParams(EkeyAPP.getInstance().getUser());
		prams.putParams("page", -1);
		RequestHttp requestHttp = new RequestHttp(EkeyAPP.getInstance().getWLD());
		String url = String.format(Globals.USER_NAME_OBJECT, user.getUsername());
		requestHttp.doPost(url, prams,
				new RequestHttp.HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

						if (value.contains("success")) {// 返回成功
							try {
								JSONObject jsonObject = new JSONObject(value);
								JSONArray jsonArray = jsonObject
										.getJSONArray("items");
								// 获取所有设备
								objects = new Gson().fromJson(
										jsonArray.toString(),
										new TypeToken<List<DeviceObject>>() {
										}.getType());
							} catch (JSONException e1) {
								e1.printStackTrace();
							}
						}
					}
				});

	}

	//开始请求钥匙电量
	protected void battStart() {
		if (mBtMsgService != null)
			mBtMsgService.battStart();
	}

}
