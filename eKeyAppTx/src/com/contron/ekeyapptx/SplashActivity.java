/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

import com.contron.ekeyapptx.main.TicketActivity;


/**
 * 欢迎界面
 *
 * @author hupei
 * @date 2015��4��1�� ����1:52:24
 */
public class SplashActivity extends FragmentActivity {

    private Activity mActivity;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题
        setContentView(R.layout.activity_splash);
        mActivity = this;
        TicketActivity.startActivity(mActivity, TicketActivity.EXTRA_TYPE_100);
        mActivity.finish();
//		View view = ((ViewGroup) getWindow().getDecorView()).getChildAt(0);
//		// 动画过渡
//		AlphaAnimation animation = new AlphaAnimation(0.4f, 1.0f);
//		animation.setDuration(3000);
//		view.startAnimation(animation);
//		animation.setAnimationListener(new AnimationListener() {
//			@Override
//			public void onAnimationEnd(Animation arg0) {
//				TicketActivity.startActivity(mActivity, TicketActivity.EXTRA_TYPE_100);
//				mActivity.finish();
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation animation) {
//			}
//
//			@Override
//			public void onAnimationStart(Animation animation) {
//			}
//
//		});
    }


}
