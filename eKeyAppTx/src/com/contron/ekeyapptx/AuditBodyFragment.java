/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.InputDialogFragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/**
 * 临时鉴权任务审批-查看临时鉴权任务包含锁具详情
 * 
 * @author luoyilong
 *
 */
public class AuditBodyFragment extends ListFragment {
	private BasicActivity mActivity;
	private TempTask mTempTask;// 临时鉴权任务对象
	private List<DeviceObject> mListDevice;// 临时鉴权任务中的设备
	private ContronAdapter<DeviceObject> mAdapter; //适配器
	private RequestHttp requestHttp;// 请求后台服务

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = (BasicActivity) activity;
	}

	public static AuditBodyFragment newInstance(TempTask dailyPatrol,
			List<DeviceObject> list) {
		AuditBodyFragment bodyFragment = new AuditBodyFragment();
		bodyFragment.mListDevice = list;//接收传过来临时任务下的设备
		bodyFragment.mTempTask = dailyPatrol;//接收传过来临时任务
		return bodyFragment;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		mActivity.getBasicActionBar().setTitle("审批详细");

		requestHttp = new RequestHttp(mActivity.getWld());
		mAdapter = new ContronAdapter<DeviceObject>(mActivity,
				android.R.layout.simple_list_item_1, mListDevice) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					DeviceObject item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
		setListAdapter(mAdapter);
	}

	/**
	 * 审批后更新后台状
	 */
	private void updateAuditState(String status, String memo) {

		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话萨改框
		// 初始化参数
		RequestParams prams = new RequestParams(mActivity.getUser());
		prams.putParams("action", status);
		prams.putParams("memo", memo);

		// 临时鉴权任务审批
		String url = String.format(Globals.TEMP_TASK_TID_VERIFY,
				mTempTask.getId());
		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {
			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mActivity.dismissDialog();
						if (value.contains("success")) {
							mActivity.showToast("审批成功！");
							mActivity.finish();
						} else {
							String error = "";
							try {
								JSONObject json = new JSONObject(value);
								error = json.getString("error");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							mActivity.showToast(error);
						}

					}
				});
			}
		});

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.audit_body, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home://返回
			mActivity.getSupportFragmentManager().popBackStack();
			break;
		case R.id.menu_no://审批：不同意
			InputDialogFragment inputDialogFragment = new InputDialogFragment();
			inputDialogFragment.setTitleId(R.string.audit_dialog_title);
			inputDialogFragment.setHintId(R.string.audit_dialog_hiht);
			inputDialogFragment
					.setOnInputTextListener(new InputDialogFragment.OnInputTextListener() {
						@Override
						public void OnInputText(String text) {
							updateAuditState("拒绝", text);// 拒绝意見
						}
					});
			inputDialogFragment.show(mActivity.getFragmentManager(),
					"inputDialogFragment");
			break;
		case R.id.menu_ok://审批：同意
			updateAuditState("批准", "");
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
