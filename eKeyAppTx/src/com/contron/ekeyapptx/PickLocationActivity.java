package com.contron.ekeyapptx;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.map.CollectActivity;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

/**
 * 设备坐标采集
 * 
 * @author luoyilong
 *
 */


public class PickLocationActivity extends BasicActivity {

	@ViewInject(R.id.listview_pick)
	private ListView mListView;
	private List<DeviceObject> arrayList = new ArrayList<DeviceObject>();// 设备集合
	private List<DeviceObject> tempArrayList = new ArrayList<DeviceObject>();// 临时显示设备集合
	private ContronAdapter<DeviceObject> mContronAdapter;

	@ViewInject(R.id.searchView1)
	private SearchView sear;

	private String queryText = "";// 查询过滤
	private BasicActivity mActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pick_location);
		ViewUtils.inject(this);
		getBasicActionBar().setTitle("设备坐标采集");
		EkeyAPP.getInstance().addActivity(this);
		mActivity = this;
		mContronAdapter = new ContronAdapter<DeviceObject>(this,
				R.layout.activity_pick_logation_item, tempArrayList) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					final DeviceObject item, int position) {
				viewHolder.setText(R.id.tx_lockname, item.getName());// 锁名称
				final TextView lockLatitude = viewHolder
						.findView(R.id.tx_lockLatitude);
				final TextView locklongitude = viewHolder
						.findView(R.id.tx_locklongitude);

				locklongitude.setText("经度：" + item.getLng());
				lockLatitude.setText("纬度：" + item.getLat());

				Button button = (Button) viewHolder.findView(R.id.but_pick);
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						String wld = mActivity.getWld();
						String k = mActivity.getUser().getKValue();

						CollectActivity.startActivity(mActivity, item.id, wld,
								k);
					}
				});
			}
		};
		mListView.setAdapter(mContronAdapter);

		sear.setOnQueryTextListener(new OnQueryTextListener() {

			@Override
			public boolean onQueryTextSubmit(String arg0) {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean onQueryTextChange(String arg0) {
				queryText = arg0;
				setFilter(arg0);
				return false;
			}
		});// 设置sear控件的查询事件。

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getData();
		setFilter(queryText);
	}

	/**
	 * 从数据库存读取设备数据
	 */
	private void getData() {
		// 获取数据
		arrayList.clear();// 先清空数据
		arrayList.addAll(DBHelp.getInstance(this).getEkeyDao()
				.getDeviceObjectAll());
	}

	/**
	 * 界面跳转
	 * 
	 * @param context
	 */
	public static void gotoActivity(Context context) {
		context.startActivity(new Intent(context, PickLocationActivity.class));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.picklocation, menu);

		// MenuItem searchItem = menu.findItem(R.id.action_search); //
		// 获取菜单中的R.id.action_search菜单控件。
		// sear = (SearchView) searchItem.getActionView();//
		// 通过searchItem菜控件的getActionView()方法获取SearchView对象。
		// sear.setQueryHint("输入设备名称进行搜索");// 设置sear控件展开时显示的文本。

		// 搜索按钮展开，关闭
		// searchItem.setOnActionExpandListener(new
		// MenuItem.OnActionExpandListener() {
		// @Override
		// public boolean onMenuItemActionExpand(MenuItem item) {
		// return true;
		// }
		//
		// @Override
		// public boolean onMenuItemActionCollapse(MenuItem item) {
		// mActivity.getBasicActionBar().setTitle("设备位置采集");
		// return true;
		// }
		// });

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			mActivity.finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/***
	 * 根据传入字符搜索
	 * 
	 * @param str
	 */

	public void setFilter(String str) {
		tempArrayList.clear();

		if (str.equals("")) {
			tempArrayList.addAll(arrayList);
		} else {
			for (int i = 0; i < arrayList.size(); i++) {
				DeviceObject device = arrayList.get(i);
				if (device.getName().contains(str)) {
					tempArrayList.add(device);
				}

			}
		}
		mContronAdapter.notifyDataSetChanged();
	}

}
