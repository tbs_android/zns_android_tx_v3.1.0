package com.contron.ekeyapptx.fsu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.Station;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RemoteUnlockFragment extends Fragment {

    @ViewInject(R.id.lv_public)
    private ListView lisview;

    @ViewInject(R.id.tx_empty)
    private TextView empty;

    private static final String ARG_PARAM = "DeviceObject";

    private BasicActivity mActivity;
    private RequestHttp requestHttp;// 后台请求对象
    private ContronAdapter<Lockset> mAdapter;// 适配器
    private View loadMoreView;// 下一页视图
    private User user = null;// 当前登录用户

    private Station mDevice;
    private List<Lockset> mLocksetList = new ArrayList<Lockset>();// 设备

    public static RemoteUnlockFragment newInstance(Station device) {
        RemoteUnlockFragment fragment = new RemoteUnlockFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM, device);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
        if (getArguments() != null) {
            mDevice = getArguments().getParcelable(ARG_PARAM);
            if (mDevice != null) {
                mLocksetList = mDevice.getLocksets();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        ViewUtils.inject(this, view);
        view.findViewById(R.id.layout_search).setVisibility(View.GONE);
        loadMoreView = inflater.inflate(R.layout.item_footerview, null);
        loadMoreView.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item40_name));

        mAdapter = new ContronAdapter<Lockset>(mActivity,
                R.layout.fragment_remote_unlock, mLocksetList) {
            @Override
            public void setViewValue(ContronViewHolder holder,
                                     final Lockset item, final int position) {
                holder.setText(R.id.tx_lock_name, item.getName());
                String lockStatus = "";
                switch (item.getLock_status()) {
                    case "false":
                        lockStatus = "关闭";
                        break;
                    case "true":
                        lockStatus = "开启";
                        break;
                    default:
                        lockStatus = "-";
                        break;
                }
                holder.setText(R.id.tx_lock_status, lockStatus);
                holder.findView(R.id.bt_unlock).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 发送解锁指令
                        remoteUnlock(item);
                    }
                });
            }
        };
        lisview.addFooterView(loadMoreView); // 设置列表底部视图
        lisview.setAdapter(mAdapter);
        lisview.setEmptyView(empty);
        mAdapter.notifyDataSetChanged();
    }

    private void remoteUnlock(Lockset lockset) {
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());
        params.putParams("oid", mDevice.getId());
        params.putParams("doorcode", lockset.getDoorcode());
        params.putParams("lockName", lockset.getName());

        // 请求接口
        String url = String.format(Globals.REMOTE_UNLOCK);

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.dismissDialog();
                        if (value.contains("success")) {
                            mActivity.showToast("远程开门成功");
                        } else {
                            String error = "";
                            try {
                                JSONObject object = new JSONObject(value);
                                error = object.getString("error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mActivity.showToast("远程开门失败：" + error);
                        }

                    }
                });

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item44_name);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackFragment();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 返回前一个页面
     */
    private void onBackFragment() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            mActivity.getSupportFragmentManager().popBackStack();
        else
            mActivity.finish();
    }
}
