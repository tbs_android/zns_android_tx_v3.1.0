package com.contron.ekeyapptx.fsu;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.plan.TicketDetaiActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.LockerResult;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.util.Gps;
import com.lidroid.xutils.util.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * FSU-权限内锁具显示
 * @author luoyilong
 *
 */
public class FsuDistantListFragment extends ListFragment {
	private BasicActivity mActivity;
	private List<Lockset> arrayList;// 设备锁集合
	private ContronAdapter<Lockset> mContronAdapter;//显示权限内锁适配器
	double distance = 1000;
	private LocationClient mLocationClient;
	private ProgressDialog progDia;

	public static Fragment newInstance() {
		// TODO Auto-generated method stub
		return new FsuDistantListFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
		arrayList = new ArrayList<Lockset>();
		
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		mActivity.getBasicActionBar().setTitle(R.string.ticket_fsudistant_title);
		
		
		arrayList = DBHelp.getInstance(mActivity).getEkeyDao().getLocksetAll();
		mContronAdapter = new ContronAdapter<Lockset>(mActivity,
				R.layout.fragment_fsudistant_item, arrayList) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					final Lockset item, int position) {
				viewHolder.setText(R.id.lock_type, item.getName());
				Button button = (Button) viewHolder.findView(R.id.button1);
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						getlat(item);// 获取经纬度
					}
				});
			}
		};

		setListAdapter(mContronAdapter);
		setEmptyText("暂无数据！");
	}

	/**
	 * 远控请求解锁
	 */
	private void requestFsuUnbindLoke(Lockset lockset, boolean state) {
		LockerResult lockerResult;
		User user = EkeyAPP.getInstance().getUser();

		if (state) {
			lockerResult = DBHelp.getInstance(mActivity).getEkeyDao()
					.saveLockeResult(lockset, user, "Fsu远控操作", "开锁指令失败");
			mActivity.showToast("Fsu远控请求解锁失败！");
		} else {
			lockerResult = DBHelp.getInstance(mActivity).getEkeyDao()
					.saveLockeResult(lockset, user, "Fsu远控操作", "开锁指令失败");
			mActivity.showToast("请在距离10米之内申请！");
		}

		// 刷新操作记录
		EventBusManager.getInstance().post(lockerResult);

	}

	/**
	 * 获取当前经纬度与数据库保存的对比
	 */
	private void getlat(final Lockset lockset) {
		mLocationClient = Gps.getGps(mActivity);

		distance = 1000;
		if (TextUtils.isEmpty(lockset.getLatitude())
				|| TextUtils.isEmpty(lockset.getLongitude())) {
			requestFsuUnbindLoke(lockset, false);
			return;
		}

		// 弹出对话�?
		progDia = mActivity.showSpinnerDialog(mActivity, "提示",
				"Fsu远控解锁申请...请稍候！", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Gps.stop(mLocationClient);
					}
				});

		Gps.start(mLocationClient, new BDLocationListener() {
			@Override
			public void onReceiveLocation(BDLocation location) {
				LogUtils.e("经度=============纬度");
				if (location != null) {
					String lat = Gps.format(8, location.getLatitude());
					String longitude = Gps.format(8, location.getLongitude());
					LogUtils.e("经度=====:" + longitude + "========纬度:" + lat);

					mActivity.showToast("经度1:" + longitude + "经度2:"
							+ lockset.getLongitude() + "纬度1:" + lat + "纬度1:"
							+ lockset.getLatitude());

					Gps.stop(mLocationClient);
					if (!TextUtils.isEmpty(lat)
							&& !TextUtils.isEmpty(longitude)) {
						distance = Gps.getDistance(
								Double.parseDouble(lockset.getLongitude()),
								Double.parseDouble(lockset.getLatitude()),
								Double.parseDouble(longitude),
								Double.parseDouble(lat));
					}
					LogUtils.e("===========距离=========�?" + distance);
					if (progDia != null)
						progDia.dismiss();

					// mActivity.showToast("距离:" + distance);

					if (distance > 10)
						requestFsuUnbindLoke(lockset, false);
					else
						requestFsuUnbindLoke(lockset, true);
				}
			}
		});

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.fsuapply, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();
			break;
		case R.id.menu_apply:// 申请智能钥匙开锁
			applyKey();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 请求申请智能钥匙解锁
	 */
	private void applyKey() {
		// 界面跳转
		TicketDetaiActivity.startActivity(mActivity);
		
//		setHasOptionsMenu(false);
//		 mActivity.hideAdd(FsuDistantFragment.class,
//		 TicketDetailFragment.newInstance());
		
//		mActivity.repalce(TicketDetailFragment.newInstance(), false);

	}
	
}
