package com.contron.ekeyapptx.fsu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.LockerResult;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * FSU-操作记录
 * @author luoyilong
 *
 */
public class FsuDistantResultFragment extends ListFragment {
	private BasicActivity mActivity;
	private ContronAdapter<LockerResult> mAdapter;
	private RequestHttp requestHttp;

	public static Fragment newInstance() {
		// TODO Auto-generated method stub
		return new FsuDistantResultFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
		EventBusManager.getInstance().register(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		EventBusManager.getInstance().unregister(this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		requestHttp = new RequestHttp(mActivity.getWld());
		mAdapter = new ContronAdapter<LockerResult>(mActivity,
				R.layout.fragment_empower_item, new ArrayList<LockerResult>()) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					LockerResult item, int position) {

				viewHolder
						.setText(R.id.empower_item_textView1, item.getOname())
						.setText(R.id.empower_item_textView2,
								item.getUnlock_at());

				TextView tvStatus = viewHolder
						.findView(R.id.empower_item_textView3);

				tvStatus.setText(item.getResult());
			}
		};
		setListAdapter(mAdapter);

	}

	public void onEventMainThread(LockerResult ticketResult) {
		if (!mAdapter.getDatas().contains(ticketResult)) {
			mAdapter.addData(ticketResult);
		}

		mAdapter.notifyDataSetChanged();
		// 按时间排序
		mAdapter.sortDatas(new Comparator<LockerResult>() {

			@Override
			public int compare(LockerResult lhs, LockerResult rhs) {
				return rhs.getUnlock_at().compareTo(lhs.getUnlock_at());
			}
		});
		uploadResult(ticketResult);// 上传
	}

	/**
	 * 上传操作日志
	 * 
	 * @param ticketResult
	 */
	private void uploadResult(LockerResult ticketResult) {
		// 上传操作日志
		
		//参数设置
		RequestParams params=new RequestParams(mActivity.getUser());
		params.putParams("lid", ticketResult.getLid());
		params.putParams("action", "temp");// temp表示“临时开锁“
		params.putParams("result", 0);// 0表示操作失败
		params.putParams("tid", 0);

		requestHttp.doPost(Globals.UPDATA_LOADRESULT,params,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

					}
				});
	}

}
