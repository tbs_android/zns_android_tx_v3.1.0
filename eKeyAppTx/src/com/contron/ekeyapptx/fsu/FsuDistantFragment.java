package com.contron.ekeyapptx.fsu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.doorlock.DoorLockerListFragment;
import com.contron.ekeyapptx.doorlock.DoorLockerResultFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 
 * @author luoyilong FSU远控操作
 */
public class FsuDistantFragment extends Fragment {

	@ViewInject(R.id.fragment1)
	private FrameLayout mFragmentLeft;
	@ViewInject(R.id.fragment2)
	private FrameLayout mFragmentRight;
	private BasicActivity mActivity;

	public static FsuDistantFragment newInstance() {
		return new FsuDistantFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BasicActivity) getActivity();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_fsudistant, container,
				false);
		ViewUtils.inject(this, view);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.add(R.id.fragment1, FsuDistantListFragment.newInstance(),
				DoorLockerListFragment.class.getSimpleName());
		transaction.add(R.id.fragment2, FsuDistantResultFragment.newInstance(),
				DoorLockerResultFragment.class.getSimpleName());
		transaction.commit();
	}
}
