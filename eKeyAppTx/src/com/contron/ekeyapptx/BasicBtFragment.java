/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtSearchManage;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.lidroid.xutils.util.LogUtils;

public abstract class BasicBtFragment extends Fragment {

	/**
	 * 蓝牙选择回调
	 * 
	 * @param address
	 */
	protected void onSelectBt(String address) {
	}
	protected boolean mBeginScan = true;
	protected BasicActivity mActivity;
	private Intent gattServiceIntent;
	protected BtMsgService mBtMsgService;//蓝牙服务
	protected BtSearchManage mBtSearchManage;//蓝牙管理工厂
	private ServiceConnection mServiceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName,
				IBinder service) {
			mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
			if (mBtMsgService.initialize()) {
				mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
				LogUtils.i("蓝牙服务绑定成功");
				mBtSearchManage = new BtSearchManage(mActivity,
						mBtMsgService.getBluetoothAdapter(),
						new BtSearchManage.OnSelectBtKeyListener() {
							@Override
							public void onSelectBtKey(Smartkey btKey) {
								// TODO 选择蓝牙
								String bttmp = btKey.getBtaddr().replaceAll(
										"(.{2})", "$1:");
								String btaddr = bttmp.substring(0,
										bttmp.length() - 1);
								EkeyAPP.getInstance().setBtAddress(btaddr);
								onSelectBt(EkeyAPP.getInstance().getBtAddress());
								connBt();
							}
						});
				if (mBeginScan) {
					connBt();
				}

			} else {
				LogUtils.e("Unable to initialize Bluetooth");
				mActivity.showToast("绑定蓝牙服务失败");
				mActivity.finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBtMsgService = null;
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
	}

	@Override
	public void onResume() {
		super.onResume();
		//注册EventBusManager事件
		EventBusManager.getInstance().register(this);
		if (gattServiceIntent == null)
			gattServiceIntent = new Intent(mActivity, BtMsgService.class);
		//启动服务
		mActivity.bindService(gattServiceIntent, mServiceConnection,
				Activity.BIND_AUTO_CREATE);
	}

	@Override
	public void onPause() {
		super.onPause();
		scanLeDevice(false);
		//注销EventBusManager事件
		EventBusManager.getInstance().unregister(this);
		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
			if (!mBtMsgService.isConnected())
				mBtMsgService.removeCallbacksConn();
		}
		//解除蓝牙服务邦定
		mActivity.unbindService(mServiceConnection);

	}

	/**
	 * 启动、停止\搜索
	 * 
	 * @param enable
	 */
	protected void scanLeDevice(boolean enable) {
		if (mBtSearchManage != null)
			mBtSearchManage.scanLeDevice(enable);
	}

	/**
	 * 连接蓝牙
	 * 
	 * @author hupei
	 * @date 2015�?5�?9�? 上午9:51:31
	 */
	protected void connBt() {
		LogUtils.e("连接开始");
		if (mBtMsgService != null
				&& !TextUtils.isEmpty(EkeyAPP.getInstance().getBtAddress()))
			mBtMsgService.autoConnect(EkeyAPP.getInstance().getBtAddress());
		else {
			if (mBtSearchManage != null)
				mBtSearchManage.scanLeDevice(true);
		}
	}

	//开始请求钥匙电量 
	protected void battStart() {
		if (mBtMsgService != null)
			mBtMsgService.battStart();
	}

	//移除连接蓝牙线程
	protected void removeCallbacksConn() {
		if (mBtMsgService != null)
			mBtMsgService.removeCallbacksConn();
	}

	protected void onMenuConn() {
		if (mBtMsgService.isConnected()) {
			mBtMsgService.disconnect();
		} else {
			connBt();//连接
		}
	}
}
