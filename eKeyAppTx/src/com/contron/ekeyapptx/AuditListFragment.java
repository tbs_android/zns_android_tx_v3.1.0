/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;

import com.bumptech.glide.Glide;
import com.contron.ekeyapptx.main.TicketActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.BitmapCallback;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.ConfigUtil;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.InputDialogFragment;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.BitmapUtils;

/**
 * 临时任务审批列表
 * 
 * @author luoyilong
 *
 */
public class AuditListFragment extends SwipeRefreshListFragment {
	public static final String KEY_LIST_REFRESH = "AuditListFragmentRefresh";
	private TicketActivity mActivity;
	private ContronAdapter<TempTask> mAdapter;// 适配器显示

	private RequestHttp requestHttp;// 后台服务请求
	private List<TempTask> ticketHeads;// 获取任务

	// 保存任务下设备信息
	private List<List<DeviceObject>> listDevice = new ArrayList<List<DeviceObject>>();

	// 返回当前实例
	public static AuditListFragment newInstance() {
		AuditListFragment fragment = new AuditListFragment();
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (TicketActivity) getActivity();

		// 实例化图片对象
		final BitmapUtils bitmapUtils = new BitmapUtils(mActivity);
		bitmapUtils.configDefaultLoadingImage(R.drawable.photo_report_un);
		bitmapUtils.configDefaultBitmapConfig(Config.RGB_565);
		mAdapter = new ContronAdapter<TempTask>(mActivity,
				R.layout.fragment_audit_list_item, new ArrayList<TempTask>()) {

			@Override
			public void setViewValue(ContronViewHolder holder,
					final TempTask item, final int position) {
				// TODO Auto-generated method stub
				holder.setText(android.R.id.text1, item.getName()).setText(
						android.R.id.text2,
						"申  请  人：" + item.getApply_by() + "\n开始时间："
								+ DateUtil.timeCompare(item.getBeginAt())
								+ "\n结束时间："
								+ DateUtil.timeCompare(item.getEndAt()));

				// 头像
				final ImageView portraitimg = holder
						.findView(R.id.temporary_apply_iv_head);
				final String portraiturl = mActivity.getWld() + "/public/"
						+ item.getApply_username()
						+ "/portrait-temp-" + item.getId() + ".jpg";
				Glide.with(getContext()).load(portraiturl).placeholder(R.drawable.ic_head).into(portraitimg);

//				if (item.getPortraitmap() != null) {
//					portraitimg.setImageBitmap(item.getPortraitmap());
					portraitimg.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ArrayList<String> urlList = new ArrayList<String>();
							final String url = "/public/"
									+ item.getApply_username()
									+ "/portrait-temp-" + item.getId() + ".jpg";
							urlList.add(portraiturl);
							PhotoBrowsePagerActivity.gotoActivity(mActivity,
									urlList, position, "头像照片浏览");
						}
					});
//				} else
//					portraitimg.setImageResource(R.drawable.ic_head);// 显示默认照片

				// 身份证
				final ImageView idcardimg = holder
						.findView(R.id.temporary_apply_iv_identity);
				final String idcardurl = mActivity.getWld() + "/public/"
						+ item.getApply_username()
						+ "/idcard-temp-" + item.getId() + ".jpg";
				Glide.with(getContext()).load(idcardurl).placeholder(R.drawable.ic_identity).into(idcardimg);

//				if (item.getIdcardmap() != null) {
//					idcardimg.setImageBitmap(item.getIdcardmap());
					idcardimg.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						ArrayList<String> urlList = new ArrayList<String>();

						final String url = "/public/"
								+ item.getApply_username()
								+ "/idcard-temp-" + item.getId() + ".jpg";
						urlList.add(idcardurl);
						PhotoBrowsePagerActivity.gotoActivity(mActivity,
								urlList, position, "身份证照片浏览");
						}
					});
//				} else
//					idcardimg.setImageResource(R.drawable.ic_identity);// 显示默认照片

				holder.findView(R.id.audit_list_item_button2)// 不同意按钮
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								InputDialogFragment inputDialogFragment = new InputDialogFragment();
								inputDialogFragment
										.setTitleId(R.string.audit_dialog_title);
								inputDialogFragment
										.setHintId(R.string.audit_dialog_hiht);
								inputDialogFragment
										.setOnInputTextListener(new InputDialogFragment.OnInputTextListener() {
											@Override
											public void OnInputText(String text) {

												updateAudit("拒绝", text, item,
														position);
											}
										});
								inputDialogFragment.show(
										mActivity.getFragmentManager(),
										"inputDialogFragment");
							}
						});

				holder.findView(R.id.audit_list_item_button1)// 同意按钮
						.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								updateAudit("批准", "", item, position);
							}
						});
			}
		};

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);// 想让Fragment中的onCreateOptionsMenu生效必须先调用setHasOptionsMenu方法
		setLoadMoreView();
		setListAdapter(mAdapter);
	}

	@Override
	protected void createLoadMoreView(LayoutInflater inflater) {
		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = loadMoreView.findViewById(R.id.rad_next);


		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadTicket(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
	}

	@Override
	public void onResume() {
		super.onResume();
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item3_name);

		if (ConfigUtil.getInstance(mActivity).getBoolean(KEY_LIST_REFRESH)) {
			ConfigUtil.getInstance(mActivity).setBoolean(KEY_LIST_REFRESH,
					false);
		}
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			onResume();
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		TempTask tempTask = mAdapter.getItem(position);
		// 查看临时任务详情
		mActivity.hideAdd(
				AuditListFragment.class,
				AuditBodyFragment.newInstance(tempTask,
						listDevice.get(position)));
	}

	/**
	 * 下载自己相关的待执行与执行中的临时任务票
	 */
	private void downloadTicket(int currentItem) {
		setRefreshing(false);

		if (currentItem == 0) {// 当前页数为0时清空数据
			listDevice.clear();
			mAdapter.setEmpty();
			mAdapter.notifyDataSetChanged();
		}

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		// 初始化参数
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("page", currentItem);

		requestHttp.doPost(Globals.TEMP_TASK_VERIFY, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								if (value.contains("success")) {
									// 对查询出来的结果转换成实体
									getAuditListDone(value);
									downloadPic(ticketHeads);// 下载图片
								} else {
									setEmptyText("暂时无数据！");
									setEmptyViewShown(true);
								}

								mActivity.dismissDialog();// 关闭对话框
							}
						});
					}
				});
	}

	/**
	 * 刷新数据
	 */
	@Override
	public void onListRefresh() {
		super.onListRefresh();

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		downloadTicket(0);// 下载临时鉴权任务
	}

	/**
	 * 更新单个审批结果
	 * 
	 * @param status
	 * @param memo
	 * @param mTempTask
	 */
	private void updateAudit(String status, String memo, TempTask mTempTask,
			final int position) {

		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话框

		// 参数设置
		RequestParams prams = new RequestParams(mActivity.getUser());
		prams.putParams("action", status);
		prams.putParams("memo", memo);
		// 请求Url
		String url = String.format(Globals.TEMP_TASK_TID_VERIFY,
				mTempTask.getId());

		// 请求
		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {

			@Override
			public void resultValue(final String value) {
				// TODO Auto-generated method stub
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mActivity.dismissDialog();
						if (value.contains("success")) {// 返回成功
							mActivity.showToast(R.string.offline_audit_sussmsg);
							mAdapter.removeDatas(position);
							// mActivity.invalidateOptionsMenu();// 加载菜单
						} else {// 返回失败
							String error = "";
							try {
								JSONObject json = new JSONObject(value);
								error = json.getString("error");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							mActivity.showToast(error);

						}

					}
				});
			}
		});

	}

	/**
	 * 更新批量审批结果
	 * 
	 * @param status
	 */
	private void updateAuditAll(String status, String memo) {

		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话框

		// 参数设置
		RequestParams prams = new RequestParams(mActivity.getUser());
		prams.putParams("action", status);
		prams.putParams("memo", memo);
		mAdapter.setEmpty();
		// 请求
		requestHttp.doPost(Globals.TEMP_TASK_VERIFY_ALL, prams,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {// 返回成功
									mActivity.showToast("全部审批成功！");
									// mActivity.invalidateOptionsMenu();// 加载菜单
								} else {// 返回失败
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}

							}
						});
					}
				});

	}

	/**
	 * 对查找出来的数据转换成实体
	 * 
	 * @param wsdlJsonResult
	 *            查詢結果
	 */
	private void getAuditListDone(String wsdlJsonResult) {

		try {
			JSONObject jsonObject = new JSONObject(wsdlJsonResult);
			JSONArray jsonArrayHead = jsonObject.getJSONArray("items");

			// 获取总页数
			if (mcurrentItem == 0) {
				mItemCount = jsonObject
						.getInt("pages");
			}

			// 获取任务中设备
			for (int i = 0; i < jsonArrayHead.length(); i++) {
				JSONObject itemsjson = jsonArrayHead.getJSONObject(i);
				JSONArray objectjson = itemsjson.getJSONArray("object");

				if (objectjson != null) {
					List<DeviceObject> ticketBodys = new Gson().fromJson(
							objectjson.toString(),
							new TypeToken<List<DeviceObject>>() {
							}.getType());
					listDevice.add(ticketBodys);

				}

			}

			// 获取任务
			ticketHeads = new Gson().fromJson(jsonArrayHead.toString(),
					new TypeToken<List<TempTask>>() {
					}.getType());

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 下载图片\更新适配器
	 */
	private void downloadPic(final List<TempTask> ticketHeads) {

		if (ticketHeads != null && ticketHeads.size() > 0) {

			for (int i = 0; i < ticketHeads.size(); i++) {
				final int j = i;
				TempTask item = ticketHeads.get(j);

				final String portraiturl = "/public/"
						+ item.getApply_username() + "/portrait-temp-"
						+ item.getId() + ".jpg";
				final String idcardurl = "/public/"
						+ item.getApply_username() + "/idcard-temp-"
						+ item.getId() + ".jpg";

				DisplayMetrics displayMetrics = new DisplayMetrics();
				getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);// 获取分辨率
				// 获取头像
				requestHttp.downloadImg(portraiturl, displayMetrics.widthPixels, displayMetrics.heightPixels, new BitmapCallback() {

					@Override
					public void resultValueBitmap(final Bitmap bitmap) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (bitmap != null) {
									ticketHeads.get(j).setPortraitmap(bitmap);
									mAdapter.notifyDataSetChanged();
								}
							}
						});

					}
				});

				// 获取身份证
				requestHttp.downloadImg(idcardurl, displayMetrics.widthPixels, displayMetrics.heightPixels, new BitmapCallback() {

					@Override
					public void resultValueBitmap(final Bitmap bitmap) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (bitmap != null) {
									ticketHeads.get(j).setIdcardmap(bitmap);
									mAdapter.notifyDataSetChanged();
								}
							}
						});

					}
				});
			}

			// mActivity.invalidateOptionsMenu();// 重新加载菜单
			mAdapter.addAllDatas(ticketHeads);
			setEmptyViewShown(false);
		} else {
			setEmptyText("暂时无数据！");
			setEmptyViewShown(true);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// 屏蔽批量审批菜單
		// if (mAdapter != null && !mAdapter.isEmpty()) {
		// inflater.inflate(R.menu.audit_list, menu);
		// }
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		switch (id) {
		case android.R.id.home:// 返回
			mActivity.finish();
			break;
		case R.id.menu_ok_all:// 批量批准
			updateAuditAll("批准", "");
			break;
		case R.id.menu_no_all:// 批量拒绝
			auditAllOn();
			break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * 批量拒绝
	 */
	private void auditAllOn() {
		InputDialogFragment inputDialogFragment = new InputDialogFragment();
		inputDialogFragment.setTitleId(R.string.audit_dialog_title);
		inputDialogFragment.setHintId(R.string.audit_dialog_hiht);
		inputDialogFragment
				.setOnInputTextListener(new InputDialogFragment.OnInputTextListener() {
					@Override
					public void OnInputText(String text) {
						updateAuditAll("拒绝", text);
					}
				});
		inputDialogFragment.show(mActivity.getFragmentManager(),
				"inputDialogFragment");
	}

}
