/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx.doorlock;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ListFragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.contron.ekeyapptx.qrscanner.ScannerActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.bluetooth.BtConnAuto;
import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.bluetooth.code.BtErrorCode;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecControlUnlockMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvAckMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.bluetooth.code.send.BtSendCheckDoorState;
import com.contron.ekeypublic.bluetooth.code.send.BtSendCheckLockerState;
import com.contron.ekeypublic.bluetooth.code.send.BtSendControlUnlockMsg;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.DoorLocker;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.TicketResult;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.WorkPatrol;
import com.contron.ekeypublic.entities.Workbill;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType.BtHex;
import com.contron.ekeypublic.eventbus.EventBusType.BtPIO;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.AppUtils;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.MsgBoxFragment;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.plan.TicketDetaiActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 在线蓝牙开锁-显示在线锁具
 *
 * @author luoyilong
 */
public class DoorLockerListFragment extends ListFragment {
    private BasicActivity mActivity;
    private DoorLocker mDoorLocker;// 在线锁对象
    private ContronAdapter<DoorLocker> mContronAdapter;// 显示在线锁适配器
    private boolean playBeep;// 提示音
    private BtMsgService mBtMsgService;// 蓝牙服务
    private BluetoothAdapter mBluetoothAdapter;// 蓝牙适配器
    private MsgBoxFragment mBoxFragment = new MsgBoxFragment();
    private boolean mScanning;
    private Handler mHandler = new Handler();
    private MediaPlayer mediaPlayer;
    private boolean showDialog;
    private TempTask mTemptask = null;// 临时任务
    private Workbill mWorkbill = null;// 派工任务
    private WorkPatrol mWorkPatrol = null;// 巡检

    private List<DeviceObject> objects;

    private int tempLockerId = 0;
    private int tempStatus = -1;
    private boolean menuState = false;// 菜单上‘申请智能钥匙解锁’ true显示 false不显示
    private boolean unregister = false;// true标识 从智能钥匙界面跳转过来

    private Intent gattServiceIntent;
    private static String mScannedAddress = "";

    private ServiceConnection mServiceConnection;

    // 启动提示音
    private void playBeepSound() {
        if (playBeep && mediaPlayer != null) {
            mediaPlayer.start();
        }
    }

    // 初始化音频
    private void initBeepSound() {
        if (playBeep && mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnCompletionListener(beepListener);
            AssetFileDescriptor file = getResources().openRawResourceFd(
                    R.raw.beep);
            try {
                mediaPlayer.setDataSource(file.getFileDescriptor(),
                        file.getStartOffset(), file.getLength());
                file.close();
                mediaPlayer.setVolume(0.10f, 0.10f);
                mediaPlayer.prepare();
            } catch (IOException e) {
                mediaPlayer = null;
            }
        }
    }

    private final OnCompletionListener beepListener = new OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };

    /**
     * 连接蓝牙
     *
     * @author hupei
     * @date 2015年5月9日 上午9:51:31
     */
    private void connBt(String address) {
        if (mBtMsgService != null) {
            LogUtils.e("连接开始");

            mActivity.startDialog(R.string.progressDialog_connectbt_msg);

            String bttmp = address.replaceAll("(.{2})", "$1:");
            String btaddr = bttmp.substring(0, bttmp.length() - 1);
            mBtMsgService.autoConnect(btaddr);
            showDialog = true;
        }
    }

    /**
     * 用于非提示的自动连接蓝牙
     *
     * @param address
     * @author guanhaiping
     * @date 2015年11月18日上午9:03:48
     */
    private void connBtNoDialog(String address) {
        if (mBtMsgService != null) {
            LogUtils.e("自动连接开始");
            String bttmp = address.replaceAll("(.{2})", "$1:");
            String btaddr = bttmp.substring(0, bttmp.length() - 1);
            mBtMsgService.autoConnect(btaddr);
            showDialog = false;
        }
    }

    /**
     * 界面跳转
     *
     * @return
     */
    public static DoorLockerListFragment newInstance() {
        return newInstance(null);
    }

    /**
     * 界面跳转
     *
     * @return
     */
    public static DoorLockerListFragment newInstanceScanner(String addr) {
        mScannedAddress = addr;
        return newInstance(null);
    }

    /**
     * 界面跳转 带参数
     *
     * @return
     */
    public static DoorLockerListFragment newInstance(TempTask temptask) {
        DoorLockerListFragment doorLockerListFragment = new DoorLockerListFragment();
        doorLockerListFragment.mTemptask = temptask;
        return doorLockerListFragment;
    }

    /**
     * 界面跳转 带参数
     *
     * @return
     */
    public static DoorLockerListFragment newInstanceWorkbill(Workbill tasking) {
        DoorLockerListFragment doorLockerListFragment = new DoorLockerListFragment();
        doorLockerListFragment.mWorkbill = tasking;
        return doorLockerListFragment;
    }

    /**
     * 界面跳转 带参数
     *
     * @return
     */
    public static DoorLockerListFragment newInstanceWorkPatrol(
            WorkPatrol workPatrol) {
        DoorLockerListFragment doorLockerListFragment = new DoorLockerListFragment();
        doorLockerListFragment.mWorkPatrol = workPatrol;
        return doorLockerListFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (BasicActivity) getActivity();
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName,
                                           IBinder service) {
                mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
                if (mBtMsgService.initialize()) {
                    mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
                    LogUtils.i("蓝牙服务绑定成功");
                    mBluetoothAdapter = mBtMsgService.getBluetoothAdapter();
                    if ("".equals(mScannedAddress)) {
                        beginScanBth();
                    }
                } else {
                    LogUtils.e("Unable to initialize Bluetooth");
                    mActivity.showToast("绑定蓝牙服务失败");
                    mActivity.finish();
                }

            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                mBtMsgService = null;
            }
        };
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (EkeyAPP.getInstance().getUser().isOnline) {
            downloadLocksetAll();
        } else {
            EventBusManager.getInstance().register(DoorLockerListFragment.this);
            if (gattServiceIntent == null)
                gattServiceIntent = new Intent(mActivity, BtMsgService.class);

            mActivity.bindService(gattServiceIntent, mServiceConnection,
                    Activity.BIND_AUTO_CREATE);
        }
        setHasOptionsMenu(true);
        mActivity.getBasicActionBar().setTitle(R.string.ticket_door_title);
        mContronAdapter = new ContronAdapter<DoorLocker>(mActivity,
                R.layout.fragment_door_list, new ArrayList<DoorLocker>()) {

            @Override
            public void setViewValue(ContronViewHolder viewHolder,
                                     DoorLocker item, int position) {

                if (item.state == 1) {// 判断当前锁是否在权限内
                    viewHolder.setText(R.id.text1, item.deviceName);
                } else if (item.state == 0) {
                    viewHolder.setText(R.id.text1, item.deviceName + "(未授权)");
                    viewHolder.setTextColor(R.id.text1, 0xFFFF0000);// 红色
                }

                if (item.doorState == 1) {// 开门状态
                    viewHolder.findView(R.id.imageView1).getLayoutParams().height = 64;
                    viewHolder.findView(R.id.imageView1).getLayoutParams().width = 64;
                    if (!item.lockerTypeId.equalsIgnoreCase("AT"))
                        viewHolder.findView(R.id.imageView1)
                                .setBackgroundResource(R.drawable.door_open);
                } else {// 关门状态
                    viewHolder.findView(R.id.imageView1).getLayoutParams().height = 64;
                    viewHolder.findView(R.id.imageView1).getLayoutParams().width = 64;
                    if (!item.lockerTypeId.equalsIgnoreCase("AT"))
                        viewHolder.findView(R.id.imageView1)
                                .setBackgroundResource(R.drawable.door_close);
                }
                if (item.lockState == 1) {// 开锁状态
                    viewHolder.findView(R.id.imageView2).getLayoutParams().height = 64;
                    viewHolder.findView(R.id.imageView2).getLayoutParams().width = 64;
                    if (!item.lockerTypeId.equalsIgnoreCase("AT"))
                        viewHolder
                                .findView(R.id.imageView2)
                                .setBackgroundResource(R.drawable.lock_unlocked);
                } else {// 闭锁状态
                    viewHolder.findView(R.id.imageView2).getLayoutParams().height = 64;
                    viewHolder.findView(R.id.imageView2).getLayoutParams().width = 64;
                    if (!item.lockerTypeId.equalsIgnoreCase("AT"))
                        viewHolder.findView(R.id.imageView2)
                                .setBackgroundResource(R.drawable.lock_locked);
                }
            }

        };
        setListAdapter(mContronAdapter);
        setEmptyText("无在线锁具！");
        playBeep = true;
        AudioManager audioService = (AudioManager) mActivity
                .getSystemService(Context.AUDIO_SERVICE);
        if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
            playBeep = false;
        }
        initBeepSound();
        initDialg();
        if (!"".equals(mScannedAddress)) {
            mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
//                    while (objects == null || objects.size() == 0) {
//                        try {
//                            Thread.sleep(100);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
                    mActivity.dismissDialog();
                    DoorLocker doorLocker = getDoorLockerByAddress(mScannedAddress);
                    if (doorLocker == null) {
                        mActivity.showToast("您无权开门，请获取权限后再试！");
                        return;
                    }
                    if (!mContronAdapter.getDatas().contains(doorLocker)) {
                        mDoorLocker = doorLocker;
                        mContronAdapter.addData(mDoorLocker);
                        // 连接蓝牙锁
                        connBt(mDoorLocker.bthAddr);
                    }
                }
            }, 1000);

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (unregister) {// 从智能钥匙界面跳转过来
            unregister = false;
            EventBusManager.getInstance().register(this);
            if (gattServiceIntent == null)
                gattServiceIntent = new Intent(mActivity, BtMsgService.class);

            mActivity.bindService(gattServiceIntent, mServiceConnection,
                    Activity.BIND_AUTO_CREATE);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtils.i("onPause");

        if (unregister) {// 跳转去智能钥匙界面
            scanLeDevice(false);
            EventBusManager.getInstance().unregister(this);
            if (mBtMsgService != null) {
                if (mBtMsgService.isConnected())// 连接中则断开
                    mBtMsgService.disconnect();
                else
                    mBtMsgService.removeCallbacksConn();
            }
            mActivity.unbindService(mServiceConnection);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // 释放资源
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer = null;
        }

        scanLeDevice(false);
        EventBusManager.getInstance().unregister(this);
        if (mBtMsgService != null) {
            if (mBtMsgService.isConnected())// 连接中则断开
                mBtMsgService.disconnect();
            else
                mBtMsgService.removeCallbacksConn();
        }
        mActivity.unbindService(mServiceConnection);
    }

    private void beginScanBth() {
        mActivity.dismissDialog();
        mActivity.startDialog(R.string.progressDialog_title_searchline,
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        scanLeDevice(false);
                    }
                });
        scanLeDevice(true);// 重新搜索蓝牙设备刷新
    }

    /**
     * 初始化对话框
     */
    private void initDialg() {
        mBoxFragment.setTitle(getString(R.string.ticket_door_diaog_title));
        mBoxFragment// 取消开锁
                .setNegativeOnClickListener(new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // mBtMsgService.writeValue(new
                        // BtSendControlUnlockMsg(false).createBtMsg());// 禁止开锁
                    }
                });
        mBoxFragment// 确定开锁
                .setPositiveOnClickListener(new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (mDoorLocker.lockerTypeId.equalsIgnoreCase("AT"))
                            mBtMsgService.writeValueString("AT+PIO21");
                        else
                            mBtMsgService
                                    .writeValue(new BtSendControlUnlockMsg(true)
                                            .createBtMsg());
                    }
                });
        mBoxFragment.setCanceledOnTouchOutside(false);

    }

    /**
     * 开始搜索
     *
     * @param enable
     */
    private void scanLeDevice(final boolean enable) {
        if (mBluetoothAdapter == null) {
            return;
        }
        if (enable) {
            if (mScanning) {
                return;
            }
            mScanning = true;
            double scanTime = 8.0;
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mScanning) {
                        mActivity.dismissDialog();
                        mScanning = false;
                        mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        changingMenuState();// 根据查找到的设备数量切换‘申请智能钥匙’显示状态
                    }
                }
            }, (long) scanTime * 1000);
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mActivity.dismissDialog();
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice bluetoothDevice,
                             final int rssi, final byte[] scanRecord) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    LogUtils.e("device=" + bluetoothDevice.getAddress());
                    String address = bluetoothDevice.getAddress().replace(":",
                            "");

                    mDoorLocker = getDoorLockerByAddress(address);
                    if (mDoorLocker == null) {
                        return;
                    }
                    if (!mContronAdapter.getDatas().contains(mDoorLocker)) {
                        if (!mDoorLocker.lockerTypeId
                                .equalsIgnoreCase("AT")) {
                            if (mScanning)// 停止搜索
                                scanLeDevice(false);
                            // 连接蓝牙锁
                            connBtNoDialog(mDoorLocker.bthAddr);

                        } else {
                            mActivity.dismissDialog();
                            mContronAdapter.addData(mDoorLocker);
                        }
                    }


                }
            });
        }
    };

    private DoorLocker getDoorLockerByAddress(String address) {
        DoorLocker doorLocker = null;
        Lockset mLockset = findLocket(address);
        if (mLockset != null) {// 判断锁是否存在
            doorLocker = new DoorLocker();
            doorLocker.state = 1; // 授权的钥匙 演示用
            doorLocker.lockerTypeId = mLockset.getType()
                    .substring(0, 2);
            // doorLocker.lockerTypeId="AT";
            doorLocker.bthAddr = mLockset.getBtaddr();
            doorLocker.deviceName = mLockset.getName();
            doorLocker.lockerId = mLockset.getId();
            doorLocker.section = mLockset.getSection();
        }
        return doorLocker;
    }

    private ProgressDialog mProgressDialog;

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        scanLeDevice(false);
        User user = EkeyAPP.getInstance().getUser();

        if (user == null) {
            mActivity.showToast("读用户信息失败");
            return;
        }

        // 在线模式无网络
        if (user.isOnline && !AppUtils.isNetworkAvailable(mActivity)) {
            mActivity.showToast("当前无网络禁止开锁,请连接网络后再试!");
            return;
        }

        // 判断临时鉴权任务是否过期
        if (mTemptask != null) {// 临时鉴权开锁
            boolean tempTaskState = judegTempTask();
            if (!tempTaskState)// 过期
            {
                mActivity.showMsgBox(R.string.temporary_ticket_overdue,
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                mActivity.finish();// 关闭当前窗口
                            }
                        });
                return;
            }
        }

        mDoorLocker = mContronAdapter.getItem(position);
        // 如果点击是同一个蓝牙设备，直接弹出开锁确定框，否则先连接。
        if (mContronAdapter.getSelectedPosition() == position) {
            if (mBtMsgService.isConnected()) {
                showDoorLockerDiaog(mDoorLocker);
            } else if (mDoorLocker.state == 0) {// 未授权
                showDoorLockerDiaog(mDoorLocker);
            } else {
                connBt(mDoorLocker.bthAddr);
            }
        } else {
            // 切换蓝牙设备，先断开当前连接。
            if (mBtMsgService.isConnected())
                mBtMsgService.disconnect();
            mContronAdapter.setSelectedPosition(position);
            if (mDoorLocker.state == 0) {// 未授权
                showDoorLockerDiaog(mDoorLocker);
            } else {
                connBt(mDoorLocker.bthAddr);
            }
        }
    }

    /**
     * 切换菜单‘申请智能钥匙解锁’
     */
    private void changingMenuState() {

        // 获取适配器中数据
        List<DoorLocker> doorlocker = mContronAdapter.getDatas();
        if (doorlocker == null || doorlocker.size() == 0) {// 一个设备都没搜索到，显示‘申请智能钥匙解锁’菜单
            menuState = true;
        } else {// 搜索到设备，隐藏‘申请智能钥匙解锁’菜单
            // DoorLocker door = doorlocker.get(0);
            // int lockNumber = DBHelp.getInstance(mActivity).getEkeyDao()
            // .selectLockNumber(door.lockerId);
            // if(doorlocker.size()<lockNumber)
            // menuState = true;
            // else
            menuState = false;
        }

        if (menuState) {// 弹出对话框
            mActivity.showMsgBox(R.string.hint_title, R.string.prompt_title,
                    false, new DialogInterface.OnClickListener() {// 确定
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mActivity.invalidateOptionsMenu();// 重新加载菜单
                            applyKey();
                        }
                    }, new DialogInterface.OnClickListener() {// 取消
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mActivity.invalidateOptionsMenu();// 重新加载菜单
                        }
                    });
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.refresh, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            if (menuItem.getItemId() == R.id.menu_scan ||
                    menuItem.getItemId() == R.id.menu_refresh) {
                menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            } else {
                if (menuState) {
                    menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
                }
            }
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.finish();
                break;
            case R.id.menu_refresh:
                beginScanBth();
                break;
            case R.id.menu_apply:// 申请智能钥匙开锁
                applyKey();
                break;
            case R.id.menu_scan:
                if (ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //权限还没有授予，需要在这里写申请权限的代码
                    ActivityCompat.requestPermissions(mActivity,
                            new String[]{Manifest.permission.CAMERA}, 60);
                } else {
                    ScannerActivity.gotoActivity(mActivity, true, 0, 1,
                            false, true, true);
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 请求申请智能钥匙解锁
     */
    private void applyKey() {

        unregister = true;

        if (mTemptask != null)// 判断是否为临时鉴权
        {
            TicketDetaiActivity.startActivity(mActivity, mTemptask);
        } else if (mWorkbill != null) {
            TicketDetaiActivity.startActivity(mActivity, mWorkbill);
        } else if (mWorkPatrol != null) {
            TicketDetaiActivity.startActivity(mActivity, mWorkPatrol);
        } else {
            TicketDetaiActivity.startActivity(mActivity);
        }

    }

    /**
     * 接收BtMsgService发出的蓝牙自动连接 <br>
     * {@linkplain EventBusManager#post(Object)}
     *
     * @param btConnAuto
     * @author hupei
     * @date 2015年9月22日 上午11:24:45
     */
    public void onEventMainThread(BtConnAuto btConnAuto) {
        if (btConnAuto.isConnFinish()) {
            mActivity.dismissDialog();// 关闭搜索对话框
            changingMenuState();// 根据查找到的设备数量切换‘申请智能钥匙’显示状态
            mBtMsgService.removeCallbacksConn();
        }
    }

    /**
     * 接收BtMsgService发出的状态<br>
     * {@linkplain EventBusManager#post(Object)}
     *
     * @param state
     * @author hupei
     * @date 2015年9月22日 上午9:36:04
     */
    public void onEventMainThread(BtState state) {
        switch (state) {
            case DISCONNECTED:
                break;
            case CONNECTED:
                break;
            case CONNECTOUTTIME:// 连接超时
                mActivity.dismissDialog();
                showMsgBox("蓝牙连接超时,请重试!");
                break;
            case DISCOVERED:

                if (mDoorLocker != null) {
                    if (showDialog == false) {// 挨个去查询蓝牙锁状态
                        if (!mDoorLocker.lockerTypeId.equalsIgnoreCase("AT"))
                            mBtMsgService.writeValue(new BtSendCheckLockerState()
                                    .createBtMsg());
                    } else {
                        // 连接成功会弹出，所选择设备的提示框
                        mActivity.dismissDialog();// 关闭连接蓝牙对话框
                        showDoorLockerDiaog(mDoorLocker);
                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * 弹出对话框
     *
     * @param device
     */
    private void showDoorLockerDiaog(DoorLocker device) {
        LogUtils.e("========");
        if (device.state == 1) {
            mBoxFragment.setMessage(getString(R.string.ticket_door_diaog_msg,
                    device.deviceName));
        } else if (device.state == 0) {
            mBoxFragment.setMessage(getString(R.string.ticket_door_diaog_msg1,
                    device.deviceName));
        }

        mBoxFragment.show(mActivity.getFragmentManager(), "msgBox");
    }

    /**
     * 接收BtMsgService发出的PIO命令 <br>
     * {@linkplain EventBusManager#post(Object)}
     *
     * @param btPIO
     * @author hupei
     * @date 2015年9月22日 上午10:31:51
     */
    public void onEventMainThread(BtPIO btPIO) {
        if (!btPIO.isSuccess()) {
            // uploadResult(0);// 开锁指令失败
            return;
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                mBtMsgService.writeValueString("AT+PIO20");
            }
        }, 2000);

        uploadResult(1);// 开锁指令成功
    }

    /**
     * 接收BtMsgService发出的蓝牙规约数据<br>
     * {@linkplain EventBusManager#post(Object)}
     *
     * @param btHex
     * @author hupei
     * @date 2015年9月22日 上午10:25:18
     */
    public void onEventMainThread(BtHex btHex) {
        final String read = btHex.commd;
        if (!TextUtils.isEmpty(read)) {
            BtRecvMsg btRecvMsg = BtMsgTools.recv(read);
            if (btRecvMsg != null) {
                if (btRecvMsg instanceof BtRecControlUnlockMsg) {// 接收锁闭合状态
                    BtRecControlUnlockMsg ControlUnlockMsg = (BtRecControlUnlockMsg) btRecvMsg;
                    ControlUnlockMsg.createDataSource();
                    if (ControlUnlockMsg.functionCode.equalsIgnoreCase("02")) { // 锁查询

                        if (ControlUnlockMsg.lockState == 1) { // 开锁状态
                            mDoorLocker.lockState = 1;
                        } else {// 闭锁状态
                            mDoorLocker.lockState = 0;
                        }

                        // 下发门状态查询
                        mBtMsgService.writeValue(new BtSendCheckDoorState()
                                .createBtMsg());

                    } else if (ControlUnlockMsg.functionCode
                            .equalsIgnoreCase("01")) { // 锁主动汇报
                        // 获取当前选择的锁对象
                        int position = mContronAdapter.getSelectedPosition();
                        mDoorLocker = mContronAdapter.getItem(position);

                        if (ControlUnlockMsg.lockState == 1) { // 开锁状态
                            mDoorLocker.lockState = 1;
                        } else {// 闭锁状态
                            mDoorLocker.lockState = 0;
                        }

                        // 更新在线锁状态
                        mContronAdapter.getDatas().set(position, mDoorLocker);
                        mContronAdapter.notifyDataSetChanged();

                        // 更新操作记录
                        if (mDoorLocker.lockState == 1)
                            uploadResult(2);// 开锁成功
                        else
                            uploadResult(5);// 闭锁成功

                    } else if (ControlUnlockMsg.functionCode
                            .equalsIgnoreCase("03")) {// 门状态汇报

                        // 获取当前选择的锁对象
                        int position = mContronAdapter.getSelectedPosition();
                        mDoorLocker = mContronAdapter.getItem(position);

                        if (ControlUnlockMsg.lockState == 1) { // 开門
                            mDoorLocker.doorState = 1;
                        } else {// 关门
                            mDoorLocker.doorState = 0;
                        }

                        // 更新在线门状态
                        mContronAdapter.getDatas().set(position, mDoorLocker);
                        mContronAdapter.notifyDataSetChanged();

                        // 更新操作记录
                        if (mDoorLocker.doorState == 1)
                            uploadResult(3); // 开門成功
                        else
                            uploadResult(4);// 关门成功

                    } else if (ControlUnlockMsg.functionCode
                            .equalsIgnoreCase("04")) {// 门查询

                        mActivity.dismissDialog();// 关闭搜索对话框

                        // 设备门状态
                        if (ControlUnlockMsg.lockState == 1) { // 开门
                            mDoorLocker.doorState = 1;
                        } else {// 关门
                            mDoorLocker.doorState = 0;
                        }

                        // 适配器中不存在当前设备，则添加
                        if (!mContronAdapter.getDatas().contains(mDoorLocker)) {
                            mContronAdapter.addData(mDoorLocker);
                            int count = mContronAdapter.getCount();
                            mContronAdapter.setSelectedPosition(count - 1);
                        }

                        // 再次开始搜索
                        scanLeDevice(true);

                    }

                } else if (btRecvMsg instanceof BtRecvAckMsg) {// 接收发送解锁的ACK
                    BtRecvAckMsg ackMsg = (BtRecvAckMsg) btRecvMsg;
                    ackMsg.createDataSource();
                    if (ackMsg.btErrorCode == BtErrorCode.SUCCESS) {
                        // mActivity.showToast("ack ok");
                        uploadResult(1);// 开锁指令成功
                    } else if (ackMsg.btErrorCode.code.equals("83")) {
                        mActivity.showMsgBox("FSU提示", "FSU通讯正常,禁止使用蓝牙开锁!",
                                new OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0,
                                                        int arg1) {

                                    }
                                });

                    } else {
                        mActivity.showToast(ackMsg.btErrorCode.value);
                        uploadResult(0);// 开锁指令失败
                    }
                } else {// 数据响应不匹配
                    // mActivity.showToast(R.string.shared_bt_error);
                }
            } else {// 数据解析失败
                mActivity.showToast(R.string.shared_bt_error1);
            }
        } else {// 接收数据为空
            mActivity.showToast(R.string.shared_bt_error2);
        }
    }

    /**
     * 更新DoorLockerResultFragment界面上的记录
     */
    private void uploadResult(int state) {
        // if (tempLockerId == mDoorLocker.lockerId && tempStatus == state)
        // return;
        tempLockerId = mDoorLocker.lockerId;
        tempStatus = state;
        User user = EkeyAPP.getInstance().getUser();
        if (user == null) {
            mActivity.showToast("读用户信息失败");
        } else {
            TicketResult ticketResult = DBHelp.getInstance(mActivity)
                    .getEkeyDao()
                    .saveTicketResultDoor(mDoorLocker, user.getId());
            ticketResult.opStatus = state;

            if (mTemptask != null)
                ticketResult._id = mTemptask.getId();

            playBeepSound();

            // 刷新操作记录
            ticketResult.section = mDoorLocker.section;
            EventBusManager.getInstance().post(ticketResult);
        }

    }

    /**
     * 离线鉴权 从tempTask\tasking锁集合中查找对应rfid的锁
     *
     * @param address
     * @return
     */
    private Lockset findLocket(String address) {

        if (mTemptask != null) {// 临时任务
            for (Lockset lockset : mTemptask.getLocksetList()) {
                if (TextUtils.equals(lockset.getBtaddr(), address))
                    return lockset;
            }
        } else if (mWorkbill != null) {// 派工任务
            if (mWorkbill.getObject() != null) {
                List<Lockset> locksetList = mWorkbill.getObject().getLockset();
                if (locksetList != null) {
                    for (Lockset lockset : locksetList) {
                        if (TextUtils.equals(lockset.getBtaddr(), address))
                            return lockset;
                    }
                }
            }
        } else if (mWorkPatrol != null) {
            List<Lockset> locksetList = getLocksetList(mWorkPatrol.getObjects());
            if (locksetList != null) {
                for (Lockset lockset : locksetList) {
                    if (TextUtils.equals(lockset.getBtaddr(), address))
                        return lockset;
                }
            }
        } else {
            if (!EkeyAPP.getInstance().getUser().isOnline) {
                Lockset lockset = DBHelp.getInstance(mActivity).getEkeyDao()
                        .findFirstLockset(address);
                return lockset;
            }
            List<Lockset> locksetList = getLocksetList(objects);
            if (locksetList != null) {
                for (Lockset lockset : locksetList) {
                    if (TextUtils.equals(lockset.getBtaddr(), address))
                        return lockset;
                }
            }
        }
        return null;
    }


    private List<Lockset> getLocksetList(List<DeviceObject> deviceObjectList) {
        List<Lockset> locksetList = new ArrayList<Lockset>();
        if (deviceObjectList != null && deviceObjectList.size() > 0) {
            for (DeviceObject deviceObject : deviceObjectList) {
                if (deviceObject.getLockset() != null && deviceObject.getLockset().size() > 0) {
                    List<Lockset> locksets = deviceObject.getLockset();
                    for (Lockset lockset : locksets) {
                        lockset.setSection(deviceObject.getSection());
                    }
                    locksetList.addAll(locksets);
                }
            }
        }
        return locksetList;
    }

    /**
     * 判断临时鉴权任务是否过期
     *
     * @return true未过期\false过期
     */
    private boolean judegTempTask() {

        // 任务结束时间
        String temptaskEndDate = DateUtil.getStringByFormat(
                mTemptask.getEndAt(), "yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss");
        int minutes = DateUtil.getMinutes(temptaskEndDate);

        if (minutes > 0) // 未过期
            return true;
        else
            // 过期
            return false;

    }

    /**
     * 弹出提示框
     */
    public void showMsgBox(String msg) {
        mActivity.showMsgBox("提示", msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
    }

    private void downloadLocksetAll() {

        // 设置参数
        RequestParams prams = new RequestParams(EkeyAPP.getInstance().getUser());
        prams.putParams("page", -1);
        RequestHttp requestHttp = new RequestHttp(EkeyAPP.getInstance().getWLD());
        String url = String.format(Globals.USER_NAME_OBJECT, mActivity.getUser().getUsername());
        requestHttp.doPost(url, prams,
                new RequestHttp.HttpRequestCallBackString() {
                    @Override
                    public void resultValue(String value) {

                        if (value.contains("success")) {// 返回成功
                            try {
                                JSONObject jsonObject = new JSONObject(value);
                                JSONArray jsonArray = jsonObject
                                        .getJSONArray("items");
                                // 获取所有设备
                                objects = new Gson().fromJson(
                                        jsonArray.toString(),
                                        new TypeToken<List<DeviceObject>>() {
                                        }.getType());
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        EventBusManager.getInstance().register(DoorLockerListFragment.this);
                                        if (gattServiceIntent == null)
                                            gattServiceIntent = new Intent(mActivity, BtMsgService.class);

                                        mActivity.bindService(gattServiceIntent, mServiceConnection,
                                                Activity.BIND_AUTO_CREATE);
                                    }
                                });

                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                });

    }

}
