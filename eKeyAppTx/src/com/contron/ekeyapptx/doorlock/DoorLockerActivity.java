package com.contron.ekeyapptx.doorlock;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.qrscanner.ScannerActivity;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.WorkPatrol;
import com.contron.ekeypublic.entities.Workbill;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.mylhyl.zxing.scanner.common.Scanner;

/**
 * 在线蓝牙锁开锁主界面
 *
 * @author luoyilong
 */
public class DoorLockerActivity extends BasicActivity {

    private TempTask mTemptask = null;// 临时任务

    private Workbill mWorkbill = null;// 派工

    private WorkPatrol mWorkPatrol = null;// 巡检

    private boolean mUseScanner = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_door_locker);
        ViewUtils.inject(this);
        mTemptask = getIntent().getParcelableExtra("TempTask");
        mWorkbill = getIntent().getParcelableExtra("Workbill");
        mWorkPatrol = getIntent().getParcelableExtra("WorkPatrol");
        mUseScanner = getIntent().getBooleanExtra("UseScanner", false);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        if (mTemptask != null) {
            transaction.replace(R.id.fragment1,
                    DoorLockerListFragment.newInstance(mTemptask),
                    DoorLockerListFragment.class.getSimpleName());

        } else if (mWorkbill != null) {

            transaction.replace(R.id.fragment1,
                    DoorLockerListFragment.newInstanceWorkbill(mWorkbill),
                    DoorLockerListFragment.class.getSimpleName());

        } else if (mWorkPatrol != null) {
            transaction.replace(R.id.fragment1,
                    DoorLockerListFragment.newInstanceWorkPatrol(mWorkPatrol),
                    DoorLockerListFragment.class.getSimpleName());
        } else if (mUseScanner) {
            //启动二维码扫描，返回扫描到的蓝牙地址发送到DoorLockerListFragment并直接连接
            ScannerActivity.gotoActivity(this, true, 0, 1,
                    false, true, true);
        } else {
            transaction.replace(R.id.fragment1,
                    DoorLockerListFragment.newInstance(),
                    DoorLockerListFragment.class.getSimpleName());
        }

        transaction.replace(R.id.fragment2,
                DoorLockerResultFragment.newInstance(),
                DoorLockerResultFragment.class.getSimpleName());
        transaction.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (resultCode != Activity.RESULT_CANCELED && resultCode == Activity.RESULT_OK) {
            if (requestCode == ScannerActivity.REQUEST_CODE_SCANNER) {
                if (data != null) {
                    String stringExtra = data.getStringExtra(Scanner.Scan.RESULT);
                    if (!"".equals(stringExtra)) {
                        int index = stringExtra.indexOf("id=") + 3;
                        String btAddr = stringExtra.substring(index, index + 12);
                        DoorLockerListFragment fragment = DoorLockerListFragment.newInstanceScanner(btAddr);
                        transaction.replace(R.id.fragment1, fragment, DoorLockerListFragment.class.getSimpleName())
                        .commit();
                        return;
                    }
                }
            }
        }
        transaction.replace(R.id.fragment1, DoorLockerListFragment.newInstance(), DoorLockerListFragment.class.getSimpleName())
        .commit();
    }

    /**
     * 界面跳转
     *
     * @param context
     */
    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, DoorLockerActivity.class));
    }

    /**
     * 界面跳转 从主页点击二维码扫描跳转
     *
     * @param context
     */
    public static void startActivity(Context context, boolean flag) {
        Intent intent = new Intent(context, DoorLockerActivity.class);
        intent.putExtra("UseScanner", flag);
        context.startActivity(intent);
    }

    /**
     * 界面跳转 从临时鉴权开锁界面跳转过来
     *
     * @param context
     * @param temptask
     */
    public static void startActivity(Context context, TempTask temptask) {
        Intent intent = new Intent(context, DoorLockerActivity.class);
        intent.putExtra("TempTask", temptask);
        context.startActivity(intent);
    }

    /**
     * 界面跳转 从派工执行跳转过来
     *
     * @param context
     * @param tasking
     */
    public static void startActivity(Context context, Workbill tasking) {
        Intent intent = new Intent(context, DoorLockerActivity.class);
        intent.putExtra("Workbill", tasking);
        context.startActivity(intent);
    }

    /**
     * 界面跳转 从派工执行跳转过来
     *
     * @param context
     * @param workPatrol
     */
    public static void startActivity(Context context, WorkPatrol workPatrol) {
        Intent intent = new Intent(context, DoorLockerActivity.class);
        intent.putExtra("WorkPatrol", workPatrol);
        context.startActivity(intent);
    }

}
