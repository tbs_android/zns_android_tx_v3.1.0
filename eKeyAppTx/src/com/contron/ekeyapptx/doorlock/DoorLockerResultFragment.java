/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx.doorlock;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.OfflineOperation;
import com.contron.ekeypublic.entities.TicketResult;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.DateUtil;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.db.sqlite.WhereBuilder;
import com.lidroid.xutils.exception.DbException;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 在线蓝牙开锁-操作记录界面
 * 
 * @author luoyilong
 *
 */
public class DoorLockerResultFragment extends ListFragment {
	private BasicActivity mActivity;
	private ContronAdapter<OfflineOperation> mAdapter;// 显示操作记录适配器
	private RequestHttp requestHttp;// 服务请求
	private String opid = "ff";// 保存上传指令后台返回的操作ID

	private List<TicketResult> listTicket = new ArrayList<TicketResult>();// 保存未上传的结果

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
		EventBusManager.getInstance().register(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		EventBusManager.getInstance().unregister(this);

		User user = mActivity.getUser();
		if (user != null && !user.isOnline)// 离线模式保存当前所有记录数据到本地
			saveResult();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		TextView tvHeader = new TextView(mActivity);

		requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
		tvHeader.setGravity(Gravity.CENTER);
		tvHeader.setBackgroundResource(android.R.color.background_light);
		tvHeader.setText("操作记录");
		getListView().addHeaderView(tvHeader);
		setEmptyText("无操作记录！");

		mAdapter = new ContronAdapter<OfflineOperation>(mActivity,
				R.layout.fragment_doorlockerresult,
				new ArrayList<OfflineOperation>()) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					OfflineOperation item, int position) {

				if (position == 0) {// 第一个item设置背景颜色
					LinearLayout iteimlayout = viewHolder
							.findView(R.id.layout_item);
					iteimlayout.setBackgroundColor(getResources().getColor(
							R.color.bg_titlebar));
				}
				initItemValue(viewHolder, item);// 填充适配器中Item
			}
		};
		setListAdapter(mAdapter);
	}

	public static DoorLockerResultFragment newInstance() {
		return new DoorLockerResultFragment();
	}

	/**
	 * 填充适配器中Item
	 * 
	 * @param viewHolder
	 * @param item
	 */
	private void initItemValue(ContronViewHolder viewHolder,
			OfflineOperation item) {

		viewHolder.setText(R.id.tx_devicename, item.getDeviceName());
		TextView instructionDate = viewHolder.findView(R.id.tx_instructionDate);// 指令时间
		TextView instructionState = viewHolder
				.findView(R.id.tx_instructionState);// 指令状态
		TextView openLockerDate = viewHolder.findView(R.id.tx_openLockerDate);// 开锁时间
		TextView openLockerState = viewHolder.findView(R.id.tx_openLockerState);// 开锁状态
		TextView openDoorDate = viewHolder.findView(R.id.tx_openDoorDate);// 开门时间
		TextView openDoorState = viewHolder.findView(R.id.tx_openDoorState);// 开门状态
		TextView closeDoorDate = viewHolder.findView(R.id.tx_closeDoorDate);// 关门时间
		TextView closeDoorState = viewHolder.findView(R.id.tx_closeDoorState);// 关门状态
		TextView closeLockerDate = viewHolder.findView(R.id.tx_closeLockerDate);// 闭锁时间
		TextView closeLockerState = viewHolder
				.findView(R.id.tx_closeLockerState);// 闭锁状态

		instructionDate.setVisibility(View.GONE);
		instructionState.setVisibility(View.GONE);
		openLockerDate.setVisibility(View.GONE);
		openLockerState.setVisibility(View.GONE);
		openDoorDate.setVisibility(View.GONE);
		openDoorState.setVisibility(View.GONE);
		closeDoorDate.setVisibility(View.GONE);
		closeDoorState.setVisibility(View.GONE);
		closeLockerDate.setVisibility(View.GONE);
		closeLockerState.setVisibility(View.GONE);

		boolean warn = false; // true 表示锁锁指令为空 false 表示开锁指令为非空
		if (TextUtils.isEmpty(item.getDirective_at()))
			warn = true;

		if (!warn) {// 开锁指令
			instructionDate.setVisibility(View.VISIBLE);
			instructionState.setVisibility(View.VISIBLE);

			if (!TextUtils.isEmpty(item.getDirective_fault_at())) {// 开锁失败
				instructionDate.setTextColor(Color.RED);
				instructionState.setTextColor(Color.RED);
				instructionState
						.setText(R.string.upload_ticket_result_item_enable0);
				instructionDate.setText(DateUtil.timeCompare(item
						.getDirective_fault_at()));
			} else {// 开锁成功
				instructionDate.setTextColor(Color.GREEN);
				instructionState.setTextColor(Color.GREEN);
				instructionState
						.setText(R.string.upload_ticket_result_item_enable1);
				instructionDate.setText(DateUtil.timeCompare(item
						.getDirective_at()));
			}
		}

		if (!TextUtils.isEmpty(item.getOpenLockerDate())) {// 开锁
			openLockerDate.setVisibility(View.VISIBLE);
			openLockerState.setVisibility(View.VISIBLE);
			openLockerDate.setText(DateUtil.timeCompare(item
					.getOpenLockerDate()));
			openLockerState.setText(R.string.upload_ticket_result_item_enable2);
			if (warn) // 开锁指令为空时显示为红色告警信息
			{
				openLockerState.setTextColor(Color.RED);
				openLockerDate.setTextColor(Color.RED);
			} else {
				openLockerState.setTextColor(Color.GREEN);
				openLockerDate.setTextColor(Color.GREEN);
			}
		}

		if (!TextUtils.isEmpty(item.getOpenDoorDate())) {// 开门
			openDoorDate.setVisibility(View.VISIBLE);
			openDoorState.setVisibility(View.VISIBLE);
			openDoorDate.setText(DateUtil.timeCompare(item.getOpenDoorDate()));
			openDoorState.setText(R.string.upload_ticket_result_item_enable3);
			if (warn)// 开锁指令为空时显示为红色告警信息
			{
				openDoorDate.setTextColor(Color.RED);
				openDoorState.setTextColor(Color.RED);
			} else {
				openDoorDate.setTextColor(Color.GREEN);
				openDoorState.setTextColor(Color.GREEN);
			}
		}

		if (!TextUtils.isEmpty(item.getCloseDoorDate())) {// 关门
			closeDoorDate.setVisibility(View.VISIBLE);
			closeDoorState.setVisibility(View.VISIBLE);
			closeDoorDate
					.setText(DateUtil.timeCompare(item.getCloseDoorDate()));
			closeDoorState.setText(R.string.upload_ticket_result_item_enable5);
			if (warn)// 开锁指令为空时显示为红色告警信息
			{
				closeDoorDate.setTextColor(Color.RED);
				closeDoorState.setTextColor(Color.RED);

			} else {
				closeDoorDate.setTextColor(Color.GREEN);
				closeDoorState.setTextColor(Color.GREEN);
			}

		}

		if (!TextUtils.isEmpty(item.getCloseLockerDate())) {// 闭锁
			closeLockerDate.setVisibility(View.VISIBLE);
			closeLockerState.setVisibility(View.VISIBLE);
			closeLockerDate.setText(DateUtil.timeCompare(item
					.getCloseLockerDate()));
			closeLockerState
					.setText(R.string.upload_ticket_result_item_enable4);
			if (warn)// 开锁指令为空时显示为红色告警信息
			{
				closeLockerDate.setTextColor(Color.RED);
				closeLockerState.setTextColor(Color.RED);
			} else {
				closeLockerDate.setTextColor(Color.GREEN);
				closeLockerState.setTextColor(Color.GREEN);

			}
		}

		if (!TextUtils.isEmpty(item.getUltra_at())) {// 越权
			closeLockerDate.setVisibility(View.VISIBLE);
			closeLockerState.setVisibility(View.VISIBLE);
			closeLockerDate.setText(DateUtil.timeCompare(item.getUltra_at()));
			closeLockerDate.setTextColor(Color.RED);
			closeLockerState.setTextColor(Color.RED);
			closeLockerState
					.setText(R.string.upload_ticket_result_item_disable);
		}

	}

	/**
	 * 更新操作记录
	 * 
	 * @param ticketResult
	 */
	public void onEventMainThread(TicketResult ticketResult) {
		int state = -1;

		if (ticketResult == null)// TicketAllResult对象为null直接返回
			return;

		OfflineOperation ticketAllResult = groupTicketAllResult(ticketResult);

		// if (mAdapter.getDatas().contains(ticketAllResult)) //
		// 适配器中包含当前TicketAllResult对象直接返回
		// return;

		List<OfflineOperation> allResults = mAdapter.getDatas();

		if (allResults != null) {// 遍历allResults查询其中是否有相同锁ID并且未结束对�?
			for (int i = 0; i < allResults.size(); i++) {
				OfflineOperation result = allResults.get(i);
				if (result.getLockerId() == ticketResult.lockerId
						&& result.isEndSign() == 0) {
					state = i;
				}
			}
		}

		if (state == -1) {// 新增一条数据到适配器中
			if (ticketResult.opStatus != 1) {// 新增记录非开锁指令成功时标识为结束
				ticketAllResult.setEndSign(1);// 非法操作信息一条显示不需要合并标识为结束
			}

			if (ticketResult.opStatus == 2 || ticketResult.opStatus == 3)
				uploadWarnMsgResult(ticketResult);// 只上传非正常开锁、开门的警告信息

			mAdapter.addData(ticketAllResult);

		} else {// 拼接到适配器数据中

			if (ticketResult.opStatus == 1)// 重复开锁指令
			{
				mAdapter.getDatas().get(state).setEndSign(1);// 上一条开锁指令标识结束
				mAdapter.addData(ticketAllResult);// 新添加一条开锁指令
			} else {

				mAdapter.getDatas().set(
						state,
						groupTicketAllResult(allResults.get(state),
								ticketResult));
			}
		}

		// 按时间倒序
		mAdapter.sortDatas(new Comparator<OfflineOperation>() {
			@Override
			public int compare(OfflineOperation lhs, OfflineOperation rhs) {
				return rhs.getUpdateDateTime().compareTo(
						lhs.getUpdateDateTime());
			}
		});

		update(ticketResult);// 上传操作記錄

	}

	/**
	 * 组合TicketAllResult对象
	 */
	private OfflineOperation groupTicketAllResult(TicketResult ticketResult) {

		String currDate = DateUtil.getCurrentDate("yyyyMMddHHmmss");// 当前时间
		OfflineOperation ticketAllResult = new OfflineOperation(
				ticketResult.deviceName, ticketResult.lockerId, currDate, 0);
		String ticketResultDate = DateUtil.getStringByFormat(
				ticketResult.backDateTime, "yyyyMMddHHmmss");
		
		if (ticketResult.opStatus == 0) {// 开锁指令失败
			ticketAllResult.setDirective_fault_at(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
			ticketAllResult.setEndSign(1);
		} else if (ticketResult.opStatus == 1) {// 开锁指令
			ticketAllResult.setDirective_at(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
		} else if (ticketResult.opStatus == 2) {// 开锁
			ticketAllResult.setOpenLockerDate(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
		} else if (ticketResult.opStatus == 3) {// 开门
			ticketAllResult.setOpenDoorDate(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
		} else if (ticketResult.opStatus == 4) {// 关门
			ticketAllResult.setCloseDoorDate(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
		} else if (ticketResult.opStatus == 5) {// 闭锁
			ticketAllResult.setCloseLockerDate(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
			ticketAllResult.setEndSign(1);
		} else if (ticketResult.opStatus == 255) {// 越权
			ticketAllResult.setUltra_at(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
			ticketAllResult.setEndSign(1);
		}

		ticketAllResult.setAction("fixed");
		return ticketAllResult;

	}

	/**
	 * 组合TicketAllResult对象
	 */
	private OfflineOperation groupTicketAllResult(
			OfflineOperation ticketAllResult, TicketResult ticketResult) {
		OfflineOperation ticketAllResults = ticketAllResult;

		String ticketResultDate = DateUtil.getStringByFormat(
				ticketResult.backDateTime, "yyyyMMddHHmmss");

		if (ticketResult.opStatus == 0) {// 开锁指令失败
			ticketAllResult.setDirective_fault_at(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
			ticketAllResult.setEndSign(1);
		} else if (ticketResult.opStatus == 1) {// 开锁指令成功
			ticketAllResult.setDirective_at(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
		} else if (ticketResult.opStatus == 2) {// 开锁
			ticketAllResult.setOpenLockerDate(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
		} else if (ticketResult.opStatus == 3) {// 开门
			ticketAllResult.setOpenDoorDate(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
		} else if (ticketResult.opStatus == 4) {// 关门
			ticketAllResult.setCloseDoorDate(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
		} else if (ticketResult.opStatus == 5) {// 闭锁
			ticketAllResult.setCloseLockerDate(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
			ticketAllResult.setEndSign(1);
		} else if (ticketResult.opStatus == 255) {// 越权
			ticketAllResult.setUltra_at(ticketResultDate);
			ticketAllResult.setResult(ticketResult.opStatus);
			ticketAllResult.setEndSign(1);
		}

		ticketAllResult.setAction("fixed");
		return ticketAllResults;

	}

	/**
	 * 判断延时更新
	 * 
	 * @param ticketResult
	 */
	private void update(final TicketResult ticketResult) {

		User user = mActivity.getUser();
		if (user == null || !user.isOnline)// 离线模式直接返回
			return;

		// 因为需要opStatus=1时上传后返回的opid
		if (ticketResult.opStatus == 2) {// 延时4秒
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						Thread.sleep(4000);
						uploadResult(ticketResult);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}).start();

		} else
			uploadResult(ticketResult);
	}

	/**
	 * 上传操作記錄
	 * 
	 * @param ticketResult
	 */
	private synchronized void uploadResult(final TicketResult ticketResult) {

		if (ticketResult.opStatus == 0 || ticketResult.opStatus == 1
				|| ticketResult.opStatus == 255) {
			opid = "";
		}

		// 参数设置
		RequestParams prams = new RequestParams(mActivity.getUser());
		prams.putParams("lid", ticketResult.lockerId);// 锁具id
		prams.putParams("action", "fixed");// 鉴权开锁fixed|临时申请开锁temp
		prams.putParams("result", ticketResult.opStatus);// 当前步骤的结果
		prams.putParams("opid", opid);// 指令命令返回的操作id
		if (ticketResult._id != 0)// 临时鉴权
			prams.putParams("tid", ticketResult._id);// 临时申请开锁时必带此项，鉴权开锁没有此项

		requestHttp.doPost(Globals.OPERATION_DO, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

						if (value.contains("success")) {// 返回成功
							try {
								JSONObject jsonObject = new JSONObject(value);
								if (value.contains("opid"))
									opid = jsonObject.getString("opid");
							} catch (JSONException e) {
								e.printStackTrace();
							}
						} else {
							// 上传失败则保存当前记录到本地
							saveErrorResult(ticketResult);
						}

					}
				});

	}

	/**
	 * 上传报警記錄
	 * 
	 * @param ticketResult
	 */
	private void uploadWarnMsgResult(final TicketResult ticketResult) {
		User user = mActivity.getUser();
		if (user == null || !user.isOnline)// 离线模式直接返回
			return;

		String state = "";// 报警记录只上传开锁及开门的
		switch (ticketResult.opStatus) {
		case 2:
			state = "[非法开锁]";
			break;
		case 3:
			state = "[非法开门]";
			break;
		}

		// 参数设置
		RequestParams prams = new RequestParams(user);
		prams.putParams("msg", ticketResult.section + " 锁具：" + ticketResult.deviceName
				+ state);

		// 上传操作日志
		requestHttp.doPost(Globals.UPDATA_ALERT, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

					}
				});
	}

	/**
	 * 保存操作記錄[离线操作]
	 * 
	 *
	 */
	private void saveResult() {
		// 如果没有操作记录则直接返回
		if (mAdapter.getDatas().size() < 1)
			return;

		List<OfflineOperation> lid = new ArrayList<OfflineOperation>();

		// 判断是保存一条数据还是保存所有mAdapter数据
		lid.addAll(mAdapter.getDatas());

		if (lid != null && lid.size() > 0) {
			DbUtils dbUtils = DBHelp.getInstance(mActivity).getDb();
			SQLiteDatabase database = dbUtils.getDatabase();
			database.beginTransaction();
			try {
				dbUtils.replaceAll(lid);// 保存到数据库
				database.setTransactionSuccessful();
			} catch (DbException e) {
				e.printStackTrace();
			} finally {
				database.endTransaction();
			}
		}
	}

	/**
	 * 保存操作記錄[在线上传失败]
	 * 
	 * @param ticketResult
	 * 
	 */
	private void saveErrorResult(final TicketResult ticketResult) {

		OfflineOperation offlie = DBHelp.getInstance(mActivity).getEkeyDao()
				.findOfflineOperation(ticketResult.lockerId);// 本地数据库中操作记录

		OfflineOperation opStatus1 = null;// 开锁指令操作记录

		boolean newStrte = true;// true，新增记录。false，更新记录

		if (offlie == null) {// 数据库中不存在当前记录，则新增一记录

			offlie = groupTicketAllResult(ticketResult);
			if (ticketResult.opStatus != 1)
				offlie.setEndSign(1);// 标记结束

		} else {// 数据库中存在当前记录
			if (ticketResult.opStatus == 1) {// 如果是开锁指令，数据库又存在
				opStatus1 = groupTicketAllResult(ticketResult);// 新增一条记录
				offlie.setEndSign(1);// 旧记录标识结束
				newStrte = false;
			} else {// 把当前状态组合到数据库中
				offlie = groupTicketAllResult(offlie, ticketResult);
				newStrte = false;
			}

		}

		DbUtils dbUtils = DBHelp.getInstance(mActivity).getDb();
		SQLiteDatabase database = dbUtils.getDatabase();
		database.beginTransaction();
		try {
			if (offlie != null) {
				if (newStrte)
					dbUtils.replace(offlie);
				else
					dbUtils.update(offlie,
							WhereBuilder.b("id", "=", offlie.getId()),
							"endSign", "directive_at", "unlock_at",
							"opendoor_at", "closedoor_at", "lock_at",
							"ultra_at");
			}

			if (opStatus1 != null)
				dbUtils.replace(opStatus1);
		} catch (DbException e1) {

			e1.printStackTrace();
		}
		database.setTransactionSuccessful();
		database.endTransaction();

	}
}
