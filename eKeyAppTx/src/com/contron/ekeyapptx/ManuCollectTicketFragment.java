package com.contron.ekeyapptx;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.amap.api.maps.model.LatLng;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvEmpowerMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.Zone;
import com.contron.ekeypublic.eventbus.EventBusType.BtHex;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.map.CollectActivity;
import com.contron.ekeypublic.view.MsgBoxFragment;
import com.google.gson.Gson;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * 锁码采集
 * 
 * @author luoyilong
 *
 */
public class ManuCollectTicketFragment extends BasicBtFragment {

	@ViewInject(R.id.locker_department)
	private Spinner mSpinnerDepartment;// 所属区域
	@ViewInject(R.id.spinner_objectType)
	private Spinner mSpinnerObjectType;// 设备类型
	@ViewInject(R.id.spinner_scope)
	private Spinner mSpinnerScope;// 所属范围
	@ViewInject(R.id.label_scope)
	private TextView mLabelScope;// 所属范围
	@ViewInject(R.id.spinner_carrier)
	private Spinner mSpinnerCarrier;// 运营商
	@ViewInject(R.id.match_btn)
	private Button matchBtn;
	@ViewInject(R.id.save_button)
	private Button saveBtn;
	
	@ViewInject(R.id.manul_listview)
	private ListView mlistView;
	@ViewInject(R.id.Device_Name)
	private EditText deviceName;
	@ViewInject(R.id.but_lnglat)
	private Button but_lnglat;// 采集设备坐标
	
	@ViewInject(R.id.but_search)
	private Button but_search;// 模糊查询
	@ViewInject(R.id.Search_Name)
	private EditText searchName; //模糊查询设备名称

	private LatLng latlng;// 当前经纬度

	private List<Section> sections = new ArrayList<Section>();// 获取所有区域
	private List<String> sectionsStr = new ArrayList<String>();
	private Section deviceDepartment;// 设备区域
	private String deviceDepartmentStr;// 设备区域
	private ContronAdapter<Section> spinnerAdapter;// 设备区域适配器

	private List<Zone> mScope = new ArrayList<Zone>();// 获取所属范围
	private Zone mZone = null;// 闭锁范围
	private ContronAdapter<Zone> mscopeAdapter;// 设备所属范围适配器

	private List<Dict> mObjectType = new ArrayList<Dict>();// 得到设备类型
	private ArrayList<String> mObjectTypeStr = new ArrayList<String>();
	private String deviceType;// 设备类型
	private ContronAdapter<String> objectTypeAdapter;// 设备类型适配器

	private List<Dict> mCarrierType = new ArrayList<Dict>();// 获取运营商
	private List<String> mCarrierTypeStr = new ArrayList<String>();
	private String carrierType;// 运营商
	private ContronAdapter<String> carrierAdapter;// 运营商适配器

	private List<Dict> mLockType = new ArrayList<Dict>();// 获取锁类型
	private Spinner spinnerLockStyle;
	private ContronAdapter<Dict> locketAdapter;// 锁类型适配器
	private List<Lockset> mListlock = new ArrayList<Lockset>();// 创建的锁对象

	private boolean flag = true;// 标识spinnerLockStyle锁类型第一次加载不做作任务处理【设备选择事件就会触发一次】
	private int size = 1;// 标识spinnerLockStyle锁类型第一次加载不做作任务处理
	private ContronAdapter<Lockset> mAdapter;
	private MsgBoxFragment btDeviceDialog;// 蓝牙搜索结果设备框
	private ContronAdapter<BluetoothDevice> btAdapter;
	private int selectPosition;
	final static Gson mgson = new Gson();
	private RequestHttp requestHttp;
	private Fragment mFragment;
	
	private ContronAdapter<DeviceObject> deviceAdapter;
	private ArrayList<DeviceObject> devices;
	private MsgBoxFragment deviceDialog;// 修改设备，设备搜索结果设备框
	private DeviceObject modifyDevice;
    private Boolean isModify;
    
	public static ManuCollectTicketFragment newInstance() {
		return new ManuCollectTicketFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_manually_collect,
				container, false);
		ViewUtils.inject(this, view);
		mFragment = this;
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mActivity.getBasicActionBar().setTitle(
				getString(R.string.mannul_collect_ticket_title,
						getString(R.string.btstate_connnone)));
		requestHttp = new RequestHttp(mActivity.getWld());

		initscope();// 获取范围信息填充到控件
		initSection();// 获取区域信息填充到控件
		initAddLocketAdapter();// 初始化新增锁适配器
		initBluetoothDevice();// 初始化蓝牙适配器
		initMsgBoxFragment();// 初始化蓝牙钥匙弹出对话框
		initLocketType();// 获取锁类型信息填充到控件
		initDeviceStyle();// 初始化设备类型适配器
		initCarrier();// 运营商

		matchBtn.setText("增加锁具");
		matchBtn.setBackgroundResource(R.drawable.navi_history_notes_bg);
 
		isModify = false;
		saveBtn.setVisibility(View.GONE);
		searchName.setFocusable(true);
		searchName.setFocusableInTouchMode(true);
		searchName.requestFocus();
		searchName.findFocus();
	}
	
	
	/**
	 * 修改设备锁具或者新增
	 */
	private void updataLocket(Lockset lock,final int position) {
		// TODO Auto-generated method stub
		int id = lock.getId();//id /oid
		mActivity.startDialog(R.string.progressDialog_title_submit);
		if(id == 0){ //新增
			RequestParams params = new RequestParams(mActivity.getUser());
			params.putParams("type", lock.getType());
			params.putParams("rfid", lock.getRfid());
			params.putParams("btaddr", lock.getBtaddr());
			params.putParams("name", lock.getName());
						
			String url = String.format(Globals.LOCKSET_OID_NEW, modifyDevice.getId());
			requestHttp.doPost(url, params, new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) { // 上传成功则清掉数据
									mActivity.showToast("新增成功");
									JSONObject jsonObject;
									try {
										jsonObject = new JSONObject(value);
										int lid = jsonObject.getInt("id");//根据实际情况定
										mListlock.get(position).setId(lid);
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
								} else {// 返回失败
									String error = "";
									try {
										JSONObject object = new JSONObject(value);
										error = object.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}

							}
					});
				}
			});
		}else{//修改
			RequestParams params = new RequestParams(mActivity.getUser());
			params.putParams("type", lock.getType());
			params.putParams("rfid", lock.getRfid());
			params.putParams("btaddr", lock.getBtaddr());
			params.putParams("name", lock.getName());
			
			String url = String.format(Globals.LOCKSET_OID_MODIFY, id);
			requestHttp.doPost(url, params, new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) { // 上传成功则清掉数据
									mActivity.showToast("修改成功");						
								} else {// 返回失败
									String error = "";
									try {
										JSONObject object = new JSONObject(value);
										error = object.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}

							}
					});
				}
			});
		}
		
	}
	
	/**
	 * 删除锁具
	 */
	private void deleteLockset(Lockset lock) {
		// TODO Auto-generated method stub
		int sid = lock.getId();//id /oid（父id，设备id）
		if(sid == 0){
			return;
		}
		mActivity.startDialog(R.string.progressDialog_title_submit);
		RequestParams params = new RequestParams(mActivity.getUser());
			
		String url = String.format(Globals.LOCKSET_OID_DEL, sid);
		requestHttp.doPost(url, params, new HttpRequestCallBackString() {
				@Override
				public void resultValue(final String value) {
					mActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							mActivity.dismissDialog();
							if (value.contains("success")) { // 上传成功则清掉数据
								mActivity.showToast("删除成功");						
							} else {// 返回失败
								String error = "";
								try {
									JSONObject object = new JSONObject(value);
									error = object.getString("error");
								} catch (JSONException e) {
									e.printStackTrace();
								}
								mActivity.showToast(error);
							}

						}
				});
			}
		});
	}
	
	/**
	 * 初始化新增锁适配器
	 */
	private void initAddLocketAdapter() {

		mAdapter = new ContronAdapter<Lockset>(mActivity,
				R.layout.adapter_manually, mListlock) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					Lockset item, final int position) {
				viewHolder.setText(R.id.tx_lockindex, "新增锁 " + (position + 1));
				
				if(!isModify){
					viewHolder.findView(R.id.save_button1).setVisibility(View.GONE);
				}
				viewHolder.findView(R.id.save_button1).setOnClickListener(
						new OnClickListener() {
							@Override
							public void onClick(View v) {
								AlertDialog.Builder builder = new AlertDialog.Builder(
										getActivity());
								builder.setIcon(R.drawable.advise); // 设置对话框的图标
								builder.setTitle("提示"); // 设置对话框的标题
								builder.setMessage("确定保存？"); // 设置要显示的内容
								// 确定按钮
								builder.setPositiveButton(android.R.string.ok,
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												updataLocket(mListlock.get(position),position);
											}

											
										});
								// 取消按钮
								builder.setNegativeButton(
										android.R.string.cancel,
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
											}
										});
								// 创建对话框
								AlertDialog dialog = builder.create();
								// 显示对话框
								dialog.show();
							}

						});
				viewHolder.findView(R.id.del_button1).setOnClickListener(
						new OnClickListener() {
							@Override
							public void onClick(View v) {
								AlertDialog.Builder builder = new AlertDialog.Builder(
										getActivity());
								builder.setIcon(R.drawable.advise); // 设置对话框的图标
								builder.setTitle("提示"); // 设置对话框的标题
								builder.setMessage("确定删除？"); // 设置要显示的内容
								// 确定按钮
								builder.setPositiveButton(android.R.string.ok,
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												
												if(isModify){ //修改设备信息的删除锁具
													deleteLockset(mListlock.get(position));
												}
												mListlock.remove(position);
												updataLocketAdapter();
											}

										});
								// 取消按钮
								builder.setNegativeButton(
										android.R.string.cancel,
										new DialogInterface.OnClickListener() {

											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
											}
										});
								// 创建对话框
								AlertDialog dialog = builder.create();
								// 显示对话框
								dialog.show();
							}

						});

				// 获取锁蓝牙地址
				viewHolder.findView(R.id.add_btaddr).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View arg0) {

								btAdapter.setEmpty();// 清空适配器

								if (mBtMsgService != null) {
									mBtMsgService.disconnect();
									if (!mBtMsgService.isConnected())
										mBtMsgService.removeCallbacksConn();
								}

								mBtMsgService.getBluetoothAdapter()
										.startLeScan(leScanCallback);
								btDeviceDialog.show(
										mActivity.getFragmentManager(), "btt");
								mAdapter.setSelectedPosition(position);
							}

						});

				// 获取锁rfid码
				viewHolder.findView(R.id.add_rfid).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View arg0) {
								mActivity
										.showToast("请连接智能钥匙成功后,把钥匙插入锁孔进行锁码采集！");
								mAdapter.setSelectedPosition(position);
							}
						});

				// 锁Rfid
				final EditText edRfid = viewHolder.findView(R.id.rfid);
				edRfid.setText(item.getRfid());
				edRfid.setOnFocusChangeListener(new OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (!hasFocus) { // edRfid失去焦点
							String rfid = edRfid.getText().toString()
									.toUpperCase();
							if (!TextUtils.isEmpty(rfid)
									&& position < mListlock.size()) {

								mListlock.get(position).setRfid(
										rfid.replace(" ", ""));
							}
						}
					}
				});
				// 锁蓝牙地址
				final EditText edaddress = viewHolder.findView(R.id.bt_addr);
				edaddress.setText(item.getBtaddr());

				edaddress.setOnFocusChangeListener(new OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						// TODO Auto-generated method stub
						if (!hasFocus) { // edRfid失去焦点
							String address = edaddress.getText().toString()
									.toUpperCase();
							if (!TextUtils.isEmpty(address)
									&& position < mListlock.size()) {
								mListlock.get(position).setBtaddr(
										address.replace(" ", ""));
							}
						}
					}
				});

				// 锁名称
				final EditText lockName = viewHolder.findView(R.id.locker_name);
				lockName.setText(item.getName());
				lockName.setOnFocusChangeListener(new OnFocusChangeListener() {

					@Override
					public void onFocusChange(View arg0, boolean arg1) {
						if (!arg1) { // lockName失去焦点
							// mAdapter.setSelectedPosition(position);

							String lockname = lockName.getText().toString();
							if (!TextUtils.isEmpty(lockname)
									&& position < mListlock.size()) {
								mListlock.get(position).setName(
										lockname.replace(" ", ""));
							}
						}

					}
				});

				// 锁类型
				spinnerLockStyle = viewHolder.findView(R.id.lock_type_spinner);
				spinnerLockStyle.setAdapter(locketAdapter);

				// 设置当前选中的锁类型
				for (int i = 0; i < mLockType.size(); i++) {
					if (mLockType.get(i).getName().equals(item.getType()))
						spinnerLockStyle.setSelection(i);
				}

				// 锁类型设置选择事件
				spinnerLockStyle
						.setOnItemSelectedListener(new LockOnItemSelectedListener(
								position));

			}

		};
		mlistView.setAdapter(mAdapter);

	}

	/**
	 * 更新锁数量
	 * 
	 * @param locket
	 * @param position
	 */
	private void updataLocketAdapter() {
		if (mAdapter != null) {
			// 用于标识加载锁类型时不触发选择事件
			flag = true;
			size = 1;
			mAdapter.notifyDataSetChanged();// 更新适配器显示数据

		}
	}

	/**
	 * 锁类型选择事件
	 * 
	 * @author luoyilong
	 *
	 */
	private class LockOnItemSelectedListener implements OnItemSelectedListener {

		int position;

		public LockOnItemSelectedListener(int positions) {
			position = positions;
		}

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {

			if (flag) {// 第一次加载
				if (size < mAdapter.getCount()) {
					size++;
				} else {
					flag = false;
				}

			} else {// 选择事件
				Dict type = (Dict) arg0.getItemAtPosition(arg2);
				mListlock.get(position).setType(type.getName());
				updataLocketAdapter();
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			spinnerLockStyle.setSelection(0);
		}

	}

	// 蓝牙搜索回调方法
	private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi,
				final byte[] scanRecord) {
			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// 搜索蓝牙结果
					String address = device.getAddress().replace(":", "");
					// 屏蔽显示智能钥匙
					Smartkey btKey = DBHelp.getInstance(mActivity).getEkeyDao()
							.findFirstBtKey(address);

					if (btKey == null && !btAdapter.getDatas().contains(device))
						btAdapter.addData(device);

				}
			});
		}
	};

	/**
	 * 获取所属范围
	 */
	private void initscope() {

		// 获取开锁范围
		mScope = DBHelp.getInstance(mActivity).getEkeyDao().getZone();

		if (mScope == null)
			mScope = new ArrayList<Zone>();

		// 初始化适配器
		mscopeAdapter = new ContronAdapter<Zone>(mActivity,
				android.R.layout.simple_spinner_item, mScope) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder, Zone item,
					int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};

		mscopeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerScope.setAdapter(mscopeAdapter);
//		if(mZone != null){
//			mSpinnerScope.setSelection(mScope.indexOf(mZone));
//		}else{
//			mSpinnerScope.setSelection(0);
//		}
		mSpinnerScope.setSelection(0);
		mSpinnerScope.setOnItemSelectedListener(new DeviceScope());

	}

	/**
	 * 设备范围选择事件
	 * 
	 * @author luoyilong
	 *
	 */
	private class DeviceScope implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			mZone = (Zone) arg0.getItemAtPosition(arg2);
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}

	}

	/**
	 * 获取运营商
	 */
	private void initCarrier() {

		// 获取开锁范围
		mCarrierType = DBHelp.getInstance(mActivity).getEkeyDao()
				.getDicType("carrier");

		if (mCarrierType == null)
			mCarrierType = new ArrayList<Dict>();

		for(int i=0;i<mCarrierType.size();i++){
			mCarrierTypeStr.add( mCarrierType.get(i).getName());
		}
		// 初始化适配器
		carrierAdapter = new ContronAdapter<String>(mActivity,
				android.R.layout.simple_spinner_item, mCarrierTypeStr) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder, String item,
					int position) {
				viewHolder.setText(android.R.id.text1, item);
			}
		};

		carrierAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerCarrier.setAdapter(carrierAdapter);
		if(carrierType != null){
			mSpinnerCarrier.setSelection(mCarrierType.indexOf(carrierType));
		}else{
			mSpinnerCarrier.setSelection(0);
		}
		mSpinnerCarrier.setOnItemSelectedListener(new Carrier());
	

	}

	/**
	 * 运营商选择事件
	 * 
	 * @author luoyilong
	 *
	 */
	private class Carrier implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			carrierType = (String) arg0.getItemAtPosition(arg2);
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}

	}

	/**
	 * 获取区域信息填充到控件
	 */
	private void initSection() {
		// 从数据库中读取区域信息
		sections = DBHelp.getInstance(mActivity).getEkeyDao().getSection();

		if (sections == null)
			sections = new ArrayList<Section>();

		for(int i=0; i<sections.size();i++){
			sectionsStr.add(sections.get(i).getName());
		}
		// 初始化适配器
		spinnerAdapter = new ContronAdapter<Section>(mActivity,
				android.R.layout.simple_spinner_item, sections) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					Section item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};

		spinnerAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerDepartment.setAdapter(spinnerAdapter);
//		if(deviceDepartment != null){
//			mSpinnerDepartment.setSelection(sectionsStr.indexOf(deviceDepartment));
//		}else{
//			mSpinnerDepartment.setSelection(0);
//		}
		mSpinnerDepartment.setSelection(0);
		mSpinnerDepartment
				.setOnItemSelectedListener(new DepartmentSelectLisener());
	}

	/**
	 * 区域选择事件
	 * 
	 * @author luoyilong
	 *
	 */
	private class DepartmentSelectLisener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			deviceDepartment = (Section) arg0.getItemAtPosition(arg2);
			deviceDepartmentStr = deviceDepartment.getName();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			// spinnerDepartment.setSelection(0);
		}

	}

	/**
	 * 初始化锁类型适配器
	 */
	private void initLocketType() {

		mLockType = DBHelp.getInstance(mActivity).getEkeyDao()
				.getDicType("lock_type");

		if (mLockType == null)
			mLockType = new ArrayList<Dict>();

		locketAdapter = new ContronAdapter<Dict>(mActivity,
				android.R.layout.simple_spinner_item, mLockType) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder, Dict item,
					int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};

		locketAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	}

	/**
	 * 初始化蓝牙钥匙适配器
	 */
	private void initBluetoothDevice() {

		btAdapter = new ContronAdapter<BluetoothDevice>(mActivity,
				android.R.layout.simple_list_item_2,
				new ArrayList<BluetoothDevice>()) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					BluetoothDevice item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName()).setText(
						android.R.id.text2, item.getAddress());
			}
		};
	}

	/**
	 * 初始化蓝牙钥匙弹出对话框
	 */
	private void initMsgBoxFragment() {

		btDeviceDialog = new MsgBoxFragment();
		btDeviceDialog.setCancelable(false);
		btDeviceDialog
				.setNegativeOnClickListener(new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO 取消搜索
						mBtMsgService.getBluetoothAdapter().stopLeScan(
								leScanCallback);
						btDeviceDialog.dismiss();
					}
				});
		btDeviceDialog.setTitle(mActivity.getResources().getString(
				R.string.main_bluetooth_menu));
		btDeviceDialog.setAdapter(btAdapter,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO 选择蓝牙
						String btaddr = btAdapter.getDatas().get(which)
								.getAddress().replace(":", "");

						selectPosition = mAdapter.getSelectedPosition();
						if (selectPosition == -1)
							selectPosition = 0;

						mListlock.get(selectPosition).setBtaddr(
								btaddr.toUpperCase());

						updataLocketAdapter();

						mBtMsgService.getBluetoothAdapter().stopLeScan(
								leScanCallback);
						btDeviceDialog.dismiss();
					}
				});
	}

	/**
	 * 初始化设备适配器
	 */
	private void initDeviceStyle() {

		// 请求获取设备類型

		mObjectType = DBHelp.getInstance(mActivity).getEkeyDao()
				.getDicType("obj_type");
		if (mObjectType == null)
			mObjectType = new ArrayList<Dict>();

		for(int i=0;i<mObjectType.size();i++){
			String test = mObjectType.get(i).getName();
			mObjectTypeStr.add(test);
		}
		
		objectTypeAdapter = new ContronAdapter<String>(mActivity,
				android.R.layout.simple_spinner_item, mObjectTypeStr) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder, String item,
					int position) {
				viewHolder.setText(android.R.id.text1, item);
			}
		};
		objectTypeAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinnerObjectType.setAdapter(objectTypeAdapter);
		if(deviceType != null){
			mSpinnerObjectType.setSelection(mObjectTypeStr.indexOf(deviceType));
		}else{
			mSpinnerObjectType.setSelection(0);
		}
		mSpinnerObjectType
				.setOnItemSelectedListener(new ProvOnItemSelectedListener());
	}

	/**
	 * 设备类型选择事件
	 * 
	 * @author luoyilong
	 *
	 */
	private class ProvOnItemSelectedListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			deviceType = (String) arg0.getItemAtPosition(arg2);
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}

	}

	/**
	 * 返回事件
	 */
	private void onBack() {
		EkeyAPP.getInstance().setBtAddress("");
		if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
			mActivity.getSupportFragmentManager().popBackStack();
		else
			mActivity.finish();
	}

	@OnClick({ R.id.match_btn, R.id.but_lnglat, R.id.but_search , R.id.save_button})
	public void OnClick(View v) {
		switch (v.getId()) {
		case R.id.match_btn:// 增加锁具
			String name = deviceName.getText().toString().trim();
			int size = mAdapter.getCount();
			if (TextUtils.isEmpty(name)) {// 设备名称为空时
				mActivity.showToast("请输入设备名称！");
			} else if (size == 20) {
				mActivity.showToast("一个设备下最多可添加20把锁!");
			} else {
				Lockset locker = new Lockset();
				// 设置默认锁类型
				if (mLockType != null && mLockType.size() > 0)
					locker.setType(mLockType.get(0).getName());
				mListlock.add(locker);
				updataLocketAdapter();
				mAdapter.setSelectedPosition(mAdapter.getSelectedPosition() + 1);
			}
			break;

		case R.id.but_lnglat:
			CollectActivity.startActivityForResult(mFragment);
			break;
		case R.id.but_search:{
			
			  String searchname = searchName.getText().toString().trim();
			  if (TextUtils.isEmpty(searchname)) {// 设备名称为空时
				  mActivity.showToast("请输入设备名称！");
				 return; 
			  }
			  isModify = true;
			 
			  searchDevece();
			 
		    }
			break;
		case R.id.save_button:{
			
			String device = deviceName.getText().toString().trim();
			if (deviceDepartment == null) {
				mActivity.showToast("请选择设备所属区域！");
				return;
			}

			if (carrierType == null) {
				mActivity.showToast("请选择云营商！");
				return;
			}

			if (deviceType == null) {
				mActivity.showToast("请选择设备所属类型！");
				return;
			}

			if (TextUtils.isEmpty(device)) {
				mActivity.showToast("设备名称不能为空！");
				return;
			}


			mActivity.startDialog(R.string.progressDialog_title_submit);
			RequestParams params = new RequestParams(mActivity.getUser());
			params.putParams("name", device);
			params.putParams("type", deviceType);
			params.putParams("carrier", carrierType);
			params.putParams("area", deviceDepartment.getName());
			
			
			if (latlng != null) {// 设备坐标不为null是上 传
				params.putParams("lat", latlng.latitude);
				params.putParams("lng", latlng.longitude);
			}

			int sid = modifyDevice.getId();
			String url = String.format(Globals.OBJECT_OID_UPDATE, sid);
			requestHttp.doPost(url, params, new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) { // 上传成功则清掉数据
									mActivity.showToast("上传成功");
//									deviceName.setText("");
//									searchName.setText("");
//									mAdapter.clearDatas();
//									mAdapter.setSelectedPosition(-1);
//									isModify = false;
//									saveBtn.setVisibility(View.GONE);
//
//									mSpinnerScope.setVisibility(View.VISIBLE);//mSpinnerScope
//									mLabelScope.setVisibility(View.VISIBLE);
//									latlng = null;
//									but_lnglat.setText("采集设备坐标");
								} else {// 返回失败
									String error = "";
									try {
										JSONObject object = new JSONObject(value);
										error = object.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}

							}
					});
				}
			});
		}
		    break;
		default:
			break;
		}
	}
	
	/*
	 * 显示查询出来的设备
	 */

	private void searchDevece(){
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("page", -1);
		params.putParams("search", searchName.getText().toString().trim());
//		params.putParams("section", 0);

		// 请求接口
		String url = String.format(Globals.SECTION_DEPARTMENTID_OBJECT, 0);
		requestHttp.doPost(url, params, new HttpRequestCallBackString() {
			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mActivity.dismissDialog();
						if (value.contains("success")) { // 上传成功则清掉数据
							devices = JsonTools.getObjects(value);
							initDeviceChoseDialog();
							initDeviceChoseBoxFragment();						
						} else {// 返回失败
							String error = "";
							try {
								JSONObject object = new JSONObject(value);
								error = object.getString("error");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							mActivity.showToast(error);
						}

					}
				});
			}
		});
	}
	
	/**
	 * 初始化设备选择适配器
	 */
	private void initDeviceChoseDialog() {

		deviceAdapter = new ContronAdapter<DeviceObject>(mActivity,
				android.R.layout.simple_list_item_1,
				devices) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					DeviceObject item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
	}
	/**
	 * 初始化蓝牙钥匙弹出对话框
	 */
	private void initDeviceChoseBoxFragment() {

		deviceDialog = new MsgBoxFragment();
		deviceDialog.setCancelable(false);
		deviceDialog
				.setNegativeOnClickListener(new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO 取消搜索	
						deviceDialog.dismiss();
					}
				});
		deviceDialog.setTitle(mActivity.getResources().getString(
				R.string.modify_device_dialog_title));
		deviceDialog.setAdapter(deviceAdapter,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO 选择蓝牙
						modifyDevice = devices.get(which);
						refreshDisplay();
						
						deviceDialog.dismiss();
					}
				});
		deviceDialog.show(
				mActivity.getFragmentManager(), "device");
	}
	
	/*
	 * 
	 * 显示要修改设备的信息
	 * 
	 */
	private void refreshDisplay(){
		
		saveBtn.setVisibility(View.VISIBLE);
		mSpinnerScope.setVisibility(View.GONE);//mSpinnerScope
		mLabelScope.setVisibility(View.GONE);
		
		deviceDepartmentStr = modifyDevice.getArea();
		mSpinnerDepartment.setSelection(sections.indexOf(sectionsStr.indexOf(deviceDepartmentStr)));

		deviceType = modifyDevice.getType();
		mSpinnerObjectType.setSelection(mObjectTypeStr.indexOf(deviceType));
		
		carrierType = modifyDevice.getCarrier();
		mSpinnerCarrier.setSelection(mCarrierTypeStr.indexOf(carrierType));
		
		deviceName.setText(modifyDevice.getName());
		mListlock = modifyDevice.getLockset();
		initAddLocketAdapter();
	}
	
	
	/**
	 * 蓝牙选择回调
	 */
	protected void onSelectBt(String address) {
		// toggleBtnExecute(address);
		scanLeDevice(false);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.submit_seek, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBack();
			break;
		case R.id.menu_submit:// 保存
			searchName.setFocusable(true);
			searchName.setFocusableInTouchMode(true);
			searchName.requestFocus();
			searchName.findFocus();

			if(isModify){
				submitCollectTicket(); // 上传设备
				
			}else{
				if (latlng == null) {
					mActivity.showMsgBox("提示", "现在采集当前设备坐标吗？", false,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									CollectActivity
											.startActivityForResult(mFragment);// 跳转到采集设备坐标界面
								}
							}, new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									submitCollectTicket();// 上传设备【无设备坐标】
								}
							});

				} else {
					submitCollectTicket(); // 上传设备
				}
			}
			

			break;
		case R.id.menu_seek:// 搜索钥匙

			if (mBtMsgService != null) {
				mBtMsgService.disconnect();
				if (!mBtMsgService.isConnected())
					mBtMsgService.removeCallbacksConn();
			}
			scanLeDevice(true);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 提交保存锁信息
	 */
	private void submitCollectTicket() {
		if(!isModify){
			if (mZone == null) {
				mActivity.showToast("请选择设备所属开锁范围！");
				return;
			}
			if (deviceDepartment == null) {
				mActivity.showToast("请选择设备所属区域！");
				return;
			}
		}
		String device = deviceName.getText().toString().trim();


		if (carrierType == null) {
			mActivity.showToast("请选择云营商！");
			return;
		}

		if (deviceType == null) {
			mActivity.showToast("请选择设备所属类型！");
			return;
		}

		if (TextUtils.isEmpty(device)) {
			mActivity.showToast("设备名称不能为空！");
			return;
		}

		int lockestIndex = 0;
		for (Lockset lockset : mListlock) {
			lockestIndex++;
			// 判断锁名称
			if (TextUtils.isEmpty(lockset.getName().trim())) {
				mActivity.showToast("新增锁" + lockestIndex + ":锁名称不能为空！");
				return;
			}

			String rfid = lockset.getRfid().trim();
			// 判断锁RFID16进制数据
			if (!TextUtils.isEmpty(rfid)
					&& (rfid.length() != 10 || !judge16(rfid))) {
				mActivity
						.showToast("新增锁" + lockestIndex + ":请输入10位十六进制锁RFID码!");
				return;
			}

			String addbt = lockset.getBtaddr().trim();
			// 判断锁蓝牙地址
			if (!TextUtils.isEmpty(addbt)
					&& (addbt.length() != 12 || !judge16(addbt))) {
				mActivity.showToast("新增锁" + lockestIndex + ":请输入12位十六进制锁蓝牙地址!");
				return;
			}
		}

		mActivity.startDialog(R.string.progressDialog_title_submit);
		String lockset = mgson.toJson(mListlock);
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("name", device);
		params.putParams("type", deviceType);
		params.putParams("carrier", carrierType);
		params.putParams("area", deviceDepartment.getName());
		
		if(!isModify){
			params.putParams("zid", mZone.getId());	
		}
		if (latlng != null) {// 设备坐标不为null是上 传
			params.putParams("lat", latlng.latitude);
			params.putParams("lng", latlng.longitude);
		}

		try {
			JSONArray locksetArray = new JSONArray(lockset);
			params.putParams("lockset", locksetArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if(isModify){
			mActivity.dismissDialog();
			mActivity.showToast("保存成功");
			deviceName.setText("");
			searchName.setText("");
			mAdapter.clearDatas();
			mAdapter.setSelectedPosition(-1);
			isModify = false;
			saveBtn.setVisibility(View.GONE);

			mSpinnerScope.setVisibility(View.VISIBLE);//mSpinnerScope
			mLabelScope.setVisibility(View.VISIBLE);
			latlng = null;
			but_lnglat.setText("采集设备坐标");
//			int sid = modifyDevice.getId();
//			String url = String.format(Globals.OBJECT_OID_UPDATE, sid);
//			requestHttp.doPost(url, params, new HttpRequestCallBackString() {
//				@Override
//				public void resultValue(final String value) {
//					mActivity.runOnUiThread(new Runnable() {
//						@Override
//						public void run() {
//							mActivity.dismissDialog();
//							if (value.contains("success")) { // 上传成功则清掉数据
//								mActivity.showToast("上传成功");
//								deviceName.setText("");
//								searchName.setText("");
//								mAdapter.clearDatas();
//								mAdapter.setSelectedPosition(-1);
//								isModify = false;
//
//								mSpinnerScope.setVisibility(View.VISIBLE);//mSpinnerScope
//								mLabelScope.setVisibility(View.VISIBLE);
//								latlng = null;
//								but_lnglat.setText("采集设备坐标");
//							} else {// 返回失败
//								String error = "";
//								try {
//									JSONObject object = new JSONObject(value);
//									error = object.getString("error");
//								} catch (JSONException e) {
//									e.printStackTrace();
//								}
//								mActivity.showToast(error);
//							}
//
//						}
//					});
//				}
//			});
		}else{
			int sid = deviceDepartment.getId();
			String url = String.format(Globals.SECTION_SID_OBJECT_NEW, sid);
			requestHttp.doPost(url, params, new HttpRequestCallBackString() {
				@Override
				public void resultValue(final String value) {
					mActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							mActivity.dismissDialog();
							if (value.contains("success")) { // 上传成功则清掉数据
								mActivity.showToast("上传成功");
								deviceName.setText("");
								mAdapter.clearDatas();
								mAdapter.setSelectedPosition(-1);

								latlng = null;
								but_lnglat.setText("采集设备坐标");
							} else {// 返回失败
								String error = "";
								try {
									JSONObject object = new JSONObject(value);
									error = object.getString("error");
								} catch (JSONException e) {
									e.printStackTrace();
								}
								mActivity.showToast(error);
							}

						}
					});
				}
			});
		}
		
		

	}

	

	/**
	 * 接收BtMsgService发出的状态消息<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtState state) {
		switch (state) {
		case CONNECTING:// 连接中
			mActivity.getActionBar().setTitle(
					getString(R.string.mannul_collect_ticket_title,
							getString(R.string.btstate_conning)));
			mActivity.invalidateOptionsMenu();
			// toggleBtnExecute(""); // 断开后选择钥匙
			break;
		case DISCONNECTED:// 连接断开
			mActivity.getActionBar().setTitle(
					getString(R.string.mannul_collect_ticket_title,
							getString(R.string.btstate_disconn)));
			mActivity.invalidateOptionsMenu();
			// toggleBtnExecute(""); // 断开后选择钥匙
			break;
		case CONNECTED:// 连接成功
			mActivity.invalidateOptionsMenu();
			mActivity.getActionBar().setTitle(
					getString(R.string.mannul_collect_ticket_title,
							getString(R.string.btstate_conned)));
			// toggleBtnExecute(EkeyAPP.getInstance().getBtAddress()); //
			// 断开后选择钥匙
			break;
		case CONNECTOUTTIME:// 连接超时
			mActivity.dismissDialog();
			showMsgBox("蓝牙连接超时,请重试!");
			break;
		case DISCOVERED:
			break;
		default:
			break;
		}
	}

	/**
	 * 接收BtMsgService发出的蓝牙规约数据<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午10:25:18
	 * @param btHex
	 */
	public void onEventMainThread(BtHex btHex) {
		final String read = btHex.commd;
		if (!TextUtils.isEmpty(read)) {
			BtRecvMsg btRecvMsg = BtMsgTools.recv(read);
			if (btRecvMsg != null) {
				if (btRecvMsg instanceof BtRecvEmpowerMsg) {// 接收授权记录

					selectPosition = mAdapter.getSelectedPosition();
					if (selectPosition == -1)
						selectPosition = 0;

					mListlock.get(selectPosition).setRfid(
							btRecvMsg.data.toUpperCase());

					updataLocketAdapter();

				} else {// 数据响应不匹配
					mActivity.showMsgBox(R.string.shared_bt_error);
				}
			} else {// 数据解析失败
				mActivity.showMsgBox(R.string.shared_bt_error1);
			}
		} else {// 接收数据为空
			mActivity.showMsgBox(R.string.shared_bt_error2);
		}
	}

	/**
	 * 判断是否为16进制
	 * 
	 * @param str
	 * @return true 16进制;false非16进制
	 */
	private boolean judge16(String str) {

		if (TextUtils.isEmpty(str))
			return false;

		for (int i = 0; i < str.length(); i++) {
			int temp = (int) str.charAt(i);

			if (temp > 47 && temp < 58) {// 0~9 的ASCII 是 48~57

			} else if (temp > 64 && temp < 71) { // A~F 的ASCII 是 65~70

			} else {
				return false;
			}
		}

		return true;

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == CollectActivity.REQUESTCODE
				&& resultCode == CollectActivity.RESULTCODE && data != null) {
			latlng = data.getParcelableExtra("latlng");
			if (latlng != null) {
				but_lnglat.setText("经度：" + latlng.longitude + " 纬度："
						+ latlng.latitude);
			}
		}

	}

	/**
	 * 弹出提示框
	 */
	public void showMsgBox(String msg) {
		mActivity.showMsgBox("提示", msg, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
	}
}
