package com.contron.ekeyapptx.qrscanner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;

import com.contron.ekeyapptx.R;
import com.google.zxing.Result;
import com.mylhyl.zxing.scanner.ScannerView;
import com.mylhyl.zxing.scanner.common.Scanner;

public class ScannerActivity extends BasicScanActivity {

    public static final String EXTRA_LASER_LINE_MODE = "extra_laser_line_mode";
    public static final String EXTRA_SCAN_MODE = "extra_scan_mode";
    public static final String EXTRA_SHOW_THUMBNAIL = "EXTRA_SHOW_THUMBNAIL";
    public static final String EXTRA_SCAN_FULL_SCREEN = "EXTRA_SCAN_FULL_SCREEN";
    public static final String EXTRA_HIDE_LASER_FRAME = "EXTRA_HIDE_LASER_FRAME";

    private ScannerView mScannerView;
    private ImageButton mFlashLightBtn;
    private Result mLastResult;
    private boolean lightStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        lightStatus = false;
        mScannerView = (ScannerView) findViewById(R.id.scanner_view);
        mScannerView.setOnScannerCompletionListener(this);
        mFlashLightBtn = (ImageButton) findViewById(R.id.flashLightBtn);
        mFlashLightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lightStatus = !lightStatus;
                mScannerView.toggleLight(lightStatus);
            }
        });
//        mScannerView.setScanMode(Scanner.ScanMode.QR_CODE_MODE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (mLastResult != null) {
                    restartPreviewAfterDelay(0L);
                    return true;
                }
                break;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void restartPreviewAfterDelay(long delayMS) {
        mScannerView.restartPreviewAfterDelay(delayMS);
        resetStatusView();
    }

    private void resetStatusView() {
        mLastResult = null;
    }

    @Override
    protected void onResume() {
        mScannerView.onResume();
        resetStatusView();
        super.onResume();
    }

    @Override
    protected void onPause() {
        mScannerView.onPause();
        super.onPause();
    }

    public static void gotoActivity(Activity activity, boolean isBackResult
            , int laserMode, int scanMode, boolean showThumbnail, boolean isScanFullScreen
            , boolean isHideLaserFrame) {
        activity.startActivityForResult(new Intent(Scanner.Scan.ACTION)
                        .putExtra(BasicScanActivity.EXTRA_RETURN_SCANNER_RESULT, isBackResult)
                        .putExtra(ScannerActivity.EXTRA_LASER_LINE_MODE, laserMode)
                        .putExtra(ScannerActivity.EXTRA_SCAN_MODE, scanMode)
                        .putExtra(ScannerActivity.EXTRA_SHOW_THUMBNAIL, showThumbnail)
                        .putExtra(ScannerActivity.EXTRA_SCAN_FULL_SCREEN, isScanFullScreen)
                        .putExtra(ScannerActivity.EXTRA_HIDE_LASER_FRAME, isHideLaserFrame)
                , BasicScanActivity.REQUEST_CODE_SCANNER);
    }
}
