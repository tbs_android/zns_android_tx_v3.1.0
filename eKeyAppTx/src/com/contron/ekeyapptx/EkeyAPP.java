/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.InitializeData;
import com.contron.ekeypublic.entities.Offline;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.WarnMsg;
import com.contron.ekeypublic.util.AppUtils;
import com.contron.ekeypublic.util.ConfigUtil;
import com.lidroid.xutils.util.LogUtils;
import com.qihoo.linker.logcollector.LogCollector;

public class EkeyAPP extends Application {

	// 首次初始化
	public static final String KEY_FIRST_INIT_DATA = "key_first_initData";
	private Context context;
	private TempTask tempTask = null;
	private Offline offline = null;// 离线鉴权任务
	private RecLenEventListener mRecLenEventListener;
	private Timer mTimer = null;
	private TimerTask mTimerTask = null;
	private static EkeyAPP instance = null;
	private static final int TIMER = 1;
	private Stack<Activity> mListActivity = new Stack<Activity>();
	private int recLen = 0;// 注销计时器标示，单位秒
	private String btAddress;// 选择后的蓝牙地址
	private ConfigUtil configUtil;
	private int count = 0;// 告警条数
	private List<WarnMsg> listWarnMsg = new ArrayList<WarnMsg>();// 告警数据

	public interface RecLenEventListener {
		void onTimer();
	}

	@SuppressLint("HandlerLeak")
	private final Handler mReclenHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case TIMER:
				if (mRecLenEventListener != null)
					mRecLenEventListener.onTimer();
				break;

			default:
				break;
			}
		}
	};

	public static EkeyAPP getInstance() {
		if (instance == null)
			instance = new EkeyAPP();
		return instance;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();
		instance = this;
		if ("com.contron.ekeycommunication".equals(AppUtils
				.getCurProcessName(getApplicationContext()))) {
			LogUtils.customTagPrefix = getString(R.string.app_name);
			LogCollector.setDebugMode(true);
			LogCollector.init(getApplicationContext(), null, null);
		}

	}

	public String getBtAddress() {
		return btAddress;
	}

	public void setBtAddress(String btAddress) {
		this.btAddress = btAddress;
	}

	/**
	 * 取使用模式的状态
	 * 
	 * @return the isChoiceMode
	 */
	public boolean isChoiceMode() {

		return ConfigUtil.getInstance(this).getBoolean("isChoiceMode");
	}

	/**
	 * 设置登陆状态
	 * 
	 * @param isChoiceMode
	 *            the isLoginStatus to set
	 */
	public void setChoiceMode(boolean isChoiceMode) {
		ConfigUtil.getInstance(this).setBoolean("isChoiceMode", isChoiceMode);
	}

	/**
	 * 自动注销事件
	 * 
	 * @param mAppEventListener
	 *            the mAppEventListener to set
	 */
	public void startTimeTask(RecLenEventListener mAppEventListener) {
		this.mRecLenEventListener = mAppEventListener;
		stopTimeTask();
		mTimer = new Timer();
		mTimerTask = new TimerTask() {

			@Override
			public void run() {
				mReclenHandler.sendEmptyMessage(TIMER);
			}
		};
		// 1秒后开始，每1秒执行一次
		mTimer.schedule(mTimerTask, 1000, 1000);
	}

	private void stopTimeTask() {
		if (mTimer != null) {
			mTimer.cancel();
			mTimer = null;
		}
		if (mTimerTask != null) {
			mTimerTask.cancel();
			mTimerTask = null;
		}
	}

	public Offline getOffline() {
		return offline;
	}

	public void setOffline(Offline offline) {
		this.offline = offline;
	}

	public void addActivity(Activity activity) {
		mListActivity.add(activity);
	}

	public void removeActivity(Activity activity) {
		if (activity != null)
			mListActivity.remove(activity);
	}

	/**
	 * 结束指定类名的Activity
	 */
	public void finishActivity(Class<?> cls) {
		for (Activity activity : mListActivity) {
			if (activity.getClass().equals(cls)) {
				finishActivity(activity);
			}
		}
	}

	/**
	 * 结束指定的Activity
	 */
	public void finishActivity(Activity activity) {
		if (activity != null) {
			removeActivity(activity);
			activity.finish();
			activity = null;
		}
	}

	/**
	 * 取登陆状态
	 * 
	 * @return the isLoginStatus
	 */
	public boolean isLoginStatus() {
		return ConfigUtil.getInstance(this).getBoolean("isLoginStatus");
	}

	/**
	 * 设置登陆状态
	 * 
	 * @param isLoginStatus
	 *            the isLoginStatus to set
	 */
	public void setLoginStatus(boolean isLoginStatus) {
		ConfigUtil.getInstance(this).setBoolean("isLoginStatus", isLoginStatus);
	}

	public User getUser() {
		ConfigUtil configUtil = ConfigUtil.getInstance(this);
		return configUtil.getObject("user", User.class);
	}

	public void setUser(User user) {
		if (user != null) {
			ConfigUtil configUtil = ConfigUtil.getInstance(this);
			configUtil.setObject("user", user);
		}
	}

	public int getRecLen() {
		return recLen;
	}

	/**
	 * 清除当前注销的时间
	 */
	public void clearRecLen() {
		this.recLen = 0;
	}

	/**
	 * 更新自动注销的时间
	 * 
	 * @author hupei
	 * @date 2015�?4�?1�? 上午10:28:33
	 */
	public void updateRecLen() {
		recLen++;
	}

	// /**
	// * 获取Gps的LocationClient对像
	// *
	// * @return
	// */
	// public LocationClient getGps(Context context) {
	// if (mLocationClient == null) {
	// mLocationClient = new LocationClient(context);
	// }
	// return mLocationClient;
	// }

	/**
	 * 注销用户
	 * 
	 * @author hupei
	 * @date 2015年4月1日 上午10:28:33
	 */
	public void exitRecLen() {
		for (final Activity activity : mListActivity) {
			activity.finish();
		}
		mListActivity.clear();
		this.mRecLenEventListener = null;
		stopTimeTask();
		clearRecLen();
		setLoginStatus(false);
		restart();
	}

	/**
	 * 获取web服务地址
	 * 
	 * @return
	 */
	public String getWLD() {
		String address = "";
		ConfigUtil configUtil = ConfigUtil.getInstance(getContext());
		if (TextUtils.isEmpty(address))
			address = configUtil.getString(Globals.KEY_WSDL_ADDRESS);
		return address;
	}

	/**
	 * 获取socket服务地址
	 * 
	 * @return
	 */
	public String getSocketAddress() {
		String socketAddress = "";
		ConfigUtil configUtil = ConfigUtil.getInstance(getContext());
		if (TextUtils.isEmpty(socketAddress))
			socketAddress = configUtil
					.getString(Globals.KEY_SOCKET_ADDRESS);
		return socketAddress;
	}

	/**
	 * 退出APP
	 * 
	 * @author hupei
	 * @date 2015�?4�?1�? 下午1:43:19
	 */
	public void exitApp() {
		android.os.Process.killProcess(android.os.Process.myPid());
		System.exit(0);
	}

	public TempTask getTempTask() {
		return tempTask;
	}

	public void setTempTask(TempTask tempTask) {
		this.tempTask = tempTask;
	}

	/**
	 * 获取报警信息条数
	 * 
	 * @return
	 */
	public int getWarnCount() {
		return count;
	}

	/**
	 * 保存报警信息条数
	 * 
	 * @param warnmsg
	 */
	public void setWarnCount(int warnmsg) {
		count = warnmsg;
	}

	/**
	 * 获取报警信息
	 * 
	 * @return
	 */
	public List<WarnMsg> getListWarnMsg() {
		return listWarnMsg;
	}

	/**
	 * 设置报警信息
	 * 
	 * @return
	 */
	public void setListWarnMsg(WarnMsg warnMsg) {
		listWarnMsg.add(warnMsg);
	}

	/**
	 * 下载基础数据
	 * 
	 * @param mActivity
	 * @param dialogState
	 *            true显示提示框,false不显示提示框
	 * @param initDataState
	 *            true下载基础数据
	 * @param OfflineState
	 *            true 时 1上传操作记录，2下载离线任务
	 * 
	 * @return 无网络直接返回
	 */
	public synchronized void initData(BasicActivity activity,
			boolean dialogState, boolean initDataState, boolean OfflineState) {

		User user = getUser();
		// 无网络直接返回
		if (!AppUtils.isNetworkAvailable(activity)) {
			activity.showToast("请检查网络连接是否正常！");
			return;
		}

		// 实例化InitializeData对象
		InitializeData initializeData = new InitializeData(activity, user,
				getWLD(), dialogState);

		if (initDataState) {
			initializeData.InitData();// 下载初始化数据			
		} else {
			configUtil = ConfigUtil.getInstance(activity);
			String time = configUtil.getString("updatetime");// 获取更新时间	
			initializeData.autoUpdateDict(time);//下载更新基础数据
		}

		if (OfflineState)
			initializeData.Upload();// 上传离线操作开锁记录，下载离线鉴权
	}

	public Context getContext() {
		return context;
	}

	/**
	 * @return 上次登录用户与本次登录用户不一致返回true
	 */
	public boolean getOlduser() {
		return ConfigUtil.getInstance(this).getBoolean("olduser");
	}

	/**
	 * 当前登录账号与上次登录是否相同 true为相同
	 * 
	 * @param olduser
	 */
	public void setOlduser(boolean olduser) {
		ConfigUtil.getInstance(this).setBoolean("olduser", olduser);
	}

	/**
	 * 是否CYG_SCDZCT
	 * @return
	 */
	public boolean isSCLock() {
		return ConfigUtil.getInstance(this).getBoolean("scLock");
	}

	/**
	 * 设置是否CYG_SCDZCT
	 * @param scLock
	 */
	public void setSCLock(boolean scLock) {
		ConfigUtil.getInstance(this).setBoolean("scLock", scLock);
	}

	/**
	 * 重启程序
	 */
	private void restart() {
		Intent intent = getBaseContext().getPackageManager()
				.getLaunchIntentForPackage(getBaseContext().getPackageName());
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
}
