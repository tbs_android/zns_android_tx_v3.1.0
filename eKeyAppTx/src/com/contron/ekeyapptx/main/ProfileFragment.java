package com.contron.ekeyapptx.main;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeyapptx.key.InitializeKeyActivity;
import com.contron.ekeyapptx.PickLocationActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.entities.User;

import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class ProfileFragment extends CommonFragment implements OnItemClickListener {

	@Override
	protected ContronAdapter<Function> getAdapter(User user) {
		int itemLayoutId = R.layout.activity_function_item;
		List<Function> listData = getListData(user);
		mAdapter = new ContronAdapter<Function>(getContext(), itemLayoutId, listData) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder, Function item, int position) {
				TextView tvName = viewHolder.findView(R.id.function_item_tv_name);
				tvName.setText(item.nameResId);
				tvName.setCompoundDrawablesWithIntrinsicBounds(0, item.icoResId, 0, 0);
				TextView tvtag = viewHolder.findView(R.id.tx_tag);
				if (item.nameResId == R.string.main_tab_item34_name) {
					MainActivity mainActivity = (MainActivity) getActivity();
					if (!TextUtils.isEmpty(mainActivity.mNoticeMsg)) {
						setTag(tvtag);
					}
				}
			}
		};
		mGridView.setOnItemClickListener(this);
		return mAdapter;
	}

	private void setTag(TextView tvtag) {
		tvtag.setVisibility(View.VISIBLE);
		tvtag.setText("News");
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		int functionId = mAdapter.getItem(position).nameResId;
		switch (functionId) {
			case R.string.main_tab_item17_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_17);
				break;
			case R.string.main_tab_item19_name:
				PickLocationActivity.gotoActivity(getActivity());
				break;
			case R.string.main_tab_item21_name:
				InitializeKeyActivity.gotoActivity(getActivity());
				break;
			case R.string.main_tab_item22_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_22);
				break;
			case R.string.main_tab_item2_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_4);
				break;
			case R.string.main_tab_item34_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_34);
				break;
			case R.string.main_tab_item35_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_35);
				break;
			case R.string.main_tab_item32_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_32);
				break;
		}
	}

	private List<Function> getListData(User user) {
		List<Function> functions = new ArrayList<Function>();
		// 锁具采码
		functions.add(new Function(R.drawable.ic_collect, R.string.main_tab_item17_name));
		// 设备坐标采集
		functions.add(new Function(R.drawable.latitude, R.string.main_tab_item19_name));
		// 通讯录
		functions.add(new Function(R.drawable.jilu, R.string.main_tab_item32_name));
		// 初始化钥匙
		functions.add(new Function(R.drawable.keypackage, R.string.main_tab_item21_name));
		// 回传操作记录
		functions.add(new Function(R.drawable.keyback, R.string.main_tab_item22_name));
		// 系统设置
		functions.add(new Function(R.drawable.ic_setting, R.string.main_tab_item2_name));

		// 公告
		functions.add(new Function(R.drawable.jilu, R.string.main_tab_item34_name));

		// 账号信息
		functions.add(new Function(R.drawable.jilu, R.string.main_tab_item35_name));
		return functions;
	}

}
