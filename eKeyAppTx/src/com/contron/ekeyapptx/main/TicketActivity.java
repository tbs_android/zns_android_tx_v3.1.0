/*
 * Copyright (c) 2015. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx.main;

import com.contron.ekeyapptx.AuditListFragment;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.HistoryFragment;
import com.contron.ekeyapptx.ManuCollectTicketFragment;
import com.contron.ekeyapptx.fsu.RemoteUnlockManageFragment;
import com.contron.ekeyapptx.map.StationNavigationFragment;
import com.contron.ekeyapptx.OperateSelectFragment;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.SettingsFragment;
import com.contron.ekeyapptx.devices.DevicesExplorationFragment;
import com.contron.ekeyapptx.doorlock.DoorLockerActivity;
import com.contron.ekeyapptx.fsu.DistantApplyFragment;
import com.contron.ekeyapptx.fsu.DistantApprovalFragment;
import com.contron.ekeyapptx.fsu.FsuDistantFragment;
import com.contron.ekeyapptx.key.ReturnKeyConfirm;
import com.contron.ekeyapptx.key.ReturnKeyRegist;
import com.contron.ekeyapptx.plan.FixationTaskFragment;
import com.contron.ekeyapptx.plan.PlanTicketAccreditFragment;
import com.contron.ekeyapptx.plan.PlanTicketApplyFragment;
import com.contron.ekeyapptx.plan.PlanTicketListFragment;
import com.contron.ekeyapptx.plan.TicketDetaiActivity;
import com.contron.ekeyapptx.profile.AnnouncementFragment;
import com.contron.ekeyapptx.profile.UserInfoFragment;
import com.contron.ekeyapptx.search.AlertInfoFragment;
import com.contron.ekeyapptx.search.ContactFragment;
import com.contron.ekeyapptx.plan.FixedSysTaskFragment;
import com.contron.ekeyapptx.search.DeviceSearchFragment;
import com.contron.ekeyapptx.user.PersonalCenterFragment;
import com.contron.ekeyapptx.workorder.MyWorkActivity;
import com.contron.ekeyapptx.workorder.WorkApprovalFragment;
import com.contron.ekeyapptx.workorder.WorkBillFragment;
import com.contron.ekeyapptx.workorder.WorkBillTimelinessFragment;
import com.contron.ekeyapptx.workorder.WorkPatrolExecuteFragment;
import com.contron.ekeyapptx.workorder.WorkPatrolFragment;
import com.contron.ekeyapptx.workorder.WorkPatrolTimelinessFragment;
import com.contron.ekeypublic.PermissionsActivity;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType;
import com.contron.ekeypublic.nfc.ReadMifareUltralight;
import com.contron.ekeyapptx.borrowkey.BorrowKeyConfirm;
import com.contron.ekeyapptx.borrowkey.BorrowkeyMainFragment;
import com.contron.ekeyapptx.permission.OfflineFixedTaskDelayApplyFragment;
import com.contron.ekeyapptx.permission.OfflineFixedTaskDelayAuditMainFragment;
import com.contron.ekeyapptx.user.LoginFragment;
import com.contron.ekeyapptx.user.UserRegisterAuditListFragment;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

/**
 * Fragment显示公用Activity类
 * 
 * @author luoyilong
 *
 */
public class TicketActivity extends BasicActivity {

	LoginFragment mLoginFragment = null;// 处理LoginFragment界面设置权限后返回事件

	/** 鉴权开锁 */
	public static final int EXTRA_TYPE_UNLOCK_BY_KEY = 0;
	/** 临时任务 */
	public static final int EXTRA_TYPE_1 = 1;
	/** 开锁审批 */
	public static final int EXTRA_TYPE_2 = 2;
	/** 注册审批 */
	public static final int EXTRA_TYPE_3 = 3;
	/** 系统设置 */
	public static final int EXTRA_TYPE_4 = 4;
	/** 蓝牙开门 */
	public static final int EXTRA_TYPE_5 = 5;
	/** 设备采码 */
	public static final int EXTRA_TYPE_6 = 6;
	/** 离线鉴权开锁延时申请 */
	public static final int EXTRA_TYPE_7 = 7;
	/** 离线鉴权开锁延时申请审批 */
	public static final int EXTRA_TYPE_8 = 8;
	/** 采码 */
	public static final int EXTRA_TYPE_9 = 9;
	/** 临时任务授权 */
	public static final int EXTRA_TYPE_10 = 10;
	/** 借钥匙登记 */
	public static final int EXTRA_TYPE_11 = 11;
	/** 借钥匙确认 */
	public static final int EXTRA_TYPE_12 = 12;
	/** 还钥匙登记 */
	public static final int EXTRA_TYPE_13 = 13;
	/** 还钥匙确认 */
	public static final int EXTRA_TYPE_14 = 14;
	/** FSU远控 */
	public static final int EXTRA_TYPE_15 = 15;
	/** 操作查询 */
	public static final int EXTRA_TYPE_16 = 16;
	/** 手动采码 */
	public static final int EXTRA_TYPE_17 = 17;
	/** 临时权限授权 */
	public static final int EXTRA_TYPE_18 = 18;
	/** 设备坐标采集 */
	public static final int EXTRA_TYPE_19 = 19;

	/** 预置任务 */
	public static final int EXTRA_TYPE_20 = 20;

	/** 回传操作记录 */
	public static final int EXTRA_TYPE_22 = 22;

	/** 固定任务 */
	public static final int EXTRA_TYPE_23 = 23;

	/** 派工 */
	public static final int EXTRA_TYPE_24 = 24;

	/** 待办工作 */
	public static final int EXTRA_TYPE_25 = 25;

	/** 添加巡检任务 */
	public static final int EXTRA_TYPE_26 = 26;

	/** 查询巡检任务 */
	public static final int EXTRA_TYPE_27 = 27;

	/** 到站及时率 */
	public static final int EXTRA_TYPE_28 = 28;

	/** 巡检率 */
	public static final int EXTRA_TYPE_29 = 29;

	/** 通讯录查询 */
	public static final int EXTRA_TYPE_32 = 32;

	/** 告警信息查询 */
	public static final int EXTRA_TYPE_33 = 33;

	/** 公告 **/
	public static final int EXTRA_TYPE_34 = 34;

	/** 账号信息 **/
	public static final int EXTRA_TYPE_35 = 35;

	/** 远程解锁审批 **/
	public static final int EXTRA_TYPE_36 = 36;

	/** 远程解锁申请 **/
	public static final int EXTRA_TYPE_37 = 37;

	/** 设备勘查 **/
	public static final int EXTRA_TYPE_38 = 38;

	/** 设备查询 **/
	public static final int EXTRA_TYPE_39 = 39;

	/** 进站审批 **/
	public static final int EXTRA_TYPE_31 = 31;

	/** 远程开门 **/
	public static final int EXTRA_TYPE_OPEN_DOOR_BY_FSU = 40;

	/** 站址导航 **/
	public static final int EXTRA_TYPE_STATION_NAVIGATION = 41;

	/** 个人中心 **/
	public static final int EXTRA_TYPE_PERSONAL_CENTER = 42;

	/** 登录 */
	public static final int EXTRA_TYPE_100 = 100;
	private int mType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ticket);
		EkeyAPP.getInstance().addActivity(this);
		if (savedInstanceState == null) {
			Bundle extras = getIntent().getExtras();
			if (extras != null)
				mType = extras.getInt("type");
			switch (mType) {
				case EXTRA_TYPE_UNLOCK_BY_KEY:// 鉴权开锁
					// repalce(TicketDetailFragment.newInstance(), false);
					TicketDetaiActivity.startActivity(this);
					this.finish();
					break;
				case EXTRA_TYPE_1:// 临时任务申请
					repalce(PlanTicketApplyFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_2:// 开锁审批
					repalce(AuditListFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_3:// 注册审批
					repalce(UserRegisterAuditListFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_4:// 设置
					repalce(SettingsFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_5:// 在线蓝牙开锁
					// repalce(DoorLockerFragment.newInstance(), false);
					 DoorLockerActivity.startActivity(this);
					 this.finish();
					break;
				case EXTRA_TYPE_6:// 临时任务开锁
					repalce(PlanTicketListFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_7:// 离线鉴权申请
					repalce(OfflineFixedTaskDelayApplyFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_8:// 离线鉴权审核
					repalce(OfflineFixedTaskDelayAuditMainFragment.newInstance(),
							false);
					break;
				case EXTRA_TYPE_9:
					// repalce(CollectTicketListFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_10: // 临时任务授权 wangfang
					// repalce(TemporaryMandateFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_11: // 借钥匙登记 wangfang
					repalce(BorrowkeyMainFragment.newInstance(), false);
					// repalce(BorrowKeyRegist.newInstance(), false);
					break;
				case EXTRA_TYPE_12: // 借钥匙确认 wangfang
					repalce(BorrowKeyConfirm.newInstance(), false);
					break;
				case EXTRA_TYPE_13: // 还钥匙登记 wangfang
					repalce(ReturnKeyRegist.newInstance(), false);
					break;
				case EXTRA_TYPE_14: // 还钥匙确认 wangfang
					repalce(ReturnKeyConfirm.newInstance(), false);
					break;
				case EXTRA_TYPE_15: // FSU远控
					repalce(FsuDistantFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_16: // 操作查询
					repalce(OperateSelectFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_17: // 锁具采码
					repalce(ManuCollectTicketFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_18: // 临时权限授权
					repalce(PlanTicketAccreditFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_20: // 预置任务
					repalce(FixationTaskFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_22: // 回传操作记录
					repalce(HistoryFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_23: // 固定系统任务
					repalce(FixedSysTaskFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_24: // 进站派工
					repalce(WorkBillFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_25:
					MyWorkActivity.startActivity(this);
					break;
				case EXTRA_TYPE_26:
					repalce(WorkPatrolFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_27:
					repalce(WorkPatrolExecuteFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_28:
					repalce(WorkBillTimelinessFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_29:
					repalce(WorkPatrolTimelinessFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_31:
					repalce(WorkApprovalFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_32:
					repalce(ContactFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_33:
					repalce(AlertInfoFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_34:
					repalce(AnnouncementFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_35:
					repalce(UserInfoFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_36:
					repalce(DistantApprovalFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_37:
					repalce(DistantApplyFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_38:
					repalce(DevicesExplorationFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_39:
					repalce(DeviceSearchFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_OPEN_DOOR_BY_FSU:
					repalce(RemoteUnlockManageFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_STATION_NAVIGATION:
					repalce(StationNavigationFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_PERSONAL_CENTER:
					repalce(PersonalCenterFragment.newInstance(), false);
					break;
				case EXTRA_TYPE_100:// 登录
					mLoginFragment = LoginFragment.newInstance();
					repalce(mLoginFragment, false);
					break;
				default:
					break;
			}
		} else {
			// 用于内存不足的时候保证fragment不会重叠
			Fragment fragment = getSupportFragmentManager().findFragmentByTag(
					PlanTicketListFragment.class.getSimpleName());
			if (fragment != null) {
				getSupportFragmentManager().beginTransaction().hide(fragment)
				.addToBackStack(null).commit();
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// 移除当前Activity
		EkeyAPP.getInstance().removeActivity(this);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	// Nfc读取【截取NFC读取的Intent，进行解析】
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

		if (tagFromIntent == null)
			return;
		// 解析NFC卡读取蓝牙地址
		String address = ReadMifareUltralight.readTag(tagFromIntent);

		if (!TextUtils.isEmpty(address))// 发送给【借钥匙登记、归还钥匙登记】界面
			EventBusManager.getInstance().post(
					new EventBusType.EvenNfcLyAddress(address));

	}

	/**
	 * 
	 * @author hupei
	 * @date 2015年7月27日 下午3:01:15
	 * @param activity
	 * @param type
	 *            跳转界面标示 {@link #EXTRA_TYPE_UNLOCK_BY_KEY}，{@link #EXTRA_TYPE_1}，
	 *            {@link #EXTRA_TYPE_2}，{@link #EXTRA_TYPE_3}，
	 *            {@link #EXTRA_TYPE_4}， {@link #EXTRA_TYPE_100}
	 */
	public static void startActivity(Activity activity, int type) {
		activity.startActivity(new Intent(activity, TicketActivity.class)
				.putExtra("type", type));
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (mLoginFragment != null) {
			// 拒绝时, 关闭页面, 缺少主要权限, 无法运行
			if (requestCode == LoginFragment.REQUEST_CODE
					&& resultCode == PermissionsActivity.PERMISSIONS_DENIED) {
				finish();
			} else {
				mLoginFragment.setMobile();
			}
		}

	}
}
