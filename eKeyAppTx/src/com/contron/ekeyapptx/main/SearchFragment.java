package com.contron.ekeyapptx.main;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.entities.User;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class SearchFragment extends CommonFragment implements OnItemClickListener{

	@Override
	protected ContronAdapter<Function> getAdapter(User user) {
		int itemLayoutId = R.layout.activity_function_item;
		List<Function> listData = getListData(user);
		mAdapter = new ContronAdapter<Function>(getContext(), itemLayoutId, listData) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder, Function item, int position) {
				TextView tvName = viewHolder.findView(R.id.function_item_tv_name);
				tvName.setText(item.nameResId);
				tvName.setCompoundDrawablesWithIntrinsicBounds(0, item.icoResId, 0, 0);
			}
		};
		mGridView.setOnItemClickListener(this);
		return mAdapter;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		int functionId = mAdapter.getItem(position).nameResId;
		switch (functionId) {
//			case R.string.main_tab_item16_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_16);
//				break;
//			case R.string.main_tab_item12_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_12);
//				break;
//			case R.string.main_tab_item14_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_14);
//				break;
//			case R.string.main_tab_item33_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_33);
//				break;
			case R.string.main_tab_item28_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_28);
				break;
			case R.string.main_tab_item29_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_29);
				break;
//			case R.string.main_tab_item39_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_39);
//				break;
		}
	}

	private List<Function> getListData(User user) {
		List<Function> functions = new ArrayList<Function>();
		// 操作记录查询
//		functions.add(new Function(R.drawable.query, R.string.main_tab_item16_name));
		// 借用钥匙确认
//		functions.add(new Function(R.drawable.lendkey, R.string.main_tab_item12_name));
		// 归还钥匙确认
//		functions.add(new Function(R.drawable.returnkey, R.string.main_tab_item14_name));

		// 告警信息
//		functions.add(new Function(R.drawable.query_alarm, R.string.main_tab_item33_name));
		// 到站及时率
		functions.add(new Function(R.drawable.query_workintime, R.string.main_tab_item28_name));
		// 巡检率
		functions.add(new Function(R.drawable.query_workpatrol, R.string.main_tab_item29_name));
		// 设备查询
//		functions.add(new Function(R.drawable.query_device, R.string.main_tab_item39_name));
		return functions;
	}

}
