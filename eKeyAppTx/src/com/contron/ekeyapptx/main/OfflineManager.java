package com.contron.ekeyapptx.main;

import java.util.Calendar;
import java.util.Date;

import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.key.EmpowerActivity;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.Offline;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.util.DateUtil;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;

import android.os.Handler;
import android.text.TextUtils;
import android.widget.Toast;

public class OfflineManager {

	private static OfflineManager instance;

	private int mSeconds = 0;
	private int mMinutes = 0;
	private int mHours = 0;
	Handler handler = new Handler();
	OfflineTimeListener mTimeListener;

	private OfflineManager() {

	}

	public static OfflineManager getInstance() {
		if (instance == null)
			instance = new OfflineManager();
		return instance;
	}

	private void showToast(CharSequence text) {
		Toast.makeText(EkeyAPP.getInstance().getContext(), text, Toast.LENGTH_LONG).show();
	}

	/**
	 * 当离线鉴权第一次登录时，开始计时（写入开始时间），后续根据开始时间和延时时长判定离线鉴权是否有效
	 * 
	 * @author guanhaiping
	 * @date
	 * @return
	 */
	public void updateFixedTicketDelayBeginTime() {
		DbUtils dbUtils = DBHelp.getInstance(EkeyAPP.getInstance().getContext()).getDb();
		String sql = "update offline set beg_at = '"
				+ DateUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss")
				+ "' where (beg_at = '' or beg_at is NULL) and apply_username = "
				+ String.valueOf(EkeyAPP.getInstance().getUser().getUsername());
		try {
			dbUtils.execNonQuery(sql);
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void getLastFixedTicketDelayInfo() {
		// 获取最后一次审批通过的延时记录
		Offline delay = DBHelp
				.getInstance(EkeyAPP.getInstance().getContext())
				.getEkeyDao()
				.findLastAuditTicketDelay(
						String.valueOf(EkeyAPP.getInstance().getUser()
								.getUsername()));

		EkeyAPP.getInstance().setOffline(delay);

		if (null == delay) {
			showToast("没有离线鉴权开锁信息，请先进行离线鉴权延时申请！");
			return;
		}
		if (TextUtils.isEmpty(delay.getBeg_at())) {
//			showToast("离线鉴权开锁授权信息数据有误，请重新进行离线鉴权延时申请！");
			return;
		}

		int delayMinutes = (int) (delay.getDuration());

		String end = DateUtil.getStringByOffset(delay.getBeg_at(),
				"yyyy-MM-dd HH:mm:ss", Calendar.MINUTE, delayMinutes);
		Date endTime = DateUtil.getDateByFormat(end, "yyyy-MM-dd HH:mm:ss");
		Date now = DateUtil.getDateByFormat(DateUtil.getCurrentDate(),
				"yyyy-MM-dd HH:mm:ss");
		if (endTime.before(now)) {
			showToast("离线鉴权开锁信息已超时，请重新进行离线鉴权延时申请！");
			return;
		}

		int minutes = DateUtil.getOffectMinutes(endTime.getTime(),
				now.getTime());
		mHours = minutes / 60;
		mMinutes = minutes % 60;
		if (mMinutes > 0) {
			mMinutes--;
			mSeconds = 59;
		} else {
			mHours--;
			mMinutes = 59;
			mSeconds = 59;
		}
	}

	Runnable runnable = new Runnable() {
		@Override
		public void run() {
			if (mHours == 0 && mMinutes == 0 && mSeconds == 0) {
				// 定时关闭打开的窗口
				EkeyAPP.getInstance().finishActivity(EmpowerActivity.class);
				EkeyAPP.getInstance().finishActivity(TicketActivity.class);
				return;
			}
			if (mSeconds <= 0) {
				mSeconds = 60;
				mMinutes--;
				if (mMinutes <= 0) {
					if (mHours > 0) {
						mMinutes = 59;
						mHours--;
					}
					if (mHours <= 0)
						mHours = 0;
				}
			}
			mSeconds--;
			String sHours = "00" + mHours;
			sHours = sHours.substring(sHours.length() - 2, sHours.length());
			String sMinutes = "00" + mMinutes;
			sMinutes = sMinutes.substring(sMinutes.length() - 2,
					sMinutes.length());
			String sSeconds = "00" + mSeconds;
			sSeconds = sSeconds.substring(sSeconds.length() - 2,
					sSeconds.length());
			if (null != mTimeListener)
				mTimeListener.refreshTimeText(sHours + ":" + sMinutes + ":" + sSeconds);
			handler.postDelayed(this, 1000);
		}
	};

	public void startOfflineTime(OfflineTimeListener timeListener) {
		// 更新离线鉴权最后一次审批操作开始时间
		mTimeListener = timeListener;
		startOfflineTime();
	}
	
	private void startOfflineTime() {
		// 更新离线鉴权最后一次审批操作开始时间
//		updateFixedTicketDelayBeginTime();
		boolean ret = DBHelp
				.getInstance(EkeyAPP.getInstance().getContext())
				.getEkeyDao()
				.isOffineStart(EkeyAPP.getInstance().getUser()
								.getUsername());
		if(!ret)
			return;
		getLastFixedTicketDelayInfo();
		handler.postDelayed(runnable, 1000);
	}
	
	public boolean isOfflineTimeUp() {
		return mHours == 0 && mMinutes == 0 && mSeconds == 0;
	}
	
	public void unlock(User user, boolean isOffline) {
		if (!user.isOnline) {
			// 未启动倒计时时执行
			if (!isOffline) {
				startOfflineTime();
				isOffline = true;
			}
			if (OfflineManager.getInstance().isOfflineTimeUp()) {
				showToast("无离线鉴权信息或者离线鉴权申请的延时已过期，请重新申请！");
				return;
			}
		}	
	}

	public interface OfflineTimeListener {
		 void refreshTimeText(String timeText);
	}

	public String getTotalDelayInfo() {
		Offline delay = DBHelp
				.getInstance(EkeyAPP.getInstance().getContext())
				.getEkeyDao()
				.findLastAuditTicketDelay(
						String.valueOf(EkeyAPP.getInstance().getUser()
								.getUsername()));
		if(delay == null)
			return "00:00:00";
		int delayMinutes = (int) (delay.getDuration());
		int duration = delayMinutes / 60;
		String hours = "00" + duration;
		hours = hours.substring(hours.length() - 2, hours.length());
		mHours = Integer.valueOf(hours);
		return hours + ":" + "00:00";
	}

}
