package com.contron.ekeyapptx.main;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeyapptx.*;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.entities.User;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class ApprovalFragment extends CommonFragment implements OnItemClickListener{


	@Override
	protected ContronAdapter<Function> getAdapter(User user) {
		int itemLayoutId = R.layout.activity_function_item;
		List<Function> listData = getListData(user);
		final MainActivity mainActivity = (MainActivity) getActivity();
		mAdapter = new ContronAdapter<Function>(getContext(), itemLayoutId, listData) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder, Function item, int position) {
				TextView tvName = viewHolder.findView(R.id.function_item_tv_name);
				tvName.setText(item.nameResId);
				tvName.setCompoundDrawablesWithIntrinsicBounds(0, item.icoResId, 0, 0);
				TextView tvtag = viewHolder.findView(R.id.tx_tag);

				switch (item.nameResId) {
					case R.string.main_tab_item3_name:
						setTag(tvtag, mainActivity.mTempTask);
						break;
					case R.string.main_tab_item8_name:
						setTag(tvtag, mainActivity.mOffline);
						break;
					case R.string.main_tab_item4_name:
						setTag(tvtag, mainActivity.mRegister);
						break;
					case R.string.main_tab_item31_name:
						setTag(tvtag, mainActivity.mWrokbill);
						break;
//					case R.string.main_tab_item36_name:
//						setTag(tvtag, mainActivity.mUnlock);
//						break;
				}
			}
		};
		mGridView.setOnItemClickListener(this);
		return mAdapter;
	}

	private void setTag(TextView tvtag, int count) {
		if (count != 0) {
			tvtag.setVisibility(View.VISIBLE);

		}else {
			tvtag.setVisibility(View.INVISIBLE);
		}
		tvtag.setText(String.valueOf(count));
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		int functionId = mAdapter.getItem(position).nameResId;
		switch (functionId) {
			case R.string.main_tab_item3_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_2);
				break;
			case R.string.main_tab_item8_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_8);
				break;
			case R.string.main_tab_item4_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_3);
				break;
			case R.string.main_tab_item31_name:
				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_31);
				break;
//			case R.string.main_tab_item36_name:
//				TicketActivity.startActivity(getActivity(), TicketActivity.EXTRA_TYPE_36);
//				break;
		}
	}

	private List<Function> getListData(User user) {
		List<Function> functions = new ArrayList<Function>();
		// 有审批注册用户才显示
		functions.add(new Function(R.drawable.yonghu, R.string.main_tab_item4_name));
		// 离线鉴权审批
		functions.add(new Function(R.drawable.delayaudit, R.string.main_tab_item8_name));
		// 有审批任务权限才显示
		functions.add(new Function(R.drawable.jilu, R.string.main_tab_item3_name));
		// 进站审批
		functions.add(new Function(R.drawable.jinzhan_shenpi, R.string.main_tab_item31_name));
		// 远控解锁审批
//		functions.add(new Function(R.drawable.yuancheng_jiesuoshenpi, R.string.main_tab_item36_name));
		return functions;
	}

}
