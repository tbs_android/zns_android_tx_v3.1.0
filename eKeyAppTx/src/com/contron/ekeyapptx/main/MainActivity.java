package com.contron.ekeyapptx.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.MarkerOptions;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.WarnMsgActivity;
import com.contron.ekeyapptx.doorlock.DoorLockerActivity;
import com.contron.ekeyapptx.socket.SocketService;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.InitializeData;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.LockProtocol;
import com.contron.ekeypublic.entities.Log;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.Verify;
import com.contron.ekeypublic.entities.WarnMsg;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.ConfigUtil;
import com.contron.ekeypublic.util.DateUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.readystatesoftware.viewbadger.BadgeView;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends BasicActivity implements OnClickListener, OnPageChangeListener {
    private ViewPager mViewPager;
    private ImageView mUnlockImg;
    private ImageView mWorkImg;
    private ImageView mApprovalImg;
    private ImageView mSearchImg;
    //	private ImageView mSettingImg;
    private ImageView mMainImg;

    private TextView mUnlockText;
    private TextView mWorkText;
    private TextView mApprovalText;
    private TextView mSearchText;
    //	private TextView mSettingText;
    private TextView mMainText;
    private View mBottomLayout;

    private BadgeView badgeTag;// 显示警告信息条数
    private TextView mLockTypeText;

    private TextView warnmsg;// 告警信息

    private FragmentPagerAdapter mAdapter;
    private List<CommonFragment> mFragments;

    private BluetoothAdapter mBluetoothAdapter;

    private User mUser;
    private Intent mIntentSocketService;// Socket服务
    protected int count;

    protected int mTempTask = 0;// 临时权限审批条数
    protected int mRegister = 0;// 注册审批条数
    protected int mOffline = 0;// 离线审批条数
    protected int mUnlock = 0;// 解锁审批条数
    protected int mWrokbill = 0;// 工单审批条数
    protected String mNoticeMsg;

    private RequestHttp requestHttp;
    private Timer mTimer;

    public static void startActivity(Activity activity) {
        activity.startActivity(new Intent(activity, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.util.Log.d("MainActivity", "Begin onCreate at:" + DateUtil.getCurrentDate());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_index);
//        showTransitionPage();
        EkeyAPP.getInstance().addActivity(this);
        // 注册EventBusManager事件
        EventBusManager.getInstance().register(this);
        mUser = EkeyAPP.getInstance().getUser();
        initView();
        initData();

        if (!mUser.isOnline)
            setSelect(0);
        else
            setSelect(2);
        startUnLockProtocolTask();

        android.util.Log.d("MainActivity", "End onCreate at:" + DateUtil.getCurrentDate());
    }

    private void showTransitionPage() {
        View view = ((ViewGroup) getWindow().getDecorView()).getChildAt(0);
        // 动画过渡
        AlphaAnimation animation = new AlphaAnimation(0.4f, 1.0f);
        animation.setDuration(3000);
        view.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation arg0) {
                setContentView(R.layout.activity_index);
                if (!mUser.isOnline)
                    setSelect(0);
                else
                    setSelect(2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
            }

        });

    }

    private void initView() {
        android.util.Log.d("MainActivity", "Begin initView at:" + DateUtil.getCurrentDate());
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mBottomLayout = findViewById(R.id.bottom_layout);
        warnmsg = (TextView) findViewById(R.id.warnmsg_text);
        warnmsg.setOnClickListener(this);

        findViewById(R.id.unlock_item).setOnClickListener(this);
        findViewById(R.id.work_item).setOnClickListener(this);
        findViewById(R.id.approval_item).setOnClickListener(this);
        findViewById(R.id.search_item).setOnClickListener(this);
//		findViewById(R.id.setting_item).setOnClickListener(this);
        findViewById(R.id.main_item).setOnClickListener(this);

        mUnlockImg = (ImageView) findViewById(R.id.unlock_img);
        mWorkImg = (ImageView) findViewById(R.id.work_img);
        mApprovalImg = (ImageView) findViewById(R.id.approval_img);
        mSearchImg = (ImageView) findViewById(R.id.search_img);
//		mSettingImg = (ImageView) findViewById(R.id.setting_img);
        mMainImg = (ImageView) findViewById(R.id.main_img);

        mUnlockText = (TextView) findViewById(R.id.unlock_text);
        mWorkText = (TextView) findViewById(R.id.work_text);
        mApprovalText = (TextView) findViewById(R.id.approval_text);
        mSearchText = (TextView) findViewById(R.id.search_text);
//		mSettingText = (TextView) findViewById(R.id.setting_text);
        mMainText = (TextView) findViewById(R.id.main_text);

        initTitle();
        initFragment();
//        InitializeData init = new InitializeData(this, mUser, getWld(), false);
//        init.getDicts();
        EkeyAPP.getInstance().initData(this, true, true, true);// 下载初始化数据
        android.util.Log.d("MainActivity", "End initView at:" + DateUtil.getCurrentDate());
    }

    private void initTitle() {
        mLockTypeText = (TextView) findViewById(R.id.lock_type_text);

        if (EkeyAPP.getInstance().isChoiceMode()) {
            mLockTypeText.setText("中国铁塔智能锁控管理系统");
            Drawable drawable = getResources().getDrawable(R.drawable.ic_launcher);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            mLockTypeText.setCompoundDrawables(drawable, null, null, null);
        } else {
            mLockTypeText.setText(R.string.app_namepw);
            Drawable drawable = getResources().getDrawable(R.drawable.ekey_logo);
            drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
            mLockTypeText.setCompoundDrawables(drawable, null, null, null);
        }
    }

    private void initFragment() {
        android.util.Log.d("MainActivity", "Begin initFragment at:" + DateUtil.getCurrentDate());
        mFragments = new ArrayList<CommonFragment>();
        if (mUser.isOnline) {
            CommonFragment workFragment = new WorkFragment();
            CommonFragment approvalFragment = new ApprovalFragment();
            CommonFragment searchFragment = new SearchFragment();
//			CommonFragment settingFragment = new ProfileFragment();
            CommonFragment mainFragment = new MainFragment();
            CommonFragment unlockFragment = new UnlockFragment();

            mFragments.add(0, workFragment);
            mFragments.add(1, unlockFragment);
            mFragments.add(2, mainFragment);
            mFragments.add(3, approvalFragment);
            mFragments.add(4, searchFragment);
//			mFragments.add(settingFragment);
        } else {
            CommonFragment unlockFragment = new UnlockFragment();
            mFragments.add(unlockFragment);
        }
        mAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public Fragment getItem(int arg0) {
                return mFragments.get(arg0);
            }
        };
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(this);
        mViewPager.setOffscreenPageLimit(5);
        android.util.Log.d("MainActivity", "End initFragment at:" + DateUtil.getCurrentDate());
    }

    /**
     * 初始化数据
     */
    private void initData() {
        adjustByUser();
        initBlutTooth();

        /** 初始化警告条数显示小标签 */
        /**
         * 暂时不显示
         */
        badgeTag = new BadgeView(this, warnmsg);
        badgeTag.setBadgeMargin(0);
//        badgeTag.show();
    }

    private void initBlutTooth() {
        // 判断当前手机是否支持蓝牙
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "您的设备不支持BLE", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        // 获取蓝牙服务
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }



    private void adjustByUser() {
        // added by guanhaiping 设置离线鉴权倒计时信息框是否可见
        if (mUser.isOnline) {
            // 非离线情况下启动即时通讯服务
            mIntentSocketService = new Intent(EkeyAPP.getInstance().getContext(), SocketService.class);
            startService(mIntentSocketService);
            // 当前登录用户与上次登录用户不同时初始化数据\上传离线操作数据、下载离线鉴权
//            EkeyAPP.getInstance().initData(this, false, EkeyAPP.getInstance().getOlduser(), true);
        } else {
            mBottomLayout.setVisibility(View.GONE);// 隐藏底部菜单

        }
    }



    @Override
    protected void onResume() {
        android.util.Log.d("MainActivity", "Begin onResume at:" + DateUtil.getCurrentDate());
        super.onResume();
        // 获取告警信息条数
        count = EkeyAPP.getInstance().getWarnCount();
        badgeTag.setText(count + "");
        mTimer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {// 蓝牙关闭状态
                    mBluetoothAdapter.enable();// 强制打开蓝牙
                }
            }
        };
        mTimer.schedule(task, 200);
        android.util.Log.d("MainActivity", "End onResume at:" + DateUtil.getCurrentDate());
    }

    /**
     * 接收Socket发送过来的审批数量
     */
    public void onEventMainThread(Verify msg) {
        if (msg == null)
            return;
        mTempTask = msg.getTemp_task();
        mRegister = msg.getRegister();
        mOffline = msg.getOffline();
        mUnlock = msg.getUnlock();
        mWrokbill = msg.getWorkbill();
        if (null != mFragments && mFragments.size() > 3 && null != mFragments.get(3)) {
            mFragments.get(3).mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 接收Socket发送过来的告警信息记录
     *
     * @param msg 告警信息
     */
    public void onEventMainThread(WarnMsg msg) {
        if (msg == null)
            return;
        ++count;
        badgeTag.setText(count + "");
        EkeyAPP.getInstance().setWarnCount(count);
        if (mFragments.size() == 5) {
            MainFragment mainFrame = (MainFragment) mFragments.get(2);
            mainFrame.updateAlertCountTag();
        }
    }

    /**
     * 接收Socket发送过来的公告信息
     *
     * @param msg 公告信息
     */
    public void onEventMainThread(String msg) {
        if (msg == null)
            return;
        mNoticeMsg = msg;
//        if (null != mFragments && mFragments.size() > 4 && null != mFragments.get(4)) {
//            mFragments.get(4).mAdapter.notifyDataSetChanged();
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBusManager.getInstance().unregister(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.work_item:
                setSelect(0);
                break;
            case R.id.unlock_item:
                setSelect(1);
                break;
            case R.id.main_item:
                setSelect(2);
                break;
            case R.id.approval_item:
                setSelect(3);
                break;
            case R.id.search_item:
                setSelect(4);
                break;
//            case R.id.setting_item:
//                setSelect(4);
//                break;
            case R.id.warnmsg_text:
                WarnMsgActivity.gotoActivity(this);// 跳转到WarnMsgActivity
                break;
        }
    }

    @Override
    public void onPageSelected(int arg0) {
        int currentItem = mViewPager.getCurrentItem();
        setTab(currentItem);
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {

    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    private void setSelect(int i) {
//         Animation animation = new TranslateAnimation(currIndex * one,
//         arg0
//         * one, 0, 0);// 平移动画
//         animation.setFillAfter(true);// 动画终止时停留在最后一帧，不然会回到没有执行前的状态
//         animation.setDuration(200);// 动画持续时间0.2秒
//         mimg_cursor.startAnimation(animation);// 是用ImageView来显示动画的
        setTab(i);
        mViewPager.setCurrentItem(i);
    }

    private void setTab(int i) {
        resetSelected();
        switch (i) {
            case 0:
                mWorkImg.setSelected(true);
                mWorkText.setSelected(true);
                break;
            case 1:
                mUnlockImg.setSelected(true);
                mUnlockText.setSelected(true);
                break;
            case 2:
                mMainImg.setSelected(true);
                mMainText.setSelected(true);
                break;
            case 3:
                mApprovalImg.setSelected(true);
                mApprovalText.setSelected(true);
                break;
            case 4:
                mSearchImg.setSelected(true);
                mSearchText.setSelected(true);
                break;
//		case 4:
//			mSettingImg.setSelected(true);
//			mSettingText.setSelected(true);
//			break;
        }
        setFunctionTitleVisible(i);
    }

    private void resetSelected() {
        mUnlockImg.setSelected(false);
        mWorkImg.setSelected(false);
        mApprovalImg.setSelected(false);
        mSearchImg.setSelected(false);
//        mSettingImg.setSelected(false);
        mMainImg.setSelected(false);

        mUnlockText.setSelected(false);
        mWorkText.setSelected(false);
        mApprovalText.setSelected(false);
        mSearchText.setSelected(false);
//        mSettingText.setSelected(false);
        mMainText.setSelected(false);
    }

    private void setFunctionTitleVisible(int page) {
        if (2 == page) {
//            mUserView.setVisibility(View.VISIBLE);
//            mScannerView.setVisibility(View.VISIBLE);
//            mMapView.setVisibility(View.VISIBLE);
        } else {
//            mUserView.setVisibility(View.GONE);
//            mScannerView.setVisibility(View.GONE);
//            mMapView.setVisibility(View.GONE);
        }
    }

    protected static final int EXIT_TIME_END = 2000;// 按二次退出时间的间隔
    protected long exitTime = 0;// 记录按第一次退出的时间

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > EXIT_TIME_END) {
                showToast(R.string.exit_app_tip);
                exitTime = System.currentTimeMillis();
            } else {
                if (mBluetoothAdapter.isEnabled())// 关闭蓝牙
                    mBluetoothAdapter.disable();
                if (mUser != null && mUser.isOnline)
                    stopService(mIntentSocketService);// 关闭监听服务
                EkeyAPP.getInstance().exitApp();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void startUnLockProtocolTask() {
        User user = getUser();
        if (user == null)
            return;
        if (!user.isOnline)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(getWld());// 服务请求
        startDialog(R.string.progressDialog_title_load);// 弹出对话框
        // 设置参数
        RequestParams params = new RequestParams(user);
        requestHttp.doPost(Globals.CONF_UNLOCK_PROTOCOL, params,
                new RequestHttp.HttpRequestCallBackString() {

                    @Override
                    public void resultValue(final String value) {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dismissDialog();
                                if (value.contains("success")) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(value);
                                        JSONArray jsonArray = jsonObject
                                                .getJSONArray("items");
                                        if (jsonArray.length() == 0) {
                                            EkeyAPP.getInstance().setSCLock(false);
                                        } else {
                                            List<LockProtocol> items = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<LockProtocol>>() {
                                            }.getType());
                                            if (items != null && items.size() > 0) {
                                                LockProtocol item = items.get(0);
                                                int protocolValue = Integer.parseInt(item.getValue());
                                                EkeyAPP.getInstance().setSCLock(protocolValue == 2);
                                            } else {
                                                EkeyAPP.getInstance().setSCLock(false);
                                            }
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    ConfigUtil.getInstance(MainActivity.this).setBoolean("scLock", false);
                                    try {
                                        JSONObject json = new JSONObject(value);
                                        String error = json.getString("error");
                                        showMsgBox(error);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }
                        });

                    }
                });
    }

}