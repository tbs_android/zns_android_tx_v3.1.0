package com.contron.ekeyapptx;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v4.app.Fragment;

import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.ConfigUtil;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.util.VersionUtil;
import com.google.gson.JsonObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class UpdateFragment extends Fragment {

    public static final String APP_VERSION = "AppVersion";
    public static final String APP_UPDATE_TIME = "UpdateTime";
    @ViewInject(R.id.update_version_textView)
    private TextView mVersion;

    @ViewInject(R.id.update_time_textView)
    private TextView mTime;

    private BasicActivity mActivity;
    private User user;
    private RequestHttp requestHttp;
    private String mAppVersion;
    private String mAppUpdateTime;
    private String mAppUpdatePath = "";

    public static UpdateFragment newInstance() {
        return new UpdateFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_update, container, false);
        ViewUtils.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAppVersion = VersionUtil.getVersionName(mActivity);
        mAppUpdateTime = VersionUtil.getLastUpdateTime(mActivity, DateUtil.dateFormatYMD);
        setAppVersionInfo(mAppVersion, mAppUpdateTime);
        setHasOptionsMenu(true);
        checkForUpdate();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(0, 0, 0, "更新");
        MenuItem menuItem = menu.findItem(0);
        menuItem.setIcon(R.drawable.settings_update);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackFragment();
                break;
            case 0:
                checkForUpdate();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkForUpdate() {
        // 后台获取最新app版本信息，对比当前app版本
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());

        // 请求接口
        String url = String.format(Globals.APP_UPDATE);

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.dismissDialog();
                        if (value.contains("success")) {
                            try {
                                JSONObject jsonObject = new JSONObject(value);
                                String version = jsonObject.getString("version");
                                if (null != version && !version.equals(mAppVersion)) {
                                    mAppUpdatePath = jsonObject.getString("path");
                                    update();
                                } else {
                                    mActivity.showToast("当前程序是最新版本");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            mActivity.showToast("检查更新失败");
                        }

                    }
                });

            }
        });
    }

    private void update() {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setIcon(R.drawable.advise); // 设置对话框的图标
        builder.setTitle("提示"); // 设置对话框的标题
        builder.setMessage("是否进行更新？"); // 设置要显示的内容
        // 确定按钮
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface dialog,
                            int which) {
                        //TODO 跳转到浏览器下载页面
                        Uri uri = Uri.parse(mActivity.getWld() + mAppUpdatePath);//要跳转的网址
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }

                });
        // 取消按钮
        builder.setNegativeButton(
                android.R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface dialog,
                            int which) {
                    }
                });
        // 创建对话框
        AlertDialog dialog = builder.create();
        // 显示对话框
        dialog.show();
    }

    private void setAppVersionInfo(String version, String time) {
        mVersion.setText("当前版本：" + version);
        mTime.setText("更新时间：" + time);
    }

    /**
     * 返回前一个页面
     */
    private void onBackFragment() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            mActivity.getSupportFragmentManager().popBackStack();
        else
            mActivity.finish();
    }

}
