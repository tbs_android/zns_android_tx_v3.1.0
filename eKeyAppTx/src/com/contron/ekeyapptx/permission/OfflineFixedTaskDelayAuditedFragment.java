/**
 * 
 */
package com.contron.ekeyapptx.permission;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Offline;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.ConfigUtil;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * 离线鉴权审核-已审核离线鉴权
 * 
 * @author luoyilong
 *
 */

public class OfflineFixedTaskDelayAuditedFragment extends
		SwipeRefreshListFragment {
	public static final String KEY_LIST_REFRESH = "AuditListFragmentRefresh";
	private BasicActivity mActivity;
	private RequestHttp requestHttp;

	private ContronAdapter<Offline> mAdapter;

	public static OfflineFixedTaskDelayAuditedFragment newInstance() {
		OfflineFixedTaskDelayAuditedFragment fragment = new OfflineFixedTaskDelayAuditedFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		
		mAdapter = new ContronAdapter<Offline>(mActivity,
				R.layout.fragment_fixedtask_audited, new ArrayList<Offline>()) {

			@Override
			public void setViewValue(ContronViewHolder holder,
					final Offline item, final int position) {

				holder.setText(R.id.edtfixedtaskname,
						"申  请  人：" + item.getApply_by());

				holder.setText(R.id.edtapplyinfo,
						"申请时间：" + DateUtil.timeCompare(item.getApply_at()));

				holder.setText(R.id.tx_delayeddate,
						"延时时长：" + String.valueOf(item.getDuration()/60) + "(小时)");

				holder.setText(R.id.edtreplyinfo,
						"审核时间：" + DateUtil.timeCompare(item.getVerify_at()));

				holder.setText(R.id.tx_statu, item.getStatus());

				if (TextUtils.equals(item.getStatus(), "已拒绝"))
					holder.setTextColor(R.id.tx_statu, Color.RED);
				else if (TextUtils.equals(item.getStatus(), "待下载"))
					holder.setTextColor(R.id.tx_statu, Color.BLACK);
			}

		};
		setLoadMoreView();
		setListAdapter(mAdapter);

		super.onActivityCreated(savedInstanceState);
	}

	@Override
	protected void createLoadMoreView(LayoutInflater inflater) {
		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);


		// 下一页按钮
		loadMoreButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downLoadReply(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
	}

	@Override
	public void onResume() {
		super.onResume();
		
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item8_name);
		if (ConfigUtil.getInstance(mActivity).getBoolean(KEY_LIST_REFRESH)) {
			mcurrentItem = 0;
			downLoadReply(mcurrentItem);
			ConfigUtil.getInstance(mActivity).setBoolean(KEY_LIST_REFRESH,
					false);
		}
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			onResume();
		}
	}

	@Override
	public void onListRefresh() {
		super.onListRefresh();

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
		mcurrentItem = 0;
		downLoadReply(mcurrentItem);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();// getSupportFragmentManager().popBackStack();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 下载已审核离线鉴权
	 */
	private void downLoadReply(int currentItem) {

		User user = EkeyAPP.getInstance().getUser();
		if (user != null && user.isOnline) {// 非离线模式下载

			if (currentItem == 0) {// 当前页数为0时清空数据
				mAdapter.setEmpty();
				mAdapter.notifyDataSetChanged();
			}
			List<String> status = new ArrayList<String>();
			status.add("待下载");
			status.add("已拒绝");
			JSONArray jsonstatu = new JSONArray(status);
			// 设置参数
			RequestParams params = new RequestParams(mActivity.getUser());
//			params.putParams("status", jsonstatu);
			params.putParams("page", currentItem);

			String url = String.format(Globals.OFFLINE,
					user.getUsername());
			requestHttp.doPost(url, params, new HttpRequestCallBackString() {
				@Override
				public void resultValue(final String value) {
					mActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (value.contains("success")) {
								List<Offline> offlist;
								try {
									Gson gson = new Gson();
									JSONObject jsonObject = new JSONObject(
											value);
									JSONArray offlistgson = jsonObject
											.getJSONArray("items");
									offlist = gson.fromJson(
											offlistgson.toString(),
											new TypeToken<List<Offline>>() {
											}.getType());

									// 获取总页数
									if (mcurrentItem == 0) {
										mItemCount = jsonObject
												.getInt("pages");
									}

									if (offlist != null && offlist.size() > 0)
										mAdapter.addAllDatas(offlist);

								} catch (JSONException e) {
									e.printStackTrace();
								}

							}
							
							setRefreshing(false);

						}
					});

				}
			});

		}

	}
}
