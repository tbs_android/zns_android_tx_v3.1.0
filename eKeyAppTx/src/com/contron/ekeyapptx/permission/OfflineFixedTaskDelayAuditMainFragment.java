/**
 * 
 */
package com.contron.ekeyapptx.permission;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.lidroid.xutils.ViewUtils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 离线鉴权审核
 * @author guanhaiping
 * 
 */
public class OfflineFixedTaskDelayAuditMainFragment extends Fragment {
	
	private BasicActivity mActivity;

	public static OfflineFixedTaskDelayAuditMainFragment newInstance() {
		return new OfflineFixedTaskDelayAuditMainFragment();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BasicActivity) getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_fixedtask_auditmain, container,
				false);
		ViewUtils.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.add(R.id.fragment1, OfflineFixedTaskDelayAuditFragment.newInstance(),
				OfflineFixedTaskDelayAuditFragment.class.getSimpleName());
		transaction.add(R.id.fragment2, OfflineFixedTaskDelayAuditedFragment.newInstance(),
				OfflineFixedTaskDelayAuditedFragment.class.getSimpleName());
		transaction.commit();
	}
}
