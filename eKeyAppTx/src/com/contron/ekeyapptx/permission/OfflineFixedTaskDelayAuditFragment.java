package com.contron.ekeyapptx.permission;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Offline;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.ConfigUtil;
import com.contron.ekeypublic.view.InputDialogFragment;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 离线鉴权审核-未审核离线鉴权
 * 
 * @author luoyilong
 *
 */
public class OfflineFixedTaskDelayAuditFragment extends
		SwipeRefreshListFragment {
	public static final String KEY_LIST_REFRESH = "AuditListFragmentRefresh";
	private BasicActivity mActivity;
	private RequestHttp requestHttp;
	private ContronAdapter<Offline> mAdapter;

	public static OfflineFixedTaskDelayAuditFragment newInstance() {
		OfflineFixedTaskDelayAuditFragment fragment = new OfflineFixedTaskDelayAuditFragment();
		return fragment;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setHasOptionsMenu(true);

		mAdapter = new ContronAdapter<Offline>(mActivity,
				R.layout.fragment_fixedtask_audit, new ArrayList<Offline>()) {

			@Override
			public void setViewValue(ContronViewHolder holder,
					final Offline item, final int position) {

				holder.setText(android.R.id.text2,
						"申  请  人：" + item.getApply_by());
				String time = String.valueOf(item.getDuration()/60);
				holder.setText(R.id.TextView01, "延时时长：" + time + "(小时)");

				holder.findView(R.id.btn_audit_no).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								InputDialogFragment inputDialogFragment = new InputDialogFragment();
								inputDialogFragment
										.setTitleId(R.string.audit_dialog_title);
								inputDialogFragment
										.setHintId(R.string.audit_dialog_hiht);
								inputDialogFragment
										.setOnInputTextListener(new InputDialogFragment.OnInputTextListener() {
											@Override
											public void OnInputText(String text) {
												updateFixedTaskDelayApplyState(
														"拒绝", item.getId(),
														text, position);
											}
										});
								inputDialogFragment.show(
										mActivity.getFragmentManager(),
										"inputDialogFragment");

							}
						});
				holder.findView(R.id.btn_audit_yes).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								updateFixedTaskDelayApplyState("批准",
										item.getId(), "", position);

							}
						});
			}

		};
		setLoadMoreView();
		setListAdapter(mAdapter);

		super.onActivityCreated(savedInstanceState);
	}

	@Override
	protected void createLoadMoreView(LayoutInflater inflater) {
		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = (RadioButton) loadMoreView.findViewById(R.id.rad_next);


		// 下一页按钮
		loadMoreButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downLoadApply(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
	}

	@Override
	public void onResume() {
		super.onResume();

		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item8_name);
		if (ConfigUtil.getInstance(mActivity).getBoolean(KEY_LIST_REFRESH)) {
			mcurrentItem = 0;
			downLoadApply(mcurrentItem);
			ConfigUtil.getInstance(mActivity).setBoolean(KEY_LIST_REFRESH,
					false);
		}
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			onResume();
		}
	}

	@Override
	public void onListRefresh() {
		super.onListRefresh();

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
		mcurrentItem = 0;
		downLoadApply(mcurrentItem);
	}

	/**
	 * 下载未审核离线鉴权
	 */
	private void downLoadApply(int currentItem) {

		User user = EkeyAPP.getInstance().getUser();
		if (user != null && user.isOnline) {// 非离线模式下载

			if (currentItem == 0) {// 当前页数为0时清空数据
				mAdapter.setEmpty();
				mAdapter.notifyDataSetChanged();
			}
			List<String> status = new ArrayList<String>();
			status.add("待审批");
			JSONArray json = new JSONArray(status);

			// 设置参数
			RequestParams params = new RequestParams(mActivity.getUser());
			params.putParams("status", json);
			params.putParams("page", currentItem);

			requestHttp.doPost(Globals.OFFLINE, params,
					new HttpRequestCallBackString() {
						@Override
						public void resultValue(final String value) {

							mActivity.runOnUiThread(new Runnable() {
								@Override
								public void run() {

									if (value.contains("success")) {// 返回成功
										List<Offline> offlist;
										try {
											Gson gson = new Gson();
											JSONObject jsonObject = new JSONObject(
													value);
											JSONArray offlistgson = jsonObject
													.getJSONArray("items");
											offlist = gson.fromJson(
													offlistgson.toString(),
													new TypeToken<List<Offline>>() {
													}.getType());

											// 获取总页数
											if (mcurrentItem == 0) {
												mItemCount = jsonObject
														.getInt("pages");
											}

											if (offlist != null
													&& offlist.size() > 0)
												mAdapter.addAllDatas(offlist);

										} catch (JSONException e) {
											e.printStackTrace();
										}

									}

									setRefreshing(false);
								}
							});

						}
					});

		}

	}

	/**
	 * 审批离线鉴权
	 * 
	 * @param action
	 *            批准|拒绝
	 * @param oid
	 * @param memo
	 *            操作的附言
	 * @param position
	 *            索引
	 */
	private void updateFixedTaskDelayApplyState(String action, int oid,
			String memo, final int position) {

		User user = EkeyAPP.getInstance().getUser();
		if (user != null && user.isOnline) {// 非离线模式下载
			mActivity.startDialog(R.string.progressDialog_title_submit);
			// 设置参数
			RequestParams params = new RequestParams(mActivity.getUser());
			params.putParams("action", action);
			params.putParams("memo", memo);

			// 请求接口
			String url = String.format(Globals.OFFLINE_OID_VERIFY, oid);

			requestHttp.doPost(url, params, new HttpRequestCallBackString() {
				@Override
				public void resultValue(final String value) {

					mActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							mActivity.dismissDialog();
							if (value.contains("success")) {// 返回成功
								mActivity
										.showToast(R.string.offline_audit_sussmsg);
								mAdapter.removeDatas(position);
							} else {// 返回失败
								String error = "";
								try {
									JSONObject jsonObject = new JSONObject(
											value);
									error = jsonObject.getString("error");
								} catch (JSONException e) {

									e.printStackTrace();
								}
								mActivity.showToast(error);
							}
						}
					});

				}
			});
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
