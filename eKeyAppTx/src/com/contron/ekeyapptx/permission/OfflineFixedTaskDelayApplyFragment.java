package com.contron.ekeyapptx.permission;

import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Offline;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.AppUtils;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.R.id;
import com.contron.ekeypublic.util.FileUtil;
import com.contron.ekeypublic.util.ImageUtil;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.bitmap.BitmapCommonUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;

/**
 * 离线鉴权申请
 * 
 * @author luoyilong
 *
 */

public class OfflineFixedTaskDelayApplyFragment extends Fragment {
	private static final int REQUEST_CODE_HEAD = 101;
	private static final int REQUEST_CODE_IDENTITY = 102;
	private BasicActivity mActivity;
	@ViewInject(R.id.lastapplytime)
	private EditText edtLastApplyTime;// 最后一次有效申请时间
	@ViewInject(R.id.lastdelaytimelong)
	private EditText edtLastDelayTimelong;// 最后一次有效申请时长
	@ViewInject(R.id.fixedtask_delay)
	private EditText edtDelay;// 当前申请时长
	@ViewInject(R.id.fixedtask_apply_submit)
	private Button btnSubmit;// 提交申请

	@ViewInject(R.id.fixedtask_apply_iv_head)
	private ImageView ivHead;
	@ViewInject(R.id.fixedtask_apply_iv_identity)
	private ImageView ivIdentity;

	private String picSaveDir = "";// 文件路径
	private String picHeadLargePath = "";// 头像保存文件
	private String picIdentityLargePath = "";// 身份证文件
	private String picHeadThumbnailPath = "";// 头像缩略图文件
	private String picIdentityThumbnailPath = "";// 身份证缩略图文件

	private boolean isDelPhoto;//是否删除图片

	private RequestHttp requestHttp;// 后台服务请求
	private BitmapUtils bitmapUtils;// 图片处理对象

	public static OfflineFixedTaskDelayApplyFragment newInstance() {
		OfflineFixedTaskDelayApplyFragment fragment = new OfflineFixedTaskDelayApplyFragment();
		// fragment.setArguments(new Bundle());
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BasicActivity) getActivity();
		bitmapUtils = new BitmapUtils(mActivity).configDefaultBitmapMaxSize(
				BitmapCommonUtils.getScreenSize(mActivity))
				.configDefaultBitmapConfig(Bitmap.Config.RGB_565);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_fixedtask_apply,
				container, false);
		ViewUtils.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item7_name);
		requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		getLastoffling();

		picSaveDir = mActivity.getExternalCacheDir().getPath()
				+ "/OfflineApplyPic/";// 文件路径
		// // 输入延时时长并作判断
		// edtDelay.addTextChangedListener(new TextWatcher() {
		// public void afterTextChanged(Editable edt) {
		// String temp = edt.toString();
		// int posDot = temp.indexOf(".");
		// if (posDot <= 0) {
		// if (temp.length() <= 4) {
		// return;
		// } else {
		// edt.delete(4, 5);
		// return;
		// }
		// }
		// if (temp.length() - posDot - 1 > 2) {
		// edt.delete(posDot + 3, posDot + 4);
		// }
		// }
		//
		// public void beforeTextChanged(CharSequence arg0, int arg1,
		// int arg2, int arg3) {
		// }
		//
		// public void onTextChanged(CharSequence arg0, int arg1, int arg2,
		// int arg3) {
		// }
		// });
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (isDelPhoto) {
			File picFile = new File(picSaveDir);
			if (picFile.exists()) {// 文件存在
				FileUtil.RecursionDeleteFile(picFile);// 删除文件
			}
		}
	}

	@OnClick({R.id.fixedtask_apply_submit, R.id.fixedtask_apply_iv_head, R.id.fixedtask_apply_iv_identity})
	public void onClick(View v) {
		switch (v.getId()) {
			case id.fixedtask_apply_submit:
				attemptOfflineApply();
				break;
			case R.id.fixedtask_apply_iv_head:
				picHeadLargePath = startCamera(REQUEST_CODE_HEAD);
				break;
			case R.id.fixedtask_apply_iv_identity:
				picIdentityLargePath = startCamera(REQUEST_CODE_IDENTITY);
				break;
			default:
				break;
		}

	}

	private void attemptOfflineApply() {
		String date = edtDelay.getText().toString();

		if (TextUtils.isEmpty(date)) {
			mActivity.showToast("申请延时时长不能为空！");
			return;
		}
		int index = date.indexOf("0");
		if (index == 0 && date.length() > 1) {
			mActivity.showToast("申请延时时长格式不对,首位不能为0！");
			return;
		}

		Integer delayTime = Integer.parseInt(date);
		if (delayTime > 24 || delayTime < 1) {
			mActivity.showToast("申请延时时长,超出正常范围！");
			return;
		}

		if (TextUtils.isEmpty(picHeadThumbnailPath)) {
			mActivity.showToast("请拍摄头像照片！");
			return;
		}

		if (TextUtils.isEmpty(picIdentityThumbnailPath)) {
			mActivity.showToast("请拍摄身份证照片！");
			return;
		}

		offlineapply(delayTime * 60);// 申请离线鉴权
	}

	/**
	 * 申请离线鉴权
	 */
	private void offlineapply(Integer date) {
		// 参数设置
		mActivity.startDialog(R.string.progressDialog_title_submit);
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("duration", date);

		requestHttp.doPost(Globals.OFFLINE_APPLY, params,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {

						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {// 返回成功
								// mActivity
								// .showToast(R.string.offline_fixedtaskdelay_apply_msg);

									int tid = 0;
									try {
										JSONObject json = new JSONObject(value);
										tid = json.getInt("id");
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									if (tid != 0)
										uploadImage(tid);// 上传图片

									isDelPhoto = true;

									mActivity.showMsgBox("提示", "离线鉴权申请提交成功！",
											new OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
													mActivity.finish();
												}
											});

								} else {// 返回失败
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}

									mActivity.showToast(error);
								}
							}
						});
					}
				});

	}

	/**
	 * 上传图片
	 *
	 * @param tid
	 */
	private void uploadImage(int tid) {
		String Url1 = "/offline/" + tid + "/portrait?s=1&c=android&v=1";
		String Url2 = "/offline/" + tid + "/idcard?s=1&c=android&v=1";

		requestHttp.uploadImage(Url1, picHeadThumbnailPath, "portrait");

		requestHttp.uploadImage(Url2, picIdentityThumbnailPath, "idcard");

	}

	private String startCamera(int requestCode) {
		// 判断存储卡是否可以用，可用进行存储
		if (!AppUtils.sdCardIsExsit()) {
			mActivity.showToast("无SD卡");
			return "";
		}
		// 存放照片的文件夹
		File savedir = new File(picSaveDir);
		if (!savedir.exists()) {
			savedir.mkdirs();
		}
		String timeStamp = DateUtil.getCurrentDate("yyyyMMddHHmmss");
		String t_photoName = timeStamp + ".jpg";// 照片名称
		String largePath = picSaveDir + t_photoName;// 该照片的绝对路径
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri uri;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			String authorities = mActivity.getPackageName() + ".fileProvider";
			uri= FileProvider.getUriForFile(mActivity, authorities, new File(largePath));
		} else {
			uri = Uri.fromFile(new File(largePath));
		}
		intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		startActivityForResult(intent, requestCode);
		return largePath;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode != Activity.RESULT_CANCELED) {
			switch (requestCode) {
				case REQUEST_CODE_HEAD:

					// 创建一个线程处理，避免返回后2秒才回到当前界面
					Thread thread = new Thread(new Runnable() {
						@Override
						public void run() {
							picHeadThumbnailPath = createThumbnailPic(
									picHeadLargePath, "head_", ivHead);
						}
					});
					thread.start();

					break;
				case REQUEST_CODE_IDENTITY:

					// 创建一个线程处理，避免返回后2秒才回到当前界面
					Thread thread1 = new Thread(new Runnable() {
						@Override
						public void run() {
							picIdentityThumbnailPath = createThumbnailPic(
									picIdentityLargePath, "identity_",
									ivIdentity);
						}
					});
					thread1.start();

					break;
			}
		} else {
			// 删除图片
			FileUtil.deleteFileWithPath(picHeadLargePath);
			FileUtil.deleteFileWithPath(picIdentityLargePath);
		}
	}

	/**
	 * 创建缩略图，并显示在 ImageView
	 *
	 * @author hupei
	 * @date 2015年5月28日 上午10:22:22
	 */
	private String createThumbnailPic(String largePath, String smallFileName,
									  ImageView iv) {
		String thumbnailPath = picSaveDir + "small_" + smallFileName
				+ FileUtil.getFileName(largePath);
		File file = new File(thumbnailPath);
		// 判断是否已存在缩略图
		if (!file.exists()) {
			try {
				// 压缩生成上传的1920宽度图片
				ImageUtil.createImageThumbnail(mActivity, largePath,
						thumbnailPath, 1920, 80);
				// 删除原图
				FileUtil.deleteFileWithPath(largePath);
				displayPhoto(iv, thumbnailPath);
				return thumbnailPath;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "";

	}

	/**
	 * 把圖片显示到UI上
	 *
	 * @param iv
	 *            ImageView
	 * @param thumbnailPath
	 *            文件
	 */
	private void displayPhoto(final ImageView iv, final String thumbnailPath) {
		if (TextUtils.isEmpty(thumbnailPath))
			return;

		if (bitmapUtils != null) {
			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					bitmapUtils.display(iv, thumbnailPath);// 显示
				}
			});

		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();// getSupportFragmentManager().popBackStack();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 下载用户最后一次的 离线鉴权
	 * 
	 */
	private void getLastoffling() {

		User user = EkeyAPP.getInstance().getUser();

		// 设置参数
		RequestParams prams = new RequestParams(mActivity.getUser());

		// 请求接口
		String url = String.format(Globals.USER_NAME_LAST_OFFLINE,
				user.getUsername());

		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {

			@Override
			public void resultValue(final String value) {

				mActivity.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						if (value.contains("success")) {
							Offline offline = JsonTools.getoffline(value);
							if (offline != null
									&& !TextUtils.isEmpty(offline.getApply_at())) {
								float duration = offline.getDuration();
								edtLastApplyTime.setText(DateUtil
										.timeCompare(offline.getApply_at()));
								edtLastDelayTimelong.setText(String
										.valueOf(duration / 60));
							}
						}
					}
				});

			}
		});

	}

}
