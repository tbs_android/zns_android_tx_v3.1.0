package com.contron.ekeyapptx;

import java.util.ArrayList;
import java.util.List;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.entities.WarnMsg;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.util.DateUtil;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

public class WarnMsgActivity extends BasicActivity {

	@ViewInject(R.id.listview_warnmsg)
	private ListView mListView;// 显示警告信息的ListView

	@ViewInject(R.id.tx_empty)
	private TextView empty;

	private ContronAdapter<WarnMsg> mAdapter;// 适配器
	private BasicActivity mActivity;
	private List<WarnMsg> listWarnMsg = new ArrayList<WarnMsg>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_warn_msg);
		ViewUtils.inject(this);
		mActivity = this;
		this.getBasicActionBar().setTitle("警告信息");

		// 初始化适配器、从EkeyAPP获取警告信息
		mAdapter = new ContronAdapter<WarnMsg>(this, R.layout.activity_warnmsg,
				new ArrayList<WarnMsg>()) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					WarnMsg item, int position) {

				String date = DateUtil.timeCompare(item.date);

				String msg = item.deviceName;

				viewHolder.setText(R.id.tx_warnmsg_name, date + "\n" + msg);

			}
		};

		mListView.setAdapter(mAdapter);
		mListView.setEmptyView(empty);
	}

	@Override
	protected void onResume() {
		super.onResume();
		listWarnMsg = EkeyAPP.getInstance().getListWarnMsg();
		if (listWarnMsg != null && listWarnMsg.size() > 0) {
			mAdapter.clearDatas();
			mAdapter.addAllDatas(listWarnMsg);
		}

		EventBusManager.getInstance().register(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		EkeyAPP.getInstance().setWarnCount(0);
		EventBusManager.getInstance().unregister(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();		
	}

	/**
	 * 更新警告信息记录
	 * 
	 * @param msg
	 */
	public void onEventMainThread(final WarnMsg msg) {

		mActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				mAdapter.addData(msg);// 添加到适配器中
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.warn_msg, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			finish();
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * 跳转到WarnMsgActivity界面
	 * 
	 * @param activity
	 */
	public static void gotoActivity(Activity activity) {
		activity.startActivity(new Intent(activity, WarnMsgActivity.class));
	}
}
