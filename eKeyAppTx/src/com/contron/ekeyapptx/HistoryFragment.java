package com.contron.ekeyapptx;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtSearchManage;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.bluetooth.BtTimeOut;
import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBackReplyMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBackReplyMsg.BtRecvCollectBackData;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBackReplyMsg.BtRecvBackData;
import com.contron.ekeypublic.bluetooth.code.send.BtSendBackReplyMsg;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType.BtHex;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.util.TaskType;
import com.google.gson.Gson;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class HistoryFragment extends Fragment {

	private BtMsgService mBtMsgService;// 蓝牙服务
	protected BtSearchManage mBtSearchManage;// 蓝牙管理工厂
	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求服务

	private BtSendBackReplyMsg sendBackReply = new BtSendBackReplyMsg();
	private int frameCount = 0;// 总帧数
	private int currentFrameCount = 0;// 当前帧数
	private int uid;// 任务操作用户ID
	private SparseArray<String> taskIdList = null;// 查询返回任务ID
	private String currentTaskId = "";// 当前回传任务ID
	private int taskNume = 0;// 任务总数
	private String taskType = "";// 当前回传任务类型
	private int tid = -1;

	// 钥匙返回操作记录集合
	private List<BtRecvBackData> btRecvBackDatas = new ArrayList<BtRecvBackData>();

	// 采码记录
	private List<BtRecvCollectBackData> btRecvBackCollectDatas = new ArrayList<BtRecvCollectBackData>();

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,
				IBinder service) {
			mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
			if (mBtMsgService.initialize()) {
                mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
				LogUtils.i("蓝牙服务绑定成功");
				mBtSearchManage = new BtSearchManage(mActivity,
						mBtMsgService.getBluetoothAdapter(),
						new BtSearchManage.OnSelectBtKeyListener() {
							@Override
							public void onSelectBtKey(Smartkey btKey) {
								// TODO 选择蓝牙
								scanLeDevice(false);
								mActivity
										.startDialog(R.string.main_bluetooth_msg0);
								String bttmp = btKey.getBtaddr().replaceAll(
										"(.{2})", "$1:");
								String btaddr = bttmp.substring(0,
										bttmp.length() - 1);
								EkeyAPP.getInstance().setBtAddress(btaddr);
								connBt();
							}
						});

			} else {
				LogUtils.e("Unable to initialize Bluetooth");
				mActivity.showToast("绑定蓝牙服务失败");
				mActivity.finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBtMsgService = null;
		}
	};

	public static HistoryFragment newInstance() {
		HistoryFragment fragment = new HistoryFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_history, container, false);
		ViewUtils.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item22_name);
		EventBusManager.getInstance().register(this);
		Intent gattServiceIntent = new Intent(mActivity, BtMsgService.class);
		mActivity.bindService(gattServiceIntent, mServiceConnection, 1);
	}

	/**
	 * 登录、用户注册
	 * 
	 * @param v
	 * @throws DbException
	 */
	@OnClick({ R.id.but_return })
	public void onClickLogin(View v) throws DbException {
		switch (v.getId()) {
		case R.id.but_return:// 回传操作记录
			EkeyAPP.getInstance().setBtAddress("");
			scanLeDevice(true);
			break;
		default:
			break;
		}
	}

	/**
	 * 启动、停止\搜索
	 * 
	 * @param enable
	 */
	private void scanLeDevice(boolean enable) {

		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
			if (!mBtMsgService.isConnected())
				mBtMsgService.removeCallbacksConn();
		}

		if (mBtSearchManage != null)
			mBtSearchManage.scanLeDevice(enable);
	}

	/**
	 * 连接蓝牙
	 */
	protected void connBt() {
		LogUtils.e("连接开始");
		if (mBtMsgService != null
				&& !TextUtils.isEmpty(EkeyAPP.getInstance().getBtAddress()))
			mBtMsgService.autoConnect(EkeyAPP.getInstance().getBtAddress());
		else {
			if (mBtSearchManage != null)
				mBtSearchManage.scanLeDevice(true);
		}
	}

	/**
	 * 断开蓝牙连接
	 */
	private void disconnect() {
		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		scanLeDevice(false);
		// 注销EventBusManager事件
		EventBusManager.getInstance().unregister(this);
		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
			if (!mBtMsgService.isConnected())
				mBtMsgService.removeCallbacksConn();
		}
		// 解除蓝牙服务邦定
		mActivity.unbindService(mServiceConnection);
		EkeyAPP.getInstance().removeActivity(mActivity);
	}

	/**
	 * 接收BtMsgService发出的状态消息<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtState state) {
		switch (state) {
		case CONNECTING:// 连接中
			break;
		case DISCONNECTED:// 连接断开
			break;
		case CONNECTED:// 连接成功
			break;
		case CONNECTOUTTIME:// 连接超时
			mActivity.dismissDialog();
			showMsgBox("蓝牙连接超时,请重试!");
			break;
		case DISCOVERED:// 发现设备，可发送蓝牙命令了
			mActivity.dismissDialog();
			mActivity.startDialog("任务回传中...请稍候！");
			mBtMsgService.writeValue(sendBackReply.createTaskSelectFrameMsg());
			break;
		default:
			break;
		}
	}

	/**
	 * 接收mBtTimeOut超时连接<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author luoyilong
	 * @date 2016年6月24日 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtTimeOut mBtTimeOut) {
		mActivity.dismissDialog();
		showMsgBox("回传任务失败,请重试！");
		disconnect();
		LogUtils.i("mBtTimeOut");
	}

	/**
	 * 接收BtMsgService发出的蓝牙规约数据<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午10:25:18
	 * @param btHex
	 */
	public void onEventMainThread(BtHex btHex) {
		final String read = btHex.commd;
		if (!TextUtils.isEmpty(read)) {
			BtRecvMsg btRecvMsg = BtMsgTools.recv(read);
			if (btRecvMsg != null) {

				if (BtCommand.CODE_05.value.equals(btRecvMsg.command)) {// 钥匙中任务查询应答

					taskIdList = sendBackReply.getAllTaskId(btRecvMsg.data);// 获取查询出来的任务数
					taskNume = taskIdList.size();
					startTask(false);

				} else if (btRecvMsg instanceof BtRecvBackReplyMsg) {// 任务操作记录回传应答

					// 是否为响应内容
					final BtRecvBackReplyMsg backReplyMsg = (BtRecvBackReplyMsg) btRecvMsg;

					if (BtFunctionCode.CODE_00.value
							.equals(backReplyMsg.functionCode)) {// 接收总帧
						frameCount = backReplyMsg.getCountFrame();
						uid = backReplyMsg.getUserFrame();
						currentFrameCount = 1;
						getRecord();

					} else if (BtFunctionCode.CODE_01.value
							.equals(backReplyMsg.functionCode)) {// 接收帧信息

						if ("TransCollectRFID".equals(taskType)) {// 采码任务
							btRecvBackCollectDatas.addAll(backReplyMsg
									.parserCollectBackData());
						} else {// 操作记录
							btRecvBackDatas.addAll(backReplyMsg
									.parserBackData());
						}

						getRecord();

					}

				}

			} else {// 数据解析失败
				mActivity.dismissDialog();
				// mActivity.showMsgBox(R.string.shared_bt_error1);
				disconnect();
			}
		} else {// 接收数据为空
			mActivity.dismissDialog();
			// mActivity.showMsgBox(R.string.shared_bt_error2);
			disconnect();
		}
	}

	/**
	 * 循环开始任务回传
	 */
	private void startTask(boolean isdelect) {

		if (btRecvBackCollectDatas != null)
			btRecvBackCollectDatas.clear();// 清空数据

		if (btRecvBackDatas != null)
			btRecvBackDatas.clear();// 清空数据

		if (isdelect && taskIdList.size() > 0) {
			taskIdList.removeAt(0);
		}

		// 判断钥匙中是否存在当前任务
		if (null != taskIdList && taskIdList.size() > 0) {// 存在
			tid = taskIdList.keyAt(0);
			currentTaskId = taskIdList.get(tid);
			getType(currentTaskId);
			mBtMsgService.writeValue(sendBackReply
					.createTaskTitleFrameMsg(currentTaskId));
		} else {// 不存在
			mActivity.dismissDialog();
			disconnect();
			showMsgBox("回传任务成功,共" + taskNume + "个任务!");
		}

	}

	/**
	 * 获取任务类型
	 * 
	 * @param currentTaskId
	 */
	private void getType(String currentTaskId) {

		TaskType task = TaskType.valuesOf(currentTaskId.substring(0, 1));

		switch (task) {
		case TransAllUnlock://全站解锁
			taskType = "TransAllUnlock";
			break;
		case TransTemp://临时任务
			taskType = "TransTemp";
			break;
		case TransGuardTour://电子巡更
			taskType = "TransGuardTour";
			break;
		case TransSectionalLock://部门锁上锁任务
			taskType = "TransSectionalLock";
			break;
		case TransSectionalUnlock://部门锁解锁任务
			taskType = "TransSectionalUnlock";
			break;
		case TransFixed://固定任务
			taskType = "TransFixed";
			break;
		case TransPermanent://预置任务
			taskType = "TransPermanent";
			break;
		case TransCollectRFID://采码任务
			taskType = "TransCollectRFID";
			break;
		case TransTask://任务授权
			taskType = "TransTask";
			break;
		default://其它任务
			taskType = TaskType.other.value;
			break;
		}
	}

	/**
	 * 获取钥匙中的操作记录并处理
	 */
	private void getRecord() {

		if (frameCount == 0)// 没有操作记录
		{ // 删除任务记录
			mBtMsgService.writeValue(sendBackReply
					.createDeletTaskFrameMsg(currentTaskId));

			startTask(true);
		} else if (currentFrameCount > frameCount) {// 操作记录获取完成

			if ("TransCollectRFID".equals(taskType)) {// 采码任务
				updateCollectTask();
			} else {// 操作记录
				updateTask();// 上传操作记录
			}
		} else {// 获取操作记录
			mBtMsgService.writeValue(sendBackReply
					.createTaskFrameMsg(currentFrameCount++));
		}

	}

	/**
	 * 回传任务操作记录
	 */
	private void updateTask() {

		// 无操作记录时直接返回
		if (btRecvBackDatas.size() == 0)
			return;

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());

		params.putParams("tid", tid);
		params.putParams("action", taskType);
		params.putParams("uid", uid);
		
		if (taskType.equals("TransAllUnlock")) {//全站解锁任务，没有任务ID
			params.putParams("tid", currentTaskId);
		}
		
		String data = (new Gson()).toJson(btRecvBackDatas);
		JSONArray RecvBackDataArray = null;
		try {
			RecvBackDataArray = new JSONArray(data);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		params.putParams("taskHistory", RecvBackDataArray);

		// 请求接口
		requestHttp.doPost(Globals.HISTORY_PRESET_TASK_NEW, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								if (value.contains("success")) {

									mBtMsgService.writeValue(sendBackReply
											.createDeletTaskFrameMsg(currentTaskId));

									startTask(true);

								} else {
									mActivity.dismissDialog();
									disconnect();
									String error;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										error = jsonObject.getString("error");
										showMsgBox(error);
									} catch (JSONException e) {
										e.printStackTrace();
									}

								}
							}
						});

					}
				});
	}

	/**
	 * 回传采码任务操作记录
	 */
	private void updateCollectTask() {

		// 无操作记录时直接返回
		if (btRecvBackCollectDatas.size() == 0)
			return;

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());

		String data = (new Gson()).toJson(btRecvBackCollectDatas);
		JSONArray RecvBackDataArray = null;
		try {
			RecvBackDataArray = new JSONArray(data);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		params.putParams("lockset", RecvBackDataArray);

		// 请求接口
		requestHttp.doPost(Globals.LOCKSET_UPDATE_RFID, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								if (value.contains("success")) {

									mBtMsgService.writeValue(sendBackReply
											.createDeletTaskFrameMsg(currentTaskId));

									startTask(true);

								} else {
									mActivity.dismissDialog();
									disconnect();
									String error;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										error = jsonObject.getString("error");
										showMsgBox(error);
									} catch (JSONException e) {
										e.printStackTrace();
									}

								}
							}
						});

					}
				});
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// inflater.inflate(R.menu.temporary, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			mActivity.finish();
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 弹出提示框
	 */
	public void showMsgBox(String msg) {
		mActivity.showMsgBox("提示", msg, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
	}

}
