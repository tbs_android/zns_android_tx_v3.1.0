package com.contron.ekeyapptx.map;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.Polyline;
import com.amap.api.maps.model.PolylineOptions;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.WorkPatrolDetail;
import com.contron.ekeypublic.http.RequestHttp;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * AMapV2地图中简单介绍一些Marker的用法.
 */
public class InfoWindowActivity extends BasicActivity implements OnClickListener, AMap.OnMarkerClickListener {
	private AMap aMap;
	private MapView mapView;
	private Marker marker;
	private BasicActivity mActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = this;
		setContentView(R.layout.marker_activity);
		/*
		 * 设置离线地图存储目录，在下载离线地图或初始化地图设置; 使用过程中可自行设置, 若自行设置了离线地图存储的路径，
		 * 则需要在离线地图下载和使用地图页面都进行路径设置
		 */
		// Demo中为了其他界面可以使用下载的离线地图，使用默认位置存储，屏蔽了自定义设置
		// MapsInitializer.sdcardDir =OffLineMapUtils.getSdCacheDir(this);
		mapView = (MapView) findViewById(R.id.map);
		mapView.onCreate(savedInstanceState); // 此方法必须重写
		downloadWorkPatrolDetail(0);

	}

	/**
	 * 初始化AMap对象
	 */
	private void init(List<WorkPatrolDetail> workPatrolDetails) {
		aMap = mapView.getMap();
		aMap.setOnMarkerClickListener(this);
		ArrayList<LatLng> latLngs = new ArrayList<LatLng>();
		for (int i = 0; i < workPatrolDetails.size(); i++) {
			WorkPatrolDetail workPatrolDetail = workPatrolDetails.get(i);
			if (null == workPatrolDetail.getObject())
				continue;
			LatLng latLng = new LatLng(workPatrolDetail.getObject().getLat(), workPatrolDetail.getObject().getLng());
			if (TextUtils.isEmpty(workPatrolDetail.getPatrol_by()) || TextUtils.isEmpty(workPatrolDetail.getPatrol_at())) {
			} else {
				latLngs.add(latLng);
			}

			boolean isStatusOK = "正常".equals(workPatrolDetail.getObject().getStatus()) &&
					"正常".equals(workPatrolDetail.getObject().getLockstatus());

			addMarkersToMap(latLng, (i + 1) + "",
					workPatrolDetail.getObject().getType(),
					workPatrolDetail.getObject().toString(),
					isStatusOK);
			if (i == (workPatrolDetails.size() - 1)) {
				aMap.moveCamera(CameraUpdateFactory.changeLatLng(latLng));
			}
		}
		if (latLngs.size() == 0) {
			showToast("设备信息为空，请检查服务器数据是否正常！");
		}
		Polyline polyline =aMap.addPolyline(new PolylineOptions().addAll(latLngs).width(6).color(Color.RED));

	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mapView.onResume();
		getBasicActionBar().setTitle("轨迹");
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onPause() {
		super.onPause();
		mapView.onPause();
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mapView.onSaveInstanceState(outState);
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
	}


	private void addMarkersToMap(LatLng latlng, String text, String title, String detail, boolean isStatusOk) {
		marker=aMap.addMarker(new MarkerOptions()
				.position(latlng)
				.title(title)
				.snippet(detail)
				.draggable(true)
				.icon(BitmapDescriptorFactory.fromView(getMarkView(text, isStatusOk))));
		marker.showInfoWindow();

	}

	private View getMarkView(String text, boolean isStatusOK) {
		View markView = LayoutInflater.from(this).inflate(R.layout.mark_view, null, false);
		TextView numText = (TextView) markView.findViewById(R.id.num_text);
		ImageView markerImg = (ImageView) markView.findViewById(R.id.marker_img);
		numText.setText(text);
		if (!isStatusOK) {
			markerImg.setColorFilter(Color.RED);
			numText.setTextColor(Color.WHITE);
		}
		return markView;
	}


	@Override
	public void onClick(View v) {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				this.finish();
				return true;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 下载巡检详情
	 */
	private void downloadWorkPatrolDetail(int currentItem) {

		User user = mActivity.getUser();
		if (user == null)
			return;
		RequestHttp requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		// 设置参数
		String pid = getIntent().getExtras().getString("pid");
		JSONObject queryJsonObject = new JSONObject();
		try {
			queryJsonObject.put("k", user.getKValue());
			queryJsonObject.put("page", currentItem);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String url = String.format(Globals.WORKPATROL_FINISH_DETAIL, pid);
		requestHttp.doPost(url, queryJsonObject,
				new RequestHttp.HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();

								if (value.contains("success")) {
									ArrayList<WorkPatrolDetail> listTasking;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										JSONArray jsonArrayTempTask = jsonObject
												.getJSONArray("items");
										// 获取任务
										listTasking = new Gson().fromJson(
												jsonArrayTempTask.toString(),
												new TypeToken<List<WorkPatrolDetail>>() {
												}.getType());

										if (listTasking != null
												&& listTasking.size() > 0) {
											init(listTasking);
										}
									} catch (JSONException e) {
										e.printStackTrace();
									}

								} else {
									try {
										JSONObject json = new JSONObject(value);
										String error = json.getString("error");
										mActivity.showMsgBox(error);

									} catch (JSONException e) {
										e.printStackTrace();
									}

								}

							}
						});

					}
				});
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		if (aMap != null) {
			marker.showInfoWindow();
		}
		return true;
	}
}
