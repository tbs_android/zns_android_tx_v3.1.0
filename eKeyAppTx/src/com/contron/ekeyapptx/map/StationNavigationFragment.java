package com.contron.ekeyapptx.map;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Station;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.map.DisplayActivity;
import com.contron.ekeypublic.view.ClearEditText;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/**
 * A simple {@link Fragment} subclass.
 */
public class StationNavigationFragment extends Fragment {
    @ViewInject(R.id.lv_public)
    private ListView lisview;

    @ViewInject(R.id.tx_empty)
    private TextView empty;

    @ViewInject(R.id.btn_search)
    private TextView btnSearch;

    @ViewInject(R.id.et_search)
    private ClearEditText etSearch;


    private BasicActivity mActivity;
    private RequestHttp requestHttp;// 后台请求对象
    private ContronAdapter<Station> mAdapter;// 适配器
    private View loadMoreView;// 下一页视图
    private RadioButton loadMoreButton;// 下一页按钮
    private int mcurrentPage = 0;// 当前页数
    private int mPageCount = 1;// 总页数
    private User user=null;// 当前登录用户
    private List<Station> devices = new ArrayList<Station>();// 设备
    private ArrayList<DeviceObject> deviceObjects = new ArrayList<>();

    public static StationNavigationFragment newInstance() {
        StationNavigationFragment fragment = new StationNavigationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        ViewUtils.inject(this, view);

        loadMoreView = inflater.inflate(R.layout.item_footerview, null);
        loadMoreButton = loadMoreView.findViewById(R.id.rad_next);

        // 下一页按钮
        loadMoreButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mcurrentPage++;
                if (mcurrentPage < mPageCount)// 当前页小于总页数
                {
                    downloadStationDevices(mcurrentPage);
                } else {
                    mActivity.showMsgBox("已经是最后页了！");
                }

            }

        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadStationDevices(0);
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item41_name));

        mAdapter = new ContronAdapter<Station>(mActivity,
                R.layout.fragment_station_info, devices) {
            @Override
            public void setViewValue(ContronViewHolder holder,
                                     final Station item, final int position) {
                holder.setText(R.id.tx_station_name, item.getName());
                holder.setText(R.id.tx_section, item.getSection());
                holder.setText(R.id.tx_station_id, item.getSitecode());
                holder.setText(R.id.tx_company, item.getCompany_name());
                holder.setText(R.id.tx_controller_id, item.getControl_tool_ID());
                holder.setText(R.id.tx_work_mode, item.getControl_work_mode());
            }
        };
        lisview.addFooterView(loadMoreView); // 设置列表底部视图
        lisview.setAdapter(mAdapter);
        lisview.setEmptyView(empty);
        lisview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Station station = (Station) parent.getItemAtPosition(position);
                if (checkApkExist(mActivity, "com.autonavi.minimap")) {
                    mActivity.showMsgBox("导航", "是否跳转到高德地图进行导航？", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            goToGaode(String.valueOf(station.getLat()), String.valueOf(station.getLng()), station.getName());
                        }
                    });
                } else {
                    mActivity.showToast("请先安装高德地图才能进行导航");
                }
//                   else if (checkApkExist(mActivity, "com.baidu.BaiduMap")) {
//                    mActivity.showMsgBox("导航", "是否跳转到百度地图进行导航？", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            double lat = 0, lon = 0;
//                            bd_encrypt(station.getLat(), station.getLng(), lat, lon);
//                            goToBaidu(String.valueOf(lat), String.valueOf(lon), station.getName());
//                        }
//                    });
//                }
            }
        });

        downloadStationDevices(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item41_name);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.map, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.getItem(0);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackFragment();
                break;
            case R.id.menu_map:
                downloadStationDevices(-1);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 判断是否安装了地图
     * @param context
     * @return
     */
    private boolean checkApkExist(Context context, String packageName) {
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName,
                    PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * 跳转到高德地图进行路径规划
     * @param lat 目标横坐标
     * @param lon 目标纵坐标
     * @param distance 目的地名称
     */
    private void goToGaode(String lat, String lon, String distance) {
        StringBuffer stringBuffer = new StringBuffer("amapuri://route/plan/?sourceApplication=").append("amap");

        stringBuffer.append("&dlat=").append(lat)
                .append("&dlon=").append(lon)
                .append("&dname=").append(distance)
                .append("&dev=").append(0)
                .append("&t=").append(0);

        Intent intent = new Intent("android.intent.action.VIEW", android.net.Uri.parse(stringBuffer.toString()));
        intent.setPackage("com.autonavi.minimap");
        startActivity(intent);
    }

    /**
     * 跳转到百度地图进行路径规划
     * @param lat 目标横坐标
     * @param lon 目标纵坐标
     * @param distance 目的地名称
     */
    private void goToBaidu(String lat, String lon, String distance){
        StringBuffer stringBuffer = new StringBuffer("baidumap://map/navi?");
        stringBuffer.append("location=").append(lon).append(",").append(lat)
                .append("&src=").append("andr.contron.eKeyAppTx");

        Intent intent = new Intent();
        intent.setData(Uri.parse(stringBuffer.toString()));
        startActivity(intent);
    }

    /**
     * 坐标系转换，从高德坐标转换成百度坐标
     * @param gg_lat 高德纬度
     * @param gg_lon 高德经度
     * @param bd_lat 百度纬度
     * @param bd_lon 百度经度
     */
    void bd_encrypt(double gg_lat, double gg_lon, double bd_lat, double bd_lon) {
        double x = gg_lon, y = gg_lat;
        double z = sqrt(x * x + y * y) + 0.00002 * sin(y * Math.PI);
        double theta = atan2(y, x) + 0.000003 * cos(x * Math.PI);
        bd_lon = z * cos(theta) + 0.0065;
        bd_lat = z * sin(theta) + 0.006;
    }

    private void downloadStationDevices(final int currentPage) {
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        if (currentPage == 0) {// 当前页数为0时清空数据
            mcurrentPage = 0;
            devices.clear();
            mAdapter.notifyDataSetChanged();
        }
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());
        params.putParams("page", currentPage);
        params.putParams("pagesize", 20);
        String searchText = etSearch.getText().toString().trim();
        if (!"".equals(searchText)) {
            params.putParams("search", searchText);
        }
        int sid = user.getSid();

        // 请求接口
        String url = String.format(Globals.GET_OBJECT_DOWNWARD, sid);

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mActivity.dismissDialog();

                        if (value.contains("success")) {
                            ArrayList<Station> listDeviceObject;
                            // 获取站点列表
                            listDeviceObject = JsonTools.getStations(value);

                            if (listDeviceObject != null
                                    && listDeviceObject.size() > 0) {
                                if (currentPage >= 0) {
                                    mAdapter.setEmpty();
                                    mAdapter.addAllDatas(listDeviceObject);
                                } else {
                                    for (Station s : listDeviceObject) {
                                        DeviceObject deviceObject = Station.modelAconvertoB(s, DeviceObject.class);
                                        deviceObjects.add(deviceObject);
                                    }
                                    DisplayActivity.startActivity(mActivity, deviceObjects);
                                }
//                                setEmptyViewShown(false);
                            } else {
//                                setEmptyText("暂无数据！");
//                                setEmptyViewShown(true);
                            }
                        } else {
//                            setEmptyText("暂无数据！");
//                            setEmptyViewShown(true);
                        }
                    }
                });

            }
        });
    }

    /**
     * 返回前一个页面
     */
    private void onBackFragment() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            mActivity.getSupportFragmentManager().popBackStack();
        else
            mActivity.finish();
    }
}
