package com.contron.ekeyapptx.socket;

import java.net.URISyntaxException;

import org.json.JSONException;
import org.json.JSONObject;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.WarnMsgActivity;
import com.contron.ekeypublic.entities.Verify;
import com.contron.ekeypublic.entities.WarnMsg;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeyapptx.R;
import com.lidroid.xutils.util.LogUtils;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class SocketService extends Service {

	private Socket mSocket;
	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mBuilder;

	public class LocalBinder extends Binder {
		SocketService getService() {
			return SocketService.this;
		}
	}

	private final IBinder mBinder = new LocalBinder();

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		LogUtils.e("onBind~~~~~~~~~~~~");

		return mBinder; // 也可以像上面几个语句那样重新new一个IBinder
		// 如果这边不返回一个IBinder的接口实例，那么ServiceConnection中的onServiceConnected就不会被调用
		// 那么bind所具有的传递数据的功能也就体现不出来~\(≧▽≦)/~啦啦啦（这个返回值是被作为onServiceConnected中的第二个参数的）
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		LogUtils.e("onCreate~~~~~~~~~~");
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		unregisterSocket();
		mNotificationManager.cancel(100);// 取消通知栏显示
		LogUtils.e("onDestroy~~~~~~~~~~~");
	}

	@Override
	public void onStart(Intent intent, int startId) {
		LogUtils.e("onStart~~~~~~");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		LogUtils.e("onStartCommand~~~~~~~~~~~~");
		registerSocket();// 注册Socket
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		initNotify();// 注册通知栏
		return super.onStartCommand(intent, flags, startId);
	}

	/**
	 * 注册Socket
	 */
	public void registerSocket() {
		LogUtils.e("registerSocket~~~~~~~~~~~~");
		{
			try {
				String url = EkeyAPP.getInstance().getSocketAddress();
				// mSocket = IO.socket("http://chat.socket.io");
				mSocket = IO.socket(url);

				LogUtils.i("Socket:地址" + url);

			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
		}
		mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
		mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
		mSocket.on(Socket.EVENT_CONNECT, connect);
		mSocket.on("alert", alert);// alert监听事件
		mSocket.on("verify", verifynews);// verifynews监听事件
		mSocket.on("notice", notice); // 监听公告
		connectedSocket();// 连接Socket
	}

	/**
	 * 连接Socket
	 */
	private void connectedSocket() {
		LogUtils.e("连接中~~~~~~~~~~~~");
		if (mSocket == null)
			return;
		// 连接
		if (!mSocket.connected()) {
			mSocket.connect();
		}

	}

	/**
	 * 注销Socket
	 * 
	 * @param mSocket
	 */
	public void unregisterSocket() {
		mSocket.disconnect();
		mSocket.close();
		mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
		mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
		mSocket.off(Socket.EVENT_CONNECT, connect);
		mSocket.off("alert", alert);// 注销 alert监听事件
		mSocket.off("verify", verifynews);// 注销verifynews监听事件
		mSocket.off("notice", notice); // 注销公告
	}

	/**
	 * 发送警告信息/审批消息date为空
	 */
	private void sendWarnMsg(JSONObject msg, String type) {

		if (msg == null)
			return;

		if (type.equals("alert")) {
			String message = "";
			String time = "";
			try {
				message = msg.getString("msg");
				time = msg.getString("create_at");
			} catch (JSONException e) {
				return;
			}

			WarnMsg warnMsg = new WarnMsg(time, message);
			EkeyAPP.getInstance().setListWarnMsg(warnMsg);
			EventBusManager.getInstance().post(warnMsg);// 发送到主界面

		} else if (type.equals("verifynews")) {

			int offline;
			int temp_task;
			int register;
			int unlock;
			int workbill;
			try {
				offline = msg.getInt("offline");
				temp_task = msg.getInt("temp_task");
				register = msg.getInt("register");
				unlock = msg.getInt("unlock");
				workbill = msg.getInt("workbill");
			} catch (JSONException e) {
				return;
			}

			Verify verify = new Verify(offline, temp_task, register, unlock, workbill);
			EventBusManager.getInstance().post(verify);// 发送到主界面
		} else if (type.equals("notice")) {

		}

	}

	/** 初始化通知栏 */
	private void initNotify() {
		mBuilder = new NotificationCompat.Builder(this);
		mBuilder.setContentTitle("测试标题")
				.setContentText("测试内容")
				.setContentIntent(
						getDefalutIntent(Notification.FLAG_AUTO_CANCEL))
				// .setNumber(number)//显示数量
				.setTicker("测试通知来啦")// 通知首次出现在通知栏，带上升动画效果的
				.setWhen(System.currentTimeMillis())// 通知产生的时间，会在通知信息里显示
				.setPriority(Notification.PRIORITY_DEFAULT)// 设置该通知优先级
				.setAutoCancel(true)// 设置这个标志当用户单击面板就可以让通知将自动取消
				.setOngoing(false)// ture，设置他为一个正在进行的通知。他们通常是用来表示一个后台任务,用户积极参与(如播放音乐)或以某种方式正在等待,因此占用设备(如一个文件下载,同步操作,主动网络连接)
				.setDefaults(Notification.DEFAULT_SOUND)// 向通知添加声音、闪灯和振动效果的最简单、最一致的方式是使用当前的用户默认设置，使用defaults属性，可以组合：
				// Notification.DEFAULT_ALL Notification.DEFAULT_SOUND 添加声音
				// //Notification.DEFAULT_VIBRATE 振动
				// requires VIBRATE permission
				.setSmallIcon(R.drawable.ic_launcher);

		// 设置自定义音乐
		// mBuilder.build().sound=Uri.parse("android.resource://" +
		// getPackageName() + "/" +R.raw.**);

	}

	/**
	 * @获取默认的pendingIntent,为了防止2.3及以下版本报错
	 * @flags属性: 在顶部常驻:Notification.FLAG_ONGOING_EVENT 点击去除：
	 *           Notification.FLAG_AUTO_CANCEL
	 */
	public PendingIntent getDefalutIntent(int flags) {
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 1,
				new Intent(), flags);
		return pendingIntent;
	}

	/** 显示通知栏 */
	public void showNotify(String msg) {
		mBuilder.setContentTitle("警告信息！").setContentText(msg)
				.setTicker("警告信息！");// 通知首次出现在通知栏，带上升动画效果的

		Intent intent = new Intent(this, WarnMsgActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				intent, PendingIntent.FLAG_CANCEL_CURRENT);// 点击跳转到警告信息查看页面
		mBuilder.setContentIntent(pendingIntent);

		mNotificationManager.notify(100, mBuilder.build());
		// String url=getApplicationContext().getFilesDir().getAbsolutePath();
		// LogUtils.e("程序路径："+url);

	}

	// 连接Socket异常监听
	private Emitter.Listener onConnectError = new Emitter.Listener() {
		@Override
		public void call(Object... args) {
			LogUtils.e("连接Socket异常");
			// 延时5秒后重新连接
			Thread thread = new Thread(new Runnable() {

				public void run() {
					try {
						Thread.sleep(1000 * 5);
						connectedSocket();// 重新连接
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}

			});
			thread.start();

		}
	};

	// 监听获取服务器推送的警报
	private Emitter.Listener alert = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			JSONObject data = (JSONObject) args[0];
			String message;
			try {
				message = data.getString("msg");
			} catch (JSONException e) {
				return;
			}
			sendWarnMsg(data, "alert");// 发送警告信息
			showNotify(message);// 通知栏显
		}
	};

	// 监听获取服务器推送的审批消息
	private Emitter.Listener verifynews = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			JSONObject data = (JSONObject) args[0];
			JSONObject jsonmessage = null;
			try {
				String message = data.getString("msg");
				jsonmessage = new JSONObject(message);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			LogUtils.e("socket收到信息：" + data.toString());
			sendWarnMsg(jsonmessage, "verifynews");// 审批
		}
	};

	private Emitter.Listener notice = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			JSONObject data = (JSONObject) args[0];
			JSONObject jsonmessage = null;
			try {
				String message = data.getString("msg");
				jsonmessage = new JSONObject(message);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			LogUtils.e("socket收到信息：" + data.toString());
			sendWarnMsg(jsonmessage, "notice");// 审批
		}
	};

	// 监听连接状态
	private Emitter.Listener connect = new Emitter.Listener() {
		@Override
		public void call(final Object... args) {
			LogUtils.e("己连接。。。。。。。。。");
			String k = EkeyAPP.getInstance().getUser().getKValue();

			JSONObject data = new JSONObject();
			try {
				data.put("message", "测试");
				data.put("k", k);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			// 发关连接消息、增加Ack回调
			mSocket.emit("connectin", data, new Ack() {
				@Override
				public void call(Object... arg0) {
					LogUtils.e("发送回调:" + arg0.toString());
				}
			});
		}
	};

}
