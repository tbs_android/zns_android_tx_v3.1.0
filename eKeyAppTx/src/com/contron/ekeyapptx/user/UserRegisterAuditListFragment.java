package com.contron.ekeyapptx.user;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.bumptech.glide.Glide;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.RegisterUser;
import com.contron.ekeypublic.entities.Role;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.BitmapCallback;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.InputDialogFragment;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.PhotoBrowsePagerActivity;
import com.contron.ekeyapptx.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.exception.DbException;

/**
 * 用户注册审批
 * 
 * @author luoyilong
 *
 */
public class UserRegisterAuditListFragment extends SwipeRefreshListFragment {
	private BasicActivity mActivity;

	private ContronAdapter<RegisterUser> mAdapter;// 显示待审批注册用户适配器
	private RoleDialogFragment boxFragment;
	private RequestHttp requestHttp;// 后台服务请求
	private List<RegisterUser> users = new ArrayList<RegisterUser>();
	private List<RegisterUser> tempUsers = new ArrayList<RegisterUser>();

	// 返回当前对象
	public static UserRegisterAuditListFragment newInstance() {
		UserRegisterAuditListFragment fragment = new UserRegisterAuditListFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
		setAdapter();
        initRoleDialog();
//		try {
//			initRoleDialog();
//		} catch (DbException e) {
//			e.printStackTrace();
//		}

	}

	@Override
	protected void createLoadMoreView(LayoutInflater inflater) {
		loadMoreView = inflater.inflate(R.layout.item_footerview, null);
		loadMoreButton = loadMoreView.findViewById(R.id.rad_next);


		// 下一页按钮
		loadMoreButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mcurrentItem++;
				if (mcurrentItem < mItemCount)// 当前页小于总页数
				{
					downloadUserRegister(mcurrentItem);
				} else {
					mActivity.showMsgBox("已经是最后页了！");
				}

			}

		});
	}

	@Override
	public void onListRefresh() {
		super.onListRefresh();
		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
		mcurrentItem = 0;
		downloadUserRegister(mcurrentItem);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);// 先初始化适配器再刷新数据
		setHasOptionsMenu(true);// 想让Fragment中的onCreateOptionsMenu生效必须先调用setHasOptionsMenu方法
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item4_name);
		setLoadMoreView();
		setListAdapter(mAdapter);
	}

	/**
	 * 设备适配器
	 */
	private void setAdapter() {
		mAdapter = new ContronAdapter<RegisterUser>(mActivity,
				R.layout.fragment_user_register_audit_item,
				new ArrayList<RegisterUser>()) {

			@Override
			public void setViewValue(ContronViewHolder holder,
					final RegisterUser item, final int position) {

				ImageView head = holder.findView(R.id.temporary_apply_iv_head);

				ImageView identity = holder
						.findView(R.id.temporary_apply_iv_identity);

				final String thumHeadUrl = mActivity.getWld() + "/public/"
						+ item.username + "/portrait" + ".jpg";
				Glide.with(getContext()).load(thumHeadUrl).placeholder(R.drawable.ic_head).into(head);
				// 头像
//				if (item.getPortraitmap() != null) {
//					head.setImageBitmap(item.getPortraitmap());
//
//					// 头像点击放大
					head.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ArrayList<String> urlList = new ArrayList<String>();

							final String headUrl = "/public/"
									+ item.username + "/portrait" + ".jpg";

							urlList.add(thumHeadUrl);
							PhotoBrowsePagerActivity.gotoActivity(mActivity,
									urlList, position, "头像照片浏览");
						}
					});
//
//				} else {
//					head.setImageResource(R.drawable.ic_head);// 显示默认照片
//					// 头像点击放大
//					head.setOnClickListener(new OnClickListener() {
//
//						@Override
//						public void onClick(View v) {
//
//						}
//					});
//				}

				final String thumIdcarUrl = mActivity.getWld() + "/public/"
						+ item.username + "/idcard" + ".jpg";
				Glide.with(getContext()).load(thumIdcarUrl).placeholder(R.drawable.ic_identity).into(identity);

				// 身份证
//				if (item.getIdcardmap() != null) {
//					identity.setImageBitmap(item.getIdcardmap());

					// 身份证点击放大
					identity.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ArrayList<String> urlList = new ArrayList<String>();

							final String idcardurl = "/public/"
									+ item.username + "/idcard" + ".jpg";

							urlList.add(thumIdcarUrl);
							PhotoBrowsePagerActivity.gotoActivity(mActivity,
									urlList, position, "身份证照片浏览");
						}
					});

//				} else {
//					identity.setImageResource(R.drawable.ic_identity);// 显示默认照片
//					// 头像点击放大
//					identity.setOnClickListener(new OnClickListener() {
//
//						@Override
//						public void onClick(View v) {
//
//						}
//					});
//				}

				holder.setText(android.R.id.text1, item.name);
				TextView tv = holder.findView(android.R.id.text2);
				tv.setText("部        门：" + item.section);
				tv.append("\n手  机  号：");
				tv.append(item.username);
				tv.append("\n角        色：");
				tv.append(item.roleCheckToString());
				tv.append("\n个人说明：");
				tv.append(item.memo);
				tv.append("\n注册时间：");
				tv.append(DateUtil.timeCompare(item.reg_at));

				holder.findView(R.id.user_register_audit_item_button1)
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								InputDialogFragment inputDialogFragment = new InputDialogFragment();
								inputDialogFragment
										.setTitleId(R.string.audit_dialog_title);
								inputDialogFragment
										.setHintId(R.string.audit_dialog_hiht);
								inputDialogFragment
										.setOnInputTextListener(new InputDialogFragment.OnInputTextListener() {

											@Override
											public void OnInputText(String text) {

												updateAuditState(false, text,
														item, position);
											}
										});
								inputDialogFragment.show(
										mActivity.getFragmentManager(),
										"inputDialogFragment");
							}
						});

				holder.findView(R.id.user_register_audit_item_button2)
						.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								if (item.roleCheckEmpty())
									mActivity.showToast("请设置角色！");
								else
									updateAuditState(true, "", item, position);
							}
						});

			}
		};

	}

	/**
	 * 角色选择对话框
	 * 
	 * @throws DbException
	 */
	private void initRoleDialog() {
		boxFragment = new RoleDialogFragment();
		boxFragment.setTitleId(R.string.role_dialog_title);
	}

	/**
	 * 审批后更新
	 * 
	 * @param flag
	 * @param auditDesc
	 * @param mTicketAudit
	 */
	private void updateAuditState(boolean flag, String auditDesc,
			RegisterUser mTicketAudit, final int position) {

		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话框

		// 把角色字符串转换成JSON格式上传
		String role = mTicketAudit.roleCheckToIntString();
		ArrayList<Integer> rollist = new ArrayList<Integer>();
		if (!TextUtils.isEmpty(role)) {
			if (role.contains(",")) {
				String[] r = role.split(",");
				for (int i = 0; i < r.length; i++) {
					rollist.add(Integer.parseInt(r[i]));
				}
			} else {
				rollist.add(Integer.parseInt(role));
			}
		}
		JSONArray roleJson = new JSONArray(rollist);

		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("action", flag ? "批准" : "拒绝");
		params.putParams("memo", auditDesc);
		params.putParams("role", roleJson);

		String url = String.format(Globals.USER_NAME_VERIFY,
				mTicketAudit.username);

		requestHttp.doPost(url, params, new HttpRequestCallBackString() {
			@Override
			public void resultValue(final String value) {

				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {

						mActivity.dismissDialog();

						if (value.contains("success")) {// 返回成功
							mAdapter.removeDatas(position);
							mActivity.showToast(R.string.offline_audit_sussmsg);
						} else {// 返回失败
							String error = "";
							try {
								JSONObject json = new JSONObject(value);
								error = json.getString("error");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							mActivity.showToast(error);
						}

					}
				});
			}
		});

	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		final RegisterUser userRegister = mAdapter.getItem(position);
		// 每次点击都重新获取游标，得到最新选择的角色状态
		if (userRegister != null && boxFragment != null) {
			final List<Role> rolesAll = new ArrayList<Role>();// 只保存选中的
			if (userRegister.roleAllEmpty()) {
				try {
					userRegister.rolesALL = DBHelp.getInstance(mActivity)
							.getDb().findAll(Role.class);
				} catch (DbException e) {

					e.printStackTrace();
				}
			}
			rolesAll.addAll(userRegister.rolesALL);
			boxFragment
					.setRoleData(rolesAll)
					.setOnRoleItemClickListener(
							new RoleDialogFragment.OnRoleItemClickListener() {

								@Override
								public void onRoleItem(Role role, int position,
										boolean isChecked) {
									if (isChecked) {
										role.setIsCheck(1);
									} else {
										role.setIsCheck(0);
									}
									// 保存到所有中
									rolesAll.set(position, role);
								}
							})
					.setPositiveOnClickListener(
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO 确定，只添加选中的角色
									userRegister.addAllRoleAll(rolesAll);
									mAdapter.notifyDataSetChanged();
								}
							})
					.setNegativeOnClickListener(
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO 取消
									boxFragment.dismiss();
								}
							});
			boxFragment.show(mActivity.getFragmentManager(), "role");
		}
	}

	/**
	 * 下载待审批的用户
	 */
	private void downloadUserRegister(int currentItem) {
		setRefreshing(false);
		if (currentItem == 0) {// 当前页数为0时清空数据
			users.clear();
			mAdapter.setEmpty();
		}
		tempUsers.clear();
		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("page", currentItem);
		params.putParams("status", "待审批");
		requestHttp.doPost(Globals.USER, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {

						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								if (value.contains("success")) {// 返回成功
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										JSONArray jsonArray = jsonObject
												.getJSONArray("items");

										List<RegisterUser> listUsers = new Gson().fromJson(
												jsonArray.toString(),
												new TypeToken<List<RegisterUser>>() {
												}.getType());

										// 获取总页数
										if (mcurrentItem == 0) {
											mItemCount = jsonObject
													.getInt("pages");
										}

										if (listUsers != null
												&& listUsers.size() > 0) {
											tempUsers.addAll(listUsers);
											users.addAll(tempUsers);
										}
										mAdapter.addAllDatas(tempUsers);
										mAdapter.notifyDataSetChanged();
//										download();// 下载图片

									} catch (JSONException e) {
										e.printStackTrace();
									}

								} else {// 返回失败

									setEmptyText("暂时无数据！");
									setEmptyViewShown(true);
								}

								mActivity.dismissDialog();// 关闭对话�?
							}
						});

					}
				});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			mActivity.finish();
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/**
	 * 下载图片\更新适配器
	 */
	private void download() {

		if (users != null && users.size() > 0) {
			for (int i = (users.size() - tempUsers.size()); i < users.size(); i++) {

				final int j = i;
				RegisterUser item = users.get(i);

				final String portraiturl = "/public/" + item.username
						+ "/portrait" + ".jpg";

				final String idcardurl = "/public/" + item.username
						+ "/idcard" + ".jpg";

				DisplayMetrics displayMetrics = new DisplayMetrics();
				getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);// 获取分辨率

				// 获取头像
				requestHttp.downloadImg(portraiturl, displayMetrics.widthPixels, displayMetrics.heightPixels, new BitmapCallback() {

					@Override
					public void resultValueBitmap(final Bitmap bitmap) {

						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (bitmap != null) {
									users.get(j).setPortraitmap(bitmap);
									mAdapter.notifyDataSetChanged();
								}
							}
						});
					}
				});

				// 获取身份证
				requestHttp.downloadImg(idcardurl, displayMetrics.widthPixels, displayMetrics.heightPixels, new BitmapCallback() {

					@Override
					public void resultValueBitmap(final Bitmap bitmap) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (bitmap != null) {
									users.get(j).setIdcardmap(bitmap);
									mAdapter.notifyDataSetChanged();
								}
							}
						});
					}
				});
			}

			mAdapter.addAllDatas(tempUsers);
			setEmptyViewShown(false);
		} else {
			setEmptyText("暂无数据！");
			setEmptyViewShown(true);

		}

	}

	/**
	 * 对话框
	 * 
	 * @author hupei
	 * @date 2014�?12�?19�? 下午2:59:14
	 */
	private static class RoleDialogFragment extends DialogFragment {
		public interface OnRoleItemClickListener {
			void onRoleItem(Role role, int position, boolean isChecked);
		}

		private int titleId;
		private DialogInterface.OnClickListener okListener;
		private DialogInterface.OnClickListener noListener;
		private List<Role> mRoles;
		private OnRoleItemClickListener mOnRoleItemClickListener;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			final ListView listView = new ListView(getActivity());
			listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			builder.setView(listView);
			if (titleId != 0)
				builder.setTitle(titleId);
			final ContronAdapter<Role> mAdapter = new ContronAdapter<Role>(
					getActivity(),
					android.R.layout.simple_list_item_multiple_choice, mRoles) {

				@Override
				public void setViewValue(ContronViewHolder viewHolder,
						Role item, int position) {
					CheckedTextView mCheckedTextView = viewHolder
							.findView(android.R.id.text1);
					mCheckedTextView.setText(item.getName());
					mCheckedTextView.setChecked(item.getIsCheck() == 1);
					listView.setItemChecked(position,
							mCheckedTextView.isChecked());
				}
			};
			listView.setAdapter(mAdapter);
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					if (mOnRoleItemClickListener != null) {
						mOnRoleItemClickListener.onRoleItem(
								mAdapter.getItem(position), position,
								listView.isItemChecked(position));
					}
				}
			});

			if (okListener != null)
				builder.setPositiveButton(android.R.string.ok, okListener);
			if (noListener != null)
				builder.setNegativeButton(android.R.string.cancel, noListener);
			AlertDialog dialog = builder.create();
			return dialog;
		}

		public RoleDialogFragment setTitleId(int titleId) {
			this.titleId = titleId;
			return this;
		}

		public RoleDialogFragment setRoleData(List<Role> roles) {
			this.mRoles = roles;
			return this;
		}

		public RoleDialogFragment setOnRoleItemClickListener(
				OnRoleItemClickListener listener) {
			this.mOnRoleItemClickListener = listener;
			return this;
		}

		public RoleDialogFragment setPositiveOnClickListener(
				DialogInterface.OnClickListener listener) {
			this.okListener = listener;
			return this;
		}

		public RoleDialogFragment setNegativeOnClickListener(
				DialogInterface.OnClickListener listener) {
			this.noListener = listener;
			return this;
		}

		@Override
		public void show(FragmentManager manager, String tag) {
			FragmentTransaction transaction = manager.beginTransaction();
			// 指定一个过渡动画
			transaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.add(this, tag);
			transaction.commitAllowingStateLoss();// 防止按home键后，show的时候调用父类中的commit方法异常
		}
	}
}
