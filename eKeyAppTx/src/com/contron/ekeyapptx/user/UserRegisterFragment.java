/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx.user;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apaches.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Company;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.AppUtils;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.util.FileUtil;
import com.contron.ekeypublic.util.ImageUtil;
import com.contron.ekeypublic.util.SIMCardInfo;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.bitmap.BitmapCommonUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.lidroid.xutils.view.annotation.event.OnFocusChange;
import com.lidroid.xutils.view.annotation.event.OnItemSelected;

/**
 * 用户注册
 * <p>
 * 1：手机号离开焦点，检测服务器数据库是否存在或者已注册，并填充界面数据，并禁用提交按钮。<br>
 * 1.1 存在：隐藏审批状态，审批信息 <br>
 * 1.2 已注册：显示审批状态与审批信息
 * 
 * @author
 * @date 2015年12月9日 上午10:09:27
 */
public class UserRegisterFragment extends Fragment {
	private static final int REQUEST_CODE_HEAD = 101;
	private static final int REQUEST_CODE_IDENTITY = 102;

	private BasicActivity mActivity;
	@ViewInject(R.id.user_register_moblie_progressBar)
	private ProgressBar progressBar;
	@ViewInject(R.id.user_register_has_moblie)
	private TextView tvHasUserRegister;
	@ViewInject(R.id.user_register_moblie)
	private EditText etMobile;
	@ViewInject(R.id.user_register_pwd)
	private EditText etPwd;
	@ViewInject(R.id.user_register_confirmPwd)
	private EditText etConfirmPwd;
	@ViewInject(R.id.user_register_userName)
	private EditText etUserName;
	@ViewInject(R.id.user_register_desc)
	private EditText etUserDesc;
	@ViewInject(R.id.user_register_department)
	private Spinner spinnerDepartment;
	@ViewInject(R.id.user_register_company)
	private Spinner spinnerAgency;
	private EditText etAuditDesc;
	@ViewInject(R.id.temporary_apply_iv_head)
	private ImageView ivHead;
	@ViewInject(R.id.temporary_apply_iv_identity)
	private ImageView ivIdentity;
	private ContronAdapter<Section> spSectionAdapter;
	private ContronAdapter<Company> spCompanyAdapter;
	private RequestHttp requestHttp;
	private List<Section> sections = new ArrayList<Section>();
	private Section section;
	private Company company;

	private String picSaveDir = "";// 文件路径
	private String picHeadLargePath = "";// 头像保存文件
	private String picIdentityLargePath = "";// 身份证文件
	private String picHeadThumbnailPath = "";// 头像缩略图文件
	private String picIdentityThumbnailPath = "";// 身份证缩略图文件

	private BluetoothAdapter bluetoothAdapter;// 蓝牙适配器
	private String btaddress = "";// 获取本设蓝牙地址
	private BitmapUtils bitmapUtils;

	public static UserRegisterFragment newInstance() {
		return new UserRegisterFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BasicActivity) getActivity();

		// 初始化图片处理对象
		bitmapUtils = new BitmapUtils(mActivity);
		bitmapUtils.configDefaultBitmapMaxSize(BitmapCommonUtils
				.getScreenSize(mActivity));
		bitmapUtils.configDefaultBitmapConfig(Bitmap.Config.RGB_565);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_user_register,
				container, false);
		ViewUtils.inject(this, view);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		mActivity.getBasicActionBar().setTitle(R.string.user_register_title);
		requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		// StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
		// .detectDiskReads().detectDiskWrites().detectNetwork()
		// .penaltyLog().build());

		picSaveDir = mActivity.getExternalCacheDir().getPath() + "/User/";// 文件路径

		// StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
		// .detectLeakedSqlLiteObjects().detectLeakedClosableObjects()
		// .penaltyLog().penaltyDeath().build());

		// 初始化部门适配器
		spSectionAdapter = new ContronAdapter<Section>(mActivity,
				android.R.layout.simple_spinner_item, sections) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					Section item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
		spSectionAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerDepartment.setAdapter(spSectionAdapter);
		spinnerDepartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				Section section = spSectionAdapter.getItem(position);
				getCompany(section);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		spCompanyAdapter = new ContronAdapter<Company>(mActivity,
				android.R.layout.simple_spinner_item, new ArrayList<Company>()) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
									 Company item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
		spCompanyAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerAgency.setAdapter(spCompanyAdapter);

		getMobile();// 获取手机号

		initDepartment();// 请求获取部位

		// 获取蓝牙服务
		BluetoothManager bluetoothManager = (BluetoothManager) mActivity
				.getSystemService(Context.BLUETOOTH_SERVICE);
		bluetoothAdapter = bluetoothManager.getAdapter();
		btaddress = bluetoothAdapter.getAddress();

		if (savedInstanceState != null) {
			picSaveDir = savedInstanceState.getString("picSaveDir");
			picHeadLargePath = savedInstanceState.getString("picHeadLargePath");
			picIdentityLargePath = savedInstanceState
					.getString("picIdentityLargePath");
			picHeadThumbnailPath = savedInstanceState
					.getString("picHeadThumbnailPath");
			picIdentityThumbnailPath = savedInstanceState
					.getString("picIdentityThumbnailPath");
		}

	}

	private void getCompany(Section section) {
		spCompanyAdapter.clearDatas();
		List<Company> companies = section.getCompanyList();
		if (companies.size() != 0 && null != companies.get(0).getName()) {
			companies.add(0, new Company());
		}
		spCompanyAdapter.addAllDatas(companies);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		File picFile = new File(picSaveDir);
		if (picFile.exists()) {// 文件存在
			FileUtil.RecursionDeleteFile(picFile);// 删除文件
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/**
	 * 获取手机�?
	 */
	private void getMobile() {
		String mobile = new SIMCardInfo(mActivity).getNativePhoneNumber();
		if (!TextUtils.isEmpty(mobile)) {
			etMobile.setText(mobile);
			// hasUserRegister();
		}
	}

	/**
	 * 切换圆形进度条状态，显示校验结果
	 * 
	 * @param visibility
	 *            圆形进度条状�?
	 * @param text
	 *            校验结果
	 * @param color
	 *            结果显示颜色
	 */
	private void toggleHasUserRegiserInfo(int visibility, String text, int color) {
		progressBar.setVisibility(visibility);
		tvHasUserRegister.setVisibility(View.VISIBLE);
		tvHasUserRegister.setText(text);
		tvHasUserRegister.setTextColor(color);
	}

	/**
	 * 请求获取部位
	 */
	private void initDepartment() {

		JSONObject params = new JSONObject();

		String url = String.format(Globals.GET_SECTION_AND_COMPANY, 0);
		requestHttp.doPost(url, params,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (value.contains("success")) {// 返回成功
									sections = JsonTools.getSections("section",
											value);

									if (sections != null && sections.size() > 0) {

										for (Section session : sections) {
											if ("根区域".equals(session.getName())) {// 去掉根节点
												sections.remove(session);
												break;
											}
										}

										spSectionAdapter.addAllDatas(sections);
									}
								} else// 返回失败
								{
									String error = "";

									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showMsgBox(error);
								}
							}
						});

					}
				});

	}

	@OnFocusChange(R.id.user_register_moblie)
	public void onFocusChange(View v, boolean hasFocus) {
		if (!hasFocus) {// 失去焦点
			if (etMobile.getText().length() != 11) {
				mActivity.showToast("请输入有效的电话号码");
				etMobile.setText("");
			} else {
				toggleHasUserRegiserInfo(View.VISIBLE, "", Color.RED);
				hasUserRegister();
			}
		} else {// 获取焦点
			toggleHasUserRegiserInfo(View.GONE, "", Color.RED);
		}
	}

	/**
	 * 用户手机号校验
	 */
	private void hasUserRegister() {

		if (TextUtils.isEmpty(etMobile.getText().toString()))
			return;

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("username", etMobile.getText().toString());

		requestHttp.doPost(Globals.HAS_REGISTER_USER, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								if (value.contains("success")) {// 可以申请
									toggleHasUserRegiserInfo(View.GONE,
											"验证通过！", Color.RED);
								} else {
									String error = "验证失败！";
									toggleHasUserRegiserInfo(View.GONE, error,
											Color.RED);
								}

							}
						});

					}
				});

	}

	@OnClick({ R.id.temporary_apply_iv_head, R.id.temporary_apply_iv_identity })
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.temporary_apply_iv_head:
			picHeadLargePath = startCamera(REQUEST_CODE_HEAD);
			break;
		case R.id.temporary_apply_iv_identity:
			picIdentityLargePath = startCamera(REQUEST_CODE_IDENTITY);
			break;
		default:
			break;
		}
	}

	/**
	 * 获取相片保存绝对路径，并启动拍照功能
	 * 
	 * @param requestCode
	 * @return
	 */
	private String startCamera(int requestCode) {
		// 判断存储卡是否可以用，可用进行存在
		if (!AppUtils.sdCardIsExsit()) {
			mActivity.showToast("无SD卡");
			return "";
		}
		// 存放照片的文件夹
		File savedir = new File(picSaveDir);
		if (!savedir.exists()) {
			savedir.mkdirs();
		}
		String timeStamp = DateUtil.getCurrentDate("yyyyMMddHHmmss");
		String t_photoName = timeStamp + ".jpg";// 照片名称
		String largePath = picSaveDir + t_photoName;// 该照片的绝对路径
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri uri;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			String authorities = mActivity.getPackageName() + ".fileProvider";
			uri= FileProvider.getUriForFile(mActivity, authorities, new File(largePath));
		} else {
			uri = Uri.fromFile(new File(largePath));
		}
		intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		startActivityForResult(intent, requestCode);
		return largePath;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode != Activity.RESULT_CANCELED) {
			switch (requestCode) {
			case REQUEST_CODE_HEAD:
				// 创建一个线程处理，避免返回后2秒才回到当前界面
				Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						picHeadThumbnailPath = createThumbnailPic(
								picHeadLargePath, "head_", ivHead);
					}
				});
				thread.start();

				break;
			case REQUEST_CODE_IDENTITY:

				// 创建一个线程处理，避免返回后2秒才回到当前界面
				Thread thread1 = new Thread(new Runnable() {
					@Override
					public void run() {
						picIdentityThumbnailPath = createThumbnailPic(
								picIdentityLargePath, "identity_", ivIdentity);
					}
				});
				thread1.start();

				break;
			}
		} else {
			// 删除文件
			FileUtil.deleteFileWithPath(picHeadLargePath);
			FileUtil.deleteFileWithPath(picIdentityLargePath);
		}

	}

	/**
	 * 创建缩略图，并显示在 ImageView
	 * 
	 * @author hupei
	 * @date 2015年5月28日 上午10:22:22
	 */
	private String createThumbnailPic(String largePath, String smallFileName,
			ImageView iv) {
		String thumbnailPath = picSaveDir + "small_" + smallFileName
				+ FileUtil.getFileName(largePath);
		File file = new File(thumbnailPath);

		LogUtils.e("largePath：" + largePath);
		LogUtils.e("smallFileName：" + smallFileName);
		LogUtils.e("文件名称：" + file.toString());

		// 判断是否已存在缩略图
		if (!file.exists()) {
			try {
				// 压缩生成上传的1920宽度图片
				ImageUtil.createImageThumbnail(mActivity, largePath,
						thumbnailPath, 1920, 80);
				// 删除原图
				FileUtil.deleteFileWithPath(largePath);
				displayPhoto(iv, thumbnailPath);
				return thumbnailPath;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "";

	}

	/**
	 * 把圖片显示到UI上
	 * 
	 * @param iv
	 *            ImageView
	 * @param thumbnailPath
	 *            文件
	 */
	private void displayPhoto(final ImageView iv, final String thumbnailPath) {
		if (TextUtils.isEmpty(thumbnailPath))
			return;

		if (bitmapUtils != null) {
			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					bitmapUtils.display(iv, thumbnailPath);// 显示
				}
			});

		}
	}

	@OnItemSelected({R.id.user_register_department, R.id.user_register_company})
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		switch (view.getId()) {
			case R.id.user_register_department:
				section = (Section) parent.getItemAtPosition(position);
				break;
			case R.id.user_register_company:
				company = (Company) parent.getItemAtPosition(position);
				break;
		}

	}

	/**
	 * 提交用户注册信息
	 */
	private void submitUser() {

		LogUtils.e("本手机蓝牙地址" + btaddress);

		if (btaddress.isEmpty()) {// 蓝牙地址不为空
			mActivity.showMsgBox("提示", "当前设备不支持蓝牙,无法提交申请！",
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});

			return;
		}

		final String mobile = etMobile.getText().toString().trim();
		if (TextUtils.isEmpty(mobile)) {
			mActivity.showToast("手机号不能为空！");
			return;
		}

		if (mobile.length() != 11 || mobile.indexOf("1") != 0) {
			mActivity.showToast("手机号格式不对！");
			return;
		}

		String pwd = etPwd.getText().toString().trim();
		if (TextUtils.isEmpty(pwd)) {
			mActivity.showToast("密码不能为空！");
			return;
		}
		String pwdConfirm = etConfirmPwd.getText().toString().trim();
		if (TextUtils.isEmpty(pwdConfirm)) {
			mActivity.showToast("确认密码不能为空！");
			return;
		}
		if (!pwd.equals(pwdConfirm)) {
			mActivity.showToast("二次输入密码不一致！");
			return;
		}

		String userName = etUserName.getText().toString().trim();
		if (TextUtils.isEmpty(userName)) {
			mActivity.showToast("用户名不能为空！");
			return;
		}

		section = (Section)spinnerDepartment.getSelectedItem();
		if (section == null) {
			mActivity.showToast("请选择部门！");
			return;
		}

		if (TextUtils.isEmpty(picHeadThumbnailPath)) {
			mActivity.showToast("请拍摄头像照片！");
			return;
		}

		if (TextUtils.isEmpty(picIdentityThumbnailPath)) {
			mActivity.showToast("请拍摄身份证照片！");
			return;
		}

		String bt = DigestUtils.sha512Hex(btaddress);

		mActivity.startDialog(R.string.progressDialog_title_submit);

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("username", mobile);
		params.putParams("password", DigestUtils.sha512Hex(pwd));
		params.putParams("name", userName);
		params.putParams("section", section.getId());
		params.putParams("memo", etUserDesc.getText().toString());
		params.putParams("uniqueid", bt);
		company = (Company) spinnerAgency.getSelectedItem();
		if (company != null)
			params.putParams("companyid", company.getId());

		requestHttp.doPost(Globals.REGISTER_USER, params,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();

								if (value.contains("success")) {// 成功
									uploadImage(mobile);
									mActivity.showMsgBox("提示", "注册成功...请等待审批！",
											new OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
													mActivity
															.getSupportFragmentManager()
															.popBackStack();
												}
											});

								} else {// 失败
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);

								}

							}
						});

					}
				});

	}

	/**
	 * 上传图片
	 * 
	 * @param username
	 */
	private void uploadImage(String username) {
		String Url1 = "/user/" + username + "/portrait?s=1&c=android&v=1";
		String Url2 = "/user/" + username + "/idcard?s=1&c=android&v=1";
		requestHttp.uploadImage(Url1, picHeadThumbnailPath, "portrait");
		requestHttp.uploadImage(Url2, picIdentityThumbnailPath, "idcard");
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.submit, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.getSupportFragmentManager().popBackStack();
			break;
		case R.id.menu_submit:
			submitUser();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putString("picSaveDir", picSaveDir);
		outState.putString("picHeadLargePath", picHeadLargePath);
		outState.putString("picIdentityLargePath", picIdentityLargePath);
		outState.putString("picHeadThumbnailPath", picHeadThumbnailPath);
		outState.putString("picIdentityThumbnailPath", picIdentityThumbnailPath);

	}
}