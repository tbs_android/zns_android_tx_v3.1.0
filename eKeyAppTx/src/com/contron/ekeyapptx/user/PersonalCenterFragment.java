package com.contron.ekeyapptx.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.SettingsFragment;
import com.contron.ekeyapptx.key.KeyManageFragment;
import com.contron.ekeyapptx.profile.AnnouncementFragment;
import com.contron.ekeyapptx.profile.UserInfoFragment;
import com.contron.ekeyapptx.search.ContactFragment;
import com.contron.ekeyapptx.stations.StationManageFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class PersonalCenterFragment extends Fragment {

    @ViewInject(R.id.user_info_button)
    private Button mUserInfoBtn;
    @ViewInject(R.id.station_manager_button)
    private Button mStationManagerBtn;
    @ViewInject(R.id.key_manager_button)
    private Button mKeyManagerBtn;
    @ViewInject(R.id.addr_book_button)
    private Button mAddrBookBtn;
    @ViewInject(R.id.notice_button)
    private Button mNoticeBtn;
    @ViewInject(R.id.config_button)
    private Button mConfigBtn;

    private BasicActivity mActivity;

    public static PersonalCenterFragment newInstance() {
        PersonalCenterFragment fragment = new PersonalCenterFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BasicActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal_center, container, false);
        ViewUtils.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item42_name);
    }

    @OnClick({R.id.user_info_button, R.id.station_manager_button, R.id.key_manager_button,
            R.id.addr_book_button, R.id.notice_button, R.id.config_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.user_info_button: //账号信息
                mActivity.hideAdd(PersonalCenterFragment.class, UserInfoFragment.newInstance());
                break;
            case R.id.station_manager_button: //站点信息管理
                mActivity.hideAdd(PersonalCenterFragment.class, StationManageFragment.newInstance());
                break;
            case R.id.key_manager_button: //钥匙信息管理
                mActivity.hideAdd(PersonalCenterFragment.class, KeyManageFragment.newInstance());
                break;
            case R.id.addr_book_button: //通讯录
                mActivity.hideAdd(PersonalCenterFragment.class, ContactFragment.newInstance());
                break;
            case R.id.notice_button: //公告
                mActivity.hideAdd(PersonalCenterFragment.class, AnnouncementFragment.newInstance());
                break;
            case R.id.config_button: //设置
                mActivity.hideAdd(PersonalCenterFragment.class, SettingsFragment.newInstance());
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mActivity.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
