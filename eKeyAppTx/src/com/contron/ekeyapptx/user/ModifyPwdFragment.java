package com.contron.ekeyapptx.user;

import org.apaches.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnCompoundButtonCheckedChange;

/**
 * 修改密码
 * 
 * @author luoyilong
 *
 */
public class ModifyPwdFragment extends Fragment {
	@ViewInject(R.id.modifypwd_et_userMobile)
	private EditText etUserMobile;
	@ViewInject(R.id.modifypwd_et_oldPwd)
	private EditText metOldPwd;
	@ViewInject(R.id.modifypwd_et_newPwd)
	private EditText metNewPwd;
	@ViewInject(R.id.modifypwd_et_confirmPwd)
	private EditText metConfirmPwd;
	@ViewInject(R.id.modifypwd_cb_none)
	private CheckBox mchPwdNone;

	private BasicActivity mActivity;

	private RequestHttp requestHttp;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BasicActivity) getActivity();
	}

	public static ModifyPwdFragment newInstance() {
		return new ModifyPwdFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_modifypwd, container,
				false);
		ViewUtils.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mActivity.getBasicActionBar().setTitle("修改密码");

		requestHttp = new RequestHttp(mActivity.getWld());
		etUserMobile.setText(EkeyAPP.getInstance().getUser().getUsername());
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getActivity().getActionBar().setTitle("修改密码");
	}

	@OnCompoundButtonCheckedChange(R.id.modifypwd_cb_none)
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			metOldPwd.setTransformationMethod(HideReturnsTransformationMethod
					.getInstance());
			metNewPwd.setTransformationMethod(HideReturnsTransformationMethod
					.getInstance());
			metConfirmPwd
					.setTransformationMethod(HideReturnsTransformationMethod
							.getInstance());
		} else {
			metOldPwd.setTransformationMethod(PasswordTransformationMethod
					.getInstance());
			metNewPwd.setTransformationMethod(PasswordTransformationMethod
					.getInstance());
			metConfirmPwd.setTransformationMethod(PasswordTransformationMethod
					.getInstance());
		}
	}

	private void onModifyPwd() {
		final String strOldPwd = metOldPwd.getText().toString().trim();
		final String strNewPwd = metNewPwd.getText().toString().trim();
		final String strConfirmPwd = metConfirmPwd.getText().toString().trim();

		if (TextUtils.isEmpty(strOldPwd)) {
			mActivity.showToast("原密码不能为空！");
			return;
		}
		if (TextUtils.isEmpty(strNewPwd)) {
			mActivity.showToast("新密码不能为空！");
			return;
		}
		if (TextUtils.isEmpty(strConfirmPwd)) {
			mActivity.showToast("再次新密码不能为空！");
			return;
		}
		if (!strNewPwd.equals(strConfirmPwd)) {
			mActivity.showToast("新密码与再次新密码不一致！");
			return;
		}

		mActivity.startDialog(R.string.progressDialog_title_submit);
		RequestParams params = new RequestParams(mActivity.getUser());
		try {
			params.put("old", DigestUtils.sha512Hex(strOldPwd));
			params.put("new", DigestUtils.sha512Hex(strConfirmPwd));
		} catch (JSONException e) {
			e.printStackTrace();
		}

		String username = EkeyAPP.getInstance().getUser().getUsername();
		String url = String.format(Globals.USER_NAME_PASSWORD, username);

		requestHttp.doPost(url, params, new HttpRequestCallBackString() {
			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mActivity.dismissDialog();
						if (value.contains("success")) {// 修改成功
							mActivity.showMsgBox("提示", "密码修改成功！",
									new OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.dismiss();
											mActivity.finish();
										}
									});
						} else {// 修改失败
							String msg = "";
							try {
								JSONObject json = new JSONObject(value);
								msg = msg + json.getString("error");
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							mActivity.showMsgBox("密码修改失败！", msg,
									new OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.dismiss();
										}
									});

						}

					}
				});
			}
		});

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.save, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackFragment();
			break;
		case R.id.menu_save:
			onModifyPwd();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void onBackFragment() {
		if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
			mActivity.getSupportFragmentManager().popBackStack();
		else
			mActivity.finish();
	}

}