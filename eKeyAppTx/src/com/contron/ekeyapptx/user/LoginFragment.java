/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx.user;

import java.util.List;

import org.apaches.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.SettingsDialogFragment;
import com.contron.ekeyapptx.main.MainActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.SettingsFragment;
import com.contron.ekeypublic.PermissionsActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.ChoiceMode;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.update.UpdateManage;
import com.contron.ekeypublic.util.AppUtils;
import com.contron.ekeypublic.util.ConfigUtil;
import com.contron.ekeypublic.util.SIMCardInfo;
import com.contron.ekeypublic.view.ClearEditText;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * 登录界面
 * 
 * @author
 * @date 2015�?12�?8�? 10:51:01
 */
public class LoginFragment extends Fragment {
	@ViewInject(R.id.login_et_mobile)
	private ClearEditText etMobile;
	@ViewInject(R.id.login_et_password)
	private ClearEditText etPassword;

	@ViewInject(R.id.login_btn)
	private Button btnLogin;
	@ViewInject(R.id.login_tv_reclen_tips)
	private TextView tvRecLenTips;
	private BasicActivity mActivity;
	private ConfigUtil mConfigUtil;
	private static boolean choiceMode = true; // true为通讯领域
	private RoleDialogFragment boxFragment;
	private RequestHttp requestHttp;// 后台服务请求
	private BluetoothAdapter bluetoothAdapter;// 蓝牙适配器
	private String btaddress = "";// 获取本设蓝牙地址

	public static final int REQUEST_CODE = 0; // 请求码

	// 所需的全部权限
	static final String[] PERMISSIONS = new String[] {
			// Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
			Manifest.permission.WRITE_EXTERNAL_STORAGE,// 写文件
			Manifest.permission.ACCESS_FINE_LOCATION,// 定位
			Manifest.permission.READ_PHONE_STATE, };// 手机状态

	public static LoginFragment newInstance() {
		LoginFragment loginFragment = new LoginFragment();
		return loginFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BasicActivity) getActivity();
		mConfigUtil = ConfigUtil.getInstance(mActivity);
//		saveDefaultSetting();
		EkeyAPP.getInstance().setLoginStatus(false);// 设置为未曾登录状态
//		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_login, container, false);
		ViewUtils.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		mActivity.getLoginBasicActionBar().setTitle(R.string.login_title);
		startPermissionsActivity();// 6.0版本设置权限
		// 获取蓝牙服务
		BluetoothManager bluetoothManager = (BluetoothManager) mActivity
				.getSystemService(Context.BLUETOOTH_SERVICE);

		bluetoothAdapter = bluetoothManager.getAdapter();
		requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		// 检查是否有配置过地址，没有则提示跳转框口
		if (mConfigUtil.getString(SettingsFragment.KEY_WSDL_ADDRESS).isEmpty()) {

			mActivity.showMsgBox(R.string.first_run_tips,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
//							mActivity.repalce(SettingsDialogFragment.newInstance(),
//									true);
							SettingsDialogFragment.newInstance().show(getFragmentManager(), "Login");

						}
					});
		} else {
			// 取出设置中的自动注销时长
			tvRecLenTips.setText(getString(R.string.login_tv_reclen_tips,
					mConfigUtil.getString(SettingsFragment.KEY_RECLEN_TIME)));
			// 网络正常状态下才检查更新软件
			if (AppUtils.isNetworkAvailable(mActivity)) {
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						// 初始化数据，只有首次显示提示
						// 软件更新
						UpdateManage updateManage = new UpdateManage(mActivity,
								mActivity.getWld());
						updateManage.start(mConfigUtil
								.getString(SettingsFragment.KEY_VERSION_ADDRESS));
					}
				}, 200);
			}
		}

//		etMobile.setText("13726291998");
//		etPassword.setText("1");
	}

	@Override
	public void onResume() {
		super.onResume();
//		startPermissionsActivity();// 6.0版本设置权限
//		// 获取蓝牙服务
//		BluetoothManager bluetoothManager = (BluetoothManager) mActivity
//				.getSystemService(Context.BLUETOOTH_SERVICE);
//
//		bluetoothAdapter = bluetoothManager.getAdapter();

	}

	//设置默认的ip、端口号
    void saveDefaultSetting() {
	       String serviceAddress = "120.76.190.199:2111";
	       String socketAddress = "120.76.190.199:9095";
	      // 初始WebService地址
	       mConfigUtil.setString(SettingsFragment.KEY_WSDL_ADDRESS, "http://" + serviceAddress);

			// 访问socket服务地址
	       mConfigUtil.setString(SettingsFragment.KEY_SOCKET_ADDRESS, "http://" + socketAddress);

			// IP地址
	       mConfigUtil.setString(SettingsFragment.KEY_IP_ADDRESS, "120.76.190.199");

			// 服务端口
	       mConfigUtil.setString(SettingsFragment.KEY_POST, "2111");

			// socket端口
	       mConfigUtil.setString(SettingsFragment.SOCKET_POST, "9095");

			// 初始APP升级版本文件地址
	       mConfigUtil.setString(SettingsFragment.KEY_VERSION_ADDRESS,
					String.format(Globals.VERSION_URL, serviceAddress));

			// 初始注销时长
	       mConfigUtil.setString(SettingsFragment.KEY_RECLEN_TIME, "30");
    }
	/**
	 * 登录、用户注册
	 * 
	 * @param v
	 * @throws DbException
	 */
	@OnClick({ R.id.login_btn, R.id.login_bt_user_register })
	public void onClickLogin(View v) throws DbException {
		switch (v.getId()) {
			case R.id.login_btn:
				choiceMode = false;
				EkeyAPP.getInstance().setChoiceMode(choiceMode);
				login();
				break;
			case R.id.login_bt_user_register:// 注册
				mActivity.repalce(UserRegisterFragment.newInstance(), true);
				break;
			default:
				break;
		}
	}

	public static class RoleDialogFragment extends DialogFragment {
		public interface OnRoleItemClickListener {
			public void onRoleItem(ChoiceMode choiceMode, int position,
					boolean isChecked);
		}

		private DialogInterface.OnClickListener okListener;
		private DialogInterface.OnClickListener noListener;
		private List<ChoiceMode> mChoiceModes;
		private OnRoleItemClickListener mOnRoleItemClickListener;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			final ListView listView = new ListView(getActivity());
			listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			builder.setView(listView);
			builder.setTitle("请选择类型");

			final ContronAdapter<ChoiceMode> mAdapter = new ContronAdapter<ChoiceMode>(
					getActivity(),
					android.R.layout.simple_list_item_single_choice,
					mChoiceModes) {

				@Override
				public void setViewValue(ContronViewHolder viewHolder,
						ChoiceMode item, int position) {
					CheckedTextView mCheckedTextView = viewHolder
							.findView(android.R.id.text1);
					mCheckedTextView.setText(item.commuMode);
					if (item.commuMode == "通讯领域") { // 默认选择通讯领域
						mCheckedTextView.setChecked(true);
						choiceMode = true;
					}
					listView.setItemChecked(position,
							mCheckedTextView.isChecked());

				}
			};
			listView.setAdapter(mAdapter);
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					if (mOnRoleItemClickListener != null) {
						mOnRoleItemClickListener.onRoleItem(
								mAdapter.getItem(position), position,
								listView.isItemChecked(position));
					}
				}
			});

			if (okListener != null)
				builder.setPositiveButton(android.R.string.ok, okListener);
			if (noListener != null)
				builder.setNegativeButton(android.R.string.cancel, noListener);
			AlertDialog dialog = builder.create();
			return dialog;
		}

		public RoleDialogFragment setRoleData(List<ChoiceMode> choiceModes) {
			this.mChoiceModes = choiceModes;
			return this;
		}

		public RoleDialogFragment setOnRoleItemClickListener(
				OnRoleItemClickListener listener) {
			this.mOnRoleItemClickListener = listener;
			return this;
		}

		public RoleDialogFragment setPositiveOnClickListener(
				DialogInterface.OnClickListener listener) {
			this.okListener = listener;
			return this;
		}

		public RoleDialogFragment setNegativeOnClickListener(
				DialogInterface.OnClickListener listener) {
			this.noListener = listener;
			return this;
		}

		@Override
		public void show(FragmentManager manager, String tag) {
			FragmentTransaction transaction = manager.beginTransaction();
			// 指定1个过渡动画
			transaction
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.add(this, tag);
			transaction.commitAllowingStateLoss(); // 防止按home键后，show的时候调用父类中的commit方法异常
		}
	}

	/**
	 * 用户申请登录
	 */
	private void login() {

		mActivity.hideSoftInputView();
		String mobile = etMobile.getText().toString();

		if (mobile.isEmpty()) {// 用户账号不为空
			mActivity.showToast(R.string.login_msg0);
			return;
		}

		mConfigUtil.setString(Globals.KEY_MOBILE, mobile);
		String pwd = etPassword.getText().toString();

		if (pwd.isEmpty()) {// 用户密码不为空
			mActivity.showToast(R.string.login_msg1);
			return;
		}
		mConfigUtil.setString(Globals.KEY_PASS, pwd);

		if (bluetoothAdapter != null)
			btaddress = bluetoothAdapter.getAddress();

		LogUtils.e("本手机蓝牙地址" + btaddress);

		if (btaddress.isEmpty()) {// 蓝牙地址不为�?

			mActivity.showMsgBox("提示", "当前设备不支持蓝牙,无法登录！",
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			return;
		}

		String newPwd = DigestUtils.sha512Hex(pwd);
		String bt = DigestUtils.sha512Hex(btaddress);
		if (AppUtils.isNetworkAvailable(EkeyAPP.getInstance().getContext())) {// 网络可用

			JSONObject params = new JSONObject();
			try {
				params.put("username", mobile);
				params.put("password", newPwd);
				params.put("uniqueid", bt);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}

			oldUser(mobile);// 判断当前用户与上次登录用户是否相同

			mActivity.startDialog(R.string.progressDialog_title_login);

			requestHttp.doPost(Globals.MOBILE_LOGIN, params,
					new HttpRequestCallBackString() {
						@Override
						public void resultValue(final String value) {

							mActivity.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									mActivity.dismissDialog();
									if (value.contains("success")) {
										// 获取用户信息
										User user = JsonTools.getUser(value);
										if (user.getId() != 0) {
											user.setPassword(AppUtils
													.encrypt(etPassword
															.getText()
															.toString()));
											// 用户实体类保存网络状态
											user.isOnline = true;
											saveUser(user);// 保存用户信息
											gotoMainAct();// 跳转到主界面
										}

									} else {// 用户、密码校验失败
										String msg = "";
										try {
											JSONObject jsonObject = new JSONObject(
													value);
											msg = jsonObject.getString("error");
										} catch (JSONException e) {
											e.printStackTrace();
										}
										mActivity.showMsgBox("登录失败", msg,
												new OnClickListener() {

													@Override
													public void onClick(
															DialogInterface dialog,
															int which) {
														dialog.dismiss();
													}
												});
									}

								}
							});

						}
					});
		} else {// 网络不可用时
			// 离线登录
			offLogin(etMobile.getText().toString(), etPassword.getText()
					.toString());

		}

	}

	/**
	 * 离线登录判断
	 * 
	 * @param mobile
	 * @param pwd
	 */
	private void offLogin(String mobile, String pwd) {
		// 离线登录判断
		User user = EkeyAPP.getInstance().getUser();
		if (user == null) {
			mActivity.showToast("查找不到当前用户！");
			return;
		}
		if (user.getUsername().equals(mobile)
				&& user.getPassword().equals(AppUtils.encrypt(pwd))) {
			user.isOnline = false;
			showOffLoginDiaog(user);
		} else {
			mActivity.showToast("查找不到当前用户！");
		}
	}

	private void showOffLoginDiaog(final User user) {
		mActivity.showMsgBox(R.string.wsdl_error_msg0,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						saveUser(user);
						gotoMainAct();
					}
				});
	}

	/**
	 * 设置为已登录状态\保存用户信息
	 * 
	 * @param user
	 */
	private void saveUser(User user) {
		EkeyAPP.getInstance().setLoginStatus(true);// 设置为已登录状�??
		EkeyAPP.getInstance().setUser(user);
	}

	/**
	 * 跳转到MainActivity
	 */
	private void gotoMainAct() {
		MainActivity.startActivity(mActivity);
		mActivity.finish();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		inflater.inflate(R.menu.settings, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:// 进入系统设置
//			mActivity.repalce(SettingsDialogFragment.newInstance(), true);
			SettingsDialogFragment.newInstance().show(getFragmentManager(), "Login");
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 判断当前用户与上次登录用户是 否相�?
	 * 
	 * @param mobile
	 *            return 用户名相同或为空返回true
	 */
	private void oldUser(String mobile) {
		// 离线登录判断
		User user = EkeyAPP.getInstance().getUser();
		if (user == null || !user.getUsername().equals(mobile)) {
			EkeyAPP.getInstance().setOlduser(true);
		} else {
			EkeyAPP.getInstance().setOlduser(false);
		}

	}

	/**
	 * android6.0以上设置权限
	 */
	private void startPermissionsActivity() {
		int version = Build.VERSION.SDK_INT;
		if (version >= 23) {
			PermissionsActivity.startActivityForResult(mActivity, REQUEST_CODE,
					PERMISSIONS);
		} else {
			setMobile();
		}
	}

	/**
	 * 设置用户名密码
	 */
	public void setMobile() {
		// 检查是否有最后一次登录的用户
		String mobile = mConfigUtil.getString(Globals.KEY_MOBILE);
		String pass = mConfigUtil.getString(Globals.KEY_PASS);

		if (TextUtils.isEmpty(mobile)) {
			mobile = new SIMCardInfo(mActivity).getNativePhoneNumber();
		}

		if (!TextUtils.isEmpty(mobile)) {
			etMobile.setText(mobile);// 设置用户名，并移动光标到用户栏
			etMobile.setSelection(mobile.length());
			if (null != pass && !"".equals(pass))
//				etPassword.setText(pass);
			etPassword.requestFocus();
		}

		etPassword
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {

					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						// TODO Auto-generated method stub
						// login();
						return false;
					}
				});
	}
}
