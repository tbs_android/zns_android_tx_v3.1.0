package com.contron.ekeyapptx.borrowkey;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.SmartkeyBorrow;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 钥匙借出确认
 * 
 * @author luoyilong
 *
 */
public class BorrowKeyConfirm extends SwipeRefreshListFragment {

	public BasicActivity mActivity;
	private ContronAdapter<SmartkeyBorrow> mAdapter;//适配器
	private RequestHttp requestHttp;//后台请求服务
	
	//钥匙借还记录集合
	private List<SmartkeyBorrow> smartkey = new ArrayList<SmartkeyBorrow>();
	
	public static BorrowKeyConfirm newInstance() {
		BorrowKeyConfirm fragment = new BorrowKeyConfirm();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item12_name);
		mAdapter = new ContronAdapter<SmartkeyBorrow>(mActivity,
				R.layout.fargment_lend_confirm, smartkey) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					final SmartkeyBorrow item, final int position) {
				TextView tv = viewHolder.findView(android.R.id.text2);
				tv.setText("借用钥匙：" + item.getSname() + "\n借  用  人："
						+ item.getBorrow_by() + "\n借用时间：" +  item.getCreate_at());
			}
		};
		setListAdapter(mAdapter);
		super.onActivityCreated(savedInstanceState);
	}

	
	@Override
	public void onListRefresh() {
		super.onListRefresh();
		downloadLendKeyInfo();
	}
	
	
	private void downloadLendKeyInfo() {
		setRefreshing(false);
		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框
		mAdapter.setEmpty();
		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());

		// 参数设置
		RequestParams prams = new RequestParams(mActivity.getUser());
		prams.putParams("page", -1);
		prams.putParams("status", "未归还");

		// 请求
		requestHttp.doPost(Globals.SMARTKEY_BORROW, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {

						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {// 返回成功
									try {
										JSONObject json = new JSONObject(value);
										JSONArray array = json
												.getJSONArray("items");

										smartkey = new Gson().fromJson(
												array.toString(),
												new TypeToken<List<SmartkeyBorrow>>() {
												}.getType());
									} catch (JSONException e) {
										e.printStackTrace();
									}

									if (smartkey != null && smartkey.size() > 0) {
										mAdapter.addAllDatas(smartkey);
									} else {
										setEmptyText("暂无数据！");
									}

								} else {// 返回失败
									setEmptyText("暂无数据！");
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}

							}
						});
					}
				});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			mActivity.finish();
		}
		return super.onOptionsItemSelected(item);
	}


}
