package com.contron.ekeyapptx.borrowkey;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtSearchManage;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType.EvenNfcLyAddress;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.nfc.NFC;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

/**
 * 钥匙借出登记
 * 
 * @author luoyilong
 *
 */
public class BorrowKeyRegist extends Fragment {

	private BasicActivity mActivity;
	@ViewInject(R.id.borrow_key_borrow_name)
	private EditText etBorrowName;//借用钥匙人
	@ViewInject(R.id.borrow_key_choice_btn)
	private Button btnChoiceKey;//借用钥匙
	@ViewInject(R.id.borrow_key_submit_btn)
	private Button btnSubmit;//登记按钮
	@ViewInject(R.id.tx_nfc)
	private TextView txnfc;//nfc状态显示
	private NFC mNFC;
	private int smartkeyid = 0;
	private RequestHttp requestHttp = null;//后台服务请求
	protected BtMsgService mBtMsgService;//蓝牙服务
	protected BtSearchManage mBtSearchManage;//蓝牙管理工厂
	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
			if (mBtMsgService.initialize()) {
                mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
				mBtSearchManage = new BtSearchManage(mActivity,
						mBtMsgService.getBluetoothAdapter(),
						new BtSearchManage.OnSelectBtKeyListener() {

							@Override
							public void onSelectBtKey(Smartkey btKey) {
								mBtSearchManage.scanLeDevice(false);
								btnChoiceKey.setText(btKey.getName());
								btnChoiceKey
										.setBackgroundColor(R.drawable.bg_splash);
								smartkeyid = btKey.getId();

							}
						});
			} else {
				LogUtils.e("Unable to initialize Bluetooth");
				mActivity.showToast("未初始化蓝牙");
				mActivity.finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mBtMsgService = null;
		}
	};

	


	//返回当前实例
	public static BorrowKeyRegist newInstance() {
		BorrowKeyRegist fragment = new BorrowKeyRegist();
		return fragment;
	}

	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BasicActivity) getActivity();
		mNFC = new NFC(mActivity);// 实例化NFC

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item11_name);
		// 注册EventBusManager事件
		EventBusManager.getInstance().register(this);

		// 设置借用人
		etBorrowName.setText(EkeyAPP.getInstance().getUser().getName());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_borrow_key_application,
				container, false);
		ViewUtils.inject(this, view);
		return view;
	}

	
	
	@Override
	public void onResume() {
		super.onResume();
		//邦定蓝牙服务
		Intent gattServiceIntent = new Intent(mActivity, BtMsgService.class);
		mActivity.bindService(gattServiceIntent, mServiceConnection,
				Activity.BIND_AUTO_CREATE);

		// 判断NFC状态
		int nfcstate = mNFC.isNfcAdapter();
		switch (nfcstate) {
		case 0:
			txnfc.setText("[不支持NFC功能]");
			break;
		case 1:
			txnfc.setText("[NFC功能已启用]");
			break;
		case 2:
			txnfc.setText("[NFC功能未启用]");
			break;

		}

		mNFC.onStart(0);
	}

	
	
	@Override
	public void onPause() {
		super.onPause();
		mNFC.onColse();
		if (mBtMsgService != null) {
			if (mBtMsgService.isConnected())// 判断蓝牙是否连接
				mBtMsgService.disconnect();
			else
				mBtMsgService.removeCallbacksConn();//移除连接
		}
		// 解除蓝牙服务邦定
		mActivity.unbindService(mServiceConnection);
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		// 注消EventBusManager事件
		EventBusManager.getInstance().unregister(this);
	}

	
	
	// 登记按钮
	@OnClick({ R.id.borrow_key_submit_btn })
	public void OnClick(View v) {
		SubmitBorrowKeyInfo();
	}

	/**
	 * 接收TicketActivity发�?�过来的nfc蓝牙地址信息
	 * 
	 */
	public void onEventMainThread(EvenNfcLyAddress lyaddre) {
		String address = lyaddre.address;
		if (address.length() > 14)
			getSmartkey(address.substring(2, 14).toUpperCase());
	}

	/**
	 * 判断当前蓝牙地址是否在数据库中存在
	 * 
	 * @param address
	 */
	private void getSmartkey(String address) {
		Smartkey smartkey = DBHelp.getInstance(mActivity).getEkeyDao()
				.getSmartkey(address);

		if (smartkey != null) {// 己注册钥匙
			smartkeyid = smartkey.getId();
			btnChoiceKey.setText(smartkey.getName());
			btnChoiceKey.setBackgroundColor(R.drawable.bg_splash);
		} else {// 未注册钥匙
			mActivity.showMsgBox(R.string.borrowkey_noregistkey);
		}
	}

	/**
	 * 登记借出
	 */
	private void SubmitBorrowKeyInfo() {
		String addressName = btnChoiceKey.getText().toString();
		if (TextUtils.isEmpty(addressName)) {
			mActivity.showToast("请选择钥匙");
		} else {
			update();// 上传借钥匙记录

		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.selectkey, menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			mActivity.finish();
		} else if (item.getItemId() == R.id.menu_selectkey) {
			if (mBtSearchManage != null)
				mBtSearchManage.scanLeDevice(true);
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * 借得钥匙登记
	 */
	private void update() {
		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话框
		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());

		// 参数设置
		RequestParams prams = new RequestParams(mActivity.getUser());

		// 请求Url
		String url = String.format(Globals.SMARTKEY_BORROW_KID, smartkeyid);

		// 请求
		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {
			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mActivity.dismissDialog();
						if (value.contains("success")) {// 返回成功
							mActivity.showMsgBox("提示", "登记成功！",
									new OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.dismiss();
										}
									});

						} else {// 返回失败
							String error = "";
							try {
								JSONObject json = new JSONObject(value);
								error = json.getString("error");
							} catch (JSONException e) {
								e.printStackTrace();
							}
							mActivity.showToast(error);

						}

					}
				});
			}
		});

	}

}
