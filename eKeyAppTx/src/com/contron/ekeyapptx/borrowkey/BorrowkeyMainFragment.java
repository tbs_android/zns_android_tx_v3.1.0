package com.contron.ekeyapptx.borrowkey;



import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.lidroid.xutils.ViewUtils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 借钥匙-主界面
 * @author luoyilong
 *
 */
public class BorrowkeyMainFragment extends Fragment {
	private BasicActivity mActivity;


	public static BorrowkeyMainFragment newInstance() {
		return new BorrowkeyMainFragment();
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BasicActivity) getActivity();
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_borrowkey_main, 
				     container, false);
		ViewUtils.inject(view);
		return view;
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.add(R.id.fragment1, BorrowKeyRegist.newInstance(),
				BorrowKeyRegist.class.getSimpleName());
		transaction.add(R.id.fragment2, BorrowKeyList.newInstance(),
				BorrowKeyList.class.getSimpleName());
		transaction.commit();
	}
	
	
  
}
