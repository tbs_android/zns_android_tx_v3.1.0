package com.contron.ekeyapptx.borrowkey;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.SmartkeyBorrow;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;


/**
 * 借钥匙登记-显示钥匙
 * 
 * @author luoyilong
 *
 */
public class BorrowKeyList extends SwipeRefreshListFragment {

	public BasicActivity mActivity;
	private ContronAdapter<SmartkeyBorrow> mAdapter;//显示借还钥匙显示适配器
	private List<SmartkeyBorrow> listSmartKey = new ArrayList<SmartkeyBorrow>();
	private RequestHttp requestHttp;//后台请求服务


	public static BorrowKeyList newInstance() {
		BorrowKeyList fragment = new BorrowKeyList();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		setHasOptionsMenu(true);
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item11_name);

		mAdapter = new ContronAdapter<SmartkeyBorrow>(mActivity,
				R.layout.fragment_return_register, listSmartKey) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					final SmartkeyBorrow item, final int position) {
				TextView tv = viewHolder.findView(android.R.id.text2);
				String string = "";
				String state = "未归还";
				
				string = "借  用  人：" + item.getCreate_by() + "\n借用时间："
						+ item.getCreate_at();

				if (!state.equals(item.getReturn_at())) {// 归还
					string = string + "\n归还登记人：" + item.getBorrow_by()
							+ "\n归还时间：" + item.getReturn_at();
				}
				tv.setText("钥匙名称：" + item.getSname());
				tv.append("\n");
				tv.append(string);

			}
		};
		setListAdapter(mAdapter);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onListRefresh() {
		super.onListRefresh();
		downloadBorrowKeyInfo();
	}

	private void downloadBorrowKeyInfo() {

		setRefreshing(false);
		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框
		mAdapter.setEmpty();//清空适配器
		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());

		// 参数设置
		RequestParams prams = new RequestParams(mActivity.getUser());
		prams.putParams("page", 0);
		prams.putParams("username", mActivity.getUser().getUsername());

		// 请求
		requestHttp.doPost(Globals.SMARTKEY_BORROW, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {// 返回成功
									try {
										JSONObject json = new JSONObject(value);
										JSONArray array = json
												.getJSONArray("items");
										listSmartKey = new Gson().fromJson(
												array.toString(),
												new TypeToken<List<SmartkeyBorrow>>() {
												}.getType());
									} catch (JSONException e) {
										e.printStackTrace();
									}

									if (listSmartKey != null
											&& listSmartKey.size() > 0) {
										mAdapter.addAllDatas(listSmartKey);
									} else {
										setEmptyText("暂无数据！");
									}
								} else {// 返回失败
									setEmptyText("暂无数据！");
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);
								}

							}
						});
					}
				});

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			mActivity.finish();
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}


}
