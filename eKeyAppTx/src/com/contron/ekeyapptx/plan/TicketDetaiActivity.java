package com.contron.ekeyapptx.plan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.WorkPatrol;
import com.contron.ekeypublic.entities.Workbill;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

/**
 * 智能钥匙操作界面Activity
 * 
 * @author luoyilong
 *
 */
public class TicketDetaiActivity extends BasicActivity {

	@ViewInject(R.id.fragment1)
	private FrameLayout mFragmentLeft;
	private TempTask mTemptask = null;// 临时任务
	private Workbill mWorkbill = null;// 派工任务
	private WorkPatrol mWorkPatrol = null;// 巡检任务

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ticket_detai);
		ViewUtils.inject(this);
		mTemptask = getIntent().getParcelableExtra("TempTask");
		mWorkbill = getIntent().getParcelableExtra("Workbill");
		mWorkPatrol = getIntent().getParcelableExtra("WorkPatrol");

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();

		if (mTemptask != null) {
			transaction.replace(R.id.fragment1,
					TicketDetailFragment.newInstance(0, mTemptask),
					TicketDetailFragment.class.getSimpleName());
		} else if (mWorkbill != null) {
			transaction.replace(R.id.fragment1,
					TicketDetailFragment.newInstanceWorkbill(2, mWorkbill),
					TicketDetailFragment.class.getSimpleName());
		} else if (mWorkPatrol != null) {
			transaction.replace(R.id.fragment1,
					TicketDetailFragment.newInstanceWorkPatrol(3, mWorkPatrol),
					TicketDetailFragment.class.getSimpleName());
			
		} else {
			transaction.replace(R.id.fragment1,
					TicketDetailFragment.newInstance(),
					TicketDetailFragment.class.getSimpleName());
		}

		transaction.commit();

	}

	/**
	 * 界面跳转
	 * 
	 * @param context
	 */
	public static void startActivity(Context context) {
		context.startActivity(new Intent(context, TicketDetaiActivity.class));
	}

	/**
	 * 界面跳转 临时鉴权跳转过来 带参数
	 * 
	 * @param context
	 * @param temptask
	 */
	public static void startActivity(Context context, TempTask temptask) {
		Intent intent = new Intent(context, TicketDetaiActivity.class);
		intent.putExtra("TempTask", temptask);
		context.startActivity(intent);
	}

	/**
	 * 界面跳转 派工跳转过来 带参数
	 * 
	 * @param context
	 * @param temptask
	 */
	public static void startActivity(Context context, Workbill tasking) {
		Intent intent = new Intent(context, TicketDetaiActivity.class);
		intent.putExtra("Workbill", tasking);
		context.startActivity(intent);
	}

	/**
	 * 界面跳转 巡检跳转过来 带参数
	 * 
	 * @param context
	 * @param temptask
	 */
	public static void startActivity(Context context, WorkPatrol workPatrol) {
		Intent intent = new Intent(context, TicketDetaiActivity.class);
		intent.putExtra("WorkPatrol", workPatrol);
		context.startActivity(intent);
	}

}
