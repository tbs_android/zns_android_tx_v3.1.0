package com.contron.ekeyapptx.plan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.MyFragmentPagerAdapter;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;

/**
 * 我的工单
 *
 * @author luoyilong
 *
 */
public class PlanActivity extends BasicActivity {

    @ViewInject(R.id.viewpager)
    private ViewPager mv_viewPager;// viewpager控件

    @ViewInject(R.id.rad_plan)
    private RadioButton mtv_plan;


    @ViewInject(R.id.rad_fixed)
    private RadioButton mtv_fixed;



    private ArrayList<Fragment> fragmentList;

//	private int bmpW;// 横线图片宽度
//	private int offset;// 图片移动的偏移量

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        getBasicActionBar().setTitle(R.string.main_tab_item25_name);
//		mActivity = this;
        ViewUtils.inject(this);
        InitView();
    }

    /**
     * 初始化控件
     */
    private void InitView() {
        /** 标签页 **/
        mtv_plan.setOnClickListener(new txListener(0));
        mtv_fixed.setOnClickListener(new txListener(1));
        mtv_plan.setChecked(true);

        /** ViewPager **/
        fragmentList = new ArrayList<Fragment>();
        Fragment delayFragment = PlanTicketListFragment.newInstance();
        Fragment fulfillFragment = FixedSysTaskFragment.newInstance();
        fragmentList.add(delayFragment);
        fragmentList.add(fulfillFragment);
        // 给ViewPager设置适配器
        mv_viewPager.setAdapter(new MyFragmentPagerAdapter(
                getSupportFragmentManager(), fragmentList));
        mv_viewPager.setCurrentItem(0);// 设置当前显示标签页为第一页
        mv_viewPager.setOnPageChangeListener(new MyOnPageChangeListener());// 页面变化时的监听器

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.execute_work, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    /**
     * tab标签点击事件
     *
     * @author luoyilong
     *
     */
    public class txListener implements View.OnClickListener {
        private int index = 0;

        public txListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
//            int position = v.getId();
//            mtv_plan.setChecked(position == R.id.rad_plan);
//            mtv_fixed.setChecked(position == R.id.rad_fixed);
//            PlanTicketListFragment planTicketListFragment = (PlanTicketListFragment) fragmentList.get(0);
//            FixedSysTaskFragment fixedSysTaskFragment = (FixedSysTaskFragment) fragmentList.get(1);
//            planTicketListFragment.setListenToBT(position == R.id.rad_plan);
//            fixedSysTaskFragment.setListenToBT(position == R.id.rad_fixed);
            mv_viewPager.setCurrentItem(index);
        }
    }

    // 页面滑动监听
    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
//		private int one = offset * 2 + bmpW;// 两个相邻页面的偏移量

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageSelected(int arg0) {

            int position = 0;

            switch (arg0) {
                case 0:
                    position = R.id.rad_plan;
                    break;
                case 1:
                    position = R.id.rad_fixed;
                    break;
                default:
                    position = R.id.rad_plan;
                    break;
            }

            mtv_plan.setChecked(position == R.id.rad_plan);
            mtv_fixed.setChecked(position == R.id.rad_fixed);
            PlanTicketListFragment planTicketListFragment = (PlanTicketListFragment) fragmentList.get(0);
            FixedSysTaskFragment fixedSysTaskFragment = (FixedSysTaskFragment) fragmentList.get(1);
            planTicketListFragment.setListenToBT(position == R.id.rad_plan);
            fixedSysTaskFragment.setListenToBT(position == R.id.rad_fixed);

            // TODO Auto-generated method stub
            // Animation animation = new TranslateAnimation(currIndex * one,
            // arg0
            // * one, 0, 0);// 平移动画
//			currIndex = arg0;
            // animation.setFillAfter(true);// 动画终止时停留在最后一帧，不然会回到没有执行前的状态
            // animation.setDuration(200);// 动画持续时间0.2秒
            // mimg_cursor.startAnimation(animation);// 是用ImageView来显示动画的
//			int i = currIndex + 1;
//			Toast.makeText(MyWorkActivity.this, "您选择了第" + i + "个页卡",
//					Toast.LENGTH_SHORT).show();
        }
    }

    // 界面跳转
    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, PlanActivity.class);
        activity.startActivity(intent);
    }
}

