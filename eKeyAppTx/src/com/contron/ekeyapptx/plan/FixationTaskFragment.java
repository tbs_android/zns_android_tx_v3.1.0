package com.contron.ekeyapptx.plan;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtSearchManage;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.bluetooth.BtTimeOut;
import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtErrorCode;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvAckMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBackReplyMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBackReplyMsg.BtRecvBackData;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.bluetooth.code.send.BtSendBackReplyMsg;
import com.contron.ekeypublic.bluetooth.code.send.BtSendTaskMsg;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType.BtHex;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.util.LogUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FixationTaskFragment extends SwipeRefreshListFragment {

	private ContronAdapter<TempTask> mAdapter;// 适配器
	private ArrayList<TempTask> listTaskData = new ArrayList<TempTask>();//
	private BtMsgService mBtMsgService;// 蓝牙服务
	protected BtSearchManage mBtSearchManage;// 蓝牙管理工厂
	private BasicActivity mActivity;
	private RequestHttp requestHttp;// 后台请求服务
	private BtSendTaskMsg sendFixationTaskMsg; // 发送固定任务
	private String errormsg = ""; // 异常时弹出消息
	private String sendmsg = "";// 下发命令时，显示消息
	private boolean sendStatus = true;// true 下载任务，false 回传任务
	private int frameCount = 0;// 总帧数
	private int currentFrameCount = 0;// 当前帧数
	private BtSendBackReplyMsg sendBackReply = new BtSendBackReplyMsg();
	private int tid = 0;// 任务ID
	private boolean isEnd = false;// 标识是否完成 true 完成，false 未完成。
	private int uid;// 任务操作用户ID

	String isBackTask = "";// 查询返回任务ID

	// 钥匙返回操作记录集合
	private List<BtRecvBackData> btRecvBackDatas = new ArrayList<BtRecvBackData>();

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,
				IBinder service) {
			mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
			if (mBtMsgService.initialize()) {
				mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
				LogUtils.i("蓝牙服务绑定成功");
				mBtSearchManage = new BtSearchManage(mActivity,
						mBtMsgService.getBluetoothAdapter(),
						new BtSearchManage.OnSelectBtKeyListener() {
							@Override
							public void onSelectBtKey(Smartkey btKey) {
								// TODO 选择蓝牙
								scanLeDevice(false);
								mActivity
										.startDialog(R.string.main_bluetooth_msg0);
								String bttmp = btKey.getBtaddr().replaceAll(
										"(.{2})", "$1:");
								String btaddr = bttmp.substring(0,
										bttmp.length() - 1);
								EkeyAPP.getInstance().setBtAddress(btaddr);
								connBt();
							}
						});

			} else {
				LogUtils.e("Unable to initialize Bluetooth");
				mActivity.showToast("绑定蓝牙服务失败");
				mActivity.finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBtMsgService = null;
		}
	};

	public static FixationTaskFragment newInstance() {
		FixationTaskFragment fragment = new FixationTaskFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();

		// mActivity.setContentView(R.layout.activity_fixation_key_task);

		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item20_name);
		EventBusManager.getInstance().register(this);
		Intent gattServiceIntent = new Intent(mActivity, BtMsgService.class);
		mActivity.bindService(gattServiceIntent, mServiceConnection, 1);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		setAdapter();
		super.onActivityCreated(savedInstanceState);
	}

	/**
	 * 实例化适配器
	 */
	private void setAdapter() {
		mAdapter = new ContronAdapter<TempTask>(mActivity,
				R.layout.item_fixationkeytask, listTaskData) {
			@Override
			public void setViewValue(ContronViewHolder holder,
					final TempTask item, final int position) {
				holder.setText(R.id.tx_taskname, "任务名称：" + item.getName());

				holder.setText(R.id.tx_limit, "有效时长：" + item.getLimited()
						+ " 分钟");

				Button but_download = holder.findView(R.id.but_download);

				// 下载固定任务到钥匙
				but_download.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub

						errormsg = "传票失败,请重试！";
						sendmsg = "传票中...请稍候！";
						sendStatus = true;
						isEnd = false;
						// 实例化
						sendFixationTaskMsg = new BtSendTaskMsg(item);

						// 搜索蓝牙
						scanLeDevice(true);

					}
				});

				// 回传固定任务
				Button but_uploading = holder.findView(R.id.but_uploading);

				but_uploading.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						errormsg = "回传任务失败,请重试！";
						sendmsg = "任务回传中...请稍候！";
						sendStatus = false;
						isEnd = false;
						tid = item.getId();
						btRecvBackDatas.clear();

						// 搜索蓝牙
						scanLeDevice(true);
					}
				});
			}
		};

		setListAdapter(mAdapter);

	}

	/**
	 * 启动、停止\搜索
	 * 
	 * @param enable
	 */
	private void scanLeDevice(boolean enable) {

		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
			if (!mBtMsgService.isConnected())
				mBtMsgService.removeCallbacksConn();
		}

		if (mBtSearchManage != null)
			mBtSearchManage.scanLeDevice(enable);
	}

	/**
	 * 连接蓝牙
	 */
	protected void connBt() {
		LogUtils.e("连接开始");
		if (mBtMsgService != null
				&& !TextUtils.isEmpty(EkeyAPP.getInstance().getBtAddress()))
			mBtMsgService.autoConnect(EkeyAPP.getInstance().getBtAddress());
		else {
			if (mBtSearchManage != null)
				mBtSearchManage.scanLeDevice(true);
		}
	}

	@Override
	public void onListRefresh() {
		super.onListRefresh();
		setRefreshing(false);// 停止刷新

		// 下载固定任务
		downloadFixation();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		scanLeDevice(false);

		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
			if (!mBtMsgService.isConnected())
				mBtMsgService.removeCallbacksConn();
		}
		// 注销EventBusManager事件
		EventBusManager.getInstance().unregister(this);

		// 解除蓝牙服务邦定
		mActivity.unbindService(mServiceConnection);
		EkeyAPP.getInstance().removeActivity(mActivity);
	}

	/**
	 * 断开蓝牙连接
	 */
	private void disconnect() {
		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
		}
	}

	/**
	 * 接收BtMsgService发出的状态消息<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtState state) {
		switch (state) {
		case CONNECTING:// 连接中
			break;
		case DISCONNECTED:// 连接断开
			break;
		case CONNECTED:// 连接成功
			break;
		case CONNECTOUTTIME:// 连接超时
			mActivity.dismissDialog();
			showMsgBox("蓝牙连接超时,请重试!");
			break;
		case DISCOVERED:// 发现设备，可发送蓝牙命令了
			mActivity.dismissDialog();
			mActivity.startDialog(sendmsg);
			if (sendStatus) {// 发送固定任务标题
				// 先发送时间
				mBtMsgService.writeValue(sendFixationTaskMsg
						.createDateFrameMsg());
			} else {// 回传任务

				mBtMsgService.writeValue(sendBackReply
						.createTaskSelectFrameMsg());
			}

			break;
		default:
			break;
		}
	}

	/**
	 * 接收mBtTimeOut超时连接<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author luoyilong
	 * @date 2016年6月24日 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtTimeOut mBtTimeOut) {
		mActivity.dismissDialog();
		showMsgBox(errormsg);
		disconnect();
		LogUtils.i("mBtTimeOut");
	}

	/**
	 * 接收BtMsgService发出的蓝牙规约数据<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午10:25:18
	 * @param btHex
	 */
	public void onEventMainThread(BtHex btHex) {
		final String read = btHex.commd;
		if (!TextUtils.isEmpty(read)) {
			BtRecvMsg btRecvMsg = BtMsgTools.recv(read);
			if (btRecvMsg != null) {

				if (BtCommand.CODE_01.value.equals(btRecvMsg.command)) {// 接时间返回信息

					BtRecvAckMsg btRecvAckMsg = (BtRecvAckMsg) btRecvMsg;
					btRecvAckMsg.createDataSource();

					// 响应成功
					if (btRecvAckMsg.btErrorCode == BtErrorCode.SUCCESS) {
						if (BtFunctionCode.CODE_00.value
								.equals(btRecvMsg.functionCode)) {// 校时 完成返回ACK码

							// 发送任务表头
							mBtMsgService
									.writeValue(sendFixationTaskMsg
											.createTaskTitleFrameMsg(BtFunctionCode.CODE_06.value));
						}
					} else {// 校时失败
						mActivity.dismissDialog();
						showMsgBox("传票-校时失败,请重试！");
						disconnect();
					}

				} else if (BtCommand.CODE_03.value.equals(btRecvMsg.command)) {// 返回传票ACK码

					BtRecvAckMsg btRecvAckMsg = (BtRecvAckMsg) btRecvMsg;
					btRecvAckMsg.createDataSource();

					// 响应成功
					if (btRecvAckMsg.btErrorCode == BtErrorCode.SUCCESS) {

						if (BtFunctionCode.CODE_06.value
								.equals(btRecvMsg.functionCode)) {// 返回发送固定任务标题

							// 发送固定任务总帧
							mBtMsgService
									.writeValue(sendFixationTaskMsg
											.createTotalFrameMsg(BtFunctionCode.CODE_07.value));

						} else if (BtFunctionCode.CODE_07.value
								.equals(btRecvMsg.functionCode)) {// 返回发送 固定任务总帧

							// 发送固定任务分帧
							mBtMsgService
									.writeValue(sendFixationTaskMsg
											.createFenFrameMsg(BtFunctionCode.CODE_08.value));

						} else if (BtFunctionCode.CODE_08.value
								.equals(btRecvMsg.functionCode)) {// 返回发送固定任务分帧

							boolean isSendDone = sendFixationTaskMsg
									.isSendDone();

							if (!isSendDone) {// 未发送完
								// 发送固定任务分帧
								mBtMsgService
										.writeValue(sendFixationTaskMsg
												.createFenFrameMsg(BtFunctionCode.CODE_08.value));

							} else {// 发送完成
								if (!isEnd) {// 第一次接收发送完成信息
									mActivity.dismissDialog();
									isEnd = true;
									showMsgBox("任务传票成功！");
									disconnect();
								}

							}

						}

					} else {// 任务传票失败
						mActivity.dismissDialog();
						showMsgBox("任务传票失败,请检查钥匙中是否已存在任务！");
						disconnect();
					}

				} else if (BtCommand.CODE_05.value.equals(btRecvMsg.command)) {// 钥匙中任务查询应答

					isBackTask = sendBackReply.isKeyBackTask(btRecvMsg.data,
							tid);

					// 判断钥匙中是否存在当前任务
					if (!"".equals(isBackTask)) {// 存在
						mBtMsgService.writeValue(sendBackReply
								.createTaskTitleFrameMsg(isBackTask));
					} else {// 不存在
						mActivity.dismissDialog();
						showMsgBox("钥匙中无当前任务操作记录！");
						disconnect();
					}

				} else if (btRecvMsg instanceof BtRecvBackReplyMsg) {// 任务操作记录回传应答

					// 是否为响应内容
					final BtRecvBackReplyMsg backReplyMsg = (BtRecvBackReplyMsg) btRecvMsg;

					if (BtFunctionCode.CODE_00.value
							.equals(backReplyMsg.functionCode)) {// 接收总帧
						frameCount = backReplyMsg.getCountFrame();
						uid = backReplyMsg.getUserFrame();
						currentFrameCount = 1;
						getRecord();

					} else if (BtFunctionCode.CODE_01.value
							.equals(backReplyMsg.functionCode)) {// 接收帧信息
						btRecvBackDatas.addAll(backReplyMsg.parserBackData());
						getRecord();

					}

				}

			} else {// 数据解析失败
				mActivity.dismissDialog();
				// mActivity.showMsgBox(R.string.shared_bt_error1);
				disconnect();
			}
		} else {// 接收数据为空
			mActivity.dismissDialog();
			// mActivity.showMsgBox(R.string.shared_bt_error2);
			disconnect();
		}
	}

	/**
	 * 获取钥匙中的操作记录并处理
	 */
	private void getRecord() {

		if (frameCount == 0)// 没有操作记录
		{
			// 删除任务记录
			mBtMsgService.writeValue(sendBackReply
					.createDeletTaskFrameMsg(isBackTask));
			mActivity.dismissDialog();
			mActivity.showMsgBox("提示", "钥匙中无当前任务操作记录！",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							disconnect();
						}
					});
		} else if (currentFrameCount > frameCount) {// 操作记录获取完成
			mActivity.dismissDialog();
			updateFixation();// 上传操作记录

		} else {// 获取操作记录
			mBtMsgService.writeValue(sendBackReply
					.createTaskFrameMsg(currentFrameCount++));
		}

	}

	/**
	 * 下载固定任务
	 */
	private void downloadFixation() {

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

		mAdapter.setEmpty();
		User user = mActivity.getUser();

		if (user == null)
			return;

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("page", -1);
		params.putParams("status", "待下载");

		String url = String.format(Globals.PRESET_TASK_USERID_GET,
				user.getUsername());
		// 请求接口
		requestHttp.doPost(url, params, new HttpRequestCallBackString() {

			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mActivity.dismissDialog();

						if (value.contains("success")) {
							// 转换成实体对象

							try {
								JSONObject jsonObject = new JSONObject(value);

								JSONArray jsonArrayTempTask = jsonObject
										.getJSONArray("items");

								// 获取任务
								listTaskData = new Gson().fromJson(
										jsonArrayTempTask.toString(),
										new TypeToken<List<TempTask>>() {
										}.getType());

								// 获取任务下的设备、用户
								listTaskData = JsonTools.getFixationTask(
										listTaskData, jsonArrayTempTask);

								if (listTaskData != null
										&& listTaskData.size() > 0) {
									mAdapter.addAllDatas(listTaskData);
								}

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} else {
							String error;
							try {
								JSONObject jsonObject = new JSONObject(value);
								error = jsonObject.getString("error");
								showMsgBox(error);

							} catch (JSONException e) {
								e.printStackTrace();
							}

						}
					}
				});

			}
		});
	}

	/**
	 * 回传固定任务操作记录
	 */
	private void updateFixation() {

		// 无操作记录时直接返回
		if (btRecvBackDatas.size() == 0)
			return;

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话框

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());

		params.putParams("tid", tid);
		params.putParams("action", "TransPermanent");
		params.putParams("uid", uid);
		String data = (new Gson()).toJson(btRecvBackDatas);
		JSONArray RecvBackDataArray = null;
		try {
			RecvBackDataArray = new JSONArray(data);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		params.putParams("taskHistory", RecvBackDataArray);

		// 请求接口
		requestHttp.doPost(Globals.HISTORY_PRESET_TASK_NEW, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								mActivity.dismissDialog();
								if (value.contains("success")) {

									btRecvBackDatas.clear();
									mBtMsgService.writeValue(sendBackReply
											.createDeletTaskFrameMsg(isBackTask));

									mActivity
											.showMsgBox(
													"提示",
													"任务回传成功！",
													new DialogInterface.OnClickListener() {
														@Override
														public void onClick(
																DialogInterface arg0,
																int arg1) {
															disconnect();
														}
													});

								} else {
									disconnect();
									String error;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										error = jsonObject.getString("error");
										showMsgBox(error);
									} catch (JSONException e) {
										e.printStackTrace();
									}

								}
							}
						});

					}
				});
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// inflater.inflate(R.menu.temporary, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			mActivity.finish();
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 弹出提示框
	 */
	public void showMsgBox(String msg) {
		mActivity.showMsgBox("提示", msg, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
	}

}
