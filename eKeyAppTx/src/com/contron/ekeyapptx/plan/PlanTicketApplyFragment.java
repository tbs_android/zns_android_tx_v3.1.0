/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx.plan;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.ParString;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.AppUtils;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.util.FileUtil;
import com.contron.ekeypublic.util.ImageUtil;
import com.contron.ekeypublic.view.DateTimePickerFragment;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.bitmap.BitmapCommonUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 临时鉴权申请
 * 
 * @author luoyilong
 *
 */
public class PlanTicketApplyFragment extends Fragment {
	private static final int REQUEST_CODE_DEVICE = 100;
	private static final int REQUEST_CODE_HEAD = 101;
	private static final int REQUEST_CODE_IDENTITY = 102;
	@ViewInject(R.id.temporary_apply_et_user_name)
	private EditText etUserName;
	@ViewInject(R.id.temporary_apply_et_mobile)
	private EditText etMobile;
	@ViewInject(R.id.temporary_apply_et_task_name)
	private EditText etTaskName;
	@ViewInject(R.id.temporary_apply_btn_beginTime)
	private Button btnBeginTime;
	@ViewInject(R.id.temporary_apply_btn_endTime)
	private Button btnEndTime;
	@ViewInject(R.id.temporary_apply_btn_device)
	private Button btnDevice;
	@ViewInject(R.id.temporary_apply_iv_head)
	private ImageView ivHead;
	@ViewInject(R.id.temporary_apply_iv_identity)
	private ImageView ivIdentity;

	private BasicActivity mActivity;
	private ArrayList<Section> Objects = new ArrayList<Section>();
	private boolean isDelPhoto;//
	SparseArray<ParString> oid = new SparseArray<ParString>();

	private String picSaveDir = "";// 文件路径
	private String picHeadLargePath = "";// 头像保存文件
	private String picIdentityLargePath = "";// 身份证文件
	private String picHeadThumbnailPath = "";// 头像缩略图文件
	private String picIdentityThumbnailPath = "";// 身份证缩略图文件
	private String strStartTime = "";
	private String strEndTime = "";

	private RequestHttp requestHttp;// 后台服务请求
	private BitmapUtils bitmapUtils;// 图片处理对象

	private Calendar beginCalendar = Calendar.getInstance();
	private Calendar endCalendar = Calendar.getInstance();
	private ArrayList<Section> resultObject = new ArrayList<Section>();

	public static PlanTicketApplyFragment newInstance() {
		PlanTicketApplyFragment fragment = new PlanTicketApplyFragment();
		fragment.setArguments(new Bundle());
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivity = (BasicActivity) getActivity();
		EventBusManager.getInstance().register(this);

		getObject();
		bitmapUtils = new BitmapUtils(mActivity).configDefaultBitmapMaxSize(
				BitmapCommonUtils.getScreenSize(mActivity))
				.configDefaultBitmapConfig(Bitmap.Config.RGB_565);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		EventBusManager.getInstance().unregister(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_temporary_apply,
				container, false);
		ViewUtils.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mActivity.getBasicActionBar().setTitle(
				getString(R.string.temporar_apply_title,
						getString(R.string.main_tab_item1_name)));
		requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		picSaveDir = mActivity.getExternalCacheDir().getPath()
				+ "/ticketPlanApplyPic/";// 文件路径

		if (!restoreStateFromArguments()) {
			onFirstTimeLaunched();
		}
	}

	/**
	 * 设备初始化
	 */
	private void onFirstTimeLaunched() {
		//
		EventBusManager.getInstance().post(
				new EventBusType.DownLoadStationLockerDevice());
		User user = EkeyAPP.getInstance().getUser();
		// mPlanApply.setApplyBy(user.getName());
		etUserName.setText(user.getName());
		etMobile.setText(user.getUsername());
//		btnBeginTime.setText(DateUtil.getCurrentDate());
//		btnEndTime.setText(DateUtil.getCurrentDate());
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// 保存状态
		saveStateToArguments();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		saveStateToArguments();
		if (isDelPhoto) {
			File picFile = new File(picSaveDir);
			if (picFile.exists()) {// 文件存在
				FileUtil.RecursionDeleteFile(picFile);// 删除文件
			}
		}
	}

	public void onEventAsync(EventBusType.DownLoadStationLockerDevice downLoadStationLockerDevice) {
		
	}

	/**
	 * 保存数据
	 */
	private void saveStateToArguments() {
		if (getView() != null) {
			Bundle outState = new Bundle();
			// 开始时间结束时间
//			outState.putString("beginTime", btnBeginTime.getText().toString());
//			outState.putString("endTime", btnEndTime.getText().toString());
			outState.putString("beginTime", strStartTime);
			outState.putString("endTime", strEndTime);
			// 区域信息
			outState.putParcelableArrayList("Objects", Objects);

			// 选择设备
			outState.putSparseParcelableArray("oid", oid);

			// 头像
			outState.putString("picHeadLargePath", picHeadLargePath);
			outState.putString("picHeadThumbnailPath", picHeadThumbnailPath);
			// 身份证
			outState.putString("picIdentityLargePath", picIdentityLargePath);
			outState.putString("picIdentityThumbnailPath",
					picIdentityThumbnailPath);

			Bundle arguments = getArguments();
			if (arguments != null)
				arguments.putBundle("internalSavedViewState", outState);
		}
	}

	/**
	 * 获取保存数据
	 * 
	 * @return
	 */
	private boolean restoreStateFromArguments() {
		Bundle arguments = getArguments();
		if (arguments != null) {
			Bundle savedInstanceState = arguments
					.getBundle("internalSavedViewState");
			if (savedInstanceState != null) {
				// 开始时间结束时间
				btnBeginTime.setText(savedInstanceState.getString("beginTime").substring(0,16));
				btnEndTime.setText(savedInstanceState.getString("endTime").substring(0,16));

				strStartTime = savedInstanceState.getString("beginTime");
				strEndTime = savedInstanceState.getString("endTime");

				// 区域信息
				oid = savedInstanceState.getSparseParcelableArray("oid");

				Display();// 显示所选择设备

				// 头像
				picHeadLargePath = savedInstanceState
						.getString("picHeadLargePath");
				picHeadThumbnailPath = savedInstanceState
						.getString("picHeadThumbnailPath");
				displayPhoto(ivHead, picHeadThumbnailPath);
				// 身份证
				picIdentityLargePath = savedInstanceState
						.getString("picIdentityLargePath");
				picIdentityThumbnailPath = savedInstanceState
						.getString("picIdentityThumbnailPath");
				displayPhoto(ivIdentity, picIdentityThumbnailPath);

				// 所用设备
				Objects = savedInstanceState.getParcelableArrayList("Objects");

				return true;
			}
		}
		return false;
	}

	@OnClick({ R.id.temporary_apply_btn_beginTime,
			R.id.temporary_apply_btn_endTime, R.id.temporary_apply_btn_device,
			R.id.temporary_apply_iv_head, R.id.temporary_apply_iv_identity })
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.temporary_apply_btn_beginTime:
			DateTimePickerFragment datePickerBegin = DateTimePickerFragment
					.newInstance(
                            new DateTimePickerFragment.OnDateTimeDoneListener() {

								@Override
								public void onDateTime(int year,
										int monthOfYear, int dayOfMonth,
										int hourOfDay, int minute,
										String dateTime) {

									beginCalendar.set(Calendar.YEAR, year);
									beginCalendar.set(Calendar.MONTH,
											monthOfYear);
									beginCalendar.set(Calendar.DAY_OF_MONTH,
											dayOfMonth);
									beginCalendar.set(Calendar.HOUR_OF_DAY,
											hourOfDay);
									beginCalendar.set(Calendar.MINUTE, minute);

									strStartTime = dateTime;

									btnBeginTime.setText(dateTime.substring(0, 16));
								}
							}, true);
			datePickerBegin.setInitDateTime(DateUtil.getCurrentDate(DateUtil.dateFormatYMDHMS), strEndTime);
			datePickerBegin.show(mActivity.getFragmentManager(), "dateStart");
			break;
		case R.id.temporary_apply_btn_endTime:
			DateTimePickerFragment datePickerEnd = DateTimePickerFragment
					.newInstance(
							new DateTimePickerFragment.OnDateTimeDoneListener() {

								@Override
								public void onDateTime(int year,
										int monthOfYear, int dayOfMonth,
										int hourOfDay, int minute,
										String dateTime) {

									endCalendar.set(Calendar.YEAR, year);
									endCalendar.set(Calendar.MONTH,
											monthOfYear);
									endCalendar.set(Calendar.DAY_OF_MONTH,
											dayOfMonth);
									endCalendar.set(Calendar.HOUR_OF_DAY,
											hourOfDay);
									endCalendar.set(Calendar.MINUTE, minute);

									strEndTime = dateTime;

									btnEndTime.setText(dateTime.substring(0, 16));
								}
							}, true);
			if (strStartTime.length() > 0) {
				datePickerEnd.setInitDateTime(strStartTime, null);
			}
            else {
                datePickerEnd.setInitDateTime(DateUtil.getCurrentDate(DateUtil.dateFormatYMDHMS), null);
            }
			datePickerEnd.show(mActivity.getFragmentManager(), "dateEnd");
			break;
		case R.id.temporary_apply_btn_device:
			if (resultObject != null && resultObject.size() > 0) {
				DeviceActivity.startActivityForResult(this, REQUEST_CODE_DEVICE, resultObject);
			} else {
				DeviceActivity.startActivityForResult(this, REQUEST_CODE_DEVICE, Objects);
			}
			break;
		case R.id.temporary_apply_iv_head:
			picHeadLargePath = startCamera(REQUEST_CODE_HEAD);
			break;
		case R.id.temporary_apply_iv_identity:
			picIdentityLargePath = startCamera(REQUEST_CODE_IDENTITY);
			break;
		default:
			break;
		}
	}

	private String startCamera(int requestCode) {
		// 判断存储卡是否可以用，可用进行存储
		if (!AppUtils.sdCardIsExsit()) {
			mActivity.showToast("无SD卡");
			return "";
		}
		// 存放照片的文件夹
		File savedir = new File(picSaveDir);
		if (!savedir.exists()) {
			savedir.mkdirs();
		}
		String timeStamp = DateUtil.getCurrentDate("yyyyMMddHHmmss");
		String t_photoName = timeStamp + ".jpg";// 照片名称
		String largePath = picSaveDir + t_photoName;// 该照片的绝对路径
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri uri;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			String authorities = mActivity.getPackageName() + ".fileProvider";
			uri= FileProvider.getUriForFile(mActivity, authorities, new File(largePath));
		} else {
			uri = Uri.fromFile(new File(largePath));
		}
		intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		startActivityForResult(intent, requestCode);
		return largePath;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_DEVICE) {
			if (resultCode == Activity.RESULT_OK && data != null) {
				Bundle bundle = data.getExtras();
				if (bundle != null) {
					setStationsData(bundle);
				}
			}
		} else {
			if (resultCode != Activity.RESULT_CANCELED) {
				switch (requestCode) {
				case REQUEST_CODE_HEAD:

					// 创建一个线程处理，避免返回后2秒才回到当前界面
					Thread thread = new Thread(new Runnable() {
						@Override
						public void run() {
							picHeadThumbnailPath = createThumbnailPic(
									picHeadLargePath, "head_", ivHead);
						}
					});
					thread.start();

					break;
				case REQUEST_CODE_IDENTITY:

					// 创建一个线程处理，避免返回后2秒才回到当前界面
					Thread thread1 = new Thread(new Runnable() {
						@Override
						public void run() {
							picIdentityThumbnailPath = createThumbnailPic(
									picIdentityLargePath, "identity_",
									ivIdentity);
						}
					});
					thread1.start();

					break;
				}
			} else {
				// 删除图片
				FileUtil.deleteFileWithPath(picHeadLargePath);
				FileUtil.deleteFileWithPath(picIdentityLargePath);
			}
		}
	}

	/**
	 * 保存已选择的设备
	 * 
	 * @param bundle
	 */
	private void setStationsData(Bundle bundle) {
		ArrayList<Section> obj = bundle.getParcelableArrayList("stations");
		if (obj == null || obj.size() <= 0) {
			return;
		}
		resultObject.clear();
		resultObject.addAll(obj);
		if (resultObject != null && resultObject.size() > 0) {
			oid.clear();
			for (Section station : resultObject) {
				for (DeviceObject device : station.deviceList) {
					if (device.isCheck) {

						ParString parString = new ParString(device.id,
								device.getName());
						oid.append(device.id, parString);
					}
				}
			}
		}
		// 显示所选择设备
		Display();
	}

	/**
	 * 在按钮上显示选择的设备名称
	 */
	private void Display() {

		StringBuffer sb = new StringBuffer();
		if (oid != null && oid.size() > 0) {
			for (int i = 0; i < oid.size(); i++) {
				sb.append(oid.valueAt(i).getName() + ",");
			}

			sb.deleteCharAt(sb.length() - 1);
			btnDevice.setText(sb.toString());
		} else {
			btnDevice.setText(R.string.device_title);
		}

	}

	/**
	 * 创建缩略图，并显示在 ImageView
	 * 
	 * @author hupei
	 * @date 2015年5月28日 上午10:22:22
	 */
	private String createThumbnailPic(String largePath, String smallFileName,
			ImageView iv) {
		String thumbnailPath = picSaveDir + "small_" + smallFileName
				+ FileUtil.getFileName(largePath);
		File file = new File(thumbnailPath);
		// 判断是否已存在缩略图
		if (!file.exists()) {
			try {
				// 压缩生成上传的1920宽度图片
				ImageUtil.createImageThumbnail(mActivity, largePath,
						thumbnailPath, 1920, 80);
				// 删除原图
				FileUtil.deleteFileWithPath(largePath);
				displayPhoto(iv, thumbnailPath);
				return thumbnailPath;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "";

	}

	/**
	 * 把圖片显示到UI上
	 * 
	 * @param iv
	 *            ImageView
	 * @param thumbnailPath
	 *            文件
	 */
	private void displayPhoto(final ImageView iv, final String thumbnailPath) {
		if (TextUtils.isEmpty(thumbnailPath))
			return;

		if (bitmapUtils != null) {
			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					bitmapUtils.display(iv, thumbnailPath);// 显示
				}
			});

		}

	}

	private void onSaveApply() {

		String taskName = etTaskName.getText().toString().trim();

		if (TextUtils.isEmpty(taskName)) {
			mActivity.showToast("请输入任务名");
			return;
		}

		if (null == strStartTime || "".equals(strStartTime)) {
			mActivity.showToast("请选择开始时间！");
			return;
		}

		if (null == strEndTime || "".equals(strEndTime)) {
			mActivity.showToast("请选择结束时间！");
			return;
		}

		if (endCalendar.before(beginCalendar)) {
			Toast.makeText(getActivity(), "结束日期时间不能小于开始日期时间", Toast.LENGTH_LONG).show();
			return;
		}

		if (oid == null || oid.size() < 1) {
			mActivity.showToast("请选择闭锁对象！");
			return;
		}

		if (TextUtils.isEmpty(picHeadThumbnailPath)) {
			mActivity.showToast("请拍摄头像照片！");
			return;
		}

		if (TextUtils.isEmpty(picIdentityThumbnailPath)) {
			mActivity.showToast("请拍摄身份证照片！");
			return;
		}

//		String BeginTime = DateUtil.getStringByFormat(btnBeginTime.getText()
//				.toString(), "yyyyMMddHHmmss");
//		String EndTime = DateUtil.getStringByFormat(btnEndTime.getText()
//				.toString(), "yyyyMMddHHmmss");

		String BeginTime = DateUtil.getStringByFormat(strStartTime, DateUtil.dateFormatYMDHMS, DateUtil.DATE_FORMAT_YMDHMS);
		String EndTime = DateUtil.getStringByFormat(strEndTime, DateUtil.dateFormatYMDHMS, DateUtil.DATE_FORMAT_YMDHMS);

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		JSONArray object = new JSONArray();
		try {
			for (int i = 0; i < oid.size(); i++) {
				object.put(i, oid.valueAt(i).getId());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		params.putParams("name", taskName);
		params.putParams("begin_at", BeginTime);
		params.putParams("end_at", EndTime);
		params.putParams("object", object);

		mActivity.startDialog(R.string.progressDialog_title_submit);
		requestHttp.doPost(Globals.TEMP_TASK, params,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {// 返回成功
									int tid = 0;
									try {
										JSONObject json = new JSONObject(value);
										tid = json.getInt("id");
									} catch (JSONException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									if (tid != 0)
										uploadImage(tid);// 上传图片

									isDelPhoto = true;

									mActivity.showMsgBox("提示", "临时鉴权申请提交成功！",
											new OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
													mActivity.finish();
												}
											});

								} else {// 返回失败
									String error = "";
									try {
										JSONObject json = new JSONObject(value);
										error = json.getString("error");
									} catch (JSONException e) {
										e.printStackTrace();
									}
									mActivity.showToast(error);

								}

							}
						});

					}
				});

	}

	/**
	 * 获取设备
	 */
	public void getObject() {
		EkeyDao ekeyDao = DBHelp.getInstance(getContext()).getEkeyDao();
		List<Section> stations = ekeyDao.getSection();
		if (stations == null)
			stations = new ArrayList<Section>();
		Objects.clear();
		Objects.addAll(stations);

	}

	/**
	 * 上传图片
	 * 
	 * @param tid
	 */
	private void uploadImage(int tid) {
		String Url1 = "/temp_task/" + tid + "/portrait?s=1&c=android&v=1";
		String Url2 = "/temp_task/" + tid + "/idcard?s=1&c=android&v=1";

		requestHttp.uploadImage(Url1, picHeadThumbnailPath, "portrait");

		requestHttp.uploadImage(Url2, picIdentityThumbnailPath, "idcard");

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.save, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();
			break;
		case R.id.menu_save:
			onSaveApply();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
