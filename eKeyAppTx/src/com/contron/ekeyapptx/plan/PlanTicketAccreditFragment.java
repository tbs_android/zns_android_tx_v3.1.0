package com.contron.ekeyapptx.plan;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.SelectUserActivity;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.ParString;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.DateTimePickerFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 主动授权临时权限
 * 
 * @author luoyilong
 *
 */
public class PlanTicketAccreditFragment extends Fragment {
	private static final int REQUEST_CODE_DEVICE = 100;

	@ViewInject(R.id.but_apply_username)
	private Button etUserName;

	@ViewInject(R.id.temporary_apply_et_task_name)
	private EditText etTaskName;
	@ViewInject(R.id.temporary_apply_btn_beginTime)
	private Button btnBeginTime;
	@ViewInject(R.id.temporary_apply_btn_endTime)
	private Button btnEndTime;
	@ViewInject(R.id.temporary_apply_btn_device)
	private Button btnDevice;

	private BasicActivity mActivity;
	private ArrayList<Section> Objects = new ArrayList<Section>();
	SparseArray<ParString> oid = new SparseArray<ParString>();

    private String strStartTime = "";
    private String strEndTime = "";
    
	private RequestHttp requestHttp;// 后台服务请求
	private User selectUser;// 选择操作用户
	private Calendar beginCalendar = Calendar.getInstance();
	private Calendar endCalendar = Calendar.getInstance();
	private ArrayList<Section> resultObject = new ArrayList<Section>();

	public static PlanTicketAccreditFragment newInstance() {
		PlanTicketAccreditFragment fragment = new PlanTicketAccreditFragment();
		fragment.setArguments(new Bundle());
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getObject();
		mActivity = (BasicActivity) getActivity();
		EventBusManager.getInstance().register(this);

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		EventBusManager.getInstance().unregister(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_planticketaccredit,
				container, false);
		ViewUtils.inject(this, view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);

		mActivity.getBasicActionBar().setTitle(
				getString(R.string.temporar_apply_title,
						getString(R.string.main_tab_item18_name)));
		requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		if (!restoreStateFromArguments()) {
			onFirstTimeLaunched();
		}
	}

	/**
	 * 设备初始化
	 */
	private void onFirstTimeLaunched() {
		//
		EventBusManager.getInstance().post(
				new EventBusType.DownLoadStationLockerDevice());
//        btnBeginTime.setText(DateUtil.getCurrentDate());
//        btnEndTime.setText(DateUtil.getCurrentDate());
	}

	/**
	 * 获取设备
	 */
	public void getObject() {
		EkeyDao ekeyDao = DBHelp.getInstance(getContext()).getEkeyDao();
		List<Section> stations = ekeyDao.getSection();
		if (stations == null)
			stations = new ArrayList<Section>();
		Objects.clear();
		Objects.addAll(stations);

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// 保存状态
		saveStateToArguments();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		saveStateToArguments();
	}

	/**
	 * 保存数据
	 */
	private void saveStateToArguments() {
		if (getView() != null) {
			Bundle outState = new Bundle();
			// 开始时间结束时间
//            outState.putString("beginTime", btnBeginTime.getText().toString());
//            outState.putString("endTime", btnEndTime.getText().toString());
            outState.putString("beginTime", strStartTime);
            outState.putString("endTime", strEndTime);
			// 区域信息
			outState.putParcelableArrayList("Objects", Objects);

			// 选择设备
			outState.putSparseParcelableArray("oid", oid);

			Bundle arguments = getArguments();
			if (arguments != null)
				arguments.putBundle("internalSavedViewState", outState);
		}
	}

	/**
	 * 获取保存数据
	 * 
	 * @return
	 */
	private boolean restoreStateFromArguments() {
		Bundle arguments = getArguments();
		if (arguments != null) {
			Bundle savedInstanceState = arguments
					.getBundle("internalSavedViewState");
			if (savedInstanceState != null) {
				// 开始时间结束时间
				btnBeginTime.setText(savedInstanceState.getString("beginTime").substring(0,16));
				btnEndTime.setText(savedInstanceState.getString("endTime").substring(0,16));

                strStartTime = savedInstanceState.getString("beginTime");
                strEndTime = savedInstanceState.getString("endTime");
                
				// 区域信息
				oid = savedInstanceState.getSparseParcelableArray("oid");

				Display();// 显示所选择设备

				// 所用设备
				Objects = savedInstanceState.getParcelableArrayList("Objects");

				return true;
			}
		}
		return false;
	}

	/**
	 * ����{@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015��9��25�� ����11:40:38
	 * @param downLoadStationLockerDevice
	 */
	public void onEventAsync(
			EventBusType.DownLoadStationLockerDevice downLoadStationLockerDevice) {
		List<Section> stations = DBHelp.getInstance(mActivity).getEkeyDao()
				.getSection();

		if (stations == null)
			stations = new ArrayList<Section>();

		Objects.clear();
		Objects.addAll(stations);

	}

	@OnClick({ R.id.temporary_apply_btn_beginTime,
			R.id.temporary_apply_btn_endTime, R.id.temporary_apply_btn_device,
			R.id.but_apply_username })
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.temporary_apply_btn_beginTime:
			DateTimePickerFragment datePickerBegin = DateTimePickerFragment
					.newInstance(
							new DateTimePickerFragment.OnDateTimeDoneListener() {

								@Override
								public void onDateTime(int year,
										int monthOfYear, int dayOfMonth,
										int hourOfDay, int minute,
										String dateTime) {

									beginCalendar.set(Calendar.YEAR, year);
									beginCalendar.set(Calendar.MONTH,
											monthOfYear);
									beginCalendar.set(Calendar.DAY_OF_MONTH,
											dayOfMonth);
									beginCalendar.set(Calendar.HOUR_OF_DAY,
											hourOfDay);
									beginCalendar.set(Calendar.MINUTE, minute);
                                    
                                    strStartTime = dateTime;

									btnBeginTime.setText(dateTime.substring(0,16));
								}
							}, true);
            datePickerBegin.setInitDateTime(DateUtil.getCurrentDate(DateUtil.dateFormatYMDHMS), strEndTime);
			datePickerBegin.show(mActivity.getFragmentManager(), "dateStart");
			break;
		case R.id.temporary_apply_btn_endTime:
			DateTimePickerFragment datePickerEnd = DateTimePickerFragment
					.newInstance(
							new DateTimePickerFragment.OnDateTimeDoneListener() {

								@Override
								public void onDateTime(int year,
										int monthOfYear, int dayOfMonth,
										int hourOfDay, int minute,
										String dateTime) {

									endCalendar.set(Calendar.YEAR, year);
									endCalendar.set(Calendar.MONTH,
											monthOfYear);
									endCalendar.set(Calendar.DAY_OF_MONTH,
											dayOfMonth);
									endCalendar.set(Calendar.HOUR_OF_DAY,
											hourOfDay);
									endCalendar.set(Calendar.MINUTE, minute);
                                    
                                    strEndTime = dateTime;

									btnEndTime.setText(dateTime.substring(0,16));
								}
							}, true);
            if (strStartTime.length() > 0) {
                datePickerEnd.setInitDateTime(strStartTime, null);
            }
            else {
                datePickerEnd.setInitDateTime(DateUtil.getCurrentDate(DateUtil.dateFormatYMDHMS), null);
            }
			datePickerEnd.show(mActivity.getFragmentManager(), "dateEnd");
			break;
		case R.id.temporary_apply_btn_device:
			if (resultObject != null && resultObject.size() > 0) {
				DeviceActivity.startActivityForResult(this, REQUEST_CODE_DEVICE, resultObject);
			} else {
				DeviceActivity.startActivityForResult(this, REQUEST_CODE_DEVICE, Objects);
			}
			break;
		case R.id.but_apply_username:
			SelectUserActivity.startActivityForResult(this);
			break;
		default:
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_DEVICE) {
			if (resultCode == Activity.RESULT_OK && data != null) {
				Bundle bundle = data.getExtras();
				if (bundle != null) {
					setStationsData(bundle);
				}
			}
		} else if (requestCode == SelectUserActivity.REQUESTCODE
				&& resultCode == SelectUserActivity.RESULTCODE) {
			selectUser = data.getParcelableExtra("user");
			etUserName.setText(selectUser.getName());
		}
	}

	/**
	 * 保存已选择的设备
	 * 
	 * @param bundle
	 */
	private void setStationsData(Bundle bundle) {
		ArrayList<Section> obj = bundle.getParcelableArrayList("stations");
		if (obj == null || obj.size() <= 0) {
			return;
		}
		resultObject.clear();
		resultObject.addAll(obj);
		if (resultObject != null && resultObject.size() > 0) {
			oid.clear();
			for (Section station : resultObject) {
				for (DeviceObject device : station.deviceList) {
					if (device.isCheck) {

						ParString parString = new ParString(device.id,
								device.getName());
						oid.append(device.id, parString);
					}
				}
			}

		}
//		List<DeviceObject> Object = bundle.getParcelableArrayList("stations");
//		if (Object != null && Object.size() > 0) {
//
//			for (DeviceObject device : Object) {
//				
//					if (state && oid != null) {// 第一次进入时清空数据
//									oid.clear();
//									state = false;
//					}
//		
//					ParString parString = new ParString(device.id,
//										device.getName());
//					oid.append(device.id, parString);
//						
//			}
//		}

		// 显示所选择设备
		Display();
	}

	/**
	 * 在按钮上显示选择的设备名称
	 */
	private void Display() {

		StringBuffer sb = new StringBuffer();
		if (oid != null && oid.size() > 0) {
			for (int i = 0; i < oid.size(); i++) {
				sb.append(oid.valueAt(i).getName() + ",");
			}

			sb.deleteCharAt(sb.length() - 1);
			btnDevice.setText(sb.toString());
		} else {
			btnDevice.setText(R.string.device_title);
		}

	}

	private void onSaveApply() {
		User user= EkeyAPP.getInstance().getUser();
		String taskName = etTaskName.getText().toString().trim();

		if (TextUtils.isEmpty(taskName)) {
			mActivity.showToast("请输入任务名");
			return;
		}
		if (selectUser==null) {
			mActivity.showToast("请选择操作人");
			return;
		}
        
        if (null == strStartTime || "".equals(strStartTime)) {
            mActivity.showToast("请选择开始时间！");
            return;
        }
        
        if (null == strEndTime || "".equals(strEndTime)) {
            mActivity.showToast("请选择结束时间！");
            return;
        }

		if (endCalendar.before(beginCalendar)) {
			Toast.makeText(getActivity(), "结束日期时间不能小于开始日期时间", Toast.LENGTH_LONG).show();
			return;
		}

		if (oid == null || oid.size() < 1) {
			mActivity.showToast("请选择闭锁对象！");
			return;
		}

        String BeginTime = DateUtil.getStringByFormat(strStartTime, DateUtil.dateFormatYMDHMS, DateUtil.DATE_FORMAT_YMDHMS);
        String EndTime = DateUtil.getStringByFormat(strEndTime, DateUtil.dateFormatYMDHMS, DateUtil.DATE_FORMAT_YMDHMS);

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		JSONArray object = new JSONArray();
		try {
			for (int i = 0; i < oid.size(); i++) {
				object.put(i, oid.valueAt(i).getId());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		params.putParams("name", taskName);
		params.putParams("begin_at", BeginTime);
		params.putParams("end_at", EndTime);
		params.putParams("object", object);

		String url=String.format(Globals.USER_USERNAME_TEMP_TASK_NEW, selectUser.getUsername());
		
		mActivity.startDialog(R.string.progressDialog_title_submit);
		requestHttp.doPost(url, params,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {// 返回成功

									mActivity.showMsgBox("提示", "临时权限授权提交成功！",
											new OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
													mActivity.finish();
												}
											});

								} else {// 返回失败
									mActivity.showMsgBox("提示", "临时权限授权提交失败！",
											new OnClickListener() {
												@Override
												public void onClick(
														DialogInterface dialog,
														int which) {
													dialog.dismiss();
												}
											});

								}

							}
						});

					}
				});

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.save, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			mActivity.finish();
			break;
		case R.id.menu_save:
			onSaveApply();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
