/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx.plan;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.doorlock.DoorLockerActivity;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.bluetooth.BtMsgService;
import com.contron.ekeypublic.bluetooth.BtSearchManage;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.bluetooth.BtTimeOut;
import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtErrorCode;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvAckMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBackReplyMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBackReplyMsg.BtRecvBackData;
import com.contron.ekeypublic.bluetooth.code.send.BtSendBackReplyMsg;
import com.contron.ekeypublic.bluetooth.code.send.BtSendTaskMsg;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType.BtHex;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.map.DisplayActivity;
import com.contron.ekeypublic.view.SwipeRefreshListFragment;
import com.contron.ekeyapptx.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.util.LogUtils;

/**
 * 临时鉴权开锁操作
 *
 * @author luoyilong
 *
 */
public class PlanTicketListFragment extends SwipeRefreshListFragment {
	public static final String KEY_LIST_REFRESH = "TemporaryListFragmentRefresh";
	private BasicActivity mActivity;
	private RequestHttp requestHttp;
	private ContronAdapter<TempTask> mAdapter;
	private BtMsgService mBtMsgService;// 蓝牙服务
	protected BtSearchManage mBtSearchManage;// 蓝牙管理工厂

	private boolean state = false;// true:从TicketDetaiActivity跳转过来
	private BtSendTaskMsg sendFixationTaskMsg; // 发送固定任务
	private String errormsg = ""; // 异常时弹出消息
	private String sendmsg = "";// 下发命令时，显示消息
	private boolean sendStatus = true;// true 下载任务，false 回传任务
	private int frameCount = 0;// 总帧数
	private int currentFrameCount = 0;// 当前帧数
	private BtSendBackReplyMsg sendBackReply = new BtSendBackReplyMsg();
	private int tid = 0;// 任务ID
	private boolean isEnd = false;// 标识是否完成 true 完成，false 未完成。
	private int uid;// 任务操作用户ID
	private User user = null;
	private boolean hasRegiste = false;

	String isBackTask = "";// 查询返回任务ID

	// 钥匙返回操作记录集合
	private List<BtRecvBackData> btRecvBackDatas = new ArrayList<BtRecvBackData>();

	private ServiceConnection mServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName componentName,
									   IBinder service) {
			mBtMsgService = ((BtMsgService.LocalBinder) service).getService();
			if (mBtMsgService.initialize()) {
				mBtMsgService.setSCLock(EkeyAPP.getInstance().isSCLock());
				LogUtils.i("蓝牙服务绑定成功");
				mBtSearchManage = new BtSearchManage(mActivity,
						mBtMsgService.getBluetoothAdapter(),
						new BtSearchManage.OnSelectBtKeyListener() {
							@Override
							public void onSelectBtKey(Smartkey btKey) {
								// TODO 选择蓝牙
								scanLeDevice(false);
								mActivity
										.startDialog(R.string.main_bluetooth_msg0);
								String bttmp = btKey.getBtaddr().replaceAll(
										"(.{2})", "$1:");
								String btaddr = bttmp.substring(0,
										bttmp.length() - 1);
								EkeyAPP.getInstance().setBtAddress(btaddr);
								connBt();
							}
						});

			} else {
				LogUtils.e("Unable to initialize Bluetooth");

				mActivity.showToast("绑定蓝牙服务失败");
				mActivity.finish();
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			mBtMsgService = null;
		}
	};

	public static PlanTicketListFragment newInstance() {
		PlanTicketListFragment fragment = new PlanTicketListFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mActivity = (BasicActivity) getActivity();

		if (!EkeyAPP.getInstance().isSCLock() && !hasRegiste) {
			hasRegiste = true;
			EventBusManager.getInstance().register(this);
			Intent gattServiceIntent = new Intent(mActivity, BtMsgService.class);
			mActivity.bindService(gattServiceIntent, mServiceConnection, 1);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		user = EkeyAPP.getInstance().getUser();
		setHasOptionsMenu(true);

		mAdapter = new ContronAdapter<TempTask>(mActivity,
				R.layout.fragment_temp_task_unlock_item,
				new ArrayList<TempTask>()) {
			@Override
			public void setViewValue(ContronViewHolder holder,
									 final TempTask item, final int position) {
				holder.setText(R.id.tx_taskname, "任务名称：" + item.getName());

				if (!TextUtils.isEmpty(item.getBeginAt()))
//					holder.setText(R.id.tx_begin_at,
//							"开始时间：" + DateUtil.timeCompare(item.getBeginAt()));
					holder.setText(R.id.tx_begin_at,
							"开始时间：" + item.getBeginAt());
				if (!TextUtils.isEmpty(item.getEndAt()))
//					holder.setText(R.id.tx_end_at,
//							"结束时间：" + DateUtil.timeCompare(item.getEndAt()));
					holder.setText(R.id.tx_end_at,
							"结束时间：" + item.getEndAt());

				// 查看地图
				Button but_state = holder.findView(R.id.but_state);
				but_state.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						DisplayActivity.startActivity(mActivity, item.getId());
					}
				});

				// 下载任务
				Button but_download = holder.findView(R.id.but_download);
				but_download.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {

						errormsg = "任务下载失败,请重试！";
						sendmsg = "传票中...请稍候！";
						sendStatus = true;
						isEnd = false;

						// 添加当前用户到任务中
						List<User> userlist = new ArrayList<User>();
						userlist.add(user);
						tid = item.getId();
						item.setUserList(userlist);
						// 实例化
						sendFixationTaskMsg = new BtSendTaskMsg(item);

						// 搜索蓝牙
						scanLeDevice(true);

					}
				});

				// 回传任务
				Button but_uploading = holder.findView(R.id.but_uploading);
				but_uploading.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {

						errormsg = "回传任务失败,请重试！";
						sendmsg = "任务回传中...请稍候!";
						sendStatus = false;
						isEnd = false;
						tid = item.getId();
						btRecvBackDatas.clear();

						// 搜索蓝牙
						scanLeDevice(true);

					}
				});
				if (EkeyAPP.getInstance().isSCLock()) {
					but_download.setVisibility(View.INVISIBLE);
					but_uploading.setVisibility(View.INVISIBLE);
				}

			}
		};
		setListAdapter(mAdapter);

		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onListRefresh() {
		super.onListRefresh();
		setRefreshing(false);// 停止刷新
		downloadTicket();
	}

	@Override
	public void onResume() {
		super.onResume();
		mActivity.getBasicActionBar().setTitle(R.string.main_tab_item6_name);
        if (state) {// 从TicketDetaiActivity跳转过来
			if (!EkeyAPP.getInstance().isSCLock() && !hasRegiste) {
				hasRegiste = true;
				EventBusManager.getInstance().register(this);
				Intent gattServiceIntent = new Intent(mActivity, BtMsgService.class);
				mActivity.bindService(gattServiceIntent, mServiceConnection, 1);


				downloadTicket();
			}
            state = false;
        }
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			onResume();
		}
	}

	/**
	 * 启动、停止\搜索
	 *
	 * @param enable
	 */
	private void scanLeDevice(boolean enable) {

		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
			if (!mBtMsgService.isConnected())
				mBtMsgService.removeCallbacksConn();
		}

		if (mBtSearchManage != null)
			mBtSearchManage.scanLeDevice(enable);
	}

	/**
	 * 连接蓝牙
	 */
	protected void connBt() {
		LogUtils.e("连接开始");
		if (mBtMsgService != null
				&& !TextUtils.isEmpty(EkeyAPP.getInstance().getBtAddress()))
			mBtMsgService.autoConnect(EkeyAPP.getInstance().getBtAddress());
		else {
			if (mBtSearchManage != null)
				mBtSearchManage.scanLeDevice(true);
		}
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (!EkeyAPP.getInstance().isSCLock() && hasRegiste) {
			hasRegiste = false;
			scanLeDevice(false);
			// 注销EventBusManager事件
			EventBusManager.getInstance().unregister(this);
			if (mBtMsgService != null) {
				mBtMsgService.disconnect();
				if (!mBtMsgService.isConnected())
					mBtMsgService.removeCallbacksConn();
			}
			// 解除蓝牙服务邦定
			mActivity.unbindService(mServiceConnection);
		}

		EkeyAPP.getInstance().removeActivity(mActivity);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		TempTask tempTask = mAdapter.getItem(position);
		String statu = tempTask.getStatus();

		if (TextUtils.isEmpty(statu))
			return;

		if (!EkeyAPP.getInstance().isSCLock() && hasRegiste) {
            hasRegiste = false;
			scanLeDevice(false);
			// 注销EventBusManager事件
			EventBusManager.getInstance().unregister(this);
			if (mBtMsgService != null) {
				mBtMsgService.disconnect();
				if (!mBtMsgService.isConnected())
					mBtMsgService.removeCallbacksConn();
			}
			// 解除蓝牙服务邦定
			mActivity.unbindService(mServiceConnection);
		}

        state = true;
		// 跳转到蓝牙开锁界面
        DoorLockerActivity.startActivity(mActivity, tempTask);
	}

	/**
	 * 下载临时申请任务
	 */
	private void downloadTicket() {

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框
		mAdapter.setEmpty();
		List<String> status = new ArrayList<String>();
		status.add("待下载");
		status.add("已下载");
		status.add("已执行");
		JSONArray jsonstatu = new JSONArray(status);

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());
		params.putParams("status", jsonstatu);
		params.putParams("page", 0);
		params.putParams("exceedflag", 1);//exceedflag过期标识，1未过期，0过期 
		String username = EkeyAPP.getInstance().getUser().getUsername();

		// 请求接口
		String url = String.format(Globals.USER_NAME_TEMP_TASK, username);

		requestHttp.doPost(url, params, new HttpRequestCallBackString() {

			@Override
			public void resultValue(final String value) {
				mActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {

						mActivity.dismissDialog();

						if (value.contains("success")) {
							ArrayList<TempTask> listTempTask;
							try {
								JSONObject jsonObject = new JSONObject(value);
								JSONArray jsonArrayTempTask = jsonObject
										.getJSONArray("items");
								// 获取任务
								listTempTask = new Gson().fromJson(
										jsonArrayTempTask.toString(),
										new TypeToken<List<TempTask>>() {
										}.getType());

								// 获取任务下的设备
								listTempTask = JsonTools.getlistTempTask(
										listTempTask, jsonArrayTempTask);

								if (listTempTask != null
										&& listTempTask.size() > 0) {
									mAdapter.addAllDatas(listTempTask);

									// 保存到数据库
									DBHelp.getInstance(mActivity).getEkeyDao()
											.saveTempTaskObject(listTempTask);

									setEmptyViewShown(false);
								} else {
									setEmptyText("暂无数据！");
									setEmptyViewShown(true);
								}

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} else {
							setEmptyText("暂无数据！");
							setEmptyViewShown(true);
						}
					}
				});

			}
		});
	}

	/**
	 * 接收BtMsgService发出的状态消息<br>
	 * {@linkplain EventBusManager#post(Object)}
	 *
	 * @author hupei
	 * @date 2015年9月22日 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtState state) {
		switch (state) {
			case CONNECTING:// 连接中
				break;
			case DISCONNECTED:// 连接断开
				break;
			case CONNECTED:// 连接成功
				break;
			case CONNECTOUTTIME:// 连接超时
				mActivity.dismissDialog();
				showMsgBox("蓝牙连接超时,请重试!");
				break;
			case DISCOVERED:// 发现设备，可发送蓝牙命令了
				if (!EkeyAPP.getInstance().isSCLock()) {
					mActivity.dismissDialog();
					mActivity.startDialog(sendmsg);
					if (sendStatus) {// 下载任务
						// 先发送时间
						mBtMsgService.writeValue(sendFixationTaskMsg
								.createDateFrameMsg());
					} else {// 回传任务

						mBtMsgService.writeValue(sendBackReply
								.createTaskSelectFrameMsg());
					}
				}
				break;
			default:
				break;
		}
	}

	/**
	 * 接收mBtTimeOut超时连接<br>
	 * {@linkplain EventBusManager#post(Object)}
	 *
	 * @author luoyilong
	 * @date 2016年6月24日 上午9:36:04
	 * @param mBtTimeOut
	 */
	public void onEventMainThread(BtTimeOut mBtTimeOut) {
		mActivity.dismissDialog();
		showMsgBox(errormsg);
		LogUtils.i("mBtTimeOut");
		disconnect();
	}

	/**
	 * 接收BtMsgService发出的蓝牙规约数据<br>
	 * {@linkplain EventBusManager#post(Object)}
	 *
	 * @author hupei
	 * @date 2015年9月22日 上午10:25:18
	 * @param btHex
	 */
	public void onEventMainThread(BtHex btHex) {
		final String read = btHex.commd;
		if (!TextUtils.isEmpty(read)) {
			BtRecvMsg btRecvMsg = BtMsgTools.recv(read);
			if (btRecvMsg != null) {
				if (BtCommand.CODE_01.value.equals(btRecvMsg.command)) {// 接时间返回信息

					BtRecvAckMsg btRecvAckMsg = (BtRecvAckMsg) btRecvMsg;
					btRecvAckMsg.createDataSource();
					// 响应成功
					if (btRecvAckMsg.btErrorCode == BtErrorCode.SUCCESS) {

						if (BtFunctionCode.CODE_00.value
								.equals(btRecvMsg.functionCode)) {// 校时 完成返回ACK码
							// 发送任务表头
							mBtMsgService
									.writeValue(sendFixationTaskMsg
											.createTaskTitleFrameMsg(BtFunctionCode.CODE_00.value));
						}
					} else {// 校时失败
						mActivity.dismissDialog();
						showMsgBox("传票-校时失败,请重试！");
						disconnect();
					}

				} else if (BtCommand.CODE_03.value.equals(btRecvMsg.command)) {// 返回传票ACK码

					BtRecvAckMsg btRecvAckMsg = (BtRecvAckMsg) btRecvMsg;
					btRecvAckMsg.createDataSource();

					// 响应成功
					if (btRecvAckMsg.btErrorCode == BtErrorCode.SUCCESS) {

						if (BtFunctionCode.CODE_00.value
								.equals(btRecvMsg.functionCode)) {// 返回发送任务标题

							// 发送任务总帧
							mBtMsgService
									.writeValue(sendFixationTaskMsg
											.createTotalFrameMsg(BtFunctionCode.CODE_01.value));

						} else if (BtFunctionCode.CODE_01.value
								.equals(btRecvMsg.functionCode)) {// 返回发送任务总帧

							// 发送任务分帧
							mBtMsgService
									.writeValue(sendFixationTaskMsg
											.createFenFrameMsg(BtFunctionCode.CODE_02.value));

						} else if (BtFunctionCode.CODE_02.value
								.equals(btRecvMsg.functionCode)) {// 返回发送任务分帧

							boolean isSendDone = sendFixationTaskMsg
									.isSendDone();

							if (!isSendDone) {// 未发送完
								// 发送任务分帧
								mBtMsgService
										.writeValue(sendFixationTaskMsg
												.createFenFrameMsg(BtFunctionCode.CODE_02.value));

							} else {// 发送完成
								if (!isEnd) {// 第一次接收发送完成信息
									mActivity.dismissDialog();
									isEnd = true;
									String url = String.format(Globals.UPDATESTATUS_TEMP_TAKS, tid);
									// 设置参数
									RequestParams params = new RequestParams(mActivity.getUser());
									params.putParams("status", "已下载");
									requestHttp.doPost(url, params, new HttpRequestCallBackString() {
												@Override
												public void resultValue(String value) {
													if(!value.toUpperCase().contains("SUCCESS"))
														showMsgBox("任务状态更新失败！");
												}
											});
											showMsgBox("任务传票成功！");
									disconnect();
								}

							}

						}
					} else {// 任务传票失败
						mActivity.dismissDialog();
						showMsgBox("任务传票失败,请检查钥匙中是否已存在任务！");
						disconnect();
					}

				} else if (BtCommand.CODE_05.value.equals(btRecvMsg.command)) {// 钥匙中任务查询应答

					isBackTask = sendBackReply.isKeyBackTask(btRecvMsg.data,
							tid);

					// 判断钥匙中是否存在当前任务
					if (!"".equals(isBackTask)) {// 存在
						mBtMsgService.writeValue(sendBackReply
								.createTaskTitleFrameMsg(isBackTask));
					} else {// 不存在
						mActivity.dismissDialog();
						showMsgBox("钥匙中无当前任务操作记录！");
						disconnect();
					}

				} else if (btRecvMsg instanceof BtRecvBackReplyMsg) {// 任务操作记录回传应答

					// 是否为响应内容
					final BtRecvBackReplyMsg backReplyMsg = (BtRecvBackReplyMsg) btRecvMsg;

					if (BtFunctionCode.CODE_00.value
							.equals(backReplyMsg.functionCode)) {// 接收总帧应答
						frameCount = backReplyMsg.getCountFrame();
						uid = backReplyMsg.getUserFrame();
						currentFrameCount = 1;
						getRecord();

					} else if (BtFunctionCode.CODE_01.value
							.equals(backReplyMsg.functionCode)) {// 接收帧信息应答
						btRecvBackDatas.addAll(backReplyMsg.parserBackData());
						getRecord();

					}

				}
			} else {// 数据解析失败
				mActivity.dismissDialog();
				// mActivity.showMsgBox(R.string.shared_bt_error1);
				disconnect();
			}
		} else {// 接收数据为空
			mActivity.dismissDialog();
			// mActivity.showMsgBox(R.string.shared_bt_error2);
			disconnect();
		}
	}

	/**
	 * 获取钥匙中的操作记录并处理
	 */
	private void getRecord() {

		if (frameCount == 0)// 没有操作记录
		{
			// 删除任务记录
			mBtMsgService.writeValue(sendBackReply
					.createDeletTaskFrameMsg(isBackTask));
			String url = String.format(Globals.UPDATESTATUS_TEMP_TAKS, tid);
			// 设置参数
			RequestParams params = new RequestParams(mActivity.getUser());
			params.putParams("status", "待下载");
			requestHttp.doPost(url, params, new HttpRequestCallBackString() {
				@Override
				public void resultValue(String value) {
					if(!value.toUpperCase().contains("SUCCESS"))
						showMsgBox("任务状态更新失败！");
				}
			});
			mActivity.dismissDialog();
			mActivity.showMsgBox("提示", "钥匙中无当前任务操作记录！",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
							disconnect();
						}
					});

		} else if (currentFrameCount > frameCount) {// 操作记录获取完成
			mActivity.dismissDialog();
			updateTask();// 上传操作记录
//			String url = String.format(Globals.UPDATESTATUS_TEMP_TAKS, tid);
//			// 设置参数
//			RequestParams params = new RequestParams(mActivity.getUser());
//			params.putParams("status", "已执行");
//			requestHttp.doPost(url, params, new HttpRequestCallBackString() {
//				@Override
//				public void resultValue(String value) {
//					if(!value.toUpperCase().contains("SUCCESS"))
//						showMsgBox("任务状态更新失败！");
//				}
//			});

		} else {// 获取操作记录
			mBtMsgService.writeValue(sendBackReply
					.createTaskFrameMsg(currentFrameCount++));
		}

	}

	/**
	 * 回传任务操作记录
	 */
	private void updateTask() {

		// 无操作记录时直接返回
		if (btRecvBackDatas.size() == 0)
			return;

		if (requestHttp == null)
			requestHttp = new RequestHttp(mActivity.getWld());// 服务请求

		mActivity.startDialog(R.string.progressDialog_title_submit);// 弹出对话框

		// 设置参数
		RequestParams params = new RequestParams(mActivity.getUser());

		params.putParams("tid", tid);
		params.putParams("action", "TransTemp");
		params.putParams("uid", uid);
		String data = (new Gson()).toJson(btRecvBackDatas);
		JSONArray RecvBackDataArray = null;
		try {
			RecvBackDataArray = new JSONArray(data);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		params.putParams("taskHistory", RecvBackDataArray);

		// 请求接口
		requestHttp.doPost(Globals.HISTORY_PRESET_TASK_NEW, params,
				new HttpRequestCallBackString() {

					@Override
					public void resultValue(final String value) {
						mActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mActivity.dismissDialog();
								if (value.contains("success")) {
									btRecvBackDatas.clear();
									mBtMsgService.writeValue(sendBackReply
											.createDeletTaskFrameMsg(isBackTask));

									mActivity.showMsgBox(
													"提示",
													"任务回传成功！",
													new DialogInterface.OnClickListener() {
														@Override
														public void onClick(
																DialogInterface arg0,
																int arg1) {
															disconnect();
															onListRefresh();
														}
													});

								} else {
									disconnect();
									String error;
									try {
										JSONObject jsonObject = new JSONObject(
												value);
										error = jsonObject.getString("error");
										showMsgBox(error);
									} catch (JSONException e) {
										e.printStackTrace();
									}

								}
							}
						});

					}
				});
	}

	/**
	 * 断开蓝牙连接
	 */
	private void disconnect() {
		if (mBtMsgService != null) {
			mBtMsgService.disconnect();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// inflater.inflate(R.menu.temporary, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home)
			mActivity.finish();
		return super.onOptionsItemSelected(item);
	}

	/**
	 * 弹出提示框
	 */
	public void showMsgBox(String msg) {
		mActivity.showMsgBox("提示", msg, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
	}

	public void setListenToBT(boolean flag) {

		if (flag) {
			if (!EkeyAPP.getInstance().isSCLock() && !hasRegiste) {
				hasRegiste = true;
				EventBusManager.getInstance().register(this);
				Intent gattServiceIntent = new Intent(mActivity, BtMsgService.class);
				mActivity.bindService(gattServiceIntent, mServiceConnection, 1);
			}
		}
		else {
			if (!EkeyAPP.getInstance().isSCLock() && hasRegiste) {
				hasRegiste = false;
				// 注销EventBusManager事件
				EventBusManager.getInstance().unregister(this);
				if (mBtMsgService != null) {
					mBtMsgService.disconnect();
					if (!mBtMsgService.isConnected())
						mBtMsgService.removeCallbacksConn();
				}
				// 解除蓝牙服务邦定
				mActivity.unbindService(mServiceConnection);
			}
		}
	}
}
