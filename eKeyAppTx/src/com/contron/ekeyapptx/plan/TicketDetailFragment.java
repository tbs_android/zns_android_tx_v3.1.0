package com.contron.ekeyapptx.plan;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicBtFragment;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.key.EmpowerActivity;
import com.contron.ekeypublic.bluetooth.BtConnAuto;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.bluetooth.BtTimeOut;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.WorkPatrol;
import com.contron.ekeypublic.entities.Workbill;
import com.contron.ekeypublic.eventbus.EventBusType.BtBatt;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

/**
 * 智能钥匙操作界面Fragment
 * 
 * @author luoyilong
 *
 */
public class TicketDetailFragment extends BasicBtFragment {
	@ViewInject(R.id.eicket_execute_tv_userName)
	private TextView tvUserName;
	@ViewInject(R.id.eicket_execute_tv_mobile)
	private TextView tvMobile;
	@ViewInject(R.id.eicket_execute_tv_unit)
	private TextView tvUnit;
	@ViewInject(R.id.eicket_execute_btn)
	private Button btnExecute;
	@ViewInject(R.id.eicket_execute_tv_batt)
	private TextView tvBatt;
	// 0=临时任务跳转，已经携带数据；1=鉴权开锁跳转，2=派工任务，从服务器下载数据
	private int mType;
	// 标识离线鉴权是否可以操作，不能操作就不弹出查钥匙界面
	// private boolean mHasOfflineData = false;
	// private FixedTicketHead mTicketHead;
	private boolean mBattMin = true;// 电量低

	private TempTask mTemptask = null;// 临时任务

	private Workbill mWorkbill = null;// 派工任务
	
	private WorkPatrol mWorkPatrol = null;// 巡检任务

	/**
	 * 鉴权开锁（固定）
	 * 
	 * @author hupei
	 * @date 2015年7月28日 上午10:05:24
	 * @return
	 */
	public static TicketDetailFragment newInstance() {
		return newInstance(1, null);
	}

	/**
	 * 临时（计划） TemporaryListFragment 跳转
	 * 
	 * @author hupei
	 * @date 2015年5月29日 下午1:54:23
	 * @param dailyPatrol
	 * @return
	 */
	public static TicketDetailFragment newInstance(int type, TempTask temptask) {
		TicketDetailFragment fragment = new TicketDetailFragment();
		fragment.mType = type;
		fragment.mTemptask = temptask;
		return fragment;
	}

	/**
	 * 派工任务操作 跳转
	 * 
	 * @param type
	 * @param tasking
	 * @return
	 */
	public static TicketDetailFragment newInstanceWorkbill(int type,
			Workbill tasking) {
		TicketDetailFragment fragment = new TicketDetailFragment();
		fragment.mType = type;
		fragment.mWorkbill = tasking;
		return fragment;
	}
	
	
	/**
	 * 巡检任务操作 跳转
	 * 
	 * @param type
	 * @param tasking
	 * @return
	 */
	public static TicketDetailFragment newInstanceWorkPatrol(int type,
			WorkPatrol workPatrol) {
		TicketDetailFragment fragment = new TicketDetailFragment();
		fragment.mType = type;
		fragment.mWorkPatrol = workPatrol;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_ticket_execute,
				container, false);
		ViewUtils.inject(this, view);
		EkeyAPP.getInstance().setBtAddress("");
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setHasOptionsMenu(true);// 想让Fragment中的onCreateOptionsMenu生效必须先调用setHasOptionsMenu方法
		User user = EkeyAPP.getInstance().getUser();
		if (mType == 1)// 鉴权开锁（固定任务）
		{
			if (user.isOnline)
				mActivity.getBasicActionBar().setTitle("智能钥匙开锁");
			else {
				mActivity.getBasicActionBar().setTitle("离线鉴权开锁");
			}

		} else if (mType == 2) {
			mActivity.getBasicActionBar().setTitle("派工任务开锁");
		} else {
			mActivity.getBasicActionBar().setTitle("临时鉴权开锁");
		}
		toggleBtnExecute(EkeyAPP.getInstance().getBtAddress());
		setViewData();

	}

	// 填充数据
	private void setViewData() {
		// 这里为登录人的信息
		tvUserName.setText(EkeyAPP.getInstance().getUser().getName());
		tvMobile.setText(EkeyAPP.getInstance().getUser().getUsername());
		tvUnit.setText(EkeyAPP.getInstance().getUser().getSection());
	}

	@Override
	protected void onSelectBt(String address) {
		toggleBtnExecute(address);
	}

	private void toggleBtnExecute(String btAddress) {
		if (TextUtils.isEmpty(btAddress)) {
			btnExecute.setText("配对钥匙");
			btnExecute.setBackgroundResource(R.drawable.navi_history_hour_bg);
		} else {
			btnExecute.setText("执行");
			btnExecute.setBackgroundResource(R.drawable.navi_history_notes_bg);
		}
	}

	@OnClick(R.id.eicket_execute_btn)
	public void onClick(View v) {
		// 判断配对后的蓝牙地址，空则搜索配对，否则连接执行
		if (TextUtils.isEmpty(EkeyAPP.getInstance().getBtAddress())) {
			scanLeDevice(true);
		} else {
			if (!mBattMin) {// 判断电量不足不允许操作
				mActivity.finish();
				if (mTemptask != null)
					EmpowerActivity.startActivity(mActivity, mTemptask);
				else if (mWorkbill != null)
					EmpowerActivity.startActivityWorkbill(mActivity, mWorkbill);
				else if(mWorkPatrol!=null)
					EmpowerActivity.startActivityWorkPatrol(mActivity, mWorkPatrol);
				else
					EmpowerActivity.startActivity(mActivity);//在线鉴权智能钥匙操作
			} else {
				battStart();
			}
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.ticket_dail_patrol, menu);
		menu.findItem(R.id.menu_end).setVisible(mType == 7);// 临时（计划）
		// menu.findItem(R.id.menu_end).setVisible(mType == 0);// 临时（计划）
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.menu_end) {// 终结

			mActivity.showMsgBox(0, R.string.end_ticket_msg, false,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							// SoapParams params = new SoapParams();
							// params.put("ticketID", mTicketHead.ticketId);
							// params.put("state", 7);
							// soapAction = new EkeyJSONSoapAction(mActivity,
							// R.string.shared_wsdl_request,
							// Globals.UPDATE_PLAN_TICKET_STATE, params,
							// TicketDetailFragment.this);
							// soapAction.start();
						}
					}, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});
		} else if (item.getItemId() == android.R.id.home) {
			setHasOptionsMenu(false);
			onBackFragment();
		}
		// mActivity.finish();
		return super.onOptionsItemSelected(item);
	}

	private void onBackFragment() {
		if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
			mActivity.getSupportFragmentManager().popBackStack();
		else
			mActivity.finish();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mBtMsgService.disconnect();
		LogUtils.i("onDestroyView");
	}

	/**
	 * 接收BtMsgService发出的蓝牙状态<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午9:36:04
	 * @param state
	 */
	public void onEventMainThread(BtState state) {
		switch (state) {
		case DISCONNECTED:
			break;
		case CONNECTED:
			break;
		case CONNECTOUTTIME:// 连接超时
			mActivity.dismissDialog();
			showMsgBox("蓝牙连接超时,请重试!");
			break;
		case DISCOVERED:
			battStart();
			break;
		default:
			break;
		}
	}

	/**
	 * 接收BtMsgService发出的蓝牙电量<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午10:03:04
	 * @param btBatt
	 */
	public void onEventMainThread(BtBatt btBatt) {
		tvBatt.setText(btBatt.des);
		mBattMin = btBatt.isMin;
		if (mBattMin) {
			tvBatt.setTextColor(Color.RED);
			// mActivity.showMsgBox(R.string.batt_min_titile, R.string.batt_min,
			// new DialogInterface.OnClickListener() {
			//
			// @Override
			// public void onClick(DialogInterface dialog, int which) {
			// onBackFragment();
			// }
			// });
		} else {
			tvBatt.setTextColor(Color.BLACK);
		}
	}

	/**
	 * 接收BtMsgService发出的蓝牙自动连接<br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午11:24:45
	 * @param btConnAuto
	 */
	public void onEventMainThread(BtConnAuto btConnAuto) {
		if (btConnAuto.isConnFinish()) {
			removeCallbacksConn();
		}
	}

	/**
	 * 接收BtMsgService发出蓝牙数据超时 <br>
	 * {@linkplain EventBusManager#post(Object)}
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午11:55:44
	 * @param btTimeOut
	 */
	public void onEventMainThread(BtTimeOut btTimeOut) {
		// mActivity.showMsgBox(R.string.batt_min_msg1,
		// new DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// battStart();
		// }
		// });
	}

//	public void onEventMainThread(EventBusType.BtHex btHex) {
//		final String read = btHex.commd;
//		if (!TextUtils.isEmpty(read)) {
//
//			if (EkeyAPP.getInstance().isSCLock()) {
//				byte[] extraData = BtMsgConver.hexStrToByte(read);
//				SCReceiveMsg receiveMsg = new SCReceiveMsg(extraData);
//				if (receiveMsg.getRecvType() == 0x05) {
//					SCBatteryResult receiveResult = receiveMsg.parseBattery();
//					if (receiveResult.ismState()) {
//						mBattMin = receiveResult.getmKeyEnergy() < 5;
//					}
//				}
//			}
//		}
//		if (mBattMin) {
//			tvBatt.setTextColor(Color.RED);
//		} else {
//			tvBatt.setTextColor(Color.BLACK);
//		}
//	}

	/**
	 * 弹出提示框
	 */
	public void showMsgBox(String msg) {
		mActivity.showMsgBox("提示", msg, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

			}
		});
	}

}
