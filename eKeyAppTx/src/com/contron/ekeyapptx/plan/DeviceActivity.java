/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeyapptx.plan;

import android.app.Activity;
import android.app.ExpandableListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.DeviceAdapter;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.view.ClearEditText;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 临时鉴权申请-设备选择
 *
 * @author luoyilong
 *
 */
public class DeviceActivity extends ExpandableListActivity {
	private DeviceAdapter mAdapter;
	private boolean ischeck = true;// true 多选，false 单选
	ArrayList<Section> list;
	ArrayList<Section> tempList = new ArrayList<Section>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		EkeyAPP.getInstance().addActivity(this);
		// 设置标题
		getActionBar().setTitle(R.string.device_title);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowHomeEnabled(false);


		getExpandableListView().addHeaderView(getSearchView());
		getExpandableListView().setGroupIndicator(null);//设置系统默认展开图标为null

		// 获取传送过来的机构
		list = getIntent().getParcelableArrayListExtra(
				"stations");
		tempList.addAll(list);
		ischeck = getIntent().getBooleanExtra("ischeck", true);
		mAdapter = new DeviceAdapter(this, list, ischeck);
		setListAdapter(mAdapter);
	}

	private View getSearchView() {
		View searchView = LayoutInflater.from(this).inflate(R.layout.public_search_layout, null, false);
		final ClearEditText etSearch = (ClearEditText) searchView.findViewById(R.id.et_search);
//		etSearch.setDisableClear(true);
		etSearch.setHint("输入基站名");
		Button btnSearch = (Button) searchView.findViewById(R.id.btn_search);
		btnSearch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				searchSection(etSearch.getText().toString());
			}
		});
		return searchView;
	}

	private void searchSection(String searchText) {
		list.clear();
		list.addAll(tempList);
		if (!TextUtils.isEmpty(searchText)) {
			Iterator<Section> it = list.iterator();
			while(it.hasNext()){
				Section section = it.next();
				if(!section.getName().contains(searchText)){
					it.remove();
				}
			}
		}
		mAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		EkeyAPP.getInstance().removeActivity(this);
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
								int groupPosition, int childPosition, long id) {
		mAdapter.toggleChildSelect(v, groupPosition, childPosition);
		return true;
	}

	@Override
	public void onBackPressed() {
		onBack(null);// 返回
		super.onBackPressed();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.save, menu);
		menu.findItem(R.id.menu_save).setTitle("保存");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_save:
				onBack(mAdapter.getStations());// 返回
				break;
			case android.R.id.home:
				onBackPressed();
				break;
			default:
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	// 返回时设置返回参数
	private void onBack(ArrayList<Section> arrayList) {
		Intent data = getIntent();
		data.putParcelableArrayListExtra("stations", arrayList);
		setResult(RESULT_OK, data);
		finish();
	}

	public static void startActivityForResult(Fragment fragment,
											  int requestCode, ArrayList<Section> objects2) {
		Intent intent = new Intent(fragment.getActivity(), DeviceActivity.class);
		intent.putParcelableArrayListExtra("stations", objects2);
		fragment.startActivityForResult(intent, requestCode);
	}

	public static void startActivityForResult(Fragment fragment,
											  boolean ischeck, int requestCode, ArrayList<Section> objects2) {
		Intent intent = new Intent(fragment.getActivity(), DeviceActivity.class);
		intent.putParcelableArrayListExtra("stations", objects2);
		intent.putExtra("ischeck", ischeck);
		fragment.startActivityForResult(intent, requestCode);
	}

	public static void startActivityForResult(Activity activity,
											  boolean ischeck, int requestCode, ArrayList<Section> objects2) {
		Intent intent = new Intent(activity, DeviceActivity.class);
		intent.putParcelableArrayListExtra("stations", objects2);
		intent.putExtra("ischeck", ischeck);
		activity.startActivityForResult(intent, requestCode);
	}

}
