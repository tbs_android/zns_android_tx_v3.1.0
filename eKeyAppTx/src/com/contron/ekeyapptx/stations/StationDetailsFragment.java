package com.contron.ekeyapptx.stations;


import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.amap.api.maps.model.LatLng;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.BasicBtFragment;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.bluetooth.BtState;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvEmpowerMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.Company;
import com.contron.ekeypublic.entities.Controller;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.Station;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.Zone;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.map.CollectActivity;
import com.contron.ekeypublic.view.MsgBoxFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class StationDetailsFragment extends BasicBtFragment {

    private static final String ARG_PARAM = "DeviceObject";
    private Station mDevice;
    private BasicActivity mActivity;
    private ContronAdapter<Lockset> mLocksetAdapter;
    private ContronAdapter<Section> mSectionAdapter;
    private ContronAdapter<Company> mCompanyAdapter;
    private ContronAdapter<Zone> mZoneAdapter;
    private RequestHttp requestHttp;// 后台请求对象
    private User user;
    private Fragment mFragment;

    @ViewInject(R.id.layout_section)
    private LinearLayout layoutSection;

    @ViewInject(R.id.layout_zone)
    private LinearLayout layoutZone;

    @ViewInject(R.id.sp_station_section)
    private Spinner spStationSection;

    @ViewInject(R.id.sp_station_zone)
    private Spinner spStationZone;

    @ViewInject(R.id.et_station_name)
    private EditText etStationName;

    @ViewInject(R.id.sp_station_type)
    private Spinner spStationType;

    @ViewInject(R.id.sp_company)
    private Spinner spCompany;

    @ViewInject(R.id.et_station_location)
    private EditText etStationLocation;

    @ViewInject(R.id.bt_collect_latlng)
    private Button btCollectLatlng;

    @ViewInject(R.id.et_controller_id)
    private EditText etControllerId;

    @ViewInject(R.id.sp_controller_type)
    private Spinner spControllerType;

    @ViewInject(R.id.sp_work_mode)
    private Spinner spWorkMode;

    @ViewInject(R.id.et_memo)
    private EditText etMemo;

    @ViewInject(R.id.lv_public)
    private ListView lvLocksets;

    @ViewInject(R.id.btn_add_lockset)
    private Button btnAddLockset;

    @ViewInject(R.id.btn_photos)
    private Button btnPhotos;

    @ViewInject(R.id.bt_locker_delete)
    private Button btnDelLockset;

    private ContronAdapter<BluetoothDevice> btAdapter;
    private MsgBoxFragment btDeviceDialog;// 蓝牙搜索结果设备框

    private List<Lockset> mLocksets = new ArrayList<Lockset>();
    private List<Controller> mControllers = new ArrayList<Controller>();
    private boolean mModifyStation = false;
    private LatLng mLatlng;

    public StationDetailsFragment() {
        // Required empty public constructor
    }

    public static StationDetailsFragment newInstance(Station device) {
        StationDetailsFragment fragment = new StationDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM, device);
        fragment.setArguments(args);
        return fragment;
    }

    public void setPhotoPaths(List<String> photoPaths) {
        if (mDevice == null)
            return;
        mDevice.setFilePath(photoPaths);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBeginScan = false;
        if (getArguments() != null) {
            mDevice = getArguments().getParcelable(ARG_PARAM);
            if (mDevice != null) {
                mModifyStation = true;
                mLocksets = mDevice.getLocksets();
                mControllers = mDevice.getControllers();
            }
        }

        mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
        mFragment = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_station_details, container, false);
        ViewUtils.inject(this, view);
        if (mModifyStation) {
            layoutSection.setVisibility(View.GONE);
//            layoutZone.setVisibility(View.GONE);
        }
        if (mDevice == null) {
            btnPhotos.setVisibility(View.INVISIBLE);
        }
        etStationLocation.setFocusable(false);
        btCollectLatlng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CollectActivity.startActivityForResult(mFragment);
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item44_name));
        requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        // 初始化设备锁具的列表
        mLocksetAdapter = new ContronAdapter<Lockset>(mActivity, R.layout.list_lockset, mLocksets) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, Lockset item, final int position) {
                viewHolder.setText(R.id.tx_locker_flag, "锁具" + String.valueOf(position + 1));
                viewHolder.setText(R.id.et_locker_name, item.getName());
                viewHolder.setText(R.id.et_locker_rfid, item.getRfid());
                viewHolder.setText(R.id.et_locker_btaddr, item.getBtaddr());
                // 初始化下拉框内的默认值
                final Spinner spDoorType = viewHolder.findView(R.id.sp_door_type);
                initStringAdapter(spDoorType, item.getDoortype());
                getDataFromDict(spDoorType, "door_type");
                setSpinnerSelectionByValue(spDoorType, item.getDoortype());
                String doorType = "";
                if (spDoorType.getCount() > 0) {
                    doorType = (String) spDoorType.getItemAtPosition(0);
                }
                if (doorType != null && !"".equals(doorType)) {
                    ((ContronAdapter<String>) spDoorType.getAdapter()).addData("", 0);
                }

                final Spinner spDoorCode = viewHolder.findView(R.id.sp_door_code);
                initStringAdapter(spDoorCode, item.getDoorcode());
                getDataFromDict(spDoorCode, "doorcode");
                setSpinnerSelectionByValue(spDoorCode, item.getDoorcode());
                String doorCode = "";
                if (spDoorCode.getCount() > 0) {
                    doorCode = (String) spDoorCode.getItemAtPosition(0);
                }
                if (doorCode != null && !"".equals(doorCode)) {
                    ((ContronAdapter<String>) spDoorCode.getAdapter()).addData("", 0);
                }

                final Spinner spLockerType = viewHolder.findView(R.id.sp_locker_type);
                initStringAdapter(spLockerType, item.getType());
                getDataFromDict(spLockerType, "lock_type");
                setSpinnerSelectionByValue(spLockerType, item.getType());

                viewHolder.findView(R.id.bt_locker_delete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                getActivity());
                        builder.setTitle("提示"); // 设置对话框的标题
                        builder.setMessage("确定删除？"); // 设置要显示的内容
                        // 确定按钮
                        builder.setPositiveButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                        mLocksetAdapter.removeDatas(position);
                                    }

                                });
                        // 取消按钮
                        builder.setNegativeButton(
                                android.R.string.cancel,
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                    }
                                });
                        // 创建对话框
                        AlertDialog dialog = builder.create();
                        // 显示对话框
                        dialog.show();
                    }
                });

                final EditText etLockerName = viewHolder.findView(R.id.et_locker_name);
                etLockerName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) { // edRfid失去焦点
                            String name = etLockerName.getText().toString();
                            if (!TextUtils.isEmpty(name)
                                    && position < mLocksets.size()) {
                                mLocksets.get(position).setName(
                                        name.replace(" ", ""));
                            }
                        }
                    }
                });
                spDoorType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int poi, long id) {
                        String doorType = (String) spDoorType.getItemAtPosition(poi);
                        if (!TextUtils.isEmpty(doorType)
                                && position < mLocksets.size()) {
                            mLocksets.get(position).setDoortype(
                                    doorType.replace(" ", ""));

                            if (!"光交箱门".equals(doorType)) {
                                spDoorCode.setSelection(0);
                                spDoorCode.setEnabled(false);
                            } else {
                                spDoorCode.setEnabled(true);
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                spDoorCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int poi, long id) {
                        String doorCode = (String) spDoorCode.getItemAtPosition(poi);
                        if (!TextUtils.isEmpty(doorCode)
                                && position < mLocksets.size()) {
                            mLocksets.get(position).setDoorcode(
                                    doorCode.replace(" ", ""));
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                spLockerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int poi, long id) {
                        String lockerType = (String) spLockerType.getItemAtPosition(poi);
                        if (!TextUtils.isEmpty(lockerType)
                                && position < mLocksets.size()) {
                            mLocksets.get(position).setType(
                                    lockerType.replace(" ", ""));
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                final EditText etLockerRFID = viewHolder.findView(R.id.et_locker_rfid);
                etLockerRFID.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) { // edRfid失去焦点
                            String rfid = etLockerRFID.getText().toString()
                                    .toUpperCase();
                            if (!TextUtils.isEmpty(rfid)
                                    && position < mLocksets.size()) {
                                mLocksets.get(position).setRfid(
                                        rfid.replace(" ", ""));
                            }
                        }
                    }
                });
                final EditText etLockerBtAddr = viewHolder.findView(R.id.et_locker_btaddr);
                etLockerBtAddr.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) { // edRfid失去焦点
                            String btaddr = etLockerBtAddr.getText().toString()
                                    .toUpperCase();
                            if (!TextUtils.isEmpty(btaddr)
                                    && position < mLocksets.size()) {
                                mLocksets.get(position).setBtaddr(
                                        btaddr.replace(" ", ""));
                            }
                        }
                    }
                });
                viewHolder.findView(R.id.bt_scan_btaddr).setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {

                                btAdapter.setEmpty();// 清空适配器
                                if (mBtMsgService != null) {
                                    mBtMsgService.disconnect();
                                    if (!mBtMsgService.isConnected())
                                        mBtMsgService.removeCallbacksConn();
                                }
                                mBtMsgService.getBluetoothAdapter()
                                        .startLeScan(leScanCallback);
                                btDeviceDialog.show(
                                        mActivity.getFragmentManager(), "btt");
                                mLocksetAdapter.setSelectedPosition(position);
                            }

                        });
                viewHolder.findView(R.id.bt_collect_rfid).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mBtMsgService != null && !mBtMsgService.isConnected()) {
                            scanLeDevice(true);
                        } else {
                            mActivity.showToast("请直接使用钥匙采码");
                        }
                        mLocksetAdapter.setSelectedPosition(position);
                    }
                });
            }
        };
        mLocksetAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lvLocksets.setAdapter(mLocksetAdapter);
        btnAddLockset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Lockset newLockset = new Lockset();
                newLockset.setId(0);
                mLocksetAdapter.addData(newLockset);
            }
        });
        btnPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StationPhotoFragment fragment = StationPhotoFragment.newInstance();
                if (mDevice != null) {
                    fragment.setDatas(mDevice.getId(), mDevice.getFilePath());
                }
//                mActivity.repalce(fragment, true);
                mActivity.hideAdd(StationDetailsFragment.class, fragment);
            }
        });
        initAdapters();
        initSpinnerData();
        initFragmentData();
        initBluetoothDevice();
        initMsgBoxFragment();
    }



    /**
     * 初始化蓝牙钥匙适配器
     */
    private void initBluetoothDevice() {

        btAdapter = new ContronAdapter<BluetoothDevice>(mActivity,
                android.R.layout.simple_list_item_2,
                new ArrayList<BluetoothDevice>()) {

            @Override
            public void setViewValue(ContronViewHolder viewHolder,
                                     BluetoothDevice item, int position) {
                viewHolder.setText(android.R.id.text1, item.getName()).setText(
                        android.R.id.text2, item.getAddress());
            }
        };
    }

    /**
     * 初始化蓝牙钥匙弹出对话框
     */
    private void initMsgBoxFragment() {

        btDeviceDialog = new MsgBoxFragment();
        btDeviceDialog.setCancelable(false);
        btDeviceDialog
                .setNegativeOnClickListener(new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mBtMsgService.getBluetoothAdapter().stopLeScan(
                                leScanCallback);
                        btDeviceDialog.dismiss();
                    }
                });
        btDeviceDialog.setTitle(mActivity.getResources().getString(
                R.string.main_bluetooth_menu));
        btDeviceDialog.setAdapter(btAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String btaddr = btAdapter.getDatas().get(which)
                                .getAddress().replace(":", "");
                        int selectPosition = mLocksetAdapter.getSelectedPosition();
                        if (selectPosition == -1)
                            selectPosition = 0;

                        mLocksets.get(selectPosition).setBtaddr(
                                btaddr.toUpperCase());
                        mLocksetAdapter.notifyDataSetChanged();
                        mBtMsgService.getBluetoothAdapter().stopLeScan(
                                leScanCallback);
                        btDeviceDialog.dismiss();
                    }
                });
    }

    // 蓝牙搜索回调方法
    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi,
                             final byte[] scanRecord) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // 搜索蓝牙结果
                    String address = device.getAddress().replace(":", "");
                    // 屏蔽显示智能钥匙
                    Smartkey btKey = DBHelp.getInstance(mActivity).getEkeyDao()
                            .findFirstBtKey(address);

                    if (btKey == null && !btAdapter.getDatas().contains(device))
                        btAdapter.addData(device);

                }
            });
        }
    };

    /**
     * 蓝牙选择回调
     */
    protected void onSelectBt(String address) {
        // toggleBtnExecute(address);
        scanLeDevice(false);
    }

    /**
     * 接收BtMsgService发出的状态消息<br>
     * {@linkplain EventBusManager#post(Object)}
     *
     * @author hupei
     * @date 2015年9月22日 上午9:36:04
     * @param state
     */
    public void onEventMainThread(BtState state) {
        switch (state) {
            case CONNECTING:// 连接中
                // toggleBtnExecute(""); // 断开后选择钥匙
                break;
            case DISCONNECTED:// 连接断开
                mActivity.showToast("钥匙连接已断开");
                // toggleBtnExecute(""); // 断开后选择钥匙
                break;
            case CONNECTED:// 连接成功
                mActivity.showToast("钥匙已连接，请使用钥匙采码");
                // 断开后选择钥匙
                break;
            case CONNECTOUTTIME:// 连接超时
                mActivity.showMsgBox("蓝牙连接超时,请重新连接钥匙!");
                break;
            case DISCOVERED:
                break;
            default:
                break;
        }
    }

    /**
     * 接收BtMsgService发出的蓝牙规约数据<br>
     * {@linkplain EventBusManager#post(Object)}
     *
     * @author hupei
     * @date 2015年9月22日 上午10:25:18
     * @param btHex
     */
    public void onEventMainThread(EventBusType.BtHex btHex) {
        final String read = btHex.commd;
        if (!TextUtils.isEmpty(read)) {
            BtRecvMsg btRecvMsg = BtMsgTools.recv(read);
            if (btRecvMsg != null) {
                if (btRecvMsg instanceof BtRecvEmpowerMsg) {// 接收授权记录

                    int selectPosition = mLocksetAdapter.getSelectedPosition();
                    if (selectPosition == -1)
                        selectPosition = 0;

                    mLocksets.get(selectPosition).setRfid(
                            btRecvMsg.data.toUpperCase());

                    mLocksetAdapter.notifyDataSetChanged();

                } else {// 数据响应不匹配
                    mActivity.showMsgBox(R.string.shared_bt_error);
                }
            } else {// 数据解析失败
                mActivity.showMsgBox(R.string.shared_bt_error1);
            }
        } else {// 接收数据为空
            mActivity.showMsgBox(R.string.shared_bt_error2);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.submit, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackFragment();
                break;
            case R.id.menu_submit:
                submit();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * 初始化并绑定spinner的adapter
     */
    private void initAdapters() {
        // 初始化所属区域adapter
        initSectionAdapter();

        // 初始化代维单位adapter
        initCompanyAdapter();

        // 初始化开锁范围adapter
        initZoneAdapter();

        // 初始化站点类型adapter
        initStringAdapter(spStationType, mDevice == null ? "" : mDevice.getType());

        // 初始化控制器类型adapter
        initStringAdapter(spControllerType, mDevice == null ? "" : mDevice.getControl_tool_type());

        // 初始化工作模式adapter
        initStringAdapter(spWorkMode, mDevice == null ? "" : mDevice.getControl_work_mode());
    }

    private void initSectionAdapter() {
        mSectionAdapter = new ContronAdapter<Section>(mActivity,
                android.R.layout.simple_spinner_item, new ArrayList<Section>()) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, Section item, int position) {
                viewHolder.setText(android.R.id.text1, item.getName());
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                if (mDevice == null) {
                    Section section = null;
                    if (spStationSection.getCount() > 0) {
                        section = (Section) spStationSection.getItemAtPosition(0);
                    }
                    getCompany(section);
                    return;
                }
                int position = 0;
                String totalSection = mDevice.getSection().trim();
                int index = totalSection.lastIndexOf(" ");
                String currentSection = totalSection.substring(index+1);
                for (Section temp : this.getDatas()) {
                    if (currentSection.equals(temp.getName())) {
                        spStationSection.setSelection(position);
                        getCompany((Section) spStationSection.getSelectedItem());
                        break;
                    }
                    position++;
                }
            }
        };
        mSectionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStationSection.setAdapter(mSectionAdapter);
        spStationSection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Section section = mSectionAdapter.getItem(position);
                getCompany(section);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCompanyAdapter() {
        mCompanyAdapter = new ContronAdapter<Company>(mActivity,
                android.R.layout.simple_spinner_item, new ArrayList<Company>()) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, Company item, int position) {
                viewHolder.setText(android.R.id.text1, item.getName());
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                int position = 0;
                if (!mModifyStation) {
                    return;
                }
                int companyId = mDevice.getCompanyid();
                for (Company temp : this.getDatas()) {
                    if (temp.getId() == companyId) {
                        spCompany.setSelection(position);
                    }
                    position++;
                }
            }
        };
        mCompanyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCompany.setAdapter(mCompanyAdapter);
    }

    private void initZoneAdapter() {
        mZoneAdapter = new ContronAdapter<Zone>(mActivity,
                android.R.layout.simple_spinner_item, new ArrayList<Zone>()) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, Zone item, int position) {
                viewHolder.setText(android.R.id.text1, item.getName());
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                if (!mModifyStation) {
                    return;
                }
                int count = mDevice.getZones().size();
                if (count <= 0) {
                    return;
                }
                int position = 0;
                for (Zone temp : this.getDatas()) {
                    if (temp.getId() == mDevice.getZones().get(0).getId()) {
                        spStationZone.setSelection(position);
                        break;
                    }
                    position++;
                }
            }
        };
        mZoneAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStationZone.setAdapter(mZoneAdapter);
    }

    private void initStringAdapter(final Spinner spinner, final String value) {
        if (spinner == null) {
            return;
        }
        ContronAdapter<String> adapter = new ContronAdapter<String>(mActivity,
                android.R.layout.simple_spinner_item, new ArrayList<String>()) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, String item, int position) {
                viewHolder.setText(android.R.id.text1, item);
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                int position = 0;
                for (String temp : this.getDatas()) {
                    if (temp.equals(value)) {
                        spinner.setSelection(position);
                        break;
                    }
                    position++;
                }
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /**
     * 初始化Fragment的界面数据
     */
    private void initSpinnerData() {
        // 初始化站点类型数据
        getDataFromDict(spStationType, "obj_type");

        // 初始化控制类型数据
        getDataFromDict(spControllerType, "controller_type");

        // 初始化工作模式数据
        getDataFromDict(spWorkMode, "work_mode");

        // 初始化区域、代维单位和开锁范围数据
        getSection();
        getZone();
    }

    private void getDataFromDict(Spinner spinner, String type) {
        ContronAdapter<String> adapter = (ContronAdapter<String>) spinner.getAdapter();
        if (adapter.getCount() != 0) {
            return;
        }
        List<Dict> dicts = DBHelp.getInstance(mActivity).getEkeyDao()
                .getDicType(type);
        List<String> data = new ArrayList<String>();
        for (Dict dict : dicts) {
            data.add(dict.getValue());
        }
        adapter.addAllDatas(data);
    }

    private void getCompany(Section section) {
        if (section == null)
            return;
        mCompanyAdapter.clearDatas();
        List<Company> companies = section.getCompanyList();
        if (companies.size() != 0 && null != companies.get(0).getName()) {
            Company company = new Company();
            company.setName("");
            companies.add(0, company);
        }
        mCompanyAdapter.addAllDatas(companies);
        if (mDevice != null) {
            for (int i = 0; i < mCompanyAdapter.getCount(); i++) {
                if (mDevice.getCompanyid() == mCompanyAdapter.getItem(i).getId()) {
                    mCompanyAdapter.setSelectedPosition(i);
                    break;
                }
            }
        }
    }

    private void getSection() {
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());

        // 请求接口
        String url = String.format(Globals.GET_SECTION_AND_COMPANY, user.getUsername());

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.dismissDialog();
                        if (value.contains("success")) {
                            List<Section> listSection;
                            listSection = JsonTools.getSections("", value);

                            if (listSection != null
                                    && listSection.size() > 0) {
                                for (Section session : listSection) {
                                    if ("根区域".equals(session.getName())) {// 去掉根节点
                                        listSection.remove(session);
                                        break;
                                    }
                                }
                                mSectionAdapter.addAllDatas(listSection);
//                                setEmptyViewShown(false);
                            } else {
//                                setEmptyText("暂无数据！");
//                                setEmptyViewShown(true);
                            }
                        } else {
                            mActivity.showToast("获取区域列表失败");
                        }

                    }
                });

            }
        });
    }

    private void getZone() {
        List<Zone> zones = DBHelp.getInstance(mActivity).getEkeyDao().getZone();
        if (zones.size() != 0 && zones.get(0).getName() != null) {
            Zone zone = new Zone();
            zone.setName("");
            zones.add(0, zone);
        }
        mZoneAdapter.addAllDatas(zones);
    }

    private void initFragmentData() {
        if (mDevice == null) {
            return;
        }
        etStationName.setText(mDevice.getName());
        setSpinnerSelectionByValue(spStationType, mDevice.getType());
        setSpinnerSelectionByValue(spCompany, String.valueOf(mDevice.getCompanyid()));
        mLatlng = new LatLng(mDevice.getLat(), mDevice.getLng());
        String latlng = "经度：" + mLatlng.longitude + "\r\n纬度："
                + mLatlng.latitude;
        etStationLocation.setText(latlng);
        if (mControllers.size() == 1) {
            etControllerId.setText(mControllers.get(0).getCode());
            setSpinnerSelectionByValue(spControllerType, mControllers.get(0).getType());
            setSpinnerSelectionByValue(spWorkMode, mControllers.get(0).getWork_mode());
            etMemo.setText(mControllers.get(0).getRemark());
        }
//        mLocksetAdapter.addAllDatas(mLocksets);
        mLocksetAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CollectActivity.REQUESTCODE
                && resultCode == CollectActivity.RESULTCODE && data != null) {
            mLatlng = data.getParcelableExtra("latlng");
            if (mLatlng != null) {
                String latlng = "经度：" + mLatlng.longitude + "\r\n纬度："
                        + mLatlng.latitude;
                etStationLocation.setText(latlng);
            }
        }
    }

    /**
     * 设置spinner的默认选中
     *
     * @param spinner
     * @param value
     */
    private void setSpinnerSelectionByValue(Spinner spinner, String value) {

        if ("".equals(value)) {
            spinner.setSelection(0);
            return;
        }
        SpinnerAdapter adapter = spinner.getAdapter();
        int positionCount = adapter.getCount();
        for (int i = 0; i < positionCount; i++) {
            if (adapter.getItem(i) instanceof Zone) {
                Section section = (Section) adapter.getItem(i);
                if (section.getName().equals(value)) {
                    spinner.setSelection(i);
                    return;
                }
            } else if (adapter.getItem(i) instanceof String) {
                String string = (String) adapter.getItem(i);
                if (string.equals(value)) {
                    spinner.setSelection(i);
                    return;
                }
            }
        }
    }

    private void submit() {
        if (!mModifyStation) {
            mDevice = new Station();
        }
        if (!checkData()) {
            return;
        }
        saveStation(mModifyStation);
    }

    private void saveStation(boolean modifyStation) {
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框

        // 设置参数
        RequestParams params = new RequestParams(mActivity.getUser());
        params.putParams("name", mDevice.getName());
        params.putParams("area", mDevice.getArea());
        params.putParams("type", mDevice.getType());
        params.putParams("lng", mDevice.getLng());
        params.putParams("lat", mDevice.getLat());
        params.putParams("companyid", mDevice.getCompanyid());
        params.putParams("remark", mDevice.getRemark());
        Zone zone = (Zone) spStationZone.getSelectedItem();
        if (zone != null && !"".equals(zone.getName())) {
            String zids = "[" + String.valueOf(zone.getId()) + "]";
            params.putParams("zid", zids);
        }

        JSONArray locksetArray = new JSONArray();
        try {
            for (Lockset lockset : mLocksets) {
                JSONObject locksetParams = new JSONObject();
                if (lockset.getId() != 0) {
                    locksetParams.put("id", lockset.getId());
                }
                locksetParams.put("name", lockset.getName());
                locksetParams.put("doortype", lockset.getDoortype());
                locksetParams.put("doorcode", lockset.getDoorcode());
                locksetParams.put("type", lockset.getType());
                locksetParams.put("rfid", lockset.getRfid());
                locksetParams.put("btaddr", lockset.getBtaddr());
                locksetArray.put(locksetParams);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        params.putParams("lockset", locksetArray);

        JSONArray controllerArray = new JSONArray();
        try {
            for (Controller controller : mControllers) {
                JSONObject controllerParams = new JSONObject();
                controllerParams.put("code", controller.getCode());
                controllerParams.put("type", controller.getType());
                controllerParams.put("work_mode", controller.getWork_mode());
                controllerParams.put("remark", controller.getRemark());
                controllerArray.put(controllerParams);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        params.putParams("controller", controllerArray);


        // 请求接口
        String url;
        if (modifyStation) {
            url = String.format(Globals.UPDATE_STATION, mDevice.getId());
        } else {
            url = String.format(Globals.CREATE_STATION, mDevice.getSid());
        }

        requestHttp.doPost(url, params, new RequestHttp.HttpRequestCallBackString() {

            @Override
            public void resultValue(final String value) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.dismissDialog();
                        if (value.contains("success")) {
                            mActivity.showToast("保存站点成功");
                            onBackFragment();
                        } else {
                            String error = "";
                            JSONObject json = null;
                            try {
                                json = new JSONObject(value);
                                error = json.getString("error");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            mActivity.showToast("保存站点失败：" + error);
                        }

                    }
                });

            }
        });
    }


    private boolean checkData() {
        String stationName = etStationName.getText().toString();
        if (TextUtils.isEmpty(stationName)) {
            mActivity.showToast("站点名称不能为空！");
            return false;
        }
        String stationType = (String) spStationType.getSelectedItem();
        if (TextUtils.isEmpty(stationType)) {
            mActivity.showToast("站点类型不能为空！");
            return false;
        }
        Section section = (Section) spStationSection.getSelectedItem();
        if (section == null) {
            mActivity.showToast("所属单位不能为空！");
            return false;
        }
//        Zone zone = (Zone) spStationZone.getSelectedItem();
//        if (zone == null) {
//            mActivity.showToast("开锁范围不能为空！");
//            return false;
//        }
        String controllerName = etControllerId.getText().toString();
        if (TextUtils.isEmpty(controllerName)) {
            mActivity.showToast("控制器ID不能为空！");
            return false;
        }
        String controllerType = (String) spControllerType.getSelectedItem();
        if (TextUtils.isEmpty(controllerType)) {
            mActivity.showToast("控制器类型不能为空！");
            return false;
        }
        String controllerWorkMode = (String) spWorkMode.getSelectedItem();
        if (TextUtils.isEmpty(controllerWorkMode)) {
            mActivity.showToast("控制器工作模式不能为空！");
            return false;
        }
        mDevice.setName(stationName);
        mDevice.setType(stationType);
        mDevice.setArea(section.getName());
        mDevice.setSection(section.getName());
        mDevice.setSid(String.valueOf(section.getId()));
        Company company = (Company) spCompany.getSelectedItem();
        if (company != null && !"".equals(company.getName())) {
            mDevice.setCompanyid(company.getId());
        }
        if (mLatlng != null) {
            mDevice.setLat(mLatlng.latitude);
            mDevice.setLng(mLatlng.longitude);
        }

        Controller controller = new Controller();
        controller.setCode(etControllerId.getText().toString());
        controller.setType((String) spControllerType.getSelectedItem());
        controller.setWork_mode((String)spWorkMode.getSelectedItem());
        controller.setRemark(etMemo.getText().toString());
        mControllers.clear();
        mControllers.add(controller);
        return true;
    }

    /**
     * 返回前一个页面
     */
    private void onBackFragment() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            mActivity.getSupportFragmentManager().popBackStack();
        else
            mActivity.finish();
    }
}
