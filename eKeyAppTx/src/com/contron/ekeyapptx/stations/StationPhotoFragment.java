package com.contron.ekeyapptx.stations;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.PhotoBrowsePagerActivity;
import com.contron.ekeyapptx.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.util.AppUtils;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.util.FileUtil;
import com.contron.ekeypublic.util.ImageUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.bitmap.BitmapCommonUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.json.JSONArray;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StationPhotoFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final int REQUEST_CODE_DEVICE = 100;
    @ViewInject(R.id.photo_gridView)
    private GridView mPhotosView;

    private BasicActivity mActivity;
    private ContronAdapter<String> mAdapter;
    private ArrayList<String> photoList = new ArrayList<>();// 需要显示的图片的地址
    private List<String> tempPhotoList = new ArrayList<>();

    private String picSaveDir = "";// 文件路径
    private List<String> newPicThumbnailPaths = new ArrayList<>();// 新拍摄的照片的压缩后路径
    private String oriPicThumbnailPath = "";// 新拍摄的照片的原始路径

    private int mStationId;

    private BitmapUtils bitmapUtils;
    private RequestHttp requestHttp;// 后台请求对象
    private User user;

    public static StationPhotoFragment newInstance() {
        StationPhotoFragment fragment = new StationPhotoFragment();

        return fragment;
    }

    public void setDatas(int stationId, List<String> photoPaths) {
        mStationId = stationId;
        tempPhotoList = photoPaths;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
        requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        // 初始化图片处理对象
        bitmapUtils = new BitmapUtils(mActivity);
        bitmapUtils.configDefaultBitmapMaxSize(BitmapCommonUtils
                .getScreenSize(mActivity));
        bitmapUtils.configDefaultBitmapConfig(Bitmap.Config.RGB_565);

        picSaveDir = mActivity.getExternalCacheDir().getPath()
                + "/stationsPic/";// 文件路径
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_station_photo,
                container, false);
        ViewUtils.inject(this, view);
        initAdapters();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item46_name));
    }

    /**
     * 向服务器提交数据，成功后返回
     */
    private void submit() {
        if (user == null)
            return;
        if (requestHttp == null)
            requestHttp = new RequestHttp(mActivity.getWld());// 服务请求
        mActivity.startDialog(R.string.progressDialog_title_load);// 弹出对话框
        String url = String.format(Globals.UPLOAD_STATION_PHOTO, mStationId);
        requestHttp.uploadImage(url, newPicThumbnailPaths, "objectimg", new RequestHttp.UploadCallback() {
            @Override
            public void onUploadResult(final boolean isOk) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.dismissDialog();
                        mActivity.showToast(isOk ? "上传成功" : "上传失败");
                        if (isOk) {
                            deletePhotos();
                            onBackFragment();
                        }
                    }
                });

            }

            @Override
            public void onUploadResultData(JSONArray uploadResultData) {
                Gson gson = new Gson();
                List<String> paths = gson.fromJson(uploadResultData.toString(),
                        new TypeToken<List<String>>(){
                        }.getType());
                tempPhotoList.addAll(paths);
            }
        });
    }

    private void initAdapters() {
        for (String photopath : tempPhotoList) {
            String path = mActivity.getWld() + photopath;
            photoList.add(path);
        }
        mAdapter = new ContronAdapter<String>(mActivity, R.layout.image_item, photoList) {
            @Override
            public void setViewValue(ContronViewHolder viewHolder, String item, int position) {
                ImageView imageView = viewHolder.findView(R.id.imageView_photo);
                String thumbnailPath = item;
                Glide.with(mActivity).load(thumbnailPath).placeholder(R.drawable.photo_report_un).into(imageView);
            }
        };
        mPhotosView.setAdapter(mAdapter);
        adjustGridView();
        mPhotosView.setOnItemClickListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.photo_and_save, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackFragment();
                break;
            case R.id.menu_photo:
                oriPicThumbnailPath = startCamera(REQUEST_CODE_DEVICE);
                break;
            case R.id.menu_save:
                submit();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private String startCamera(int requestCode) {
        // 判断存储卡是否可以用，可用进行存储
        if (!AppUtils.sdCardIsExsit()) {
            mActivity.showToast("无SD卡");
            return "";
        }
        // 存放照片的文件夹
        File savedir = new File(picSaveDir);
        if (!savedir.exists()) {
            savedir.mkdirs();
        }
        String timeStamp = DateUtil.getCurrentDate("yyyyMMddHHmmss");
        String t_photoName = timeStamp + ".jpg";// 照片名称
        String largePath = picSaveDir + t_photoName;// 该照片的绝对路径
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri uri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            String authorities = mActivity.getPackageName() + ".fileProvider";
            uri= FileProvider.getUriForFile(mActivity, authorities, new File(largePath));
        } else {
            uri = Uri.fromFile(new File(largePath));
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, requestCode);
        return largePath;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != REQUEST_CODE_DEVICE) {
            return;
        } else {
            if (resultCode != Activity.RESULT_CANCELED) {
                newPicThumbnailPaths.add(createThumbnailPic(oriPicThumbnailPath));
            } else {
                // 删除图片
                FileUtil.deleteFileWithPath(oriPicThumbnailPath);
            }
        }
    }

    /**
     * 创建缩略图，并显示在 ImageView
     *
     * @author hupei
     * @date 2015年5月28日 上午10:22:22
     */
    private String createThumbnailPic(String largePath) {
        String thumbnailPath = picSaveDir + "small_" + FileUtil.getFileName(largePath);
        File file = new File(thumbnailPath);
        // 判断是否已存在缩略图
        if (!file.exists()) {
            try {
                // 压缩生成上传的1920宽度图片
                ImageUtil.createImageThumbnail(mActivity, largePath,
                        thumbnailPath, 1920, 80);
                // 删除原图
                FileUtil.deleteFileWithPath(largePath);
                displayPhoto(thumbnailPath);
                return thumbnailPath;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";

    }

    /**
     * 把圖片显示到UI上
     *
     * @param thumbnailPath
     *            文件路径
     */
    private void displayPhoto(final String thumbnailPath) {
        if (TextUtils.isEmpty(thumbnailPath))
            return;

        mAdapter.addData(thumbnailPath);
    }

    /**
     * 填充适配器
     *
     */
    private void adjustGridView() {
        mPhotosView.setNumColumns(3);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PhotoBrowsePagerActivity.gotoActivity(mActivity,
                photoList, position, "照片浏览");
    }

    private void deletePhotos() {
        File dirFile = new File(picSaveDir);
        if (!dirFile.exists()) {
            return;
        }
        dirFile.delete();
    }

    /**
     * 返回前一个页面
     */
    private void onBackFragment() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0) {
            StationDetailsFragment fragment = (StationDetailsFragment) mActivity.getSupportFragmentManager().
                    findFragmentByTag(StationDetailsFragment.class.getSimpleName());
            if (fragment != null) {
                fragment.setPhotoPaths(tempPhotoList);
            }
            mActivity.getSupportFragmentManager().popBackStack();
        }
        else
            mActivity.finish();
    }
}
