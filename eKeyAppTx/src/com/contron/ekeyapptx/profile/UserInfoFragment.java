package com.contron.ekeyapptx.profile;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.contron.ekeyapptx.BasicActivity;
import com.contron.ekeyapptx.EkeyAPP;
import com.contron.ekeyapptx.R;
import com.contron.ekeyapptx.main.TicketActivity;
import com.contron.ekeyapptx.user.ModifyPwdFragment;
import com.contron.ekeypublic.entities.User;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class UserInfoFragment extends Fragment {
    @ViewInject(R.id.tx_name)
    private TextView txName;

    @ViewInject(R.id.tx_account)
    private TextView txAccount;

    @ViewInject(R.id.tx_section)
    private TextView txSection;

    @ViewInject(R.id.tx_last_login)
    private TextView txLastLogin;

    @ViewInject(R.id.modify_pwd)
    private Layout mModifyPwdLayout;

    private BasicActivity mActivity;
    private User user = null;

    /**
     * 获取实例
     *
     * @return
     */
    public static UserInfoFragment newInstance() {
        UserInfoFragment fragment = new UserInfoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = (BasicActivity) getActivity();
        user = mActivity.getUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_info, container, false);
        ViewUtils.inject(this, view);

        txName.setText(user.getName());
        txAccount.setText(user.getUsername());
        txSection.setText(user.getSection());
        txLastLogin.setText(user.getLastlogin_at() + "");
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity.getBasicActionBar().setTitle(
                getString(R.string.main_tab_item35_name));
        setHasOptionsMenu(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.getBasicActionBar().setTitle(R.string.main_tab_item35_name);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.add(0, 0, 0, "注销");
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.getItem(0);
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackFragment();
                break;
            case 0: // 注销登录
                EkeyAPP.getInstance().exitRecLen();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.modify_pwd)
    private void onClick(View view) {
        mActivity.hideAdd(UserInfoFragment.class, ModifyPwdFragment.newInstance());
    }

    /**
     * 返回前一个页面
     */
    private void onBackFragment() {
        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() > 0)
            mActivity.getSupportFragmentManager().popBackStack();
        else
            mActivity.finish();
    }
}
