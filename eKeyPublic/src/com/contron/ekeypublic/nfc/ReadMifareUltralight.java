package com.contron.ekeypublic.nfc;

import java.io.IOException;
import java.nio.charset.Charset;

import android.nfc.Tag;
import android.nfc.tech.MifareUltralight;

import com.lidroid.xutils.util.LogUtils;

/**
 * 读取MifareUltralight类型的标签
 * @author luoyilong
 *
 */
public class ReadMifareUltralight {

	
	// 读取MifareUltralight类型的标签
	public static String readTag(Tag tag) {

		MifareUltralight mifare = MifareUltralight.get(tag);
		try {
			mifare.connect();
			byte[] payload1 = mifare.readPages(4);
			LogUtils.e(bytesToHexString(payload1));
			return bytesToHexString(payload1);			
//			return new String(payload1, Charset.forName("US-ASCII"));
			
		} catch (IOException e) {
			System.out
					.println("IOException while writing MifareUltralight message...");
			return "读取失败！";
		} catch (Exception e) {
			System.out.println("Exception message...");
			return "读取失败！";
		} finally {
			if (mifare != null) {
				try {
					mifare.close();
				} catch (Exception e) {
					System.out.println("Error closing tag...");
				}
			}
		}
	}

	// 字符序列转换为16进制字符串
	private static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder("0x");
		if (src == null || src.length <= 0) {
			return null;
		}
		char[] buffer = new char[2];
		for (int i = 0; i < src.length; i++) {
			buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
			buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
			// System.out.println(buffer);
			stringBuilder.append(buffer);
		}
		return stringBuilder.toString();
	}
}
