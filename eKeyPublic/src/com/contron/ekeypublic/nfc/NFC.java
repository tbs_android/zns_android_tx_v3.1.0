package com.contron.ekeypublic.nfc;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NfcAdapter;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.widget.Toast;

/***
 * NFC封装类
 * 
 * @author luoyilong
 * @说明：1、在Activity中onResume()方法中调用onStart()启动前台调度
 * @2、在Activity中onPause()方法中调用onColse()禁用前台调度
 * @3、在Activity中onNewIntent()方法中使用扫描标签Tag
 */
public class NFC {

	private Activity mActivity;

	/**
	 * Nfc适配器
	 */
	private NfcAdapter mNfcAdapter;

	/**
	 * 标签解析技术条件类 例 new String[] {NfcF.class.getName()}
	 */
	private static String[][] manalysisCondition;

	public NFC(Activity activity) {
		this.mActivity = activity;
		this.mNfcAdapter = getNfcAdapter();
		this.manalysisCondition = new String[][] {
				new String[] { MifareClassic.class.getName() },
				new String[] { Ndef.class.getName() },
				new String[] { MifareUltralight.class.getName() } };

	}

	/***
	 * 获取NFC适配器
	 * 
	 * @author luoyilong
	 * @date 2015年10月20日 下午2:15:01
	 * @param context
	 * @return
	 */
	private NfcAdapter getNfcAdapter() {
		NfcAdapter adapter = NfcAdapter.getDefaultAdapter(mActivity);
		return adapter;

	}

	/***
	 * Intent封装
	 * 
	 * @author luoyilong
	 * @date 2015年10月20日 下午1:34:21
	 * @param context
	 * @return
	 */
	private PendingIntent getPendingIntent() {
		PendingIntent pendingIntent = PendingIntent.getActivity(mActivity, 0,
				new Intent(mActivity, mActivity.getClass())
						.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		return pendingIntent;
	}

	/***
	 * Intent 过滤TECH格式
	 * 
	 * @author luoyilong
	 * @date 2015年10月20日 下午1:33:10
	 * @param filter
	 * @param action
	 * @return
	 */
	private IntentFilter getTECHIntentFilter() {
		IntentFilter tech = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
		return tech;
	}

	/***
	 * Intent 过滤NDEF格式
	 * 
	 * @author luoyilong
	 * @date 2015年10月20日 下午1:33:10
	 * @param filter
	 * @param action
	 * @return
	 */
	private IntentFilter getNDEFIntentFilter() {
		IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
		try {
			ndef.addDataType("*/*");
		} catch (MalformedMimeTypeException e) {
			e.printStackTrace();
		}

		return ndef;
	}

	/**
	 * Intent过滤条件组
	 * 
	 * @author luoyilong
	 * @date 2015年10月20日 下午3:51:24
	 * @param type
	 *            标签类型 0：NDEF,1:TECH
	 * @return
	 */
	public IntentFilter[] getIntentFilterList(int type) {
		IntentFilter[] intentFiltersArray = new IntentFilter[1];
		IntentFilter intentFilter = null;
		if (type == 0)
			intentFilter = getNDEFIntentFilter();
		else if (type == 1)
			intentFilter = getTECHIntentFilter();

		intentFiltersArray[0] = intentFilter;

		return intentFiltersArray;
	}

	/**
	 * 启动前台调度
	 * 
	 * @author luoyilong
	 * @date 2015年10月20日 下午3:55:12
	 * @param type
	 *            标签类型过滤 0：NDEF,1:TECH
	 */
	public void onStart(int type) {

		if (isNfcAdapter()==1)
			mNfcAdapter.enableForegroundDispatch(mActivity, getPendingIntent(),
					getIntentFilterList(type), manalysisCondition);
	}

	/**
	 * 禁用前台调度
	 * 
	 * @author luoyilong
	 * @date 2015年10月20日 上午11:51:31
	 */
	public void onColse() {
		if (mNfcAdapter != null)
			mNfcAdapter.disableForegroundDispatch(mActivity);
	}

	/***
	 * 判断设备中NFC模块是否可用
	 * 
	 * @author luoyilong
	 * @date 2015年10月15日 上午9:07:11
	 * @return 0表示：当前设备不支持NFC,1表示：NFC功能正常，2表示： NFC功能未启用
	 */
	public int isNfcAdapter() {
		if (mNfcAdapter == null) {
			return 0;
			// Toast.makeText(mActivity, "当前设备不支持NFC读取钥匙蓝牙地址！", 3000).show();
		} else if (!mNfcAdapter.isEnabled()) {
			return 2;
			// Toast.makeText(mActivity, "NFC功能未启用,如需使用可在系统设置中启用NFC功能！",
			// 6000).show();
		} else {
			return 1;
			// Toast.makeText(mActivity, "NFC功能正常，可把钥匙背面与与手机背面贴合读取钥匙蓝牙地址！",
			// 6000)
			// .show();
		}

	}

}
