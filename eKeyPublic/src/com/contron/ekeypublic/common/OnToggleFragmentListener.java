package com.contron.ekeypublic.common;

import android.support.v4.app.Fragment;

public interface OnToggleFragmentListener {

	/**
	 * 隐藏添加
	 * 
	 * @author hupei
	 * @date 2015年5月13日 上午11:45:38
	 * @param hideFragmentClass
	 * @param addFragment
	 */
	public void hideAdd(Class<? extends Fragment> hideFragmentClass, Fragment addFragment);

	/**
	 * 替换
	 * 
	 * @author hupei
	 * @date 2015年5月25日 下午3:47:43
	 * @param fragment
	 */
	public void repalce(Fragment fragment, boolean isToBackStack);
}
