/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.common;

import java.io.File;
import android.os.Environment;

public class Globals {
	/** 程序版本地址 */
	public static final String VERSION_URL = "http://%1$s/userdata/ekeyMin/appFile/appVersion.xml";
	public static final String POST_POST_ADDRESS = "http://%1$s/ekeyMin";
	public static final String POST_TICKET_PLAN_APPLY = "/ticketPlanApply";
	public static final String KEY_VERSION_ADDRESS = "key_version_address";
	public static final String KEY_WSDL_ADDRESS = "key_wsdl_address";
	public static final String KEY_RECLEN_TIME = "key_reclen_time";
	public static final String DATABASE_NAME = "ekeytxmin.db";

	public static final String KEY_POST = "key_post";
	public static final String SOCKET_POST = "Socket_post";
	public static final String KEY_SOCKET_ADDRESS = "key_socket_address";
	public static final String KEY_IP_ADDRESS = "key_ip_address";
	public static final String KEY_UNLOCK_TYPE = "key_unlock_type";

	public static final String UNLOCK_BLUETOOTH = "unlock_bluetooth";
	public static final String UNLOCK_OTHER = "unlock_other";

	public static final String ATTACH_KEY = "?s=1&c=android&v=1";
	/** SD卡根目录 */
	public static final String EKEY_MIN_DIR_ROOT = Environment
			.getExternalStorageDirectory() + File.separator + "cyg/ekeyMin";
	/** 数据库目录 */
	public static final String DB_DIR = EKEY_MIN_DIR_ROOT + "/database";
	public static final String DOWNLOAD_DIR = EKEY_MIN_DIR_ROOT + "/download";

	public static final String KEY_MOBILE = "key_mobile";// 保存手机号

	public static final String KEY_PASS = "key_pass";// 保存密码

	// ---------------------------------以下为后台服务器
	/** URL */
	public static final String URL = "http://%1$s/ekeyMin/services/estTicketService";
	/** 命名空间 */
	public static final String NAME_SPACE = "http://ekeymin.contron.com/";

	/** 获取所有的蓝牙锁 */
	public static final String GET_ALL_DOORLOCKER = "/role/1/zone" + ATTACH_KEY;

	/** 下载临时任务 **/
	public static final String DOWNLOAD_TEMP_TASK = "/temp_task/verify" + ATTACH_KEY;

	/** 修改临时任务状态 **/
	public static final String UPDATESTATUS_TEMP_TAKS = "/temp_task/%1$s/updatestatus" + ATTACH_KEY;

	/** 用户注册 */
	public static final String REGISTER_USER = "/user/reg" + ATTACH_KEY;

	/** 下载日常巡视任务 */
	public static final String GET_FIXED_TICKET = "";
	/** 下载临时任务 */
	public static final String DOWNLOAD_PLAN_TICKET = "";
	/** 上传操作结果 */
	public static final String UPLOAD_TICKET_RESULT = "";
	/** 初始化数据 */
	public static final String INIT_DATA = "";
	/** 改变计划票的状态 */
	public static final String UPDATE_PLAN_TICKET_STATE = "";
	/** 下载审批票 */
	public static final String GET_AUDIT_TICKET = "";
	/** 更新审批票状态 */
	public static final String UPDATE_AUDIT_TICKET_STATE = "";
	/** 修改密码 */
	public static final String MODIFY_PWD = "";

	/** 列出所有用户 */
	public static final String USER_ALL = "/user/" + ATTACH_KEY;
	/** 审批用户注册 */
	public static final String UPDATE_REGISTER_USER = "";

	/** 下载待审核注册用户 */
	public static final String GET_REGISTER_USER = "";
	/** 下载采码票 */
	public static final String GET_COLLECT_TICKET = "";
	/** 上传锁码 */
	public static final String UPDATE_LOCKER = "";
	/** 鉴权开锁离线延时申请 */
	public static final String OFFLINE_FIXEDTASKDELAY_APPLY = "";
	/** 鉴权开锁离线延时申请最后一次审批通过的数据 */
	public static final String OFFLINE_FIXEDTASKDELAY_REPLY = "";
	/** 下载待审批的离线鉴权延时申请 */
	public static final String GET_OFFLINE_FXIEDTASKDELAY_APPLY = "";
	/** 下载自己最近审批过的离线鉴权延时记录 */
	public static final String GET_OFFLINE_FIXEDTASKDELAY_REPLY = "";
	/** 更新离线鉴权延时申请状态 */
	public static final String UPDATE_OFFLINE_FIXEDTASKDELAY_APPLY_STATE = "";
	/** 同步数据 */
	public static final String SYNC_DATA = "";
	/** 通过手机号码获取用户名称 */
	public static final String GET_USER_BY_MOBILE = "";
	/** 上传临时授权任务 */
	public static final String POST_UPLOAD_PLAN_TICKET = "";
	/** 上传借用钥匙申请 */
	public static final String UPLOAD_LEND_KEY = "";
	/** 下载借用钥匙记录 */
	public static final String DOWNLOAD_LEND_KEY = "";
	/** 上传借用钥匙申请 */
	public static final String UPLOAD_BORROW_KEY = "";
	/** 下载借用钥匙记录 */
	public static final String DOWNLOAD_BORROW_KEY = "";
	/** 上传借用人归还钥匙确认记录 */
	public static final String UPLOAD_BORROWER_RETURN_KEY = "";
	/** 上传借出人归还钥匙确认记录 */
	public static final String UPLOAD_LENDER_RETURN_KEY = "";

	/**
	 * ------------------------------------------------------------------------
	 * --
	 */

	/** 登录 请求Url */
	public static final String MOBILE_LOGIN = "/user/login" + ATTACH_KEY;

	/** 查询待审批临时任务请求Url */
	public static final String TEMP_TASK_VERIFY = "/temp_task/verify/" + ATTACH_KEY;

	/** 单个 临时鉴权任务审批请求Url */
	public static final String TEMP_TASK_TID_VERIFY = "/temp_task/%1$s/verify" + ATTACH_KEY;

	/** 批量审批临时鉴权任务请求Url */
	public static final String TEMP_TASK_VERIFY_ALL = "/temp_task/verifyall/" + ATTACH_KEY;

	/** 蓝牙锁开门日志上传请求Url */
	public static final String OPERATION_DO = "/operation/do" + ATTACH_KEY;

	/** 蓝牙锁开门上传报警记录请求Url */
	public static final String UPDATA_ALERT = "/alert/msg" + ATTACH_KEY;

	/** 离线鉴权延时申请请求Url */
	public static final String OFFLINE_APPLY = "/offline/apply" + ATTACH_KEY;

	/** 离线鉴权请求Url */
	public static final String OFFLINE = "/offline/" + ATTACH_KEY;

	// /** 查找某个用户已审核离线鉴权请求Url */
	// public static final String USER_NAME_OFFLINE =
	// "/user/%1$s/offline/" + ATTACH_KEY;

	/** 查找某个用户已审核离线鉴权请求Url */
	public static final String USER_NAME_VERIFIED_OFFLINE = "/user/%1$s/verified/offline/" + ATTACH_KEY;

	/** 审批离线鉴权请求Url */
	public static final String OFFLINE_OID_VERIFY = "/offline/%1$s/verify" + ATTACH_KEY;

	/** 日志查询请求Url */
	public static final String HISTORY = "/history/" + ATTACH_KEY;
	
	/** 短信查询请求Url */
	public static final String SMS = "/sms/get/" + ATTACH_KEY;

	/** 申请临时鉴权任务请求Url */
	public static final String TEMP_TASK = "/temp_task/apply" + ATTACH_KEY;

	/** 临时鉴权开锁请求Url */
	public static final String USER_NAME_TEMP_TASK = "/user/%1$s/temp_task/" + ATTACH_KEY;

	/** 列出所有机构 */
	public static final String SECTION_ALL = "/section/" + ATTACH_KEY;
	
	/** 列出所有机构 */
	public static final String SECTION_EX_ALL = "/section/all" + ATTACH_KEY;

	/** 检查用户是否注册 */
	public static final String HAS_REGISTER_USER = "/user/checkusername_post" + ATTACH_KEY;

	/** 获取智能钥匙 */
	public static final String SECTION_SID_SMARTKEY = "/section/%1$s/smartkey/" + ATTACH_KEY;

	/** 获取用户权限内锁 */
	public static final String USER_NAME_OBJECT = "/user/%1$s/object/" + ATTACH_KEY;

	/** 获取所有锁 */
	public static final String OBJECT = "/object/" + ATTACH_KEY;

	/** 获取授权范围 */
	public static final String USER_NAME_ZONE = "/user/%1$s/zone/" + ATTACH_KEY;

	/** 获取用户角色 */
	public static final String ROLE = "/role/" + ATTACH_KEY;

	/** 获取所有用户 */
	public static final String All_USER = "/user/" + ATTACH_KEY;

	/** 下载最后一条批准的 离线鉴权 */
	public static final String USER_NAME_LAST_OFFLINE = "/user/%1$s/last_offline/" + ATTACH_KEY;

	/** 获取用户已审批的 离线鉴权 */
	public static final String USER_NAME_OFFLINE = "/user/%1$s/offline/" + ATTACH_KEY;

	/** 提交离线鉴权的执行时间 */
	public static final String OFFLINE_ID_ACTION_AT_DATE = "/offline/%1$s/action_at/%2$s" + ATTACH_KEY;

	/** 上传离线智能钥匙操作记录 */
	public static final String HISTORY_OFFLINE = "/history/offline/new" + ATTACH_KEY;

	/** 上传离线开门操作记录 */
	public static final String OPERATION_OFFLINE = "/operation/offline/do" + ATTACH_KEY;

	/** 智能钥匙开锁上传操作记录请求Url */
	public static final String UPDATA_LOADRESULT = "/history/new" + ATTACH_KEY;

	/** 下载基础库中的锁类型请求Url */
	public static final String DICT_LOCK_TYPE = "/dict/lock_type/" + ATTACH_KEY;

	/** 下载基础库中的设备类型请求Url */
	public static final String DICT_OBJ_TYPE = "/dict/obj_type/" + ATTACH_KEY;

	/** 添加设备锁信息请求Url */
	public static final String SECTION_SID_OBJECT_NEW = "/section/%1$s/object/new/lockset/" + ATTACH_KEY;
	
	/** 删除设备锁具请求Url */
	public static final String LOCKSET_OID_DEL = "/lockset/%1$s/del/" + ATTACH_KEY;
	
	/** 新增设备锁具请求Url */
	public static final String LOCKSET_OID_NEW = "/object/%1$s/lockset/new/" + ATTACH_KEY;
	
	/** 修改设备锁具请求Url */
	public static final String LOCKSET_OID_MODIFY = "/lockset/%1$s/update/" + ATTACH_KEY;

	/** 搜索设备锁具请求Url */
	public static final String LOCKSET_OID_SEARCH = "/lockset/%1$s/search/" + ATTACH_KEY;
	
	/** 修改设备信息请求Url */
	public static final String OBJECT_OID_UPDATE = "/object/%1$d/update" + ATTACH_KEY;

	/** 添加设备坐标请求Url */
	public static final String OBJECT_OID_LOCATION = "/object/%1$d/location" + ATTACH_KEY;

	/** 创建临时权限任务请求Url */
	public static final String USER_USERNAME_TEMP_TASK_NEW = "/user/%1$s/temp_task/new" + ATTACH_KEY;

	/** 新用户审批请求Url */
	public static final String USER_NAME_VERIFY = "/user/%1$s/verify" + ATTACH_KEY;

	/** 下载用户信息请求Url */
	public static final String USER = "/user/" + ATTACH_KEY;

	/** 修改密码请求Url */
	public static final String USER_NAME_PASSWORD = "/user/%1$s/password" + ATTACH_KEY;

	/** 获取当前组织的开锁范围请求Url */
	public static final String ZONE_SID = "/zone/%1$s/" + ATTACH_KEY;

	/** 向授权范围添加设备请求Url */
	public static final String ZONE_ZID_ADD_OBJECT = "/zone/%1$s/object/add/%2$s" + ATTACH_KEY;

	/** 列出某类型的常量请求Url */
	public static final String DICT_TYPE = "/dict/%1$s/" + ATTACH_KEY;

	/** 列出所有类型的常量请求Url */
	public static final String DICT = "/dict/" + ATTACH_KEY;

	/** 借得智能钥匙请求Url */
	public static final String SMARTKEY_BORROW_KID = "/smartkey/borrow/%1$d" + ATTACH_KEY;

	/** 归还钥匙请求Url */
	public static final String SMARTKEY_RETURN_KID = "/smartkey/return/%1$d" + ATTACH_KEY;

	/** 钥匙借出归还记录请求Url */
	public static final String SMARTKEY_BORROW = "/smartkey/borrow/" + ATTACH_KEY;

	/** 初始化E2钥匙请求锁信息 */
	public static final String SECTION_DEPARTMENTID_OBJECT = "/section/%1$s/object/" + ATTACH_KEY;

	/** 预置任务下载请求锁信息 */
	public static final String PRESET_TASK_USERID_GET = "/preset/task/execute/%1$s/get" + ATTACH_KEY;
	
	/** 预置任务下载请求锁信息 */
	public static final String FIXATION_TASK_USERID_GET = "/fixation/task/execute/%1$s/get" + ATTACH_KEY;

	/** 预置任务记录上传请求信息 */
	public static final String HISTORY_PRESET_TASK_NEW = "/history/task/log/new" + ATTACH_KEY;

	/** 根据时间更新基础数据 */
	public static final String DICT_AUTO_UPDATE_DICT = "/dict/auto/updatetime" + ATTACH_KEY;

	/** 采码票回传 */
	public static final String LOCKSET_UPDATE_RFID = "/lockset/updateRFID" + ATTACH_KEY;

	/** 获取派工单 */
	public static final String WORKBILL_GET = "/workbill/" + ATTACH_KEY;

	/** 新增派工单 */
	public static final String WORKBILL_NEW = "/workbill/new" + ATTACH_KEY;

	/** 更新派工单 */
	public static final String WORKBILL_UPDATE = "/workbill/%1$d/update" + ATTACH_KEY;

	/** 接单 **/
	public static final String WORKBILL_ACCEPT = "/workbill/%1$s/accept" + ATTACH_KEY;

	/** 回单 **/
	public static final String WORKBILL_RETURN = "/workbill/%1$s/return" + ATTACH_KEY;

	/** 进站及时率 */
	public static final String WORKBILL_INTIME = "/workbill/intime" + ATTACH_KEY;

	/** 申请工单 */
	public static final String WORKBILL_APPLY = "/workbill/apply" + ATTACH_KEY;

	/** 审批工单 */
	public static final String WORKBILL_VERIFY = "/workbill/%1$s/verify" + ATTACH_KEY;

	/** 进站及时率 */
	public static final String WORKBILL_INTIME_DETAIL = "/workbill/intime/detail" + ATTACH_KEY;

	/** 新增巡检 */
	public static final String WORKPATROL_NEW = "/workpatrol/new" + ATTACH_KEY;

	/** 获取巡检 */
	public static final String WORKPATROL_GET = "/workpatrol/" + ATTACH_KEY;

	/** 获取运维设备 */
	public static final String WORKPATROL_SECTION_GET = "/section/0/agent/object/downward/" + ATTACH_KEY;

	/** 获取巡检率 */
	public static final String WORKPATROL_FINISH = "/workpatrol/execute" + ATTACH_KEY;

	/** 获取巡检率明细 */
	public static final String WORKPATROL_FINISH_DETAIL = "/workpatrol/execute/%1$s/detail" + ATTACH_KEY;

	/** 获取默认解锁顺序 */
	public static final String CONF_UNLOCK_ORDER = "/dict/unlock_order/" + ATTACH_KEY;

	/** 获取锁具协议类型 */
	public static final String CONF_UNLOCK_PROTOCOL = "/dict/unlock_protocol/dictObject/" + ATTACH_KEY;

	/** 获取验证码 **/
	public static final String CAPTCHA = "/common/captcha" + ATTACH_KEY;

	/** 获取通讯录 **/
	public static final String CONTACT = "/contact/" + ATTACH_KEY;

	/** 报警信息 **/
	public static final String ALERT = "/alert/" + ATTACH_KEY;

	/** 公告 **/
	public static final String ANNOUNCEMENT = "/public/notice/" + ATTACH_KEY;

	/** 设备勘查 **/
	public static final String DEVICE_EXPLORATION = "/exploration/" + ATTACH_KEY;

	public static final String DISTANT_UNLOCK_APPLY = "/unlock/apply" + ATTACH_KEY;

	public static final String DISTANT_UNLOCK_VERIFY = "/unlock/%1$s/verify" + ATTACH_KEY;

	public static final String DISTANT_UNLOCK = "/unlock/" + ATTACH_KEY;

	public static final String EXPLORATION_UPLOAD_IMG = "/exploration/%1$s/file" + ATTACH_KEY;

	public static final String EXPLORATION_NEW = "/object/%1$s/exploration/new" + ATTACH_KEY;

	public static final String DEVICE_SEARCH = "/section/%1$s/object/downward" + ATTACH_KEY;

	/** 获取地区设备信息（首页地图的标签） **/
	public static final String MAIN_MAP_INFO = "/object/map3" + ATTACH_KEY;

	/** 获取权限内用户 **/
	public static final String GET_USER_UNDER = "/user/getUser" + ATTACH_KEY;

	/** 获取当前用户可见的设备（站点）列表 **/
	public static final String GET_OBJECT_DOWNWARD = "/section/%1$s/object/downward" + ATTACH_KEY;

	/** 获取当前用户可见的区域和代维单位 **/
	public static final String GET_SECTION_AND_COMPANY = "/section/%1$s/getsec" + ATTACH_KEY;

	/** 新建钥匙 **/
	public static final String SMARTKEY_NEW = "/section/%1$s/smartkey/new" + ATTACH_KEY;

	/** 修改钥匙 **/
	public static final String SMARTKEY_UPDATE = "/smartkey/%1$s/update" + ATTACH_KEY;

	/** 远程开门 **/
	public static final String REMOTE_UNLOCK = "/controllerLog/unlock" + ATTACH_KEY;

	/** 版本更新 **/
	public static final String APP_UPDATE = "/appPackage/getversion" + ATTACH_KEY;

	/** 新增站点 **/
	public static final String CREATE_STATION = "/section/%1$s/object/new/lockset/zone" + ATTACH_KEY;

	/** 修改站点 **/
	public static final String UPDATE_STATION = "/object/%1$s/update2" + ATTACH_KEY;

	/** 帮助 **/
	public static final String HELPER = "/public/help/" + ATTACH_KEY;

	/** 上传站点图片 **/
	public static final String UPLOAD_STATION_PHOTO = "/object/%1$s/file" + ATTACH_KEY;


}
