/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.adapter;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * 泛型通用适配器, 如果布局有复选框, 只支持 CheckedTextView
 * 
 * <pre>
 * 		adapter = new ContronAdapter(activity, R.layout.check_listview_item, persons) {
 * 
 * 			public void setViewBinder(ContronViewHolder viewHolder, Student item, int position) {
 * 
 * 				viewHolder.setText(R.id.textView1, item.name);
 * 				CheckedTextView checkedTextView = viewHolder.findViewById(R.id.checkedTextView1);
 * 				if (adapter.getCheckedData().get(position) != null) 
 * 					checkedTextView.setChecked(true);
 * 				else 
 * 					checkedTextView.setChecked(false);
 * 			}
 *  	};
 * 
 * 		listView.setOnItemClickListener(new OnItemClickListener() {
 * 
 * 			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
 * 				只有当ListView选择模式已被设置为 CHOICE_MODE_SINGLE 或 CHOICE_MODE_MULTIPLE 时, isItemChecked 结果才有效
 * 				boolean checked = listView.isItemChecked(position);
 * 				if (checked)
 * 					adapter.getCheckedData().put(position, adapter.getItem(position));
 * 				else
 * 					adapter.getCheckedData().remove(position);
 * 			}
 * 		});
 * </pre>
 * 
 * @author hupei
 * @date 2014年9月5日 上午9:21:18
 * @param <T>
 */
public abstract class ContronAdapter<T> extends BasicListAdapter<T> implements ListAdapterImpl<T> {
	protected LayoutInflater mInflater;
	protected Context mContext;
	private int mResource;// 视图ID
	private int mDropDownResource;// spinner下拉视图
	private List<T> mDatas;// 数据源
	private int selectedPosition = -1;// 当前选择的行号
	private SparseArray<T> selectedDatas;// 复选框选中的对象集

	public ContronAdapter(Context context, int resource, List<T> datas) {
		mInflater = LayoutInflater.from(context);
		this.mContext = context;
		this.mResource = mDropDownResource = resource;
		this.mDatas = datas;
		selectedDatas = new SparseArray<T>();
	}

	@Override
	public int getCount() {
		if (mDatas == null)
			return 0;
		return mDatas.size();
	}

	@Override
	public T getItem(int position) {
		return mDatas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return createViewFromResource(position, convertView, parent, mResource);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return createViewFromResource(position, convertView, parent, mDropDownResource);
	}

	private View createViewFromResource(int position, View convertView, ViewGroup parent, int resource) {
		// 获取缓存视图
		ContronViewHolder viewHolder = ContronViewHolder.get(mContext, mInflater, resource, convertView, parent);
		// 填充数据
		setViewValue(viewHolder, getItem(position), position);
		return viewHolder.getConvertView();
	}

	@Override
	public int getSelectedPosition() {
		return selectedPosition;
	}

	@Override
	public void setSelectedPosition(int selectedPosition) {
		this.selectedPosition = selectedPosition;
		notifyDataSetChanged();
	}

	@Override
	public List<T> getDatas() {
		return mDatas;
	}

	@Override
	public SparseArray<T> getSelectedDatas() {
		return selectedDatas;
	}

	@Override
	public void clearSelectedDatas() {
		selectedDatas.clear();
		notifyDataSetChanged();
	}

	@Override
	public void clearDatas() {
		if (mDatas != null)
			mDatas.clear();
	}

	@Override
	public void setEmpty() {
		clearDatas();
		notifyDataSetChanged();
	}

	@Override
	public void addData(T data) {
		if (mDatas != null)
			mDatas.add(data);
		notifyDataSetChanged();
	}
	public void addData(T data,int location) {
		if (mDatas != null)
			mDatas.add(location, data);
		notifyDataSetChanged();
	}


	@Override
	public void addAllDatas(Collection<? extends T> collection) {
		if (mDatas == null && collection == null)
			return;
		mDatas.addAll(collection);
		notifyDataSetChanged();
	}

	@Override
	public void sortDatas(Comparator<T> comparator) {
		Collections.sort(getDatas(), comparator);
		notifyDataSetChanged();
	}

	@Override
	public void removeDatas(int location) {
		if (mDatas == null)
			return;
		mDatas.remove(location);
		notifyDataSetChanged();
	}

	public void setDropDownViewResource(int resource) {
		this.mDropDownResource = resource;
	}
}
