/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.adapter;

import android.util.SparseArray;

interface AdapterImpl<T> {

	/**
	 * 获取选择的行号
	 * 
	 * @author hupei
	 * @date 2014年12月15日 下午4:26:04
	 * @return
	 */
	int getSelectedPosition();

	/**
	 * 设置选择的行号 并调用 notifyDataSetChanged 刷新
	 * 
	 * @author hupei
	 * @date 2014年12月15日 下午4:38:58
	 * @param selectedPosition
	 */
	void setSelectedPosition(int selectedPosition);

	/**
	 * 删除指定下标的数据
	 * 
	 * @author hupei
	 * @date 2014年12月15日 下午5:01:50
	 * @param location
	 */
	void removeDatas(int location);

	/**
	 * 获取复选框选中的对象集
	 * 
	 * @author hupei
	 * @date 2014年12月15日 下午4:45:49
	 * @return
	 */
	SparseArray<T> getSelectedDatas();

	/**
	 * 清除数据不刷新
	 * 
	 * @author hupei
	 * @date 2014年12月15日 下午4:53:09
	 */
	void clearDatas();

	/**
	 * 清空所有的选中对象 并调用 notifyDataSetChanged 刷新
	 * 
	 * @author hupei
	 * @date 2014年12月15日 下午4:39:55
	 */
	void clearSelectedDatas();

	/**
	 * 设置空数据, 并刷新
	 * 
	 * @author hupei
	 * @date 2014年12月15日 下午4:46:30
	 */
	void setEmpty();
}