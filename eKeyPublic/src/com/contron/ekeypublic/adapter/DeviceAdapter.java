/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Parcel;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.contron.ekeypublic.R;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Section;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class DeviceAdapter extends BaseExpandableListAdapter {
	private static boolean ischeck = true;// true 多选,false 单选

	private List<Boolean> m_ListGroupHolder = new ArrayList<Boolean>();

	static class GroupHolder {

		private TextView tvStationName;

		private CheckBox cbStationSelect;

		private ImageView img;

		public GroupHolder(View view) {
			this.tvStationName = (TextView) view.findViewById(R.id.TV1);
			this.cbStationSelect = (CheckBox) view
					.findViewById(R.id.chbox_setion);
			this.img = (ImageView) view.findViewById(R.id.img_icon);
			
			
			if (!ischeck) {// 单选侧隐藏
				cbStationSelect.setVisibility(View.GONE);
			}
		}

	}

	static class ChildHolder {
		private CheckedTextView tvDeviceName;

		public ChildHolder(View view) {
			this.tvDeviceName = (CheckedTextView) view.findViewById(R.id.TV2);
		}
	}

	private Context mContext;
	private ArrayList<Section> mStations;
	private LayoutInflater inflater;

//	public DeviceAdapter(Context context, ArrayList<Section> list) {
//		this.mContext = context;
//		this.mStations = list;
//		inflater = LayoutInflater.from(mContext);
//	}

	public DeviceAdapter(Context context, ArrayList<Section> list,
			boolean ischeck) {
		this.mContext = context;
		this.mStations = list;
		this.ischeck = ischeck;
		inflater = LayoutInflater.from(mContext);

		if (ischeck) {// 多选
			initialGroupHolder(mStations.size(), false);
		}
		for (int i = 0; i < mStations.size(); i++) {
			checkChildIfAllSelect(i);
		}
	}

	/**
	 * 初始始单位选中、取消选中
	 * 
	 * @param groupCount
	 * @param initalValue
	 */
	private void initialGroupHolder(int groupCount, boolean initalValue) {
		for (int i = 0; i < groupCount; i++) {
			m_ListGroupHolder.add(initalValue);
		}
	}

	@Override
	public int getGroupCount() {
		if (mStations == null)
			return 0;
		return mStations.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (mStations == null
				&& mStations.get(groupPosition).getDeviceList() == null)
			return 0;
		return mStations.get(groupPosition).deviceList.size();
	}

	@Override
	public Section getGroup(int groupPosition) {
		return mStations.get(groupPosition);
	}

	@Override
	public DeviceObject getChild(int groupPosition, int childPosition) {
		return mStations.get(groupPosition).deviceList.get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return 0;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return 0;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		// TODO Auto-generated method stub
		super.onGroupExpanded(groupPosition);
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		View view = convertView;
		final GroupHolder holder;
		if (view == null) {
			view = inflater.inflate(R.layout.fragment_section_list, parent,
					false);
			holder = new GroupHolder(view);
			view.setTag(holder);
		} else {
			holder = (GroupHolder) view.getTag();
		}
		
		if(isExpanded){//设置展开图标
			holder.img.setBackgroundResource(R.drawable.tree_ex);
		}else{
			holder.img.setBackgroundResource(R.drawable.tree_ec);
		}
		
		Section deviceObject = getGroup(groupPosition);
		holder.tvStationName.setText(deviceObject.getName());
		CheckBox cbStationSelect = holder.cbStationSelect;// 单位复选框

		
		
		if (ischeck && null != cbStationSelect) {// 多选时
			cbStationSelect.setChecked(m_ListGroupHolder.get(groupPosition));

			cbStationSelect.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					m_ListGroupHolder.set(groupPosition,
							!m_ListGroupHolder.get(groupPosition));

					childAllSelect(groupPosition,
							m_ListGroupHolder.get(groupPosition));
				}
			});

		}

		return view;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		View view = convertView;
		ChildHolder holder;
		if (view == null) {
			view = inflater.inflate(R.layout.fragment_device_list, parent,
					false);
			holder = new ChildHolder(view);
			// ViewUtils.inject(holder, view);
			view.setTag(holder);
		} else {
			holder = (ChildHolder) view.getTag();
		}
		DeviceObject device = getChild(groupPosition, childPosition);
		holder.tvDeviceName.setText(device.getName());
		holder.tvDeviceName.setChecked(device.isCheck() ? true : false);
		return view;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;// ������
	}

	public void addDataAll(List<Section> collection) {
		if (mStations != null && collection != null) {
			mStations.addAll(collection);
			notifyDataSetChanged();
		}
	}

	public void toggleChildSelect(View v, int groupPosition, int childPosition) {
		DeviceObject device = getChild(groupPosition, childPosition);
		ChildHolder holder = (ChildHolder) v.getTag();

		if (ischeck) {// 多选
			if (holder.tvDeviceName.isChecked()) {// 当前设备为已经选择
				m_ListGroupHolder.set(groupPosition, false);
				// holder.tvDeviceName.setChecked(false);
				// mStations.get(groupPosition).getDeviceList().get(childPosition)
				// .setCheck(false);
				// device.isCheck=false;
				device.setCheck(false);
			} else {// 当前设备没有选择
				// holder.tvDeviceName.setChecked(true);
				// mStations.get(groupPosition).getDeviceList().get(childPosition)
				// .setCheck(true);
				// device.isCheck = true;
				device.setCheck(true);
				checkChildIfAllSelect(groupPosition);
			}
		} else {// 单选择

			if (holder.tvDeviceName.isChecked()) {// 当前设备为已经选择
				// holder.tvDeviceName.setChecked(false);
				// mStations.get(groupPosition).getDeviceList().get(childPosition)
				// .setCheck(false);
				// device.isCheck=false;
				device.setCheck(false);

			} else {// 当前设备没有选择
				clearSelect();
				// holder.tvDeviceName.setChecked(true);
				// mStations.get(groupPosition).getDeviceList().get(childPosition)
				// .setCheck(true);
				// device.isCheck = true;
				device.setCheck(true);
			}

		}

		notifyDataSetChanged();
	}

	public ArrayList<Section> clearSelect() {
		for (Section station : mStations) {
			for (DeviceObject device : station.deviceList) {
				device.setCheck(false);
			}
		}
		return getStations();
	}

	public ArrayList<Section> getStations() {
		return mStations;
	}

	/**
	 * 选择\取消 单位下所有设备
	 */
	private void childAllSelect(int groupPosition, boolean check) {
		List<DeviceObject> deviceObjectList = mStations.get(groupPosition)
				.getDeviceList();

		if (null == deviceObjectList)
			return;

		for (DeviceObject device : deviceObjectList) {
			device.setCheck(check);
		}
		notifyDataSetChanged();
	}

	/**
	 * 检查当前组是否全部选择了
	 * @param groupPosition
	 */
	private void checkChildIfAllSelect(int groupPosition) {
		if (!ischeck) //如果是单选则不判断是否全部选中
			return;
		List<DeviceObject> deviceObjectList = mStations.get(groupPosition)
				.getDeviceList();

		if (null == deviceObjectList || deviceObjectList.size() == 0)
			return;

		for (DeviceObject device : deviceObjectList) {
			if (!device.isCheck()) {
				return;
			}
		}
		m_ListGroupHolder.set(groupPosition, true);
		notifyDataSetChanged();
	}
}
