/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.TextView;

public final class ContronViewHolder {
	private SparseArray<View> views;
	private View convertView;

	private ContronViewHolder(Context context, LayoutInflater inflater, int resource, ViewGroup parent) {
		views = new SparseArray<View>();
		convertView = inflater.inflate(resource, parent, false);
		convertView.setTag(this);
	}

	/**
	 * 获取CommonViewHolder 实例
	 * 
	 * @author hupei
	 * @date 2014年9月4日 下午2:23:56
	 * @param context
	 * @param resource
	 * @param convertView
	 * @param parent
	 * @return
	 */
	public static ContronViewHolder get(Context context, LayoutInflater inflater, int resource, View convertView, ViewGroup parent) {
		if (convertView == null)
			return new ContronViewHolder(context, inflater, resource, parent);
		return (ContronViewHolder) convertView.getTag();
	}

	/**
	 * 通过ID查找控件, 如果没有则绑定并加入views中
	 * 
	 * @author hupei
	 * @param <T>
	 * @date 2014年9月4日 下午2:22:56
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends View> T findView(int id) {
		View view = views.get(id);
		if (view == null) {
			view = convertView.findViewById(id);
			views.put(id, view);
			//由于ViewStub 只能 inflate 一次，所以在这里inflate
			if(view instanceof ViewStub)
				((ViewStub) view).inflate();
		}
		return (T) view;
	}

	/**
	 * 设置 TextView 的显示的字符串
	 * 
	 * @author hupei
	 * @date 2014年9月4日 下午5:03:31
	 * @param viewId
	 *            TextView的 ID
	 * @param text
	 *            TextView显示的字符串
	 * @return
	 */
	public ContronViewHolder setText(int viewId, String text) {
		View view = findView(viewId);
		if (view instanceof TextView) {
			final TextView textView = (TextView) view;
			textView.setText(text);
		} else if (view instanceof Button) {
			final Button btn = (Button) view;
			btn.setText(text);
		}
		return this;
	}
	 

	

	/**
	 * 设置 ImageView 图片 src
	 * 
	 * @author hupei
	 * @date 2014年9月12日 下午4:16:46
	 * @param viewId
	 * @param resId
	 * @return
	 */
	public ContronViewHolder setImageResource(int viewId, int resId) {
		return setImageSrc(viewId, resId);
	}

	/**
	 * 设置 ImageView 图片 src
	 * 
	 * @author hupei
	 * @date 2014年9月12日 下午4:16:36
	 * @param viewId
	 * @param drawable
	 * @return
	 */
	public ContronViewHolder setImageDrawable(int viewId, Drawable drawable) {
		return setImageSrc(viewId, drawable);
	}

	/**
	 * 设置 ImageView 图片 src
	 * 
	 * @author hupei
	 * @date 2014年9月12日 下午4:17:27
	 * @param viewId
	 * @param bitmap
	 * @return
	 */
	public ContronViewHolder setImageBitmap(int viewId, Bitmap bitmap) {
		return setImageSrc(viewId, bitmap);
	}

	/**
	 * 设置 ImageView 图片 src
	 * 
	 * @author hupei
	 * @date 2014年9月5日 上午10:04:11
	 * @param viewId
	 * @param src
	 * @return
	 */
	private ContronViewHolder setImageSrc(int viewId, Object src) {
		final ImageView imageView = findView(viewId);
		if (src instanceof Integer)
			imageView.setImageResource((Integer) src);
		else if (src instanceof Drawable)
			imageView.setImageDrawable((Drawable) src);
		else if (src instanceof Bitmap)
			imageView.setImageBitmap((Bitmap) src);
		return this;
	}

	/**
	 * 设置 CheckedTextView 的选择状态
	 * 
	 * @author hupei
	 * @date 2014年9月5日 下午1:28:22
	 * @param viewId
	 * @param checked
	 * @return
	 */
	public ContronViewHolder setChecked(int viewId, boolean checked) {
		final CheckedTextView checkedTextView = findView(viewId);
		checkedTextView.setChecked(checked);
		return this;
	}

	/**
	 * 根据条件切换背景
	 * 
	 * @author hupei
	 * @date 2014年9月12日 下午4:23:30
	 * @param viewId
	 * @param flag
	 * @param colorHover
	 * @param colorNormal
	 * @return
	 */
	public ContronViewHolder setBackgroundColor(int viewId, boolean flag, int colorHover, int colorNormal) {
		return toggleBackground(findView(viewId), flag, BackgroundType.Color, colorHover, colorNormal);
	}

	/**
	 * 根据条件切换背景
	 * 
	 * @author hupei
	 * @date 2014年9月12日 下午4:24:43
	 * @param viewId
	 * @param flag
	 * @param residHover
	 * @param residNormal
	 * @return
	 */
	public ContronViewHolder setBackgroundResource(int viewId, boolean flag, int residHover, int residNormal) {
		return toggleBackground(findView(viewId), flag, BackgroundType.Resource, residHover, residNormal);
	}

	/**
	 * 根据条件切换背景
	 * 
	 * @author hupei
	 * @date 2014年9月12日 下午4:25:01
	 * @param viewId
	 * @param flag
	 * @param bgHover
	 * @param bgNormal
	 * @return
	 */
	public ContronViewHolder setBackgroundDrawable(int viewId, boolean flag, Drawable bgHover, Drawable bgNormal) {
		return toggleBackground(findView(viewId), flag, BackgroundType.Drawable, bgHover, bgNormal);
	}

	/**
	 * 根据条件切换背景.
	 * 
	 * @author hupei
	 * @date 2014年9月5日 下午1:28:38
	 * @param viewID
	 *            切换背景的VIEWID
	 * @param flag
	 *            true 为 选择时背景
	 * @param type
	 *            背景值的类型 setBackgroundColor setBackgroundResource
	 * @param bgHover
	 *            选择时的背景颜色值
	 * @param bgNormal
	 *            非选择时的背景颜色值
	 * @return
	 */
	public ContronViewHolder toggleBackground(int viewID, boolean flag, BackgroundType type, Object bgHover, Object bgNormal) {
		return toggleBackground(findView(viewID), flag, type, bgHover, bgNormal);
	}

	/**
	 * 根据条件切换背景
	 * 
	 * @author hupei
	 * @date 2014年9月12日 下午4:23:30
	 * @param view
	 * @param flag
	 * @param colorHover
	 * @param colorNormal
	 * @return
	 */
	public ContronViewHolder setBackgroundColor(View view, boolean flag, int colorHover, int colorNormal) {
		return toggleBackground(view, flag, BackgroundType.Color, colorHover, colorNormal);
	}

	/**
	 * 根据条件切换背景
	 * 
	 * @author hupei
	 * @date 2014年9月12日 下午4:24:43
	 * @param view
	 * @param flag
	 * @param residHover
	 * @param residNormal
	 * @return
	 */
	public ContronViewHolder setBackgroundResource(View view, boolean flag, int residHover, int residNormal) {
		return toggleBackground(view, flag, BackgroundType.Resource, residHover, residNormal);
	}

	/**
	 * 根据条件切换背景
	 * 
	 * @author hupei
	 * @date 2014年9月12日 下午4:25:01
	 * @param view
	 * @param flag
	 * @param bgHover
	 * @param bgNormal
	 * @return
	 */
	public ContronViewHolder setBackgroundDrawable(View view, boolean flag, Drawable bgHover, Drawable bgNormal) {
		return toggleBackground(view, flag, BackgroundType.Drawable, bgHover, bgNormal);
	}

	/**
	 * 根据条件切换背景.
	 * 
	 * @author hupei
	 * @date 2014年9月5日 下午1:28:38
	 * @param view
	 *            切换背景的VIEW
	 * @param flag
	 *            true 为 选择时背景
	 * @param type
	 *            背景值的类型 setBackgroundColor setBackgroundResource
	 * @param bgHover
	 *            选择时的背景颜色值
	 * @param bgNormal
	 *            非选择时的背景颜色值
	 * @return
	 */
	private ContronViewHolder toggleBackground(View view, boolean flag, BackgroundType type, Object bgHover, Object bgNormal) {
		int background = 0;
		Drawable backgroundDrawable = null;
		if (bgHover instanceof Integer && bgNormal instanceof Integer) {
			if (flag)
				background = (Integer) bgHover;
			else
				background = (Integer) bgNormal;
		} else if (bgHover instanceof Drawable && bgNormal instanceof Drawable) {
			if (flag)
				backgroundDrawable = (Drawable) bgHover;
			else
				backgroundDrawable = (Drawable) bgNormal;
		} else {
			return this;
		}
		switch (type) {
		case Color:
			view.setBackgroundColor(background);
			break;
		case Resource:
			view.setBackgroundResource(background);
			break;
		case Drawable:
			view.setBackgroundDrawable(backgroundDrawable);
			break;
		default:
			break;
		}
		return this;
	}

	public View getConvertView() {
		return convertView;
	}

	private enum BackgroundType {
		Color, Resource, Drawable;
	}

	public ContronViewHolder setTextColor(int viewId, int color) {
		// TODO Auto-generated method stub
			View view = findView(viewId);
			if (view instanceof TextView) {
				final TextView textView = (TextView) view;
				textView.setTextColor(color);
				
			}	
			return this;
		
	}
	public ContronViewHolder setBackgroundResource(int viewId, int source){
		View view = findView(viewId);
		if (view instanceof TextView) {
			final TextView textView = (TextView) view;
			textView.setBackgroundResource(source);
		}
		return this;	
	}
}
