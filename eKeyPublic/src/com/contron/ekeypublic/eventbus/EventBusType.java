/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.eventbus;

public class EventBusType {
	
	/**
	 * 本地数据库查询站、设备、锁
	 * 
	 * @author hupei
	 * @date 2015年9月25日 上午11:36:59
	 */
	public static class DownLoadStationLockerDevice {

	}

	/**
	 * 蓝牙电量
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午9:55:17
	 */
	public static class BtBatt {
		public String des;
		public boolean isMin;

		public BtBatt() {
		}

		public BtBatt(String des, boolean isMin) {
			this.des = des;
			this.isMin = isMin;
		}
	}

	/**
	 * 十六进制规约
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午10:49:01
	 */
	public static class BtHex {
		public String commd;

		public BtHex() {
		}

		public BtHex(String commd) {
			this.commd = commd;
		}
	}

	/**
	 * PIO类型命令
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午10:47:51
	 */
	public static class BtPIO {
		public String commd;

		public BtPIO() {
		}

		public BtPIO(String commd) {
			this.commd = commd;
		}

		/**
		 * 解锁是否成功
		 * 
		 * @author hupei
		 * @date 2015年9月22日 上午10:29:47
		 * @return true成功
		 */
		public boolean isSuccess() {
			return commd.equalsIgnoreCase("OK+PIO2:1");
		}
	}
	
	
	
	/**
	 * nfc读取的智能钥匙蓝牙地址
	 * 
	 * @author luoyilong
	 *
	 */
	public static class EvenNfcLyAddress {
		public String address;// 蓝牙地址

		public EvenNfcLyAddress(String address) {
			super();
			this.address = address;
		}
	}
	
	
	
}
