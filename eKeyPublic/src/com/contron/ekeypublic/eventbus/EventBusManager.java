/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.eventbus;
import de.greenrobot.event.EventBus;

public class EventBusManager {
	private static EventBusManager instance = null;
	private EventBus eventBus;

	public static EventBusManager getInstance() {
		if (instance == null) {
			synchronized (EventBusManager.class) {
				if (instance == null)
					instance = new EventBusManager();
			}
		}
		return instance;
	}

	private EventBusManager() {
		eventBus = EventBus.getDefault();
	}

	public void register(Object subscriber) {
		eventBus.register(subscriber);
	}

	public void unregister(Object subscriber) {
		eventBus.unregister(subscriber);
	}

	/**
	 * 发送蓝牙状态 <br>
	 * {@linkplain TicketDetailFragment#onEventMainThread(BtState)} 接收<br>
	 * {@linkplain EmpowerActivity#onEventMainThread(BtState)} 接收<br>
	 * {@linkplain DoorLockerFragment#onEventMainThread(BtState)} 接收 <br>
	 * {@linkplain DoorLockerListFragment#onEventMainThread(BtState)} 接收 <br>
	 * 
	 * 发送蓝牙电量<br>
	 * {@linkplain TicketDetailFragment#onEventMainThread(BtBatt)} 接收<br>
	 * {@linkplain EmpowerActivity#onEventMainThread(BtBatt)} 接收<br>
	 * 
	 * 发送蓝牙数据超时 <br>
	 * {@linkplain TicketDetailFragment#onEventMainThread(com.contron.ekeycommunication.bluetooth.BtTimeOut)}
	 * 接收<br>
	 * {@linkplain EmpowerActivity#onEventMainThread(com.contron.ekeycommunication.bluetooth.BtTimeOut)}
	 * 	 * 接收<br>
	 * {@linkplain DoorLockerListFragment#onEventMainThread(com.contron.ekeycommunication.bluetooth.BtTimeOut)}
	 * 接收<br>
	 * 
	 * 蓝牙自动连接 <br>
	 * {@linkplain TicketDetailFragment#onEventMainThread(com.contron.ekeycommunication.bluetooth.BtConnAuto)}
	 * 接收<br>
	 * {@linkplain EmpowerActivity#onEventMainThread(com.contron.ekeycommunication.bluetooth.BtConnAuto)}
	 * 接收<br>
	 * {@linkplain DoorLockerFragment#onEventMainThread(com.contron.ekeycommunication.bluetooth.BtConnAuto)}
	 *  接收<br>
	 * {@linkplain DoorLockerListFragment#onEventMainThread(com.contron.ekeycommunication.bluetooth.BtConnAuto)}
	 * 接收 <br>
	 * 
	 * 本地数据库查询站、设备、锁<br>
	 * {@linkplain PlanTicketApplyFragment#onEventAsync(com.contron.ekeycommunication.eventbus.EventBusType.DownLoadStationLockerDevice)}
	 * 
	 * 刷新操作记录<br>
	 * {@linkplain DoorLockerResultFragment#onEventMainThread(com.contron.ekeycommunication.entities.TicketResult)}
	
	
	 * 刷新操作记录<br>
	 * {@linkplain FsuDistantResultFragment#onEventMainThread(com.contron.ekeycommunication.entities.LockerResult)}
	 * 
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午9:37:24
	 * @param event
	 */
	public void post(Object event) {
		eventBus.post(event);
	}
}
