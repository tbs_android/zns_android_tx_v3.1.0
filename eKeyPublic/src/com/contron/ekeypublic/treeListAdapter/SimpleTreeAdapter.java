package com.contron.ekeypublic.treeListAdapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.contron.ekeypublic.R;
import com.contron.ekeypublic.entities.Section;

public class SimpleTreeAdapter extends TreeListViewAdapter {

	public interface BackResult {// 返回选择中数据
		public void onBackResult(Node node);
	}

	private BackResult backResult;

	public SimpleTreeAdapter(BackResult backResult, ListView mTree,
			Context context, List<Section> datas, int defaultExpandLevel)
			throws IllegalArgumentException, IllegalAccessException {		
		super(mTree, context, datas, defaultExpandLevel);
		this.backResult=backResult;
	}

	@Override
	public View getConvertView(final Node node, int position, View convertView,
			ViewGroup parent) {

		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(
					R.layout.list_item, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.icon = (ImageView) convertView
					.findViewById(android.R.id.icon);
			viewHolder.label = (TextView) convertView
					.findViewById(android.R.id.text1);
			
			RadioButton rad=(RadioButton) convertView
					.findViewById(R.id.rad_check);
			
			rad.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
				if(null!=backResult)
					backResult.onBackResult(node);
					
				}
			});
			convertView.setTag(viewHolder);

		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		if (node.getIcon() == -1) {
			viewHolder.icon.setVisibility(View.INVISIBLE);
		} else {
			viewHolder.icon.setVisibility(View.VISIBLE);
			viewHolder.icon.setImageResource(node.getIcon());
		}

		viewHolder.label.setText(node.getName());

		return convertView;
	}

	private final class ViewHolder {
		ImageView icon;
		TextView label;
	}

}
