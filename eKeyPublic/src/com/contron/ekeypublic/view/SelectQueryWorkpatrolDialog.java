package com.contron.ekeypublic.view;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeypublic.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.SectionAll;
import com.contron.ekeypublic.util.CygPickerDateFragment;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.util.CygPickerDateFragment.OnDateDoneListener;
import com.lidroid.xutils.util.LogUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.CursorAdapter;
import android.text.Selection;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

public class SelectQueryWorkpatrolDialog extends BasicDialogFragment {

	private EditText mEditObj;// 基站

	private EditText mEditCycleNum;// 周期数

	private Spinner mSpinYear;// 年份

	private String mTitle;// 标题

	private Activity mActivity;

	private List<Dict> mListYear;// 年份数据

	private List<SectionAll> mListAgency;// 代维公司

	private List<Dict> mListCycle;// 周期数据

	private Dict mSelectYear;// 选中的年份

	private SectionAll mSelectAgency;// 选中的代维公司

	private Dict mSelectCycle;// 选择的周期

	private int seleYear = 0;// 默认年份

	// 年份适配器
	private ContronAdapter<Dict> mAdapterYear;

	// 代理公司适配器
	private ContronAdapter<SectionAll> mAdapterAgency;

	// 周期适配器
	private ContronAdapter<Dict> mAdapterCycle;

	// 回调接口
	private SelectListener mSelectListener;

	// 点击确定按钮后返回
	public interface SelectListener {
		void select(JSONObject jsonObject);
	}

	/**
	 * 构造方法
	 * 
	 * @param activity
	 * @param selectListener
	 *            回调接口
	 * @param listYear
	 *            年份
	 * @param listAgency
	 *            代维公司
	 * @param listCycle
	 *            周期
	 * @param title
	 */
	public SelectQueryWorkpatrolDialog(Activity activity,
			SelectListener selectListener, List<Dict> listYear,
			List<SectionAll> listAgency, List<Dict> listCycle, String title) {
		mActivity = activity;
		this.mListYear = listYear;
		this.mListAgency = listAgency;
		this.mListCycle = listCycle;
		this.mSelectListener = selectListener;
		this.mTitle = title;
		initData();
	}

	/**
	 * 为代维公司、地点添加默认值
	 */
	private void initData() {

		Dict defaultDict = new Dict();
		defaultDict.setName("请选择!");
		defaultDict.setValue("");

		SectionAll defaultSection = new SectionAll();
		defaultSection.setId(-1);
		defaultSection.setName("请选择!");

		if (mListYear != null) {
			String currYear = DateUtil.getCurrentDate("yyyy");
			for (int i = 0; i < mListYear.size(); i++) {
				if (currYear.equals(mListYear.get(i).getValue()))
					seleYear = i;
			}
		}

		if (mListAgency != null)
			mListAgency.add(0, defaultSection);

		if (mListCycle != null)
			mListCycle.add(0, defaultDict);

	}

	@Override
	public View initDialog() {
		LayoutInflater inflater = mActivity.getLayoutInflater();
		View view = inflater.inflate(R.layout.fragment_query_workpatrol_dialog,
				null);
		mSpinYear = (Spinner) view.findViewById(R.id.spin_year);
		Spinner mspinAgency = (Spinner) view.findViewById(R.id.spin_agency);
		Spinner mspinCycle = (Spinner) view.findViewById(R.id.spin_cycle);
		mSpinYear.setSelection(seleYear);
		mSpinYear.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				mSelectYear = (Dict) arg0.getItemAtPosition(arg2);

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		mspinAgency.setSelection(0);
		mspinAgency.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				mSelectAgency = (SectionAll) arg0.getItemAtPosition(arg2);

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		mspinCycle.setSelection(0);
		mspinCycle.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				mSelectCycle = (Dict) arg0.getItemAtPosition(arg2);

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});

		setAdapter(mSpinYear, mspinAgency, mspinCycle);

		mEditObj = (EditText) view.findViewById(R.id.edit_obj);

		mEditCycleNum = (EditText) view.findViewById(R.id.edit_cyclenum);

		Button but_ok = (Button) view.findViewById(R.id.but_ok);// 确定

		Button but_no = (Button) view.findViewById(R.id.but_no);// 取消

		but_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String cycleNum = mEditCycleNum.getText().toString();
				JSONObject json = new JSONObject();
				try {
					json.put("year", mSelectYear.getValue());// 年份
					json.put("cycle", mSelectCycle.getValue());// 周期
					json.put("sid", mSelectAgency.getId());// 代维公司
					json.put("oname", mEditObj.getText().toString());// 基站
					json.put("cyclenum", ("".equals(cycleNum) ? -1 : cycleNum));// 周期数
				} catch (JSONException e) {

					e.printStackTrace();
				}
				if (mSelectListener != null)// 回调方法不为空后进行回调
					mSelectListener.select(json);
				dismiss();
			}
		});

		but_no.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dismiss();
			}
		});

		return view;
	}

	private void setAdapter(Spinner checktype, Spinner agency, Spinner section) {

		if (mListYear == null)
			mListYear = new ArrayList<Dict>();

		if (mListAgency == null)
			mListAgency = new ArrayList<SectionAll>();

		if (mListCycle == null)
			mListCycle = new ArrayList<Dict>();

		// 初始化适配器
		mAdapterYear = new ContronAdapter<Dict>(mActivity,
				android.R.layout.simple_spinner_item, mListYear) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder, Dict item,
					int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
		mAdapterYear
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		checktype.setAdapter(mAdapterYear);

		mAdapterAgency = new ContronAdapter<SectionAll>(mActivity,
				android.R.layout.simple_spinner_item, mListAgency) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					SectionAll item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
		mAdapterAgency
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		agency.setAdapter(mAdapterAgency);

		mAdapterCycle = new ContronAdapter<Dict>(mActivity,
				android.R.layout.simple_spinner_item, mListCycle) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder, Dict item,
					int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
		mAdapterCycle
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		section.setAdapter(mAdapterCycle);
	}

	protected void showToast(CharSequence text) {
		Toast.makeText(mActivity, text, Toast.LENGTH_LONG).show();
	}

	@Override
	public String setTitle() {
		return mTitle;
	}
}
