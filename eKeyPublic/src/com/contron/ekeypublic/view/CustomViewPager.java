package com.contron.ekeypublic.view;

/**
 * Created by luoyilong on 2015/12/15.
 */

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 可禁用滑动的 ViewPager
 *
 * @author hupei
 * @date 2014年9月9日 上午9:05:26
 */
public class CustomViewPager extends ViewPager {

    private boolean enabled = true;

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setPagerTouchEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (enabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }
}

