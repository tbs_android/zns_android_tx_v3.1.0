package com.contron.ekeypublic.view;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeypublic.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.SectionAll;
import com.contron.ekeypublic.util.CygPickerDateFragment;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.util.CygPickerDateFragment.OnDateDoneListener;
import com.lidroid.xutils.util.LogUtils;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;

public class SelectQueryWorkbillDialog extends BasicDialogFragment {

	private ImageTextButton mImgbutStartimg;// 开始时间

	private ImageTextButton mImgbutStoptimg;// 结束时间

	private EditText mEditObj;// 基站

	private EditText mEditExecby;// 执行人

	private Spinner mSpinChecktype;// 查询方式

	private String mTitle;// 标题

	private Activity mActivity;

	private List<Dict> mListChecktype;// 查询方式数据

	private List<SectionAll> mListAgency;// 代维公司数据

	private List<SectionAll> mListSection;// 地点数据

	private Dict mSelectChecktype;// 选中的查询方式

	private SectionAll mSelectAgency;// 选中的代维公司

	private SectionAll mSelectSection;// 选择的地点

	// 查询方式适配器
	private ContronAdapter<Dict> mAdapterChecktype;

	// 代理公司适配器
	private ContronAdapter<SectionAll> mAdapterAgency;
	// 地点适配器
	private ContronAdapter<SectionAll> mAdapterSection;

	// 回调接口
	private SelectListener mSelectListener;

	// 点击确定按钮后返回
	public interface SelectListener {
		void select(JSONObject jsonObject);
	}

	/**
	 * 构造方法
	 * 
	 * @param activity
	 * @param selectListener
	 * @param listChecktype
	 * @param listAgency
	 * @param listSection
	 * @param title
	 */
	public SelectQueryWorkbillDialog(Activity activity,
			SelectListener selectListener, List<Dict> listChecktype,
			List<SectionAll> listAgency, List<SectionAll> listSection, String title) {
		mActivity = activity;
		this.mListChecktype = listChecktype;
		this.mListAgency = listAgency;
		this.mListSection = listSection;
		this.mSelectListener = selectListener;
		this.mTitle = title;
		initData();
	}

	/**
	 * 为代维公司、地点添加默认值
	 */
	private void initData() {
		SectionAll defaultAgency = new SectionAll();
		defaultAgency.setId(-1);
		defaultAgency.setName("请选择！");

		if (mListAgency != null)
			mListAgency.add(0, defaultAgency);

		if (mListSection != null)
			mListSection.add(0, defaultAgency);

	}

	@Override
	public View initDialog() {
		LayoutInflater inflater = mActivity.getLayoutInflater();
		View view = inflater.inflate(R.layout.fragment_query_workbill_dialog,
				null);
		mSpinChecktype = (Spinner) view.findViewById(R.id.spin_checktype);
		Spinner mSpinAgency = (Spinner) view.findViewById(R.id.spin_agency);
		Spinner mSpinSection = (Spinner) view.findViewById(R.id.spin_section);
		mSpinChecktype.setSelection(0);
		mSpinChecktype.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				mSelectChecktype = (Dict) arg0.getItemAtPosition(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		mSpinAgency.setSelection(0);
		mSpinAgency.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				mSelectAgency = (SectionAll) arg0.getItemAtPosition(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		mSpinSection.setSelection(0);
		mSpinSection.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				mSelectSection = (SectionAll) arg0.getItemAtPosition(arg2);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		setAdapter(mSpinChecktype, mSpinAgency, mSpinSection);

		mEditObj = (EditText) view.findViewById(R.id.edit_obj);
		mEditExecby = (EditText) view.findViewById(R.id.edit_execby);

		String currDate = DateUtil.getCurrentDate("yyyy-MM-dd");
		String currstartDate = DateUtil.getCurrentDate("yyyy-MM");

		mImgbutStartimg = (ImageTextButton) view
				.findViewById(R.id.imgbut_startdate);

		mImgbutStartimg.setText(currstartDate + "-01");

		mImgbutStoptimg = (ImageTextButton) view
				.findViewById(R.id.imgbut_stopdate);
		mImgbutStoptimg.setText(currDate);

		mImgbutStartimg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				getDate(R.id.imgbut_startdate);
			}
		});

		mImgbutStoptimg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				getDate(R.id.imgbut_stopdate);
			}
		});

		Button but_ok = (Button) view.findViewById(R.id.but_ok);// 确定

		Button but_no = (Button) view.findViewById(R.id.but_no);// 取消

		but_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				String createfrom = mImgbutStartimg.getText().toString();
				String createto = mImgbutStoptimg.getText().toString();
				int type = (int) mSpinChecktype.getSelectedItemId();

				JSONObject json = new JSONObject();
				try {
					json.put("type", type + 1);// 查看方式
					json.put("sid", mSelectAgency.getId());// 代维公司
					json.put("oname", mEditObj.getText().toString());// 基站
					json.put("createfrom", createfrom.replace("-", "")
							+ "000000");// 开始时间
					json.put("createto", createto.replace("-", "") + "235959");// 结束时间
					json.put("area", mSelectSection.getId());// 地点
					json.put("createby", mEditExecby.getText().toString());// 创建
																			// 人
				} catch (JSONException e) {

					e.printStackTrace();
				}
				if (mSelectListener != null)// 回调方法不为空后进行回调
					mSelectListener.select(json);
				dismiss();
			}
		});

		but_no.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dismiss();
			}
		});

		return view;
	}

	/**
	 * 弹出日期选择框获取时间
	 * 
	 * @param button
	 *            点击的控
	 * 
	 */
	private void getDate(final int button) {

		CygPickerDateFragment date = CygPickerDateFragment
				.newInstance(new OnDateDoneListener() {

					@Override
					public void onDate(int year, int monthOfYear,
							int dayOfMonth, String date) {
						LogUtils.e("=====日期=======" + date);
						if (button == R.id.imgbut_startdate) {
							startDateCompare(date);
						} else if (button == R.id.imgbut_stopdate) {
							stopDateCompare(date);
						}

					}
				});

		date.show(mActivity.getFragmentManager(), "datetime");
	}

	/**
	 * 对所选择开始时间进行判断、更新
	 * 
	 * @param startDate
	 */
	private void startDateCompare(String startDate) {

		if (TextUtils.isEmpty(startDate)
				|| startDate.equals(mImgbutStartimg.getText().toString()))
			return;

		if (DateUtil.timeCompareWithCurrentDate(startDate)) {
			showToast("开始时间不能大于当前时间！");
			return;
		}

		String stopDate = mImgbutStoptimg.getText();
		if (!TextUtils.isEmpty(stopDate)) {
			if (DateUtil.timeCompare(stopDate, startDate)) {
				showToast("开始时间不能大于结束时间！");

				return;
			}
		}
		mImgbutStartimg.setText(startDate);
	}

	/**
	 * 对所选择结束时间进行判断、更新
	 * 
	 * @param date
	 */
	private void stopDateCompare(String stopDate) {

		if (TextUtils.isEmpty(stopDate)
				|| stopDate.equals(mImgbutStoptimg.getText().toString()))
			return;

		if (DateUtil.timeCompareWithCurrentDate(stopDate)) {
			showToast("结束时间不能大于当前时间！");
			return;
		}

		String startDate = mImgbutStartimg.getText();
		if (!TextUtils.isEmpty(startDate)) {
			if (DateUtil.timeCompare(stopDate, startDate)) {
				showToast("结束时间不能小于开始时间！");
				return;
			}
		}
		mImgbutStoptimg.setText(stopDate);
	}

	private void setAdapter(Spinner checktype, Spinner agency, Spinner section) {

		if (mListChecktype == null)
			mListChecktype = new ArrayList<Dict>();

		if (mListAgency == null)
			mListAgency = new ArrayList<SectionAll>();

		if (mListSection == null)
			mListSection = new ArrayList<SectionAll>();

		// 初始化适配器
		mAdapterChecktype = new ContronAdapter<Dict>(mActivity,
				android.R.layout.simple_spinner_item, mListChecktype) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder, Dict item,
					int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
		mAdapterChecktype
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		checktype.setAdapter(mAdapterChecktype);

		mAdapterAgency = new ContronAdapter<SectionAll>(mActivity,
				android.R.layout.simple_spinner_item, mListAgency) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					SectionAll item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
		mAdapterAgency
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		agency.setAdapter(mAdapterAgency);

		mAdapterSection = new ContronAdapter<SectionAll>(mActivity,
				android.R.layout.simple_spinner_item, mListSection) {
			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					SectionAll item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName());
			}
		};
		mAdapterSection
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		section.setAdapter(mAdapterSection);
	}

	protected void showToast(CharSequence text) {
		Toast.makeText(mActivity, text, Toast.LENGTH_LONG).show();
	}

	@Override
	public String setTitle() {
		return mTitle;
	}
}
