package com.contron.ekeypublic.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 输入对话框
 * 
 * @author hupei
 * @date 2014年12月19日 下午2:59:14
 */
public class InputDialogFragment extends DialogFragment {
	public interface OnInputTextListener {
		void OnInputText(String text);
	}

	private int titleId;
	private int hintId;
	private String msg;
	private OnInputTextListener mInputTextListener;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final EditText editText = new EditText(getActivity());
		editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(32) });
		if (null != msg && !"".equals(msg)) {
			editText.setText(msg);
		}
		editText.setHint(hintId);
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(editText);
		if (titleId != 0)
			builder.setTitle(titleId);
		else
			builder.setTitle("输入内容");

		builder.setPositiveButton(android.R.string.ok, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				String text = editText.getText().toString();
				if (TextUtils.isEmpty(text)) {
					Toast.makeText(getActivity(), "输入内容不能为空", Toast.LENGTH_LONG)
							.show();
					return;
				}
				if (mInputTextListener != null)
					mInputTextListener.OnInputText(text);
			}
		});
		builder.setNegativeButton(android.R.string.cancel,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dismiss();
					}
				});
		return builder.create();
	}

	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}

	public void setHintId(int hintId) {
		this.hintId = hintId;
	}

	public void setdefaultMsg(String context) {
		this.msg = context;
	}

	/**
	 * 确定事件
	 * 
	 * @param listener
	 *            the listener to set
	 */
	public void setOnInputTextListener(OnInputTextListener listener) {
		this.mInputTextListener = listener;
	}

	@Override
	public void show(FragmentManager manager, String tag) {
		FragmentTransaction transaction = manager.beginTransaction();
		// 指定一个过渡动画
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		transaction.add(this, tag);
		transaction.commitAllowingStateLoss();// 防止按home键后，show的时候调用父类中的commit方法异常
	}
}
