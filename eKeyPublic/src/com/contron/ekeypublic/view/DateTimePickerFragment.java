/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.view;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.contron.ekeypublic.R;
import com.contron.ekeypublic.util.DateUtil;
import com.lidroid.xutils.util.LogUtils;

import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.Toast;

public class DateTimePickerFragment extends DialogFragment {
	public interface OnDateTimeDoneListener {
		public void onDateTime(int year, int monthOfYear, int dayOfMonth,
				int hourOfDay, int minute, String dateTime);
	}

	private DatePicker datePicker;
	private TimePicker timePicker;
	private Calendar mCalendar;// 记录当前选择时间
	private int mYear, mMonthOfYear, mDayOfMonth, mHourOfDay, mMinute;
	private OnDateTimeDoneListener mOnDateTimeDoneListener;
	private Calendar mPopupCalendar;// 最小时间限制
    private Calendar mMaxCalendar; //最大时间限制
	private boolean limitDate = false;// true 限制当前日期以后的日期

	private DateTimePickerFragment() {
	}

	public static DateTimePickerFragment newInstance(
			OnDateTimeDoneListener onDateTimeListener, boolean limitDate) {
		DateTimePickerFragment fragment = new DateTimePickerFragment();
		fragment.mOnDateTimeDoneListener = onDateTimeListener;
		fragment.limitDate = limitDate;
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (mPopupCalendar == null)
			mPopupCalendar = Calendar.getInstance();// 弹出对话框时时间
		if (mCalendar == null)
			mCalendar = Calendar.getInstance();// 记录当前选择时间
	}

	public void setInitDateTime(String initDateTime, String maxDateTime) {
		mPopupCalendar = getCalendarByInintData(initDateTime);
		mCalendar = getCalendarByInintData(initDateTime);
        if (maxDateTime != null && maxDateTime != "") {
            mMaxCalendar = getCalendarByInintData(maxDateTime);
        }
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		LinearLayout view = new LinearLayout(getActivity());
		view.setOrientation(LinearLayout.VERTICAL);
		// view.setPadding(10, 0, 10, 0);
		LinearLayout pickerView = new LinearLayout(getActivity());
		pickerView.setGravity(Gravity.CENTER_HORIZONTAL);// 居中
		// 初始化日期控件
		datePicker = new DatePicker(getActivity());
		resizePicker(datePicker);

		datePicker.setSpinnersShown(true);
		datePicker.setCalendarViewShown(false);// 设置为非日历模式
		mYear = mCalendar.get(Calendar.YEAR);
		mMonthOfYear = mCalendar.get(Calendar.MONTH);
		mDayOfMonth = mCalendar.get(Calendar.DAY_OF_MONTH);
		datePicker.init(mYear, mMonthOfYear, mDayOfMonth,
				new DatePicker.OnDateChangedListener() {

					@Override
					public void onDateChanged(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						if (limitDate) {
							Calendar end = Calendar.getInstance();
							end.set(year, monthOfYear, dayOfMonth, mHourOfDay, mMinute);

							if (end.before(mPopupCalendar)) {
								datePicker.updateDate(mCalendar.get(Calendar.YEAR),
										mCalendar.get(Calendar.MONTH),
										mCalendar.get(Calendar.DAY_OF_MONTH));
								return;
							}
                            if (mMaxCalendar != null && end.after(mMaxCalendar)) {
                                datePicker.updateDate(mCalendar.get(Calendar.YEAR),
                                                      mCalendar.get(Calendar.MONTH),
                                                      mCalendar.get(Calendar.DAY_OF_MONTH));
                                return;
                            }
						}
						updateDate(year, monthOfYear, dayOfMonth);
						updateTitle();
					}

					private void updateDate(int year, int monthOfYear,
							int dayOfMonth) {
						mYear = year;
						mMonthOfYear = monthOfYear;
						mDayOfMonth = dayOfMonth;
						mCalendar.set(Calendar.YEAR, mYear);
						mCalendar.set(Calendar.MONDAY, mMonthOfYear);
						mCalendar.set(Calendar.DAY_OF_MONTH, mDayOfMonth);
					}
				});
		pickerView.addView(datePicker);
		// 初始化时间控件
		timePicker = new TimePicker(getActivity());
		resizePicker(timePicker);

		mHourOfDay = mCalendar.get(Calendar.HOUR_OF_DAY);
		mMinute = mCalendar.get(Calendar.MINUTE);
		timePicker.setIs24HourView(true);
		timePicker.setCurrentHour(mHourOfDay);
		timePicker.setCurrentMinute(mMinute);
		timePicker
				.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {

					@Override
					public void onTimeChanged(TimePicker view, int hourOfDay,
							int minute) {
						if (limitDate) {
							Calendar end = Calendar.getInstance();
							end.set(mYear, mMonthOfYear, mDayOfMonth, hourOfDay, minute);

							if (end.before(mPopupCalendar)) {
								timePicker.setCurrentHour(mCalendar.get(Calendar.HOUR_OF_DAY));
								timePicker.setCurrentMinute(mCalendar.get(Calendar.MINUTE));
								return;
							}
                            if (mMaxCalendar != null && end.after(mMaxCalendar)) {
                                timePicker.setCurrentHour(mCalendar.get(Calendar.HOUR_OF_DAY));
                                timePicker.setCurrentMinute(mCalendar.get(Calendar.MINUTE));
                                return;
                            }
						}
						updateTime(hourOfDay, minute);
						updateTitle();
					}

					private void updateTime(int hourOfDay, int minute) {
						mHourOfDay = hourOfDay;
						mMinute = minute;
						mCalendar.set(Calendar.HOUR_OF_DAY, mHourOfDay);
						mCalendar.set(Calendar.MINUTE, mMinute);
					}
				});
		pickerView.addView(timePicker);
		view.addView(pickerView);

		LinearLayout btnView = new LinearLayout(getActivity());
		LinearLayout.LayoutParams btnViewParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT, 1);
		// 初始化取消按钮
		Button btnNegative = new Button(getActivity());
		btnNegative.setLayoutParams(btnViewParams);
		btnNegative.setText(android.R.string.cancel);
		btnNegative.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		btnView.addView(btnNegative);
		// 初始化确定按钮
		Button btnPositive = new Button(getActivity());
		btnPositive.setLayoutParams(btnViewParams);
		btnPositive.setText(android.R.string.ok);
		btnPositive.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (mOnDateTimeDoneListener != null)
					mOnDateTimeDoneListener.onDateTime(mYear, mMonthOfYear,
							mDayOfMonth, mHourOfDay, mMinute,
							// 12小时制=hh; 24小时制=kk
							DateFormat.format("yyyy-MM-dd HH:mm:ss", mCalendar)
									.toString());
				dismiss();
			}
		});
		btnView.addView(btnPositive);

		view.addView(btnView);
		return view;
	}

	private void resizePicker(ViewGroup viewGroup) {
		int count = viewGroup.getChildCount();
		View childAt = null;
		if (viewGroup != childAt)
			for (int i = 0; i < count; i++) {
				childAt = viewGroup.getChildAt(i);
				if (childAt instanceof NumberPicker) {
					NumberPicker numPicker = (NumberPicker) childAt;
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
							pixelsToDip(getActivity(), 60),
							LinearLayout.LayoutParams.WRAP_CONTENT);
					params.leftMargin = 0;
					params.rightMargin = 5;
					numPicker.setLayoutParams(params);
				} else if (childAt instanceof LinearLayout) {
					resizePicker((ViewGroup) childAt);// 递归查找
				}
			}
	}

	private int pixelsToDip(Context context, int pixels) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				pixels, getResources().getDisplayMetrics());
	}

	private void updateTitle() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String dateStr = sdf.format(mCalendar.getTime());
		getDialog().setTitle(dateStr);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		updateTitle();
	}

	/**
	 * 实现将初始日期时间2012年07月02日 16:45 拆分成年 月 日 时 分 秒,并赋值给calendar
	 *
	 * @param initDateTime
	 *            初始日期时间值 字符串型
	 * @return Calendar
	 */
	private Calendar getCalendarByInintData(String initDateTime) {
		Calendar calendar = Calendar.getInstance();

		// 将初始日期时间2012年07月02日 16:45 拆分成年 月 日 时 分 秒
		String date = spliteString(initDateTime, " ", "index", "front"); // 日期
		String time = spliteString(initDateTime, " ", "index", "back"); // 时间

		String yearStr = spliteString(date, "-", "index", "front"); // 年份
		String monthAndDay = spliteString(date, "-", "index", "back"); // 月日

		String monthStr = spliteString(monthAndDay, "-", "index", "front"); // 月
		String dayStr = spliteString(monthAndDay, "-", "index", "back"); // 日

		String hourStr = spliteString(time, ":", "index", "front"); // 时
		String minuteAndSecond = spliteString(time, ":", "index", "back"); // 分秒

		String minuteStr = spliteString(minuteAndSecond, ":", "index", "front"); // 分
		String secondStr = spliteString(minuteAndSecond, ":", "index", "back"); // 秒

		int currentYear = Integer.valueOf(yearStr.trim()).intValue();
		int currentMonth = Integer.valueOf(monthStr.trim()).intValue() - 1;
		int currentDay = Integer.valueOf(dayStr.trim()).intValue();
		int currentHour = Integer.valueOf(hourStr.trim()).intValue();
		int currentMinute = Integer.valueOf(minuteStr.trim()).intValue();
		int currentSecond = Integer.valueOf(secondStr.trim()).intValue();

		calendar.set(currentYear, currentMonth, currentDay, currentHour,
				currentMinute, currentSecond);
		return calendar;
	}

	/**
	 * 截取子串
	 *
	 * @param srcStr
	 *            源串
	 * @param pattern
	 *            匹配模式
	 * @param indexOrLast
	 * @param frontOrBack
	 * @return
	 */
	public static String spliteString(String srcStr, String pattern,
									  String indexOrLast, String frontOrBack) {
		String result = "";
		int loc = -1;
		if (indexOrLast.equalsIgnoreCase("index")) {
			loc = srcStr.indexOf(pattern); // 取得字符串第一次出现的位置
		} else {
			loc = srcStr.lastIndexOf(pattern); // 最后一个匹配串的位置
		}
		if (frontOrBack.equalsIgnoreCase("front")) {
			if (loc != -1)
				result = srcStr.substring(0, loc); // 截取子串
		} else {
			if (loc != -1)
				result = srcStr.substring(loc + 1, srcStr.length()); // 截取子串
		}
		return result;
	}

}
