/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Layout;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Google自家下拉刷新 SwipeRefreshLayout 只能有一个
 * child，并是可滚动的view即可，如ScrollView或者ListView
 * 
 * @author hupei
 * @date 2015年7月31日 上午9:05:42
 */
public class SwipeRefreshListFragment extends Fragment {
	private SwipeRefreshLayout mSwipeRefreshLayout;
	private ListView mListView;
	private TextView mEmptyView;

	protected View loadMoreView;// 下一页视图
	protected RadioButton loadMoreButton;// 下一页按钮
	protected int mcurrentItem = 0;// 当前页数
	protected int mItemCount = 1;// 总页数

	private final AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			onListItemClick((ListView) parent, view, position, id);
		}
	};

	private final SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {

		@Override
		public void onRefresh() {
			onListRefresh();
		}
	};

	@Override
	public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		mSwipeRefreshLayout = new SwipeRefreshLayout(getActivity());
		final FrameLayout frameLayout = new FrameLayout(getActivity());

		mListView = new ListView(getActivity());
		mListView.setId(android.R.id.list);
		mListView.setDrawSelectorOnTop(false);
		frameLayout.addView(mListView, params);

		mEmptyView = new TextView(getActivity());
		mEmptyView.setId(android.R.id.empty);
		mEmptyView.setTextAppearance(getActivity(), android.R.attr.textAppearanceSmall);
		mEmptyView.setGravity(Gravity.CENTER);
		mEmptyView.setVisibility(View.GONE);
		frameLayout.addView(mEmptyView, params);

		mSwipeRefreshLayout.addView(frameLayout, params);

		createLoadMoreView(inflater);

		return mSwipeRefreshLayout;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
		// 设置刷新时动画的颜色，可以设置4个
		mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_light, android.R.color.holo_red_light,
				android.R.color.holo_orange_light, android.R.color.holo_green_light);

		// 首次加载显示下拉动画
		mSwipeRefreshLayout.setProgressViewOffset(false, 0,
		// 24数值转换为24dip
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
		setRefreshing(true);
	}

	/**
	 * 适配器设置数据源
	 * 
	 * @author hupei
	 * @date 2015年7月31日 上午9:06:14
	 * @param adapter
	 */
	public void setListAdapter(ListAdapter adapter) {
		mListView.setAdapter(adapter);
		mListView.setVisibility(View.VISIBLE);
		mListView.setOnScrollListener(new SwpipeListViewOnScrollListener(mSwipeRefreshLayout));
		mListView.setOnItemClickListener(mOnItemClickListener);
	}

	/**
	 * 启停刷新
	 * 
	 * @author hupei
	 * @date 2015年7月31日 上午9:06:27
	 * @param refreshing
	 */
	public void setRefreshing(boolean refreshing) {
		mSwipeRefreshLayout.setRefreshing(refreshing);
		if (refreshing)
			onListRefresh();
	}

	/**
	 * ListView 空数据时，显示字符
	 * 
	 * @author hupei
	 * @date 2015年7月31日 上午9:06:57
	 * @param text
	 */
	public void setEmptyText(CharSequence text) {
		mEmptyView.setText(text);
		setEmptyViewShown(true);
	}

	/**
	 * 设置空数据提示语 View 是否显示
	 * 
	 * @author hupei
	 * @date 2015年7月31日 上午9:07:25
	 * @param shown
	 */
	public void setEmptyViewShown(boolean shown) {
		/*
		 * ListView不能设置隐藏，否则下拉刷新有问题，因为 SwipeRefreshLayout 必须有滚动的view。
		 * mListView.setVisibility(!shown ? View.VISIBLE : View.GONE);
		 */
		mEmptyView.setVisibility(shown ? View.VISIBLE : View.GONE);
	}

	/**
	 * 下拉刷新
	 * 
	 * @author hupei
	 * @date 2015年7月30日 下午3:26:40
	 */
	public void onListRefresh() {

	}

	/**
	 * ListView Item 点击事件
	 * 
	 * @author hupei
	 * @date 2015年7月31日 上午9:08:08
	 * @param l
	 * @param v
	 * @param position
	 * @param id
	 */
	public void onListItemClick(ListView l, View v, int position, long id) {
	}

	public SwipeRefreshLayout getSwipeRefreshLayout() {
		return mSwipeRefreshLayout;
	}

	public ListView getListView() {
		return mListView;
	}

	/**
	 * SwipeRefreshLayout结合ListView使用的时候有时候存在下拉冲突
	 * 结果当第一个item长度超过一屏，明明还没有到达列表顶部，Scroll事件就被拦截，列表无法滚动，同时启动了刷新。
	 * 
	 * @author hupei
	 * @date 2015年7月30日 下午2:35:12
	 */
	final class SwpipeListViewOnScrollListener implements AbsListView.OnScrollListener {
		private SwipeRefreshLayout mSwipeView;
		private AbsListView.OnScrollListener mOnScrollListener;

		public SwpipeListViewOnScrollListener(SwipeRefreshLayout swipeView) {
			mSwipeView = swipeView;
		}

		public SwpipeListViewOnScrollListener(SwipeRefreshLayout swipeView, OnScrollListener onScrollListener) {
			mSwipeView = swipeView;
			mOnScrollListener = onScrollListener;
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {

		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			final View firstView = view.getChildAt(firstVisibleItem);
			// 当firstVisibleItem是第0位。如果firstView==null说明列表为空，需要刷新;或者top==0说明已经到达列表顶部,
			// 也需要刷新
			if (firstVisibleItem == 0 && (firstView == null || firstView.getTop() == 0)) {
				mSwipeView.setEnabled(true);
			} else {
				mSwipeView.setEnabled(false);
			}
			if (null != mOnScrollListener) {
				mOnScrollListener.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
			}
		}
	}

	protected void createLoadMoreView(LayoutInflater inflater) {

	}

	protected void setLoadMoreView() {
		mListView.addFooterView(loadMoreView); // 设置列表底部视图
	}
}
