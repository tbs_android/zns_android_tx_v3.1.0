package com.contron.ekeypublic.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ListAdapter;

/**
 * 对话框
 * 
 * @author hupei
 * @date 2014年12月19日 下午2:59:14
 */
public class MsgBoxFragment extends DialogFragment {
	private String title;
	private String message;
	private DialogInterface.OnClickListener okListener;
	private DialogInterface.OnClickListener noListener;
	private String[] items;
	private boolean isTouchOutside;
	private ListAdapter adapter;
	private DialogInterface.OnClickListener itemsListener;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		if (!TextUtils.isEmpty(title))
			builder.setTitle(title);
		if (!TextUtils.isEmpty(message))
			builder.setMessage(message);
		if (items != null) {
			builder.setItems(items, itemsListener);
		}
		if (adapter != null)
			builder.setAdapter(adapter, itemsListener);
		if (okListener != null)
			builder.setPositiveButton(android.R.string.ok, okListener);
		if (noListener != null)
			builder.setNegativeButton(android.R.string.cancel, noListener);
		AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(isTouchOutside);
		return dialog;
	}

	public MsgBoxFragment setCanceledOnTouchOutside(boolean cancel) {
		this.isTouchOutside = cancel;
		return this;
	}

	public MsgBoxFragment setTitle(String title) {
		this.title = title;
		return this;
	}

	public MsgBoxFragment setMessage(String message) {
		this.message = message;
		return this;
	}

	public MsgBoxFragment setAdapter(ListAdapter adapter,
			DialogInterface.OnClickListener listener) {
		this.adapter = adapter;
		this.itemsListener = listener;
		return this;
	}

	public MsgBoxFragment setItems(String[] items,
			DialogInterface.OnClickListener listener) {
		this.items = items;
		this.itemsListener = listener;
		return this;
	}

	/**
	 * 确定事件
	 * 
	 * @param listener
	 *            the listener to set
	 */
	public MsgBoxFragment setPositiveOnClickListener(
			DialogInterface.OnClickListener listener) {
		this.okListener = listener;
		return this;
	}

	/**
	 * 取消事件
	 * 
	 * @param listener
	 *            the listener to set
	 */
	public MsgBoxFragment setNegativeOnClickListener(
			DialogInterface.OnClickListener listener) {
		this.noListener = listener;
		return this;
	}

	@Override
	public void show(FragmentManager manager, String tag) {
		FragmentTransaction transaction = manager.beginTransaction();
		// 指定一个过渡动画
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		if (!this.isAdded())// 先判断是否被add过
			transaction.add(this, tag);
		transaction.commitAllowingStateLoss();// 防止按home键后，show的时候调用父类中的commit方法异常
	}
}
