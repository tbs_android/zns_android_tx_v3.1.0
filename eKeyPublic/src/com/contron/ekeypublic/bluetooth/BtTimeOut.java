/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth;

/**
 * 发送数据超时
 * 
 * @author hupei
 * @date 2015年9月22日 上午11:44:00
 */
public class BtTimeOut {
	private int sendRepeat;// 重发次数 收到应答时（默认超时时间3秒），应重新发送，连续三遍无应答，判为超时错误
	private int timeout;// 3秒超时，单位秒

	public int getSendRepeat() {
		return sendRepeat;
	}

	public int getTimeout() {
		return timeout;
	}

	public void clearData() {
		sendRepeat = 0;
		timeout = 0;
	}

	public void addTimeOut() {
		timeout++;// 每次+1秒
	}

	public void addSendRepeat() {
		sendRepeat++;// 增加一次发送
	}

	public boolean isFinish() {
		// 重复3次3秒则为超时
		return sendRepeat == 3 && timeout == 9;
	}

	public boolean isSend() {
		// 第一次马上发送，或每3秒后重新发送
		return sendRepeat == 0 || timeout % 3 == 0;
	}
}