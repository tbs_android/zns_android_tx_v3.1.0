/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth;

public enum BtState {
	/** 连接中 */
	CONNECTING,
	/** 连接成功 */
	CONNECTED,
	/** 连接断开 */
	DISCONNECTED,
	/** 发现设备，可发送蓝牙命令了 */
	DISCOVERED,
	/** 连接超时 */
	CONNECTOUTTIME;
}
