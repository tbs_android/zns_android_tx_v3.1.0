/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth;

/**
 * 蓝牙自连
 * 
 * @author hupei
 * @date 2015年9月22日 上午11:40:08
 */
public class BtConnAuto {
	public int connRepeat;// 连接次数

	public BtConnAuto() {
	}

	public BtConnAuto(int connRepeat) {
		this.connRepeat = connRepeat;
	}

	public int getConnRepeat() {
		return connRepeat;
	}

	/**
	 * 清0连接次数
	 * 
	 * @author hupei
	 * @date 2015年10月9日 下午2:20:17
	 */
	public void clearConnRepeat() {
		this.connRepeat = 0;
	}

	public boolean isConn() {
		return connRepeat <= 3;
	}

	public void addConnRepeat() {
		connRepeat++;
	}

	/**
	 * 3次连接完成
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午11:25:39
	 * @return
	 */
	public boolean isConnFinish() {
		return connRepeat == 3;
	}
}