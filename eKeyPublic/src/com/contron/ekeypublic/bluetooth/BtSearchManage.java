/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.TextUtils;

import com.contron.ekeypublic.R;
import com.contron.ekeypublic.adapter.ContronAdapter;
import com.contron.ekeypublic.adapter.ContronViewHolder;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.view.MsgBoxFragment;
import com.lidroid.xutils.util.LogUtils;

/**
 * 蓝牙搜索管理
 * 
 * @author hupei
 * @date 2015年9月22日 上午8:42:37
 */
public class BtSearchManage {
	public interface OnSelectBtKeyListener {
		public void onSelectBtKey(Smartkey btKey);
	}

	private Activity mActivity;
	private BluetoothAdapter mBluetoothAdapter;
	private OnSelectBtKeyListener mOnSelectBtKeyListener;

	private boolean mScanning;

	private Handler mHandler = new Handler();
	private MsgBoxFragment btDeviceDialog;// 蓝牙搜索结果设备框
	private ContronAdapter<Smartkey> mAdapter;
	private EkeyDao mEkeyDao;
	List<Smartkey> smartkeys;

	public List<Smartkey> getSmartkeys() {
		return smartkeys;
	}

	public void setSmartkeys(List<Smartkey> smartkeys) {
		this.smartkeys = smartkeys;
	}

	public boolean ismScanning() {
		return mScanning;
	}

	public void setmScanning(boolean mScanning) {
		this.mScanning = mScanning;
	}

	public BtSearchManage(Activity activity, BluetoothAdapter bluetoothAdapter,
			OnSelectBtKeyListener listener) {
		this.mActivity = activity;
		this.mBluetoothAdapter = bluetoothAdapter;
		this.mOnSelectBtKeyListener = listener;
		mEkeyDao = DBHelp.getInstance(mActivity).getEkeyDao();
		// 初始蓝牙相关
		mAdapter = new ContronAdapter<Smartkey>(activity,
				android.R.layout.simple_list_item_2, new ArrayList<Smartkey>()) {

			@Override
			public void setViewValue(ContronViewHolder viewHolder,
					Smartkey item, int position) {
				viewHolder.setText(android.R.id.text1, item.getName()).setText(
						android.R.id.text2, item.getBtaddr());
			}
		};
		btDeviceDialog = new MsgBoxFragment();
		btDeviceDialog.setCancelable(false);
		btDeviceDialog
				.setNegativeOnClickListener(new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO 取消搜索
						scanLeDevice(false);
					}
				});
		btDeviceDialog.setTitle(mActivity.getResources().getString(
				R.string.main_bluetooth_menu));

		btDeviceDialog.setAdapter(mAdapter,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO 选择蓝牙后
						if (mOnSelectBtKeyListener != null)
							mOnSelectBtKeyListener.onSelectBtKey(mAdapter
									.getDatas().get(which));
						scanLeDevice(false);
						mHandler.removeCallbacksAndMessages(null);
					}
				});
	}

	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, final int rssi,
				final byte[] scanRecord) {
			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					// 搜索蓝牙结果
					// LogUtils.i("device=" + device.getAddress());
					String btAddr = device.getAddress().replace(":", "");
					// 搜索蓝牙锁地址时过滤智能钥匙
					Smartkey btKeyFromDB = mEkeyDao.findFirstBtKey(btAddr);
					Smartkey btKey = btKeyFromDB != null ? btKeyFromDB : findKey(btAddr);
					if (btKey != null && !mAdapter.getDatas().contains(btKey))
						mAdapter.addData(btKey);

				}
			});
		}
	};

	private Smartkey findKey(String btAddr) {
		if (smartkeys != null) {
			for (Smartkey smartkey : smartkeys) {
				if (TextUtils.equals(smartkey.getBtaddr(), btAddr))
					return smartkey;
			}
		}
		return null;
	}

	/**
	 * 开始与停止搜索
	 * 
	 * @author hupei
	 * @date 2015年5月21日 下午3:21:49
	 * @param enable
	 *            true 开始 false 停止
	 */
	public void scanLeDevice(final boolean enable) {
		if (enable && !mScanning) {
			mAdapter.setEmpty();// 每次都清空
			btDeviceDialog.show(mActivity.getFragmentManager(), "bt");
			// 10秒后停止扫描
			mHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (mScanning) {
						mScanning = false;
						mBluetoothAdapter.stopLeScan(mLeScanCallback);
					}
				}
			}, 10 * 1000);

			mScanning = true;
			// F000E0FF-0451-4000-B000-000000000000
			mBluetoothAdapter.startLeScan(mLeScanCallback);
		} else {
			mScanning = false;
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
		}
	}

}
