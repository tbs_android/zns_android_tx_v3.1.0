package com.contron.ekeypublic.bluetooth;

import java.io.UnsupportedEncodingException;

import android.annotation.SuppressLint;

/**
 * 16进制值与String/Byte之间的转换
 */
@SuppressLint("DefaultLocale")
public class BtMsgConver {
	// 用于建立十六进制字符的输出的大写字符数组
		private static final char[] DIGITS_UPPER = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D',
				'E', 'F' };

		/**
		 * 高低位调之后，再十六进制转十进制
		 * 
		 * @author hupei
		 * @date 2015年6月3日 上午11:28:14
		 * @param hex
		 * @return
		 */
		public static int hexToInt(String hex) {
			byte[] byteLittle = hexStrToByteLittle(hex);
			String string = bytesToHexStr(byteLittle);
			return Integer.parseInt(string, 16);
		}
		
		/**
		 * 十六进制转十进制
		 * 
		 * @author hupei
		 * @date 2015年6月3日 上午11:28:14
		 * @param hex
		 * @return
		 */
		public static int hexToInts(String hex) {
//			byte[] byteLittle = hexStrToByte(hex);
//			String string = bytesToHexStr(byteLittle);
			return Integer.parseInt(hex, 16);
		}

		/**
		 * 十进制转十六进制，再高低位调换
		 * 
		 * @author hupei
		 * @date 2015年6月1日 下午12:53:57
		 * @param i
		 *            如：2015=7DF，调换后DF7
		 * @return
		 */
		public static String toHexString(int i) {
			String hex = Integer.toHexString(i).toUpperCase();
			if (hex.length() % 2 != 0) {
				hex = "0" + hex;
			}
			// 高底调
			byte[] bytes = hexStrToByteLittle(hex);
			return bytesToHexStr(bytes);
		}

		/**
		 * 字符串转换成十六进制字符串
		 * 
		 * @param String
		 *            str 待转换的ASCII字符串
		 * @return String 每个Byte之间无分隔，如: [616C6B]
		 */
		public static String strToHexStr(String str) {
			StringBuilder sb = new StringBuilder();
			try {
				byte[] bs = str.getBytes("GBK");
				int bit;
				for (int i = 0; i < bs.length; i++) {
					bit = (bs[i] & 0x0f0) >> 4;
					sb.append(DIGITS_UPPER[bit]);
					bit = bs[i] & 0x0f;
					sb.append(DIGITS_UPPER[bit]);
				}
				return sb.toString();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			return sb.toString();
		}

		/**
		 * 十六进制字符串计算 异或和校验
		 * 
		 * @param hexStr
		 * @return
		 */
		public static String calcCheckSum(String hexStr) {
			String resStr = "";
			char[] chars = hexStr.toCharArray();
			int msgLength = chars.length;
			if (msgLength % 2 != 0)// 十六进制规则判断必须2位陪数
				return "";
			// 赋初始值与前二位异或，其结果保持与前二位样。
			String binarStr = "00000000";
			for (int i = 0; i < msgLength; i += 2) {// 遍历十六进制，每二位取出
				byte[] temp = binarStr.getBytes();// 取出字节码
				binarStr = "";
				String str = chars[i] + "" + chars[i + 1];// 拼接
				// 转换十六进制为二进制
				byte[] bytes = hexStrToBinaryStr(str).getBytes();
				for (int j = 0; j < 8; j++) {// 将二进制数逐个单字节异或
					binarStr += temp[j] ^= bytes[j];
				}
			}
			// 将最终的二进制转十六进制
			resStr = Integer.toHexString(Integer.valueOf(binarStr, 2).intValue()).toString();
			return resStr.toUpperCase();
		}

		/**
		 * 十六进制字符串转二进制字符串
		 * 
		 * @param hexStr
		 * @return
		 */
		public static String hexStrToBinaryStr(String hexStr) {
			if (hexStr == null || hexStr.length() % 2 != 0)
				return null;
			String bString = "", tmp;
			for (int i = 0; i < hexStr.length(); i++) {
				tmp = "0000" + Integer.toBinaryString(Integer.parseInt(hexStr.substring(i, i + 1), 16));
				bString += tmp.substring(tmp.length() - 4);
			}
			return bString;
		}

		/**
		 * 字节数组转换为十六进制字符串 (高低调换)
		 * 
		 * @param data
		 * @return
		 */
		public static String bytesToHexStrLittle(byte[] data) {
			int len = data.length;
			// 高低调换
			byte b;
			byte temp[] = new byte[len];
			for (int i = 0; i < len; i++) {
				b = data[i];
				temp[len - i - 1] = b;
			}
			String stmp = "";
			StringBuilder sb = new StringBuilder("");
			for (int n = 0; n < len; n++) {
				stmp = Integer.toHexString(temp[n] & 0xFF);
				sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
			}
			return sb.toString().toUpperCase().trim();
		}

		/**
		 * 字节数组转换为十六进制字符串
		 * 
		 * @param data
		 * @return 无分隔符 如:[616C6B]
		 */
		public static String bytesToHexStr(byte[] data) {
			int len = data.length;
			String stmp = "";
			StringBuilder sb = new StringBuilder("");
			for (int n = 0; n < len; n++) {
				stmp = Integer.toHexString(data[n] & 0xFF);
				sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
			}
			return sb.toString().toUpperCase().trim();
		}

		/**
		 * 十六进制字符串转换为字节数组
		 * 
		 * @param String
		 *            str Byte字符串(Byte之间无分隔符 如:[616C6B])
		 * @return String 对应的字符串
		 */
		public static byte[] hexStrToByte(String hexStr) {
			String str = new String(DIGITS_UPPER);
			char[] hexs = hexStr.toCharArray();
			byte[] bytes = new byte[hexStr.length() / 2];
			int n;
			int len = bytes.length;
			for (int i = 0; i < len; i++) {
				n = str.indexOf(hexs[2 * i]) * 16;
				n += str.indexOf(hexs[2 * i + 1]);
				bytes[i] = (byte) (n & 0xff);
			}
			return bytes;
		}

		/**
		 * 十六进制转换为字节数组(高低调换)
		 * 
		 * @param String
		 *            无分隔符 如:[616C6B]
		 * @return String 对应的字符串
		 */
		public static byte[] hexStrToByteLittle(String hexStr) {
			String str = new String(DIGITS_UPPER);
			char[] hexs = hexStr.toCharArray();
			byte[] bytes = new byte[hexStr.length() / 2];
			int n;
			int len = bytes.length;
			for (int i = 0; i < len; i++) {// 6-3-1
				// 高低位调换
				n = str.indexOf(hexs[2 * (len - i) - 1]);// 1 3 5 7 9 11
				n += str.indexOf(hexs[2 * (len - i - 1)]) * 16;// 0 2 4 6 8 10
				bytes[i] = (byte) (n & 0xff);
			}
			return bytes;
		}

		/**
		 * String的字符串转换成unicode的String
		 * 
		 * @param String
		 *            strText 全角字符串
		 * @return String 每个unicode之间无分隔符
		 * @throws Exception
		 */
		public static String strToUnicode(String strText) {
			char c;
			StringBuilder str = new StringBuilder();
			int intAsc;
			String strHex;
			for (int i = 0; i < strText.length(); i++) {
				c = strText.charAt(i);
				intAsc = (int) c;
				strHex = Integer.toHexString(intAsc);
				if (intAsc > 128)
					str.append("\\u" + strHex);
				else
					// 低位在前面补00
					str.append("\\u00" + strHex);
			}
			return str.toString();
		}

		/**
		 * unicode的String转换成String的字符串
		 * 
		 * @param String
		 *            hex 16进制值字符串 （一个unicode为2byte）
		 * @return String 全角字符串
		 */
		public static String unicodeToString(String hex) {
			int t = hex.length() / 6;
			StringBuilder str = new StringBuilder();
			for (int i = 0; i < t; i++) {
				String s = hex.substring(i * 6, (i + 1) * 6);
				// 高位需要补上00再转
				String s1 = s.substring(2, 4) + "00";
				// 低位直接转
				String s2 = s.substring(4);
				// 将16进制的string转为int
				int n = Integer.valueOf(s1, 16) + Integer.valueOf(s2, 16);
				// 将int转换为字符
				char[] chars = Character.toChars(n);
				str.append(new String(chars));
			}
			return str.toString();
		}

		/**
		 * 将单字节转成16进制
		 * 
		 * @param b
		 * @return
		 */
		public static String byteTohex(byte b) {
			StringBuffer buf = new StringBuffer();
			int high = ((b & 0xf0) >> 4);
			int low = (b & 0x0f);
			buf.append(DIGITS_UPPER[high]);
			buf.append(DIGITS_UPPER[low]);
			return buf.toString();
		}

		/**
		 * 合并两个byte数组
		 */
		public static byte[] byteMerger(byte[] byte_1, byte[] byte_2) {
			byte[] byte_3 = new byte[byte_1.length + byte_2.length];
			System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
			System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
			return byte_3;
		}

		/**
		 * 合并byte数组与byte
		 */
		public static byte[] byteMerger(byte[] byte_1, byte byte_2) {
			byte[] src = { byte_2 };
			byte[] byte_3 = new byte[byte_1.length + src.length];
			System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
			System.arraycopy(src, 0, byte_3, byte_1.length, src.length);
			return byte_3;
		}
}