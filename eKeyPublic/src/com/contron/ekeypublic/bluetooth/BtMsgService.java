package com.contron.ekeypublic.bluetooth;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.TextUtils;

import com.contron.ekeypublic.bluetooth.code.BtMsg;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBattMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.bluetooth.code.send.BtSendBattMsg;
import com.contron.ekeypublic.bluetooth.sc.SCBatteryResult;
import com.contron.ekeypublic.bluetooth.sc.SCReceiveBaseResult;
import com.contron.ekeypublic.bluetooth.sc.SCReceiveMsg;
import com.contron.ekeypublic.bluetooth.sc.SCSendMsg;
import com.contron.ekeypublic.eventbus.EventBusManager;
import com.contron.ekeypublic.eventbus.EventBusType;
import com.contron.ekeypublic.eventbus.EventBusType.BtPIO;
import com.lidroid.xutils.util.LogUtils;

/**
 * 蓝牙收发数据服务，通过eventbus框架发出事件
 * 
 * @author hupei
 * @date 2015年4月29日 上午10:38:03
 */
public class BtMsgService extends Service {
	private BluetoothManager mBluetoothManager;
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothGatt mBluetoothGatt;
	private String mBluetoothAddress;

	public final static UUID UUID_NOTIFY = UUID
			.fromString("0000ffe1-0000-1000-8000-00805f9b34fb");
	public final static UUID UUID_SERVICE = UUID
			.fromString("0000ffe0-0000-1000-8000-00805f9b34fb");

	public final static UUID UUID_READ_SC = UUID
			.fromString("0000fff7-0000-1000-8000-00805f9b34fb"); //读 通知 通道
	public final static UUID UUID_NOTIFY_SC = UUID
			.fromString("0000fff6-0000-1000-8000-00805f9b34fb"); //写 通道
	public final static UUID UUID_SERVICE_SC = UUID
			.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
	public final static UUID UUID_CCC = UUID
			.fromString("00002902-0000-1000-8000-00805f9b34fb"); //蓝牙模块通知通道

	private BtState mBtState = BtState.DISCONNECTED;// 连接状态
	private BtConnAuto mBtConnAuto = new BtConnAuto();// 蓝牙自动连接3次后停止
	private BluetoothGattCharacteristic mNotifyCharacteristic;
	private byte[] byteValue;
	private BtTimeOut mBtTimeOut = new BtTimeOut();// 超时
	private Handler mHandler = new Handler();
	private TimeOutRunnable mRunnableTimeOut = new TimeOutRunnable();
	private ConnRunnable mRunnableConn = new ConnRunnable();

	private boolean isSCLock = false; // 是否为四川协议锁
	private byte[] mData = new byte[256]; //四川协议数据
	/**
	 * 重试发送
	 * 
	 * @author hupei
	 * @date 2015年6月10日 上午10:21:38
	 */
	private class TimeOutRunnable implements Runnable {

		@Override
		public void run() {
			LogUtils.i("sendRepeat=" + mBtTimeOut.getSendRepeat()
					+ ">>timeout=" + mBtTimeOut.getTimeout());
			if (mBtState == BtState.DISCONNECTED)// 连接成功才往下走
				return;
			if (mBtTimeOut.isFinish()) {// 重复3次3秒则为超时，并发出广播
				removeCallbacksTimeOut();
				EventBusManager.getInstance().post(mBtTimeOut);
			} else {
				// 第一次马上发送，或每3秒后重新发送
				if (mBtTimeOut.isSend()) {
					if (isSCLock) {
						writeValueRepeatBySiChuan(byteValue);
					}
					else {
						writeValueRepeat(byteValue);
					}

					mBtTimeOut.addSendRepeat();// 增加一次发送
				}
				mBtTimeOut.addTimeOut();// 每次+1秒
				mHandler.postDelayed(this, 1000);
			}
		}
	}

	/**
	 * 断开自动连接
	 * 
	 * @author hupei
	 * @date 2015年6月10日 上午10:22:04
	 */
	private class ConnRunnable implements Runnable {

		@Override
		public void run() {

			EventBusManager.getInstance().post(mBtConnAuto);
			LogUtils.e("mConnRepeat:" + mBtConnAuto.getConnRepeat());

			if (mBtConnAuto.isConn()) {
				connect(mBluetoothAddress);
				mBtConnAuto.addConnRepeat();
			}

			if (mBtConnAuto.isConn())
				// 每5秒自动连接一次蓝牙
				mHandler.postAtTime(this, SystemClock.uptimeMillis() + 5 * 1000);// 5秒发起一次自动连接
			else
				setBtState(BtState.CONNECTOUTTIME);//连接超时
		}
	}

//	/**
//	 * CYG_SCDZCT 写数据
//	 *
//	 * @author pengx
//	 * @date 2017年11月23日 下午23:38:28
//	 * @param byteValue
//	 */
//	public void writeValueBySiChuan(byte[] data) {
//		if (isConnected()) {
//			this.byteValue = data;
//			mBtTimeOut.clearData();
//			mRunnableTimeOut.run();
//		}
//	}

	private void writeValueRepeatBySiChuan(byte[] data) {
		if (mNotifyCharacteristic != null && data != null) {
			int count = 1;
			byte[] sendData = new byte[100];
			byte[] tempData = new byte[20];
			if (19 < data.length) {
				count = (data.length / 19) + 1;
			}
			for (int i = 0; i < count; i++) {
				int length = 19;
				if (1 == count - i)
					length = data.length - (i * 19);
				for (int j = 0; j < tempData.length; j++) {
					tempData[j] = 0;
				}
				tempData[0] = (byte) (i + 1);
				System.arraycopy(data, i * 19, tempData, 1, length);
				System.arraycopy(tempData, 0, sendData, i * 20, tempData.length);

				LogUtils.i("writeValueRepeatBySiChuan " + i + " = " + BtMsgConver.bytesToHexStr(tempData));
			}
			mNotifyCharacteristic.setValue(sendData);
			mBluetoothGatt.writeCharacteristic(mNotifyCharacteristic);
		}
	}

	/**
	 * 写数据
	 * 
	 * @author hupei
	 * @date 2015年6月2日 下午4:44:28
	 * @param byteValue
	 */
	public void writeValue(byte[] byteValue) {
		if (isConnected()) {
			this.byteValue = byteValue;
			mBtTimeOut.clearData();
			mRunnableTimeOut.run();
		}
	}

	public void writeValueString(String value) {
		if (isConnected()) {
			mNotifyCharacteristic.setValue(value);
			if (mBluetoothGatt != null)
				mBluetoothGatt.writeCharacteristic(mNotifyCharacteristic);
		}
	}

	/**
	 * 重复发送，BLE协议规定每次发送最大字节为20
	 * 
	 * @author hupei
	 * @date 2015年6月2日 下午4:40:27
	 */
	private void writeValueRepeat(byte[] byteValue) {
		if (mNotifyCharacteristic != null && byteValue != null) {
			LogUtils.d("传送值大小：" + byteValue.length);
			LogUtils.d("传送值：" + byteValue.toString());
			// 拆包每20字节一组
			byte[] value = byteValue.clone();
			int len = value.length;
			int ceil = (int) Math.ceil((double) len / 20);// 最大值
			for (int i = 0; i < ceil; i++) {
				byte[] bytes = Arrays.copyOfRange(value, i * 20,
						i == ceil - 1 ? len : i * 20 + 20);
				mNotifyCharacteristic.setValue(bytes);
				if (mBluetoothGatt != null)
					mBluetoothGatt.writeCharacteristic(mNotifyCharacteristic);
				else
					LogUtils.e("分帧发送失败");
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 移掉重试发送
	 * 
	 * @author hupei
	 * @date 2015年6月2日 下午4:43:39
	 */
	private void removeCallbacksTimeOut() {
		LogUtils.d("mRunnableTimeOut");
		mHandler.removeCallbacks(mRunnableTimeOut);
	}

	public void lockInfoStart() {
		writeValue(SCSendMsg.getInstance().readLockerFrame());
	}

	public void battStart() {
		if (isSCLock) {
			writeValue(SCSendMsg.getInstance().queryBatteryFrame());
		}
		else {
			writeValue(new BtSendBattMsg().createBtMsg());// 请求电量
		}
	}

	public void findService(List<BluetoothGattService> gattServices) {
		for (BluetoothGattService gattService : gattServices) {
			LogUtils.i("gattService=" + gattService.getUuid().toString());
			if (gattService.getUuid().toString()
					.equalsIgnoreCase(UUID_SERVICE.toString())) {
				List<BluetoothGattCharacteristic> gattCharacteristics = gattService
						.getCharacteristics();
				for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
					LogUtils.i("gattCharacteristic="
							+ gattCharacteristic.getUuid().toString());
					if (gattCharacteristic.getUuid().toString()
							.equalsIgnoreCase(UUID_NOTIFY.toString())) {
						LogUtils.i("可以开始干活了");
						setBtState(BtState.DISCOVERED);// 蓝牙服务连接成功，工作状态
						removeCallbacksConn();// 移动自动连接
						mNotifyCharacteristic = gattCharacteristic;
						setCharacteristicNotification(gattCharacteristic, true);
						return;
					}
				}
			}
			if (gattService.getUuid().toString()
					.equalsIgnoreCase(UUID_SERVICE_SC.toString())) {
				List<BluetoothGattCharacteristic> gattCharacteristics = gattService
						.getCharacteristics();
				for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
					LogUtils.i("gattCharacteristic2="
							+ gattCharacteristic.getUuid().toString());
					if (gattCharacteristic.getUuid().toString()
							.equalsIgnoreCase(UUID_NOTIFY_SC.toString())) {
						LogUtils.i("可以开始干活了");
						setBtState(BtState.DISCOVERED);// 蓝牙服务连接成功，工作状态
						removeCallbacksConn();// 移动自动连接
						mNotifyCharacteristic = gattCharacteristic;
						setCharacteristicNotification(gattCharacteristic, true);
					}
					if (gattCharacteristic.getUuid().toString()
							.equalsIgnoreCase(UUID_READ_SC.toString())) {
						setCharacteristicNotification(gattCharacteristic, true);
					}
				}
			}
		}
	}

	// 通过BLE API的不同类型的回调方法
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		// 当连接状态发生改变
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			LogUtils.i("oldStatus=" + status + " NewStates=" + newState);
			if (status == BluetoothGatt.GATT_SUCCESS) {
				if (newState == BluetoothProfile.STATE_CONNECTED) {// 当蓝牙设备已经连接
					LogUtils.i("蓝牙连接了");
					setBtState(BtState.CONNECTED);
					// 必须调用此方法，onServicesDiscovered 才回调
					mBluetoothGatt.discoverServices();// 搜索连接设备所支持的service。
				} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {// 当蓝牙设备无法连接
					LogUtils.i("蓝牙断开了");
					if (mBluetoothGatt != null) {
						mBluetoothGatt.close();
						mBluetoothGatt = null;
					}
					setBtState(BtState.DISCONNECTED);// 连接断开
					if (!TextUtils.isEmpty(mBluetoothAddress)
							&& mBluetoothAdapter != null
							&& mBluetoothAdapter.isEnabled())// 不是空时才自动连接
						autoConnect(mBluetoothAddress);
				}
			}
		}

		// 发现新服务端
		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			if (status != BluetoothGatt.GATT_SUCCESS) {
				// 没有发新服务端，则关闭蓝牙再开启，并启动连接
				LogUtils.e("onServicesDiscovered received: " + status);
				mBluetoothAdapter.disable();
				try {
					Thread.sleep(2500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				mBluetoothAdapter.enable();
				// if (!TextUtils.isEmpty(mBluetoothAddress) &&
				// mBluetoothAdapter != null && mBluetoothAdapter.isEnabled())//
				// 不是空时才自动连接
				// autoConnect(mBluetoothAddress);
			}
			if (status == BluetoothGatt.GATT_SUCCESS) {
				findService(gatt.getServices());
				LogUtils.e("findService" + gatt.getServices().toString());
			} else {
				if (mBluetoothGatt.getDevice().getUuids() == null)
					LogUtils.e("onServicesDiscovered received: " + status);
			}
		}

		// 读写特性
		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				LogUtils.d("onCharacteristicRead");
			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			// TODO 收到数据
			byte[] data = characteristic.getValue();
			if (data == null || data.length <= 0) {
				return;
			}
			String result = new String(data);
			if (result.startsWith("OK+PIO")) {// 是否为PIO命令，此命令不需要转十六进制字符，直接发送原文
				postData(new BtPIO(result));
			} else {
				result = BtMsgConver.bytesToHexStr(data);
				LogUtils.i("onCharacteristicChanged = " + result);
				if (isSCLock){
					if (data.length <= 7) {
						return;
					}
					System.arraycopy(data, 1, mData, (data[0] - 1) * 19, 19);
					if (mData[2] <= data[0] * 19) {
						SCReceiveMsg receiveMsg = new SCReceiveMsg(mData);
						if (receiveMsg.getRecvType() == 0x05) {
							SCBatteryResult batteryResult = receiveMsg.parseBattery();
							if (batteryResult.ismState()) {
								postData(new EventBusType.BtBatt(batteryResult.battDes(), batteryResult.isBattMin()));
							}
						}
						else {
							postData(new EventBusType.BtHex(BtMsgConver.bytesToHexStr(mData)));
						}
					}
				}
				else {
					if (result.startsWith(BtMsg.HEAD)) {// 报文是同步头开始
						// 如果是校验码正确，则报文是完整的并广播发送
						if (equalsCheckCRC16(data)) {
							BtRecvMsg btRecvMsg = BtMsgTools.recv(result);
							if (btRecvMsg != null
									&& btRecvMsg instanceof BtRecvBattMsg) {// 电量回应
								BtRecvBattMsg recvBattMsg = (BtRecvBattMsg) btRecvMsg;
								recvBattMsg.createDataSource();
								postData(new EventBusType.BtBatt(
										recvBattMsg.strBatt,
										recvBattMsg.isBattMin()));
							} else {
								postData(new EventBusType.BtHex(result));
							}
						} else {// 检验码不正确，报文不完整则组包下一段报文
							sbReceiveData.append(result);
						}
					} else {
						// 组合报文 再次检查校验码，正确为此
						sbReceiveData.append(result);
						byte[] resceiveBytes = BtMsgConver
								.hexStrToByte(sbReceiveData.toString());
						if (equalsCheckCRC16(resceiveBytes)) {
							postData(new EventBusType.BtHex(
									sbReceiveData.toString()));
						}
					}
				}
			}
		}

		/**
		 * 检查CRC校验码，确定报文完整性
		 * 
		 * @author hupei
		 * @date 2015年6月11日 上午11:46:07
		 * @param data
		 * @return
		 */
		private boolean equalsCheckCRC16(byte[] data) {
			int len = data.length;
			// 4个同步头 + 2个校验码，余下内容必须大于1，所以 len 小于 7 则返回 false
			if (len < 7) {
				return false;
			}
			// 取出需要计算校验内容部分：报文长度开始至动态部分结束 = 报文 - 4个同步头 - 2个校验码
			byte[] dataBody = Arrays.copyOfRange(data, 4, len - 2);
			byte[] newCheck = CRC16.checkCRC16toBytes(dataBody);// 计算得到的校验码
			// 取出源报文检验码
			byte[] oldCheck = Arrays.copyOfRange(data, len - 2, len);
			// 比较校验码
			if (Arrays.equals(newCheck, oldCheck))
				return true;
			else
				return false;
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			// TODO 写入数据
			// final byte[] data = characteristic.getValue();
			// if (data != null && data.length > 0) {
			// String result = BtMsgConver.bytesToHexStr(data);
			// LogUtils.i("[" + data.length + "]蓝牙写入数据=" + result);
			// } else {
			// LogUtils.e("蓝牙写入数据失败");
			// }
		}

		@Override
		public void onDescriptorRead(BluetoothGatt gatt,
				BluetoothGattDescriptor bd, int status) {
			LogUtils.d("onDescriptorRead");
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt,
				BluetoothGattDescriptor bd, int status) {
			LogUtils.d("onDescriptorWrite");
		}

		@Override
		public void onReadRemoteRssi(BluetoothGatt gatt, int a, int b) {
			LogUtils.d("onReadRemoteRssi");
		}

		@Override
		public void onReliableWriteCompleted(BluetoothGatt gatt, int a) {
			LogUtils.d("onReliableWriteCompleted");
		}

	};

	private StringBuffer sbReceiveData = new StringBuffer();

	/**
	 * 发送接收数据，并移除超时与 置空 sbReceiveData 数据
	 * 
	 * @author hupei
	 * @date 2015年9月22日 上午10:40:01
	 * @param event
	 */
	protected void postData(Object event) {
		EventBusManager.getInstance().post(event);
		removeCallbacksTimeOut();// 移除超时
		sbReceiveData.setLength(0);
		mData = new byte[256];
	}

	public class LocalBinder extends Binder {
		public BtMsgService getService() {
			return BtMsgService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mBinder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		close();
		LogUtils.e("service onUnbind");
		return super.onUnbind(intent);
	}

	private final IBinder mBinder = new LocalBinder();

	@Override
	public void onDestroy() {
		super.onDestroy();
		LogUtils.e("service onDestroy");
	}

	/**
	 * 初始化本地蓝牙适配器
	 *
	 * @return 初始化成功返回true
	 */
	public boolean initialize() {
		if (mBluetoothManager == null) {
			mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			if (mBluetoothManager == null) {
				LogUtils.e("Unable to initialize BluetoothManager.");
				return false;
			}
		}

		mBluetoothAdapter = mBluetoothManager.getAdapter();
		if (mBluetoothAdapter == null) {
			LogUtils.e("Unable to obtain a BluetoothAdapter.");
			return false;
		}
		if (!mBluetoothAdapter.isEnabled()) {// 强制打开蓝牙
			mBluetoothAdapter.enable();
		}
		return true;
	}

	public synchronized void autoConnect(String address) {
		this.mBluetoothAddress = address;
		setBtState(BtState.CONNECTING);
		LogUtils.e("autoConnect");
		mBtConnAuto.clearConnRepeat();
		mRunnableConn.run();
	}

	/**
	 * 移除自动连接
	 * 
	 * @author hupei
	 * @date 2015年10月9日 下午2:20:05
	 */
	public synchronized void removeCallbacksConn() {
		LogUtils.e("removeCallbacksConn");
		mHandler.removeCallbacks(mRunnableConn);
	}

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 *
	 * @param address
	 *            The device address of the destination device.
	 *
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */
	private synchronized boolean connect(String address) {
		if (mBluetoothAdapter == null || TextUtils.isEmpty(address)) {
			LogUtils.e("蓝牙未初始化或蓝牙地址未指定");
			return false;
		}
		BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
		if (device == null) {
			LogUtils.e("无法找到设备。无法连接。");
			return false;
		}
		// 多次创建gatt连接对象的直接结果是创建过6个以上gatt后就会再也连接不上任何设备，原因应该是android中对BLE限制了同时连接的数量为6个
		// 解决办法是在每一次重新连接时都执行一次gatt.close();关闭上一个连接
		if (mBluetoothGatt != null) {
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}
		// 连接GATT Server。
		mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
		LogUtils.e("创建一个新的蓝牙连接。");
		return true;
	}

	/**
	 * 断开蓝牙.
	 */
	public void disconnect() {
		mBtState = BtState.DISCONNECTED;
		// 必须在此处置 NULL，否则 STATE_DISCONNECTED 状态会启动自动连接 autoConn()方法
		mBluetoothAddress = null;
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			LogUtils.e("蓝牙未初始化");
			return;
		}
		// 非连接状态下，自动连接也开始，所以在此状态下才移除
		if (!isConnected() && !TextUtils.isEmpty(mBluetoothAddress))
			removeCallbacksConn();
		mBluetoothGatt.disconnect();
		LogUtils.e("断开蓝牙");
	}

	/**
	 * 释放资源
	 */
	private void close() {
		LogUtils.e("释放资源");
		if (mBluetoothGatt != null) {
			mBluetoothGatt.disconnect();
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}

	}

	/**
	 * Request a read on a given {@code BluetoothGattCharacteristic}. The read
	 * result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
	 * callback.
	 *
	 * @param characteristic
	 *            The characteristic to read from.
	 */
	public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			LogUtils.w("蓝牙未初始化");
			return;
		}
		mBluetoothGatt.readCharacteristic(characteristic);
	}

	/**
	 * 启用或禁用通知
	 */
	private void setCharacteristicNotification(
			BluetoothGattCharacteristic characteristic, boolean enabled) {
		if (mBluetoothAdapter == null || mBluetoothGatt == null) {
			LogUtils.w("蓝牙未初始化");
			return;
		}
		//开启蓝牙模块消息通知
		mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
		BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID_CCC);
		if (descriptor != null) {
			descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
			mBluetoothGatt.writeDescriptor(descriptor);
		}
	}

	public List<BluetoothGattService> getSupportedGattServices() {
		if (mBluetoothGatt == null)
			return null;
		return mBluetoothGatt.getServices();
	}

	/**
	 * @return 返回本地蓝牙适配器
	 */
	public BluetoothAdapter getBluetoothAdapter() {
		return mBluetoothAdapter;
	}

	public void setBtState(BtState btState) {
		this.mBtState = btState;
		EventBusManager.getInstance().post(btState);
	}

	/**
	 * @return 返回连接状态，连接中 true
	 */
	public boolean isConnectIng() {
		return mBtState == BtState.CONNECTING;
	}

	/**
	 * @return 返回连接状态，成功true
	 */
	public boolean isConnected() {
		return mBtState == BtState.DISCOVERED;
	}

	public boolean isConnRepeatDone() {
		return mBtConnAuto.isConnFinish();
	}

	public void setSCLock(boolean scLock) {
		isSCLock = scLock;
	}
}
