package com.contron.ekeypublic.bluetooth.sc;

/**
 * Created by pengx on 2017/11/28.
 */

public class SCBatteryResult extends SCReceiveBaseResult {
    private int mKeyEnergy;
    private int mKeyType;
    private int mKeyMan;

    public int getmKeyEnergy() {
        return mKeyEnergy;
    }

    public void setmKeyEnergy(int mKeyEnergy) {
        this.mKeyEnergy = mKeyEnergy;
    }

    public int getmKeyType() {
        return mKeyType;
    }

    public void setmKeyType(int mKeyType) {
        this.mKeyType = mKeyType;
    }

    public int getmKeyMan() {
        return mKeyMan;
    }

    public void setmKeyMan(int mKeyMan) {
        this.mKeyMan = mKeyMan;
    }

    public boolean isBattMin() {
        return getmKeyEnergy() < 5;
    }

    public String battDes() {
        String strBatt = getmKeyEnergy() + "%";
        if (isBattMin()) {
            strBatt = "电量不足";
        }
        else if (getmKeyEnergy() >= 100){
            strBatt = "电量满";
        }

        return strBatt;
    }
}
