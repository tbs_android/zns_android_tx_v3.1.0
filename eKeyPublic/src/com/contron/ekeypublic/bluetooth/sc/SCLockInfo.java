package com.contron.ekeypublic.bluetooth.sc;

/**
 * Created by pengx on 2017/11/27.
 */

public class SCLockInfo extends Object {

    //锁状态 0: 未知 1: 开 2: 关
    private int mLockState;
    //锁安装状态 0:未安装 1:已安装
    private int mLockInstallState;
    private byte[] mLockerId;
    private byte[] mSessionKey;

//    private byte[] mOriginalCode = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

    private static SCLockInfo instance = null;

    public static SCLockInfo getInstance() {
        if (instance == null) {
            instance = new SCLockInfo();

            instance.mSessionKey = new byte[]{0, 1, 2, 3, 4, 5, 6, 7};
            instance.mLockState = 0;
        }

        return instance;
    }

    public int getmLockState() {
        return mLockState;
    }

    public void changemLockerState() {
        if (this.mLockState == 0 || this.mLockState == 2) {
            this.mLockState = 1;
        }
        else {
            this.mLockState = 2;
        }
    }

    public int getmLockInstallState() {
        return mLockInstallState;
    }

    public void setmLockInstallState(int mLockInstallState) {
        this.mLockInstallState = mLockInstallState;
    }

    public byte[] getmLockerId() {
        return mLockerId;
    }

    public byte[] getmSessionKey() {
        return mSessionKey;
    }

    public void setmSessionKey(byte[] mSessionKey) {
        this.mSessionKey = mSessionKey;
    }

    public void setLockInfo(int lockState, int lockInstallState, byte[] lockId, byte[] sessionKey) {
        this.mLockState = lockState;
        this.mLockInstallState = lockInstallState;
        this.mLockerId = lockId;
        this.mSessionKey = sessionKey;
    }
}
