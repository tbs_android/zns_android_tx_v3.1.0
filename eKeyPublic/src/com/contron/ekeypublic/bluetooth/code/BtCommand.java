/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code;

/**
 * 命令码枚举
 * 
 * @author hupei
 * @date 2015年4月15日 上午10:51:41
 */
public enum BtCommand {
	CODE_01("01"), CODE_02("02"), CODE_03("03"), CODE_04("04"), CODE_05("05"),CODE_06("06"), CODE_20("20"), CODE_07("07"), CODE_21(
			"21");
	private BtCommand(String value) {
		this.value = value;
	}

	public String value = "";

	/**
	 * 查找枚举
	 * 
	 * @author hupei
	 * @date 2015年6月3日 上午11:47:17
	 * @param arg
	 * @return
	 */
	public static BtCommand valuesOf(String arg) {
		for (BtCommand command : BtCommand.values()) {
			if (command.value.equalsIgnoreCase(arg)) {
				return command;
			}
		}
		return null;
	}
}
