/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code;

import java.util.HashMap;
import java.util.Map;

import com.contron.ekeypublic.bluetooth.code.imp.BtRecvImp;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBackReplyMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecControlUnlockMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvAckMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvBattMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvEmpowerMsg;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;

public final class BtMsgKit {
	private static final Map<String, BtRecvMsg> recvMap = new HashMap<String, BtRecvMsg>();

	static {
		// ACK； key=控制字
		recvMap.put(BtControl.CODE_40.value, new BtRecvAckMsg());
		// 解锁
		recvMap.put(BtControl.CODE_10.value + BtCommand.CODE_20.value, new BtRecvEmpowerMsg());
		recvMap.put(BtControl.CODE_10.value + BtCommand.CODE_21.value, new BtRecControlUnlockMsg());
		recvMap.put(BtControl.CODE_20.value + BtCommand.CODE_21.value, new BtRecControlUnlockMsg());
		recvMap.put(BtControl.CODE_20.value + BtCommand.CODE_07.value, new BtRecvBattMsg());		
		recvMap.put(BtControl.CODE_20.value + BtCommand.CODE_04.value, new BtRecvBackReplyMsg());//任务回传
		recvMap.put(BtControl.CODE_20.value + BtCommand.CODE_05.value, new BtRecvAckMsg());//钥匙任务查询后响应
	}

	public static BtRecvMsg parse(String msg) {
		BtRecvMsg recvMsg = parseError(msg);
		if (recvMsg == null)
			recvMsg = parseData(msg);
		return recvMsg;
	}

	/**
	 * 转换为实体
	 * 
	 * @author hupei
	 * @date 2015年4月29日 上午9:59:56
	 * @param msg
	 *            数据包正文（只有ACK与错误码）
	 * @return
	 */
	private static BtRecvMsg parseError(String msg) {
		// 比较报文头是否一样
		String head = msg.substring(0, 8);
		if (head.equals(BtMsg.HEAD)) {
			final String key = msg.substring(20, 22);// 取出控制字
			if (null != key) {
				BtRecvImp parser = recvMap.get(key);
				if (null != parser) {
					return parser.parserBtMsg(msg);
				}
			}
		}
		return null;
	}

	/**
	 * 转为实体
	 * 
	 * @author hupei
	 * @date 2015年6月3日 下午1:02:29
	 * @param msg
	 *            数据包正文需要解析成结构
	 * @return
	 */
	private static BtRecvMsg parseData(String msg) {
		// 比较报文头是否一样
		String head = msg.substring(0, 8);
		if (head.equals(BtMsg.HEAD)) {
			final String key = msg.substring(20, 24);// 取出控制字+命令码
			if (null != key) {
				BtRecvImp parser = recvMap.get(key);
				if (null != parser) {
					return parser.parserBtMsg(msg);
				}
			}
		}
		return null;
	}
}
