/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code;


/**
 * 功能码枚举
 * 
 * @author hupei
 * @date 2015年4月15日 下午1:41:18
 */
public enum BtFunctionCode {
	CODE_00("00"),
	CODE_01("01"),
	CODE_02("02"),
	CODE_03("03"),
	CODE_04("04"),
	CODE_05("05"),
	CODE_06("06"),
	CODE_07("07"),
	CODE_08("08"),
	CODE_09("09"),
	CODE_10("10"),
	CODE_0A("0A"),
	CODE_0B("0B"),
	CODE_0C("0C"),
	CODE_0D("0D"),
	CODE_0E("0E"),
	CODE_0F("0F"),
	CODE_11("11"),
	CODE_12("12");
	private BtFunctionCode(String value) {
		this.value = value;
	}

	public String value = "";

	/**
	 * 查找枚举
	 * 
	 * @author hupei
	 * @date 2015年6月3日 上午11:47:25
	 * @param arg
	 * @return
	 */
	public static BtFunctionCode valuesOf(String arg) {
		for (BtFunctionCode functionCode : BtFunctionCode.values()) {
			if (functionCode.value.equalsIgnoreCase(arg)) {
				return functionCode;
			}
		}
		return null;
	}
}
