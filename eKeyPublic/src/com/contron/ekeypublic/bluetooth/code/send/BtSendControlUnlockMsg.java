package com.contron.ekeypublic.bluetooth.code.send;

import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtControl;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;

public class BtSendControlUnlockMsg extends BtSendMsg {


	/**
	 * 
	 * @author wangfang
	 * @date 2015年11月2日 下午2:15:57
	 * @param isEnabled
	 *            true 允许操作
	 */
	public BtSendControlUnlockMsg(boolean isEnabled) {
		this.dataSource = isEnabled ? "01" : "00";
		this.control = BtControl.CODE_10.value;
		this.command = BtCommand.CODE_21.value;
		this.functionCode = BtFunctionCode.CODE_00.value;
		createData();
	}

	@Override
	public void createData() {
		// TODO Auto-generated method stub
		if (!this.dataSource.isEmpty())// 判断空
			this.data = this.dataSource;// RFID源文就是十六进制
	}

}
