package com.contron.ekeypublic.bluetooth.code.send;

import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtControl;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;

public class BtSendCheckDoorState extends BtSendMsg {

	@Override
	public void createData() {
		// TODO Auto-generated method stub
		if (!this.dataSource.isEmpty())// 判断空
			this.data = this.dataSource;// RFID源文就是十六进制
	}
	/**
	 * 
	 * @author wangfang
	 * @date 2015年11月2日 下午2:15:57
	 * @param isEnabled
	 *            true 允许操作
	 */
	public BtSendCheckDoorState() {
		this.control = BtControl.CODE_10.value; //(主动发送)
		this.command = BtCommand.CODE_21.value;  
		this.functionCode = BtFunctionCode.CODE_04.value;
		createData();
	}
}
