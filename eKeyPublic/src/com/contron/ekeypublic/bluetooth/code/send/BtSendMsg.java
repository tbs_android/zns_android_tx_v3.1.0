/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code.send;

import android.annotation.SuppressLint;

import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.bluetooth.CRC16;
import com.contron.ekeypublic.bluetooth.code.BtMsg;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.bluetooth.code.imp.BtSendImp;
import com.lidroid.xutils.util.LogUtils;

@SuppressLint("DefaultLocale")
public abstract class BtSendMsg extends BtMsg implements BtSendImp {

	@Override
	public void createDynamicData() {
		this.dynamicData = new StringBuffer(this.command).append(this.functionCode).append(this.data).toString();
	}

	@Override
	public void createLength() {
		int value = this.dynamicData.length() / 2;// 除2来计算个数
		this.length = BtMsgTools.completeAfter(value, "0000");
	}

	@Override
	public void createCheckCode() {
		// 包长度+动态部分
		final StringBuffer hexStr = new StringBuffer(this.length).append(this.sourceAddress).append(this.targetAddress)
				.append(this.control).append(this.dynamicData);
		this.checkCode = CRC16.checkCRC16(BtMsgConver.hexStrToByte(hexStr.toString()));
	}

	@Override
	public byte[] createBtMsg() {
		createDynamicData();// 1：组装动态部分
		createLength();// 2：计算包长度
		createCheckCode();// 3：计算校验CRC16
		this.msg = new StringBuffer(this.head).append(this.length).append(this.sourceAddress)
				.append(this.targetAddress).append(this.control).append(this.dynamicData).append(this.checkCode)
				.toString();
		
		LogUtils.i("send：" + this.msg.replaceAll("(.{2})", "$1 "));
		return BtMsgConver.hexStrToByte(this.msg);
	}
}
