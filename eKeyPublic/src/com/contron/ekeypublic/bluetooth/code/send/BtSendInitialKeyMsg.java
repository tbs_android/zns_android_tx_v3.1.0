package com.contron.ekeypublic.bluetooth.code.send;

import java.util.List;

import android.util.Log;

import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtControl;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.util.DateUtil;

public class BtSendInitialKeyMsg extends BtSendMsg {

	/**
	 * 数据包最大262个字节
	 */
	private static final int DATA_MAX_LEN = 262;

	/**
	 * 用户结构体长度(用户id 2，用户名 19,密码 6 ,rfid 5)共32个字节
	 */
	private static final int USER_STRUCT_LEN = 32;

	/**
	 * 锁ID结构体长度(锁ID 2，锁站号2)4个字节
	 */
	private static final int LOCKER_ID_STRUCT_LEN = 4;

	/**
	 * 锁RFID结构体长度(锁ID)5个字节
	 */
	private static final int LOCKER_RFID_STRUCT_LEN = 5;

	/**
	 * 锁名称结构体长度(锁ID)60个字节
	 */
	private static final int LOCKER_NAME_STRUCT_LEN = 60;

	/**
	 * 锁类型结构体长度(锁ID)1个字节
	 */
	private static final int LOCKER_TYPE_STRUCT_LEN = 1;

	/**
	 * 锁站号结构体长度(锁ID)2个字节
	 */
	private static final int LOCKER_STATION_STRUCT_LEN = 2;

	/**
	 * 锁逻辑结构体长度(锁ID)4个字节
	 */
	private static final int LOCKER_LOGIC_STRUCT_LEN = 4;

	/**
	 * 每帧锁结构体最大总字节：(DATA_MAX_LEN 262)-（命令+功能+帧号+帧锁数量=6）=256
	 */
	private static final int STRUCT_MAX_LEN = DATA_MAX_LEN - 6;

	/**
	 * 每帧最大的用户数量=8个
	 */
	private static final int USER_MAX_COUNT = STRUCT_MAX_LEN / USER_STRUCT_LEN;

	/**
	 * 每帧最大的锁id数量=64个
	 */
	private static final int LOCKER_ID_MAX_COUNT = STRUCT_MAX_LEN
			/ LOCKER_ID_STRUCT_LEN;

	/**
	 * 每帧最大的锁RFID数量=51个
	 */
	private static final int LOCKER_RFID_MAX_COUNT = STRUCT_MAX_LEN
			/ LOCKER_RFID_STRUCT_LEN;

	/**
	 * 每帧最大的锁名称数量=4个
	 */
	private static final int LOCKER_NAME_MAX_COUNT = STRUCT_MAX_LEN
			/ LOCKER_NAME_STRUCT_LEN;

	/**
	 * 每帧最大的锁类型数量=256个
	 */
	private static final int LOCKER_TYPE_MAX_COUNT = STRUCT_MAX_LEN
			/ LOCKER_TYPE_STRUCT_LEN;

	/**
	 * 每帧最大的锁站号数量=128个
	 */
	private static final int LOCKER_STATION_MAX_COUNT = STRUCT_MAX_LEN
			/ LOCKER_STATION_STRUCT_LEN;

	/**
	 * 每帧最大的锁逻辑数量=128个
	 */
	private static final int LOCKER_LOGIC_MAX_COUNT = STRUCT_MAX_LEN
			/ LOCKER_LOGIC_STRUCT_LEN;

	private int lockIdFrameCount;// 锁id总帧数
	private int lockRfidFrameCount;// 锁rfid总帧数
	private int lockNameFrameCount;// 锁name总帧数
	private int lockTypeFrameCount;// 锁type总帧数
	// private int lockStationFrameCount;// 锁station总帧数
	private int lockLogicFrameCount;// 锁logic总帧数
	private int userFrameCount;// 用户总帧数

	private int currentLockIdFrameNO = 1;// 锁id 当前发送帧号，从1开始
	private int currentLockRfidFrameNO = 1;// 锁rfid 当前发送帧号，从1开始
	private int currentLockNameFrameNO = 1;// 锁name 当前发送帧号，从1开始
	private int currentLockTypeFrameNO = 1;// 锁type 当前发送帧号，从1开始
	// private int currentLockStationFrameNO = 1;// 锁station 当前发送帧号，从1开始
	private int currentLockLogicFrameNO = 1;// 锁logic 当前发送帧号，从1开始
	private int currentUserFrameNO = 1;// 用户 当前发送帧号，从1开始

	private int lockerCount = 0;// 锁总量
	private List<Lockset> locksets;// 所有锁
	private List<User> allUser;// 所有用户
	private int userCount = 0;// 用户总量

	/**
	 * 构造方法
	 * 
	 * @param lockFrameCount
	 */
	public BtSendInitialKeyMsg(List<Lockset> locksets, List<User> allUser) {
		this.control = BtControl.CODE_10.value;// 控制码主动发送
		this.locksets = locksets;
		this.allUser = allUser;

		// 用户总量
		userCount = allUser.size();
		// 锁总量
		lockerCount = locksets.size();

		lockIdFrameCount = getFrameCount(lockerCount, LOCKER_ID_MAX_COUNT);
		lockRfidFrameCount = getFrameCount(lockerCount, LOCKER_RFID_MAX_COUNT);
		lockNameFrameCount = getFrameCount(lockerCount, LOCKER_NAME_MAX_COUNT);
		lockTypeFrameCount = getFrameCount(lockerCount, LOCKER_TYPE_MAX_COUNT);
		// lockStationFrameCount = getFrameCount(lockerCount,
		// LOCKER_STATION_MAX_COUNT);
		lockLogicFrameCount = getFrameCount(lockerCount, LOCKER_LOGIC_MAX_COUNT);
		userFrameCount = getFrameCount(userCount, USER_MAX_COUNT);

		Log.i("初始化钥匙数据", "用户总量:" + userCount + "锁总量:" + lockerCount);
		Log.i("初始化钥匙数据", "锁ID总帧:" + lockIdFrameCount);
		Log.i("初始化钥匙数据", "锁RFID总帧:" + lockRfidFrameCount);
		Log.i("初始化钥匙数据", "锁名称总帧:" + lockNameFrameCount);
		Log.i("初始化钥匙数据", "锁类型总帧:" + lockTypeFrameCount);
		Log.i("初始化钥匙数据", "锁验电逻辑帧:" + lockLogicFrameCount);
		Log.i("初始化钥匙数据", "用户总帧:" + userFrameCount);
	}

	/**
	 * 获取总贞数
	 * 
	 * @param Count
	 *            锁数据、用户数据
	 * 
	 * @param maxCount
	 *            一帧最大数量
	 * @return
	 */
	private int getFrameCount(int Count, int maxCount) {

		// 计算总帧
		int frameCount = 0;

		if (Count < maxCount) {
			frameCount = 1;
		} else {
			int temp = (int) (Count / maxCount);
			frameCount = temp * maxCount == Count ? temp : temp + 1;
		}

		return frameCount;

	}

	/**
	 * 获取总帧报文
	 * 
	 * @param functionCode
	 *            功能码
	 * @return
	 */
	public byte[] createTotalFrameMsg(String functionCode) {
		this.command = BtCommand.CODE_02.value;// 命令码02打包钥匙
		this.functionCode = functionCode; // 总帧功能

		int frameCountHex = 0;// 总帧数，双字节

		if (functionCode.equals(BtFunctionCode.CODE_01.value)) {// 打包锁ID 总帧

			frameCountHex = lockIdFrameCount;
			
		} else if (functionCode.equals(BtFunctionCode.CODE_03.value)) {
			frameCountHex = lockRfidFrameCount;// 打包锁RFID
												// 总帧

		} else if (functionCode.equals(BtFunctionCode.CODE_05.value)) {// 打包锁名称
																		// 总帧

			frameCountHex = lockNameFrameCount;

		} else if (functionCode.equals(BtFunctionCode.CODE_07.value)) {// 打包锁类型
																		// 总帧

			frameCountHex = lockTypeFrameCount;

		} else if (functionCode.equals(BtFunctionCode.CODE_09.value)) {// 打包锁站号
																		// 总帧

			// frameCountHex = lockStationFrameCount;

		} else if (functionCode.equals(BtFunctionCode.CODE_0B.value)) {// 打包逻辑关系
																		// 总帧

			frameCountHex = lockLogicFrameCount;

		} else if (functionCode.equals(BtFunctionCode.CODE_0D.value)) {// 打包用户
																		// 总帧

			frameCountHex = userFrameCount;

		}

		this.data = BtMsgTools.completeAfter(frameCountHex, "0000");

		Log.i("初始化钥匙数据", "发送总帧报文-----");
		return createBtMsg();
	}

	/**
	 * 打包钥匙配置信息
	 * 
	 * @return
	 */
	public byte[] createFenFrameMsg(String key_name, String addr) {
		this.command = BtCommand.CODE_02.value;// 命令码02打包钥匙
		String ekeyName = BtMsgConver.strToHexStr(key_name);// 任务名转为十六进制字符
		String packetVar = BtMsgConver.strToHexStr("1.0");// 版本转为十六进制字符
		String superPwd = BtMsgConver.strToHexStr("400030");// 密码转为十六进制字符

		String lock_num = BtMsgTools.completeAfter(lockerCount, "00000000");
		String user_num = BtMsgTools.completeAfter(userCount, "0000");
		String ekey_Name = BtMsgTools.completeAfter(ekeyName,
				"00000000000000000000000000000000");
		String packet_ver = BtMsgTools.completeAfter(packetVar,
				"00000000000000000000000000000000");
		String super_pwd = BtMsgTools.completeAfter(superPwd, "000000000000");

		String bt_addr = BtMsgConver.strToHexStr(addr);

		String phone = BtMsgConver.strToHexStr("13825681580");

		String bt_addrLeteAfter = BtMsgTools.completeAfter(bt_addr,
				"000000000000");
		String phoneLeteAfter = BtMsgTools.completeAfter(phone, "000000000000");

		this.dataSource = lock_num + "" + user_num + ekey_Name + packet_ver
				+ super_pwd + bt_addr + phone;

		this.data = (new StringBuffer().append(lock_num).append(user_num)
				.append(ekey_Name).append(packet_ver).append(super_pwd))
				.append(bt_addrLeteAfter).append(phoneLeteAfter).toString();

		this.functionCode = BtFunctionCode.CODE_00.value;// 打包钥匙配置信息功能码

		Log.i("初始化钥匙数据", "打包钥匙配置信息-----");
		
		return createBtMsg();

	}

	/**
	 * 判断发送分帧是否完成
	 * 
	 * @param functionCode
	 *            分帧功能码
	 * @return true分贞结束
	 */
	public boolean isSendDone(BtFunctionCode functionCode) {

		if (functionCode.value.equals(BtFunctionCode.CODE_00.value)) {
			return true;// 打包钥匙配置信息
		} else if (functionCode.value.equals(BtFunctionCode.CODE_02.value)) {			
			// 取出当前锁量的锁码ID
			return currentLockIdFrameNO > lockIdFrameCount;

		} else if (functionCode.value.equals(BtFunctionCode.CODE_04.value)) {
			// 取出当前锁量的锁RFID
			return currentLockRfidFrameNO > lockRfidFrameCount;
		} else if (functionCode.value.equals(BtFunctionCode.CODE_06.value)) {
			// 取出当前锁量的锁名称
			return currentLockNameFrameNO > lockNameFrameCount;
		} else if (functionCode.value.equals(BtFunctionCode.CODE_08.value)) {
			// 取出当前锁量的锁类型
			return currentLockTypeFrameNO > lockTypeFrameCount;
		}
		// else if (functionCode.value.equals(BtFunctionCode.CODE_0A.value)) {
		// 取出当前锁量的锁站名
		// return currentLockStationFrameNO > lockStationFrameCount;
		// }
		else if (functionCode.value.equals(BtFunctionCode.CODE_0C.value)) {
			// 取出当前锁量的锁逻辑
			return currentLockLogicFrameNO > lockLogicFrameCount;
		} else if (functionCode.value.equals(BtFunctionCode.CODE_0E.value)) {
			// 取出当前用户
			return currentUserFrameNO > userFrameCount;
		} else {
			return true;
		}
	}

	/**
	 * 创建锁、用户分帧报文
	 * 
	 * @param functionCode
	 *            功能码
	 * @return 组合后报文
	 */
	public byte[] createFenFrameMsg(BtFunctionCode functionCode) {
		this.command = BtCommand.CODE_02.value;// 命令码02打包钥匙
		this.functionCode = functionCode.value; // 功能码

		if (functionCode.value.equals(BtFunctionCode.CODE_02.value)) {
			
			Log.i("初始化钥匙数据", "发送分帧报文锁码ID-----当前帧："+currentLockIdFrameNO);			
			
			// 取出当前锁量的锁码ID
			toData(currentLockIdFrameNO, lockerCount, lockIdFrameCount,
					LOCKER_ID_MAX_COUNT, functionCode);
			currentLockIdFrameNO++;

		} else if (functionCode.value.equals(BtFunctionCode.CODE_04.value)) {
			
			Log.i("初始化钥匙数据", "发送分帧报文锁RFID-----当前帧："+currentLockRfidFrameNO);		
			
			// 取出当前锁量的锁RFID
			toData(currentLockRfidFrameNO, lockerCount, lockRfidFrameCount,
					LOCKER_RFID_MAX_COUNT, functionCode);

			currentLockRfidFrameNO++;
		} else if (functionCode.value.equals(BtFunctionCode.CODE_06.value)) {
			
			Log.i("初始化钥匙数据", "发送分帧报文锁名称-----当前帧："+currentLockNameFrameNO);	
			
			// 取出当前锁量的锁名称
			toData(currentLockNameFrameNO, lockerCount, lockNameFrameCount,
					LOCKER_NAME_MAX_COUNT, functionCode);
			currentLockNameFrameNO++;

		} else if (functionCode.value.equals(BtFunctionCode.CODE_08.value)) {
			
			Log.i("初始化钥匙数据", "发送分帧报文锁类型-----当前帧："+currentLockTypeFrameNO);	
			
			// 取出当前锁量的锁类型
			toData(currentLockTypeFrameNO, lockerCount, lockTypeFrameCount,
					LOCKER_TYPE_MAX_COUNT, functionCode);
			currentLockTypeFrameNO++;

		} 
		
//		else if (functionCode.value.equals(BtFunctionCode.CODE_0A.value)) {
//			// 取出当前锁量的锁站号
//			toData(currentLockStationFrameNO, lockerCount,
//					lockStationFrameCount, LOCKER_STATION_MAX_COUNT,
//					functionCode);
//			currentLockStationFrameNO++;
//
//		} 
		else if (functionCode.value.equals(BtFunctionCode.CODE_0C.value)) {
			
			Log.i("初始化钥匙数据", "发送分帧报文锁逻辑-----当前帧："+currentLockLogicFrameNO);	
			
			// 取出当前锁量的锁逻辑
			toData(currentLockLogicFrameNO, lockerCount, lockLogicFrameCount,
					LOCKER_LOGIC_MAX_COUNT, functionCode);
			currentLockLogicFrameNO++;

		} else if (functionCode.value.equals(BtFunctionCode.CODE_0E.value)) {
			Log.i("初始化钥匙数据", "发送分帧报文前用户-----当前帧："+currentUserFrameNO);	
			
			// 取出当前用户
			toData(currentUserFrameNO, userCount, userFrameCount,
					USER_MAX_COUNT, functionCode);
			currentUserFrameNO++;
		}

		return createBtMsg();

	}

	/**
	 * 组合动态数据
	 * 
	 * @param currentFrameNO
	 *            当前帧
	 * @param count
	 *            设备总数量
	 * @param frameCount
	 *            总帧数
	 * @param maxCount
	 *            一帧最多包含设备数
	 * @param functionCode
	 *            功能码
	 */
	private void toData(int currentFrameNO, int count, int frameCount,
			int maxCount, BtFunctionCode functionCode) {
		final int indexStart = (currentFrameNO - 1) * maxCount;
		// 如果不是最后一帧，则当前帧锁量结束点是 64 * 帧号
		final int indexEnd = currentFrameNO < frameCount ? maxCount
				* currentFrameNO : count;
		// 根据当前锁数量，循环取出锁ID
		final StringBuffer lockerIDandStationID = new StringBuffer();
		for (int i = indexStart; i < indexEnd; i++) {

			if (functionCode.value.equals(BtFunctionCode.CODE_02.value)) {
				// 取出当前锁量的锁码ID
				lockerIDandStationID.append(BtMsgTools.completeAfter(locksets
						.get(i).getId(), "0000"));

				// 取出当前锁量的锁站号
				lockerIDandStationID
						.append(BtMsgTools.completeAfter(1, "0000"));

			} else if (functionCode.value.equals(BtFunctionCode.CODE_04.value)) {
				// 取出当前锁量的锁RFID
				lockerIDandStationID.append(BtMsgTools.completeAfter(locksets
						.get(i).getRfid(), "0000000000"));

			} else if (functionCode.value.equals(BtFunctionCode.CODE_06.value)) {
				// 取出当前锁量的锁名称
				String lockName = BtMsgConver.strToHexStr(locksets.get(i)
						.getName());

				lockerIDandStationID.append(BtMsgTools.completeAfter(lockName,
						"00000000000000000000" + "00000000000000000000"
								+ "00000000000000000000"
								+ "00000000000000000000"
								+ "00000000000000000000"
								+ "00000000000000000000"));

			} else if (functionCode.value.equals(BtFunctionCode.CODE_08.value)) {
				// 取出当前锁量的锁类型
				int type = Integer.valueOf(locksets.get(i).getType());
				String lockType = BtMsgConver.toHexString(type);

				lockerIDandStationID.append(BtMsgTools.completeAfter(lockType,
						"00"));

			} else if (functionCode.value.equals(BtFunctionCode.CODE_0A.value)) {
				// 取出当前锁量的锁站号

				lockerIDandStationID.append(BtMsgTools.completeAfter(locksets
						.get(i).getStationId(), "0000"));

			} else if (functionCode.value.equals(BtFunctionCode.CODE_0C.value)) {
				// 取出当前锁量的锁逻辑

				String logic = BtMsgTools.completeAfter(locksets.get(i)
						.getLogic(), "0000");
				String station_id = "";
				if (logic.toUpperCase().equals("FFFF")) {// 不验电
					station_id = "FFFF";
				} else {
					station_id = "0100";
				}

				lockerIDandStationID.append(logic).append(station_id);

			} else if (functionCode.value.equals(BtFunctionCode.CODE_0E.value)) {
				// 取出当前用户
				String userId = BtMsgTools.completeAfter(
						allUser.get(i).getId(), "0000");
				String userName = BtMsgConver.strToHexStr(allUser.get(i)
						.getName());

				String leteAfterName = BtMsgTools.completeAfter(userName,
						"00000000000000000000000000000000000000");

				String password = BtMsgConver.strToHexStr(allUser.get(i)
						.getPassword());

				String leteAfterPassword = BtMsgTools.completeAfter(password,
						"000000000000");

				String rfid = BtMsgTools.completeAfter(
						allUser.get(i).getRfid(), "0000000000");

				lockerIDandStationID.append(userId).append(leteAfterName)
						.append(leteAfterPassword).append(rfid);
			}

		}

		// 当前帧锁量，如果不是最后一帧，当前帧锁量都是64
		int currentLockerNum = currentFrameNO < frameCount ? maxCount : count
				- indexStart;
		// 格式化当前帧锁量为十六进制
		final String currentLockerNumtHex = BtMsgTools.completeAfter(
				currentLockerNum, "0000");
		// 格式化帧号为十六进制
		final String frameNOHex = BtMsgTools.completeAfter(currentFrameNO,
				"0000");

		// 帧号、当前帧锁数量、锁ID结构
		this.data = frameNOHex + currentLockerNumtHex + lockerIDandStationID;

		Log.i("初始化钥匙数据", "发送分帧报文"+data);	
	}

	/**
	 * 发送时间报文
	 * 
	 * @param functionCode
	 *            功能码
	 * @return 组合后报文
	 */
	public byte[] createDateFrameMsg() {

		this.command = BtCommand.CODE_01.value;
		this.functionCode = BtFunctionCode.CODE_00.value;
		String date = DateUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss");
		this.dataSource = date;
		this.data = BtMsgTools.paserDateToHex(date);// 日期转十六进制
		
		Log.i("初始化钥匙数据", "发送时间报文-----");
		
		return createBtMsg();

	}

	@Override
	public void createData() {
		// TODO Auto-generated method stub
	}

}
