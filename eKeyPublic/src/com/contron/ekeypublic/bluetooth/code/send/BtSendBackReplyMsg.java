package com.contron.ekeypublic.bluetooth.code.send;

import android.util.SparseArray;
import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtControl;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;

/**
 * 任务回传发送信息
 * 
 * @author luoyilong
 *
 */
public class BtSendBackReplyMsg extends BtSendMsg {

	@Override
	public void createData() {
	}

	/**
	 * 查询钥匙中的回传任务
	 * 
	 * @return
	 */
	public byte[] createTaskSelectFrameMsg() {
		this.control = BtControl.CODE_10.value;
		this.command = BtCommand.CODE_05.value;
		this.functionCode = BtFunctionCode.CODE_00.value;
		return createBtMsg();
	}

	/**
	 * 删除钥匙中的回传任务
	 * 
	 * @return
	 */
	public byte[] createDeletTaskFrameMsg(String tid) {
		this.command = BtCommand.CODE_06.value;
		this.functionCode = BtFunctionCode.CODE_00.value;

		this.dataSource = tid + "";
		this.data = tid;
		return createBtMsg();
	}

	/**
	 * 判断钥匙中查询出来任务ID是否包含当前任务ID return true 存在
	 */
	public String isKeyBackTask(String data, int tid) {
		int taskNum = BtMsgConver.hexToInt(data.substring(0, 3).toString());
		String taskIds = data.subSequence(4, data.length()).toString();

		String taskID = BtMsgTools.completeBefore(BtMsgConver.toHexString(tid),
				"00000000000000000000000000000000");

		for (int i = 0; i < taskNum; i++) {
			int index = i * 32;
			int las = (i + 1) * 32;

			String tempTaskID = taskIds.substring(index, las).toString();

			StringBuilder taskID2 = new StringBuilder(tempTaskID);
			taskID2.replace(0, 1, "0");

			if (taskID.equals(taskID2.toString()))
				return tempTaskID;
		}

		return "";

	}

	/**
	 * 获取钥匙中所有任务ID
	 */
	public SparseArray<String> getAllTaskId(String data) {
		int taskNume = BtMsgConver.hexToInt(data.substring(0, 3).toString());
		String taskidS = data.subSequence(4, data.length()).toString();
		SparseArray<String> taskList = new SparseArray<String>();

		for (int i = 0; i < taskNume; i++) {
			int index = i * 32;
			int las = (i + 1) * 32;

			String tempTaskID = taskidS.substring(index, las).toString();

			StringBuffer taskId = new StringBuffer(tempTaskID);
			taskId.replace(0, 1, "0");

			int id = 0;
			try {
				if (!tempTaskID.substring(0, 1).equals("0")) {// 非全站解时获取ID，全站解锁ID为0
					id = Integer.parseInt(taskId.toString(), 16);
				}

			} catch (java.lang.NumberFormatException e) {

			} finally {
				taskList.put(id, tempTaskID);
			}

		}

		return taskList;

	}

	/**
	 * 创建票ID报文
	 * 
	 * @return
	 */
	public byte[] createTaskTitleFrameMsg(String tid) {

		this.command = BtCommand.CODE_04.value;
		this.functionCode = BtFunctionCode.CODE_00.value;

		this.dataSource = tid;

		this.data = tid;
		return createBtMsg();
	}

	/**
	 * 创建票帧信息
	 * 
	 * @return
	 */
	public byte[] createTaskFrameMsg(int frameCount) {

		this.control = BtControl.CODE_10.value;
		this.command = BtCommand.CODE_04.value;
		this.functionCode = BtFunctionCode.CODE_01.value;

		this.dataSource = frameCount + "";
		this.data = BtMsgTools.completeAfter(frameCount, "0000");
		return createBtMsg();
	}

}
