/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code.send;

import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtControl;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;

/**
 * 操作授权
 * 
 * @author hupei
 * @date 2015年5月6日 上午9:27:12
 */
public class BtSendEmpowerMsg extends BtSendMsg {
	/**
	 * 
	 * @author hupei
	 * @date 2015年5月4日 下午2:15:57
	 * @param lockerRFID
	 * @param isEnabled
	 *            true 允许操作
	 */
	public BtSendEmpowerMsg(String lockerRFID, boolean isEnabled) {
		this.dataSource = lockerRFID + (isEnabled ? "01" : "00");
		this.control = BtControl.CODE_10.value;
		this.command = BtCommand.CODE_20.value;
		this.functionCode = BtFunctionCode.CODE_00.value;
		createData();
	}

	@Override
	public void createData() {
		if (!this.dataSource.isEmpty())// 判断空
			this.data = this.dataSource;// RFID源文就是十六进制
	}
}

