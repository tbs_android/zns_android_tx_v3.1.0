package com.contron.ekeypublic.bluetooth.code.send;

import java.util.Calendar;
import java.util.List;

import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.bluetooth.code.BtCommand;
import com.contron.ekeypublic.bluetooth.code.BtControl;
import com.contron.ekeypublic.bluetooth.code.BtFunctionCode;
import com.contron.ekeypublic.bluetooth.code.BtMsgTools;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.util.TaskType;

/**
 * 构造传票帧数据与传票数据
 * 
 */
public class BtSendTaskMsg extends BtSendMsg {
	// 数据包最大262个字节
	private static final int DATA_MAX_LEN = 262;
	private static final int LOCKER_STRUCT_LEN = 4;// 锁结构体长度(锁ID，站ID)4个字节
	private static final int LOCKER_STRUCT_MAX_LEN = DATA_MAX_LEN - 6; // 每帧锁结构体最大总字节=256。命令+功能+帧号+帧锁数量=6
	private static final int LOCKER_MAX_COUNT = LOCKER_STRUCT_MAX_LEN
			/ LOCKER_STRUCT_LEN;// 每帧最大的锁数量=64个

	private int lockerCount;// 锁总量
	private int userCount; // 用户数量
	private int frameCount;// 总帧数
	private int currentFrameNO = 1;// 当前发送帧号，从1开始
	private TempTask fixationTask = null;// 任务作息
	private List<Lockset> lockers = null;// 锁集合
	private List<User> users = null;// 用户集合

	/**
	 * 任务票
	 * 
	 * @param fixationTask
	 */
	public BtSendTaskMsg(TempTask fixationTask) {
		this.control = BtControl.CODE_10.value;// 主动发送

		this.fixationTask = fixationTask;

		lockerCount = fixationTask.getLocksetList().size();
		userCount = fixationTask.getUserList().size();
		lockers = fixationTask.getLocksetList();
		users = fixationTask.getUserList();

		// 获取总帧数
		frameCount = getFrameCount(lockerCount, LOCKER_MAX_COUNT);
	}

	/**
	 * 获取总贞数
	 * 
	 * @param Count
	 *            锁数据、用户数据
	 * @param maxCount
	 *            一帧最大数量
	 * @return
	 */
	private int getFrameCount(int Count, int maxCount) {

		// 计算总帧
		int frameCount = 0;

		if (Count < maxCount) {
			frameCount = 1;
		} else {
			int temp = (int) (Count / maxCount);
			frameCount = temp * maxCount == Count ? temp : temp + 1;
		}

		return frameCount;

	}

	/**
	 * 固定任务标题报文
	 * 
	 * @return
	 */
	public byte[] createTaskTitleFrameMsg(String functionCode) {

		this.command = BtCommand.CODE_03.value;
		this.functionCode = functionCode;

		// 任务ID
		// String guid = BtMsgTools.completeAfter(fixationTask.getId(),
		// "00000000000000000000000000000000");

		// 任务ID
		String guid = BtMsgTools.completeBefore(
				BtMsgConver.toHexString(fixationTask.getId()),
				"00000000000000000000000000000000");

		// 任务名称
		String ticketNameHex = BtMsgConver.strToHexStr(fixationTask.getName());// 任务名转为十六进制字符

		String ticketName = BtMsgTools.completeAfter(ticketNameHex,
				"00000000000000000000000000000000");

		// 锁数量
		String lockerNum = BtMsgTools.completeAfter(lockerCount, "00000000");

		// 用户数量
		String userNum = BtMsgTools.completeAfter(userCount, "0000");

		// 用户ID
		StringBuffer userId = new StringBuffer();

		for (User user : users) {
			String user_id = BtMsgTools.completeAfter(user.getId(), "0000");
			userId.append(user_id);
		}

		if (BtFunctionCode.CODE_06.value.equals(functionCode)) {// 固定任务

			// 有效时长
			String limit = BtMsgTools.completeAfter(fixationTask.getLimited(),
					"00000000");

			// this.dataSource = guid + ticketName + lockerNum + userNum
			// + userId.toString() + limit;

			StringBuilder sb = new StringBuilder(guid);
			sb.replace(0, 1, TaskType.TransPermanent.value);

			this.data = sb.toString() + ticketName + lockerNum + userNum
					+ userId.toString() + limit;

		} else if (BtFunctionCode.CODE_00.value.equals(functionCode)) {// 计划任务

			StringBuilder sb = new StringBuilder(guid);

			String beginAt = fixationTask.getBeginAt();
			String endAt = fixationTask.getEndAt();

			if(beginAt == null || beginAt.length() == 0 || beginAt.equals("") || endAt == null || endAt.length() == 0 || endAt.equals("") ){ //表示系统固定任务   待测试
				fixationTask.setBeginAt(DateUtil.getCurrentDate("yyyyMMddHHmmss"));
				fixationTask.setEndAt(DateUtil.getCurrentDateByOffset("yyyyMMddHHmmss", Calendar.MINUTE, fixationTask.getLimited() ));
				
				sb.replace(0, 1, TaskType.TransFixed.value);
			}else{
//				StringBuilder sb = new StringBuilder(guid);
				sb.replace(0, 1, TaskType.TransTemp.value);
			}
//			String time = DateUtil.getStringByFormat(fixationTask.getBeginAt(), DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
			String start_time = BtMsgTools.paserDateToHex(fixationTask.getBeginAt());// 日期转十六进制
//			time = DateUtil.getStringByFormat(fixationTask.getEndAt(), DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
			String end_time = BtMsgTools.paserDateToHex(fixationTask.getEndAt());// 日期转十六进制

//			 StringBuilder sb = new StringBuilder(guid);
//			 sb.replace(0, 1, TaskType.TransTemp.value);

			this.data = sb.toString() + ticketName + lockerNum + userNum
					+ userId.toString() + start_time + end_time;
		}

		return createBtMsg();
	}

	/**
	 * 总帧数
	 * 
	 * @return
	 */
	public byte[] createTotalFrameMsg(String functionCode) {

		this.command = BtCommand.CODE_03.value;
		this.functionCode = functionCode;// 总帧功能码

		// 总帧数，双字节
		final String frameCountHex = BtMsgTools.completeAfter(frameCount,
				"0000");

		this.data = frameCountHex;

		return createBtMsg();
	}

	// 判断分帧是否发送完成
	public boolean isSendDone() {
		return currentFrameNO > frameCount;
	}

	/**
	 * 发送分帧报文
	 * 
	 * @param functionCode
	 * @return
	 */
	public byte[] createFenFrameMsg(String functionCode) {

		this.command = BtCommand.CODE_03.value;
		this.functionCode = functionCode;// 总帧功能码

		final int indexStart = (currentFrameNO - 1) * LOCKER_MAX_COUNT;
		// 如果不是最后一帧，则当前帧锁量结束点是 64 * 帧号
		final int indexEnd = currentFrameNO < frameCount ? LOCKER_MAX_COUNT
				* currentFrameNO : lockerCount;
		// 根据当前锁数量，循环取出锁ID
		final StringBuffer lockerIDandStationID = new StringBuffer();
		for (int i = indexStart; i < indexEnd; i++) {
			lockerIDandStationID.append(BtMsgTools.completeAfter(lockers.get(i)
					.getId(), "0000")); // 取出当前锁量的锁码ID

			// lockerIDandStationID.append(BtMsgTools.completeAfter(lockers.get(i)
			// .getStationId(), "0100"));// 添加站ID
			lockerIDandStationID.append("0100");// 添加站ID
		}
		// 当前帧锁量，如果不是最后一帧，当前帧锁量都是64
		int currentLockerNum = currentFrameNO < frameCount ? LOCKER_MAX_COUNT
				: lockerCount - indexStart;
		// 格式化当前帧锁量为十六进制
		final String currentLockerNumtHex = BtMsgTools.completeAfter(
				currentLockerNum, "0000");
		// 格式化帧号为十六进制
		final String frameNOHex = BtMsgTools.completeAfter(currentFrameNO++,
				"0000");
		// 帧号、当前帧锁数量、锁码结构(锁ID、变电站ID)
		this.data = frameNOHex + currentLockerNumtHex + lockerIDandStationID;
		return createBtMsg();
	}

	/**
	 * 发送时间报文
	 * 
	 * @param functionCode
	 *            功能码
	 * @return 组合后报文
	 */
	public byte[] createDateFrameMsg() {

		this.command = BtCommand.CODE_01.value;
		this.functionCode = BtFunctionCode.CODE_00.value;
		String date = DateUtil.getCurrentDate("yyyy-MM-dd HH:mm:ss");
		this.dataSource = date;
		this.data = BtMsgTools.paserDateToHex(date);// 日期转十六进制
		return createBtMsg();

	}

	@Override
	public void createData() {

	}
}
