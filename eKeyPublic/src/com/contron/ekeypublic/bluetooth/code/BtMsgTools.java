/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code;

import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.util.DateUtil;

public class BtMsgTools {

	public static BtRecvMsg recv(String msg) {
		return BtMsgKit.parse(msg);
	}

	/**
	 * 
	 * 日期转十六进制 <br>
	 * strDate都会转为yyyyMMddHHmmss格式后再转十六
	 * 
	 * @author hupei
	 * @date 2015年5月6日 上午10:40:10
	 * @param date
	 *            源格式不受限制
	 * @return
	 */
	public static String paserDateToHex(String strDate) {
		final String date = DateUtil.getStringByFormat(strDate, DateUtil.dateFormatYMDHMS, "yyyyMMddHHmmss");
		int yy = Integer.parseInt(date.substring(0, 4));
		int MM = Integer.parseInt(date.substring(4, 6));
		int dd = Integer.parseInt(date.substring(6, 8));
		int HH = Integer.parseInt(date.substring(8, 10));
		int mm = Integer.parseInt(date.substring(10, 12));
		int ss = Integer.parseInt(date.substring(12, 14));
		// YY高低位调换
		return new StringBuffer(completeBefore(BtMsgConver.toHexString(yy), "0000"))
				.append(completeBefore(Integer.toHexString(MM), "00"))
				.append(completeBefore(Integer.toHexString(dd), "00"))
				.append(completeBefore(Integer.toHexString(HH), "00"))
				.append(completeBefore(Integer.toHexString(mm), "00"))
				.append(completeBefore(Integer.toHexString(ss), "00")).toString();
	}

	/**
	 * 前补全十六进制
	 * 
	 * @author hupei
	 * @date 2015年5月6日 上午10:36:01
	 * @param strHex
	 *            需要补足的 十六进制字符
	 * @param format
	 *            字节数:如年双字节（"0000"）
	 * @return
	 */
	public static String completeBefore(String strHex, String format) {
		int strLength = strHex.length();
		int formatLength = format.length();
		String result = strLength < formatLength ? format.substring(0, formatLength - strLength) + strHex : strHex;
		return result.toUpperCase();
	}

	/**
	 * 后补全十六进制
	 * 
	 * @author hupei
	 * @date 2015年5月6日 上午10:36:01
	 * @param value
	 *            需要补足的 value
	 * @param format
	 *            字节数:如年双字节（"0000"）
	 * @return
	 */
	public static String completeAfter(int value, String format) {
		final String strHex = BtMsgConver.toHexString(value);// 转为十六进制并高低位调换
		return completeAfter(strHex, format);
	}

	/**
	 * 后补全十六进制
	 * 
	 * @author hupei
	 * @date 2015年5月6日 上午10:36:01
	 * @param strHex
	 *            需要补足的 十六进制字符
	 * @param format
	 *            字节数:如年双字节（"0000"）
	 * @return
	 */
	public static String completeAfter(String strHex, String format) {
		final int strLength = strHex.length();
		final int formatLength = format.length();
		final String result = strLength < formatLength ? strHex + format.substring(0, formatLength - strLength)
				: strHex.substring(0, formatLength);
		return result.toUpperCase();
	}
}
