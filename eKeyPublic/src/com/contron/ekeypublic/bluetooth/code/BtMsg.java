/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code;

/**
 * 报文基类
 * 
 * <pre>
 * 同步头			0XA5				4字节
 * 						0X5A	
 * 						0XA5	
 * 						0X5A	
 * 报文长度		低字节				动态部分字节数
 * 						高字节	
 * 源地址			低字节				应答时对调
 * 						高字节	
 * 目标地址		低字节	
 * 						高字节	
 * 控制字			1字节				0X1x(主动发送)
 * 												0X2x(应答)
 * 												0X4x(ACK、NAK)
 * 												0Xx0（明文）
 * 												0Xx1（密文）
 * 命令码			1字节				动态部分：最大1002字节
 * 功能码			1字节	
 * 数据正文		数据字节1	
 * 						...	
 * 						数据字节N	
 * 校验CRC16	低字节				校验内容：报文长度开始至动态部分结束
 * </pre>
 * 
 * @author hupei
 * @date 2015年4月15日 上午10:40:41
 */
public class BtMsg {
	public static final String HEAD = "A55AA55A";
	public String head = "";
	public String length = "";
	/** 源地址 2字节 */
	public String sourceAddress = "0000";
	/** 目标地址 2字节 */
	public String targetAddress = "0000";
	public String control = "";
	public String command = "";
	public String functionCode = "";

	/**
	 * 数据正文：十六进制
	 */
	public String data = "";
	public String checkCode = "";
	/**
	 * 数据正文：字符串
	 */
	public String dataSource = "";
	/**
	 * 动态部分
	 */
	public String dynamicData = "";
	public String msg = "";// 十六进制

	public BtMsg() {
		this.head = HEAD;
	}
}