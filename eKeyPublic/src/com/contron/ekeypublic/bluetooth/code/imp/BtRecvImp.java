/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code.imp;

import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;

/**
 * 接收
 * 
 * @author hupei
 * @date 2015年10月30日 上午9:26:08
 */
public interface BtRecvImp {
	/**
	 * 对数据正文进行处理时，需要实现此方法
	 * 
	 * @author hupei
	 * @date 2015年10月30日 上午9:26:14
	 */
	void createDataSource();

	/**
	 * 接收到的蓝牙报文转换为报文实体
	 * 
	 * @author hupei
	 * @date 2015年10月30日 上午9:26:35
	 * @param msg
	 * @return
	 */
	BtRecvMsg parserBtMsg(String msg);
}
