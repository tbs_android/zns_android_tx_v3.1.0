/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code.imp;

/**
 * 蓝牙发送接口
 * 
 * @author hupei
 * @date 2015年10月30日 上午9:32:58
 */
public interface BtSendImp {
	/**
	 * 创建数据正文
	 * 
	 * @author hupei
	 * @date 2015年10月30日 上午9:36:05
	 */
	void createData();

	/**
	 * 设置包长度：动态部分字节数
	 * 
	 * @author hupei
	 * @date 2015年4月15日 下午2:13:57
	 */
	void createLength();

	/**
	 * 动态部分：命令字+功能码+数据正文
	 * 
	 * @author hupei
	 * @date 2015年4月15日 下午3:49:36
	 * @return
	 */
	void createDynamicData();

	/**
	 * 设置校验码为动态部分：报文长度开始至动态部分结束
	 * 
	 * @author hupei
	 * @date 2015年4月15日 下午2:14:18
	 */
	void createCheckCode();

	/**
	 * 转换为最终蓝牙报文字节码
	 * 
	 * @author hupei
	 * @date 2015年4月15日 下午3:47:34
	 * @return 同步头
	 */
	byte[] createBtMsg();
}
