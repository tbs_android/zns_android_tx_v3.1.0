/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code.recv;

import com.contron.ekeypublic.bluetooth.BtMsgConver;

/**
 * 接收钥匙电量
 * 
 * @author hupei
 * @date 2015年5月6日 上午11:02:29
 */
public class BtRecvBattMsg extends BtRecvMsg {
	private static final int MAX_BATT = 2510;
	private static final int MIN_BATT = 2270;
	private static final int TOTAL_BATT = MAX_BATT - MIN_BATT;
	private int currentBatt;// < 2270 报警
	public String strBatt;

	@Override
	public void createDataSource() {
		this.dataSource = this.data;// 源文就是十六进制，所以不需要转换
		currentBatt = BtMsgConver.hexToInt(dataSource);
		if (isBattMin()) {
			strBatt = "电量不足";
		} else if (currentBatt > MAX_BATT) {
			strBatt = "电量满";
		} else {
			int current = currentBatt - MIN_BATT;
			strBatt = current * 100 / TOTAL_BATT + "%";
		}
	}

	/**
	 * 电量低5%报告
	 * 
	 * @author hupei
	 * @date 2015年7月24日 下午7:08:12
	 * @return
	 */
	public boolean isBattMin() {
		return currentBatt <= 2280;
	}
}
