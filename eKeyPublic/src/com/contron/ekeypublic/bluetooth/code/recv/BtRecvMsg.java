/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code.recv;

import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.bluetooth.code.BtMsg;
import com.contron.ekeypublic.bluetooth.code.imp.BtRecvImp;
//import com.lidroid.xutils.util.LogUtils;

public abstract class BtRecvMsg extends BtMsg implements BtRecvImp {

	@Override
	public BtRecvMsg parserBtMsg(String msg) {
//		LogUtils.i("receive：" + msg.replaceAll("(.{2})", "$1 "));
		this.msg = msg;
		try {
			this.head = msg.substring(0, 8);// 取出同步头
			this.length = msg.substring(8, 12);// 长度
			this.sourceAddress = msg.substring(12, 16);// 源
			this.targetAddress = msg.substring(16, 20);// 目标
			this.control = msg.substring(20, 22);// 控制
			this.command = msg.substring(22, 24);// 命令
			this.functionCode = msg.substring(24, 26);// 功能
			// 先高低位还原后，再十六进制包长度转为十进制
			int dataLength = BtMsgConver.hexToInt(this.length);
			int dataLengEnd = (dataLength - 1 - 1) * 2 + 26;// 截取数据包的结束长度：包长度-命令字-功能码
			this.data = msg.substring(26, dataLengEnd);
			this.checkCode = msg.substring(dataLengEnd, msg.length());

		} catch (StringIndexOutOfBoundsException e) {
			e.printStackTrace();
			return null;
		}
		return this;
	}

}
