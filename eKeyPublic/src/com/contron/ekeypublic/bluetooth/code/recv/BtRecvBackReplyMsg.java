package com.contron.ekeypublic.bluetooth.code.recv;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import com.contron.ekeypublic.bluetooth.BtMsgConver;
import com.contron.ekeypublic.bluetooth.code.recv.BtRecvMsg;
import com.contron.ekeypublic.util.DateUtil;


/**
 * 任务回传响应内容
 * 
 * @author luoyilong
 * @date 2016年6月23日 上午11:06:37
 */
public class BtRecvBackReplyMsg extends BtRecvMsg {
	private static final int STRUCT_LEN_STR = 17 * 2;// 一组结构体占 17 个字节，34个字符
	private static final int DATA_HEAD = 4 * 2;// 数据正文头：帧号和记录数占4 个字节，8个字符
	private int frameCount;// 总帧

//	@Override
//	public String parserDataSource() {
//		BtFunctionCode ackFunctionCode = BtFunctionCode.valuesOf(getFunctionCode());
//		setDataSource(getData());
//		// 总帧转十进制
//		frameCount = BtMsgConver.hexToInt(getDataSource());
//		return ackFunctionCode.value;
//	}

	
	
	/**
	 * 获取总帧
	 * @return
	 */
	public int getCountFrame(){
		String count=this.data.toString().substring(0, 4);
		frameCount = BtMsgConver.hexToInt(count);	
		return frameCount;
	}
	
	/**
	 * 获取操作用户ID
	 * @return
	 */
	public int getUserFrame(){
		String count=this.data.toString().substring(4, data.length());

		return BtMsgConver.hexToInt(count);	
	}
	
	
	
	public int getFrameCount() {
		return frameCount;
	}

	public void setFrameCount(int frameCount) {
		this.frameCount = frameCount;
	}

	
	/**
	 * 处理返回操作记录
	 * @return
	 */
	public List<BtRecvBackData> parserBackData() {
		List<BtRecvBackData> ackDataList = new ArrayList<BtRecvBackData>();
		final int frameDataCount = BtMsgConver.hexToInt(this.data.substring(4, 8));// 当前帧记录数
		final String structData =this.data.substring(DATA_HEAD, this.data.length());
		// 循环解析报文数据正文的结构
		for (int i = 0; i < frameDataCount; i++) {
			final BtRecvBackData ackData = new BtRecvBackData();
			ackData.lid = BtMsgConver.hexToInt(structData.substring(i * STRUCT_LEN_STR, i * STRUCT_LEN_STR + 4));
			ackData.stationId = BtMsgConver.hexToInt(structData.substring(i * STRUCT_LEN_STR + 4, i * STRUCT_LEN_STR
					+ 8));
			ackData.rfid = structData.substring(i * STRUCT_LEN_STR + 8, i * STRUCT_LEN_STR + 18);
			int year = BtMsgConver.hexToInt(structData.substring(i * STRUCT_LEN_STR + 18, i * STRUCT_LEN_STR + 22));
			int month = Integer.parseInt(structData.substring(i * STRUCT_LEN_STR + 22, i * STRUCT_LEN_STR + 24), 16);
			int day = Integer.parseInt(structData.substring(i * STRUCT_LEN_STR + 24, i * STRUCT_LEN_STR + 26), 16);
			int hourOfDay = Integer
					.parseInt(structData.substring(i * STRUCT_LEN_STR + 26, i * STRUCT_LEN_STR + 28), 16);
			int minute = Integer.parseInt(structData.substring(i * STRUCT_LEN_STR + 28, i * STRUCT_LEN_STR + 30), 16);
			int second = Integer.parseInt(structData.substring(i * STRUCT_LEN_STR + 30, i * STRUCT_LEN_STR + 32), 16);
			final Calendar calendar = Calendar.getInstance();
			calendar.set(year, month-1, day, hourOfDay, minute, second);
			ackData.unlock_at=DateUtil.getStringByFormat(calendar.getTime(), "yyyyMMddHHmmss");
			
			ackData.result = Integer.parseInt(structData.substring(i * STRUCT_LEN_STR + 32, i * STRUCT_LEN_STR + 34),
					16);
			ackDataList.add(ackData);
		}
		return ackDataList;
	}

	/**
	 * 解析采码记录
	 * @return
	 */
	public List<BtRecvCollectBackData> parserCollectBackData() {
		List<BtRecvCollectBackData> ackDataList = new ArrayList<BtRecvCollectBackData>();
		// int frameNO = BtMsgConver.hexToInt(getData().substring(0, 4));// 帧号
		final int frameDataCount = BtMsgConver.hexToInt(this.data.substring(4, 8));// 当前帧记录数
		final String structData = this.data.substring(DATA_HEAD, this.data.length());
		// 循环解析报文数据正文的结构
		for (int i = 0; i < frameDataCount; i++) {
			final BtRecvCollectBackData ackData = new BtRecvCollectBackData();
			ackData.id = BtMsgConver.hexToInt(structData.substring(i * STRUCT_LEN_STR, i * STRUCT_LEN_STR + 4));
			ackData.stationId = BtMsgConver.hexToInt(structData.substring(i * STRUCT_LEN_STR + 4, i * STRUCT_LEN_STR
					+ 8));
			ackData.rfid = structData.substring(i * STRUCT_LEN_STR + 8, i * STRUCT_LEN_STR + 18);
			ackDataList.add(ackData);
		}
		return ackDataList;
	}

	/**
	 * 任务回传分帧数据结构
	 * 
	 * @author hupei
	 * @date 2015年6月3日 上午11:06:37
	 */
	public class BtRecvBackData {
		public int lid;// 锁ID
		public int stationId;// 站ID
		public String rfid = "";// 锁RFID
		public String unlock_at = "";// 操作日期
		public int result;// 操作状态
	}

	/**
	 * 采码任务回传分帧数据结构
	 * 
	 * @author hupei
	 * @date 2015年6月3日 上午11:06:37
	 */
	public class BtRecvCollectBackData {
		public int id;// 锁ID
		public int stationId;// 站ID
		public String rfid = "";// 锁RFID
	}

	@Override
	public void createDataSource() {
		// TODO Auto-generated method stub
		
	}
}
