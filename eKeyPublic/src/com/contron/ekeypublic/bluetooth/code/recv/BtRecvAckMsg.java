/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code.recv;

import com.contron.ekeypublic.bluetooth.code.BtErrorCode;

/**
 * 接收ACK帧
 * 
 * @author hupei
 * @date 2015年5月6日 上午11:02:17
 */
public class BtRecvAckMsg extends BtRecvMsg {
	public BtErrorCode btErrorCode;

	@Override
	public void createDataSource() {
		this.dataSource = this.data;// 源文就是十六进制，所以不需要转换
		// 只截取数据包第2个字节，如果是80则成功，其它失败
		String errorCode = this.dataSource.substring(2, this.dataSource.length());
		btErrorCode = BtErrorCode.valuesOf(errorCode);
	}
}
