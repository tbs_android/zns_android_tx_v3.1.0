/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.bluetooth.code;

/**
 * 错误码枚举
 * 
 * @author hupei
 * @date 2015年4月15日 下午1:45:24
 */
public enum BtErrorCode {
	/** 正确 */
	SUCCESS("80", "发送成功"),
	/** 数据长度错误 */
	ERROR_DATA_LEN("81", "数据长度错误"),
	/** 命令字错误 */
	ERROR_COMMAND("82", "命令字错误"),
	/** 功能码错误 */
	ERROR_FUNCTION_CODE("83", "功能码错误"),
	/** 数据错误 */
	ERROR_DATA("84", "数据错误");
	private BtErrorCode(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public String code = "";
	public String value = "";

	public static BtErrorCode valuesOf(String arg) {
		for (BtErrorCode errorCode : BtErrorCode.values()) {
			if (errorCode.code.equalsIgnoreCase(arg)) {
				return errorCode;
			}
		}
		return null;
	}
}
