/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.backload;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.contron.ekeypublic.util.AppUtils;
import com.lidroid.xutils.util.LogUtils;

public class BackUploadService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		LogUtils.d("onCreate");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		LogUtils.d("onStartCommand");
		// 网络正常状态下才检查更新软件
		if (AppUtils.isNetworkAvailable(this)) {
			syncData();
		}
		/**
		 * 这里返回状态有三个值，分别是:
		 * 1、START_STICKY：当服务进程在运行时被杀死，系统将会把它置为started状态，但是不保存其传递的Intent对象
		 * ，之后，系统会尝试重新创建服务;
		 * 2、START_NOT_STICKY：当服务进程在运行时被杀死，并且没有新的Intent对象传递过来的话，
		 * 系统将会把它置为started状态， 但是系统不会重新创建服务，直到startService(Intent intent)方法再次被调用;
		 * 3、START_REDELIVER_INTENT：当服务进程在运行时被杀死，它将会在隔一段时间后自动创建，
		 * 并且最后一个传递的Intent对象将会再次传递过来
		 */
		return START_NOT_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		LogUtils.d("onDestroy");
	}

	/**
	 * 同步数据
	 * 
	 * @author hupei
	 * @date 2015年10月26日 上午11:49:02
	 * @param context
	 */
	private void syncData() {

//		new InitializeData();// 下载初始化数据

		// final EkeyDao ekeyDao = DBHelp.getInstance(this).getEkeyDao();
		// List<Locker> lockers = ekeyDao.getLocker();
		// List<TicketResult> ticketResults = ekeyDao.getTicketResult();
		// final Gson gson = new Gson();
		// SoapParams params = new SoapParams();
		// params.put("jsonDataLocker", gson.toJson(lockers));
		// params.put("jsonDataResult", gson.toJson(ticketResults));
		// JSONSoapAction soapAction = new EkeyJSONSoapAction(this,
		// Globals.SYNC_DATA, params,
		// new SoapAction.OnServiceListener() {
		//
		// @Override
		// public void onResponseSuccess(String wsdlMethodName, int code, String
		// msg, String wsdlJsonResult) {
		// // code =0 全部成功，=1采码成功，=2操作记录成功
		//
		// // 处理上传采码返回结果
		// if (code == 0 || code == 1) {
		// // 上传成功，更新采码为未采码，同等于已上传
		// ekeyDao.updateLocker();
		// }
		// // 处理上传操作记录返回结果
		// if (code == 0 || code == 2) {
		// // 上传成功，更新数据库操作记录状态为上传
		// ekeyDao.updateTicketResult();
		// }
		// stopSelf();//停止服务
		// }
		//
		// @Override
		// public void onResponseFailure(int errorCode, String wsdlMethodName) {
		// stopSelf();//停止服务
		// }
		// });
		// soapAction.start();
	}
}
