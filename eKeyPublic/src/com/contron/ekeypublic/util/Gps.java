package com.contron.ekeypublic.util;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Context;

import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;

public class Gps {
	private static final double EARTH_RADIUS = 6378137.0;

	/**
	 * 获取Gps
	 * 
	 * @param context
	 * @return
	 */
	public static LocationClient getGps(Context context) {
		LocationClient mLocationClient = new LocationClient(context);
		return mLocationClient;
	}

	/**
	 * 初始化GPS
	 * 
	 * @param mLocationClient
	 */
	private static void initLocation(LocationClient mLocationClient) {
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 可选，默认高精度，设置定位模式，高精度，低功耗，仅设备
		option.setCoorType("gcj02");// 可选，默认gcj02，设置返回的定位结果坐标系，
		int span = 1000;
		option.setScanSpan(span);// 可选，默认0，即仅定位一次，设置发起定位请求的间隔需要大于等于1000ms才是有效的
		// option.setIsNeedAddress(checkGeoLocation.isChecked());//可选，设置是否需要地址信息，默认不需要
		option.setOpenGps(true);// 可选，默认false,设置是否使用gps
		option.setLocationNotify(true);// 可选，默认false，设置是否当gps有效时按照1S1次频率输出GPS结果
		option.setIgnoreKillProcess(true);// 可选，默认true，定位SDK内部是一个SERVICE，并放到了独立进程，设置是否在stop的时候杀死这个进程，默认不杀死
		mLocationClient.setLocOption(option);
	}

	/**
	 * 注册回调事件
	 * 
	 * @param bdlocation
	 */
	private static void locationListener(LocationClient mLocationClient,
			BDLocationListener bdlocation) {
		mLocationClient.registerLocationListener(bdlocation);
	}

	/**
	 * 开始获取位置
	 */
	public static void start(LocationClient mLocationClient,
			BDLocationListener bdLocationListener) {
		if (mLocationClient != null) {
			mLocationClient.registerLocationListener(bdLocationListener);// 注册监听事件
			initLocation(mLocationClient);// 初始化GPS
			// start之后会默认发起一次定位请求，开发者无须判断isstart并主动调用request
			mLocationClient.start();
			mLocationClient.requestLocation();
		}
	}

	/**
	 * 关闭Gps
	 */
	public static void stop(LocationClient mLocationClient) {
		if (mLocationClient != null)
			mLocationClient.stop();
	}

	/**
	 * 截取小数点后多少位进行四舍五入
	 * 
	 * @param size
	 *            取的小数点后位数
	 * @param number
	 *            小数
	 * @return
	 */
	public static String format(int size, double number) {
		StringBuffer formatString = new StringBuffer("0");
		if (size > 0) {
			formatString.append(".");
		}
		for (int i = 0; i < size; i++) {
			formatString.append("#");
		}
		DecimalFormat df = new DecimalFormat(formatString.toString());
		return df.format(number);
	}

	// 返回单位是米
	public static double getDistance(double longitude1, double latitude1,
			double longitude2, double latitude2) {

		double Lat1 = rad(latitude1);
		double Lat2 = rad(latitude2);
		double a = Lat1 - Lat2;
		double b = rad(longitude1) - rad(longitude2);
		double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)
				+ Math.cos(Lat1) * Math.cos(Lat2)
				* Math.pow(Math.sin(b / 2), 2)));
		s = s * EARTH_RADIUS;
		s = Math.round(s * 10000) / 10000;
		return s;
	}

	private static double rad(double d) {
		return d * Math.PI / 180.0;
	}

}
