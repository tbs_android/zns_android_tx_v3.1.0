/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.util;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.DatePicker;

public class CygPickerDateFragment extends DialogFragment {
	public interface OnDateDoneListener {
		public void onDate(int year, int monthOfYear, int dayOfMonth, String date);
	}

	private OnDateDoneListener mOnDateDoneListener;

	private CygPickerDateFragment() {
	}

	public static CygPickerDateFragment newInstance(OnDateDoneListener onDateListener) {
		CygPickerDateFragment fragment = new CygPickerDateFragment();
		fragment.mOnDateDoneListener = onDateListener;
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Calendar mCalendar = Calendar.getInstance();
		int year = mCalendar.get(Calendar.YEAR);
		int month = mCalendar.get(Calendar.MONTH);
		int day = mCalendar.get(Calendar.DAY_OF_MONTH);
		return new DatePickerDialog(getActivity(), new OnDateSetListener() {

			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				mCalendar.set(Calendar.YEAR, year);
				mCalendar.set(Calendar.MONTH, monthOfYear);
				mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
				String date = DateFormat.format("yyyy-MM-dd", mCalendar).toString();
				if (mOnDateDoneListener != null)
					mOnDateDoneListener.onDate(year, monthOfYear, dayOfMonth, date);
			}
		}, year, month, day);
	}
}
