/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.util;

import com.lidroid.xutils.util.LogUtils;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

/**
 * 读取Sim卡信息
 * 
 * @author hupei
 * @date 2015年5月26日 下午3:55:35
 */
public class SIMCardInfo {
	/**
	 * TelephonyManager提供设备上获取通讯服务信息的入口。 应用程序可以使用这个类方法确定的电信服务商和国家 以及某些类型的用户访问信息。
	 * 应用程序也可以注册一个监听器到电话收状态的变化。不需要直接实例化这个类
	 * 使用Context.getSystemService(Context.TELEPHONY_SERVICE)来获取这个类的实例。
	 */
	private TelephonyManager telephonyManager;

	public SIMCardInfo(Context context) {
		telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
	}

	/**
	 * 获取当前设置的电话号码，去掉86
	 * 
	 * @author hupei
	 * @date 2015年5月26日 下午3:55:44
	 * @return
	 */
	public String getNativePhoneNumber() {
		getIMSI();
		String NativePhoneNumber = "";
		NativePhoneNumber = telephonyManager.getLine1Number();
		if (!TextUtils.isEmpty(NativePhoneNumber))
			if (NativePhoneNumber.startsWith("+86")) {
				NativePhoneNumber = NativePhoneNumber.substring(3);
			} else if (NativePhoneNumber.startsWith("86")) {
				NativePhoneNumber = NativePhoneNumber.substring(2);
			}
		return NativePhoneNumber;
	}

	/**
	 * 获取IMSI
	 * @return
	 */
	public String getIMSI() {
		String android_imsi = telephonyManager.getSubscriberId();
		LogUtils.e("IMSI:"+android_imsi);
		return android_imsi;
	}
}
