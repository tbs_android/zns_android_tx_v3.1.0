/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.util;

import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TimePicker;

public class CygPickerDateTimeFragment extends DialogFragment {
	public interface OnDateTimeDoneListener {
		public void onDateTime(int year, int monthOfYear, int dayOfMonth,
				int hourOfDay, int minute, String dateTime);
	}

	private Calendar mCalendar;
	private int mYear, mMonthOfYear, mDayOfMonth, mHourOfDay, mMinute;
	private OnDateTimeDoneListener mOnDateTimeDoneListener;

	private CygPickerDateTimeFragment() {
	}

	public static CygPickerDateTimeFragment newInstance(
			OnDateTimeDoneListener onDateTimeListener) {
		CygPickerDateTimeFragment fragment = new CygPickerDateTimeFragment();
		fragment.mOnDateTimeDoneListener = onDateTimeListener;
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mCalendar = Calendar.getInstance();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		LinearLayout view = new LinearLayout(getActivity());
		view.setOrientation(LinearLayout.VERTICAL);
		// view.setPadding(10, 0, 10, 0);
		LinearLayout pickerView = new LinearLayout(getActivity());
		pickerView.setGravity(Gravity.CENTER_HORIZONTAL);// 居中
		// 初始化日期控件
		DatePicker datePicker = new DatePicker(getActivity());
		resizePicker(datePicker);

		datePicker.setSpinnersShown(true);
		datePicker.setCalendarViewShown(false);// 设置为非日历模式
		mYear = mCalendar.get(Calendar.YEAR);
		mMonthOfYear = mCalendar.get(Calendar.MONTH);
		mDayOfMonth = mCalendar.get(Calendar.DAY_OF_MONTH);
		datePicker.init(mYear, mMonthOfYear, mDayOfMonth,
				new DatePicker.OnDateChangedListener() {

					@Override
					public void onDateChanged(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						updateDate(year, monthOfYear, dayOfMonth);
						updateTitle();
					}

					private void updateDate(int year, int monthOfYear,
							int dayOfMonth) {
						mYear = year;
						mMonthOfYear = monthOfYear;
						mDayOfMonth = dayOfMonth;
						mCalendar.set(Calendar.YEAR, mYear);
						mCalendar.set(Calendar.MONDAY, mMonthOfYear);
						mCalendar.set(Calendar.DAY_OF_MONTH, mDayOfMonth);
					}
				});
		pickerView.addView(datePicker);
		// 初始化时间控件
		TimePicker timePicker = new TimePicker(getActivity());
		resizePicker(timePicker);

		mHourOfDay = mCalendar.get(Calendar.HOUR_OF_DAY);
		mMinute = mCalendar.get(Calendar.MINUTE);
		timePicker.setIs24HourView(true);
		timePicker.setCurrentHour(mHourOfDay);
		timePicker.setCurrentMinute(mMinute);
		timePicker
				.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {

					@Override
					public void onTimeChanged(TimePicker view, int hourOfDay,
							int minute) {
						updateTime(hourOfDay, minute);
						updateTitle();
					}

					private void updateTime(int hourOfDay, int minute) {
						mHourOfDay = hourOfDay;
						mMinute = minute;
						mCalendar.set(Calendar.HOUR_OF_DAY, mHourOfDay);
						mCalendar.set(Calendar.MINUTE, mMinute);
					}
				});
		pickerView.addView(timePicker);
		view.addView(pickerView);

		LinearLayout btnView = new LinearLayout(getActivity());
		LinearLayout.LayoutParams btnViewParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT, 1);
		// 初始化取消按钮
		Button btnNegative = new Button(getActivity());
		btnNegative.setLayoutParams(btnViewParams);
		btnNegative.setText(android.R.string.cancel);
		btnNegative.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		btnView.addView(btnNegative);
		// 初始化确定按钮
		Button btnPositive = new Button(getActivity());
		btnPositive.setLayoutParams(btnViewParams);
		btnPositive.setText(android.R.string.ok);
		btnPositive.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mOnDateTimeDoneListener != null)
					mOnDateTimeDoneListener.onDateTime(mYear, mMonthOfYear,
							mDayOfMonth, mHourOfDay, mMinute,
							// 12小时制=hh; 24小时制=kk
							DateFormat.format("yyyy-MM-dd kk:mm", mCalendar)
									.toString());
				dismiss();
			}
		});
		btnView.addView(btnPositive);

		view.addView(btnView);
		return view;
	}

	private void resizePicker(ViewGroup viewGroup) {
		int count = viewGroup.getChildCount();
		View childAt = null;
		if (viewGroup != childAt)
			for (int i = 0; i < count; i++) {
				childAt = viewGroup.getChildAt(i);
				if (childAt instanceof NumberPicker) {
					NumberPicker numPicker = (NumberPicker) childAt;
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
							pixelsToDip(50),
							LinearLayout.LayoutParams.WRAP_CONTENT);
					params.leftMargin = 0;
					params.rightMargin = 5;
					numPicker.setLayoutParams(params);
				} else if (childAt instanceof LinearLayout) {
					resizePicker((ViewGroup) childAt);// 递归查找
				}
			}
	}

	private int pixelsToDip(int pixels) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				pixels, getResources().getDisplayMetrics());
	}

	private void updateTitle() {
		String title = DateUtils.formatDateTime(getActivity(),
				mCalendar.getTimeInMillis(), DateUtils.FORMAT_24HOUR
						| DateUtils.FORMAT_SHOW_DATE
						| DateUtils.FORMAT_SHOW_TIME
						| DateUtils.FORMAT_SHOW_WEEKDAY
						| DateUtils.FORMAT_SHOW_YEAR
						| DateUtils.FORMAT_ABBREV_MONTH);
		Dialog dialog = getDialog();
		if (dialog != null)
			dialog.setTitle(title);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		updateTitle();
	}
}
