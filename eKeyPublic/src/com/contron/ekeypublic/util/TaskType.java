package com.contron.ekeypublic.util;

public enum TaskType {
	/**
	 * 全站解锁
	 */
	TransAllUnlock("0"),
	/**
	 * 临时任务
	 */
	TransTemp("8"),
	/**
	 * 电子巡更
	 */
	TransGuardTour("9"),
	/**
	 * 部门锁上锁任务
	 */
	TransSectionalLock("A"),

	/**
	 * 部门锁解锁任务
	 */
	TransSectionalUnlock("B"),
	/**
	 * 固定任务
	 */
	TransFixed("C"),

	/**
	 * 预置任务
	 */
	TransPermanent("D"),
	/**
	 * 采码任务
	 */

	TransCollectRFID("E"),
	/**
	 * 任务授权
	 */
	TransTask("F"),
	/**
	 * 其它任务
	 */
	other("其它任务");

	public String value = "";

	private TaskType(String value) {
		this.value = value;
	}

	public static TaskType valuesOf(String arg) {
		for (TaskType type : TaskType.values()) {
			if (type.value.equalsIgnoreCase(arg)) {
				return type;
			}
		}
		return TaskType.other;
	}
}
