package com.contron.ekeypublic.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.contron.ekeypublic.entities.Offline;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Log;

/**
 * 日期处理类.
 */
@SuppressLint("SimpleDateFormat")
public class DateUtil {
	/** 一天的毫秒数 */
	public static final long DAY_MILLIS = 24 * 60 * 60 * 1000;
	/** 时间日期格式化到年月日时分秒. */
	public static final String dateFormatYMDHMS = "yyyy-MM-dd HH:mm:ss";

	public static final String dateFormatCYCMCDHMS = "yyyy年MM月dd日 HH:mm";

	/** 时间日期格式化到年月日. */
	public static final String dateFormatYMD = "yyyy-MM-dd";

	/** 时间日期格式化到年月. */
	public static final String dateFormatYM = "yyyy-MM";

	/** 时间日期格式化到年月日时分. */
	public static final String dateFormatYMDHM = "yyyy-MM-dd HH:mm";

	/** 时间日期格式化到月日时分. */
	public static final String dateFormatMDHM = "MM-dd HH:mm";

	/** 时间日期格式化到月日. */
	public static final String dateFormatMD = "MM/dd";

	/** 时分秒. */
	public static final String dateFormatHMS = "HH:mm:ss";

	/** 时分. */
	public static final String dateFormatHM = "HH:mm";

	public static final String DATE_FORMAT_YMDHMS = "yyyyMMddHHmmss";

	/** 起始执行时间. */
	public static long startLogTimeInMillis = 0;

	/**
	 * 描述：记录当前时间毫秒
	 * 
	 */
	public static void logStartTime() {
		Calendar current = Calendar.getInstance();
		startLogTimeInMillis = current.getTimeInMillis();
	}

	/**
	 * 
	 * 描述：打印这次的执行时间毫秒，需要首先调用logStartTime()
	 * 
	 * @param D
	 *            打印日志开关标记
	 * @param tag
	 *            标记
	 * @param msg
	 *            描述
	 */
	public static void logEndTime(boolean D, String tag, String msg) {
		Calendar current = Calendar.getInstance();
		long endLogTimeInMillis = current.getTimeInMillis();
		if (D)
			Log.d(tag, msg + ":" + (endLogTimeInMillis - startLogTimeInMillis)
					+ "ms");
	}

	/**
	 * 描述：String类型的日期时间转化为Date类型.
	 * 
	 * @param strDate
	 *            String形式的日期时间
	 * @param format
	 *            格式化字符串，如："yyyy-MM-dd HH:mm:ss"
	 * @return Date Date类型日期时间
	 */
	public static Date getDateByFormat(String strDate, String format) {
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = mSimpleDateFormat.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 描述：获取偏移之后的Date.
	 * 
	 * @param date
	 *            日期时间
	 * @param calendarField
	 *            Calendar属性，对应offset的值，
	 *            如(Calendar.DATE,表示+offset天,Calendar.HOUR_OF_DAY,表示＋offset小时)
	 * @param offset
	 *            偏移(值大于0,表示+,值小于0,表示－)
	 * @return Date 偏移之后的日期时间
	 */
	public Date getDateByOffset(Date date, int calendarField, int offset) {
		Calendar c = new GregorianCalendar();
		try {
			c.setTime(date);
			c.add(calendarField, offset);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return c.getTime();
	}

	/**
	 * 描述：获取指定日期时间的字符串(可偏移).
	 * 
	 * @param strDate
	 *            String形式的日期时间
	 * @param format
	 *            格式化字符串，如："yyyy-MM-dd HH:mm:ss"
	 * @param calendarField
	 *            Calendar属性，对应offset的值，
	 *            如(Calendar.DATE,表示+offset天,Calendar.HOUR_OF_DAY,表示＋offset小时)
	 * @param offset
	 *            偏移(值大于0,表示+,值小于0,表示－)
	 * @return String String类型的日期时间
	 */
	public static String getStringByOffset(String strDate, String format,
			int calendarField, int offset) {

		String mDateTime = null;
		try {
			Calendar c = new GregorianCalendar();
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			c.setTime(mSimpleDateFormat.parse(strDate));
			c.add(calendarField, offset);
			mDateTime = mSimpleDateFormat.format(c.getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return mDateTime;
	}

	/**
	 * 描述：Date类型转化为String类型(可偏移).
	 * 
	 * @param date
	 *            当前 new Date
	 * @param format
	 *            日期格式
	 * @param calendarField
	 *            不偏移可用 Calendar.ZONE_OFFSET
	 * @param offset
	 *            不偏移用0
	 * @return String String类型日期时间
	 */
	public static String getStringByOffset(Date date, String format,
			int calendarField, int offset) {
		String strDate = null;
		try {
			Calendar c = new GregorianCalendar();
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			c.setTime(date);
			c.add(calendarField, offset);
			strDate = mSimpleDateFormat.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDate;
	}

	/**
	 * 描述：Date类型转化为String类型.
	 * 
	 * @param date
	 *            the date
	 * @param format
	 *            the format
	 * @return String String类型日期时间
	 */
	public static String getStringByFormat(Date date, String format) {
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
		String strDate = null;
		try {
			strDate = mSimpleDateFormat.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDate;
	}

	/**
	 * 描述：获取指定日期时间的字符串,用于导出想要的格式.
	 * 
	 * @param strDate
	 *            String形式的日期时间，必须为yyyy-MM-dd HH:mm:ss格式
	 * @param format
	 *            输出格式化字符串，如："yyyy-MM-dd HH:mm:ss"
	 * @return String 转换后的String类型的日期时间
	 */
	public static String getStringByFormat(String strDate, String format) {
		String mDateTime = null;
		try {
			Calendar c = new GregorianCalendar();
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(
					dateFormatYMDHMS);
			c.setTime(mSimpleDateFormat.parse(strDate));
			SimpleDateFormat mSimpleDateFormat2 = new SimpleDateFormat(format);
			mDateTime = mSimpleDateFormat2.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mDateTime;
	}

	/**
	 * 描述：获取指定日期时间的字符串,用于导出想要的格式.
	 * 
	 * @param strDate
	 *            String形式的日期时间
	 * @param formatIn
	 *            输入格式化字符串，如："yyyy/MM/dd HH:mm:ss"
	 * @param formatOut
	 *            输出格式化字符串，如："yyyy-MM-dd HH:mm:ss"
	 * @return String 转换后的String类型的日期时间
	 */
	public static String getStringByFormat(String strDate, String formatIn,
			String formatOut) {
		String mDateTime = "";
		if (TextUtils.isEmpty(strDate))
			return mDateTime;
		try {
			Calendar c = new GregorianCalendar();
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(formatIn);
			c.setTime(mSimpleDateFormat.parse(strDate));
			SimpleDateFormat mSimpleDateFormat2 = new SimpleDateFormat(formatOut);
			mDateTime = mSimpleDateFormat2.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mDateTime;
	}

	/**
	 * 描述：获取milliseconds表示的日期时间的字符串.
	 * 
	 * @param milliseconds
	 *            the milliseconds
	 * @param format
	 *            格式化字符串，如："yyyy-MM-dd HH:mm:ss"
	 * @return String 日期时间字符串
	 */
	public static String getStringByFormat(long milliseconds, String format) {
		String thisDateTime = null;
		try {
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			thisDateTime = mSimpleDateFormat.format(milliseconds);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return thisDateTime;
	}

	/**
	 * 获取表示当前日期时间的字符串
	 * 
	 * @return String 返回yyyy-MM-dd HH:mm:ss 格式
	 */
	public static String getCurrentDate() {
		return getCurrentDate(dateFormatYMDHMS);
	}

	/**
	 * 描述：获取表示当前日期时间的字符串.
	 * 
	 * @param format
	 *            格式化字符串，如："yyyy-MM-dd HH:mm:ss"
	 * @return String String类型的当前日期时间
	 */
	public static String getCurrentDate(String format) {
		String curDateTime = null;
		try {
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			Calendar c = new GregorianCalendar();
			curDateTime = mSimpleDateFormat.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return curDateTime;
	}

	/**
	 * 描述：获取表示当前日期时间的字符串(可偏移).
	 * 
	 * @param format
	 *            格式化字符串，如："yyyy-MM-dd HH:mm:ss"
	 * @param calendarField
	 *            Calendar属性，对应offset的值，
	 *            如(Calendar.DATE,表示+offset天,Calendar.HOUR_OF_DAY,表示＋offset小时)
	 * @param offset
	 *            偏移(值大于0,表示+,值小于0,表示－)
	 * @return String String类型的日期时间
	 */
	public static String getCurrentDateByOffset(String format,
			int calendarField, int offset) {
		String mDateTime = null;
		try {
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			Calendar c = new GregorianCalendar();
			c.add(calendarField, offset);
			mDateTime = mSimpleDateFormat.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mDateTime;

	}

	/**
	 * 描述：计算两个日期所差的天数.
	 * 
	 * @param date1
	 *            第一个时间的毫秒表示
	 * @param date2
	 *            第二个时间的毫秒表示
	 * @return int 所差的天数
	 */
	public static int getOffectDay(long date1, long date2) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTimeInMillis(date1);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTimeInMillis(date2);
		// 先判断是否同年
		int y1 = calendar1.get(Calendar.YEAR);
		int y2 = calendar2.get(Calendar.YEAR);
		int d1 = calendar1.get(Calendar.DAY_OF_YEAR);
		int d2 = calendar2.get(Calendar.DAY_OF_YEAR);
		int maxDays = 0;
		int day = 0;
		if (y1 - y2 > 0) {
			maxDays = calendar2.getActualMaximum(Calendar.DAY_OF_YEAR);
			day = d1 - d2 + maxDays;
		} else if (y1 - y2 < 0) {
			maxDays = calendar1.getActualMaximum(Calendar.DAY_OF_YEAR);
			day = d1 - d2 - maxDays;
		} else {
			day = d1 - d2;
		}
		return day;
	}

	/**
	 * 描述：计算两个日期所差的小时数.
	 * 
	 * @param date1
	 *            第一个时间的毫秒表示
	 * @param date2
	 *            第二个时间的毫秒表示
	 * @return int 所差的小时数
	 */
	public static int getOffectHour(long date1, long date2) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTimeInMillis(date1);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTimeInMillis(date2);
		int h1 = calendar1.get(Calendar.HOUR_OF_DAY);
		int h2 = calendar2.get(Calendar.HOUR_OF_DAY);
		int h = 0;
		int day = getOffectDay(date1, date2);
		h = h1 - h2 + day * 24;
		return h;
	}

	/**
	 * 描述：计算两个日期所差的分钟数.
	 * 
	 * @param date1
	 *            第一个时间的毫秒表示
	 * @param date2
	 *            第二个时间的毫秒表示
	 * @return int 所差的分钟数
	 */
	public static int getOffectMinutes(long date1, long date2) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTimeInMillis(date1);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTimeInMillis(date2);
		int m1 = calendar1.get(Calendar.MINUTE);
		int m2 = calendar2.get(Calendar.MINUTE);
		int h = getOffectHour(date1, date2);
		int m = 0;
		m = m1 - m2 + h * 60;
		return m;
	}

	/**
	 * 描述：获取本周一.
	 * 
	 * @param format
	 *            the format
	 * @return String String类型日期时间
	 */
	public static String getFirstDayOfWeek(String format) {
		return getDayOfWeek(format, Calendar.MONDAY);
	}

	/**
	 * 描述：获取本周日.
	 * 
	 * @param format
	 *            the format
	 * @return String String类型日期时间
	 */
	public static String getLastDayOfWeek(String format) {
		return getDayOfWeek(format, Calendar.SUNDAY);
	}

	/**
	 * 描述：获取本周的某一天.
	 * 
	 * @param format
	 *            the format
	 * @param calendarField
	 *            the calendar field
	 * @return String String类型日期时间
	 */
	private static String getDayOfWeek(String format, int calendarField) {
		String strDate = null;
		try {
			Calendar c = new GregorianCalendar();
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			int week = c.get(Calendar.DAY_OF_WEEK);
			if (week == calendarField) {
				strDate = mSimpleDateFormat.format(c.getTime());
			} else {
				int offectDay = calendarField - week;
				if (calendarField == Calendar.SUNDAY) {
					offectDay = 7 - Math.abs(offectDay);
				}
				c.add(Calendar.DATE, offectDay);
				strDate = mSimpleDateFormat.format(c.getTime());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDate;
	}

	/**
	 * 描述：获取本月第一天.
	 * 
	 * @param format
	 *            the format
	 * @return String String类型日期时间
	 */
	public static String getFirstDayOfMonth(String format) {
		String strDate = null;
		try {
			Calendar c = new GregorianCalendar();
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			// 当前月的第一天
			c.set(GregorianCalendar.DAY_OF_MONTH, 1);
			strDate = mSimpleDateFormat.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDate;

	}

	/**
	 * 描述：获取本月最后一天.
	 * 
	 * @param format
	 *            the format
	 * @return String String类型日期时间
	 */
	public static String getLastDayOfMonth(String format) {
		String strDate = null;
		try {
			Calendar c = new GregorianCalendar();
			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat(format);
			// 当前月的最后一天
			c.set(Calendar.DATE, 1);
			c.roll(Calendar.DATE, -1);
			strDate = mSimpleDateFormat.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strDate;
	}

	/**
	 * 描述：获取表示当前日期的0点时间毫秒数.
	 * 
	 * @return the first time of day
	 */
	public static long getFirstTimeOfDay() {
		Date date = null;
		try {
			String currentDate = getCurrentDate(dateFormatYMD);
			date = getDateByFormat(currentDate + " 00:00:00", dateFormatYMDHMS);
			return date.getTime();
		} catch (Exception e) {
		}
		return -1;
	}

	/**
	 * 描述：获取表示当前日期24点时间毫秒数.
	 * 
	 * @return the last time of day
	 */
	public static long getLastTimeOfDay() {
		Date date = null;
		try {
			String currentDate = getCurrentDate(dateFormatYMD);
			date = getDateByFormat(currentDate + " 24:00:00", dateFormatYMDHMS);
			return date.getTime();
		} catch (Exception e) {
		}
		return -1;
	}

	/**
	 * 描述：判断是否是闰年()
	 * <p>
	 * (year能被4整除 并且 不能被100整除) 或者 year能被400整除,则该年为闰年.
	 * 
	 * @param year
	 *            年代（如2012）
	 * @return boolean 是否为闰年
	 */
	public static boolean isLeapYear(int year) {
		if ((year % 4 == 0 && year % 400 != 0) || year % 400 == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 描述：根据时间返回格式化后的时间的描述. 小于1小时显示多少分钟前 大于1小时显示今天＋实际日期，大于今天全部显示实际时间
	 * 
	 * @param strDate
	 *            the str date
	 * @param intFormat
	 *            strDate的日期格式
	 * @return the string
	 */
	public static String formatDateStr2Desc(String strDate, String intFormat) {

		DateFormat df = new SimpleDateFormat(intFormat);
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		try {
			c1.setTime(new Date());
			c2.setTime(df.parse(strDate));
			int d = getOffectDay(c1.getTimeInMillis(), c2.getTimeInMillis());
			if (d == 0) {
				int h = getOffectHour(c1.getTimeInMillis(),
						c2.getTimeInMillis());
				if (h > 0) {
					return "今天" + getStringByFormat(strDate, dateFormatHM);
					// return h + "小时前";
				} else if (h < 0) {
					// return Math.abs(h) + "小时后";
				} else if (h == 0) {
					int m = getOffectMinutes(c1.getTimeInMillis(),
							c2.getTimeInMillis());
					if (m > 0) {
						return m + "分钟前";
					} else if (m < 0) {
						// return Math.abs(m) + "分钟后";
					} else {
						return "刚刚";
					}
				}
			} else if (d == 1) {
				return "昨天" + getStringByFormat(strDate, dateFormatHM);
			} else if (d == 2) {
				return "前天" + getStringByFormat(strDate, dateFormatHM);
				// } else if (d < 7) {
				// int week1 = c1.get(Calendar.DAY_OF_WEEK) - 1;// 当天的星期
				// if (week1 == 0)
				// week1 = week1 + 7;
				// int week2 = c2.get(Calendar.DAY_OF_WEEK) - 1;// strDate日期的星期
				// if (week2 == 0)
				// week2 = week2 + 7;
				// if (week1 >= 4 && week1 > week2) {// 今天、昨天、前天所以从周4开始显示星期
				// return getWeekNumber(strDate, dateFormatYMDHMS) +
				// getStringByFormat(strDate, intFormat, dateFormatHM);
				// }
			} else if (d < 0) {
				if (d == -1) {
					// return "明天"+getStringByFormat(strDate,outFormat);
				} else if (d == -2) {
					// return "后天"+getStringByFormat(strDate,outFormat);
				} else {
					// return Math.abs(d) +
					// "天后"+getStringByFormat(strDate,outFormat);
				}
			}
			int year1 = c1.get(Calendar.YEAR);
			int year2 = c2.get(Calendar.YEAR);
			// 同年只输出月日时分
			return getStringByFormat(strDate, intFormat,
					year1 == year2 ? dateFormatMDHM : dateFormatYMDHM);
		} catch (Exception e) {
		}
		return strDate;
	}

	/**
	 * 取指定日期为星期几.
	 * 
	 * @param strDate
	 *            指定日期
	 * @param inFormat
	 *            指定日期格式
	 * @return String 星期几
	 */
	public static String getWeekNumber(String strDate, String inFormat) {
		String week = "星期日";
		Calendar calendar = new GregorianCalendar();
		DateFormat df = new SimpleDateFormat(inFormat);
		try {
			calendar.setTime(df.parse(strDate));
		} catch (Exception e) {
			return "错误";
		}
		int intTemp = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		switch (intTemp) {
		case 0:
			week = "星期日";
			break;
		case 1:
			week = "星期一";
			break;
		case 2:
			week = "星期二";
			break;
		case 3:
			week = "星期三";
			break;
		case 4:
			week = "星期四";
			break;
		case 5:
			week = "星期五";
			break;
		case 6:
			week = "星期六";
			break;
		}
		return week;
	}

	/**
	 * 比较两个时间的大小
	 * 
	 * @param firstDate
	 *            指定日期
	 * @param secondDate
	 *            指定日期格式
	 * 
	 *            返回值：如果secondDate >firstDate返回true ，否则返回false
	 */
	public static Boolean timeCompare(String firstDate, String secondDate) {

		Boolean returnValue = false;

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cfirstDate = Calendar.getInstance();
		Calendar csecondDate = Calendar.getInstance();
		try {
			cfirstDate.setTime(formatter.parse(firstDate));
			csecondDate.setTime(formatter.parse(secondDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int result = csecondDate.compareTo(cfirstDate);

		if (result > 0) {
			returnValue = true;
		}

		return returnValue;

	}

	/**
	 * 和当前时间的比较
	 * 
	 * @param compareDate
	 *            指定日期
	 * 
	 *            返回值：如果compareDate >当前返回 true，否则返回false
	 */
	public static Boolean timeCompareWithCurrentDate(String compareDate) {

		String currentDate = getCurrentDate("yyyy-MM-dd");

		return timeCompare(currentDate, compareDate);
	}

	/**
	 * 转换日期显示
	 * 
	 * @param date
	 *            格式必须是yyyyMMddHHmmss
	 * @return yyyy年MM月dd日 HH:mm:ss
	 */
	public static String timeCompare(String date) {
		if (TextUtils.isEmpty(date) || date.length() < 14)
			return "";
		else {
			date = date.replace("-", "").replace("-","").replace(" ","").replace(":","").replace("：","");
			String date1 = date.substring(0, 4) + "年" + date.substring(4, 6)
					+ "月" + date.substring(6, 8) + "日 " + date.substring(8, 10)
					+ ":" + date.substring(10, 12) + ":"
					+ date.substring(12, 14);

			return date1;
		}
	}

	/**
	 * 转换日期显示
	 *
	 * @param date
	 *            格式必须是yyyyMMddHHmmss
	 * @return yyyy年MM月dd日 HH:mm:ss
	 */
	public static String timeCompareToLine(String date) {
		if (TextUtils.isEmpty(date) || date.length() < 14)
			return "";
		else {
			String date1 = date.substring(0, 4) + "-" + date.substring(4, 6)
					+ "-" + date.substring(6, 8) + " " + date.substring(8, 10)
					+ ":" + date.substring(10, 12) + ":"
					+ date.substring(12, 14);

			return date1;
		}
	}


	/**
	 * 判断时间是否过期
	 * 
	 * @param stardate
	 *            开始时间
	 * @param duration
	 *            延时时长（单位：小时）
	 * @return
	 */
	public static boolean judgedate(String stardate, float duration) {

		int delayMinutes = (int) (duration);
		String end = DateUtil.getStringByOffset(stardate,
				"yyyy-MM-dd HH:mm:ss", Calendar.MINUTE, delayMinutes);
		Date endTime = DateUtil.getDateByFormat(end, "yyyy-MM-dd HH:mm:ss");
		Date now = DateUtil.getDateByFormat(DateUtil.getCurrentDate(),
				"yyyy-MM-dd HH:mm:ss");

		if (endTime.before(now))
			return true;// 过期
		else
			return false;// 未曾过期
	}

	/**
	 * 获取离线鉴权过期时间分钟数
	 * 
	 * @param delay
	 * @return 分钟数
	 */
	public static int getMinutes(Offline delay) {

		if (delay == null || TextUtils.isEmpty(delay.getBeg_at()))
			return 0;

		int delayMinutes = (int) (delay.getDuration());
		String end = DateUtil.getStringByOffset(delay.getBeg_at(),
				"yyyy-MM-dd HH:mm:ss", Calendar.MINUTE, delayMinutes);
		Date endTime = DateUtil.getDateByFormat(end, "yyyy-MM-dd HH:mm:ss");
		Date now = DateUtil.getDateByFormat(DateUtil.getCurrentDate(),
				"yyyy-MM-dd HH:mm:ss");
		int minutes = DateUtil.getOffectMinutes(endTime.getTime(),
				now.getTime());

		return minutes;
	}

	/**
	 * 比较 时间delay与当前时间分钟数差
	 * 
	 * @param delay
	 * @return 时间分钟数 >0表示 delay>当前时间
	 */
	public static int getMinutes(String delay) {
		if (TextUtils.isEmpty(delay))
			return 0;
		Date endTime = DateUtil.getDateByFormat(delay, "yyyy-MM-dd HH:mm:ss");
		Date now = DateUtil.getDateByFormat(DateUtil.getCurrentDate(),
				"yyyy-MM-dd HH:mm:ss");
		int minutes = DateUtil.getOffectMinutes(endTime.getTime(),
				now.getTime());
		return minutes;
	}

	/**
	 * 时转换成分钟
	 * 
	 * @param hh
	 *            时
	 * @return 分钟数
	 */
	public static float timeCompare(float hh) {

		if (hh >= 24.01) {
			return 0;
		} else {
			return hh * 60;
		}

	}

	/**
	 * 判断mCalendar与mPopupCalendar比较大小
	 * 
	 * @param mCalendar
	 * @param mPopupCalendar
	 * @return false mCalendar<mPopupCalendar
	 */
	public static boolean isDateBefore(Calendar mCalendar, Calendar mPopupCalendar) {
		if (mCalendar.before(mPopupCalendar))
			return false;
		else
			return true;
	}
}
