package com.contron.ekeypublic.util;

import com.google.gson.Gson;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public final class ConfigUtil {
	private static ConfigUtil instance;
	private SharedPreferences appPreferences, defPreferences;
	private SharedPreferences.Editor appEditor;

	private static final String FILESAVEUSER = "appConfig";// 用户设置偏好文件名

	// 初始化
	public static ConfigUtil getInstance(Context context) {
		if (instance == null)
			instance = new ConfigUtil(context);
		return instance;
	}

	private ConfigUtil(Context context) {
		defPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		appPreferences = context.getSharedPreferences(FILESAVEUSER, Context.MODE_PRIVATE);
		appEditor = appPreferences.edit();
	}

	public boolean getDefBoolean(String key) {
		return defPreferences.getBoolean(key, false);
	}

	public String getDefString(String key) {
		return defPreferences.getString(key, "");
	}

	public Long getDefLong(String key) {
		return defPreferences.getLong(key, 0);
	}

	public boolean getBoolean(String key) {
		return appPreferences.getBoolean(key, false);
	}

	public void setBoolean(String key, boolean value) {
		appEditor.putBoolean(key, value).commit();
	}

	public String getString(String key) {
		return appPreferences.getString(key, "");
	}

	public void setString(String key, String value) {
		appEditor.putString(key, value).commit();
	}

	public int getInt(String key) {
		return appPreferences.getInt(key, 0);
	}

	public void setInt(String key, int value) {
		appEditor.putInt(key, value).commit();
	}

	public void setObject(String key, Object src) {
		Gson gson = new Gson();
		String json = gson.toJson(src);
		appEditor.putString(key, json).apply();
	}

	public <T> T getObject(String key, Class<T> classOfT) {
		Gson gson = new Gson();
		return gson.fromJson(appPreferences.getString(key, ""), classOfT);
	}
}
