package com.contron.ekeypublic.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import com.contron.ekeypublic.util.DateUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 版本工具类 VersionUtil.java
 * 
 * @author hupei
 */

public class VersionUtil {
	/**
	 * 获升级取版本号，随升级次数上升
	 * 
	 * @param context
	 *            上下文
	 * @return versionCode
	 * @throws NameNotFoundException
	 */
	public static int getVersionCode(Context context) {
		PackageInfo packageInfo = getPackageInfo(context);
		if (packageInfo != null)
			return packageInfo.versionCode;
		return -1;
	}

	/**
	 * 获升级取版本号，用户看到的
	 * 
	 * @param context
	 *            上下文
	 * @return versionName
	 * @throws NameNotFoundException
	 */
	public static String getVersionName(Context context) {
		PackageInfo packageInfo = getPackageInfo(context);
		if (packageInfo != null)
			return packageInfo.versionName;
		return "";
	}

	public static String getLastUpdateTime(Context context, String dateTimeFormat) {
		long lastUpdateTime = 0;
		String str = "";
		PackageInfo packageInfo = getPackageInfo(context);
		if (packageInfo != null)
			lastUpdateTime = packageInfo.lastUpdateTime;
		if (lastUpdateTime > 0) {
			Date date = new Date(lastUpdateTime);
			str = DateUtil.getStringByFormat(date, dateTimeFormat);
		}
		return str;
	}

	private static PackageInfo getPackageInfo(Context context) {
		// 获取PackageManager 实例
		final PackageManager packageManager = context.getPackageManager();
		// 获得context所属类的包名，0表示获取版本信息
		PackageInfo packageInfo = null;
		try {
			packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return packageInfo;
	}
}
