/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.contron.ekeypublic.update;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public class UpdateInfo {
	public static final String KEY_VERSION_CODE = "versionCode";
	public static final String KEY_VERSION_NAME = "versionName";
	public static final String KEY_DOWNLOAD_URL = "downloadUrl";
	public static final String KEY_UPDATE_LOG = "updateLog";

	public int versionCode;
	public String versionName;
	public String downloadUrl;
	public String updateLog;

	public static UpdateInfo parseXml(String strXML) throws IOException {
		UpdateInfo updateInfo = null;
		XmlPullParser xmlParser = Xml.newPullParser();
		InputStream inputStream=null;
		try {
			 inputStream = new ByteArrayInputStream(strXML.getBytes());			
			xmlParser.setInput(inputStream, "utf-8");
//			xmlParser.setInput(new StringReader(strXML));//字符流会有编码的问题
			int evtType = xmlParser.getEventType();
			while (evtType != XmlPullParser.END_DOCUMENT) {
				String tag = xmlParser.getName();
				switch (evtType) {
				case XmlPullParser.START_TAG:
					if (tag.equalsIgnoreCase("android")) {
						updateInfo = new UpdateInfo();
					} else if (updateInfo != null) {
						if (tag.equalsIgnoreCase(KEY_VERSION_CODE)) {
							updateInfo.versionCode = Integer.parseInt(xmlParser.nextText());
						} else if (tag.equalsIgnoreCase(KEY_VERSION_NAME)) {
							updateInfo.versionName = xmlParser.nextText();
						} else if (tag.equalsIgnoreCase(KEY_DOWNLOAD_URL)) {
							updateInfo.downloadUrl = xmlParser.nextText();
						} else if (tag.equalsIgnoreCase(KEY_UPDATE_LOG)) {
							updateInfo.updateLog = (xmlParser.nextText());
						}
					}
					break;
				case XmlPullParser.END_TAG:
					break;
				}
				evtType = xmlParser.next();
			}
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}finally{		
			if(inputStream!=null)
				inputStream.close();
		}
		return updateInfo;
	}
}