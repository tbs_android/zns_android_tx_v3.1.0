/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.update;

import java.io.File;
import java.io.IOException;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.Toast;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.util.AppUtils;
import com.contron.ekeypublic.util.ConfigUtil;
import com.contron.ekeypublic.util.FileUtil;
import com.contron.ekeypublic.util.VersionUtil;
import com.contron.ekeypublic.view.MsgBoxFragment;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.util.LogUtils;

public class UpdateManage {
	private Activity mActivity;
	private DownloadManager mDownloadManager;
	private DownloadCompleteReceiver mReceiver;
	private long downloadId;
	private String mwld;

	public UpdateManage(Activity activity, String wld) {
		this.mActivity = activity;
		mwld = wld;
		// 获取下载服务
		mDownloadManager = (DownloadManager) activity
				.getSystemService(Activity.DOWNLOAD_SERVICE);
		mReceiver = new DownloadCompleteReceiver();
	}

	public void start(String appVersionUrl) {
		start(appVersionUrl, false);
	}

	public void start(final String appVersionUrl, final boolean isShowDialog) {
		if (TextUtils.isEmpty(appVersionUrl))
			return;
		new HttpUtils(30 * 1000).send(HttpMethod.GET, appVersionUrl,
				new RequestCallBack<String>() {
					ProgressDialog requestDialog;

					@Override
					public void onStart() {
						super.onStart();
						if (isShowDialog) {
							requestDialog = ProgressDialog.show(mActivity, "",
									"正在检查是否有更新版本，请稍候...");
						}
					}

					@Override
					public void onFailure(HttpException arg0, String arg1) {
						if (requestDialog != null) {
							requestDialog.dismiss();
							requestDialog = null;
						}
						if (isShowDialog)
							Toast.makeText(mActivity, arg1, Toast.LENGTH_LONG)
									.show();
					}

					@Override
					public void onSuccess(ResponseInfo<String> arg0) {
						if (requestDialog != null) {
							requestDialog.dismiss();
							requestDialog = null;
						}
						if (arg0 != null && !TextUtils.isEmpty(arg0.result)) {
							checkVersion(arg0);
						}
					}
				});
	}

	/**
	 * 解析服务下载的XML文件
	 * 
	 * @param arg0
	 */
	private void checkVersion(ResponseInfo<String> arg0) {
		try {
			final UpdateInfo updateInfo = UpdateInfo.parseXml(arg0.result);
			if (updateInfo == null)
				return;
			if (VersionUtil.getVersionCode(mActivity) < updateInfo.versionCode) {

				showMsgBox("新版本V" + updateInfo.versionName,
						updateInfo.updateLog, false,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								downApp(updateInfo);
							}
						}, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();

							}
						});

				// final ProgressDialog progressDialog = new
				// ProgressDialog(mActivity);
				// progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				// progressDialog.setCancelable(false);
				// progressDialog.setIcon(R.drawable.ic_launcher);
				// progressDialog.setTitle("新版本V" + updateInfo.versionName);
				// progressDialog.setMessage(updateInfo.updateLog);
				// progressDialog.setButton(DialogInterface.BUTTON_POSITIVE,
				// "升级", new OnClickListener() {
				//
				// @Override
				// public void onClick(DialogInterface dialog, int which) {
				// downApp(updateInfo);
				// }
				// });
				// progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
				// "忽略", new OnClickListener() {
				//
				// @Override
				// public void onClick(DialogInterface dialog, int which) {
				// }
				// });
				// progressDialog.show();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void downApp(final UpdateInfo updateInfo) {
		mActivity.registerReceiver(mReceiver, new IntentFilter(
				DownloadManager.ACTION_DOWNLOAD_COMPLETE));

		String uri = mwld + updateInfo.downloadUrl;

		// 文件名：文件名_服务器版本号。如：Ekey_min_1.0.1.apk
		String fileName = updateInfo.versionName + "_"
				+ FileUtil.getFileName(uri);
		// 创建下载请求
		DownloadManager.Request down = new DownloadManager.Request(
				Uri.parse(uri));
		// 设置允许使用的网络类型，这里是移动网络和wifi都可以
		down.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE
				| DownloadManager.Request.NETWORK_WIFI);
		// 禁止发出通知，既后台下载
		down.setNotificationVisibility(Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
		// 显示下载界面
		down.setVisibleInDownloadsUi(true);
		// 设置下载后文件存放的位置
		down.setDestinationInExternalFilesDir(mActivity, null, fileName);
		down.setTitle("ekeyCommunication");
		// 将下载请求放入队列
		downloadId = mDownloadManager.enqueue(down);
	}

	// 接受下载完成后的intent
	class DownloadCompleteReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(
					DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
				long downId = intent.getLongExtra(
						DownloadManager.EXTRA_DOWNLOAD_ID, -1);
				if (downId == downloadId) {
					Uri uri = mDownloadManager.getUriForDownloadedFile(downId);
					if (uri != null)
						AppUtils.install(context, uri);
					context.unregisterReceiver(this);
				}
			}
		}
	}

	public void showMsgBox(String title, String message, boolean cancel,
			DialogInterface.OnClickListener okListener,
			DialogInterface.OnClickListener noListener) {
		MsgBoxFragment boxFragment = new MsgBoxFragment();
		boxFragment.setTitle(title);
		boxFragment.setMessage(message);
		boxFragment.setPositiveOnClickListener(okListener);
		boxFragment.setNegativeOnClickListener(noListener);
		boxFragment.setCanceledOnTouchOutside(cancel);
		boxFragment.show(mActivity.getFragmentManager(), "msgBox");

	}

}
