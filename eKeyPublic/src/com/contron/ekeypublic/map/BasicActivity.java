package com.contron.ekeypublic.map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.AMapNaviListener;
import com.amap.api.navi.AMapNaviView;
import com.amap.api.navi.model.AMapLaneInfo;
import com.amap.api.navi.model.AMapNaviCross;
import com.amap.api.navi.model.AMapNaviInfo;
import com.amap.api.navi.model.AMapNaviLocation;
import com.amap.api.navi.model.AMapNaviTrafficFacilityInfo;
import com.amap.api.navi.model.AimLessModeCongestionInfo;
import com.amap.api.navi.model.AimLessModeStat;
import com.amap.api.navi.model.NaviInfo;
import com.amap.api.navi.view.RouteOverLay;
import com.autonavi.tbt.TrafficFacilityInfo;
import com.contron.ekeypublic.R;

public class BasicActivity extends Activity {

	protected Context mContext;

	// 地图相关
	protected MapView mMapView;
	protected AMap aMap;
	// 导航地图相关
	protected AMapNaviView mAmapAMapNaviView;
	protected AMapNaviListener mAMapNaviListener;

	// 地图和导航核心逻辑类
	protected AMapNavi mAmapNavi;
	// 规划线路
	protected RouteOverLay mRouteOverLay;

	// 路径规划成功事件
	private OnCalculateRouteListener calculateRouteListener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		View mapView = findViewById(R.id.map);
		if (mapView instanceof MapView) {
			mMapView = (MapView) mapView;
			if (mMapView != null) {
				aMap = mMapView.getMap();
				mMapView.onCreate(savedInstanceState); // 此方法必须重写，且以上顺序不能乱
				// if (mAmap != null)
				// mAmap.setLocationSource(this);// 设置定位资源。如果不设置此定位资源则定位按钮不可点击。
			}
		} else if (mapView instanceof AMapNaviView) {
			mAmapAMapNaviView = (AMapNaviView) mapView;
			if (mAmapAMapNaviView != null)
				mAmapAMapNaviView.onCreate(savedInstanceState);
		}

	

		startNaviListener();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mMapView != null)
			mMapView.onResume();// 此方法必须重写
		if (mAmapAMapNaviView != null)
			mAmapAMapNaviView.onResume();
		if (mAMapNaviListener != null && mAmapNavi != null)
			mAmapNavi.setAMapNaviListener(mAMapNaviListener);
		// TTSController.getInstance(this).startSpeaking();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mMapView != null)
			mMapView.onPause();// 此方法必须重写
		if (mAmapAMapNaviView != null)
			mAmapAMapNaviView.onPause();
		if (mAMapNaviListener != null && mAmapNavi != null)
			mAmapNavi
					.removeAMapNaviListener(mAMapNaviListener);
		// TTSController.getInstance(this).stopSpeaking();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mMapView != null)
			mMapView.onDestroy();// 此方法必须重写
		if (mAmapAMapNaviView != null)
			mAmapAMapNaviView.onDestroy();
		// deactivate();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mMapView != null)
			mMapView.onSaveInstanceState(outState);// 此方法必须重写
		if (mAmapAMapNaviView != null)
			mAmapAMapNaviView.onSaveInstanceState(outState);
	}

	private void startNaviListener() {

		// 导航监听
		mAMapNaviListener = new AMapNaviListener() {

			@Override
			public void onArriveDestination() {
				// TODO 到达目的地后回调函数
//				TTSController.getInstance(mContext).playText(R.string.tts_message_01);
			}

			@Override
			public void onArrivedWayPoint(int arg0) {
				// TODO 驾车路径导航到达某个途经点的回调函数
			}

			@Override
			public void onCalculateRouteFailure(int arg0) {
				// TODO 步行或者驾车路径规划失败后的回调函数
				// dissmissDialog();
				// showToast(R.string.tts_message_02);
//				TTSController.getInstance(mContext).playText(
//						R.string.tts_message_02);
				finish();
			}

			@Override
			public void onCalculateRouteSuccess() {
				// TODO 步行或者驾车路径规划成功后的回调函数
//				TTSController.getInstance(mContext).playText(
//						R.string.tts_message_03);
				if (calculateRouteListener != null) {
					// dissmissDialog();
					calculateRouteListener.onCalculateRouteSuccess();
				}
			}

			@Override
			public void onEndEmulatorNavi() {
				// TODO 模拟导航停止后回调函数
//				TTSController.getInstance(mContext).playText(
//						R.string.tts_message_04);
			}

			@Override
			public void onGetNavigationText(int arg0, String arg1) {
				// TODO 导航播报信息回调函数
//				TTSController.getInstance(mContext).playText(arg1);
			}

			@Override
			public void onInitNaviFailure() {
				// TODO 导航创建失败时的回调函数
			}

			@Override
			public void onInitNaviSuccess() {
				// TODO 导航创建成功时的回调函数
			}

			@Override
			public void onLocationChange(AMapNaviLocation arg0) {
				// TODO 当GPS位置有更新时的回调函数
			}

			@Override
			public void onReCalculateRouteForTrafficJam() {
				// TODO 驾车导航时，如果前方遇到拥堵时需要重新计算路径的回调
//				TTSController.getInstance(mContext).playText(
//						R.string.tts_message_05);
			}

			@Override
			public void onReCalculateRouteForYaw() {
				// TODO 步行或驾车导航时,出现偏航后需要重新计算路径的回调函数
//				TTSController.getInstance(mContext).playText(
//						R.string.tts_message_06);
			}

			@Override
			public void onStartNavi(int arg0) {
				// TODO 启动导航后回调函数
			}

			@Override
			public void onTrafficStatusUpdate() {
				// TODO 当前方路况光柱信息有更新时回调函数
			}

			@Override
			public void onGpsOpenStatus(boolean arg0) {
				// TODO 用户手机GPS设置是否开启的回调函数
			}

			@Override
			public void onNaviInfoUpdated(AMapNaviInfo arg0) {
				// TODO 当驾车或者步行实时导航或者模拟导航有位置变化时都会回调
			}

			@Override
			public void OnUpdateTrafficFacility(AMapNaviTrafficFacilityInfo arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void OnUpdateTrafficFacility(TrafficFacilityInfo arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void OnUpdateTrafficFacility(
					AMapNaviTrafficFacilityInfo[] arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void hideCross() {
				// TODO Auto-generated method stub

			}

			@Override
			public void hideLaneInfo() {
				// TODO Auto-generated method stub

			}

			@Override
			public void onCalculateMultipleRoutesSuccess(int[] ints) {

			}

			@Override
			public void notifyParallelRoad(int arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onNaviInfoUpdate(NaviInfo arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void showCross(AMapNaviCross arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void showLaneInfo(AMapLaneInfo[] arg0, byte[] arg1,
					byte[] arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void updateAimlessModeCongestionInfo(
					AimLessModeCongestionInfo arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void updateAimlessModeStatistics(AimLessModeStat arg0) {
				// TODO Auto-generated method stub

			}
		};
	}

	/**
	 * 设置路径规划成功事件
	 * 
	 * @param calculateRouteListener
	 *            the calculateRouteListener to set
	 */
	public void setOnCalculateRouteListener(
			OnCalculateRouteListener calculateRouteListener) {
		this.calculateRouteListener = calculateRouteListener;
	}

	public interface OnCalculateRouteListener {
		public void onCalculateRouteSuccess();
	}
}
