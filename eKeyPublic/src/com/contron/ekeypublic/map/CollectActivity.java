package com.contron.ekeypublic.map;

import java.util.List;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.AMapLocationClientOption.AMapLocationMode;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.contron.ekeypublic.R;
import com.contron.ekeypublic.R.id;
import com.contron.ekeypublic.R.layout;
import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.db.EkeyDao;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.tts.TTSController;
import com.contron.ekeypublic.view.MsgBoxFragment;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.util.LogUtils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class CollectActivity extends Activity implements LocationSource,
		AMapLocationListener, OnClickListener {

	private AMap aMap;
	private MapView mapView;
	private OnLocationChangedListener mListener;
	private AMapLocationClient mlocationClient;
	private AMapLocationClientOption mLocationOption;
	private LatLng latlng;
	private int deviceId;// 当前采集坐标设备ID
	private Activity mActivity;
	private TextView tv_lng;// 显示当前经度
	private TextView tv_lat;// 显示当前纬度
	// 实例化服务访问实例
	private RequestHttp requestHttp;// 请求类
	private String wld = "";
	private Bundle mBundle;
	private String k = "";
	private Intent intent;

	public static int REQUESTCODE = 3;
	public static int RESULTCODE = 4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 不显示程序的标题栏
		setContentView(R.layout.activity_collect);
		mActivity = this;
		mapView = (MapView) findViewById(R.id.map);
		tv_lng = (TextView) findViewById(R.id.tv_lng);
		tv_lat = (TextView) findViewById(R.id.tv_lat);
		mapView.onCreate(savedInstanceState);// 此方法必须重写

		intent = getIntent();
		mBundle = intent.getExtras();
		if (mBundle != null) {
			deviceId = mBundle.getInt("deviceId");
			wld = mBundle.getString("wld");
			k = mBundle.getString("k");
		}

		requestHttp = new RequestHttp(wld);
		init();
	}

	/**
	 * 初始化
	 */
	private void init() {
		if (aMap == null) {
			aMap = mapView.getMap();
		}
		Button but_save = (Button) findViewById(R.id.but_save);
		but_save.setOnClickListener(this);

		aMap.setLocationSource(this);// 设置定位监听
		aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
		aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
		// 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
		aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);
		
		aMap.getUiSettings().setScaleControlsEnabled(true);// 显示比例
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mapView.onResume();
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onPause() {
		super.onPause();
		mapView.onPause();
		deactivate();
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mapView.onSaveInstanceState(outState);
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
		if (null != mlocationClient) {
			mlocationClient.onDestroy();
		}

	}

	/**
	 * 定位成功后回调函数
	 */
	@Override
	public void onLocationChanged(AMapLocation amapLocation) {
		if (mListener != null && amapLocation != null) {
			if (amapLocation != null && amapLocation.getErrorCode() == 0) {
				mListener.onLocationChanged(amapLocation);// 显示系统小蓝点
				latlng = new LatLng(amapLocation.getLatitude(),
						amapLocation.getLongitude());
				tv_lng.setText("当前经度：" + latlng.longitude);
				tv_lat.setText("当前纬度：" + latlng.latitude);
				
				
				aMap.moveCamera(CameraUpdateFactory
						.newCameraPosition(new CameraPosition(latlng, 20, 30, 0)));
				
			} else {
				latlng = null;
				tv_lng.setText("当前经度：0.0");
				tv_lat.setText("当前纬度：0.0");
				String errText = "定位失败," + amapLocation.getErrorCode() + ": "
						+ amapLocation.getErrorInfo();
				Log.e("AmapErr", errText);
			}
		}
	}

	/**
	 * 激活定位
	 */
	@Override
	public void activate(OnLocationChangedListener listener) {
		mListener = listener;
		if (mlocationClient == null) {
			mlocationClient = new AMapLocationClient(this);
			mLocationOption = new AMapLocationClientOption();
			// 设置定位监听
			mlocationClient.setLocationListener(this);
			// 设置为高精度定位模式
			mLocationOption.setLocationMode(AMapLocationMode.Hight_Accuracy);
			// 设置定位参数
			mlocationClient.setLocationOption(mLocationOption);
			// 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
			// 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
			// 在定位结束后，在合适的生命周期调用onDestroy()方法
			// 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
			mlocationClient.startLocation();
		}
	}

	/**
	 * 停止定位
	 */
	@Override
	public void deactivate() {
		mListener = null;
		if (mlocationClient != null) {
			mlocationClient.stopLocation();
			mlocationClient.onDestroy();
		}
		mlocationClient = null;
	}

	public static void startActivity(Activity activity, int deviceId,
			String wld, String k) {
		Intent intent = new Intent(activity, CollectActivity.class);
		Bundle bundle = new Bundle();
		bundle.putInt("deviceId", deviceId);
		bundle.putString("wld", wld);
		bundle.putString("k", k);
		intent.putExtras(bundle);
		activity.startActivity(intent);
	}

	public static void startActivityForResult(Fragment fragment) {
		fragment.startActivityForResult(new Intent(fragment.getActivity(),
				CollectActivity.class), CollectActivity.REQUESTCODE);
	}

	@Override
	public void onClick(View arg0) {

		if (arg0.getId() == R.id.but_save) {

			if (latlng != null) {

				if (mBundle != null)
					updateLocation();
				else {
					intent.putExtra("latlng", latlng);
					mActivity.setResult(RESULTCODE, intent);
					finish();
				}
			} else {

				showMsgBox("提示", "请定位成功后,再保存设备坐标！", false,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {

							}
						}, null);
			}
		}

	}

	/**
	 * 上传设备坐标
	 */
	private void updateLocation() {

		User muser = new User();
		muser.setKValue(k);

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("lat", String.valueOf(latlng.latitude));
		prams.putParams("lng", String.valueOf(latlng.longitude));

		String url = String.format(Globals.OBJECT_OID_LOCATION, deviceId);

		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {

			@Override
			public void resultValue(final String value) {

				mActivity.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						if (value.contains("success")) {// 返回成功

							DBHelp.getInstance(mActivity)
									.getEkeyDao()
									.saveObjectLatLng(deviceId,
											latlng.latitude, latlng.longitude);

							showMsgBox("提示", "保存设备坐标成功！", false,
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface arg0, int arg1) {
											mActivity.finish();
										}

									}, null);

						} else {// 返回失败

							showMsgBox("提示", "保存设备坐标失败,请检查网络后重试！", false,
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface arg0, int arg1) {

										}

									}, null);

						}

					}
				});

			}
		});
	}

	/**
	 * 对话框
	 * 
	 * @param title
	 * @param message
	 * @param cancel
	 * @param okListener
	 * @param noListener
	 */
	public void showMsgBox(String title, String message, boolean cancel,
			DialogInterface.OnClickListener okListener,
			DialogInterface.OnClickListener noListener) {
		MsgBoxFragment boxFragment = new MsgBoxFragment();
		boxFragment.setTitle(title);
		boxFragment.setMessage(message);
		boxFragment.setPositiveOnClickListener(okListener);
		boxFragment.setNegativeOnClickListener(noListener);
		boxFragment.setCanceledOnTouchOutside(cancel);
		boxFragment.show(getFragmentManager(), "msgBox");

	}
	
}
