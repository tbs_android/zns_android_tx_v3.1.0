package com.contron.ekeypublic.map;

import java.util.ArrayList;
import java.util.List;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.AMapLocationClientOption.AMapLocationMode;
import com.amap.api.maps.AMap;
import com.amap.api.maps.AMap.InfoWindowAdapter;
import com.amap.api.maps.AMap.OnCameraChangeListener;
import com.amap.api.maps.AMap.OnMapClickListener;
import com.amap.api.maps.AMap.OnMapLoadedListener;
import com.amap.api.maps.AMap.OnMarkerClickListener;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.MapView;
import com.amap.api.maps.LocationSource.OnLocationChangedListener;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.overlay.PoiOverlay;
import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.AMapNaviListener;
import com.amap.api.navi.model.AMapLaneInfo;
import com.amap.api.navi.model.AMapNaviCross;
import com.amap.api.navi.model.AMapNaviInfo;
import com.amap.api.navi.model.AMapNaviLocation;
import com.amap.api.navi.model.AMapNaviPath;
import com.amap.api.navi.model.AMapNaviTrafficFacilityInfo;
import com.amap.api.navi.model.AimLessModeCongestionInfo;
import com.amap.api.navi.model.AimLessModeStat;
import com.amap.api.navi.model.NaviInfo;
import com.amap.api.navi.model.NaviLatLng;
import com.amap.api.navi.view.RouteOverLay;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.core.PoiItem;
import com.autonavi.tbt.TrafficFacilityInfo;
import com.contron.ekeypublic.R;
import com.contron.ekeypublic.R.layout;
import com.contron.ekeypublic.db.DBHelp;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.tts.TTSController;
import com.contron.ekeypublic.view.ClearEditText;
import com.contron.ekeypublic.view.MsgBoxFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class DisplayActivity extends Activity implements LocationSource,
		AMapLocationListener, OnMapClickListener, OnMapLoadedListener,
		OnMarkerClickListener, InfoWindowAdapter, OnCameraChangeListener,
		OnClickListener {

	private RadioGroup radbut;// 路径方式
	private AMap aMap;
	private MapView mapView;
	private ClearEditText mSearchEditText;
	private OnLocationChangedListener mListener;
	private AMapLocationClient mlocationClient;
	private AMapLocationClientOption mLocationOption;
	private ArrayList<DeviceObject> listDevice = new ArrayList<DeviceObject>();
	private ArrayList<DeviceObject> allDevices = new ArrayList<DeviceObject>();
	private int tempId = 0;
	private Marker currentMarker;// 当前选择标注
	private NaviLatLng startLatLng;// 路径始
	private NaviLatLng endLatLng;// 路径终
	String includeState = "";// ok存在有经纬度的设备

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 不显示程序的标题栏
		setContentView(R.layout.activity_display);
        radbut = findViewById(R.id.radbut);
		tempId = getIntent().getExtras().getInt("tempId", 0);
		if (tempId != 0) {
            listDevice = DBHelp.getInstance(this).getEkeyDao()
                    .getTempDevice(tempId);
        } else {
            allDevices = getIntent().getParcelableArrayListExtra("deviceObjects");
            listDevice.addAll(allDevices);
		    radbut.setVisibility(View.INVISIBLE);
        }
        mapView = findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);// 此方法必须重写
        mSearchEditText = findViewById(R.id.et_search);
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                searchStation();
            }
        });
		init();

	}

	/**
	 * 初始化
	 */
	private void init() {

//		TTSController.getInstance(this).init();

		if (aMap == null) {
			aMap = mapView.getMap();
		}
		aMap.setLocationSource(this);// 设置定位监听
		aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
		aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
		// 设置定位的类型为定位模式 ，可以由定位、跟随或地图根据面向方向旋转几种
		aMap.setMyLocationType(AMap.LOCATION_TYPE_LOCATE);

		aMap.getUiSettings().setScaleControlsEnabled(true);// 显示比例

		// 设置点击地图事件
		aMap.setOnMapClickListener(this);
		aMap.setOnMapLoadedListener(this);
		aMap.setOnMarkerClickListener(this);
		aMap.setInfoWindowAdapter(this);
		aMap.setOnCameraChangeListener(this);

		addMarkersToMap();
	}

	/**
	 * 在地图上添加marker
	 */
	private void addMarkersToMap() {
		if (listDevice != null) {
			ArrayList<MarkerOptions> listMarkerOption = new ArrayList<MarkerOptions>();

			for (DeviceObject device : listDevice) {

				LatLng latlng = new LatLng(device.getLat(), device.getLng());

				if (device.getLat() > 0 && device.getLng() > 0) {

					TextView view = (TextView) View.inflate(this,
							R.layout.marker_icon, null);
					view.setText(device.getName());
					view.setTextColor(Color.BLUE);
					view.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
							R.drawable.marker_off);

					MarkerOptions markerOption = new MarkerOptions()
							.title(device.getName()).position(latlng)
							.icon(BitmapDescriptorFactory.fromView(view))
							.draggable(false).snippet(device.getSection());

					listMarkerOption.add(markerOption);
				}
			}

			aMap.addMarkers(listMarkerOption, false);
		}

	}

    /**
     * 根据站名和区域模糊搜索站点，并在地图上刷新显示检索的站点位置
     */
	private void searchStation() {
	    aMap.clear();
	    String searchText = mSearchEditText.getText().toString();
	    if (!"".equals(searchText)) {
            listDevice.clear();
            for (DeviceObject deviceObject : allDevices) {
                if (deviceObject.getName() != null && deviceObject.getName().contains(searchText)) {
                    listDevice.add(deviceObject);
                } else if (deviceObject.getSection() != null && deviceObject.getSection().contains(searchText)) {
                    listDevice.add(deviceObject);
                } else {
                    continue;
                }
            }
        } else {
	        listDevice.addAll(allDevices);
        }
        addMarkersToMap();
    }

	/**
	 * 定位成功后回调函数
	 */
	@Override
	public void onLocationChanged(AMapLocation amapLocation) {
		if (mListener != null && amapLocation != null) {
			if (amapLocation != null && amapLocation.getErrorCode() == 0) {

				LatLng latlng = new LatLng(amapLocation.getLatitude(),
						amapLocation.getLongitude());

				startLatLng = new NaviLatLng(latlng.latitude, latlng.longitude);
                mListener.onLocationChanged(amapLocation);// 显示系统小蓝点
				// 定位到当前地点
				if ("no".equals(includeState)) {
					aMap.moveCamera(CameraUpdateFactory
							.newCameraPosition(new CameraPosition(latlng, 20,
									30, 0)));
				}

			} else {
				String errText = "定位失败," + amapLocation.getErrorCode() + ": "
						+ amapLocation.getErrorInfo();
				Log.e("AmapErr", errText);
			}
		}
	}

	/**
	 * 激活定位
	 */
	@Override
	public void activate(OnLocationChangedListener listener) {
		mListener = listener;
		if (mlocationClient == null) {
			mlocationClient = new AMapLocationClient(this);
			mLocationOption = new AMapLocationClientOption();
			// 设置定位监听
			mlocationClient.setLocationListener(this);
			// 设置为高精度定位模式
			mLocationOption.setLocationMode(AMapLocationMode.Hight_Accuracy);
			// 设置定位参数
			mlocationClient.setLocationOption(mLocationOption);
			// 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
			// 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
			// 在定位结束后，在合适的生命周期调用onDestroy()方法
			// 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
			mlocationClient.startLocation();
		}
	}

	/**
	 * 停止定位
	 */
	@Override
	public void deactivate() {
		mListener = null;
		if (mlocationClient != null) {
			mlocationClient.stopLocation();
			mlocationClient.onDestroy();
		}
		mlocationClient = null;
	}

	public static void startActivity(Activity activity, int tempId) {
		Intent intent = new Intent(activity, DisplayActivity.class);
		intent.putExtra("tempId", tempId);
		activity.startActivity(intent);
	}

	public static void startActivity(Activity activity, ArrayList<DeviceObject> deviceObjects) {
		Intent intent = new Intent(activity, DisplayActivity.class);
		intent.putParcelableArrayListExtra("deviceObjects", deviceObjects);
		activity.startActivity(intent);
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mapView.onResume();
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onPause() {
		super.onPause();
		mapView.onPause();
		deactivate();
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mapView.onSaveInstanceState(outState);
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mapView.onDestroy();
		if (null != mlocationClient) {
			mlocationClient.onDestroy();
		}

		// 主界面摧毁语音，其它子界面只是开始停止
//		TTSController.getInstance(this).destroy();
	}

	private void moveMarkers() {
		// TODO 设置所有maker显示在当前可视区域地图中
		includeState = "no";
		int size = listDevice.size();
		if (size > 0) {
			LatLngBounds.Builder bounds = new LatLngBounds.Builder();
			for (int i = 0; i < size; i++) {
				LatLng latlng = new LatLng(listDevice.get(i).getLat(),
						listDevice.get(i).getLng());
				if (latlng.latitude > 0 && latlng.longitude > 0) {
					bounds.include(latlng);
					includeState = "ok";
				}
			}
			// 参数2：padding - 设置区域和view之间的空白距离，单位像素。这个值适用于区域的四个边。
			if ("ok".equals(includeState)) {
				LatLngBounds build = bounds.build();
				aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(build, 400));
			}
		}
	}

	@Override
	public void onMapClick(LatLng arg0) {

		if (currentMarker != null && currentMarker.isInfoWindowShown()) {
			currentMarker.hideInfoWindow();
			currentMarker = null;// 已处清空当前点击或者拖拽的marker，不需要再使用
		}

	}

	@Override
	public void onCameraChange(CameraPosition arg0) {
		// 可视范围改变时回调此方法
		if (currentMarker != null && currentMarker.isInfoWindowShown())
			currentMarker.hideInfoWindow();// 移动地图时隐藏

	}

	@Override
	public void onCameraChangeFinish(CameraPosition arg0) {
		// 可视范围改变完成后回调此方法
		if (currentMarker != null && !currentMarker.isInfoWindowShown())
			// 移动地图后显示，会触发 setInfoWindowAdapter 回调
			currentMarker.showInfoWindow();

	}

	@Override
	public View getInfoContents(Marker arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		View view = View.inflate(this, R.layout.custom_info_window, null);
		setInfoWindow(marker, view);
		return view;
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		currentMarker = marker;
		endLatLng = new NaviLatLng(marker.getPosition().latitude,
				marker.getPosition().longitude);

		if (marker.isInfoWindowShown())
			marker.hideInfoWindow();
		else
			marker.showInfoWindow();

		return true;
	}

	@Override
	public void onMapLoaded() {
		// 当地图载入成功后回调此方法
		moveMarkers();

	}

	private void setInfoWindow(Marker marker, View view) {
		// 填infoWindow窗口内容
		TextView tvInfoWindowTitle = view
				.findViewById(R.id.info_window_title);
		TextView tvInfoWindowSnippet = view
				.findViewById(R.id.info_window_snippet);
		ImageButton btninfoWindowRoute = view
				.findViewById(R.id.info_window_btn_route);
		Button btnBluetoothOpenDoor = view.findViewById(R.id.but_bluetooth_openDoor);
		Button btnScannerOpenDoor = view.findViewById(R.id.but_scanner_openDoor);
		btninfoWindowRoute.setOnClickListener(this);
        btnBluetoothOpenDoor.setOnClickListener(this);
        btnScannerOpenDoor.setOnClickListener(this);
		String title = marker.getTitle();
		String snippet = marker.getSnippet();
		tvInfoWindowTitle.setText(TextUtils.isEmpty(title) ? "" : title);
		tvInfoWindowSnippet.setText(TextUtils.isEmpty(snippet) ? "" : snippet);
	}

	@Override
	public void onClick(View arg0) {
        int i = arg0.getId();
        if (i == R.id.info_window_btn_route) {
            if (startLatLng == null) {
//				TTSController.getInstance(this).playText("定位失败，请检查网络！");
                Toast.makeText(this, "定位失败，请检查网络！", Toast.LENGTH_LONG).show();
            } else {
//				RouteActivity.startActivity(this, startLatLng, endLatLng, calculateType());// 路径规化
                if (checkApkExist(this, "com.autonavi.minimap")) {
                    this.showMsgBox("导航", "是否跳转到高德地图进行导航？", false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            goToGaode(String.valueOf(endLatLng.getLatitude()), String.valueOf(endLatLng.getLongitude()), currentMarker.getTitle());
                        }
                    }, null);
                } else {
                    Toast.makeText(this, "请先安装高德地图才能进行导航", Toast.LENGTH_LONG).show();
                }
            }
        }
        /**
         * 采用隐式调用主程序的activity
         */
        else if (i == R.id.but_bluetooth_openDoor) {
            Intent intent = new Intent();
            intent.setClassName("com.contron.ekeyapptx", "com.contron.ekeyapptx.doorlock.DoorLockerActivity");
            startActivity(intent);
        } else if (i == R.id.but_scanner_openDoor) {
            Intent intent = new Intent();
            intent.setClassName("com.contron.ekeyapptx", "com.contron.ekeyapptx.doorlock.DoorLockerActivity");
            intent.putExtra("UseScanner", true);
            startActivity(intent);
        }

	}

	/**
	 * 判断是否安装了地图
	 * @param context
	 * @return
	 */
	private boolean checkApkExist(Context context, String packageName) {
		if (packageName == null || "".equals(packageName))
			return false;
		try {
			ApplicationInfo info = context.getPackageManager().getApplicationInfo(packageName,
					PackageManager.GET_UNINSTALLED_PACKAGES);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	/**
	 * 跳转到高德地图进行路径规划
	 * @param lat 目标横坐标
	 * @param lon 目标纵坐标
	 * @param distance 目的地名称
	 */
	private void goToGaode(String lat, String lon, String distance) {
		StringBuffer stringBuffer = new StringBuffer("amapuri://route/plan/?sourceApplication=").append("amap");

		stringBuffer.append("&dlat=").append(lat)
				.append("&dlon=").append(lon)
				.append("&dname=").append(distance)
				.append("&dev=").append(0)
				.append("&t=").append(0);

		Intent intent = new Intent("android.intent.action.VIEW", android.net.Uri.parse(stringBuffer.toString()));
		intent.setPackage("com.autonavi.minimap");
		startActivity(intent);
	}

	public void showMsgBox(String title, String message, boolean cancel,
						   DialogInterface.OnClickListener okListener,
						   DialogInterface.OnClickListener noListener) {
		MsgBoxFragment boxFragment = new MsgBoxFragment();
		boxFragment.setTitle(title);
		boxFragment.setMessage(message);
		boxFragment.setPositiveOnClickListener(okListener);
		boxFragment.setNegativeOnClickListener(noListener);
		boxFragment.setCanceledOnTouchOutside(cancel);
		boxFragment.show(getFragmentManager(), "msgBox");

	}

	/**
	 * 路径规划方式步行、驾车
	 */
	private String calculateType() {

		int radioButtonId = radbut.getCheckedRadioButtonId();

		if (radioButtonId == R.id.radbut_drive) {// 驾车
			return "drive";

		} else if (radioButtonId == R.id.radbut_walk) {// 步行
			return "walk";
		}
		
		return "drive";
	}
}
