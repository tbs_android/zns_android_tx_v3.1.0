package com.contron.ekeypublic.map;

import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.AMapNaviView;
import com.amap.api.navi.AMapNaviViewListener;
import com.amap.api.navi.AMapNaviViewOptions;
import com.contron.ekeypublic.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

public class NaviActivity extends BasicActivity implements AMapNaviViewListener {
	// 导航可以设置的参数
	private boolean mDayNightFlag = false;// 默认为白天模式
	private boolean mDeviationFlag = true;// 默认进行偏航重算
	private boolean mJamFlag = true;// 默认进行拥堵重算
	private boolean mTrafficFlag = true;// 默认进行交通播报
	private boolean mCameraFlag = true;// 默认进行摄像头播报
	private boolean mScreenFlag = true;// 默认是屏幕常亮
	// 导航界面风格
	private int mThemeStle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 不显示程序的标题栏
		setContentView(R.layout.activity_navi);
		super.onCreate(savedInstanceState);
		init();
	}

	/**
	 * 初始化
	 */
	private void init() {
		// 设置导航界面监听
		mAmapAMapNaviView.setAMapNaviViewListener(this);

		// 实时导航方式进行导航
		AMapNavi.getInstance(this).startNavi(AMapNavi.GPSNaviMode);
	}

	private void setAmapNaviViewOptions() {
		// TODO Auto-generated method stub
		if (mAmapAMapNaviView == null) {
			return;
		}
		AMapNaviViewOptions viewOptions = new AMapNaviViewOptions();
		viewOptions.setSettingMenuEnabled(true);// 设置导航setting可用
		viewOptions.setNaviNight(mDayNightFlag);// 设置导航是否为黑夜模式
		viewOptions.setReCalculateRouteForYaw(mDeviationFlag);// 设置导偏航是否重算
		viewOptions.setReCalculateRouteForTrafficJam(mJamFlag);// 设置交通拥挤是否重算
		viewOptions.setTrafficInfoUpdateEnabled(mTrafficFlag);// 设置是否更新路况
		viewOptions.setCameraInfoUpdateEnabled(mCameraFlag);// 设置摄像头播报
		viewOptions.setScreenAlwaysBright(mScreenFlag);// 设置屏幕常亮情况
//		viewOptions.setNaviViewTopic(mThemeStle);// 设置导航界面主题样式
		mAmapAMapNaviView.setViewOptions(viewOptions);
	}

	@Override
	public void onNaviCancel() {
		// TODO 界面左下角结束导航按钮的回调接口
		finish();
	}

	@Override
	public void onNaviMapMode(int arg0) {
		// TODO 导航界面地图状态的回调

	}

	@Override
	public void onNaviSetting() {
		// TODO 界面右下角功能设置按钮的回调接口

	}

	@Override
	public void onNaviTurnClick() {
		// TODO 导航界面左上角转向操作的点击回调

	}

	@Override
	public void onNextRoadClick() {
		// TODO 导航界面下一道路名称的点击回调

	}

	@Override
	public void onScanViewButtonClick() {
		// TODO 导航界面全览按钮的点击回调
	}

	/**
	 * 方法必须重写
	 */
	@Override
	protected void onResume() {
		super.onResume();
		mAmapAMapNaviView.onResume();
		setAmapNaviViewOptions();
	}

	@Override
	public void onPause() {
		super.onPause();
		mAmapAMapNaviView.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mAmapAMapNaviView.onDestroy();
	}

	@Override
	public void onLockMap(boolean arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onNaviBackClick() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onNaviViewLoaded() {
		// TODO Auto-generated method stub

	}

}
