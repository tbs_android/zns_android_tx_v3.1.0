package com.contron.ekeypublic.map;

import java.util.ArrayList;
import java.util.List;

import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.AMap.OnMapLoadedListener;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.navi.AMapNavi;
import com.amap.api.navi.AMapNaviListener;
import com.amap.api.navi.model.AMapLaneInfo;
import com.amap.api.navi.model.AMapNaviCross;
import com.amap.api.navi.model.AMapNaviInfo;
import com.amap.api.navi.model.AMapNaviLocation;
import com.amap.api.navi.model.AMapNaviPath;
import com.amap.api.navi.model.AMapNaviTrafficFacilityInfo;
import com.amap.api.navi.model.AimLessModeCongestionInfo;
import com.amap.api.navi.model.AimLessModeStat;
import com.amap.api.navi.model.NaviInfo;
import com.amap.api.navi.model.NaviLatLng;
import com.amap.api.navi.view.RouteOverLay;
import com.autonavi.tbt.TrafficFacilityInfo;
import com.contron.ekeypublic.R;
import com.contron.ekeypublic.R.id;
import com.contron.ekeypublic.R.layout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class RouteActivity extends BasicActivity implements
		OnMapLoadedListener, OnClickListener {

	private NaviLatLng startLatLng;// 路径始
	private NaviLatLng endLatLng;// 路径终
	private String calculate;// 路径规划方式

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);// 不显示程序的标题栏
		setContentView(R.layout.activity_route);
		super.onCreate(savedInstanceState);
		Button btn_navi = (Button) findViewById(R.id.route_btn_navi);
		btn_navi.setOnClickListener(this);

		init();
	}

	/**
	 * 初始化
	 */
	private void init() {

		mAmapNavi = AMapNavi.getInstance(this);// 初始化导航引擎
		if (mAmapNavi == null) {
			Toast.makeText(this, "导航异常，无法使用", Toast.LENGTH_LONG).show();
			return;
		}
		mAmapNavi.addAMapNaviListener(mAMapNaviListener);
		mAmapNavi.startGPS();

		mRouteOverLay = new RouteOverLay(aMap, null);

		aMap.setOnMapLoadedListener(this);
		aMap.getUiSettings().setScaleControlsEnabled(true);

		setOnCalculateRouteListener(new OnCalculateRouteListener() {

			@Override
			public void onCalculateRouteSuccess() {

				AMapNaviPath naviPath = mAmapNavi.getNaviPath();
				if (naviPath == null) {
					return;
				}
				// 获取路径规划线路，显示到地图上
				mRouteOverLay.setAMapNaviPath(naviPath);
				mRouteOverLay.addToMap();

			}
		});
	}

	public static void startActivity(Activity activity, NaviLatLng startLatLng,
			NaviLatLng endLatLng, String calculate) {
		Intent intent = new Intent(activity, RouteActivity.class);
		Bundle bundle = new Bundle();
		bundle.putParcelable("startLatLng", startLatLng);
		bundle.putParcelable("endLatLng", endLatLng);
		bundle.putString("calculate", calculate);
		intent.putExtras(bundle);
		activity.startActivity(intent);
	}

	/**
	 * 开始搜索路径规划方案
	 */
	private void routeResult() {

		// 驾车路径规划
		List<NaviLatLng> startList = new ArrayList<NaviLatLng>();
		List<NaviLatLng> endList = new ArrayList<NaviLatLng>();
		startList.add(startLatLng);
		endList.add(endLatLng);

		boolean isSuccess=false;

		if ("walk".equals(calculate)) {
			isSuccess = mAmapNavi.calculateWalkRoute(startLatLng, endLatLng);
		} else {
			isSuccess = mAmapNavi.calculateDriveRoute(startList, endList, null,
					AMapNavi.DrivingDefault);
		}

		if (!isSuccess) {
			Toast.makeText(this, "路线计算失败,请检查参数情况", Toast.LENGTH_LONG).show();
		}

	}

	@Override
	public void onMapLoaded() {
		Bundle bundle = new Bundle();
		bundle = getIntent().getExtras();
		startLatLng = (NaviLatLng) bundle.get("startLatLng");
		endLatLng = (NaviLatLng) bundle.get("endLatLng");
		calculate = bundle.getString("calculate");

		if (startLatLng != null && endLatLng != null) {

			LatLng latLng = new LatLng(startLatLng.getLatitude(),
					startLatLng.getLongitude());
			// aMap.moveCamera(CameraUpdateFactory.changeLatLng(new LatLng(
			// startLatLng.getLatitude(), startLatLng.getLongitude())));

			aMap.moveCamera(CameraUpdateFactory
					.newCameraPosition(new CameraPosition(latLng, 20, 30, 0)));
		}

		routeResult();// 规化路径

	}

	@Override
	public void onClick(View arg0) {
		if (arg0.getId() == R.id.route_btn_navi) {
			startActivity(new Intent(this, NaviActivity.class));
		}
	}
}
