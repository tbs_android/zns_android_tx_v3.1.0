/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.entities;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Finder;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;

/**
 * 机构信息
 * 
 * @author wangfang
 * @date 2015年12月28日 上午9:40:20
 */
@Table(name = "section")
public class Section extends EntityBase implements Parcelable{	
//	@Id
	@Column(column = "id")
	@NoAutoIncrement
	public int id;
	
	@Column(column = "intro")
	public String intro;//简介	
	
	@Column(column = "SectionId")
	public int SectionId;
	
	@Column(column = "name")
	public String name;//名称
	
	@Column(column = "type")
	public String type;//类型 （机构、区域、站点）
	
	@Column(column = "pid")
	public int pid;//上级单位
	
	@Finder(targetColumn = "sid", valueColumn = "id")
	public transient List<DeviceObject> deviceList;

	@Finder(targetColumn = "sid", valueColumn = "id")
	public transient List<Company> companyList;

	public Section() {
		
	}
	
	public Section( String intro, String name, int pid, String type){
		this.setIntro(intro);
		this.setName(name);
		this.setPid(pid);
		this.setType(type);
	}
	
	public Section( int id, String intro, String name, String type){
		this.setId(id);
		this.setIntro(intro);
		this.setName(name);
		this.setType(type);
	}

	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the deviceList
	 */
	public List<DeviceObject> getDeviceList() {
		return deviceList;
	}
	/**
	 * @param deviceList the deviceList to set
	 */
	public void setDeviceList(List<DeviceObject> deviceList) {
		this.deviceList = deviceList;
	}

	public List<Company> getCompanyList() {
		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {
		this.companyList = companyList;
	}

	/**
	 * @return the sectionId
	 */
	public int getSectionId() {
		return SectionId;
	}
	/**
	 * @param sectionId the sectionId to set
	 */
	public void setSectionId(int sectionId) {
		SectionId = sectionId;
	}
	
	public String getIntro() {
		return this.intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
public Section(Parcel source) {
		this.id = source.readInt();
		this.SectionId=source.readInt();
		this.pid = source.readInt();
		this.name = source.readString();
		this.intro = source.readString();
		this.type = source.readString();
		this.deviceList = source.readArrayList(DeviceObject.class.getClassLoader());
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void writeToParcel(Parcel dest, int flag) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeInt(SectionId);
		dest.writeInt(pid);
		dest.writeString(name);
		dest.writeString(intro);
		dest.writeString(type);
		dest.writeList(deviceList);
	}
	
	public static Parcelable.Creator<Section> CREATOR = new Creator<Section>() {
@Override
		public Section[] newArray(int size) {
			return new Section[size];
		}

		@Override
		public Section createFromParcel(Parcel source) {
			return new Section(source);
		}
	};}
