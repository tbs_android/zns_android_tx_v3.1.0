package com.contron.ekeypublic.entities;

import java.io.Serializable;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;

@Table(name = "LockerResult")
public class LockerResult implements Serializable {

	@Id(column = "id")
	private int id;

	@Column(column = "oid")
	private int oid;// 设备ID

	@Column(column = "oname")
	private String oname;// 设备名

	@Column(column = "lid")
	private int lid;// 锁ID

	@Column(column = "ltype")
	private String ltype;// 锁具类型

	@Column(column = "action")
	private String action;// 操作类型

	@Column(column = "unlock_by")
	private String unlock_by;// 开锁人

	@Column(column = "usection")
	private String usection = "";// 开锁人所属机构

	@Column(column = "unlock_at")
	private String unlock_at = "";// 开锁时间

	/** 操作结果 1允许; 3禁止; 255未知操作 */
	@Column(column = "result")
	private String result = "未知操作";

	@Column(column = "isUpload")
	@Transient
	public transient int isUpload = 0;// 是否上传 0未上传; 1上传

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getOname() {
		return oname;
	}

	public void setOname(String oname) {
		this.oname = oname;
	}

	public int getLid() {
		return lid;
	}

	public void setLid(int lid) {
		this.lid = lid;
	}

	public String getLtype() {
		return ltype;
	}

	public void setLtype(String ltype) {
		this.ltype = ltype;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUnlock_by() {
		return unlock_by;
	}

	public void setUnlock_by(String unlock_by) {
		this.unlock_by = unlock_by;
	}

	public String getUsection() {
		return usection;
	}

	public void setUsection(String usection) {
		this.usection = usection;
	}

	public String getUnlock_at() {
		return unlock_at;
	}

	public void setUnlock_at(String unlock_at) {
		this.unlock_at = unlock_at;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public int getIsUpload() {
		return isUpload;
	}

	public void setIsUpload(int isUpload) {
		this.isUpload = isUpload;
	}

	public LockerResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LockerResult(int oid, String oname, int lid, String ltype,
			String action, String unlock_by, String usection, String unlock_at,
			String result) {
		super();
		this.oid = oid;
		this.oname = oname;
		this.lid = lid;
		this.ltype = ltype;
		this.action = action;
		this.unlock_by = unlock_by;
		this.usection = usection;
		this.unlock_at = unlock_at;
		this.result = result;
		this.isUpload = isUpload;
	}

}
