package com.contron.ekeypublic.entities;

import java.util.Date;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;


//@Entity
@Table(name="log")
public class Log {
	@Id
    //@Column(name="id")
	//@GeneratedValue(generator = "generator")
	//@GenericGenerator(name = "generator", strategy = "increment")
	private int id;

	//@Column(name="create_at")
	private Date createAt;

	private String details;

	private String type;

	private String username;

	public Log() {
	}
	public Log(int id, String type, String details, String username, Date createAt){
		this.id = id;
		this.type = type;
		this.details = details;
		this.username = username;
		this.createAt = createAt;
	}
	public Log(String type, String details, String username){
		this.type = type;
		this.details = details;
		this.username = username;
		this.createAt = new Date();
	}
	
	public Log(Date createAt, String details, String type, String username) {
		this.createAt = createAt;
		this.details = details;
		this.type = type;
		this.username = username;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
