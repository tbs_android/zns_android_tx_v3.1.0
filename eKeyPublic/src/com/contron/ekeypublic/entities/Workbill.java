package com.contron.ekeypublic.entities;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.contron.ekeypublic.util.DateUtil;

/**
 * 派工实体
 *
 * @author luoyilong
 *
 */
public class Workbill implements Parcelable {

	private int id;// 任务ID

	private String billno;// 工单编号

	private String worktype;// 工单类型

	private String workdesc;// 工单内容

	private int oid;// 基站【设备】

	private String oname;// 基站【设备】

	private int sid;// 执行单位

	private String sname;// 执行单位

	private int limited;// 进站超时时长（小时）

	private String memo;// 备注

	private String status;// 工单状

	private String apply_by;// 派工人

	private String apply_username;// 派工人手机号

	private String apply_at;// 派工时间

	private String rec_by;// 接单人

	private String rec_username;// 接单人

	private String rectime;// 接单时间

	private String intime;// 进站时间[第一次开锁时间]

	private String backtime;// 回单时间

	private int inflag;// 超时，标志为1，否则标志为0

	private int backflag;// 默认0（正常）；1（异常

	private String backmemo;
	private String update_at;
	private int need_accompany;
	private int validate_lng_lat;
	private String mobile;

	private String applyin_at;
	private String applyleave_at;
	private String verify_username;
	private String verify_by;
	private String verify_at;
	private String rec_at;
	private String in_at;
	private String in_by;
	private String	back_at;

	private DeviceObject object;

	public DeviceObject getObject() {
		return object;
	}

	public void setObjects(DeviceObject object) {
		this.object = object;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBillno() {
		return billno;
	}

	public void setBillno(String billno) {
		this.billno = billno;
	}

	public String getWorktype() {
		return worktype;
	}

	public void setWorktype(String worktype) {
		this.worktype = worktype;
	}

	public String getWorkdesc() {
		return workdesc;
	}

	public void setWorkdesc(String workdesc) {
		this.workdesc = workdesc;
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public int getLimited() {
		return limited;
	}

	public void setLimited(int limited) {
		this.limited = limited;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRectime() {
		return rectime;
	}

	public void setRectime(String rectime) {
		this.rectime = rectime;
	}

	public String getIntime() {
		return intime;
	}

	public void setIntime(String intime) {
		this.intime = intime;
	}

	public String getBacktime() {
		return backtime;
	}

	public void setBacktime(String backtime) {
		this.backtime = backtime;
	}

	public int getInflag() {
		return inflag;
	}

	public void setInflag(int inflag) {
		this.inflag = inflag;
	}

	public int getBackflag() {
		return backflag;
	}

	public void setBackflag(int backflag) {
		this.backflag = backflag;
	}

	public String getOname() {
		return oname;
	}

	public void setOname(String oname) {
		this.oname = oname;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getApply_by() {
		return apply_by;
	}

	public void setApply_by(String apply_by) {
		this.apply_by = apply_by;
	}

	public String getApply_username() {
		return apply_username;
	}

	public void setApply_username(String apply_username) {
		this.apply_username = apply_username;
	}

	public String getRec_by() {
		return rec_by;
	}

	public void setRec_by(String rec_by) {
		this.rec_by = rec_by;
	}

	public String getRec_username() {
		return rec_username;
	}

	public void setRec_username(String rec_username) {
		this.rec_username = rec_username;
	}

	public String getBack_at() {
		return DateUtil.getStringByFormat(back_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setBack_at(String back_at) {
		this.back_at = back_at;
	}

	public String getBackmemo() {
		return backmemo;
	}

	public void setBackmemo(String backmemo) {
		this.backmemo = backmemo;
	}

	public String getUpdate_at() {
		return update_at;
	}

	public void setUpdate_at(String update_at) {
		this.update_at = update_at;
	}

	public int getNeed_accompany() {
		return need_accompany;
	}

	public void setNeed_accompany(int need_accompany) {
		this.need_accompany = need_accompany;
	}

	public int getValidate_lng_lat() {
		return validate_lng_lat;
	}

	public void setValidate_lng_lat(int validate_lng_lat) {
		this.validate_lng_lat = validate_lng_lat;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getApply_at() {
		return DateUtil.getStringByFormat(apply_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setApply_at(String apply_at) {
		this.apply_at = apply_at;
	}

	public String getApplyin_at() {
		return DateUtil.getStringByFormat(applyin_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setApplyin_at(String applyin_at) {
		this.applyin_at = applyin_at;
	}

	public String getApplyleave_at() {
		return DateUtil.getStringByFormat(applyleave_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setApplyleave_at(String applyleave_at) {
		this.applyleave_at = applyleave_at;
	}

	public String getVerify_username() {
		return verify_username;
	}

	public void setVerify_username(String verify_username) {
		this.verify_username = verify_username;
	}

	public String getVerify_by() {
		return verify_by;
	}

	public void setVerify_by(String verify_by) {
		this.verify_by = verify_by;
	}

	public String getVerify_at() {
		return DateUtil.getStringByFormat(verify_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setVerify_at(String verify_at) {
		this.verify_at = verify_at;
	}

	public String getRec_at() {
		return DateUtil.getStringByFormat(rec_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setRec_at(String rec_at) {
		this.rec_at = rec_at;
	}

	public String getIn_at() {
		return DateUtil.getStringByFormat(in_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setIn_at(String in_at) {
		this.in_at = in_at;
	}

	public String getIn_by() {
		return in_by;
	}

	public void setIn_by(String in_by) {
		this.in_by = in_by;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeInt(id);
		arg0.writeString(billno);
		arg0.writeString(worktype);
		arg0.writeString(workdesc);
		arg0.writeInt(oid);
		arg0.writeInt(sid);
		arg0.writeInt(limited);
		arg0.writeString(memo);
		arg0.writeString(status);
		arg0.writeString(apply_at);
		arg0.writeString(rectime);
		arg0.writeString(intime);
		arg0.writeString(backtime);
		arg0.writeInt(inflag);
		arg0.writeInt(backflag);
		arg0.writeString(oname);
		arg0.writeString(sname);
		arg0.writeString(apply_by);
		arg0.writeString(apply_username);
		arg0.writeString(rec_by);
		arg0.writeString(rec_username);
		arg0.writeParcelable(object, arg1);

		arg0.writeString(backmemo);
		arg0.writeString(update_at);
		arg0.writeInt(need_accompany);
		arg0.writeInt(validate_lng_lat);
		arg0.writeString(mobile);
		arg0.writeString(apply_at);
		arg0.writeString(applyin_at);
		arg0.writeString(applyleave_at);
		arg0.writeString(verify_username);
		arg0.writeString(verify_by);
		arg0.writeString(verify_at);
		arg0.writeString(rec_at);
		arg0.writeString(in_at);
		arg0.writeString(in_by);
		arg0.writeString(back_at);
	}

	@SuppressWarnings("unchecked")
	public Workbill(Parcel source) {
		this.id = source.readInt();
		this.billno = source.readString();
		this.worktype = source.readString();
		this.workdesc = source.readString();
		this.oid = source.readInt();
		this.sid = source.readInt();
		this.limited = source.readInt();
		this.memo = source.readString();
		this.status = source.readString();
		this.apply_at = source.readString();
		this.rectime = source.readString();
		this.intime = source.readString();
		this.backtime = source.readString();
		this.inflag = source.readInt();
		this.backflag = source.readInt();
		this.oname = source.readString();
		this.sname = source.readString();
		this.apply_by = source.readString();
		this.apply_username = source.readString();
		this.rec_by = source.readString();
		this.rec_username = source.readString();
		this.object = source.readParcelable(DeviceObject.class.getClassLoader());

		this.backmemo = source.readString();
		this.update_at = source.readString();
		this.need_accompany = source.readInt();
		this.validate_lng_lat = source.readInt();
		this.mobile = source.readString();
		this.apply_at = source.readString();
		this.applyin_at = source.readString();
		this.applyleave_at = source.readString();
		this.verify_username = source.readString();
		this.verify_by = source.readString();
		this.verify_at = source.readString();
		this.rec_at = source.readString();
		this.in_at = source.readString();
		this.in_by = source.readString();
		this.back_at = source.readString();
	}

	public static final Parcelable.Creator<Workbill> CREATOR = new Creator<Workbill>() {

		@Override
		public Workbill[] newArray(int size) {
			return new Workbill[size];
		}

		@Override
		public Workbill createFromParcel(Parcel source) {
			return new Workbill(source);
		}
	};

}
