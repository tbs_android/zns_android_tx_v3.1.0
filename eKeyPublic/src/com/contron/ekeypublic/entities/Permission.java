package com.contron.ekeypublic.entities;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;

//@Entity
@Table(name="permission")
public class Permission {
	@Id
	//@Column(name = "name")
	private String name;
	
	//@Column(name = "rid")
	private int rid;
	
	public Permission(){
		
	}
	public Permission(String name, int rid){
		this.name = name;
		this.rid = rid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}
}