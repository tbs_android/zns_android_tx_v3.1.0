package com.contron.ekeypublic.entities;

import java.io.Serializable;
import java.util.List;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;

public class DeviceLockers implements Serializable{

	@Id
	@NoAutoIncrement
	private int id;

	// @Column(name = "area")
	private String area;

	// @Column(name = "carrier")
	private String carrier;

	// @Column(name = "name")
	private String name;

	// @Column(name = "section")
	private String section;

	// @Column(name = "type")
	private String type;

	// @Column(name = "sid")
	private int sid;

	private List<Lockset> listLocker;

	public DeviceLockers() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public List<Lockset> getListLocker() {
		return listLocker;
	}

	public void setListLocker(List<Lockset> listLocker) {
		this.listLocker = listLocker;
	}

}
