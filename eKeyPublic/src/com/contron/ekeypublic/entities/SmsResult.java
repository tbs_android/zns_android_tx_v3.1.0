package com.contron.ekeypublic.entities;

import java.io.Serializable;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;

@Table(name = "SmsResult")
public class SmsResult implements Serializable {

	@Id(column = "id")
	private int id;

	@Column(column = "oname")
	private String oname;// 设备名

	@Column(column = "rectime")
	private String rectime;// 短信接收时间
	
	@Column(column = "type")
	private String type;// 短信类容类型

	@Column(column = "content")
	private String content;// 短信类型

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getOname() {
		return oname;
	}
	public void setOname(String oname) {
		this.oname = oname;
	}
	
	public String getRectime() {
		return rectime;
	}
	public void setRectime(String rectime) {
		this.rectime = rectime;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public SmsResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SmsResult(String oname, String type, String rectime, String content) {
		super();
		this.oname = oname;
		this.type = type;
		this.rectime = rectime;
		this.content = content;
	}
	
}
