/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.entities;

import java.util.List;

import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;

/**
 * 角色
 * 
 * @author wangfang
 * @date 2015年12月30日 上午9:41:12
 */
@Table(name = "role")
public class Role extends EntityBase {

	@NoAutoIncrement
	private int id;

	private String intro;

	private String name;

	private String status;
	
	private List<String>  permission;
	
	private List<User> users;
	
	private int isCheckedColumn=0;

	public Role(){
		
	}
	public Role(String name, String intro) {
		this.name = name;
		this.intro = intro;
		this.status = "启用";
	}
	public Role(String name, String intro, String status) {
		this.name = name;
		this.intro = intro;
		this.status = status;
	}
	public Role(int id, String name, String intro, String status) {
		this.id = id;
		this.name = name;
		this.intro = intro;
		this.status = status;
	}

	public Role(int id, String intro, String name, String status,
			int isCheckedColumn) {
		super();
		this.id = id;
		this.intro = intro;
		this.name = name;
		this.status = status;
		this.isCheckedColumn = isCheckedColumn;
	}
	public int getIsCheck(){
		return this.isCheckedColumn;
	}
	public void setIsCheck(int isCheck){
		this.isCheckedColumn = isCheck;
	}
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIntro() {
		return this.intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}


}
