package com.contron.ekeypublic.entities;

import java.io.Serializable;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;


/**
 * The persistent class for the smartkey database table.
 * 
 */
@Table(name = "smartkey")
//@NamedQuery(name="Smartkey.findAll", query="SELECT s FROM Smartkey s")
public class Smartkey extends EntityBase {
//	private static final long serialVersionUID = 1L;

//	@Id
	//@Column(name = "id")
	@NoAutoIncrement
	private int id;

	//@Column(name = "btaddr")
	private String btaddr;

	//@Column(name = "name")
	private String name;

	//@Column(name = "section")
	private String section;

	//@Column(name = "type")
	private String type;

	//@Column(name = "user")
	private String user;
	
	//@Column(name = "sid")
	private int sid;

	public Smartkey() {
	}
	
	public Smartkey(int sid, String name, String type, String section, String user, String btaddr) {
		this.setSid(sid);
		this.setName(name);
		this.setType(type);
		this.setSection(section);
		this.setUser(user);
		this.setBtaddr(btaddr);
	}
	
	public Smartkey(int id, int sid, String name, String type, String section, String user, String btaddr) {
		this.setId(id);
		this.setSid(sid);
		this.setName(name);
		this.setType(type);
		this.setSection(section);
		this.setUser(user);
		this.setBtaddr(btaddr);
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBtaddr() {
		return this.btaddr;
	}

	public void setBtaddr(String btaddr) {
		this.btaddr = btaddr;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSection() {
		return this.section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUser() {
		return this.user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	@Override
	public boolean equals(java.lang.Object obj) {
		// TODO Auto-generated method stub
//		return super.equals(o);
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Smartkey other = (Smartkey) obj;
		if (btaddr == null) {
			if (other.btaddr != null)
				return false;
		} else if (!btaddr.equals(other.btaddr))
			return false;
		return true;
	}


	
//	public boolean equals(Object obj) {  
//        if (obj instanceof Smartkey) {  
//        	Smartkey u = (Smartkey) obj;  
//            return this.btaddr.equals(u.btaddr) ;  
//        }  
//        return super.equals(obj); 

//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Smartkey other = (Smartkey) obj;
//		if (btaddr == null) {
//			if (other.btaddr != null)
//				return false;
//		} else if (!btaddr.equals(other.btaddr))
//			return false;
//		return true;
//	}
	
}