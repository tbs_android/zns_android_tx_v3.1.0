/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.entities;

import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;

/**
 * 蓝牙设备锁
 * 
 * @author hupei
 * @date 2015年4月8日 下午2:33:22
 */
@Table(name = "doorLocker")
public class DoorLocker extends EntityBase {
	public int _id;
	public String ticketId = "";// 任务ID（32字符的GUID）
	public int ticketType;// 任务类型
	public int stationId;// 站号
	public int lockerId;// 锁ID
	public String bthAddr = "";
	public String deviceName = "";// 设备名称
	public int state; // 1代表授权，0代表未授权
	public int lockState;// 1代表开， 0代表关
	public int doorState;// 1代表开， 0代表关
	public String deviceNo = "";// 设备编号
	public String lockerTypeId;// 蓝牙锁 发at的 0 ；e40 255
	
	@Transient
	public transient String section;

	public DoorLocker() {

	}

	public DoorLocker(int _id, int lockerId, String bthAddr, String deviceName) {
		super();
		this._id = _id;
		this.lockerId = lockerId;
		this.bthAddr = bthAddr;
		this.deviceName = deviceName;
	}

	/**
	 * @author hupei
	 * @date 2015年9月29日 下午5:24:43
	 * @param _id
	 * @param ticketId
	 * @param ticketType
	 * @param stationId
	 * @param lockerId
	 * @param bthAddr
	 * @param deviceName
	 */
	// D._id, FH.ticketId, FH.ticketTypeId, FB.stationId, L.lockerId, D.bthAddr,
	// D.deviceName
	public DoorLocker(int _id, String ticketId, int ticketType, int stationId,
			int lockerId, String bthAddr, String deviceName, String lockerTypeId) {
		// TODO Auto-generated constructor stub
		super();
		this._id = _id;
		this.ticketId = ticketId;
		this.ticketType = ticketType;
		this.stationId = stationId;
		this.lockerId = lockerId;
		this.bthAddr = bthAddr;
		this.deviceName = deviceName;
		this.lockerTypeId = lockerTypeId;
	}

	// D._id, D.stationId, L.lockerId, D.bthAddr,D.deviceName
	// ,L.lockerTypeId,D.deviceNo

	public DoorLocker(int _id, int stationId, int lockerId, String bthAddr,
			String deviceName, String lockerTypeId, String deviceNo) {
		super();
		this._id = _id;
		this.stationId = stationId;
		this.lockerId = lockerId;
		this.bthAddr = bthAddr;
		this.deviceName = deviceName;
		this.lockerTypeId = lockerTypeId;
		this.deviceNo = deviceNo;
	}

	public DoorLocker(int _id, int stationId, int lockerId, String bthAddr,
			String deviceName, String deviceNo) {
		// TODO Auto-generated constructor stub
		super();
		this._id = _id;
		this.stationId = stationId;
		this.lockerId = lockerId;
		this.bthAddr = bthAddr;
		this.deviceName = deviceName;
		this.deviceNo = deviceNo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bthAddr == null) ? 0 : bthAddr.hashCode());
		return result;
	}

	@Override
	public boolean equals(java.lang.Object obj) {
		// TODO Auto-generated method stub
		// return super.equals(o);
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DoorLocker other = (DoorLocker) obj;
		if (bthAddr == null) {
			if (other.bthAddr != null)
				return false;
		} else if (!bthAddr.equals(other.bthAddr))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	// @Override
	// public boolean equals(Object obj) {
	// if (this == obj)
	// return true;
	// if (obj == null)
	// return false;
	// if (getClass() != obj.getClass())
	// return false;
	// DoorLocker other = (DoorLocker) obj;
	// if (bthAddr == null) {
	// if (other.bthAddr != null)
	// return false;
	// } else if (!bthAddr.equals(other.bthAddr))
	// return false;
	// return true;
	// }

}
