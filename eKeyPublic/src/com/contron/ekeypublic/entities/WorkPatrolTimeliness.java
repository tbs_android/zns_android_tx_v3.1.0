package com.contron.ekeypublic.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.contron.ekeypublic.util.DateUtil;

/**
 * 巡检率
 * 
 * @author luoyilong
 *
 */
public class WorkPatrolTimeliness implements Parcelable {

	private int id;// 巡检ID

	private String patrol_content;// 巡检内容

	private String patrol_cycle;// 巡检周期

	private String time;// 周期数

	private String patrol_rate;// 巡检完成率

	private int patrol_object_count;// 应巡站数

	private int patrol_exec_count;// 实巡站数

	private int unfinished;// 漏巡站数
	
	private String sname;// 代维公司

	/** ------------------------------巡检率明细------------------------------ */

	private String oname;// 基站

	private String patrolyear;// 年份

	private String create_at;// 创建时间

	private String create_by;// 创建人

	public WorkPatrolTimeliness() {
		super();
	}

	public WorkPatrolTimeliness(int id, String patrolcontent,
								String patrolcycle, String time, String patrolrate,
								int plancount, int realcount, int uncount) {
		super();
		this.id = id;
		this.patrol_content = patrolcontent;
		this.patrol_cycle = patrolcycle;
		this.time = time;
		this.patrol_rate = patrolrate;
		this.patrol_object_count = plancount;
		this.patrol_exec_count = realcount;
		this.unfinished = uncount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPatrol_content() {
		return patrol_content;
	}

	public void setPatrol_content(String patrol_content) {
		this.patrol_content = patrol_content;
	}

	public String getPatrol_cycle() {
		return patrol_cycle;
	}

	public void setPatrol_cycle(String patrol_cycle) {
		this.patrol_cycle = patrol_cycle;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getPatrol_rate() {
		return patrol_rate;
	}

	public void setPatrol_rate(String patrol_rate) {
		this.patrol_rate = patrol_rate;
	}

	public int getPatrol_object_count() {
		return patrol_object_count;
	}

	public void setPatrol_object_count(int patrol_object_count) {
		this.patrol_object_count = patrol_object_count;
	}

	public int getPatrol_exec_count() {
		return patrol_exec_count;
	}

	public void setPatrol_exec_count(int patrol_exec_count) {
		this.patrol_exec_count = patrol_exec_count;
	}

	public int getUnfinished() {
		return unfinished;
	}

	public void setUnfinished(int unfinished) {
		this.unfinished = unfinished;
	}

	public String getOname() {
		return oname;
	}

	public void setOname(String oname) {
		this.oname = oname;
	}

	public String getPatrolyear() {
		return patrolyear;
	}

	public void setPatrolyear(String patrolyear) {
		this.patrolyear = patrolyear;
	}

	public String getCreate_at() {
		return DateUtil.getStringByFormat(create_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setCreate_at(String create_at) {
		this.create_at = create_at;
	}

	public String getCreate_by() {
		return create_by;
	}

	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}

	
	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeInt(id);
		arg0.writeString(patrol_content);
		arg0.writeString(patrol_cycle);
		arg0.writeString(time);
		arg0.writeString(patrol_rate);
		arg0.writeInt(patrol_object_count);
		arg0.writeInt(patrol_exec_count);
		arg0.writeInt(unfinished);
		arg0.writeString(oname);
		arg0.writeString(patrolyear);
		arg0.writeString(create_at);
		arg0.writeString(create_by);
		arg0.writeString(sname);

	}

	@SuppressWarnings("unchecked")
	public WorkPatrolTimeliness(Parcel source) {

		this.id = source.readInt();
		this.patrol_content = source.readString();
		this.patrol_cycle = source.readString();
		this.time = source.readString();
		this.patrol_rate = source.readString();
		this.patrol_object_count = source.readInt();
		this.patrol_exec_count = source.readInt();
		this.unfinished = source.readInt();
		this.oname = source.readString();
		this.patrolyear = source.readString();
		this.create_at = source.readString();
		this.create_by = source.readString();
		this.sname = source.readString();

	}

	public static final Parcelable.Creator<WorkPatrolTimeliness> CREATOR = new Creator<WorkPatrolTimeliness>() {

		@Override
		public WorkPatrolTimeliness[] newArray(int size) {
			return new WorkPatrolTimeliness[size];
		}

		@Override
		public WorkPatrolTimeliness createFromParcel(Parcel source) {
			return new WorkPatrolTimeliness(source);
		}
	};
}
