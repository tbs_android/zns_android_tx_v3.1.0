package com.contron.ekeypublic.entities;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * 进站及时率
 * @author luoyilong
 *
 */
public class WorkBillTimeliness implements Parcelable{

	

	@SerializedName("name")
	@Expose
	private String name;// 基站【设备】

	@SerializedName("incomeRate")
	@Expose
	private String incomeRate;// 进站及时率

	@SerializedName("should")
	@Expose
	private int should;// 应进站数

	@SerializedName("actual")
	@Expose
	private int actual;// 实际进站数

	@SerializedName("income")
	@Expose
	private int income;// 及时进站数

	@SerializedName("unfinished")
	@Expose
	private int unfinished;// 未进站数

	@SerializedName("details")
	@Expose
	private List<Workbill> details;

	public List<Workbill> getDetails() {
		return details;
	}

	public void setDetails(List<Workbill> details) {
		this.details = details;
	}

	public WorkBillTimeliness() {
		super();
	}

	public WorkBillTimeliness(String oname, String inrate, int plancount,
			int realcount, int intimecount, int uncount) {
		super();
		this.name = oname;
		this.incomeRate = inrate;
		this.should = plancount;
		this.actual = realcount;
		this.income = intimecount;
		this.unfinished = uncount;
	}

	public String getName() {
		return name;
	}

	public void setName(String oname) {
		this.name = oname;
	}

	public String getIncomeRate() {
		return incomeRate;
	}

	public void setIncomeRate(String incomeRate) {
		this.incomeRate = incomeRate;
	}

	public int getShould() {
		return should;
	}

	public void setShould(int should) {
		this.should = should;
	}

	public int getActual() {
		return actual;
	}

	public void setActual(int actual) {
		this.actual = actual;
	}

	public int getIncome() {
		return income;
	}

	public void setIncome(int income) {
		this.income = income;
	}

	public int getUnfinished() {
		return unfinished;
	}

	public void setUnfinished(int unfinished) {
		this.unfinished = unfinished;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		
		arg0.writeString(name);
		arg0.writeString(incomeRate);
		arg0.writeInt(should);
		arg0.writeInt(actual);
		arg0.writeInt(income);
		arg0.writeInt(unfinished);
		
	}
	@SuppressWarnings("unchecked")
	public WorkBillTimeliness(Parcel source) {
	
		this.name = source.readString();
		this.incomeRate = source.readString();
		this.should = source.readInt();
		this.actual = source.readInt();
		this.income = source.readInt();
		this.unfinished = source.readInt();
		
	}

	public static final Parcelable.Creator<WorkBillTimeliness> CREATOR = new Creator<WorkBillTimeliness>() {

		@Override
		public WorkBillTimeliness[] newArray(int size) {
			return new WorkBillTimeliness[size];
		}

		@Override
		public WorkBillTimeliness createFromParcel(Parcel source) {
			return new WorkBillTimeliness(source);
		}
	};
}
