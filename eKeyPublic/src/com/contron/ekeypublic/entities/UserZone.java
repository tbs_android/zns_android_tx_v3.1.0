package com.contron.ekeypublic.entities;


import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;

@Table(name = "userZone")
public class UserZone extends EntityBase {
	
	@NoAutoIncrement
	private int id;
	private int zid;
	private int rid;
	private String rname;
	private String zname;
	private String memo;
	private String create_by;
	private String create_at;

	public UserZone() {
	}

	public UserZone(int id, int zid, int rid, String rname, String zname,
			String memo, String create_by, String create_at) {
		super();
		this.id = id;
		this.zid = zid;
		this.rid = rid;
		this.rname = rname;
		this.zname = zname;
		this.memo = memo;
		this.create_by = create_by;
		this.create_at = create_at;
	}

	public UserZone(int zid, int rid, String rname, String zname, String memo,
			String create_by, String create_at) {
		super();
		this.zid = zid;
		this.rid = rid;
		this.rname = rname;
		this.zname = zname;
		this.memo = memo;
		this.create_by = create_by;
		this.create_at = create_at;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getZid() {
		return zid;
	}

	public void setZid(int zid) {
		this.zid = zid;
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public String getRname() {
		return rname;
	}

	public void setRname(String rname) {
		this.rname = rname;
	}

	public String getZname() {
		return zname;
	}

	public void setZname(String zname) {
		this.zname = zname;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getCreate_by() {
		return create_by;
	}

	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}

	public String getCreate_at() {
		return create_at;
	}

	public void setCreate_at(String create_at) {
		this.create_at = create_at;
	}

}
