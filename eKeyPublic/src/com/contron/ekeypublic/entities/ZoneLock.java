package com.contron.ekeypublic.entities;

import java.io.Serializable;
import java.util.Date;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;

/**
 * The persistent class for the range_lock database table.
 * 
 */
//@Entity
@Table(name="zone_lock")
//@NamedQuery(name="ZoneLock.findAll", query="SELECT z FROM ZoneLock z")
public class ZoneLock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.TABLE)
	private int id;

	//@Column(name="apply_at")
	private Date applyAt;

	//@Column(name="apply_by")
	private String applyBy;

	//@Column(name="begin_at")
	private Date beginAt;

	private int duration;

	//@Column(name="end_at")
	private Date endAt;

	private String status;

	//@Column(name="verify_at")
	private Date verifyAt;

	//@Column(name="verify_by")
	private String verifyBy;

	//bi-directional many-to-one association to Range
	//@ManyToOne
	//@JoinColumn(name="rid")
	private Zone zone;

	public ZoneLock() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getApplyAt() {
		return this.applyAt;
	}

	public void setApplyAt(Date applyAt) {
		this.applyAt = applyAt;
	}

	public String getApplyBy() {
		return this.applyBy;
	}

	public void setApplyBy(String applyBy) {
		this.applyBy = applyBy;
	}

	public Date getBeginAt() {
		return this.beginAt;
	}

	public void setBeginAt(Date beginAt) {
		this.beginAt = beginAt;
	}

	public int getDuration() {
		return this.duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Date getEndAt() {
		return this.endAt;
	}

	public void setEndAt(Date endAt) {
		this.endAt = endAt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getVerifyAt() {
		return this.verifyAt;
	}

	public void setVerifyAt(Date verifyAt) {
		this.verifyAt = verifyAt;
	}

	public String getVerifyBy() {
		return this.verifyBy;
	}

	public void setVerifyBy(String verifyBy) {
		this.verifyBy = verifyBy;
	}

	public Zone getRange() {
		return this.zone;
	}

	public void setRange(Zone range) {
		this.zone = range;
	}

}