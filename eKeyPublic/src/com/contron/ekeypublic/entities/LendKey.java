package com.contron.ekeypublic.entities;

import com.lidroid.xutils.db.annotation.Table;


@Table(name="lendkey")
public class LendKey extends EntityBase {
	private int _id;
	private String keyAddr;
	private String keyName;
	private int returnRegisterUserId;// 归还登记人ID
	private String returnRegisterUserName;// 归还登记人名
	private String returnRegisterDate;// 归还登记时间
	private int borrowUserId;// 借用人ID
	private String borrowUserName;// 借用人名
	private String borrowDate;// 借用时间
	private int keyState;//(0)借用登记,(1)归还登记

	

	public LendKey() {
		super();
	}

	
	public LendKey(int _id, String keyAddr, String keyName,
			int returnRegisterUserId, String returnRegisterUserName,
			String returnRegisterDate, int borrowUserId, String borrowUserName,
			String borrowDate, int keyState) {
		super();
		this._id = _id;
		this.keyAddr = keyAddr;
		this.keyName = keyName;
		this.returnRegisterUserId = returnRegisterUserId;
		this.returnRegisterUserName = returnRegisterUserName;
		this.returnRegisterDate = returnRegisterDate;
		this.borrowUserId = borrowUserId;
		this.borrowUserName = borrowUserName;
		this.borrowDate = borrowDate;
		this.keyState = keyState;
	}





	public int get_id() {
		return _id;
	}



	public void set_id(int _id) {
		this._id = _id;
	}



	public String getKeyAddr() {
		return keyAddr;
	}



	public void setKeyAddr(String keyAddr) {
		this.keyAddr = keyAddr;
	}



	public String getKeyName() {
		return keyName;
	}



	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}



	public int getReturnRegisterUserId() {
		return returnRegisterUserId;
	}



	public void setReturnRegisterUserId(int returnRegisterUserId) {
		this.returnRegisterUserId = returnRegisterUserId;
	}



	public String getReturnRegisterUserName() {
		return returnRegisterUserName;
	}



	public void setReturnRegisterUserName(String returnRegisterUserName) {
		this.returnRegisterUserName = returnRegisterUserName;
	}



	public String getReturnRegisterDate() {
		return returnRegisterDate;
	}



	public void setReturnRegisterDate(String returnRegisterDate) {
		this.returnRegisterDate = returnRegisterDate;
	}



	public int getBorrowUserId() {
		return borrowUserId;
	}



	public void setBorrowUserId(int borrowUserId) {
		this.borrowUserId = borrowUserId;
	}



	public String getBorrowUserName() {
		return borrowUserName;
	}



	public void setBorrowUserName(String borrowUserName) {
		this.borrowUserName = borrowUserName;
	}



	public String getBorrowDate() {
		return borrowDate;
	}



	public void setBorrowDate(String borrowDate) {
		this.borrowDate = borrowDate;
	}



	public int getKeyState() {
		return keyState;
	}



	public void setKeyState(int keyState) {
		this.keyState = keyState;
	}




	

}
