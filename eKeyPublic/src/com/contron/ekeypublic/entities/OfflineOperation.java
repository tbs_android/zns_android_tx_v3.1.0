package com.contron.ekeypublic.entities;

import android.text.TextUtils;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.util.LogUtils;

/**
 * 离线鉴权蓝牙锁的操作记录
 * 
 * @author luoyilong
 *
 */

@Table(name = "offlineoperation")
public class OfflineOperation extends EntityBase {

	@Id
	private int id;

	private String deviceName = "未知设备";// 设备名称
	private int lid;// 锁ID
	private String updateDateTime = "";// 更新时间

	private String action = "";// "fixed|temp", //鉴权开锁|临时申请开锁

	private int tid;// 临时申请开锁时必带此项，鉴权开锁没有此项

	private  int endSign = 0;// 这条记录结束标记1 结束、0 未结束

	private String directive_fault_at;// 开锁指令状态失败

	private String directive_at = "";// 开锁指令成功时间

	private String unlock_at = "";// 开锁时间

	private String opendoor_at = "";// 开门时间

	private String closedoor_at = "";// 关门时间

	private String lock_at = "";// 闭锁时间

	private String ultra_at = "";// 越权时间

	private int result;// 0|1|2|3|4|5|6|255
						// /开锁指令失败|开锁指令成功|开锁成功|开门成功|关门成功|闭锁成功|正常操作|越权未遂

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;

		LogUtils.e(getClass().toString());
		LogUtils.e(obj.getClass().toString());

		if (!getClass().toString().equals(obj.getClass().toString()))
			return false;

		OfflineOperation other = (OfflineOperation) obj;

		if (lid == other.lid) {

			if (!TextUtils.isEmpty(directive_at)
					&& TextUtils.equals(directive_at, other.directive_at)) {
				return true;
			} else if (!TextUtils.isEmpty(unlock_at)
					&& TextUtils.equals(unlock_at, other.unlock_at)) {
				return true;
			} else if (!TextUtils.isEmpty(opendoor_at)
					&& TextUtils.equals(opendoor_at, other.opendoor_at)) {
				return true;
			} else if (!TextUtils.isEmpty(closedoor_at)
					&& TextUtils.equals(closedoor_at, other.closedoor_at)) {
				return true;
			} else if (!TextUtils.isEmpty(lock_at)
					&& TextUtils.equals(lock_at, other.lock_at)) {
				return true;
			} else if (!TextUtils.isEmpty(ultra_at)
					&& TextUtils.equals(ultra_at, other.ultra_at)) {
				return true;
			} else if (!TextUtils.isEmpty(directive_fault_at)
					&& TextUtils.equals(directive_fault_at, other.directive_fault_at)) {
				return true;
			}

		}

		return false;
	}

	public OfflineOperation() {
		super();
	}

	public OfflineOperation(String deviceName, int lockerId,
			String updateDateTime, int endSign) {
		super();
		this.deviceName = deviceName;
		this.lid = lockerId;
		this.updateDateTime = updateDateTime;
		this.endSign = endSign;
	}

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public int getLockerId() {
		return lid;
	}

	public void setLockerId(int lockerId) {
		this.lid = lockerId;
	}

	public String getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(String updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public int isEndSign() {
		return endSign;
	}

	public void setEndSign(int endSign) {
		this.endSign = endSign;
	}



	public String getOpenLockerDate() {
		return unlock_at;
	}

	public void setOpenLockerDate(String openLockerDate) {
		this.unlock_at = openLockerDate;
	}

	public String getOpenDoorDate() {
		return opendoor_at;
	}

	public void setOpenDoorDate(String openDoorDate) {
		this.opendoor_at = openDoorDate;
	}

	public String getCloseDoorDate() {
		return closedoor_at;
	}

	public void setCloseDoorDate(String closeDoorDate) {
		this.closedoor_at = closeDoorDate;
	}

	public String getCloseLockerDate() {
		return lock_at;
	}

	public void setCloseLockerDate(String closeLockerDate) {
		this.lock_at = closeLockerDate;
	}



	public String getDirective_fault_at() {
		return directive_fault_at;
	}

	public void setDirective_fault_at(String directive_fault_at) {
		this.directive_fault_at = directive_fault_at;
	}

	public String getDirective_at() {
		return directive_at;
	}

	public void setDirective_at(String directive_at) {
		this.directive_at = directive_at;
	}

	public String getClosedoor_at() {
		return closedoor_at;
	}

	public void setClosedoor_at(String closedoor_at) {
		this.closedoor_at = closedoor_at;
	}

	public String getUltra_at() {
		return ultra_at;
	}

	public void setUltra_at(String ultra_at) {
		this.ultra_at = ultra_at;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getAction() {
		return action;
	}

	
	public void setAction(String action) {
		this.action = action;
	}

}
