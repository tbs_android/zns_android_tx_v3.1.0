/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.entities;

import java.util.ArrayList;
import java.util.List;







import android.graphics.Bitmap;

import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;

/**
 * 用户注册
 * 
 * @author hupei
 * @date 2015年8月6日 上午11:02:57
 */
@Table(name = "registerUser")
public class RegisterUser extends EntityBase {
	@NoAutoIncrement
	//username|password|name|section
	public int id;	
	public int sid;
	public String name = "";
	public String section="";// 部门对象
	public String username = ""; //电话号码
	public String memo = ""; //简介
//	public int auditValue;// 0待审批；1审批通过；2：审批不通过；注册时填0
	public String reg_at = "";// 注册时间
	public String lastlogin_at = "";// 上次登录时间
	public String verify_at = "";// 审核时间
	public String auditDesc = "";// 审核信息
	public String verify_by = "";//审批人
	public String status=""; //待审批、启用、禁用
	
	// 导出 JOSN 忽略
	public transient String role="";
	private Bitmap portraitmap;//头像
	private Bitmap idcardmap;//身份证
	
	
	public List<Role> rolesCheck = new ArrayList<Role>();// 选择后角色对象集合
	public transient List<Role> rolesALL = new ArrayList<Role>();// 所有角色对象集合，有状态值的

	public void addAllRoleAll(List<Role> lists) {
		rolesALL.clear();
		rolesCheck.clear();
		for (Role role : lists) {
			rolesALL.add(role);
			if (role.getIsCheck() == 1)
				rolesCheck.add(role);
		}
	}

	public Bitmap getPortraitmap() {
		return portraitmap;
	}

	public void setPortraitmap(Bitmap portraitmap) {
		this.portraitmap = portraitmap;
	}

	public Bitmap getIdcardmap() {
		return idcardmap;
	}

	public void setIdcardmap(Bitmap idcardmap) {
		this.idcardmap = idcardmap;
	}

	public String roleCheckToString() {
		if (roleCheckEmpty())
			return "";
		else {
			StringBuffer buffer = new StringBuffer();
			for (Role role : rolesCheck) {
				buffer.append(role.getName()).append(",");
			}
			if (buffer.indexOf(",") > -1) {
				buffer.deleteCharAt(buffer.length() - 1);
			}
			return buffer.toString();
		}
	}
	public String roleCheckToIntString() {
		if (roleCheckEmpty())
			return "";
		else {
			StringBuffer buffer = new StringBuffer();
			for (Role role : rolesCheck) {
				buffer.append(role.getId()).append(",");
			}
			if (buffer.indexOf(",") > -1) {
				buffer.deleteCharAt(buffer.length() - 1);
			}
			return buffer.toString();
		}
	}

	public boolean roleAllEmpty() {
		return rolesALL.size() == 0 || rolesALL.isEmpty();
	}

	public boolean roleCheckEmpty() {
		return rolesCheck.size() == 0 || rolesCheck.isEmpty();
	}
}
