/**
 * 
 */
package com.contron.ekeypublic.entities;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;

/**
 * @author guanhaiping
 * @date 2016年1月1日上午11:37:28
 */
// @Entity
@Table(name = "temp_object")
public class TempDevice extends EntityBase implements Parcelable {

	@Id
	private int id;//Id
	private int tid;//任务ID
	private int oid;//设备ID
	private String oname;//设备名称
	private double lat;//纬度
	private double lng;//经度


	public TempDevice() {

	}


	public TempDevice(int id, int tid, int oid, String oname) {
		super();
		this.id = id;
		this.tid = tid;
		this.oid = oid;
		this.oname = oname;
		
	}
	
	public TempDevice(int id,String oname,double lat,double lng) {
		super();
		this.id = id;
		this.lat = lat;
		this.lng = lng;
		this.oname = oname;
		
	}
	
	public TempDevice(int tid, int oid,String oname,double lat,double lng) {
		super();
		this.tid = tid;
		this.oid = oid;
		this.lat = lat;
		this.lng = lng;
		this.oname = oname;
		
	}


public TempDevice(int id, String name) {
		// TODO Auto-generated constructor stub
		this.id = id;
		this.oname = name;
	}
	/**
	 * @return the tid
	 */
	public int getTid() {
		return tid;
	}

	/**
	 * @param tid
	 *            the tid to set
	 */
	public void setTid(int tid) {
		this.tid = tid;
	}

	/**
	 * @return the oid
	 */
	public int getOid() {
		return oid;
	}

	/**
	 * @param oid
	 *            the oid to set
	 */
	public void setOid(int oid) {
		this.oid = oid;
	}

	/**
	 * @return the oname
	 */
	public String getOname() {
		return oname;
	}

	/**
	 * @param oname
	 *            the oname to set
	 */
	public void setOname(String oname) {
		this.oname = oname;
	}


	public double getLat() {
		return lat;
	}


	public void setLat(double lat) {
		this.lat = lat;
	}


	public double getLng() {
		return lng;
	}


	public void setLng(double lng) {
		this.lng = lng;
	}


	
	
	@SuppressWarnings("unchecked")
	public TempDevice(Parcel source) {
		this.id = source.readInt();
		this.tid = source.readInt();
		this.oid = source.readInt();
		this.oname = source.readString();
		this.lat = source.readDouble();
		this.lng = source.readDouble();
	}

	
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flag) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeInt(tid);
		dest.writeInt(oid);
		dest.writeString(oname);
		dest.writeDouble(lat);
		dest.writeDouble(lng);
	
	}
	
	public static Parcelable.Creator<TempDevice> CREATOR = new Creator<TempDevice>() {

		@Override
		public TempDevice[] newArray(int size) {
			return new TempDevice[size];
		}

		@Override
		public TempDevice createFromParcel(Parcel source) {
			return new TempDevice(source);
		}
	};

	
}
