package com.contron.ekeypublic.entities;

import java.io.Serializable;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;

@Table(name = "LocksetAll")
public class LocksetAll implements Serializable {

	@Id
	@NoAutoIncrement
	private int id;
	private String name;
	private String btaddr;
	private String btname;
	private String rfid;
	private String type;
	private int oid;

	private int state;

	private String latitude;

	private String longitude;

	public LocksetAll() {

	}

	public LocksetAll(int id, int oid, String name, String type, String rfid,
			String btname, String btaddr) {
		this.setId(id);
		this.setName(name);
		this.setOid(oid);
		this.setType(type);
		this.setRfid(rfid);
		this.setBtname(btname);
		this.setBtaddr(btaddr);
	}

	public LocksetAll(int oid, String name, String type, String rfid,
			String btname, String btaddr) {
		this.setName(name);
		this.setOid(oid);
		this.setType(type);
		this.setRfid(rfid);
		this.setBtname(btname);
		this.setBtaddr(btaddr);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBtaddr() {
		return btaddr;
	}

	public void setBtaddr(String btaddr) {
		this.btaddr = btaddr;
	}

	public String getBtname() {
		return btname;
	}

	public void setBtname(String btname) {
		this.btname = btname;
	}

	public String getRfid() {
		return rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}
