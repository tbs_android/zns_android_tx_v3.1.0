package com.contron.ekeypublic.entities;

public class ChoiceMode extends EntityBase {
	public String commuMode = "";
	public int isChoice;
  
	public ChoiceMode(){
		super();
	}

	public ChoiceMode(String commuMode, int isChoice) {
		super();
		this.commuMode = commuMode;
		this.isChoice = isChoice;
	}
	
}
