package com.contron.ekeypublic.entities;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.contron.ekeypublic.util.DateUtil;

/**
 * 巡检
 * 
 * @author luoyilong
 *
 */
public class WorkPatrol implements Parcelable {

	private int id;// ID

	private int sid;// 执行单位

	private String sname;// 执行单位

	private String patrol_content;// 巡检内容

	private String patrol_demand;// 巡检要求

	private String patrol_cycle;// 巡检周期

	private int patrol_object_count;// 巡检基站数

	private String begin_at; // 开始时间

	private String end_at; // 结束时间

	private String memo;// 巡检备注

	private String create_at;// 巡检创建时间

	private String create_username;// 巡检创建人

	private String create_by;// 巡检创建人

	private List<DeviceObject> objects;

	public List<DeviceObject> getObjects() {
		return objects;
	}

	public void setObjects(List<DeviceObject> objects) {
		this.objects = objects;
	}
	
	public WorkPatrol() {
		super();
	}

	public String getEnd_at() {
		return DateUtil.getStringByFormat(end_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setEnd_at(String end_at) {
		this.end_at = end_at;
	}

	public String getCreate_username() {
		return create_username;
	}

	public void setCreate_username(String create_username) {
		this.create_username = create_username;
	}

	public String getCreate_by() {
		return create_by;
	}

	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}

	public String getBegin_at() {
		return DateUtil.getStringByFormat(begin_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setBegin_at(String begin_at) {
		this.begin_at = begin_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getPatrol_content() {
		return patrol_content;
	}

	public void setPatrol_content(String patrol_content) {
		this.patrol_content = patrol_content;
	}

	public String getPatrol_demand() {
		return patrol_demand;
	}

	public void setPatrol_demand(String patrol_demand) {
		this.patrol_demand = patrol_demand;
	}

	public String getPatrol_cycle() {
		return patrol_cycle;
	}

	public void setPatrol_cycle(String patrol_cycle) {
		this.patrol_cycle = patrol_cycle;
	}

	public int getPatrol_object_count() {
		return patrol_object_count;
	}

	public void setPatrol_object_count(int patrol_object_count) {
		this.patrol_object_count = patrol_object_count;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getCreate_at() {
		return DateUtil.getStringByFormat(create_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setCreate_at(String create_at) {
		this.create_at = create_at;
	}

	public String getCreateUserName() {
		return create_username;
	}

	public void setCreateUserName(String createUserName) {
		this.create_username = createUserName;
	}

	public String getCreateBy() {
		return create_by;
	}

	public void setCreateBy(String createBy) {
		this.create_by = createBy;
	}


	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeInt(id);
		arg0.writeInt(sid);
		arg0.writeString(sname);
		arg0.writeString(patrol_content);
		arg0.writeString(patrol_demand);
		arg0.writeString(patrol_cycle);
		arg0.writeInt(patrol_object_count);
		arg0.writeString(create_at);
		arg0.writeString(create_username);
		arg0.writeString(create_by);
		arg0.writeString(memo);
		arg0.writeString(begin_at);
		arg0.writeString(end_at);
		arg0.writeList(objects);
	}

	@SuppressWarnings("unchecked")
	public WorkPatrol(Parcel source) {
		this.id = source.readInt();
		this.sid = source.readInt();
		this.sname = source.readString();
		this.patrol_content = source.readString();
		this.patrol_demand = source.readString();
		this.patrol_cycle = source.readString();
		this.patrol_object_count = source.readInt();
		this.create_at = source.readString();
		this.create_username = source.readString();
		this.create_by = source.readString();
		this.memo = source.readString();
		this.begin_at = source.readString();
		this.end_at = source.readString();
		this.objects = source.readArrayList(DeviceObject.class.getClassLoader());
	}

	public static final Parcelable.Creator<WorkPatrol> CREATOR = new Creator<WorkPatrol>() {

		@Override
		public WorkPatrol[] newArray(int size) {
			return new WorkPatrol[size];
		}

		@Override
		public WorkPatrol createFromParcel(Parcel source) {
			return new WorkPatrol(source);
		}
	};

}
