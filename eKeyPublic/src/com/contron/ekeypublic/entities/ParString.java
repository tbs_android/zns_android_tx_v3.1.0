package com.contron.ekeypublic.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ParString implements Parcelable {

	private int id;
	private String name;

	public ParString() {
		super();
	}

	public ParString(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeInt(id);
		arg0.writeString(name);

	}

	public ParString(Parcel source) {
		this.id = source.readInt();
		this.name = source.readString();
	}

	public static final Parcelable.Creator<ParString> CREATOR = new Creator<ParString>() {

		@Override
		public ParString[] newArray(int size) {
			return new ParString[size];
		}

		@Override
		public ParString createFromParcel(Parcel source) {
			return new ParString(source);
		}
	};

}
