package com.contron.ekeypublic.entities;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;

/**
 * 保存离线鉴权智能钥匙操作记录
 * 
 * @author luoyilong
 *
 */

@Table(name = "oflinehistory")
public class OfflineHistory {
	
	@Id
	private int id;
	
	private int tid;// 任务ID
	
	private String action = "";// 操作类型fixed、temp
	
	private int result;// 操作结果
	
	private int lid;// 锁ID
	
	private String unlock_at;//操作时间

	public int getTid() {
		return tid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public int getLid() {
		return lid;
	}

	public void setLid(int lid) {
		this.lid = lid;
	}

	public OfflineHistory() {
		super();
	}

	public String getUnlock_at() {
		return unlock_at;
	}

	public void setUnlock_at(String unlock_at) {
		this.unlock_at = unlock_at;
	}

	public OfflineHistory(int tid, String action, int result, int lid,
			String date) {
		super();
		this.tid = tid;
		this.action = action;
		this.result = result;
		this.lid = lid;
		this.unlock_at = date;
	}

}
