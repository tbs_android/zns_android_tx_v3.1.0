package com.contron.ekeypublic.entities;

/**
 * offline\temp_task\register  三个审批数量
 *
 * @author luoyilong
 */
public class Verify {

    private int offline;
    private int temp_task;
    private int register;

    private int unlock;

    private int workbill;
    public Verify(int offline, int temp_task, int register, int unlock, int workbill) {
        super();
        this.offline = offline;
        this.temp_task = temp_task;
        this.register = register;
        this.unlock = unlock;
        this.workbill = workbill;
    }


    public int getOffline() {
        return offline;
    }


    public void setOffline(int offline) {
        this.offline = offline;
    }

    public int getTemp_task() {
        return temp_task;
    }

    public void setTemp_task(int temp_task) {
        this.temp_task = temp_task;
    }

    public int getRegister() {
        return register;
    }

    public void setRegister(int register) {
        this.register = register;
    }

    public int getUnlock() {
        return unlock;
    }

    public void setUnlock(int unlock) {
        this.unlock = unlock;
    }

    public int getWorkbill() {
        return workbill;
    }

    public void setWorkbill(int workbill) {
        this.workbill = workbill;
    }

}
