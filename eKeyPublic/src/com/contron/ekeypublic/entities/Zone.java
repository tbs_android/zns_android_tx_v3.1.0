package com.contron.ekeypublic.entities;

import java.io.Serializable;
import java.util.Date;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;


/**
 * The persistent class for the range database table.
 * 
 */
//@Entity
@Table(name="zone")
public class Zone implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@NoAutoIncrement
	private int id;
	
	//@Column(name="sid")
	private int sid;
	
	//@Column(name="section")
	private String section;

	//@Column(name="memo")
	private String memo;

	//@Column(name="name")
	private String name;

	//@Column(name="type")
	private String type;

	//@Column(name="updatetime")
	private String updatetime;
	
	public Zone() {
	}
	
	public Zone(int id, String name, String memo, int sid, String section, String updatetime) {
		this.setId(id);
		this.setName(name);
		this.setType("");
		this.setMemo(memo);
		this.updatetime = updatetime;
		this.sid = sid;
		this.section = section;
	}
	
	public Zone(String name, String memo, int sid, String section) {
		this.setName(name);
		this.setType("");
		this.setMemo(memo);
		this.setUpdatetime(updatetime);
		this.sid = sid;
		this.section = section;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
	
}