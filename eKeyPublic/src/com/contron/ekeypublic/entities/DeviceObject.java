package com.contron.ekeypublic.entities;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;

/**
 * The persistent class for the object database table.
 * 
 */
// @Entity
@Table(name = "deviceObject")
// @NamedQuery(name="Object.findAll", query="SELECT o FROM Object o")
public class DeviceObject extends EntityBase implements Parcelable {

	// @Id
	@Column(column = "id")
	@NoAutoIncrement
	public int id;

	@Column(column = "area")
	public String area;

	@Column(column = "carrier")
	public String carrier;

	@Column(column = "name")
	public String name;

	@Column(column = "section")
	public String section;

	@Column(column = "type")
	public String type;

	@Column(column = "sid")
	public int sid;

	@Column(column = "lat")
	public double lat;// 纬度

	@Column(column = "lng")
	public double lng;// 经度

	public String code;//设备编号
	public String agent_sid;//代维单位ID
	public String agent_section;
	public String operationid;
	public String resourceid;
	public String districts;
	public String address;//设备地址
	public String svrlevel;
	public String source;
	public String overscene;
	public String handovertime;

	public String getLockstatus() {
		return lockstatus;
	}

	public void setLockstatus(String lockstatus) {
		this.lockstatus = lockstatus;
	}

	public String lockstatus;

	public String fsustatus;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private String status;

	public String getLockCount() {
		return lockCount;
	}

	public void setLockCount(String lockCount) {
		this.lockCount = lockCount;
	}

	public String lockCount;


	// @Finder(targetColumn = "oid", valueColumn = "id")
	@Transient
	public List<Lockset> lockset;

	@Transient
	public transient boolean isCheck;

	/**
	 * @param isCheck
	 *            the isCheck to set
	 */
	public void setCheck(boolean isCheck) {
		this.isCheck = isCheck;
	}

	public boolean isCheck() {
		// TODO Auto-generated method stub
		return this.isCheck;
	}

	/**
	 * 
	 */
	public DeviceObject() {
		super();
	}

	public DeviceObject(int id, String name, double lat, double lng) {
		this.setId(id);
		this.setName(name);
		this.setLat(lat);
		this.setLng(lng);
	}

	public DeviceObject(int id, String name, String carrier, String section,
			String type, String area) {
		this.setSid(sid);
		this.setName(name);
		this.setArea(area);
		this.setCarrier(carrier);
		this.setSection(section);
		this.setType(type);
	}

	/**
	 * @return the lockset
	 */
	public List<Lockset> getLockset() {
		return lockset;
	}

	/**
	 * @param lockset
	 *            the lockset to set
	 */
	public void setLockset(List<Lockset> lockset) {
		this.lockset = lockset;
	}

	public DeviceObject(int id, int sid, String name, String carrier,
			String section, String type, String area) {
		this.setId(id);
		this.setSid(sid);
		this.setName(name);
		this.setArea(area);
		this.setCarrier(carrier);
		this.setSection(section);
		this.setType(type);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAgent_sid() {
		return agent_sid;
	}

	public void setAgent_sid(String agent_sid) {
		this.agent_sid = agent_sid;
	}

	public String getAgent_section() {
		return agent_section;
	}

	public void setAgent_section(String agent_section) {
		this.agent_section = agent_section;
	}

	public String getOperationid() {
		return operationid;
	}

	public void setOperationid(String operationid) {
		this.operationid = operationid;
	}

	public String getResourceid() {
		return resourceid;
	}

	public void setResourceid(String resourceid) {
		this.resourceid = resourceid;
	}

	public String getDistricts() {
		return districts;
	}

	public void setDistricts(String districts) {
		this.districts = districts;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSvrlevel() {
		return svrlevel;
	}

	public void setSvrlevel(String svrlevel) {
		this.svrlevel = svrlevel;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getOverscene() {
		return overscene;
	}

	public void setOverscene(String overscene) {
		this.overscene = overscene;
	}

	public String getHandovertime() {
		return handovertime;
	}

	public void setHandovertime(String handovertime) {
		this.handovertime = handovertime;
	}

	public String getFsustatus() {
		return fsustatus;
	}

	public void setFsustatus(String fsustatus) {
		this.fsustatus = fsustatus;
	}

	public static Creator<DeviceObject> getCREATOR() {
		return CREATOR;
	}

	public static void setCREATOR(Creator<DeviceObject> CREATOR) {
		DeviceObject.CREATOR = CREATOR;
	}

	@SuppressWarnings("unchecked")
	public DeviceObject(Parcel source) {
		this.id = source.readInt();
		this.sid = source.readInt();
		this.name = source.readString();
		this.section = source.readString();
		this.type = source.readString();
		this.area = source.readString();
		this.carrier = source.readString();
		this.lat = source.readDouble();
		this.lng = source.readDouble();
		this.lockset = source.readArrayList(Lockset.class.getClassLoader());
		this.isCheck = source.readInt() == 1 ? true : false;
		this.code = source.readString();
		this.agent_sid = source.readString();
		this.agent_section = source.readString();
		this.operationid = source.readString();
		this.resourceid = source.readString();
		this.districts = source.readString();
		this.address = source.readString();
		this.svrlevel = source.readString();
		this.source = source.readString();
		this.overscene = source.readString();
		this.handovertime = source.readString();
		this.fsustatus = source.readString();
		this.status = source.readString();
		this.lockstatus = source.readString();

	}



	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flag) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeInt(sid);
		dest.writeString(name);
		dest.writeString(section);
		dest.writeString(type);
		dest.writeString(area);
		dest.writeString(carrier);
		dest.writeDouble(lat);
		dest.writeDouble(lng);
		dest.writeList(lockset);
		dest.writeInt(isCheck ? 1 : 0);
		dest.writeString(code);
		dest.writeString(agent_sid);
		dest.writeString(agent_section);
		dest.writeString(operationid);
		dest.writeString(resourceid);
		dest.writeString(districts);
		dest.writeString(address);
		dest.writeString(svrlevel);
		dest.writeString(source);
		dest.writeString(overscene);
		dest.writeString(handovertime);
		dest.writeString(fsustatus);
		dest.writeString(status);
		dest.writeString(lockstatus);
	}

	@Override
	public String toString() {
		return name + '，' +
				 section + '\n' +
				"锁状态：" + lockstatus +
				", 状态：" + status;
	}

	public static Parcelable.Creator<DeviceObject> CREATOR = new Creator<DeviceObject>() {
		@Override
		public DeviceObject[] newArray(int size) {
			return new DeviceObject[size];
		}

		@Override
		public DeviceObject createFromParcel(Parcel source) {
			return new DeviceObject(source);
		}
	};

}