package com.contron.ekeypublic.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class Station implements Parcelable {
    private String address; //设备地址
    private String area;  //设备区域
    private String code; //设备编码
    private String carrier; //运营商
    private int companyid; //代维单位id
    private String company_name; //代维单位名称
    private String gprs; //GPRS
    private int id; //
    private double lat; //经度
    private double lng; //纬度
    private List<Lockset> locksets = new ArrayList<>(); //设备内锁具
    private String name; //名称
    private String phone; //电话号码
    private String remark; //配置
    private String resourceid; //系统资源编码
    private String section; //所属单位
    private String sid; //单位id
    private String sitecode; //地理编码
    private String type; //类型
    private String control_device_id; //控制器设备id
    private String control_tool_ID; //控制器id
    private String control_tool_type; //控制器类型
    private String control_work_mode; //控制器工作模式
    private int lockCount; //锁数量
    private List<Controller> controllers = new ArrayList<>();
    private List<Zone> zones = new ArrayList<>();
    private List<String> filePath = new ArrayList<>();

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setCompanyid(int acompanyid) {
        this.companyid = acompanyid;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getGprs() {
        return gprs;
    }

    public void setGprs(String gprs) {
        this.gprs = gprs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public List<Lockset> getLocksets() {
        return locksets;
    }

    public void setLocksets(List<Lockset> locksets) {
        this.locksets = locksets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getResourceid() {
        return resourceid;
    }

    public void setResourceid(String resourceid) {
        this.resourceid = resourceid;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSitecode() {
        return sitecode;
    }

    public void setSitecode(String sitecode) {
        this.sitecode = sitecode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getControl_device_id() {
        return control_device_id;
    }

    public void setControl_device_id(String control_device_id) {
        this.control_device_id = control_device_id;
    }

    public String getControl_tool_ID() {
        return control_tool_ID;
    }

    public void setControl_tool_ID(String control_tool_ID) {
        this.control_tool_ID = control_tool_ID;
    }

    public String getControl_tool_type() {
        return control_tool_type;
    }

    public void setControl_tool_type(String control_tool_type) {
        this.control_tool_type = control_tool_type;
    }

    public String getControl_work_mode() {
        return control_work_mode;
    }

    public void setControl_work_mode(String control_work_mode) {
        this.control_work_mode = control_work_mode;
    }

    public int getLockCount() {
        return lockCount;
    }

    public void setLockCount(int lockCount) {
        this.lockCount = lockCount;
    }

    public List<Controller> getControllers() {
        return controllers;
    }

    public void setControllers(List<Controller> controllers) {
        this.controllers = controllers;
    }

    public List<Zone> getZones() {
        return zones;
    }

    public void setZones(List<Zone> zones) {
        this.zones = zones;
    }

    public List<String> getFilePath() {
        return filePath;
    }

    public void setFilePath(List<String> filePath) {
        this.filePath = filePath;
    }

    public Station() {
    }

    /**
     * 把modelA对象的属性值赋值给bClass对象的属性。(主要用于把Station转换成DeviceObject对象，方便与以前内容进行兼容)
     *
     * @param modelA
     * @param bClass
     * @param <T>
     * @return
     */
    public static <A, T> T modelAconvertoB(A modelA, Class<T> bClass) {
        try {
            Gson gson = new Gson();
            String gsonA = gson.toJson(modelA);
            T instanceB = gson.fromJson(gsonA, bClass);
            return instanceB;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.address);
        dest.writeString(this.area);
        dest.writeString(this.code);
        dest.writeString(this.carrier);
        dest.writeInt(this.companyid);
        dest.writeString(this.company_name);
        dest.writeString(this.gprs);
        dest.writeInt(this.id);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lng);
        dest.writeTypedList(this.locksets);
        dest.writeString(this.name);
        dest.writeString(this.phone);
        dest.writeString(this.remark);
        dest.writeString(this.resourceid);
        dest.writeString(this.section);
        dest.writeString(this.sid);
        dest.writeString(this.sitecode);
        dest.writeString(this.type);
        dest.writeString(this.control_device_id);
        dest.writeString(this.control_tool_ID);
        dest.writeString(this.control_tool_type);
        dest.writeString(this.control_work_mode);
        dest.writeInt(this.lockCount);
        dest.writeTypedList(this.controllers);
        dest.writeList(this.zones);
        dest.writeStringList(this.filePath);
    }

    protected Station(Parcel in) {
        this.address = in.readString();
        this.area = in.readString();
        this.code = in.readString();
        this.carrier = in.readString();
        this.companyid = in.readInt();
        this.company_name = in.readString();
        this.gprs = in.readString();
        this.id = in.readInt();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
        this.locksets = in.createTypedArrayList(Lockset.CREATOR);
        this.name = in.readString();
        this.phone = in.readString();
        this.remark = in.readString();
        this.resourceid = in.readString();
        this.section = in.readString();
        this.sid = in.readString();
        this.sitecode = in.readString();
        this.type = in.readString();
        this.control_device_id = in.readString();
        this.control_tool_ID = in.readString();
        this.control_tool_type = in.readString();
        this.control_work_mode = in.readString();
        this.lockCount = in.readInt();
        this.controllers = in.createTypedArrayList(Controller.CREATOR);
        this.zones = new ArrayList<Zone>();
        in.readList(this.zones, Zone.class.getClassLoader());
        this.filePath = in.createStringArrayList();
    }

    public static final Creator<Station> CREATOR = new Creator<Station>() {
        @Override
        public Station createFromParcel(Parcel source) {
            return new Station(source);
        }

        @Override
        public Station[] newArray(int size) {
            return new Station[size];
        }
    };
}
