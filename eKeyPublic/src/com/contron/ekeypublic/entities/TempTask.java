package com.contron.ekeypublic.entities;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.contron.ekeypublic.util.DateUtil;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;

/**
 * The persistent class for the temp_task database table.
 * 
 */
// @Entity
 @Table(name="temp_task")
public class TempTask implements Parcelable {

	@Id
	@NoAutoIncrement
	private int id;

	// @Column(name="apply_at")
	private String apply_at;

	private String apply_by;
	
	private String apply_username;

	private String apply_id;

	// @Column(name="begin_at")

	private String begin_at;

	// @Column(name="end_at")
	private String end_at;

	private String name;

	private String status;

	// @Column(name="verify_at")
	private String verify_at;

	// @Column(name="verify_by")
	private String verify_by;

	private String memo;

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Transient
	private transient int limited;//任务时长
	
	@Transient
	private transient Bitmap portraitmap;//头像
	
	@Transient
	private transient Bitmap idcardmap;//身份证
	
	

	public transient List<TempDevice> tempTaskObjects = new ArrayList<TempDevice>();

	private transient List<Lockset> locksetList = new ArrayList<Lockset>();

	private transient List<User> userList = new ArrayList<User>();
	
	public TempTask() {
	}

	

	public TempTask(int id, String name, String applyby, String applyAt,
			String verifyBy, String verifyAt, String beginAt, String endAt,String apply_username,
			String stauts) {
		this.setId(id);
		this.setName(name);
		this.setApply_username(apply_username);
		this.setApply_by(applyby);
		this.setApplyAt(applyAt);
		this.setBeginAt(beginAt);
		this.setEndAt(endAt);
		this.setVerifyBy(verifyBy);
		this.setVerifyAt(verifyAt);
		this.setStatus(stauts);
	}

	public TempTask(String name, String applyBy, String applyAt,
			String beginAt, String endAt, String stauts) {
		this.setName(name);
		this.setApply_username(applyBy);
		this.setApplyAt(applyAt);
		this.setBeginAt(beginAt);
		this.setEndAt(endAt);
		this.setStatus(stauts);
	}

	public TempTask(String name, String applyBy, String applyAt,
			String verifyBy, String verifyAt, String beginAt, String endAt,String apply_username,
			String stauts) {
		this.setName(name);
		this.setApply_username(apply_username);
		this.setApply_by(applyBy);
		this.setApplyAt(applyAt);
		this.setBeginAt(beginAt);
		this.setEndAt(endAt);
		this.setVerifyBy(verifyBy);
		this.setVerifyAt(verifyAt);
		this.setStatus(stauts);
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApplyAt() {
		return DateUtil.getStringByFormat(apply_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setApplyAt(String applyAt) {
		this.apply_at = applyAt;
	}



	public String getApply_username() {
		return apply_username;
	}

	public void setApply_username(String apply_username) {
		this.apply_username = apply_username;
	}

	public String getBeginAt() {
		return DateUtil.getStringByFormat(begin_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setBeginAt(String beginAt) {
		this.begin_at = beginAt;
	}

	public String getEndAt() {
		return DateUtil.getStringByFormat(end_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setEndAt(String endAt) {
		this.end_at = endAt;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVerifyAt() {
		return DateUtil.getStringByFormat(verify_at, DateUtil.DATE_FORMAT_YMDHMS, DateUtil.dateFormatYMDHMS);
	}

	public void setVerifyAt(String verifyAt2) {
		this.verify_at = verifyAt2;
	}

	public String getVerifyBy() {
		return this.verify_by;
	}

	public void setVerifyBy(String verifyBy) {
		this.verify_by = verifyBy;
	}

	public List<Lockset> getLocksetList() {
		return locksetList;
	}

	public void setLocksetList(ArrayList<Lockset> locksetList) {
		this.locksetList.addAll(locksetList);
	}

	

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	public List<TempDevice> getTempTaskObjects() {
		return tempTaskObjects;
	}

	public void setTempTaskObjects(List<TempDevice> tempTaskObjects) {
		this.tempTaskObjects = tempTaskObjects;
	}

	public String getApply_id() {
		return apply_id;
	}

	public void setApply_id(String apply_id) {
		this.apply_id = apply_id;
	}

	
	
	public int getLimited() {
		return limited;
	}

	public void setLimited(int limited) {
		this.limited = limited;
	}

	public Bitmap getPortraitmap() {
		return portraitmap;
	}

	public void setPortraitmap(Bitmap portraitmap) {
		this.portraitmap = portraitmap;
	}

	public Bitmap getIdcardmap() {
		return idcardmap;
	}

	public void setIdcardmap(Bitmap idcardmap) {
		this.idcardmap = idcardmap;
	}

	
	
	
	public String getApply_by() {
		return apply_by;
	}

	public void setApply_by(String apply_by) {
		this.apply_by = apply_by;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub
		arg0.writeInt(id);
		arg0.writeString(apply_at);
		arg0.writeString(apply_username);
		arg0.writeString(begin_at);
		arg0.writeString(end_at);
		arg0.writeString(name);
		arg0.writeString(status);
		arg0.writeString(apply_by);		
		arg0.writeInt(limited);
		arg0.writeList(tempTaskObjects);		
		arg0.writeList(locksetList);
		arg0.writeList(userList);
	}

	@SuppressWarnings("unchecked")
	public TempTask(Parcel source) {
		this.id = source.readInt();
		this.apply_at = source.readString();
		this.apply_username = source.readString();
		this.begin_at = source.readString();
		this.end_at = source.readString();
		this.name = source.readString();
		this.status = source.readString();
		this.apply_by = source.readString();
		this.limited = source.readInt();
		this.tempTaskObjects = source.readArrayList(TempDevice.class.getClassLoader());
		this.locksetList = source.readArrayList(Lockset.class.getClassLoader());
		this.userList = source.readArrayList(User.class.getClassLoader());
	}

	public static final Parcelable.Creator<TempTask> CREATOR = new Creator<TempTask>() {

		@Override
		public TempTask[] newArray(int size) {
			return new TempTask[size];
		}

		@Override
		public TempTask createFromParcel(Parcel source) {
			return new TempTask(source);
		}
	};

}