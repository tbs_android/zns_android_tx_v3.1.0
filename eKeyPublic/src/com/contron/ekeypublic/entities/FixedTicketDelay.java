package com.contron.ekeypublic.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;

@Table(name = "fixedTicketDelay")
public class FixedTicketDelay extends EntityBase implements Parcelable {
	@Id
	public int _id;// ��������
	public int ReplyID;
	public String ReplyTime;
	public String TicketId;
	public float Limited;
	public String BeginTime;
	public String EndTime;
	public int UserId;
	
	public FixedTicketDelay() {
		
	}
	
	public FixedTicketDelay(Parcel source) {
		super();
		this._id = source.readInt();
		this.ReplyID = source.readInt();
		this.ReplyTime = source.readString();
		this.TicketId = source.readString();
		this.Limited = source.readFloat();
		this.BeginTime = source.readString();
		this.EndTime = source.readString();
		this.UserId = source.readInt();
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(_id);
		dest.writeInt(ReplyID);
		dest.writeString(ReplyTime);
		dest.writeString(TicketId);
		dest.writeFloat(Limited);
		dest.writeString(BeginTime);
		dest.writeString(EndTime);
		dest.writeInt(UserId);
	}
}
