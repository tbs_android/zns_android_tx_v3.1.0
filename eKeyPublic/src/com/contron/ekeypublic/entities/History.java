package com.contron.ekeypublic.entities;

import java.util.Date;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;

/**
 * The persistent class for the history database table.
 * 
 */

@Table(name="history")
public class History {

	@Id
    //@Column(name="id")
	//@GeneratedValue(generator = "generator")
	//@GenericGenerator(name = "generator", strategy = "increment")
	private int id;
	
	private int oid;
	
	private String oname;
	
	private String ocarrier;
	
	private String oarea;
	
	private String otype;
	
	private int lid;
	
	private String ltype;
	
	private String action;
	
	//@Column(name="unlock_by")
	private String unlockBy;
	
	//@Column(name="unlock_at")
	private Date unlockAt;
	
	private int usid;
	
	private String usection;

	private String result;

	//@Column(name="verify_by")
	private String verifyBy;

	//@Column(name="verify_at")
	private Date verifyAt;

	public History() {
	}
	
	
	public History(String action, String result, String verify_by, Date verify_at){
		this.action = action;
		this.result = result;
		this.verifyBy = verify_by;
		this.verifyAt = verify_at;
	}
	public History(int id, int oid,String oname,String ocarrier,String oarea,String otype,int lid,String ltype, String action,String unlockBy,Date unlockAt
			,String usection , String result, String verifyBy,Date verifyAt){
		this.id = id;
		this.oid = oid;
		this.oname = oname;
		this.ocarrier = ocarrier;
		this.oarea = oarea;
		this.otype = otype;
		this.lid = lid;
		this.ltype = ltype;
		this.action = action;
		this.unlockBy = unlockBy;
		this.unlockAt = unlockAt;
		this.usection = usection;
		this.result = result;
		this.verifyBy = verifyBy;
		this.verifyAt = verifyAt;
	}
	public History(int oid, String oname, String ocarrier, String oarea, String otype, int lid, String ltype, String action, String unlock_by, Date unlock_at, int usid, String usection, String verify_by, Date verify_at, String result){
		this.oid = oid;
		this.oname = oname;
		this.ocarrier = ocarrier;
		this.oarea = oarea;
		this.otype = otype;
		this.lid = lid;
		this.ltype = ltype;
		this.action = action;
		this.unlockBy = unlock_by;
		this.unlockAt = unlock_at;
		this.usid = usid;
		this.usection = usection;
		this.verifyBy = verify_by;
		this.verifyAt = verify_at;
		this.result = result;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAction() {
		return this.action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getLid() {
		return this.lid;
	}

	public void setLid(int lid) {
		this.lid = lid;
	}

	public String getLtype() {
		return ltype;
	}
	public void setLtype(String ltype) {
		this.ltype = ltype;
	}
	public String getOarea() {
		return this.oarea;
	}

	public void setOarea(String oarea) {
		this.oarea = oarea;
	}

	public String getOcarrier() {
		return this.ocarrier;
	}

	public void setOcarrier(String ocarrier) {
		this.ocarrier = ocarrier;
	}

	public int getOid() {
		return this.oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getOname() {
		return this.oname;
	}

	public void setOname(String oname) {
		this.oname = oname;
	}

	public String getOtype() {
		return this.otype;
	}

	public void setOtype(String otype) {
		this.otype = otype;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getUnlockAt() {
		return this.unlockAt;
	}

	public void setUnlockAt(Date unlockAt) {
		this.unlockAt = unlockAt;
	}

	public String getUnlockBy() {
		return this.unlockBy;
	}

	public void setUnlockBy(String unlockBy) {
		this.unlockBy = unlockBy;
	}

	public String getUsection() {
		return this.usection;
	}

	public void setUsection(String usection) {
		this.usection = usection;
	}

	public Date getVerifyAt() {
		return this.verifyAt;
	}

	public void setVerifyAt(Date verifyAt) {
		this.verifyAt = verifyAt;
	}

	public String getVerifyBy() {
		return this.verifyBy;
	}

	public void setVerifyBy(String verifyBy) {
		this.verifyBy = verifyBy;
	}
	public int getUsid() {
		return usid;
	}
	public void setUsid(int usid) {
		this.usid = usid;
	}
}