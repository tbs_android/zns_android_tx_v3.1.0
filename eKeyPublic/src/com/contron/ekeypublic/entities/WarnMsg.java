package com.contron.ekeypublic.entities;

public class WarnMsg {

	public String date = "";// 操作时间
	public String deviceName = "";// 设备名称

	public WarnMsg(String msg) {
		super();
		this.deviceName = msg;
	}



	public WarnMsg(String date, String deviceName) {
		super();
		this.date = date;
		this.deviceName = deviceName;
	}



	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

}
