package com.contron.ekeypublic.entities;

import java.io.Serializable;
import java.util.Date;

import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;

/**
 * The persistent class for the offline database table.
 * 
 */
@Table(name = "offline")
public class Offline extends EntityBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@NoAutoIncrement
	private int id;

	private String apply_at;

	private String apply_by;
	

	private int duration;

	private int zid;

	private String status;

	private String verify_at;

	private String verify_by;
	
	private String beg_at;
	
	private String apply_username;

	private String action_at;

	public Offline() {
		super();
	}


	public Offline(int id, String apply_at, String apply_by, int duration,
			int zid, String status, String verify_at, String verify_by,
			String beg_at, String apply_username, String action_at) {
		super();
		this.id = id;
		this.apply_at = apply_at;
		this.apply_by = apply_by;
		this.duration = duration;
		this.zid = zid;
		this.status = status;
		this.verify_at = verify_at;
		this.verify_by = verify_by;
		this.beg_at = beg_at;
		this.apply_username = apply_username;
		this.action_at = action_at;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApply_at() {
		return apply_at;
	}

	public void setApply_at(String apply_at) {
		this.apply_at = apply_at;
	}

	public String getApply_by() {
		return apply_by;
	}

	public void setApply_by(String apply_by) {
		this.apply_by = apply_by;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getZid() {
		return zid;
	}

	public void setZid(int zid) {
		this.zid = zid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVerify_at() {
		return verify_at;
	}

	public void setVerify_at(String verify_at) {
		this.verify_at = verify_at;
	}

	public String getVerify_by() {
		return verify_by;
	}

	public void setVerify_by(String verify_by) {
		this.verify_by = verify_by;
	}

	public String getBeg_at() {
		return beg_at;
	}

	public void setBeg_at(String beg_at) {
		this.beg_at = beg_at;
	}

	public String getAction_at() {
		return action_at;
	}

	public void setAction_at(String action_at) {
		this.action_at = action_at;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getApply_username() {
		return apply_username;
	}


	public void setApply_username(String apply_username) {
		this.apply_username = apply_username;
	}



	
	
	
}