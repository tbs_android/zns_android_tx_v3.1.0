package com.contron.ekeypublic.entities;

public class SmartkeyBorrow extends EntityBase {

	private int id;
	private int sid;
	private String sname;
	private String stype;
	private String ssection;
	private String borrow_username;
	private String borrow_by;
	private String create_username;
	private String create_by;
	private String create_at;
	private String return_at;
	private String btaddr;

	public SmartkeyBorrow() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getStype() {
		return stype;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}

	public String getSsection() {
		return ssection;
	}

	public void setSsection(String ssection) {
		this.ssection = ssection;
	}

	public String getBorrow_username() {
		return borrow_username;
	}

	public void setBorrow_username(String borrow_username) {
		this.borrow_username = borrow_username;
	}

	public String getBorrow_by() {
		return borrow_by;
	}

	public void setBorrow_by(String borrow_by) {
		this.borrow_by = borrow_by;
	}

	public String getCreate_username() {
		return create_username;
	}

	public void setCreate_username(String create_username) {
		this.create_username = create_username;
	}

	public String getCreate_by() {
		return create_by;
	}

	public void setCreate_by(String create_by) {
		this.create_by = create_by;
	}



	public String getReturn_at() {
		return return_at;
	}

	public void setReturn_at(String return_at) {
		this.return_at = return_at;
	}

	public String getCreate_at() {
		return create_at;
	}

	public void setCreate_at(String create_at) {
		this.create_at = create_at;
	}

	public String getBtaddr() {
		return btaddr;
	}

	public void setBtaddr(String btaddr) {
		this.btaddr = btaddr;
	}



	
	
//	805623
}
