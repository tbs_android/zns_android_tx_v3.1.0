package com.contron.ekeypublic.entities;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;

/**
 * The persistent class for the lock database table.
 * 
 */
@Table(name = "lockset")
public class Lockset extends EntityBase implements Parcelable {
	private static final long serialVersionUID = 1L;

	@Id
	// @Column(name = "id")
	@NoAutoIncrement
	private int id;

	// @Column(name = "name")
	private String name = "";

	// @Column(name = "btaddr")
	private String btaddr = "";

	// @Column(name = "btname")
	private String btname = "";

	// @Column(name = "rfid")
	private String rfid = "";

	// @Column(name = "type")
	private String type;

	// @Column(name = "oid")
	private int oid;

	private String section;

	@Transient
	private transient String currentDate;

	@Transient
	private transient int stationId;

	@Transient
	private transient int opStatus;

	@Transient
	private transient int logic;

	@Transient
	public transient boolean isCheck;

	private int state;

	private String latitude;

	private String longitude;

	private String doorcode; //门编号
	private String doortype; //门类型
	private String lock_status; //锁状态
	private String gate_status; //门状态

	public Lockset() {

	}

	public Lockset(int id, int oid, String name, String type, String rfid,
			String btname, String btaddr, boolean isCheck) {
		this.setId(id);
		this.setName(name);
		this.setOid(oid);
		this.setType(type);
		this.setRfid(rfid);
		this.setBtname(btname);
		this.setBtaddr(btaddr);
		this.setCheck(isCheck);
	}

	/*
	 * public Lockset(int oid, String name, String type, String rfid, String
	 * btname, String btaddr) { this.setName(name); this.setOid(oid);
	 * this.setType(type); this.setRfid(rfid); this.setBtname(btname);
	 * this.setBtaddr(btaddr); }
	 */

	/**
	 * @return the isCheck
	 */
	public boolean isCheck() {
		return isCheck;
	}

	/**
	 * @param isCheck
	 *            the isCheck to set
	 */
	public void setCheck(boolean isCheck) {
		this.isCheck = isCheck;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBtaddr() {
		return btaddr;
	}

	public void setBtaddr(String btaddr) {
		this.btaddr = btaddr;
	}

	public String getBtname() {
		return btname;
	}

	public void setBtname(String btname) {
		this.btname = btname;
	}

	public String getRfid() {
		return rfid;
	}

	public void setRfid(String rfid) {
		this.rfid = rfid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public int getOpStatus() {
		return opStatus;
	}

	public void setOpStatus(int opStatus) {
		this.opStatus = opStatus;
	}

	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

	public int getLogic() {
		return logic;
	}

	public void setLogic(int logic) {
		this.logic = logic;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getDoorcode() {
		return doorcode;
	}

	public void setDoorcode(String doorcode) {
		this.doorcode = doorcode;
	}

	public String getDoortype() {
		return doortype;
	}

	public void setDoortype(String doortype) {
		this.doortype = doortype;
	}

	public String getLock_status() {
		return lock_status;
	}

	public void setLock_status(String lock_status) {
		this.lock_status = lock_status;
	}

	public String getGate_status() {
		return gate_status;
	}

	public void setGate_status(String gate_status) {
		this.gate_status = gate_status;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.id);
		dest.writeString(this.name);
		dest.writeString(this.btaddr);
		dest.writeString(this.btname);
		dest.writeString(this.rfid);
		dest.writeString(this.type);
		dest.writeInt(this.oid);
		dest.writeString(this.section);
		dest.writeInt(this.state);
		dest.writeString(this.latitude);
		dest.writeString(this.longitude);
		dest.writeString(this.doorcode);
		dest.writeString(this.doortype);
		dest.writeString(this.lock_status);
		dest.writeString(this.gate_status);
	}

	protected Lockset(Parcel in) {
		this.id = in.readInt();
		this.name = in.readString();
		this.btaddr = in.readString();
		this.btname = in.readString();
		this.rfid = in.readString();
		this.type = in.readString();
		this.oid = in.readInt();
		this.section = in.readString();
		this.state = in.readInt();
		this.latitude = in.readString();
		this.longitude = in.readString();
		this.doorcode = in.readString();
		this.doortype = in.readString();
		this.lock_status = in.readString();
		this.gate_status = in.readString();
	}

	public static final Creator<Lockset> CREATOR = new Creator<Lockset>() {
		@Override
		public Lockset createFromParcel(Parcel source) {
			return new Lockset(source);
		}

		@Override
		public Lockset[] newArray(int size) {
			return new Lockset[size];
		}
	};
}