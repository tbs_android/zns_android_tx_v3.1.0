/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.lidroid.xutils.db.annotation.NoAutoIncrement;
import com.lidroid.xutils.db.annotation.Table;
import com.lidroid.xutils.db.annotation.Transient;

/**
 * 用户
 * 
 * @author hupei
 * @date 2015年4月2日 下午2:23:57
 */
@Table(name = "user")
public class User extends EntityBase implements Parcelable{
	@NoAutoIncrement
	private int id;
	private String username= "";//电话号码
	private String name= "";
	private String memo= "";
	private int sid;
	private String section= "";
	@Transient
	private String reg_at;
	@Transient
	private String verify_at;
	@Transient
	private String verify_by= "";
	@Transient
	private String lastlogin_at;
	@Transient
	private List<Role> roles;
	@Transient
	private List<String> permission;
	@Transient
	private String kValue= "" ;
	@Transient
	private String password= "";
	@Transient
	private String rfid= "";
	@Transient
	public boolean isOnline;// 是否在线模式
	
	public String getLastlogin_at() {
		return lastlogin_at;
	}
	public void setLastlogin_at(String lastlogin_at) {
		this.lastlogin_at = lastlogin_at;
	}
	private String status;
	
	public User(){
		
	}
	public User(String username, String password, String name, int sid, String section, String memo, String reg_at){
		this.setUsername(username);
		this.setPassword(password);
		this.setName(name);
		this.setSid(sid);
		this.setSection(section);
		this.setMemo(memo);
		this.setReg_at(reg_at);
		this.setStatus("待审批");
	}
	public User(int id, String username, String password, 
			String name, int sid, String section, String memo, 
			String reg_at, String ver_at, String ver_by, String status, String lastlogin_at){
		this.setId(id);
		this.setUsername(username);
		this.setPassword(password);
		this.setName(name);
		this.setMemo(memo);
		this.setSid(sid);
		this.setSection(section);
		this.setReg_at(reg_at);
		this.setVerify_at(ver_at);
		this.setVerify_by(ver_by);
		this.setStatus(status);
		this.setLastlogin_at(lastlogin_at);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		// TODO Auto-generated method stub
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setKValue(String kValue) {
		// TODO Auto-generated method stub
		this.kValue = kValue;
	}
	public String getKValue() {
		return kValue;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getReg_at() {
		return reg_at;
	}
	public void setReg_at(String reg_at) {
		this.reg_at = reg_at;
	}
	public String getVerify_at() {
		return verify_at;
	}
	public void setVerify_at(String verify_at) {
		this.verify_at = verify_at;
	}
	public String getVerify_by() {
		return verify_by;
	}
	public void setVerify_by(String verify_by) {
		this.verify_by = verify_by;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<String>  getPermision() {
		return permission;
	}
	public void setPermision(List<String> permission) {
		this.permission = permission;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getRfid() {
		return rfid;
	}
	public void setRfid(String rfid) {
		this.rfid = rfid;
	}
	@SuppressWarnings("unchecked")
	public User(Parcel source) {
		this.id = source.readInt();
		this.sid = source.readInt();
		this.name = source.readString();
		this.username = source.readString();
		this.section = source.readString();
		
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flag) {
		// TODO Auto-generated method stub
		dest.writeInt(id);
		dest.writeInt(sid);
		dest.writeString(name);
		dest.writeString(username);
		dest.writeString(section);
		
	}

	public static Parcelable.Creator<User> CREATOR = new Creator<User>() {
		@Override
		public User[] newArray(int size) {
			return new User[size];
		}

		@Override
		public User createFromParcel(Parcel source) {
			return new User(source);
		}
	};
}
