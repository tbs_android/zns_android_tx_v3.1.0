/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.entities;

import com.lidroid.xutils.db.annotation.Table;

/**
 * 操作记录
 * 
 * @author hupei
 * @date 2015年4月8日 下午2:33:22
 */
@Table(name = "ticketResult")
public class TicketResult extends EntityBase {
	public transient int _id;
	public String ticketId = "";// 任务ID（32字符的GUID）
	public int ticketType;// 任务类型
	public int stationId;// 站号
	public int lockerId;// 锁ID
	public int userId;// 操作人ID
	/** 0：操作指令失败，1：操作指令成功，2：开锁成功 ，3：开门成功，4：关门成功 ，5：闭锁成功，6：正常操作，255：越权操作 */
	public int opStatus = 255;

	public String opDate = "";// 操作日期
	public String opTime = "";// 操作时间
	public String backDateTime = "";// 回传日期
	public int OpStype;
	public transient String lockerRFID = "";
	public transient String deviceName = "未知设备";// 设备名称
	public transient int isUpload = 0;// 是否上传 0未上传; 1上传
	public transient String section;//区域名称

	/**
	 * @author hupei
	 * @date 2015年4月30日 上午9:30:41
	 */
	public TicketResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @author hupei
	 * @date 2015年5月4日 下午2:03:42
	 * @param ticketId
	 * @param ticketType
	 * @param stationId
	 * @param lockerId
	 * @param lockerRFID
	 * @param deviceName
	 * @param userId
	 * @param opStatus
	 * @param opDate
	 * @param opTime
	 * @param backDateTime
	 */
	public TicketResult(String ticketId, int ticketType, int stationId,
			String lockerRFID, int userId, String opDate, String opTime,
			String backDateTime) {
		super();
		this.ticketId = ticketId;
		this.ticketType = ticketType;
		this.stationId = stationId;
		this.lockerRFID = lockerRFID;
		this.userId = userId;
		this.opDate = opDate;
		this.opTime = opTime;
		this.backDateTime = backDateTime;
	}

	/**
	 * @author hupei
	 * @date 2015年9月29日 下午4:34:00
	 * @param ticketId
	 * @param ticketType
	 * @param stationId
	 * @param lockerId
	 * @param userId
	 * @param opStatus
	 * @param opDate
	 * @param opTime
	 * @param backDateTime
	 */
	public TicketResult(String ticketId, int ticketType, int stationId,
			int lockerId, int userId, int opStatus, String opDate,
			String opTime, String backDateTime) {
		super();
		this.ticketId = ticketId;
		this.ticketType = ticketType;
		this.stationId = stationId;
		this.lockerId = lockerId;
		this.userId = userId;
		this.opStatus = opStatus;
		this.opDate = opDate;
		this.opTime = opTime;
		this.backDateTime = backDateTime;
	}

	public TicketResult(String ticketId, int ticketType, int stationId,
			int lockerId, int userId, int opStatus, String opDate,
			String opTime, String backDateTime, int opStype) {
		super();
		this.ticketId = ticketId;
		this.ticketType = ticketType;
		this.stationId = stationId;
		this.lockerId = lockerId;
		this.userId = userId;
		this.opStatus = opStatus;
		this.opDate = opDate;
		this.opTime = opTime;
		this.backDateTime = backDateTime;
		this.OpStype = opStype;
	}

	@Override
	public boolean equals(java.lang.Object obj) {
		// TODO Auto-generated method stub
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TicketResult other = (TicketResult) obj;

		if (lockerId != other.lockerId || opStatus != other.opStatus
				|| (opTime != other.opTime && opDate != other.opDate)) {
			return false;
		}

		return true;
	}
}
