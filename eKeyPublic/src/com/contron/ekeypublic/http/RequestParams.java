package com.contron.ekeypublic.http;

import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeypublic.entities.User;

public class RequestParams extends JSONObject {

	public RequestParams(User user) {
		super();
		
		try {
		
			if (user != null)
				put("k", user.getKValue());//设置参数K值
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 添加参数
	 * 
	 * @param name
	 *            参数名称
	 * @param value
	 *            参数值
	 */
	public void putParams(String name, Object value) {
		try {
			put(name, value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
