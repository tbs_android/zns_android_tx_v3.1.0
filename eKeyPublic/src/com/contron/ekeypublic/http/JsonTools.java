package com.contron.ekeypublic.http;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.contron.ekeypublic.entities.Company;
import com.contron.ekeypublic.entities.Controller;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.LockerResult;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.LocksetAll;
import com.contron.ekeypublic.entities.Offline;
import com.contron.ekeypublic.entities.OfflineHistory;
import com.contron.ekeypublic.entities.OfflineOperation;
import com.contron.ekeypublic.entities.Role;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.SmsResult;
import com.contron.ekeypublic.entities.Station;
import com.contron.ekeypublic.entities.TempDevice;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.UserZone;
import com.contron.ekeypublic.entities.Zone;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author jack
 * 
 */
public class JsonTools {
	final static Gson gson = new Gson();

	public JsonTools() {
	}

	public static User getUser(String jsonString) {
		User user = new User();
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			// user.setPassword(jsonObject.getString("password"));
			user.setUsername(jsonObject.getString("username"));
			user.setName(jsonObject.getString("name"));
			user.setMemo(jsonObject.getString("memo"));
			user.setSection(jsonObject.getString("section"));
			user.setSid(jsonObject.getInt("sid"));
			user.setId(jsonObject.getInt("id"));
			user.setKValue(jsonObject.getString("k"));

			JSONArray jsonArrayRoles = jsonObject.getJSONArray("role");
			List<Role> roles = gson.fromJson(jsonArrayRoles.toString(),
					new TypeToken<List<Role>>() {
					}.getType());
			user.setRoles(roles);

			if (jsonString.contains("permission")) {
				JSONArray jsonArrayPermissions = jsonObject
						.getJSONArray("permission");
				List<String> permissions = gson.fromJson(
						jsonArrayPermissions.toString(),
						new TypeToken<List<String>>() {
						}.getType());

				user.setPermision(permissions);
			}
			user.setStatus(jsonObject.getString("status"));
			user.setVerify_by(jsonObject.getString("verify_by"));
			user.setVerify_at(jsonObject.getString("verify_at"));
			user.setLastlogin_at(jsonObject.getString("lastlogin_at"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	public static User getRegUser(String jsonString) {
		User user = new User();
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			// user.setPassword(jsonObject.getString("password"));
			user.setUsername(jsonObject.getString("username"));
			user.setName(jsonObject.getString("name"));
			user.setMemo(jsonObject.getString("memo"));
			user.setSection(jsonObject.getString("section"));
			user.setSid(jsonObject.getInt("sid"));
			user.setId(jsonObject.getInt("id"));
			user.setStatus(jsonObject.getString("status"));
			user.setVerify_by(jsonObject.getString("verify_by"));
			// user.setVerify_at(jsonObject.getString("verify_at"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	@SuppressWarnings("finally")
	public static List<Section> getSections(String key, String jsonString) {
		List<Section> sections = null;
		List<Company> companies = null;
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArraySections = jsonObject.getJSONArray("items");

			sections = gson.fromJson(jsonArraySections.toString(),
					new TypeToken<List<Section>>() {
					}.getType());

			for (int i = 0; i < jsonArraySections.length(); i++) {
				JSONObject jsonObject1 = jsonArraySections.getJSONObject(i);
				JSONArray jsonArray1 = jsonObject1.getJSONArray("companys");
				companies = gson.fromJson(jsonArray1.toString(),
						new TypeToken<List<Company>>() {
						}.getType());
				sections.get(i).setCompanyList(companies);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			return sections;
		}
	}

	/**
	 * 权限内锁转成实体
	 * 
	 * @param jsonArrayArea
	 * @return
	 */
	@SuppressWarnings("finally")
	public static List<Lockset> getAllDoorlocker(JSONArray jsonArrayArea) {
		List<Lockset> doorLocker = null;
		ArrayList<Lockset> doorLockers = new ArrayList<Lockset>();
		try {
			// JSONObject jsonObject = new JSONObject(jsonString);
			// JSONArray jsonArrayArea = jsonObject.getJSONArray("items");//
			// 取出items数组
			for (int i = 0; i < jsonArrayArea.length(); i++) {
				JSONObject jsonObject1 = jsonArrayArea.getJSONObject(i);
				String section = jsonObject1.getString("section");
				JSONArray jsonArrayDoorLockers = jsonObject1
						.getJSONArray("lockset");// 循环取出items中的lockset数组
				doorLocker = gson.fromJson(jsonArrayDoorLockers.toString(),
						new TypeToken<List<Lockset>>() {
						}.getType());
				if (doorLocker != null) {
					for (int j = 0; j < doorLocker.size(); j++) {// doorLocker数组中的Lockset增加section字段
						doorLocker.get(j).setSection(section);
					}
					doorLockers.addAll(doorLocker);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			return doorLockers;
		}

	}

	/*
	 * public static List<DeviceObject> getAllDeviceObject(String jsonString){
	 * List <DeviceObject> DeviceObjects = null; try { JSONObject jsonObject =
	 * new JSONObject(jsonString); JSONArray jsonArrayArea =
	 * jsonObject.getJSONArray("items"); //JSONArray jsonArrayDeviceObjects =
	 * jsonObject1.getJSONArray("object"); DeviceObjects =
	 * gson.fromJson(jsonArrayArea.toString(), new
	 * TypeToken<List<DeviceObject>>() { }.getType()); } catch (JSONException e)
	 * { e.printStackTrace(); } return DeviceObjects;
	 * 
	 * }
	 */

	@SuppressWarnings("finally")
	public static List<UserZone> getUserZone(String jsonString) {
		List<UserZone> userZone = null;
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArrayuserZone = jsonObject.getJSONArray("items");
			userZone = gson.fromJson(jsonArrayuserZone.toString(),
					new TypeToken<List<UserZone>>() {
					}.getType());
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			return userZone;
		}

	}

	/**
	 * JSON转换成User
	 * 
	 * @param jsonString
	 * @return
	 */
	@SuppressWarnings("finally")
	public static List<User> getAllUser(JSONArray jsonArrayuser) {
		List<User> allUsers = new ArrayList<User>();
		try {
			for (int i = 0; i < jsonArrayuser.length(); i++) {

				User user = new User();
				JSONObject jsonUser = (JSONObject) jsonArrayuser.get(i);
				user.setId(jsonUser.getInt("id"));
				user.setSid(jsonUser.getInt("sid"));
				user.setUsername(jsonUser.getString("username"));
				user.setMemo(jsonUser.has("memo") ? jsonUser.getString("memo")
						: "");
				user.setStatus(jsonUser.getString("status"));
				user.setPassword("000000");
				user.setRfid(jsonUser.has("rfid") ? jsonUser.getString("rfid")
						: "");
				user.setSection(jsonUser.getString("section"));
				user.setName(jsonUser.has("name") ? jsonUser.getString("name")
						: "");

				allUsers.add(user);

			}

			// allUsers = gson.fromJson(jsonArrayuser.toString(),
			// new TypeToken<List<User>>() {
			// }.getType());
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			return allUsers;
		}

	}

/**
	 * JSON转换成User
	 *
	 * @param jsonString
	 * @return
	 */
	@SuppressWarnings("finally")
	public static List<User> getOwner(JSONArray jsonArrayuser) {
		List<User> allUsers = new ArrayList<User>();
		try {
			for (int i = 0; i < jsonArrayuser.length(); i++) {

				User user = new User();
				JSONObject jsonUser = (JSONObject) jsonArrayuser.get(i);
				user.setUsername(jsonUser.getString("username"));
				user.setName(jsonUser.has("name") ? jsonUser.getString("name")
						: "");

				allUsers.add(user);

			}

			// allUsers = gson.fromJson(jsonArrayuser.toString(),
			// new TypeToken<List<User>>() {
			// }.getType());
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			return allUsers;
		}

	}

	@SuppressWarnings("finally")
	public static List<Smartkey> getSectionKeys(String jsonString) {
		List<Smartkey> smartkey = null;
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArraySmartkey = jsonObject.getJSONArray("items");
			smartkey = gson.fromJson(jsonArraySmartkey.toString(),
					new TypeToken<List<Smartkey>>() {
					}.getType());
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			return smartkey;
		}

	}

	/**
	 * 查询操作日志JSON转实体
	 * 
	 * @param jsonString
	 * @return
	 */
	public static List<LockerResult> getOperationResult(String jsonString) {
		List<LockerResult> smartkey = null;
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArraySmartkey = jsonObject.getJSONArray("items");
			smartkey = gson.fromJson(jsonArraySmartkey.toString(),
					new TypeToken<List<LockerResult>>() {
					}.getType());

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return smartkey;
	}
	/**
	 * 查询短信日志JSON转实体
	 * 
	 * @param jsonString
	 * @return
	 */
	public static List<SmsResult> getSmsResult(String jsonString) {
		List<SmsResult> smartkey = null;
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArraySmartkey = jsonObject.getJSONArray("items");
			smartkey = gson.fromJson(jsonArraySmartkey.toString(),
					new TypeToken<List<SmsResult>>() {
					}.getType());

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return smartkey;
	}
	
	
	public static ArrayList<DeviceObject> getObjects(String jsonString) {
		ArrayList<DeviceObject> Objects = null;
		List<Lockset> lockerList = null;
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArray = jsonObject.getJSONArray("items");
			Objects = gson.fromJson(jsonArray.toString(),
					new TypeToken<List<DeviceObject>>() {
					}.getType());

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject1 = jsonArray.getJSONObject(i);
				JSONArray jsonArray1 = jsonObject1.getJSONArray("lockset");
				lockerList = gson.fromJson(jsonArray1.toString(),
						new TypeToken<List<Lockset>>() {
						}.getType());
				Objects.get(i).setLockset(lockerList);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return Objects;
	}

	public static ArrayList<Station> getStations(String jsonString) {
		ArrayList<Station> Objects = null;
		List<Lockset> lockerList = null;
		List<Controller> controllerList = null;
		List<Zone> zoneList = null;
		List<String> fileList = null;
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArray = jsonObject.getJSONArray("items");
			Objects = gson.fromJson(jsonArray.toString(),
					new TypeToken<List<Station>>() {
					}.getType());



			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject1 = jsonArray.getJSONObject(i);
				JSONArray jsonArrayLockset = jsonObject1.getJSONArray("lockset");
				lockerList = gson.fromJson(jsonArrayLockset.toString(),
						new TypeToken<List<Lockset>>() {
						}.getType());
				Objects.get(i).setLocksets(lockerList);

				JSONArray jsonArrayController = jsonObject1.getJSONArray("controller");
				controllerList = gson.fromJson(jsonArrayController.toString(),
						new TypeToken<List<Controller>>(){
						}.getType());
				Objects.get(i).setControllers(controllerList);

				JSONArray jsonArrayZone = jsonObject1.getJSONArray("zids");
				zoneList = gson.fromJson(jsonArrayZone.toString(),
						new TypeToken<List<Zone>>(){
						}.getType());
				Objects.get(i).setZones(zoneList);

				JSONArray jsonArrayFile = jsonObject1.getJSONArray("files");
				fileList = gson.fromJson(jsonArrayFile.toString(),
						new TypeToken<List<String>>(){
						}.getType());
				Objects.get(i).setFilePath(fileList);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return Objects;
	}

	public static List<Dict> getDicts(String jsonString) {
		List<Dict> dicts = null;
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONArray jsonArrayDicts = jsonObject.getJSONArray("items");
			dicts = gson.fromJson(jsonArrayDicts.toString(),
					new TypeToken<List<Dict>>() {
					}.getType());

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return dicts;
	}

	@SuppressWarnings("finally")
	public static List<Role> getRoles(JSONArray jsonArrayRoles) {
		List<Role> role = null;

		// JSONObject jsonObject = new JSONObject(jsonString);
		// JSONArray jsonArrayRoles = jsonObject.getJSONArray("items");
		role = gson.fromJson(jsonArrayRoles.toString(),
				new TypeToken<List<Role>>() {
				}.getType());

		return role;

	}

	@SuppressWarnings("finally")
	public static List<Zone> getZone(JSONArray jsonArrayDicts) {
		List<Zone> zone = null;

		// JSONObject jsonObject = new JSONObject(jsonString);
		// JSONArray jsonArrayDicts = jsonObject.getJSONArray("items");
		zone = gson.fromJson(jsonArrayDicts.toString(),
				new TypeToken<List<Zone>>() {
				}.getType());

		return zone;

	}

	@SuppressWarnings("finally")
	public static List<Dict> getDict(JSONArray jsonArrayDicts) {
		List<Dict> dict = null;

		// JSONObject jsonObject = new JSONObject(jsonString);
		// JSONArray jsonArrayDicts = jsonObject.getJSONArray("items");
		dict = gson.fromJson(jsonArrayDicts.toString(),
				new TypeToken<List<Dict>>() {
				}.getType());

		return dict;

	}

	/**
	 * 转换成Offline实体
	 * 
	 * @param jsonString
	 * @return
	 */
	public static Offline getoffline(String jsonString) {
		Offline offline = null;
		if (jsonString.contains("success")) {
			offline = gson.fromJson(jsonString, Offline.class);
		}
		return offline;
	}

	/**
	 * json转实体
	 * 
	 * @param jsonArrayHead
	 */
	@SuppressWarnings("finally")
	public static List<LocksetAll> getLocksetAll(JSONArray jsonArrayHead) {

		List<LocksetAll> locksetAll = new ArrayList<LocksetAll>();
		// JSONObject jsonObject;
		try {
			// jsonObject = new JSONObject(value);
			// JSONArray jsonArrayHead = jsonObject.getJSONArray("items");
			for (int i = 0; i < jsonArrayHead.length(); i++) {
				JSONObject itemsjson = jsonArrayHead.getJSONObject(i);
				if (itemsjson.toString().contains("lockset")) {
					JSONArray locksetjson = itemsjson.getJSONArray("lockset");
					List<LocksetAll> ticketBodys = new Gson().fromJson(
							locksetjson.toString(),
							new TypeToken<List<LocksetAll>>() {
							}.getType());
					locksetAll.addAll(ticketBodys);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			return locksetAll;
		}

	}



	/**
	 * 获取临时任务下的设备
	 * 
	 * @param listTempTask
	 * @param jsonArrayTempTask
	 * @return
	 */
	@SuppressWarnings("finally")
	public static ArrayList<TempTask> getlistTempTask(
			ArrayList<TempTask> listTempTask, JSONArray jsonArrayTempTask) {
		try {
			// 获取任务下设备
			if (listTempTask != null && listTempTask.size() > 0) {
				for (int i = 0; i < listTempTask.size(); i++) {// 遍历临时任务

					JSONObject itemjson = jsonArrayTempTask.getJSONObject(i);
					JSONArray objectjson = itemjson.getJSONArray("object");

					for (int o = 0; o < objectjson.length(); o++) {// 遍历任务下设备
						JSONObject deviceJso = (JSONObject) objectjson.get(o);
						// 设备
						TempDevice tempDevice = new TempDevice(listTempTask
								.get(i).getId(), deviceJso.getInt("id"),
								deviceJso.getString("name"), 26.3, 32.2);

						listTempTask.get(i).getTempTaskObjects()
								.add(tempDevice);

						// 获取设备下锁
						ArrayList<Lockset> listLockset;

						String section = deviceJso.getString("section");

						JSONArray locksetJson = deviceJso
								.getJSONArray("lockset");
						listLockset = gson.fromJson(locksetJson.toString(),
								new TypeToken<List<Lockset>>() {
								}.getType());

						if (listLockset != null && listLockset.size() > 0) {
							for (int j = 0; j < listLockset.size(); j++) {
								listLockset.get(j).setSection(section);
							}

						}

						listTempTask.get(i).setLocksetList(listLockset);
					}

				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			return listTempTask;
		}
	}

	/**
	 * 获取固定任务下的设备、用户
	 * 
	 * @param listTempTask
	 * @param jsonArrayTempTask
	 * @return
	 */
	@SuppressWarnings("finally")
	public static ArrayList<TempTask> getFixationTask(
			ArrayList<TempTask> listTempTask, JSONArray jsonArrayTempTask) {
		try {
			// 获取任务下设备
			if (listTempTask != null && listTempTask.size() > 0) {
				for (int i = 0; i < listTempTask.size(); i++) {// 遍历临时任务

					JSONObject itemjson = jsonArrayTempTask.getJSONObject(i);
					JSONArray objectjson = itemjson.getJSONArray("object");
					JSONArray userjson = itemjson.getJSONArray("users");
					int limited = itemjson.getInt("limited");
					listTempTask.get(i).setLimited(limited);

					// 遍历任务下用户
					for (int t = 0; t < userjson.length(); t++) {
						JSONObject userJso = (JSONObject) userjson.get(t);
						User user = new User();
						user.setId(userJso.getInt("id"));
						user.setUsername(userJso.getString("username"));

						listTempTask.get(i).getUserList().add(user);
					}

					// 遍历任务下设备
					for (int o = 0; o < objectjson.length(); o++) {
						JSONObject deviceJso = (JSONObject) objectjson.get(o);
						// // 设备
						// TempDevice tempDevice = new TempDevice(listTempTask
						// .get(i).getId(), deviceJso.getInt("id"),
						// deviceJso.getString("name"), 26.3, 32.2);
						//
						// listTempTask.get(i).getTempTaskObjects()
						// .add(tempDevice);

						// 获取设备下锁
						ArrayList<Lockset> listLockset;

						JSONArray locksetJson = deviceJso
								.getJSONArray("lockset");

						listLockset = gson.fromJson(locksetJson.toString(),
								new TypeToken<List<Lockset>>() {
								}.getType());

						listTempTask.get(i).setLocksetList(listLockset);
					}

				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			return listTempTask;
		}
	}

	/**
	 * 转换成实体
	 * 
	 * @param value
	 * @return
	 */
	@SuppressWarnings("finally")
	public static List<String> jsontolistString(String value) {
		List<String> list = null;

		try {
			JSONObject jsonObject = new JSONObject(value);
			JSONArray jsonArrayDicts = jsonObject.getJSONArray("items");
			list = gson.fromJson(jsonArrayDicts.toString(),
					new TypeToken<List<String>>() {
					}.getType());

		} catch (JSONException e) {

			e.printStackTrace();
		} finally {
			return list;
		}

	}

	/**
	 * 把List<OfflineOperation>转成JSONArray上传操作记录
	 * 
	 * @param list
	 * @return JSONArray
	 */
	@SuppressWarnings("finally")
	public static JSONArray getJsonArrayOfflineOperation(
			List<OfflineOperation> list) {

		String liststring = gson.toJson(list);
		JSONArray jsarray = null;
		try {
			jsarray = new JSONArray(liststring);
		} catch (JSONException e1) {
			e1.printStackTrace();
		} finally {
			return jsarray;
		}
	}

	/**
	 * 把List<OfflineHistory>转成JSONArray上传操作记录
	 * 
	 * @param list
	 * @return JSONArray
	 */
	@SuppressWarnings("finally")
	public static JSONArray getJsonArrayOfflineHistory(List<OfflineHistory> list) {

		String liststring = gson.toJson(list);
		JSONArray jsarray = null;
		try {
			jsarray = new JSONArray(liststring);
		} catch (JSONException e1) {
			e1.printStackTrace();
		} finally {
			return jsarray;
		}
	}

}
