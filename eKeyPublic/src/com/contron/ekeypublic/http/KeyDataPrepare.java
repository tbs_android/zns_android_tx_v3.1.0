package com.contron.ekeypublic.http;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.User;
import com.google.gson.Gson;

/**
 * 初始化钥匙【处理下载的数据】
 * 
 * @author luoyilong
 *
 */
public class KeyDataPrepare {
	final static Gson gson = new Gson();

	private List<Lockset> lockDatas;// 锁数据集

	private List<User> allUser;// 列出所有启用用户

	public KeyDataPrepare() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 把锁信息JSON格式转换成锁信息实体
	 * 
	 * @param value
	 */
	public void JsonToLock(String value) {
		lockDatas = new ArrayList<Lockset>();
		try {
			JSONObject jsonObject = new JSONObject(value);

			// 设备数组
			JSONArray jsonArray = jsonObject.getJSONArray("items");

			if (jsonArray == null)
				return;

			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject jsonDevice = jsonArray.getJSONObject(i);
				// 获取锁数组
				JSONArray locksetJsonArray = jsonDevice.getJSONArray("lockset");

				List<Lockset> deviceLocks = new ArrayList<Lockset>();
				boolean isCheck = false;// true：当前设备下有验电设备;
				int lockid = 0;

				if (locksetJsonArray == null)
					return;

				for (int j = 0; j < locksetJsonArray.length(); j++) {

					// 锁对象
					JSONObject locksetJson = locksetJsonArray.getJSONObject(j);

					int locketID = locksetJson.getInt("id");
					String locketRFID = locksetJson.has("rfid") ? locksetJson
							.getString("rfid") : "";
					String locketType = locksetJson.has("type") ? locksetJson
							.getString("type") : "";

					Lockset lockset = new Lockset();

					lockset.setId(locketID);
					lockset.setRfid(locketRFID);
					lockset.setStationId(1);
					lockset.setLogic(0xFFFF);

					if ("验电锁".equals(locketType)) {
						isCheck = true;
						lockid = locketID;// 记录验电锁ID
						lockset.setType("254");// 254高压带电显，253与高压带电显关联锁,251无源验电锁

					} else if ("无源验电锁".equals(locketType) && !locketRFID.isEmpty()
							&& locketRFID.length() == 10) {
						StringBuffer rfid = new StringBuffer(locketRFID);
						lockset.setRfid(rfid.replace(9, 10, "0").toString());
						isCheck = true;
						lockid = locketID;// 记录验电锁ID
						lockset.setType("251");
					} else {
						lockset.setType("1");
						if ("面板锁".equals(locketType) && !locketRFID.isEmpty()
								&& locketRFID.length() == 10) {
							StringBuffer rfid = new StringBuffer(locketRFID);
							lockset.setRfid(rfid.replace(9, 10, "0").toString());
							lockset.setType("252");
						}
					}

					deviceLocks.add(lockset);

				}

				String name = jsonDevice.getString("name");// 设备名称

				// 设置锁逻辑
//				if (isCheck) {
					for (Lockset lockset : deviceLocks) {
						lockset.setName(name);
							if (isCheck && lockset.getId() != lockid) {// 设备下验电锁
								lockset.setLogic(lockid);
								lockset.setType("253");
							}
						lockDatas.add(lockset);
					}
//				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 把用户信息JSON格式转换成用户信息实体
	 * 
	 * @param value
	 */
	public void JsonToUser(String value) {
		allUser = new ArrayList<User>();
		JSONObject jsonObject;
		JSONArray jsonArrayuser = null;
		try {
			jsonObject = new JSONObject(value);
			jsonArrayuser = jsonObject.getJSONArray("items");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		allUser = JsonTools.getAllUser(jsonArrayuser);
	}

	public List<User> getAllUser() {
		return allUser;
	}

	public List<Lockset> getLockDatas() {
		return lockDatas;
	}

	public void setLockDatas(List<Lockset> lockDatas) {
		this.lockDatas = lockDatas;
	}

}
