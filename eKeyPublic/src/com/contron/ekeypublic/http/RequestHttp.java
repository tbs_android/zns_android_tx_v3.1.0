package com.contron.ekeypublic.http;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.lidroid.xutils.util.LogUtils;

/**
 * 后台服务请求
 * 
 * @author luoyilong
 *
 */
public class RequestHttp {

	Thread thread;
	private String mwld;

	public RequestHttp(String wld) {
		super();
		mwld = wld;
	}

	public interface BitmapCallback {
		void resultValueBitmap(Bitmap bitmap);
	}

	/**
	 * 请求服务后回调接口
	 * 
	 * @author luoyilong
	 *
	 */
	public interface HttpRequestCallBackString {
		void resultValue(String value);
	}

	/**
	 * 请求服务后回调接口
	 * 
	 * @author luoyilong
	 *
	 */
	public interface HttpRequestCallBackImg {
		void resultValue(Bitmap value);
	}

	/**
	 * 请求服务端
	 * 
	 * @param url
	 *            访问端口
	 * @param params
	 *            访问参数
	 * @param requestCallBack
	 *            回调接口
	 */
	public synchronized void doPost(final String url, final JSONObject params,
			final HttpRequestCallBackString requestCallBack) {
		thread = new Thread(new Runnable() {
			@Override
			public void run() {
				DefaultHttpClient client = new DefaultHttpClient();

				// 设置请求超时
				client.getParams().setParameter(
						CoreConnectionPNames.CONNECTION_TIMEOUT, 30 * 1000);

				// // 设置读取超时
				// client.getParams().setParameter(
				// CoreConnectionPNames.SO_TIMEOUT, 30 * 1000);

				HttpPost post = new HttpPost(mwld + url);
				String result = "";
				try {
					LogUtils.e("请求地址：" + mwld + url + "\r\n请求参数：" + params.toString());
					StringEntity s = new StringEntity(params.toString(),
							"UTF-8");
					s.setContentType("application/json");// 发送json数据需要设置contentType
					post.setEntity(s);
					HttpResponse res = client.execute(post);
					if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						result = EntityUtils.toString(res.getEntity());// 返回json格式：
					}

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					LogUtils.e("返回结果：" + result);					
					if (TextUtils.isEmpty(result)) {
						JSONObject json = new JSONObject();
						try {
							json.put("error", "访问服务器失败,请检查网络及服务地址！");
						} catch (JSONException e1) {
							e1.printStackTrace();
						}
						result = json.toString();
					}
					
					if (requestCallBack != null)
						requestCallBack.resultValue(result);
				}

			}
		});
		thread.start();
	}

	public void getImg(final String attchUrl, final HttpRequestCallBackImg requestCallBack) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					String requestUrl = mwld + attchUrl;
					URL url = new URL(requestUrl);
					HttpURLConnection urlConn = (HttpURLConnection) url.openConnection();
					urlConn.setConnectTimeout(5 * 1000);
					urlConn.setReadTimeout(5 * 1000);
					urlConn.setUseCaches(true);
					urlConn.setRequestMethod("GET");
					urlConn.setRequestProperty("Content-Type", "application/json");
					urlConn.addRequestProperty("Connection", "Keep-Alive");
					urlConn.connect();
					if (urlConn.getResponseCode() == 200) {
						requestCallBack.resultValue(BitmapFactory.decodeStream(urlConn.getInputStream()));
					}
					urlConn.disconnect();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();

	}

	/**
	 * 下载图片
	 * 
	 * @param url
	 *            访问端口
	 * @param width
	 *            访问参数
	 * @param height
	 *
	 * @param bitmapCallback
	 *            回调接口
	 */
	@SuppressWarnings("finally")
	public synchronized void downloadImg(final String url, final int width, final int height,
			final BitmapCallback bitmapCallback) {

		thread = new Thread(new Runnable() {
			@Override
			public void run() {

				Bitmap bitmap = null;
				try {

					DefaultHttpClient client = new DefaultHttpClient();

					// 设置请求超时
					client.getParams().setParameter(
							CoreConnectionPNames.CONNECTION_TIMEOUT, 30 * 1000);

					String wldUrl = mwld + url;
					// 新建一个Get方法
					HttpGet get = new HttpGet(wldUrl);
					// 得到网络的回应
					HttpResponse response = client.execute(get);

					// 如果服务器响应的是OK的话！
					if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						InputStream is = response.getEntity().getContent();
						if (is != null) {
							try {
								bitmap = decodeScaleBitmapFromInputStresam(is, width, height);
							} catch (OutOfMemoryError err) {
								err.printStackTrace();
							} finally {
								is.close();
							}
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					if (bitmapCallback != null) {
						bitmapCallback.resultValueBitmap(bitmap);
					}
				}
			}
		});

		thread.start();
	}

	public Bitmap decodeScaleBitmapFromInputStresam(InputStream in, int reqWidth, int reqHeight) {
		final BitmapFactory.Options options = new BitmapFactory.Options();
//		options.inJustDecodeBounds = true;
//		BitmapFactory.decodeStream(in, null, options);
//		// Calculate inSampleSize
//		options.inSampleSize = calculateInSampleSize(options, reqWidth,
//				reqHeight);
		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		options.inPurgeable = true;
		options.inInputShareable = true;
		options.inSampleSize = 4;
		options.inTempStorage = new byte[100 * 1024];
		options.inPreferredConfig = Bitmap.Config.RGB_565;
		Bitmap bitmap = BitmapFactory.decodeStream(in, new Rect(0, 0, 0, 0), options);
		double dou = calculateInScaleSize(options, reqWidth, reqHeight);
//		return scaleBitmap(bitmap, (float) dou);
		return bitmap;
	}

	public int calculateInSampleSize(BitmapFactory.Options options,
											int maxWidth, int maxHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		if (width > maxWidth || height > maxHeight) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) maxHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) maxWidth);
			}
			final float totalPixels = width * height;
			final float maxTotalPixels = maxWidth * maxHeight * 2;
			while (totalPixels / (inSampleSize * inSampleSize) > maxTotalPixels) {
				inSampleSize++;
			}
		}
		return inSampleSize;
	}

	public double calculateInScaleSize(BitmapFactory.Options options,
											  int maxWidth, int maxHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		if (width > maxWidth || height > maxHeight) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) maxHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) maxWidth);
			}
			final float totalPixels = width * height;
			final float maxTotalPixels = maxWidth * maxHeight * 2;
			while (totalPixels / (inSampleSize * inSampleSize) > maxTotalPixels) {
				inSampleSize++;
			}
		} else {
			return Math.min(((double) maxWidth) / width, ((double) maxHeight)
					/ height);
		}
		return inSampleSize;
	}

	public Bitmap scaleBitmap(Bitmap bitmap, float scale) {
		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale); // 长和宽放大缩小的比例
		Bitmap resizeBmp = Bitmap.createBitmap(bitmap, 0, 0,
				bitmap.getWidth(), bitmap.getHeight(), matrix, true);
			/*
			 * if (!bitmap.isRecycled()) { bitmap.recycle(); bitmap = null; }
			 */
		return resizeBmp;
	}

	/**
	 * 上传图片
	 * 
	 * @param url
	 *            上传地址
	 * @param filepath
	 *            图片路径
	 * @return 成功返回true，失败返回false
	 */
	@SuppressWarnings("finally")
	public void uploadImage(final String url, final String filepath,
			final String key) {
	    List<String> filepaths = new ArrayList<>();
	    filepaths.add(filepath);
		uploadImage(url, filepaths, key, null);
	}

	public interface UploadCallback {
		void onUploadResult(boolean isOk);
		void onUploadResultData(JSONArray uploadResultData);
	}

	/**
	 * 上传图片
	 *
	 * @param url
	 *            上传地址
	 * @param filepaths
	 *            图片路径
	 * @return 成功返回true，失败返回false
	 */
	@SuppressWarnings("finally")
	public void uploadImage(final String url, final List<String> filepaths,
							final String key, final UploadCallback uploadCallback) {

		thread = new Thread(new Runnable() {
			@Override
			public void run() {

				DefaultHttpClient client = new DefaultHttpClient();

				// 设置请求超时
				client.getParams().setParameter(
						CoreConnectionPNames.CONNECTION_TIMEOUT, 30 * 1000);

				HttpPost post = new HttpPost(mwld + url);
                MultipartEntity entity = new MultipartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE);
				for (String filepath : filepaths) {
                    File file = new File(filepath);
                    FileBody filebody = new FileBody(file, "binary/octet-stream");
                    entity.addPart(key, filebody);
                }
				post.setEntity(entity);
				if (uploadCallback != null) {
                    try {
                        HttpResponse response = client.execute(post);
                        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                            String result = EntityUtils.toString(response.getEntity());// 返回json格式：
                            String s = new JSONObject(result).getString("s");
                            JSONArray photoPaths = new JSONObject(result).getJSONArray("files");
                            if ("1".equals(s)) {
                                uploadCallback.onUploadResultData(photoPaths);
                                uploadCallback.onUploadResult(true);
                            } else {
                                uploadCallback.onUploadResult(false);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        uploadCallback.onUploadResult(false);
                    }
                } else {
                    try {
                        client.execute(post);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
			}
		});

		thread.start();

	}

	/**
	 * 处理下载图片
	 * 
	 * @param options
	 * @param minSideLength
	 * @param maxNumOfPixels
	 * @return
	 */
	private int computeInitialSampleSize(BitmapFactory.Options options,
			int minSideLength, int maxNumOfPixels) {
		double w = options.outWidth;
		double h = options.outHeight;
		int lowerBound = (maxNumOfPixels == -1) ? 1 : (int) Math.ceil(Math
				.sqrt(w * h / maxNumOfPixels));
		int upperBound = (minSideLength == -1) ? 128 : (int) Math.min(
				Math.floor(w / minSideLength), Math.floor(h / minSideLength));
		if (upperBound < lowerBound) {
			return lowerBound;
		}
		if ((maxNumOfPixels == -1) && (minSideLength == -1)) {
			return 1;
		} else if (minSideLength == -1) {
			return lowerBound;
		} else {
			return upperBound;
		}
	}

	HttpRequestRetryHandler retryHandler = new HttpRequestRetryHandler() {

		@Override
		public boolean retryRequest(IOException arg0, int arg1, HttpContext arg2) {
			// retry a max of 5 times
			if (arg1 >= 3) {
				return false;
			}
			if (arg0 instanceof NoHttpResponseException) {
				return true;
			} else return arg0 instanceof ClientProtocolException;
        }
	};

	private static final String BOUNDARY = "----WebKitFormBoundaryT1HoybnYeFOGFlBR";
	/**
	 *
	 * @param params
	 *            传递的普通参数
	 * @param uploadFile
	 *            需要上传的文件名
	 * @param fileFormName
	 *            需要上传文件表单中的名字
	 * @param newFileName
	 *            上传的文件名称，不填写将为uploadFile的名称
	 * @param urlStr
	 *            上传的服务器的路径
	 * @throws IOException
	 */
	public void uploadForm(Map<String, String> params, String fileFormName,
						   File uploadFile, String newFileName, String urlStr)
			throws IOException {
		if (newFileName == null || newFileName.trim().equals("")) {
			newFileName = uploadFile.getName();
		}

		StringBuilder sb = new StringBuilder();
		/**
		 * 普通的表单数据
		 */
		if (params != null) {
			for (String key : params.keySet()) {
				sb.append("--" + BOUNDARY + "\r\n");
				sb.append("Content-Disposition: form-data; name=\"" + key + "\""
						+ "\r\n");
				sb.append("\r\n");
				sb.append(params.get(key) + "\r\n");
			}
		}
		/**
		 * 上传文件的头
		 */
		sb.append("--" + BOUNDARY + "\r\n");
		sb.append("Content-Disposition: form-data; name=\"" + fileFormName
				+ "\"; filename=\"" + newFileName + "\"" + "\r\n");
		sb.append("Content-Type: image/jpeg" + "\r\n");// 如果服务器端有文件类型的校验，必须明确指定ContentType
		sb.append("\r\n");

		byte[] headerInfo = sb.toString().getBytes("UTF-8");
		byte[] endInfo = ("\r\n--" + BOUNDARY + "--\r\n").getBytes("UTF-8");
		System.out.println(sb.toString());
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type",
				"multipart/form-data; boundary=" + BOUNDARY);
		conn.setRequestProperty("Content-Length", String
				.valueOf(headerInfo.length + uploadFile.length()
						+ endInfo.length));
		conn.setDoOutput(true);

		OutputStream out = conn.getOutputStream();
		InputStream in = new FileInputStream(uploadFile);
		out.write(headerInfo);

		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) != -1)
			out.write(buf, 0, len);

		out.write(endInfo);
		in.close();
		out.close();
		if (conn.getResponseCode() == 200) {
			System.out.println("上传成功");
		} else {
			System.out.println("上传失败");
		}

	}

}
