/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.db;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.inputmethod.CorrectionInfo;

import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.DoorLocker;
import com.contron.ekeypublic.entities.FixedTicketDelay;
import com.contron.ekeypublic.entities.LendKey;
import com.contron.ekeypublic.entities.LockerResult;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.LocksetAll;
import com.contron.ekeypublic.entities.Offline;
import com.contron.ekeypublic.entities.OfflineOperation;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.SectionAll;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.TempDevice;
import com.contron.ekeypublic.entities.TempTask;
import com.contron.ekeypublic.entities.TicketResult;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.UserZone;
import com.contron.ekeypublic.entities.Zone;
import com.contron.ekeypublic.util.DateUtil;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.db.sqlite.SqlInfo;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.util.LogUtils;

public class EkeyDao {
	private DbUtils mDbUtils;

	EkeyDao() {
	}

	EkeyDao(DbUtils dbUtils) {
		this.mDbUtils = dbUtils;
	}

	public DoorLocker findDoorLocker(String address) {
		DoorLocker doorLocker = null;
		String sqldevice = "SELECT D._id, D.stationId, L.lockerId, D.bthAddr,D.deviceName ,L.lockerTypeId,D.deviceNo FROM device D"
				+ " JOIN locker L ON L.deviceNo = D.deviceNo"
				+ " WHERE D.bthAddr = ? ";
		String sqlFixedTicket = "SELECT D._id, FH.ticketId, FH.ticketTypeId, FB.stationId, L.lockerId, D.bthAddr, D.deviceName,L.lockerTypeId FROM device D"
				+ " JOIN locker L ON L.deviceNo = D.deviceNo"
				+ " JOIN fixedTicketBody FB ON FB.deviceNo = D.deviceNo"
				+ " JOIN fixedTicketHead FH ON FH.ticketId = FB.ticketId AND FH.onOff = 1"
				+ " WHERE D.bthAddr = ? GROUP BY D._id";
		String sqlPlanTicket = "SELECT D._id, PH.ticketId, PH.ticketTypeId, PB.stationId, L.lockerId, D.bthAddr, D.deviceName,L.lockerTypeId FROM device D"
				+ " JOIN locker L ON L.deviceNo = D.deviceNo"
				+ " JOIN planTicketBody PB ON PB.deviceNo = D.deviceNo"
				+ " JOIN planTicketHead PH ON PH.ticketId = PB.ticketId"
				+ " WHERE D.bthAddr = ? AND datetime() >= PH.BeginTime AND datetime() <= PH.EndTime GROUP BY D._id";

		try {

			Cursor cursor = mDbUtils.execQuery(new SqlInfo(sqldevice,
					new Object[] { address }));
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					doorLocker = new DoorLocker(cursor.getInt(0),
							cursor.getInt(1), cursor.getInt(2),
							cursor.getString(3), cursor.getString(4),
							cursor.getString(5), cursor.getString(6));

					doorLocker.state = 0;

					// 从固定票查，如果没有去计划票查
					cursor = mDbUtils.execQuery(new SqlInfo(sqlFixedTicket,
							new Object[] { doorLocker.bthAddr }));
					if (cursor != null) {
						if (cursor.moveToFirst()) {
							doorLocker = new DoorLocker(cursor.getInt(0),
									cursor.getString(1), cursor.getInt(2),
									cursor.getInt(3), cursor.getInt(4),
									cursor.getString(5), cursor.getString(6),
									cursor.getString(7));

							doorLocker.state = 1;

						} else {
							cursor.close();
							cursor = mDbUtils.execQuery(new SqlInfo(
									sqlPlanTicket,
									new Object[] { doorLocker.bthAddr }));
							if (cursor != null) {
								if (cursor.moveToFirst())
									doorLocker = new DoorLocker(
											cursor.getInt(0),
											cursor.getString(1),
											cursor.getInt(2), cursor.getInt(3),
											cursor.getInt(4),
											cursor.getString(5),
											cursor.getString(6),
											cursor.getString(7));

								doorLocker.state = 1;
							}
						}

						cursor.close();
					}
				}
				cursor.close();
			}
		} catch (DbException e) {
			e.printStackTrace();
		}
		return doorLocker;
	}

	/**
	 * 获取所有设备信息
	 * 
	 * @return
	 */
	public List<DeviceObject> getDeviceObjectAll() {
		try {
			return mDbUtils.findAll(DeviceObject.class);
		} catch (DbException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取所有用户信息
	 * 
	 * @return
	 */
	public List<User> getAllUser() {
		try {
			return mDbUtils.findAll(User.class);
		} catch (DbException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取所有锁信息
	 * 
	 * @return
	 */
	public List<Lockset> getLocksetAll() {
		try {
			return mDbUtils.findAll(Lockset.class);
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 保存经纬度
	 * 
	 * @return
	 */
	public void saveLockset(Lockset result) {
		try {
			mDbUtils.replace(result);
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 保存开销记录
	 * 
	 * @param lockset
	 * @param user
	 * @param action
	 * @param result
	 * @return
	 */
	public LockerResult saveLockeResult(Lockset lockset, User user,
			String action, String result) {
		String curren = DateUtil.getCurrentDate();
		LockerResult locResul = new LockerResult(lockset.getOid(),
				lockset.getName(), lockset.getId(), lockset.getType(), action,
				user.getName(), " ", curren, result);
		try {
			mDbUtils.save(locResul);
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return locResul;
	}

	/**
	 * 根据离线鉴权延时审批记录的ID查找本地延时记录
	 *
	 * @author guanhaiping
	 * @date
	 * @param replyId
	 * @return 0：未找到；1：找到
	 */
	public int findFixedTicketDelay(int replyId) {
		FixedTicketDelay delay = null;
		try {
			delay = mDbUtils.findFirst(Selector.from(FixedTicketDelay.class)
					.where("ReplyId", "=", replyId));
		} catch (DbException e) {
			e.printStackTrace();
		}

		return delay == null ? 0 : 1;
	}

	/**
	 * 获取最后一次审批通过的延时申请
	 * 
	 * @author guanhaiping
	 * @date
	 * @return
	 */
	@SuppressWarnings("finally")
	public Offline findLastAuditTicketDelay(String username) {
		Offline delay = null;
		try {
			delay = mDbUtils.findFirst(Selector.from(Offline.class).where(
					"apply_username", "=", username));
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return delay;
		}

	}

	/**
	 * 查找指定蓝牙地址
	 * 
	 * @author hupei
	 * @date 2015年9月28日 上午9:40:09
	 * @param address
	 * @return
	 */
	@SuppressWarnings("finally")
	public Smartkey findFirstBtKey(String address) {
		Smartkey btKey = null;
		try {
			btKey = mDbUtils.findFirst(Selector.from(Smartkey.class).where(
					"btaddr", "=", address));
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return btKey;
		}

	}

	/*
	 * 查找指定蓝牙地址的锁
	 * 
	 * @author wangfang
	 * 
	 * @date 2015年12月31日 上午9:40:09
	 * 
	 * @param address
	 * 
	 * @return
	 */
	@SuppressWarnings("finally")
	public Lockset findFirstLockset(String address) {
		Lockset lockSet = null;
		try {
			lockSet = mDbUtils.findFirst(Selector.from(Lockset.class).where(
					"btaddr", "=", address));
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return lockSet;
		}

	}

	/**
	 * 在权限内锁表里查询
	 * 
	 * @param rfid
	 * @return
	 */
	public Lockset findFirstLocksetRfid(String rfid) {
		Lockset lockSet = null;
		try {
			lockSet = mDbUtils.findFirst(Selector.from(Lockset.class).where(
					"rfid", "=", rfid));
		} catch (DbException e) {
			e.printStackTrace();
		}
		return lockSet;
	}

	/**
	 * 在权限内锁表里查询
	 * 
	 * @param rfid
	 * @return
	 */
	public Lockset findFirstLocksetAllRfid(String rfid) {
		Lockset lockSet = null;
		try {
			LocksetAll lockSetAll = mDbUtils.findFirst(Selector.from(
					LocksetAll.class).where("rfid", "=", rfid));

			if (lockSetAll != null) {// lockSetAll不为null时转换成Lockset对象返回
				lockSet = new Lockset();
				lockSet.setId(lockSetAll.getId());
				lockSet.setBtaddr(lockSetAll.getBtaddr());
				lockSet.setBtname(lockSetAll.getBtname());
				lockSet.setName(lockSetAll.getName());
				lockSet.setRfid(lockSetAll.getRfid());
			}

		} catch (DbException e) {
			e.printStackTrace();
		}
		return lockSet;
	}

	/**
	 * 取出采码票内容
	 * 
	 * @author hupei
	 * @date 2015年9月25日 下午2:11:55
	 * @param ticketId
	 * @return
	 */
	public Cursor getCollectTicketBody(String ticketId) {
		// collectCount 采码完成情况： 0=采码完成；大于0就是未采码的数量
		String sql = new StringBuffer(
				"SELECT A.*, sum(CASE WHEN C.lockerRFID ISNULL OR C.lockerRFID ='' THEN 1 ELSE 0 END) collectCount")
				.append(" FROM collectTicketBody A JOIN locker C ON C.lockerId = A.lockerId")
				.append(" WHERE A.ticketId = ? GROUP BY A.deviceNo").toString();
		SqlInfo sqlInfo = new SqlInfo(sql, new Object[] { ticketId });
		try {
			return mDbUtils.execQuery(sqlInfo);
		} catch (DbException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 查询计划审批票内容
	 * 
	 * @author hupei
	 * @date 2015年9月25日 下午2:06:42
	 * @param ticketId
	 * @return
	 */
	public List<TempDevice> getPlanTicketBody(int id) {
		try {
			return mDbUtils.findAll(Selector.from(TempDevice.class).where(
					"tid", "=", id));
		} catch (DbException e) {
			e.printStackTrace();
		}
		return new ArrayList<TempDevice>();
	}

	/**
	 * 保存设备门锁的操作记录
	 * 
	 * @author hupei
	 * @date 2015年9月29日 下午7:11:09
	 * @param doorLocker
	 * @param userId
	 */
	public TicketResult saveTicketResultDoor(DoorLocker doorLocker, int userId) {
		TicketResult ticketResult = new TicketResult(doorLocker.ticketId,
				doorLocker.ticketType, doorLocker.stationId,
				doorLocker.lockerId, userId, 1,
				DateUtil.getCurrentDate(DateUtil.dateFormatYMD),
				DateUtil.getCurrentDate(DateUtil.dateFormatHMS),
				DateUtil.getCurrentDate());
		ticketResult.deviceName = doorLocker.deviceName;
		ticketResult.section = doorLocker.section;
		// try {
		// mDbUtils.save(ticketResult);// 保存
		// } catch (DbException e) {
		// e.printStackTrace();
		// }
		return ticketResult;
	}

	public TicketResult saveTicketResultDoor(DoorLocker doorLocker, int userId,
			int opStype) {
		TicketResult ticketResult = new TicketResult(doorLocker.ticketId,
				doorLocker.ticketType, doorLocker.stationId,
				doorLocker.lockerId, userId, 1,
				DateUtil.getCurrentDate(DateUtil.dateFormatYMD),
				DateUtil.getCurrentDate(DateUtil.dateFormatHMS),
				DateUtil.getCurrentDate(), opStype);
		ticketResult.deviceName = doorLocker.deviceName;
		try {
			mDbUtils.save(ticketResult);// 保存
		} catch (DbException e) {
			e.printStackTrace();
		}
		return ticketResult;
	}

	// /**
	// * 保存RFID操作记录
	// *
	// * @author hupei
	// * @date 2015年9月25日 下午2:05:58
	// * @param userId
	// * @param lockerRFID
	// * @param ticketId
	// * @param ticketTypeId
	// * @param stationId
	// * @return
	// */
	// public TicketResult saveTicketResultRFID(int userId, String lockerRFID,
	// String ticketId, int ticketTypeId, int stationId) {
	// // 查询该RFID是否为当前任务中，
	// // 锁码表中不存在的锁码状态为255，不在当前任务中的锁码状态为3，在任务中的锁码状态为1
	// final StringBuffer sql = new StringBuffer(
	// "SELECT L.lockerId, D.deviceName, (CASE WHEN B._id ISNULL THEN 3 ELSE 1 END) opStatus")
	// .append(" FROM locker L LEFT JOIN ");
	// if (ticketTypeId == 2) {// 根据票的类型，到不同的票内容查找
	// sql.append(TableUtils.getTableName(PlanTicketBody.class));
	// } else if (ticketTypeId == 4) {
	// sql.append(TableUtils.getTableName(FixedTicketBody.class));
	// }
	// sql.append(" B ON B.lockerId = L.lockerId AND B.ticketId = ?")
	// .append(" LEFT JOIN device D ON D.deviceNo = L.deviceNo WHERE L.lockerRFID = ?");
	// SqlInfo sqlInfo = new SqlInfo(sql.toString(), new Object[] { ticketId,
	// lockerRFID });
	// TicketResult ticketResult = new TicketResult(
	// ticketId,
	// // ticketType，计划=2；固定=4
	// ticketTypeId, stationId, lockerRFID, userId,
	// DateUtil.getCurrentDate(DateUtil.dateFormatYMD),
	// DateUtil.getCurrentDate(DateUtil.dateFormatHMS),
	// DateUtil.getCurrentDate());
	// try {
	// final Cursor cursor = mDbUtils.execQuery(sqlInfo);
	// if (cursor != null) {
	// if (cursor.moveToNext()) {
	// ticketResult.lockerId = cursor.getInt(0);
	// ticketResult.deviceName = cursor.getString(1);
	// ticketResult.opStatus = cursor.getInt(2);
	// } else {
	// ticketResult.lockerId = 0;
	// ticketResult.deviceName = "未知设备";
	// ticketResult.opStatus = 255;
	// }
	// mDbUtils.save(ticketResult);// 保存
	// cursor.close();
	// }
	// } catch (DbException e) {
	// e.printStackTrace();
	// }
	// return ticketResult;
	// }

	/**
	 * 取出所有未上传的操作记录
	 * 
	 * @author hupei
	 * @date 2015年9月25日 下午1:34:13
	 * @return
	 */
	public List<TicketResult> getTicketResult() {
		try {
			return mDbUtils.findAll(Selector.from(TicketResult.class).where(
					"isUpload", "=", 0));
		} catch (DbException e) {
			e.printStackTrace();
		}
		return new ArrayList<TicketResult>();
	}

	/**
	 * 更新所有没有上传的操作记录，为上传
	 * 
	 * @author hupei
	 * @date 2015年9月25日 下午1:19:08
	 * @return
	 */
	public void updateTicketResult() {
		try {
			mDbUtils.execNonQuery(new SqlInfo(
					"UPDATE ticketResult SET isUpload=1 WHERE isUpload = 0"));
		} catch (DbException e) {
			e.printStackTrace();
		}
	}

	// /**
	// * 更新单个锁对象
	// *
	// * @author hupei
	// * @date 2015年10月10日 下午2:15:04
	// * @param locker
	// */
	// public void updateLocker(Locker locker) {
	// try {
	// mDbUtils.update(locker, "lockerRFID", "state");
	// } catch (DbException e) {
	// e.printStackTrace();
	// }
	// }

	// /**
	// * 根据设备号取出对应的锁
	// *
	// * @author hupei
	// * @date 2015年10月9日 上午11:17:02
	// * @param deviceNo
	// * @return
	// */
	// public List<Locker> getLockers(String deviceNo) {
	// try {
	// return mDbUtils.findAll(Selector.from(Locker.class).where(
	// "deviceNo", "=", deviceNo));
	// } catch (DbException e) {
	// e.printStackTrace();
	// }
	// return new ArrayList<Locker>();
	// }

	// /**
	// * 取出所有已采码的锁
	// *
	// * @author hupei
	// * @date 2015年9月25日 下午2:18:42
	// * @return
	// */
	// public List<Locker> getLocker() {
	// try {
	// return mDbUtils.findAll(Selector.from(Locker.class).where("state",
	// "=", 1));
	// } catch (DbException e) {
	// e.printStackTrace();
	// }
	// return new ArrayList<Locker>();
	// }

	/**
	 * 更新所有已采码的，为未采码
	 * 
	 * @author hupei
	 * @date 2015年10月10日 下午2:13:44
	 */
	public void updateLocker() {
		try {
			mDbUtils.execNonQuery(new SqlInfo(
					"UPDATE locker SET state = 0 WHERE state = 1"));
		} catch (DbException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据锁的oid查找相同锁数量
	 * 
	 * @param lockId
	 */
	public int selectLockNumber(int lockId) {
		int number = 0;
		Cursor cursor = null;
		try {
			cursor = mDbUtils
					.execQuery("SELECT count(*) AS number from lockset WHERE oid=(SELECT oid FROM lockset WHERE id="
							+ lockId + ")");
			if (cursor != null) {
				cursor.moveToFirst();
				number = cursor.getInt(cursor.getColumnIndex("number"));
				cursor.close();
			}

		} catch (DbException e) {
			e.printStackTrace();
		} finally {

			if (cursor != null)
				cursor.close();
		}

		return number;
	}

	/**
	 * 取出用户可操作区域
	 * 
	 * @author wangfang
	 * @date 2016年1月12日 下午2:18:42
	 * @return
	 */
	public List<UserZone> getUserZone() {
		try {
			return mDbUtils.findAll(Selector.from(UserZone.class));
		} catch (DbException e) {
			e.printStackTrace();
		}
		return new ArrayList<UserZone>();
	}

	/**
	 * 查询组织单位
	 * 
	 * @param type
	 *            过滤条件‘机构’，‘区域’
	 * @return
	 */
	@SuppressWarnings("finally")
	public List<Section> getSection(String type) {
		List<Section> sections = null;

		try {
			sections = mDbUtils.findAll(Selector.from(Section.class).where(
					"type", "=", type));
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return sections;
		}

	}

	/**
	 * 查询组织单位
	 * 
	 * @param type
	 *            过滤条件‘机构’，‘区域’
	 * @return
	 */
	@SuppressWarnings("finally")
	public List<SectionAll> getSectionAll(String type) {
		List<SectionAll> sections = null;

		try {
			sections = mDbUtils.findAll(Selector.from(SectionAll.class).where(
					"type", "=", type));
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return sections;
		}

	}

	/**
	 * 所有区域
	 * 
	 * @return
	 */
	@SuppressWarnings("finally")
	public List<Section> getSection() {
		List<Section> sections = null;
		try {
			sections = mDbUtils.findAll(Selector.from(Section.class).where(
					"type", "!=", "机构"));
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return sections;
		}

	}

	/**
	 * 获取本地钥匙数据
	 * 
	 * @return
	 */
	public List<Smartkey> getSmartkey() {
		try {
			return mDbUtils.findAll(Smartkey.class);
		} catch (DbException e) {
			e.printStackTrace();
		}
		return new ArrayList<Smartkey>();
	}

	/**
	 * 获取本地钥匙数据
	 * 
	 * @return
	 */
	public Smartkey getSmartkey(String address) {
		Smartkey smartkey = null;
		try {
			smartkey = mDbUtils.findFirst(Selector.from(Smartkey.class).where(
					"btaddr", "=", address));
		} catch (DbException e) {
			e.printStackTrace();
		}
		return smartkey;
	}

	/**
	 * 获取本地钥匙数据
	 * 
	 * @return
	 */
	public List<Lockset> getLocksetAll(int oid) {
		List<LocksetAll> locksetAllList = null;

		List<Lockset> locksetList = new ArrayList<Lockset>();

		try {
			locksetAllList = mDbUtils.findAll(Selector.from(LocksetAll.class)
					.where("oid", "=", oid));

			if (null != locksetAllList && locksetAllList.size() > 0) {
				for (LocksetAll locksetAll : locksetAllList) {
					Lockset lockset = new Lockset(locksetAll.getId(),
							locksetAll.getOid(), locksetAll.getName(),
							locksetAll.getType(), locksetAll.getRfid(),
							locksetAll.getBtname(), locksetAll.getBtaddr(),
							true);

					locksetList.add(lockset);
				}
			}
		} catch (DbException e) {
			e.printStackTrace();
		}
		return locksetList;
	}

	/**
	 * 获取本地钥匙借出数据
	 * 
	 * @param userID
	 *            0查询所有借出钥匙记录
	 * @return
	 */
	public List<LendKey> getLendkey(int userID) {
		List<LendKey> lendkey = null;
		try {
			if (userID == 0) {
				lendkey = mDbUtils.findAll(Selector.from(LendKey.class));
			} else {
				lendkey = mDbUtils.findAll(Selector.from(LendKey.class).where(
						"borrowUserId", "=", userID));
			}
		} catch (DbException e) {
			e.printStackTrace();
		}
		return lendkey;
	}

	/**
	 * 获取本地钥匙借出数据
	 * 
	 * @return
	 */
	public List<LendKey> getkeyStateLendkey(int keyState) {
		List<LendKey> lendkey = null;
		try {
			lendkey = mDbUtils.findAll(Selector.from(LendKey.class).where(
					"keyState", "=", keyState));
		} catch (DbException e) {
			e.printStackTrace();
		}
		return lendkey;
	}

	/**
	 * 获取本地钥匙借出数据
	 * 
	 * @param keyState
	 *            记录状态
	 * @param address
	 *            蓝牙地址
	 * @return
	 */
	public LendKey getkeyLendkey(int keyState, String address) {
		LendKey lendkey = null;
		try {
			lendkey = mDbUtils.findFirst(Selector.from(LendKey.class)
					.where("keyState", "=", keyState)
					.and("keyAddr", "=", address));
		} catch (DbException e) {
			e.printStackTrace();
		}
		return lendkey;
	}

	/**
	 * 获取本地钥匙借出数据
	 * 
	 * @return
	 */
	public void saveLendkey(LendKey lendkey) {
		try {
			mDbUtils.replace(lendkey);
		} catch (DbException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 更新本地钥匙借出数据
	 * 
	 * @return
	 */
	public void updateLendkey(LendKey lendkey) {
		try {
			mDbUtils.update(lendkey);
		} catch (DbException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 判断离线任务是否开始
	 * 
	 * @return true 已开始任务
	 */
	public boolean isOffineStart(String apply_username) {
		boolean isoffline = false;
		Offline offline = null;
		try {
			offline = mDbUtils.findFirst(Selector.from(Offline.class).where(
					"apply_username", "=", apply_username).orderBy("id", true));
			List<Offline> list = mDbUtils.findAll(Selector.from(Offline.class));
			if (offline != null) {
				String beg_at = offline.getBeg_at();
				if (!"".equals(beg_at) && null != beg_at)
					isoffline = true;
			}
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return isoffline;
		}

	}

	/**
	 * 查找开锁范围
	 * 
	 * @return
	 */
	@SuppressWarnings("finally")
	public List<Zone> getZone() {
		List<Zone> listzone = null;

		try {
			listzone = mDbUtils.findAll(Zone.class);
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return listzone;
		}
	}

	/**
	 * 列出某类型的常量
	 * 
	 * @return
	 */
	@SuppressWarnings("finally")
	public List<Dict> getDicType(String type) {
		List<Dict> listdict = null;
		try {
			listdict = mDbUtils.findAll(Selector.from(Dict.class).where("type",
					"=", type));
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return listdict;
		}
	}

	/**
	 * 保存临时鉴权任务及其中设备
	 * 
	 * @param listTempTask
	 */
	public void saveTempTaskObject(ArrayList<TempTask> listTempTask) {
		SQLiteDatabase database = mDbUtils.getDatabase();

		if (mDbUtils != null && database != null) {
			database.beginTransaction();
			try {
				// 删除临时鉴权任务表
				if (mDbUtils.tableIsExist(TempTask.class))
					mDbUtils.execNonQuery("Delete from temp_task");

				// 删除临时设备表
				if (mDbUtils.tableIsExist(TempDevice.class))
					mDbUtils.execNonQuery("Delete from temp_object");

				if (listTempTask != null) {

					mDbUtils.replaceAll(listTempTask);// 保存临时鉴权任务到数据库

					List<TempDevice> tempDevice = new ArrayList<TempDevice>();

					for (TempTask tempTask : listTempTask) {
						tempDevice.addAll(tempTask.getTempTaskObjects());
					}
					mDbUtils.replaceAll(tempDevice);// 保存设备到数据库
				}

				database.setTransactionSuccessful();

			} catch (DbException e) {
				e.printStackTrace();
			} finally {
				database.endTransaction();
			}
		}

	}

	/**
	 * 保存设备经纬度
	 */
	public void saveObjectLatLng(int deviceId, double lat, double lng) {

		SqlInfo sql = new SqlInfo(
				"UPDATE deviceObject SET lat=?,lng=? WHERE id=?", new Object[] {
						lat, lng, deviceId });

		LogUtils.i(sql.getSql());
		try {
			mDbUtils.execNonQuery(sql);
		} catch (DbException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取鉴权票中设备
	 * 
	 * @param tempId
	 */
	@SuppressWarnings("finally")
	public ArrayList<DeviceObject> getTempDevice(int tempId) {
		ArrayList<DeviceObject> listDevice = new ArrayList<DeviceObject>();

		SqlInfo sql = new SqlInfo("SELECT D.name,D.lat,D.lng FROM temp_task T "
				+ "LEFT JOIN temp_object O ON O.tid = T.id "
				+ "JOIN deviceObject D ON D.id=O.oid WHERE T.id = ?",
				new Object[] { tempId });
		Cursor cursor = null;
		LogUtils.i(sql.getSql());
		try {
			cursor = mDbUtils.execQuery(sql);

			if (cursor != null) {

				while (cursor.moveToNext()) {
					DeviceObject device = new DeviceObject();
					device.setName(cursor.getString(cursor
							.getColumnIndex("name")));
					device.setLat(cursor.getDouble(cursor.getColumnIndex("lat")));
					device.setLng(cursor.getDouble(cursor.getColumnIndex("lng")));

					listDevice.add(device);
				}
			}

		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
			return listDevice;
		}

	}

	/**
	 * 查找本地库中操作记录[未结束]
	 * 
	 * @param lid
	 *            锁ID
	 * @return OfflineOperation
	 */
	@SuppressWarnings("finally")
	public OfflineOperation findOfflineOperation(int lid) {
		OfflineOperation offlineOperation = null;
		try {
			offlineOperation = mDbUtils.findFirst(Selector
					.from(OfflineOperation.class).where("lid", "=", lid)
					.and("endSign", "=", 0));// 0：未结束，1结束
		} catch (DbException e) {
			e.printStackTrace();
		} finally {
			return offlineOperation;
		}

	}
}
