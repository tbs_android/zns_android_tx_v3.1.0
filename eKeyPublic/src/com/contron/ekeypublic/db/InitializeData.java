package com.contron.ekeypublic.db;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.LocksetAll;
import com.contron.ekeypublic.entities.Offline;
import com.contron.ekeypublic.entities.OfflineHistory;
import com.contron.ekeypublic.entities.OfflineOperation;
import com.contron.ekeypublic.entities.Role;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.SectionAll;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.UserZone;
import com.contron.ekeypublic.entities.Zone;
import com.contron.ekeypublic.http.JsonTools;
import com.contron.ekeypublic.http.RequestHttp;
import com.contron.ekeypublic.http.RequestParams;
import com.contron.ekeypublic.http.RequestHttp.HttpRequestCallBackString;
import com.contron.ekeypublic.util.ConfigUtil;
import com.contron.ekeypublic.util.DateUtil;
import com.contron.ekeypublic.view.MsgBoxFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.util.LogUtils;

public class InitializeData {

	// 实例化服务访问实例
	private RequestHttp requestHttp;// 请求类
	private User muser;
	private DbUtils dbUtils = null;
	private SQLiteDatabase database = null;
	private Activity mActivity;
	private ProgressDialog progressDialog;
	private boolean dialogState = true;// true显示对话框，false不显示

	public InitializeData(Activity activity, User user, String wld,
			boolean dialogState) {
		super();
		this.mActivity = activity;
		this.dialogState = dialogState;
		muser = user;
		requestHttp = new RequestHttp(wld);// 服务请求类
		// 获取数据库对象
		dbUtils = DBHelp.getInstance(mActivity).getDb();
		database = dbUtils.getDatabase();

	}

	/**
	 * 清空表数据
	 */
	private void clear() {
		if (dbUtils != null && database != null) {

			database.beginTransaction();
			try {

				// 删除智能钥匙表
				if (dbUtils.tableIsExist(Smartkey.class))
					dbUtils.execNonQuery("Delete from smartkey");

				if (dbUtils.tableIsExist(Lockset.class))// 判断表存在则删除
					dbUtils.execNonQuery("Delete from Lockset");

				if (dbUtils.tableIsExist(LocksetAll.class))// 判断表存在则删除
					dbUtils.execNonQuery("Delete from LocksetAll");

				if (dbUtils.tableIsExist(Section.class))
					dbUtils.execNonQuery("Delete from section");

				if (dbUtils.tableIsExist(DeviceObject.class))
					dbUtils.execNonQuery("Delete from deviceObject");

				if (dbUtils.tableIsExist(UserZone.class))
					dbUtils.execNonQuery("Delete from userZone");

				if (dbUtils.tableIsExist(Role.class))
					dbUtils.execNonQuery("Delete from role");

				if (dbUtils.tableIsExist(Zone.class))
					dbUtils.execNonQuery("Delete from zone");

				if (dbUtils.tableIsExist(Dict.class))
					dbUtils.execNonQuery("Delete from dict");

				if (dbUtils.tableIsExist(User.class))
					dbUtils.execNonQuery("Delete from User");
				
				if (dbUtils.tableIsExist(SectionAll.class))
					dbUtils.execNonQuery("Delete from section_all");

				database.setTransactionSuccessful();

			} catch (DbException e) {
				e.printStackTrace();
			} finally {
				database.endTransaction();
			}
		}
	}

	/**
	 * 初始化基础数据
	 */
	public void InitData() {

		if (muser == null || !muser.isOnline) {// 离线模式直接返回
			return;
		}
		clear();// 清空数据表
		/**
		 * 获取智能钥匙信息
		 */
		getKeys();// 获取智能钥匙信息

	}

	/**
	 * 上传离线开锁数据，下载离线鉴权
	 */
	public void Upload() {

		if (muser == null || !muser.isOnline)// 离线模式直接返回
		{
			return;
		}

		UploadOfflineOperationResult();// 上传离线开门操作记录

	}

	/**
	 * 获取智能钥匙信息
	 */
	private void getKeys() {

		startDialog();// 弹出进度条对话框

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("page", -1);

		int sid = muser.getSid();
		// 请求接口
		String url = String.format(Globals.SECTION_SID_SMARTKEY, sid);
		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {
			@Override
			public void resultValue(String value) {

				if (value.contains("success")) {// 返回成功

					List<Smartkey> keys = JsonTools.getSectionKeys(value);
					if (keys != null && keys.size() > 0) {
						try {
							database.beginTransaction();
							dbUtils.replaceAll(keys);
							database.setTransactionSuccessful();
						} catch (DbException e) {
							e.printStackTrace();
						} finally {
							database.endTransaction();
						}

					}
					LogUtils.i("获取智能钥匙信息成功！");

					downloadLockset();// 下载自己相关权限锁信息

				} else {// 返回失败
					LogUtils.i("获取智能钥匙信息失败！");
					dismissDialog();// 结束对话框
					dipMsgBox("更新到智能钥匙信息失敗,请重初始化基础数据！");

				}

			}
		});

	}

	/**
	 * 下载自己相关权限锁信息
	 */
	private void downloadLockset() {

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("page", -1);

		String username = muser.getUsername();
		String url = String.format(Globals.USER_NAME_OBJECT, username);

		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {

			@Override
			public void resultValue(String value) {

				if (value.contains("success")) {// 返回成功

					JSONObject jsonObject;
					JSONArray jsonArrayArea = null;
					try {
						jsonObject = new JSONObject(value);
						jsonArrayArea = jsonObject.getJSONArray("items");// 取出items数组
					} catch (JSONException e1) {
						e1.printStackTrace();
					}

					List<Lockset> doorLockers = JsonTools
							.getAllDoorlocker(jsonArrayArea);
					if (doorLockers != null && doorLockers.size() > 0) {
						try {
							database.beginTransaction();
							dbUtils.replaceAll(doorLockers);// 保存到数据库
							database.setTransactionSuccessful();
						} catch (DbException e) {
							e.printStackTrace();
						} finally {
							database.endTransaction();
						}
					}
					LogUtils.i("下载相关权限锁信息成功");
					downloadLocksetAll();// 下载所有锁信息
				} else {// 返回失败
					LogUtils.i("下载相关权限锁信息失败");
					dismissDialog();// 结束对话框
					dipMsgBox("更新到当前用户权限内锁信息失败,请重初始化基础数据！");
				}

			}
		});

	}

	/**
	 * 下载所有设备、锁信息表到本地
	 */
	private void downloadLocksetAll() {

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("page", -1);

		requestHttp.doPost(Globals.OBJECT, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

						if (value.contains("success")) {// 返回成功

							try {
								ArrayList<DeviceObject> object;
								JSONObject jsonObject = new JSONObject(value);
								JSONArray jsonArray = jsonObject
										.getJSONArray("items");
								// 获取所有设备
								object = new Gson().fromJson(
										jsonArray.toString(),
										new TypeToken<List<DeviceObject>>() {
										}.getType());

								// 获取所有锁
								List<LocksetAll> locksetAll = new ArrayList<LocksetAll>();
								locksetAll = JsonTools.getLocksetAll(jsonArray);// json转换成实体

								// 保存所有所有设备
								database.beginTransaction();
								if (null != object && object.size() > 0) {
									dbUtils.replaceAll(object);
								}

								// 保存所有锁信息到表locksetAll
								if (null != locksetAll && locksetAll.size() > 0) {
									dbUtils.replaceAll(locksetAll);
								}

								database.setTransactionSuccessful();
							} catch (DbException e) {
								e.printStackTrace();
							} catch (JSONException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} finally {
								database.endTransaction();
							}

							LogUtils.i("下载所有锁信息成功 ");

							downloadSection();// 下载机构信息
						} else {// 返回失败
							LogUtils.i("下载所有锁信息失败");
							dismissDialog();// 结束对话框
							dipMsgBox("更新到当所有锁锁信息失败,请重初始化基础数据！");
						}

					}
				});

	}

	/**
	 * 获取机构、区域、站点、信息
	 */
	private void downloadSection() {

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("page", -1);
		requestHttp.doPost(Globals.SECTION_ALL, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

						if (value.contains("success")) {
							List<Section> Sectionlist = new ArrayList<Section>();
							try {
								JSONObject jsonObject = new JSONObject(value);
								JSONArray jsonArraySections = jsonObject
										.getJSONArray("items");
								Sectionlist = new Gson().fromJson(
										jsonArraySections.toString(),
										new TypeToken<List<Section>>() {
										}.getType());
							} catch (JSONException e) {
								e.printStackTrace();
							}

							if (Sectionlist != null && Sectionlist.size() > 0) {
								for (Section session : Sectionlist) {// 去掉根节点
									if ("根区域".equals(session.getName())) {
										Sectionlist.remove(session);
										break;
									}
								}

								try {
									database.beginTransaction();
									dbUtils.replaceAll(Sectionlist);
									database.setTransactionSuccessful();
								} catch (DbException e) {
									e.printStackTrace();
								} finally {
									database.endTransaction();
								}
							}
							LogUtils.i("下载个人机构信息成功");
							downloadSectionAll();// 下载区域信息

						} else {
							LogUtils.i("下载个人机构信息失败");
							dismissDialog();// 结束对话框
							dipMsgBox("更新到个人机构信息失败,请重初始化基础数据！");
						}

					}
				});

	}

	/**
	 * 获取机构、区域、站点、信息
	 */
	private void downloadSectionAll() {

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("page", -1);
		requestHttp.doPost(Globals.SECTION_EX_ALL, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

						if (value.contains("success")) {
							List<SectionAll> Sectionlist = new ArrayList<SectionAll>();
							try {
								JSONObject jsonObject = new JSONObject(value);
								JSONArray jsonArraySections = jsonObject
										.getJSONArray("items");
								Sectionlist = new Gson().fromJson(
										jsonArraySections.toString(),
										new TypeToken<List<SectionAll>>() {
										}.getType());
							} catch (JSONException e) {
								e.printStackTrace();
							}

							if (Sectionlist != null && Sectionlist.size() > 0) {
								for (SectionAll session : Sectionlist) {// 去掉根节点
									if ("根区域".equals(session.getName())) {
										Sectionlist.remove(session);
										break;
									}
								}

								try {
									database.beginTransaction();
									dbUtils.replaceAll(Sectionlist);
									database.setTransactionSuccessful();
								} catch (DbException e) {
									e.printStackTrace();
								} finally {
									database.endTransaction();
								}
							}
							LogUtils.i("下载所有机构信息成功");
							getUserZone();// 下载区域信息

						} else {
							LogUtils.i("下载机构信息失败");
							dismissDialog();// 结束对话框
							dipMsgBox("更新到所有机构信息失败,请重初始化基础数据！");
						}

					}
				});

	}

	/**
	 * 获取授权范围
	 */
	private void getUserZone() {

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("page", -1);
		String url = String.format(Globals.USER_NAME_ZONE, muser.getUsername());

		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {
			@Override
			public void resultValue(String value) {

				if (value.contains("success")) {// 返回成功
					List<UserZone> userZones = JsonTools.getUserZone(value);
					if (userZones != null && userZones.size() > 0) {
						try {
							database.beginTransaction();
							dbUtils.replaceAll(userZones);// 保存到数据库
							database.setTransactionSuccessful();
						} catch (DbException e) {
							e.printStackTrace();
						} finally {
							database.endTransaction();
						}
					}
					LogUtils.i("下载区域信息成功 ");
					getAllUser();// 下载用户角色
				} else {// 返回失败
					LogUtils.i("下载区域信息失败");
					dismissDialog();// 结束对话框
					dipMsgBox("更新到区域信息失败,请重初始化基础数据！");
				}

			}
		});

	}

	/**
	 * 获取所有用户信息
	 */
	private void getAllUser() {

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("page", -1);
		prams.putParams("search", "启用");

		requestHttp.doPost(Globals.All_USER, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

						if (value.contains("success")) {// 返回成功

							JSONObject jsonObject;
							JSONArray jsonArrayuser = null;
							try {
								jsonObject = new JSONObject(value);
								jsonArrayuser = jsonObject
										.getJSONArray("items");
							} catch (JSONException e1) {
								e1.printStackTrace();
							}

							List<User> allUsers = JsonTools
									.getAllUser(jsonArrayuser);
							if (allUsers != null && allUsers.size() > 0) {
								try {
									database.beginTransaction();
									dbUtils.replaceAll(allUsers);// 保存到数据库
									database.setTransactionSuccessful();
								} catch (DbException e) {
									e.printStackTrace();
								} finally {
									database.endTransaction();
								}
							}
							LogUtils.i("下载所有用户信息成功 ");
							getRoles();// 下载用户角色
						} else {// 返回失败
							LogUtils.i("下载所有用户信息失败");
							dismissDialog();// 结束对话框
							dipMsgBox("更新到所有用户信息失败,请重初始化基础数据！");
						}

					}
				});

	}

	/**
	 * 获取用户角色
	 */
	private void getRoles() {
		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("page", -1);
		requestHttp.doPost(Globals.ROLE, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

						if (value.contains("success")) {// 返回成功

							JSONObject jsonObject;
							JSONArray jsonArrayRoles = null;
							try {
								jsonObject = new JSONObject(value);
								jsonArrayRoles = jsonObject
										.getJSONArray("items");
							} catch (JSONException e1) {
								e1.printStackTrace();
							}

							List<Role> role = JsonTools
									.getRoles(jsonArrayRoles);

							if (role != null && role.size() > 0) {
								try {
									database.beginTransaction();
									dbUtils.replaceAll(role);// 保存到数据库
									database.setTransactionSuccessful();
								} catch (DbException e) {
									e.printStackTrace();
								} finally {
									database.endTransaction();
								}
							}
							LogUtils.i("下载用户角色成功 ");
							getZone();

						} else {// 返回失败
							LogUtils.i("下载用户角色失败 ");
							dismissDialog();// 结束对话框
							dipMsgBox("更新到用户角色信息失败,请重初始化基础数据！");
						}

					}
				});

	}

	/**
	 * 获取开锁范围
	 */
	private void getZone() {
		RequestParams params = new RequestParams(muser);
		params.putParams("page", -1);
		String url = String.format(Globals.ZONE_SID, -1);// -1表示所有开锁范围
		requestHttp.doPost(url, params, new HttpRequestCallBackString() {
			@Override
			public void resultValue(final String value) {
				if (value.contains("success")) {// 返回成功

					JSONObject jsonObject;
					JSONArray jsonArrayDicts = null;
					try {
						jsonObject = new JSONObject(value);
						jsonArrayDicts = jsonObject.getJSONArray("items");
					} catch (JSONException e1) {
						e1.printStackTrace();
					}

					List<Zone> mScope = JsonTools.getZone(jsonArrayDicts);

					if (mScope != null && mScope.size() > 0) {
						try {
							database.beginTransaction();
							dbUtils.replaceAll(mScope);// 保存到数据库
							database.setTransactionSuccessful();
						} catch (DbException e) {
							e.printStackTrace();
						} finally {
							database.endTransaction();
						}

					}
					LogUtils.i("下载用开锁范围成功 ");
					getDicts();// 列出所有常用类型
				} else {// 返回失败
					LogUtils.i("下载开锁范围失败 ");
					dismissDialog();// 结束对话框
					dipMsgBox("更新到开锁范围信息失败,请重初始化基础数据！");
				}

			}
		});
	}

	/**
	 * 列出所有的常量类型
	 */
	public void getDicts() {

		RequestParams params = new RequestParams(muser);
		params.putParams("page", -1);
		String url = String.format(Globals.DICT);
		requestHttp.doPost(url, params, new HttpRequestCallBackString() {
			@Override
			public void resultValue(final String value) {

				dismissDialog();// 结束对话框

				if (value.contains("success")) {// 返回成功

					JSONObject jsonObject;
					JSONArray jsonArrayDicts = null;
					try {
						jsonObject = new JSONObject(value);
						jsonArrayDicts = jsonObject.getJSONArray("items");
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					List<Dict> listdic = JsonTools.getDict(jsonArrayDicts);

					if (listdic != null && listdic.size() > 0) {
						try {
							database.beginTransaction();
							dbUtils.execNonQuery("Delete from dict");
							dbUtils.replaceAll(listdic);// 保存到数据库
							database.setTransactionSuccessful();
						} catch (DbException e) {
							e.printStackTrace();
						} finally {
							database.endTransaction();
						}

					}

					savUpdatetime(value);// 保存更新时间

					LogUtils.i("列出所有的常量类型成功 ");
					dipMsgBox("基础数据更新成功！");
				} else {// 返回失败
					LogUtils.i("列出所有的常量类型失败 ");
					dipMsgBox("更新到列出所有的常量类型信息失败,请重初始化基础数据！");
				}

			}
		});
	}

	/**
	 * 离线鉴权是否下载进行判断
	 */
	private void getOffline() {

		Offline delay = DBHelp.getInstance(mActivity).getEkeyDao()
				.findLastAuditTicketDelay(String.valueOf(muser.getUsername()));

		// 上传数据库存在离线鉴权并且开始执行过
		if (delay != null && !TextUtils.isEmpty(delay.getBeg_at())) {
			// 判断离线鉴权是否过期
			boolean isoverdue = DateUtil.judgedate(delay.getBeg_at(),
					delay.getDuration());

			if (isoverdue) {// 离线鉴权过期
				updateoffline_action_at(delay);// 更新离线鉴权状态
			}
		} else {
			getoffling();// 下载离线鉴权数据
		}

	}

	/**
	 * 下载用户己批准的 离线鉴权
	 * 
	 */
	private void getoffling() {

		// 设置参数
		RequestParams prams = new RequestParams(muser);

		// 请求接口
		String url = String.format(Globals.USER_NAME_OFFLINE,
				muser.getUsername());

		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {

			@Override
			public void resultValue(final String value) {
				if (value.contains("success")) {
					Offline offline = JsonTools.getoffline(value);

					try {
						database.beginTransaction();
						if (dbUtils.tableIsExist(Offline.class))// 删除本地数据库中当前用户的离线鉴权数据
							dbUtils.execNonQuery("Delete from offline where apply_username="
									+ muser.getUsername());
						if (offline != null && offline.getId() != 0)
							dbUtils.replace(offline);// 保存到数据库
						database.setTransactionSuccessful();
						LogUtils.i("下载离线鉴权成功 ");
					} catch (DbException e) {
						e.printStackTrace();
					} finally {
						database.endTransaction();
					}
				} else {
					LogUtils.i("下载离线鉴权失败 ");
				}

			}
		});

	}

	/**
	 * 根据当前时间更新基础数据
	 * 
	 */
	public void autoUpdateDict(String updatetime) {

		getKeys();

		startDialog();// 弹出进度条对话框

		try {
			if (dbUtils.tableIsExist(Lockset.class))// 判断表存在则删除
				dbUtils.execNonQuery("Delete from Lockset");
		} catch (DbException e1) {

			e1.printStackTrace();
		}

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("updatetime", updatetime);

		requestHttp.doPost(Globals.DICT_AUTO_UPDATE_DICT, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {

						dismissDialog();// 结束对话框

						if (value.contains("success")) {
							savUpdatetime(value);

							Gson gson = new Gson();

							try {
								JSONObject jsonObject = new JSONObject(value);

								// 钥匙更新
								// JSONArray smartkeyJson = jsonObject
								// .getJSONArray("smartkeys");
								// List<Smartkey> smartkeys = gson.fromJson(
								// smartkeyJson.toString(),
								// new TypeToken<List<Smartkey>>() {
								// }.getType());

								// 权限内锁更新
								JSONArray objectJson = jsonObject
										.getJSONArray("objects");
								List<Lockset> doorLockers = JsonTools
										.getAllDoorlocker(objectJson);

								// 所有设备及锁
								// JSONArray allObjectsJson = jsonObject
								// .getJSONArray("allObjects");
								// // 获取所有设备
								// List<DeviceObject> object = new
								// Gson().fromJson(
								// allObjectsJson.toString(),
								// new TypeToken<List<DeviceObject>>() {
								// }.getType());

								// 获取所有锁
								// List<LocksetAll> locksetAll = new
								// ArrayList<LocksetAll>();
								// locksetAll = JsonTools
								// .getLocksetAll(allObjectsJson);// json转换成实体

								// 获取区域
								// JSONArray jsonArraySections = jsonObject
								// .getJSONArray("sections");
								// List<Section> Sectionlist = new Gson()
								// .fromJson(jsonArraySections.toString(),
								// new TypeToken<List<Section>>() {
								// }.getType());

								// 获取用户
								// JSONArray jsonArrayuser = jsonObject
								// .getJSONArray("users");
								// List<User> allUsers = JsonTools
								// .getAllUser(jsonArrayuser);

								// 获取用户角色
								// JSONArray jsonArrayRoles = jsonObject
								// .getJSONArray("roles");
								// List<Role> role = JsonTools
								// .getRoles(jsonArrayRoles);

								// 获取开锁范围
								// JSONArray zoneJson = jsonObject
								// .getJSONArray("zones");
								// List<Zone> zones =
								// JsonTools.getZone(zoneJson);

								// 列出常量
								// JSONArray jsonArrayDicts = jsonObject
								// .getJSONArray("dicts");
								// List<Dict> listdic = JsonTools
								// .getDict(jsonArrayDicts);

								try {
									database.beginTransaction();
									// if (null != smartkeys
									// && smartkeys.size() > 0)
									// dbUtils.replaceAll(smartkeys);// 更新钥匙到数据库

									if (null != doorLockers
											&& doorLockers.size() > 0)
										dbUtils.replaceAll(doorLockers);// 更新权限内锁到数据库

									// if (null != object && object.size() > 0)
									// dbUtils.replaceAll(object);// 更新所有设备到数据库
									//
									// if (null != locksetAll
									// && locksetAll.size() > 0)
									// dbUtils.replaceAll(locksetAll);//
									// 更新所有锁到数据库

									// if (null != Sectionlist
									// && Sectionlist.size() > 0)
									// dbUtils.replaceAll(Sectionlist);//
									// 更新机构到数据库

									// if (null != allUsers && allUsers.size() >
									// 0)
									// dbUtils.replaceAll(allUsers);// 更新用户到数据库

									// if (null != role && role.size() > 0)
									// dbUtils.replaceAll(role);// 更新用户角色到数据库

									// if (null != zones && zones.size() > 0)
									// dbUtils.replaceAll(zones);// 更新开锁范围到数据库

									// if (null != listdic && listdic.size() >
									// 0)
									// dbUtils.replaceAll(listdic);// 更新常量到数据库

									database.setTransactionSuccessful();

									// dipMsgBox("基础数据更新成功！");
								} catch (DbException e) {
									e.printStackTrace();
								} finally {
									database.endTransaction();
								}

							} catch (JSONException e) {
								e.printStackTrace();
							}
						} else {
							LogUtils.i("更新基础数据信息失败");
							dipMsgBox("更新基础数据信息失败,请重初始化基础数据！");
						}
					}
				});

	}

	/**
	 * 保存更新时间
	 * 
	 * @param value
	 */
	private void savUpdatetime(String value) {
		try {
			JSONObject json = new JSONObject(value);
			String updatetime = json.has("updatetime") ? json
					.getString("updatetime") : "";

			if (!"".equals(updatetime)) {
				ConfigUtil.getInstance(mActivity).setString("updatetime",
						updatetime);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 提交离线鉴权的执行时间
	 * 
	 * @param offline
	 */
	private void updateoffline_action_at(Offline offline) {

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		// 请求接口
		String url = String.format(Globals.OFFLINE_ID_ACTION_AT_DATE, offline
				.getId(), DateUtil.getStringByFormat(offline.getBeg_at(),
				"yyyyMMddHHmmss"));

		requestHttp.doPost(url, prams, new HttpRequestCallBackString() {
			@Override
			public void resultValue(String value) {
				if (value.contains("success")) {
					getoffling();// 下载离线鉴权数据
				}
			}
		});

	}

	/**
	 * 上传离线智能钥匙操作记录
	 */
	private void UploadOflineHistoryResult() {

		List<OfflineHistory> offline = null;

		try {
			if (!dbUtils.tableIsExist(OfflineHistory.class))// 表不存在直接返回
			{
				getOffline();// 下载离线鉴权
				return;
			}

			offline = dbUtils.findAll(OfflineHistory.class);// 获取数据

			if (offline == null || offline.size() == 0)// 表没数据直接返回
			{
				getOffline();// 下载离线鉴权
				return;
			}

		} catch (DbException e) {
			e.printStackTrace();
		}

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("history",
				JsonTools.getJsonArrayOfflineHistory(offline));

		// 请求接口
		requestHttp.doPost(Globals.HISTORY_OFFLINE, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {
						if (value.contains("success")) {// 返回成功
							try {
								if (dbUtils.tableIsExist(OfflineHistory.class))
									dbUtils.execNonQuery("Delete from oflinehistory");
							} catch (DbException e) {
								e.printStackTrace();
							}
						}
						getOffline();// 下载离线鉴权
					}
				});

	}

	/**
	 * 上传离线蓝牙开锁操作记录
	 */
	private void UploadOfflineOperationResult() {

		List<OfflineOperation> offline = null;
		try {
			if (!dbUtils.tableIsExist(OfflineOperation.class))// 表不存在直接返回
			{
				UploadOflineHistoryResult();// 上传离线智能钥匙开锁操作记录
				return;
			}

			offline = dbUtils.findAll(OfflineOperation.class);// 获取数据

			if (offline == null || offline.size() == 0)// 没数据直接返回
			{
				UploadOflineHistoryResult();// 上传离线智能钥匙开锁操作记录
				return;

			}

		} catch (DbException e) {
			e.printStackTrace();
		}

		// 设置参数
		RequestParams prams = new RequestParams(muser);
		prams.putParams("operation",
				JsonTools.getJsonArrayOfflineOperation(offline));

		requestHttp.doPost(Globals.OPERATION_OFFLINE, prams,
				new HttpRequestCallBackString() {
					@Override
					public void resultValue(String value) {
						if (value.contains("success")) {// 返回成功
							try {
								if (dbUtils
										.tableIsExist(OfflineOperation.class))
									dbUtils.execNonQuery("Delete from offlineoperation");
							} catch (DbException e) {
								e.printStackTrace();
							}
						}

						UploadOflineHistoryResult();// 上传离线智能钥匙操作记录
					}
				});

	}

	/**
	 * 显示对话框
	 */
	private void startDialog() {

		if (!dialogState)
			return;

		if (progressDialog == null)
			progressDialog = showSpinnerDialog(mActivity, "提示",
					"基础数据更新中...请稍候！", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();

						}
					});
		else {
			progressDialog.dismiss();
			progressDialog.show();
		}
	}

	/**
	 * 关闭对话框
	 */
	private void dismissDialog() {
		if (progressDialog != null)
			progressDialog.dismiss();
	}

	/**
	 * 提示框
	 * 
	 * @param msg
	 */
	private void dipMsgBox(String msg) {

		if (!dialogState)
			return;

//		mActivity.showToast(msg);

//		showMsgBox("提示", msg, new OnClickListener() {
//
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();
//			}
//		});
	}

	/**
	 * 圆形进度对话框
	 * 
	 * @param context
	 *            上下文
	 * @param tip
	 *            显示内容
	 * */
	@SuppressWarnings("deprecation")
	public ProgressDialog showSpinnerDialog(final Context context,
			String title, String tip,
			DialogInterface.OnClickListener clickListener) {
		final ProgressDialog progressDialog = new ProgressDialog(context);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		if (!TextUtils.isEmpty(title))
			progressDialog.setTitle(title);
		progressDialog.setMessage(tip);
		progressDialog.setCancelable(false);

		if (clickListener != null)
			progressDialog.setButton("取消", clickListener);

		progressDialog.show();
		return progressDialog;
	}

	public void showMsgBox(String title, String message,
			DialogInterface.OnClickListener okListener) {
		MsgBoxFragment boxFragment = new MsgBoxFragment();
		boxFragment.setTitle(title);
		boxFragment.setMessage(message);
		boxFragment.setPositiveOnClickListener(okListener);
		boxFragment.setNegativeOnClickListener(null);
		boxFragment.setCanceledOnTouchOutside(false);
		boxFragment.show(mActivity.getFragmentManager(), "msgBox");

	}

}
