/*
 * Copyright (c) 2014. hupei (hupei132@qq.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.contron.ekeypublic.db;


import android.content.Context;
import android.database.Cursor;

import com.contron.ekeypublic.common.Globals;
import com.contron.ekeypublic.entities.DeviceObject;
import com.contron.ekeypublic.entities.Dict;
import com.contron.ekeypublic.entities.LendKey;
import com.contron.ekeypublic.entities.Lockset;
import com.contron.ekeypublic.entities.LocksetAll;
import com.contron.ekeypublic.entities.Offline;
import com.contron.ekeypublic.entities.OfflineOperation;
import com.contron.ekeypublic.entities.Role;
import com.contron.ekeypublic.entities.Section;
import com.contron.ekeypublic.entities.SectionAll;
import com.contron.ekeypublic.entities.Smartkey;
import com.contron.ekeypublic.entities.User;
import com.contron.ekeypublic.entities.UserZone;
import com.contron.ekeypublic.util.VersionUtil;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.db.sqlite.SqlInfo;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.util.LogUtils;

public class DBHelp {
	private static DBHelp mDBHelp;
	private DbUtils mDbUtils;
	private EkeyDao mEkeyDao;
	private int dbVersion = 1;
	private boolean dbVersionStatus = false;

	public static DBHelp getInstance(Context context) {
		if (mDBHelp == null) {
			synchronized (DBHelp.class) {
				if (mDBHelp == null) {
					mDBHelp = new DBHelp(context);
				}
			}
		}
		return mDBHelp;
	}

	public DBHelp(Context context) {
		createData(context);
		mEkeyDao = new EkeyDao(mDbUtils);
	}

	/**
	 * 创建数据库
	 */
	private void createData(Context context) {

		dbVersion = VersionUtil.getVersionCode(context);

//		 mDbUtils = DbUtils.create(context.getApplicationContext(),
//		 Globals.DB_DIR, Globals.DATABASE_NAME);// 调试
//		 mDbUtils = DbUtils.create(context.getApplicationContext(),
//		 Globals.DATABASE_NAME);

		mDbUtils = DbUtils.create(context.getApplicationContext(),
				Globals.DB_DIR, Globals.DATABASE_NAME, dbVersion,
				new DbUtils.DbUpgradeListener() {

					@Override
					public void onUpgrade(DbUtils arg0, int arg1, int arg2) {
						LogUtils.e("数据库升级");
						dbVersionStatus=true;					
					}
				});

	
		
		// 创建表
		try {
			
			if(dbVersionStatus){// 删除建表
				mDbUtils.dropTable(LendKey.class);// 借钥匙记录
				mDbUtils.dropTable(Smartkey.class);// 钥匙
				mDbUtils.dropTable(Lockset.class);// 权限内锁
				mDbUtils.dropTable(LocksetAll.class);// 所有锁
				mDbUtils.dropTable(DeviceObject.class);// 设备
				mDbUtils.dropTable(Section.class);// 机构
				mDbUtils.dropTable(UserZone.class);// 获取授权范围
				mDbUtils.dropTable(Role.class);// 用户角色
				mDbUtils.dropTable(Offline.class);// 离线鉴权
				mDbUtils.dropTable(User.class);// 所用用户
				mDbUtils.dropTable(OfflineOperation.class); // 离线蓝牙开锁操作记录
				mDbUtils.dropTable(Dict.class); // 字典
			}
			
			
			mDbUtils.createTableIfNotExist(LendKey.class);// 借钥匙记录
			mDbUtils.createTableIfNotExist(Smartkey.class);// 钥匙
			mDbUtils.createTableIfNotExist(Lockset.class);// 权限内锁
			mDbUtils.createTableIfNotExist(LocksetAll.class);// 所有锁
			mDbUtils.createTableIfNotExist(DeviceObject.class);// 设备
			mDbUtils.createTableIfNotExist(Section.class);// 机构
			mDbUtils.createTableIfNotExist(UserZone.class);// 获取授权范围
			mDbUtils.createTableIfNotExist(Role.class);// 用户角色
			mDbUtils.createTableIfNotExist(Offline.class);// 离线鉴权
			mDbUtils.createTableIfNotExist(User.class);// 所用用户
			mDbUtils.createTableIfNotExist(OfflineOperation.class);// 离线蓝牙开锁操作记录
			mDbUtils.createTableIfNotExist(SectionAll.class);// 离线蓝牙开锁操作记录
			mDbUtils.createTableIfNotExist(Dict.class);// 字典
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public DbUtils getDb() {
		return mDbUtils;
	}

	public EkeyDao getEkeyDao() {
		return mEkeyDao;
	}

	// 增加列名
	private void addField(DbUtils db, String tableName, String fieldName,
			String type) {
		if (tableIsExist(db, tableName)) {
			if (!fieldIsExist(db, tableName, fieldName)) {
				final String sql = "ALTER TABLE " + tableName + " ADD "
						+ fieldName + " " + type;
				try {
					db.execNonQuery(sql);
				} catch (DbException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// 判断表存在
	private static boolean tableIsExist(DbUtils db, String tableName) {
		boolean result = false;
		Cursor mCursor = null;
		final String sql = "SELECT COUNT(*)  as C FROM sqlite_master where type='table' and name=?";
		try {
			mCursor = db
					.execQuery(new SqlInfo(sql, new Object[] { tableName }));
			if (mCursor.moveToNext()) {
				int count = mCursor.getInt(0);
				if (count > 0) {
					result = true;
				}
			}
			mCursor.close();
		} catch (DbException e) {
			e.printStackTrace();
		}
		return result;
	}

	// 判断列存在
	private static boolean fieldIsExist(DbUtils db, String tableName,
			String fieldName) {
		boolean result = false;
		final String sql = "select sql from sqlite_master where tbl_name=? and type='table'";
		Cursor mCursor;
		try {
			mCursor = db
					.execQuery(new SqlInfo(sql, new Object[] { tableName }));
			if (mCursor.moveToNext()) {
				String createSql = mCursor.getString(0);
				if (createSql.indexOf(fieldName) > 0) {
					result = true;
				}
			}
			mCursor.close();
		} catch (DbException e) {
			e.printStackTrace();
		}

		return result;
	}

}
